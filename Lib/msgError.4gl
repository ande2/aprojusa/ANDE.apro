
#
# Graba en log y muestra en pantalla 
# mensaje de error correspondiente a codigo de status  
#
FUNCTION msgError(sqlstatus, mensaje)
DEFINE sqlstatus INTEGER
DEFINE mensaje STRING 

   CALL errorlog(sqlstatus USING "-<<<<"||" - "||err_get(sqlstatus) CLIPPED)
   ERROR err_get(sqlstatus)
   LET mensaje = 
      "El error numero ", sqlstatus USING "-<<<<", " ha ocurrido.\n",
      "Al intentar ",mensaje CLIPPED  
   CALL msg(mensaje)
END FUNCTION    