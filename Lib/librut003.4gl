{
Programa : librut003.4gl 
Programo : Mynor Ramirez
Objetivo : Subruitinas para crear combobox 
}

DATABASE aprojusa

DEFINE qrytext STRING 

-- Subrutina que carga el combobox de las empresas 

FUNCTION librut003_cbxempresas() 
 -- Llenando combobox 
 LET qrytext = "SELECT a.codemp,a.nomemp ",
               " FROM glb_empresas a ",
               " ORDER BY 2 "

 CALL librut002_combobox("codemp",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los vehiculos

FUNCTION librut003_CbxVehiculos()
 -- Llenando combobox 
 LET qrytext = "SELECT a.numveh,a.numpla ",
               " FROM vehiculos a ",
               " WHERE a.estado = 1 ", 
               " ORDER BY 2 "

 CALL librut002_combobox("numveh",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los productos

FUNCTION librut003_CbxProductos() 
 -- Llenando combobox 
 LET qrytext = "SELECT a.cditem,a.dsitem ",
               " FROM  inv_products a ",
               " WHERE a.estado = 1 ", 
               " ORDER BY 2 "

 CALL librut002_combobox("cditem",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de productos x categoria o x subcategoria 

{FUNCTION librut003_CbxProductosCatSub(wcodcat,wsubcat)
 DEFINE wcodcat   LIKE inv_products.codcat,
        wsubcat   LIKE inv_products.subcat,
        strcodcat STRING, 
        strsubcat STRING

 -- Verificando categoria
 LET strcodcat = NULL
 IF wcodcat IS NOT NULL THEN
    LET strcodcat = " AND a.codcat = "||wcodcat
 END IF

 -- Verificando subcategoria
 LET strsubcat = NULL
 IF wsubcat IS NOT NULL THEN
    LET strsubcat = " AND a.subcat = "||wsubcat
 END IF

 -- Llenando combobox
 LET qrytext = "SELECT a.cditem,a.dsitem ",
               " FROM  inv_products a ",
               " WHERE a.cditem IS NOT NULL ",
               strcodcat CLIPPED,
               strsubcat CLIPPED,
               " ORDER BY 2 "

 CALL librut002_combobox("cditem",qrytext)
END FUNCTION
}
-- Subrutina que carga el combobox de los camiones en transito

{FUNCTION librut003_CbxCamionesTransito(wcodemp)
 DEFINE wcodemp LIKE transacciones.codemp

 -- Llenando combobox 
 LET qrytext = 
  "SELECT a.lnktra,a.placam||' '||a.fecing||' '||",
     "a.horing||' '||TRIM(a.nomcon)||' '||NVL(TRIM(a.numcon),'-')||' '||",
     "CASE (a.tipmov) WHEN 1 THEN 'RECEPCION' WHEN 2 THEN 'SALIDA' END ",
  " FROM transacciones a ",
  " WHERE a.codemp = "||wcodemp||" AND a.transt = 1 AND a.estado = 1",
  " ORDER BY 1 "

 CALL librut002_combobox("camtra",qrytext)
END FUNCTION}

-- Subrutina que carga el combobox de los proveedores

FUNCTION librut003_CbxProveedores()
 -- Llenando combobox 
 LET qrytext = "SELECT a.codprv,a.nomprv ",
               " FROM  inv_provedrs a ",
               " WHERE a.estado = 1 ", 
               " ORDER BY 2 "

 CALL librut002_combobox("codprv",qrytext)
END FUNCTION

-- Subrutina que carga el combobox los perfiles 

FUNCTION librut003_cbxperfiles() 
 -- Llenando combobox 
 LET qrytext = "SELECT a.roleid,a.nmrole ",
               " FROM glb_rolesusr a ",
               " ORDER BY 2 "

 CALL librut002_combobox("roleid",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los clientes

FUNCTION librut003_CbxClientes() 
 -- Llenando combobox 
 LET qrytext = "SELECT a.codcli,a.nomcli ",
               " FROM fac_clientes a ",
               " WHERE a.estado = 1 ", 
               " ORDER BY 2 "

 CALL librut002_combobox("codcli",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de las categorias 

FUNCTION librut003_cbxcategorias()
 -- Llenando combobox
 LET qrytext = "SELECT a.codcat,a.nomcat ",
               " FROM glb_categors a ",
               " ORDER BY 2 "

 CALL librut002_combobox("codcat",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de las subcategorias 

{FUNCTION librut003_cbxsubcategorias(wcodcat)
 DEFINE wcodcat LIKE glb_subcateg.codcat

 -- Llenando combobox
 LET qrytext = "SELECT a.subcat,a.nomsub ",
               " FROM  glb_subcateg a ",
               " WHERE a.codcat = ",wcodcat,
               " ORDER BY 2 "

 CALL librut002_combobox("subcat",qrytext)
END FUNCTION}

-- Subrutina que carga el combobox de los numeros de bascula 

FUNCTION librut003_CbxNumeroBasculas(nbasculas) 
 DEFINE nbasculas SMALLINT 

 -- Verificando numero de basculas
 CASE (nbasculas)
  WHEN 1 CALL librut002_combobox("numbas","Uno")   
  WHEN 2 CALL librut002_combobox("numbas","Uno|Dos")   
  WHEN 3 CALL librut002_combobox("numbas","Uno|Dos|Tres")   
  WHEN 4 CALL librut002_combobox("numbas","Uno|Dos|Tres|Cuatro")   
  WHEN 5 CALL librut002_combobox("numbas","Uno|Dos|Tres|Cuatro|Cinco")   
  WHEN 6 CALL librut002_combobox("numbas","Uno|Dos|Tres|Cuatro|Cinco|Seis")   
 END CASE 
END FUNCTION

-- Subrutina para buscar los datos de una empresa (glb_empresas) 

{FUNCTION librut003_bempresa(wcodemp)
 DEFINE w_mae_emp RECORD LIKE glb_empresas.*,
        wcodemp   LIKE glb_empresas.codemp

 INITIALIZE w_mae_emp.* TO NULL 
 SELECT a.*
  INTO  w_mae_emp.*
  FROM  glb_empresas a
  WHERE (a.codemp = wcodemp)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_emp.*,FALSE
  ELSE
     RETURN w_mae_emp.*,TRUE
  END IF
END FUNCTION}

-- Subrutina para buscar los datos de un usuario (glb_usuarios) 

{FUNCTION librut003_busuario(wuserid)
 DEFINE w_mae_reg RECORD LIKE glb_usuarios.*,
        wuserid   LIKE glb_usuarios.userid

 INITIALIZE w_mae_reg.* TO NULL 
 SELECT a.*
  INTO  w_mae_reg.*
  FROM  glb_usuarios a
  WHERE (a.userid= wuserid)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_reg.*,FALSE
  ELSE
     RETURN w_mae_reg.*,TRUE
  END IF
END FUNCTION}

-- Subrutina para obtener parametros del sistema

FUNCTION librut003_parametros(wnumpar,wtippar)
 DEFINE wnumpar LIKE glb_paramtrs.numpar,
        wtippar LIKE glb_paramtrs.tippar,
        wvalchr LIKE glb_paramtrs.valchr

 -- Seleccionando parametros
 INITIALIZE wvalchr TO NULL
 SELECT a.valchr
  INTO  wvalchr
  FROM  glb_paramtrs a
  WHERE (a.numpar = wnumpar)
    AND (a.tippar = wtippar)
  IF (status!=NOTFOUND) THEN
     RETURN TRUE,wvalchr
  ELSE
     RETURN FALSE,wvalchr
  END IF
END FUNCTION

{
-- Subrutina para buscar los datos de un programa (glb_programs)

FUNCTION librut003_bpermprog(wcodpro,wprogid)
 DEFINE wcodpro   LIKE glb_dprogram.codpro,
        wprogid   LIKE glb_dprogram.progid

 SELECT UNIQUE a.progid
  FROM  glb_dprogram a
  WHERE (a.codpro= wcodpro)
    AND (a.progid= wprogid)
  IF (status=NOTFOUND) THEN
     RETURN FALSE
  ELSE
     RETURN TRUE
  END IF
END FUNCTION}

-- Subrutina para buscar los datos de un programa (glb_programs)

{FUNCTION librut003_bprograma(wcodpro)
 DEFINE w_mae_reg RECORD LIKE glb_programs.*,
        wcodpro   LIKE glb_programs.codpro

 INITIALIZE w_mae_reg.* TO NULL
 SELECT a.*
  INTO  w_mae_reg.*
  FROM  glb_programs a
  WHERE (a.codpro= wcodpro)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_reg.*,FALSE
  ELSE
     RETURN w_mae_reg.*,TRUE
  END IF
END FUNCTION
}
-- Subrutina para contar los registros seleccionados de un qry

FUNCTION librut003_NumeroRegistros(qry)
 DEFINE qry     STRING,
        xdato   CHAR(1),
        totreg  INT

 -- Contandoregistros
 PREPARE nrows FROM qry
 DECLARE nrg CURSOR FOR nrows
 LET totreg = 1
 FOREACH nrg INTO xdato
  LET totreg = totreg+1
 END FOREACH
 CLOSE nrg
 FREE  nrg
 LET totreg = totreg-1
 RETURN totreg
END FUNCTION

-- Subrutina para grabar la bitacora de pesos 

{FUNCTION librut003_BitacoraPesos(w_mae_pes) 
 DEFINE w_mae_pes  RECORD LIKE inv_regpesos.*  

 -- Asignando datos
 LET w_mae_pes.fecsis = CURRENT
 LET w_mae_pes.horsis = CURRENT HOUR TO SECOND 
 
 SET LOCK MODE TO WAIT 
 INSERT INTO inv_regpesos
 VALUES (w_mae_pes.*) 
END FUNCTION} 
