FUNCTION login(nombase)

   DEFINE nombase    STRING 
   DEFINE usuario    STRING
   DEFINE clave      STRING
   DEFINE titwin     STRING

   LET usuario=NULL
   LET clave=NULL
   
   LET titwin=%"Por favor informe sus datos de acceso al sistema"

   OPEN WINDOW w_login WITH FORM "login" ATTRIBUTES (TEXT=titwin, STYLE="login")
   
   LET int_flag = 0

   INPUT BY NAME usuario, clave
         ON ACTION CANCEL
            LET usuario=NULL
            LET clave=NULL
            EXIT INPUT
         BEFORE INPUT 
            DISPLAY "pase"
            DISPLAY "sema_ama.gif" TO semaforo
            --DISPLAY "sema_rojo.png" TO semaforo
         AFTER INPUT
            IF LENGTH(usuario)=0 THEN
               ERROR "Es obligatorio informar el usuario"
               NEXT FIELD usuario
            END IF
            IF LENGTH(clave)=0 THEN
               ERROR "Es obligatorio informar la contrase�a"
               NEXT FIELD clave
            END IF
DISPLAY "nombase ", nombase, "usuario ", usuario, "clave", clave
            WHENEVER ERROR CONTINUE
            CONNECT TO nombase USER usuario USING clave
            WHENEVER ERROR STOP
            IF STATUS=0 THEN
               DISPLAY "sema_verde.png" TO semaforo
               MESSAGE %"Usuario y contrase�a validados OK"
               CALL ui.Interface.refresh()
               SLEEP 2
            ELSE
               ERROR %"El usuario y/o la contrase�a son inv�lidos"
               DISPLAY "sema_rojo.png" TO semaforo
               NEXT FIELD usuario
            END IF
   END INPUT

   CLOSE WINDOW w_login

   RETURN usuario

END FUNCTION
