################################################################################
# Programa    : boxes.4gl
# Funcion     :
# Descripcion : Programa que contiene los boxes que se pueden utilizar en los
#					 sistemas para que funcione como anteriormente lo hacia box_lib
# Funciones   :
#
#
#
# Parametros
# Recibidos   :
# Paramscros
# Devueltos   :
#
# SCCS id No. :
# Autor       : Erick Valdez
# Fecha       : 23/05/05
# Path        : /solc/solc.app/libgral_gen.dir/boxes.4gl
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
################################################################################

FUNCTION box_inter()
	MENU "Confirmaci�n de Interrupci�n" 
		ATTRIBUTE(STYLE="dialog",COMMENT="Est� seguro de cancelar el proceso?",IMAGE="question")
		COMMAND "Si"
			LET int_flag = TRUE
			EXIT MENU
		COMMAND "No"
			LET int_flag = FALSE
			EXIT MENU
	END MENU
	RETURN int_flag
END FUNCTION

FUNCTION box_elifila()
	MENU "Confirmaci�n para la Eliminaci�n" 
		ATTRIBUTE(STYLE="dialog",COMMENT="Est� seguro de querer eliminar el registro?",IMAGE="br_error")
		COMMAND "Si"
			LET int_flag = TRUE
			EXIT MENU
		COMMAND "No"
			LET int_flag = FALSE
			EXIT MENU
	END MENU
	RETURN int_flag
END FUNCTION

FUNCTION box_valdato(mensaje)
DEFINE
	mensaje STRING --CHAR(100)

   MENU "Advertencia"
      ATTRIBUTE(STYLE="dialog",COMMENT=mensaje CLIPPED,IMAGE="exclamation")
      COMMAND "Aceptar"
         LET int_flag = FALSE
         EXIT MENU
   END MENU
END FUNCTION

FUNCTION box_gradato(mensaje)
DEFINE
	mensaje,respuesta CHAR(100)

   MENU "Confirmación del Sistema"
      ATTRIBUTE(STYLE="dialog",COMMENT=mensaje CLIPPED,IMAGE="question")
      COMMAND "Si"
         LET respuesta = "Si" CLIPPED
         EXIT MENU
      COMMAND "No"
         LET respuesta = "No" CLIPPED
         EXIT MENU
      COMMAND "Cancelar"
         LET respuesta = "Cancel" CLIPPED
         EXIT MENU
   END MENU
   RETURN respuesta
END FUNCTION

FUNCTION box_error(mensaje)
DEFINE
   mensaje CHAR(100)

   MENU "Atenci�n"
      ATTRIBUTE(STYLE="dialog",COMMENT=mensaje CLIPPED,IMAGE="stop")
      COMMAND "Aceptar"
         LET int_flag = FALSE
         EXIT MENU
   END MENU
END FUNCTION

FUNCTION box_pregunta(mensaje)
DEFINE
   mensaje,respuesta CHAR(100)

   MENU "Confirmación del Sistema"
      ATTRIBUTE(STYLE="dialog",COMMENT=mensaje CLIPPED,IMAGE="question")
      COMMAND "Si"
         LET respuesta = "Si" CLIPPED
         EXIT MENU
      COMMAND "No"
         LET respuesta = "No" CLIPPED
         EXIT MENU
   END MENU
   RETURN respuesta
END FUNCTION

FUNCTION box_confirma(mensaje)
DEFINE
   mensaje CHAR(100),
   flagDel SMALLINT  

   MENU "Confirmación del Sistema"
      ATTRIBUTE(STYLE="dialog",COMMENT=mensaje CLIPPED,IMAGE="question")
      COMMAND "Si"
         LET flagDel = 1 
         EXIT MENU
      COMMAND "No"
         LET flagDel = 0  
         EXIT MENU
   END MENU
   RETURN flagDel
END FUNCTION

