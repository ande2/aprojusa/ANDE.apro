DATABASE aprojusa 

MAIN

DEFINE runcmd, runcmdzip  STRING 
DEFINE g_reg RECORD
   contnote_contact  LIKE contnote.contnote_contact,
   contnote_num      LIKE contnote.contnote_num
END RECORD 
DEFINE fecha         DATE 
DEFINE lNomReport    STRING
DEFINE lzipname      STRING 

   CALL fgl_settitle("Reporte Diario de Imágenes")
   CALL ui.Interface.loadActionDefaults("actiondefaults")
   
   MENU 
      ON ACTION repdiarioimg
      
         --INPUT BY NAME fecha
         
         DECLARE ccur CURSOR FOR 
            SELECT DISTINCT n.contnote_contact, n.contnote_num
            FROM contnote n, contnotedocs d
            WHERE n.contnote_num = d.contnote_num
            AND   d.contdocs_tipo = 'I'
            AND   d.contdocs_doc IS NOT NULL 
            AND   DATE(n.contnote_rec_mtime) = TODAY --fecha

         LET lzipname   = "Reporte_Diario_imagenes.zip"
         LET runcmdzip = "rm ", lzipname, "; ", "zip -r ", lzipname
         
         FOREACH ccur INTO g_reg.contnote_contact, g_reg.contnote_num

            LET lNomReport = "Caso_", g_reg.contnote_contact USING "<<<<<<<<"
            
            LET runcmdzip = runcmdzip CLIPPED, " ", lNomReport, ".rtf"
            
            LET runcmd = "fglrun apror0715.42r", g_reg.contnote_contact, g_reg.contnote_num, " REPORTE ", lNomReport 
            RUN runcmd
            LET runcmd = "rm ", lNomReport
         END FOREACH 

         DISPLAY "runcmdzip ", runcmdzip
         RUN runcmdzip
         CALL fgl_putfile(lzipname, lzipname) 
          LET runcmdzip = "rm ", lzipname
          RUN runcmdzip
      ON ACTION salir
         EXIT MENU 
   END MENU 

END MAIN 