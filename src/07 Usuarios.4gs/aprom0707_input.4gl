
GLOBALS "aprom0707_glob.4gl"

FUNCTION captura_datos(operacion)
DEFINE operacion CHAR (1)
DEFINE resultado BOOLEAN
DEFINE w ui.Window
DEFINE f ui.Form

   LET resultado = FALSE 
   LET u_reg.* = g_reg.*
   IF operacion = 'I' THEN 
      INITIALIZE g_reg.* TO NULL
      DISPLAY BY NAME g_reg.*
   END IF
   DIALOG ATTRIBUTES(UNBUFFERED)
     INPUT BY NAME  g_reg.usuid, g_reg.usugrpid, g_reg.city_num, g_reg.city_num_alt, 
      g_reg.user_name, g_reg.user_id, g_reg.user_auth, g_reg.user_mod_cobro, 
      g_reg.user_mod_cobro_lic, g_reg.user_mod_cobro_adm, g_reg.user_mod_notar
      ATTRIBUTES (WITHOUT DEFAULTS)

      BEFORE INPUT
         CALL DIALOG.setActionHidden("close",TRUE)
         LET g_reg.user_auth = ga_pwd[g_reg.usuid]
         DISPLAY BY NAME g_reg.user_auth

      {ON ACTION showpwd 
         CALL showpasswd()}
         
      ON CHANGE user_id
        LET g_reg.user_id = g_reg.user_id CLIPPED  
        SELECT user_id FROM users
        WHERE user_id = g_reg.user_id 
        IF sqlca.sqlcode = 0 THEN
           CALL msg("Login ya existe")
           NEXT FIELD CURRENT 
        END IF 
          
      AFTER FIELD user_id
        IF g_reg.user_id IS NULL THEN
           CALL msg("Ingrese login")
           NEXT FIELD CURRENT  
        END IF 

      ON CHANGE user_name
        LET g_reg.user_name = g_reg.user_name CLIPPED  
        SELECT user_name FROM users
        WHERE user_name = g_reg.user_name 
        IF sqlca.sqlcode = 0 THEN
           CALL msg("Nombre ya existe")
           NEXT FIELD CURRENT 
        END IF 

      ON CHANGE user_auth
         DISPLAY "en el on chnge"
         CALL ga_pwd.remove(g_reg.usuid)
         LET ga_pwd[g_reg.usuid] = g_reg.user_auth
          
      AFTER FIELD user_name
        IF g_reg.user_name IS NULL THEN
           CALL msg("Ingrese nombre")
           NEXT FIELD CURRENT  
        END IF 

      
     
         

      {AFTER FIELD dbname 
        IF g_reg.nombre IS NOT NULL THEN
           SELECT id_commemp FROM commemp WHERE commemp.dbname = g_reg.dbname
           IF sqlca.sqlcode = 0 THEN 
              CALL msg("Nombre de base de datos ya existe")
              NEXT FIELD CURRENT
           END IF 
        END IF}
 
 
            --IF (operacion = 'I') OR (g_reg.emNomCt <> u_reg.emNomCt) THEN 
            --IF existe_cat_cod(g_reg.idBautizo) THEN 
           
               --LET g_reg.idBautizo = NULL 
            
            --END IF   
         --END IF
      
   END INPUT 

   ON ACTION ACCEPT
      
        SELECT NVL(MAX(usuid)+1,1) INTO g_reg.usuid FROM users 
        LET ga_pwd[g_reg.usuid] = g_reg.user_auth
        IF g_reg.user_id IS NULL THEN
           CALL msg("Ingrese nombre")
           NEXT FIELD user_id  
        END IF

        IF g_reg.user_name IS NULL THEN
           CALL msg("Ingrese código")
           NEXT FIELD user_name  
        END IF
        {IF g_reg.nombre IS NULL THEN
           CALL msg("Debe ingresar nombre")
           NEXT FIELD CURRENT
        END IF  

        IF g_reg.espropia IS NULL THEN
           CALL msg("Debe especificar si es empresa propia")
           NEXT FIELD esPropia
        END IF }

      
      --IF g_reg.librob IS NULL THEN
         --CALL msg("Debe ingresar nombre")
         --NEXT FIELD librob
      --END IF
      --IF (operacion = 'I') OR (g_reg.cat_id <> u_reg.cat_id) THEN 
         --IF existe_cat_id(g_reg.cat_id) THEN 
            --CALL msg("Este Id. ya esta siendo utilizado")
            --LET g_reg.cat_id = NULL 
            --NEXT FIELD cat_id 
         --END IF   
      --END IF


      {IF (operacion = 'I') OR (g_reg.librob <> u_reg.librob) THEN 
         IF existe_cat_nom(g_reg.librob) THEN 
            CALL msg("Este nombre ya esta siendo utilizado")
            LET g_reg.librob = NULL 
            NEXT FIELD librob 
         END IF   
      END IF}
      IF operacion = 'M' AND g_reg.* = u_reg.* THEN
         CALL msg("No se efectuaron cambios")
         EXIT DIALOG 
      END IF
      CASE box_gradato("Seguro de grabar")
         WHEN "Si"
            LET resultado = TRUE
            EXIT DIALOG
         WHEN "No"
            EXIT DIALOG 
         OTHERWISE
            CONTINUE DIALOG 
      END CASE 
      LET resultado = TRUE
      EXIT DIALOG 

   
      
     ON ACTION CANCEL
        EXIT dialog
   END DIALOG
   
   IF NOT resultado THEN
      LET g_reg.* = u_reg.*
      DISPLAY BY NAME g_reg.* 
   END IF 
   RETURN resultado 
END FUNCTION
