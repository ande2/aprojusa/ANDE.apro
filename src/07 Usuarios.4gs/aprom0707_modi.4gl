################################################################################
# Funcion     : %M%
# Descripcion : Funcion para modificar datos de un umdor
# Funciones   : updel_init()
#	             delete_umd() 
#               update_umd()
#               pk_estado()
#               valida_modi()
#               verifica_modi()
#               existe_datos()
#               existe_dpro()
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################
GLOBALS "aprom0707_glob.4gl"

FUNCTION update_init()
DEFINE strSql STRING 

   LET strSql = 
      "UPDATE users ",
      "SET ",
      " usugrpid           = ?,  ",
      " city_num           = ?,  ",
      " city_num_alt       = ?,  ",
      " user_name          = ?,  ",
      " user_id            = ?,  ",
      " user_auth          = ?,  ",
      " user_mod_cobro     = ?,  ",
      " user_mod_cobro_lic = ?,  ",
      " user_mod_cobro_adm = ?,  ",
      " user_mod_notar     = ?   ",
      " WHERE usuid        = ?   "

   PREPARE st_modificar FROM strSql
END FUNCTION 

FUNCTION modifica()
   CALL encabezado("Modificar")
   --CALL info_usuario()
   
   IF captura_datos('M') THEN
      RETURN actualizar()
   ELSE 
      RETURN FALSE 
   END IF 
END FUNCTION 

FUNCTION actualizar()
   LET g_reg.user_auth = ga_pwd[g_reg.usuid]
   TRY
      EXECUTE st_modificar USING 
      g_reg.usugrpid, g_reg.city_num, g_reg.city_num_alt, g_reg.user_name, g_reg.user_id, g_reg.user_auth, 
      g_reg.user_mod_cobro, g_reg.user_mod_cobro_lic, g_reg.user_mod_cobro_adm, g_reg.user_mod_notar,
      u_reg.usuid
   CATCH 
      CALL msgError(sqlca.sqlcode,"Modificar Registro")
      RETURN FALSE 
   END TRY
   LET g_reg.user_auth = "*****"
   DISPLAY BY NAME g_reg.user_auth
   CALL msg ("Registro actualizado")
   RETURN TRUE 
END FUNCTION 

FUNCTION delete_init()
DEFINE strSql STRING 

   LET strSql =
      "DELETE FROM users ",
      "WHERE usuid = ? "
      
   PREPARE st_delete FROM strSql

END FUNCTION 

FUNCTION anular()
   --Cuando la tabla es corporativa (creada en ANDE) y necesita validar que 
   -- no se haya usado en las bases de datos de empresas (db0001, db0002, etc)
   {IF NOT can_deleteAll("pemmfinca", "id_commemp", g_reg.id_commemp) THEN
      CALL msg("Empresa no se puede eliminar, ya existe en finca(s)")
      RETURN FALSE 
   END IF  }
   IF NOT box_confirma("Esta seguro de anular el registro") THEN
      RETURN FALSE
   END IF 
   TRY 
      EXECUTE st_delete USING g_reg.city_num
   CATCH 
      CALL msgError(sqlca.sqlcode,"Eliminar Registro")
      RETURN FALSE 
   END TRY
   CALL msg("Registro eliminado")
   RETURN TRUE 
END FUNCTION 

