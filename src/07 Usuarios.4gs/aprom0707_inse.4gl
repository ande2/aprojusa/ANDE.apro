################################################################################
# Funcion     : %M%
# Descripcion : Ingreso de un nuevo umdor 	
# Funciones   : input_umd() funcion para insertar datos
#              
#              
#               
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Anderson Garcia 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################

GLOBALS "aprom0707_glob.4gl"

FUNCTION insert_init()
DEFINE strSql STRING 

   LET strSql =
      "INSERT INTO users ( ",
        " usuid, usugrpid, city_num, city_num_alt, user_name, user_id, user_auth, user_status, ",
        " user_mod_cobro, user_mod_cobro_lic, user_mod_cobro_adm, user_mod_notar ",
        "   ) ",
      " VALUES (?,?,?,?,?,?,?,?,?,?,?,?)"

   PREPARE st_insertar FROM strSql

END FUNCTION 

FUNCTION ingreso()
   CALL encabezado("Ingresar")
   --CALL info_usuario()
   IF captura_datos('I') THEN
      RETURN grabar()
   ELSE 
      RETURN FALSE 
   END IF 
END FUNCTION 

FUNCTION grabar()
DEFINE u_status LIKE users.user_status

   SELECT NVL(MAX(usuid)+1,1) INTO g_reg.usuid FROM users
   LET g_reg.user_auth = ga_pwd[g_reg.usuid]
   LET u_status = 1
   TRY 
      EXECUTE st_insertar USING 
            g_reg.usuid, g_reg.usugrpid, g_reg.city_num, g_reg.city_num_alt, g_reg.user_name, 
            g_reg.user_id, g_reg.user_auth, u_status, g_reg.user_mod_cobro,
            g_reg.user_mod_cobro_lic, g_reg.user_mod_cobro_adm, g_reg.user_mod_notar 

      --CS agregarlo cuando se agregue el ID 
      --LET g_reg.idpais = SQLCA.sqlerrd[2]
   CATCH 
      CALL msgError(sqlca.sqlcode,"Grabar Registro")
      RETURN FALSE 
   END TRY
   DISPLAY BY NAME g_reg.*
   CALL box_valdato ("Registro agregado")
   --CALL combo_din2("cmbdepartamento","SELECT * FROM commdep WHERE id_commdep > 0")
   RETURN TRUE 
END FUNCTION 