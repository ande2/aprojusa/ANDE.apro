################################################################################
# Funcion     : %M%
# nombre      : Catalogo de Tipos
#               Funcion para definicion de globales
# Funciones   : 
#               
#               
#  
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        nombre de la modificacion
#
################################################################################
--DATABASE comisiones
SCHEMA aprojusa

GLOBALS 
TYPE 
   tDet RECORD 
      usuid                LIKE users.usuid,
      usugrpid             LIKE users.usugrpid,
      city_num             LIKE users.city_num,
      city_num_alt         LIKE users.city_num_alt,
      user_name            LIKE users.user_name,
      user_id              LIKE users.user_id,
      user_auth            LIKE users.user_auth,
      user_mod_cobro       LIKE users.user_mod_cobro,
      user_mod_cobro_lic   LIKE users.user_mod_cobro_lic,
      user_mod_cobro_adm   LIKE users.user_mod_cobro_adm,
      user_mod_notar       LIKE users.user_mod_notar
   END RECORD

DEFINE reg_det_attr DYNAMIC ARRAY OF RECORD 
      usuid                STRING,
      usugrpid             STRING,
      city_num             STRING,
      city_num_alt         STRING,
      user_name            STRING,
      user_id              STRING,
      user_auth            STRING,
      user_mod_cobro       STRING,
      user_mod_cobro_lic   STRING,
      user_mod_cobro_adm   STRING,
      user_mod_notar       STRING
   END RECORD
   
DEFINE
   reg_det DYNAMIC ARRAY OF tDet, 
   g_reg, u_reg tDet,
   dbname      STRING,
   
   condicion   STRING --Condicion de la clausula Where 
   CONSTANT    prog_name = "aprom0707"
   
{TYPE t_pwd RECORD
   usuid       LIKE users.usuid,
   user_auth   LIKE users.user_auth
END RECORD
 
DEFINE g_pwd   t_pwd
   
DEFINE ga_pwd DYNAMIC ARRAY OF t_pwd}

DEFINE ga_pwd DICTIONARY OF STRING

DEFINE fshowpwd SMALLINT  

END GLOBALS