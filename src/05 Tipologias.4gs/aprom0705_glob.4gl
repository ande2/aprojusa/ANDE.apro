################################################################################
# Funcion     : %M%
# nombre      : Catalogo de Tipos
#               Funcion para definicion de globales
# Funciones   : 
#               
#               
#  
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        nombre de la modificacion
#
################################################################################
--DATABASE comisiones
SCHEMA aprojusa

GLOBALS 
TYPE 
   tDet RECORD 
      orden          LIKE tipologia.orden,
      cod_tipologia  LIKE tipologia.cod_tipologia,
      des_tipologia  LIKE tipologia.des_tipologia,
      com_tipologia  LIKE tipologia.com_tipologia,
      estado         LIKE tipologia.estado
      
   END RECORD

DEFINE reg_det_attr DYNAMIC ARRAY OF RECORD 
      orden             STRING,
      cod_tipologia     STRING,
      des_tipologia     STRING,
      com_tipologia     STRING,
      estado            STRING
      
   END RECORD
   
DEFINE
   reg_det DYNAMIC ARRAY OF tDet, 
   g_reg, u_reg tDet,
   dbname      STRING,
   
   condicion   STRING --Condicion de la clausula Where 
   CONSTANT    prog_name = "aprom0705"
END GLOBALS