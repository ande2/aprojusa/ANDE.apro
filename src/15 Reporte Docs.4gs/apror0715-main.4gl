GLOBALS "apror0715_glob.4gl"

MAIN
   DEFINE cuantos            INTEGER 
   MENU
      BEFORE MENU
         IF num_args() > 1 THEN 
            LET lcontnote_contact   = arg_val(1)
            LET lcontnote_num       = arg_val(2)
            --Si se llamdo desde el programa de notas o desde el reporte del día
            LET lEjecutaDesde       = arg_val(3)
            LET lNomReport          = arg_val(4)
            
            DISPLAY "Reporte ", lcontnote_contact, " ejecuta desde ", lEjecutaDesde 
            LET cuantos = 0
            SELECT COUNT(*) INTO cuantos 
               FROM contnotedocs
               WHERE contact_num    = lcontnote_contact 
               AND   contnote_num   = lcontnote_num
               AND   contdocs_tipo  = 'I'
               DISPLAY "cuantos ", cuantos
            IF cuantos > 0 THEN
               CALL freporte()
            END IF 
            EXIT MENU
         END IF 
         
      ON ACTION reporte
         CALL freporte()
      ON ACTION salir 
         EXIT MENU 
   END MENU 

END MAIN 


FUNCTION freporte()

DEFINE handler om.SaxDocumentHandler

	If fgl_report_loadCurrentSettings("apror0715_rep1.4rp") THEN
      --CALL fgl_report_selectDevice("PDF")
      CALL fgl_report_selectDevice("RTF")
      
      IF lEjecutaDesde = "REPORTE" THEN 
         CALL fgl_report_selectPreview(FALSE)
         
         CALL fgl_report_setOutputFileName(lNomReport)
      END IF 
      
		LET handler = fgl_report_commitCurrentSettings()
	ELSE
		EXIT PROGRAM
	END IF

	IF handler IS NOT NULL THEN
		CALL run_report1_to_handler(handler)
	END IF
   
END FUNCTION 