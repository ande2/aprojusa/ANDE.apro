################################################################################
# Funcion     : %M%
# Descripcion : Ingreso de un registro nuevo 	
# Funciones   : input_umd() funcion para insertar datos
#              
#              
#               
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Anderson Garcia 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################

GLOBALS "aprom0700_glob.4gl"

FUNCTION insert_init()
DEFINE strSql STRING 

   LET strSql =
      "INSERT INTO contact ( ",
        " contact_num, contact_operacion, contact_producto, contact_name, contact_street, contact_garantia, ",
        " contact_total_cobrar, contact_estado_op, ", --, contact_tosync, ",
        --" idFinca, idEstructura, idValvula, idItem, estado ",
        " contact_dpi_cliente ",
        "   ) ",
      " VALUES (?,?,?,?,?,?,?,?,?)" --,?)"

   PREPARE st_insertar FROM strSql

   LET strSql =
      "INSERT INTO contnote ( ",
        " contnote_num, contnote_rec_muser, contnote_rec_mtime, contnote_contact, contnote_when, contnote_text ",
        "   ) ",
      " VALUES (?,?,?,?,?,?)"

   PREPARE st_insertarDet FROM strSql

END FUNCTION 

FUNCTION ingreso()
   CALL encabezado("Ingresar")
   --CALL info_usuario()
   IF captura_datos('I') THEN
      RETURN grabar()
   ELSE 
      RETURN FALSE 
   END IF 
END FUNCTION 

FUNCTION grabar()
   DEFINE i SMALLINT 
   --LET g_reg.idOrdPro = 0

   WHENEVER ERROR CONTINUE
   BEGIN WORK 
   TRY 
      EXECUTE st_insertar USING 
        g_reg.contact_num, g_reg.contact_operacion, g_reg.contact_producto, 
        g_reg.contact_name, g_reg.contact_street, g_reg.contact_garantia,
        g_reg.contact_total_cobrar, g_reg.contact_estado_op, --g_reg.idFinca,
        --g_reg.contact_tosync,
        --g_reg.idEstructura, g_reg.idValvula, g_reg.idItem,
        g_reg.contact_dpi_cliente
            
      --CS agregarlo cuando se agregue el ID 
      --LET g_reg.idOrdPro = SQLCA.sqlerrd[2]
      TRY 
         FOR i= 1 TO reg_detArr.getLength()
            LET reg_detArr[i].contnote_contact = g_reg.contact_num
            EXECUTE st_insertarDet USING 
              reg_detArr[i].contnote_num, reg_detArr[i].contnote_rec_muser,
              reg_detArr[i].contnote_rec_mtime, reg_detArr[i].contnote_contact,
              reg_detArr[i].contnote_when, reg_detArr[i].contnote_text 
         END FOR 
      CATCH 
         ROLLBACK WORK 
         WHENEVER ERROR STOP
         CALL msgError(sqlca.sqlcode,"Grabar Detalle de Registro")
         RETURN FALSE 
      END TRY
   CATCH 
      ROLLBACK WORK 
      WHENEVER ERROR STOP
      CALL msgError(sqlca.sqlcode,"Grabar Registro")
      RETURN FALSE 
   END TRY
   COMMIT WORK 
   WHENEVER ERROR STOP
   DISPLAY BY NAME g_reg.*
   CALL box_valdato ("Registro agregado")
   --CALL combo_din2("cmbdepartamento","SELECT * FROM commdep WHERE id_commdep > 0")
   RETURN TRUE 
END FUNCTION 