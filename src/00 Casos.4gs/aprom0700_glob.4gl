################################################################################
# Funcion     : %M%
# id_commdep  : Catalogo de Valvulas
#               Funcion para definicion de globales
# Funciones   : 
#               
#               
#  
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        id_commdep de la modificacion
#
################################################################################
DATABASE aprojusa

GLOBALS 
TYPE             
   tDet RECORD
      contact_num             LIKE contact.contact_num,            --id
      
      --Datos para asignación
      contact_cod_region      LIKE contact.contact_cod_region,    --Codigo de region
      contact_city            LIKE contact.contact_city,          --Sede a la que se asignó  
      contact_user            LIKE contact.contact_user,          --Usuario asignado
      contact_estado_caso     LIKE contact.contact_estado_caso,   --Estado del caso
      
      --Producto financiero
      contact_operacion       LIKE contact.contact_operacion,     --No de pr�stamo u operaci�n
      contact_producto        LIKE contact.contact_producto,      --Producto financiero
      contact_garantia        LIKE contact.contact_garantia,      --Garant�a
      contact_estado_op       LIKE contact.contact_estado_op,     --Estado actual del cr�dito
      --contact_tosync          LIKE contact.contact_tosync,

      --Datos del cliente
      contact_dpi_cliente     LIKE contact.contact_dpi_cliente,   --DPI
      contact_name            LIKE contact.contact_name,          --Nombre
      contact_street          LIKE contact.contact_street,        --Direcci�n
      contact_tel_cliente     LIKE contact.contact_tel_cliente,   --Telefonos
      contact_tel_casa        LIKE contact.contact_tel_casa,
      contact_tel_cel         LIKE contact.contact_tel_cel,
      contact_tel_trab        LIKE contact.contact_tel_trab,
      contact_tel_otro        LIKE contact.contact_tel_otro,
            
      --Datos de credito vencido --TAB1
      contact_saldo_cap       LIKE contact.contact_saldo_cap,     --Saldo a capita / saldo actual
      contact_cap_vencido     LIKE contact.contact_cap_vencido,   --Capital vencido
      contact_int_vencido     LIKE contact.contact_int_vencido,   --Inter�s vencido
      contact_saldo_imo       LIKE contact.contact_saldo_imo,     --Saldo ??
      contact_otros           LIKE contact.contact_otros,         --Otros / Otros vigentes
      contact_total           LIKE contact.contact_total,         --Salto total vencido
      contact_porc_serv       LIKE contact.contact_porc_serv,     --Valor actual del 7% por servicios
      contact_total_cobrar    LIKE contact.contact_total_cobrar,  --Total a cobrar
      contact_fec_ult_pago    LIKE contact.contact_fec_ult_pago,  --Fecha de �ltimo pago
      contact_cuotas_mora     LIKE contact.contact_cuotas_mora,   --No cuotas en mora
      contact_fec_mora        LIKE contact.contact_fec_mora,      --Fecha de mora
      
      --Datos de fiadores --TAB2
      contact_nom_fia1     LIKE contact.contact_nom_fia1,   --Nombre fiador 1
      contact_dir_fia1     LIKE contact.contact_dir_fia1,      --Direcci�n fiador 1
      contact_nom_fia2     LIKE contact.contact_nom_fia2,   --Nombre fiador 2
      contact_dir_fia2     LIKE contact.contact_dir_fia2,      --Direcci�n fiador 2
      contact_nom_fia3     LIKE contact.contact_nom_fia3,   --Nombre fiador 3
      contact_dir_fia3     LIKE contact.contact_dir_fia3,      --Direcci�n fiador 3

      --PIE DE PAGINA
      contact_rec_muser       LIKE contact.contact_rec_muser,     --Id usuario asignado
      contact_rec_mtime       LIKE contact.contact_rec_mtime,     --Fecha de actualizaci�n
      contact_rec_mstat       LIKE contact.contact_rec_mstat,     --Estado de sincronizaci�n
      
      contact_when            LIKE contact.contact_when           --Fecha de actualizaci�n

   END RECORD

DEFINE reg_det DYNAMIC ARRAY OF tDet
DEFINE ga_asigna DYNAMIC ARRAY OF tDet

--Para aplicar los estilos al arreglo principal
DEFINE reg_det_attr DYNAMIC ARRAY OF RECORD 
      contact_num             STRING,
      contact_cod_region      STRING,
      contact_city            STRING,
      contact_user            STRING,
      contact_estado_caso     STRING,

      contact_operacion       STRING,
      contact_producto        STRING,
      contact_garantia        STRING,
      contact_estado_op       STRING,
      --contact_tosync          STRING,
      
      contact_dpi_cliente     STRING,
      contact_name            STRING,
      contact_street          STRING,
      contact_tel_cliente     STRING,
      contact_tel_casa        STRING,
      contact_tel_cel         STRING,
      contact_tel_trab        STRING,
      contact_tel_otro        STRING,
      
      contact_saldo_cap       STRING,
      contact_cap_vencido     STRING,
      contact_int_vencido     STRING,
      contact_imo_saldo       STRING,
      contact_otros           STRING,
      contact_total           STRING,
      contact_porc_serv       STRING,
      contact_total_cobrar    STRING,
      contact_fec_ult_pago    STRING,
      contact_cuotas_mora     STRING,
      contact_fec_mora        STRING,
      
      contact_nom_fia1        STRING,
      contact_dir_fia1        STRING,
      contact_nom_fia2        STRING,
      contact_dir_fia2        STRING,
      contact_nom_fia3        STRING,
      contact_dir_fia3        STRING,
      
      contact_rec_muser       STRING,
      contact_rec_mtime       STRING,
      contact_rec_mstat       STRING,
      
      contact_when            STRING
   END RECORD

DEFINE g_reg, u_reg tDet

TYPE 
   tDetArr RECORD
      contnote_num         LIKE contnote.contnote_num,
      contnote_rec_muser   LIKE contnote.contnote_rec_muser,
      contnote_rec_mtime   LIKE contnote.contnote_rec_mtime,
      contnote_contact     LIKE contnote.contnote_contact,
      contnote_when        LIKE contnote.contnote_when,
      contnote_text        LIKE contnote.contnote_text
   END RECORD 

TYPE
   tDetTel RECORD 
      contact_num          LIKE contphone.contact_num,
      contphone_num        LIKE contphone.contphone_num,
      contphone_phone      LIKE contphone.contphone_phone
   END RECORD 

DEFINE grDetTel, grDetTelUpd DYNAMIC ARRAY OF tDetTel
DEFINE grTel tDetTel   
DEFINE reg_detArr DYNAMIC ARRAY OF tDetArr
DEFINE g_regArr, u_regArr tDetArr

DEFINE dbname      STRING
DEFINE usuario    STRING 
DEFINE grupo      LIKE grupo.grpnombre
DEFINE gsede      LIKE city.city_num
   
DEFINE condicion   STRING --Condicion de la clausula Where 
CONSTANT    prog_name = "aprom0700"
END GLOBALS