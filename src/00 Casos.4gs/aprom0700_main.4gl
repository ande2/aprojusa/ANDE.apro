################################################################################
# Funcion     : %M%
# Descripcion : Modulo Principal
# Funciones   : main_init(dbname)
#               main_menu()                               
#               dummy() 
#               
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo  
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################

GLOBALS "aprom0700_glob.4gl"

DEFINE usu_no_auth BOOLEAN

MAIN
	DEFINE 
		n_param 	SMALLINT,
		prog_name2 	STRING
       
		
	DEFER INTERRUPT

	OPTIONS
		INPUT  WRAP,
		HELP KEY CONTROL-W,
		COMMENT LINE OFF,
		PROMPT LINE LAST - 2,
		MESSAGE LINE LAST - 1,
		ERROR LINE LAST

	--LET n_param = num_args()

	{IF n_param = 0 THEN
		RETURN
	ELSE
        LET dbname = arg_val(1)
		CONNECT TO dbname
	END IF}
   
   LET usuario = arg_val(1)

   --Para usuarios tipo Banrural, que son administradores pero solo lectura
   SELECT id_config FROM cpj_config WHERE valor = usuario AND tipo = "usuarios"
   IF sqlca.sqlcode = 0 THEN LET usu_no_auth = TRUE ELSE LET usu_no_auth = FALSE END IF  
   
   LET grupo = grpusu(usuario)
   SELECT city_num INTO gsede FROM users WHERE user_id = usuario
   
	LET prog_name2 = prog_name||".log"   -- El progr_name es definido como constante en el arch. globals
    
	CALL STARTLOG(prog_name2)
	CALL main_init()
END MAIN

FUNCTION main_init()
DEFINE 
	nom_forma 	STRING,
    w           ui.Window,
    f           ui.Form
	  
	INITIALIZE u_reg.* TO NULL
	LET g_reg.* = u_reg.*

   --CALL trae_usuario() returning g_usuario.*
   --CALL trae_empresa(dbname) returning g_empresa.*
   --CALL trae_programa(prog_name) returning g_programa.*
   --IF g_programa.prog_id = 0 OR 
      --g_programa.prog_id IS NULL THEN
      --CALL box_valdato("Programa no existente, contacte a sistemas")
      --RETURN 
   --END IF 
    CALL ui.Interface.loadActionDefaults("actiondefaults")
    CALL ui.Interface.loadStyles("styles_sc")
   --CALL ui.Interface.loadToolbar("toolbar")

	LET nom_forma = prog_name CLIPPED, "_form"
    --LET nom_forma = "catm0202_form"
    CLOSE WINDOW SCREEN 
	OPEN WINDOW w1 WITH FORM nom_forma
   -- CALL fgl_settitle("ANDE - Orden de Producci�n")
   CALL set_title()
   
    --CALL setEnvForm()
	LET w = ui.WINDOW.getcurrent()
	LET f = w.getForm()
	--CALL fgl_settitle(g_programa.prog_des)
    
	 CALL insert_init()
    CALL update_init()
    CALL delete_init()
    CALL combo_init()
	 CALL main_menu()
END FUNCTION                                                                    

FUNCTION main_menu()                               
DEFINE  
    cuantos, cuantos2, 
    id, ids     SMALLINT,
    vopciones   CHAR(255), 
    cnt 		SMALLINT,  
    tol ARRAY[15] OF RECORD
      acc       CHAR(15),
      des_cod   SMALLINT
    END RECORD,
    aui, tb, tbi, tbs om.DomNode
    DEFINE runcmd, qry STRING 
    DEFINE i SMALLINT 
    LET cnt 		= 1
    DISPLAY "Seguimiento de Casos" TO gtit_enc
   
	 DISPLAY ARRAY reg_det TO sDet.*
      ATTRIBUTE(ACCEPT=FALSE,CANCEL=FALSE,UNBUFFERED,KEEP CURRENT ROW, FOCUSONFIELD)
   
      BEFORE DISPLAY
         
         LET cuantos = consulta(FALSE)
         IF cuantos > 0 THEN 
            CALL dialog.setCurrentRow("sdet",1)
            LET g_reg.* = reg_det[1].*
            CALL displayEnc()
            CALL displayDetArr()
            LET cuantos2 = displayPhone(g_reg.contact_num) 
            IF cuantos2 > 0 THEN 
               DISPLAY ARRAY grDetTel TO sDet3.*
                  BEFORE DISPLAY
                     EXIT DISPLAY
               END DISPLAY 
            ELSE 
               CALL grDetTel.CLEAR()
            END IF 
            CALL setAttr(1)
            CALL DIALOG.setCellAttributes(reg_det_attr)
            DISPLAY BY NAME cuantos
         END IF 
         CALL encabezado("")
         --CALL info_usuario()
         
         IF grupo != 'ADMINISTRADORES' THEN            
            CALL dialog.setActionHidden ("carga",1)
            CALL dialog.setActionHidden("mensaje",1)
            CALL dialog.setActionHidden("notasmasivas",1)
            CALL dialog.setActionHidden("asignar",1)
         END IF  

         IF usu_no_auth = TRUE THEN 
            CALL dialog.setActionHidden ("carga",1)
            CALL dialog.setActionHidden("mensaje",1)
            CALL dialog.setActionHidden("notasmasivas",1)
            CALL dialog.setActionHidden("asignar",1)
            CALL dialog.setActionHidden("informe",1)
            CALL dialog.setActionHidden("estados",1)
         END IF 
         
         IF grupo = 'SUPERVISORES' THEN            
            CALL dialog.setActionHidden("asignar",0)
         END IF  
         
      BEFORE ROW 
         CALL reg_detArr.clear()
         LET id = arr_curr()
         IF id > 0 THEN 
            LET g_reg.* = reg_det[id].*
            CALL displayEnc()
            CALL displayDetArr()
            LET cuantos2 = displayPhone(g_reg.contact_num) 
            IF cuantos2 > 0 THEN 
               DISPLAY ARRAY grDetTel TO sDet3.*
                  BEFORE DISPLAY
                     EXIT DISPLAY
               END DISPLAY 
            ELSE 
               INITIALIZE grTel.* TO NULL 
               #FOR i = 1 TO grDetTel.getLength()
               FOR i = 1 TO 5
                  DISPLAY grTel.* TO sDet3[i].*
               END FOR 
               CALL grDetTel.CLEAR()
            END IF 
            
            CALL setAttr(id)
            CALL DIALOG.setCellAttributes(reg_det_attr)
         END IF 

      --ON KEY (CONTROL-W) 
         
      ON ACTION buscar
         CALL reg_detArr.clear()
         DISPLAY ARRAY reg_detArr TO sDet2.*
            BEFORE DISPLAY  EXIT DISPLAY 
         END DISPLAY
         CALL grDetTel.clear()
         DISPLAY ARRAY grDetTel TO sDet3.*
            BEFORE DISPLAY  EXIT DISPLAY 
         END DISPLAY
         LET cuantos = consulta(true)
         IF cuantos > 0 THEN 
            LET g_reg.* = reg_det[1].*
            CALL displayEnc()
            CALL displayDetArr()
            LET cuantos2 = displayPhone(g_reg.contact_num) 
            IF cuantos2 > 0 THEN 
               DISPLAY ARRAY grDetTel TO sDet3.*
                  BEFORE DISPLAY
                     EXIT DISPLAY
               END DISPLAY 
            END IF 
            CALL setAttr(1)
            CALL DIALOG.setCellAttributes(reg_det_attr)
            DISPLAY BY NAME cuantos
         END IF 
         CALL encabezado("")
         --CALL info_usuario()
         
      --ON ACTION primero
         --CALL fgl_set_arr_curr( 1 )
      --ON ACTION ultimo
         --CALL fgl_set_arr_curr( arr_count() )
      --ON ACTION anterior 
         --CALL fgl_set_arr_curr( arr_curr() - 1 )
      --ON ACTION siguiente 
         --CALL fgl_set_arr_curr( arr_curr() + 1 )
      {ON ACTION agregar
         IF ingreso() THEN 
            LET cuantos = consulta(FALSE)
            --CALL fgl_set_arr_curr( arr_count() + 1 )
            --Refrescar Pantalla
            DISPLAY ARRAY reg_det TO sDet.*
               BEFORE DISPLAY
                  CALL fgl_set_arr_curr( arr_count() + 1 ) 
                  EXIT DISPLAY 
            END DISPLAY 
         END IF
         CALL encabezado("")
         --CALL info_usuario()}

      ON ACTION telefonos
         CALL editphone(DIALOG)
      
      ON ACTION carga
         LET runcmd = "./runpoi.sh"
         RUN runcmd 
         
         --Refresh de la pantalla
         LET cuantos = consulta(false)
         IF cuantos > 0 THEN 
            CALL dialog.setCurrentRow("sdet",1)
            LET g_reg.* = reg_det[1].*
            CALL displayEnc()
            CALL displayDetArr()
            LET cuantos2 = displayPhone(g_reg.contact_num) 
            IF cuantos2 > 0 THEN 
               DISPLAY ARRAY grDetTel TO sDet3.*
                  BEFORE DISPLAY
                     EXIT DISPLAY
               END DISPLAY 
            END IF 
            CALL setAttr(1)
            CALL DIALOG.setCellAttributes(reg_det_attr)
            DISPLAY BY NAME cuantos
         END IF 
         CALL encabezado("")  
         
      ON ACTION modificar
         LET id = arr_curr()
         LET ids = scr_line()
         IF id > 0 THEN 
            IF modifica() THEN
               LET reg_det[id].* = g_reg.*
               DISPLAY reg_det[id].* TO sDet[ids].*
            END IF   
         END IF 
         CALL encabezado("")
         --CALL info_usuario()
      {ON ACTION anular
         LET id = arr_curr()
         LET ids = scr_line()         
         IF id > 0 THEN 
            IF anular() THEN
               --Si es anulacion
               --LET reg_det[id].* = g_reg.*
               --DISPLAY reg_det[id].* TO sDet[ids].*
               --Si es eliminacion
               CALL DIALOG.deleteRow("sdet", id)
               IF id = arr_count() THEN 
                  LET id = id - 1
               END IF 
               IF id > 0 THEN 
                  LET g_reg.* = reg_det[id].*
               ELSE 
                  INITIALIZE g_reg.* TO NULL
               END IF 
               CALL displayEnc()
            END IF   
         END IF} 
      ON ACTION asignar
         LET int_flag = FALSE
         
         INPUT ARRAY reg_det FROM sDet.* 
            ATTRIBUTES(WITHOUT DEFAULTS, UNBUFFERED, APPEND ROW =FALSE, INSERT ROW =FALSE, DELETE ROW =FALSE )

            BEFORE INPUT 
               CALL reg_det.copyTo(ga_asigna)
               CALL DIALOG.setFieldActive("contact_num",0)
               CALL DIALOG.setFieldActive("contact_cod_region",0)
               CALL DIALOG.setFieldActive("contact_city",0)
               --contact_user, 
               CALL DIALOG.setFieldActive("contact_estado_caso",0)
               CALL DIALOG.setFieldActive("contact_operacion",0)
               CALL DIALOG.setFieldActive("contact_producto",0)
               CALL DIALOG.setFieldActive("contact_garantia",0)
               CALL DIALOG.setFieldActive("contact_estado_op",0)
               CALL DIALOG.setFieldActive("contact_dpi_cliente",0)
               CALL DIALOG.setFieldActive("contact_name",0)
               CALL DIALOG.setFieldActive("contact_street",0)
               CALL DIALOG.setFieldActive("contact_tel_cliente",0)
               CALL DIALOG.setFieldActive("contact_tel_casa",0)
               CALL DIALOG.setFieldActive("contact_tel_cel",0)
               CALL DIALOG.setFieldActive("contact_tel_trab",0)
               CALL DIALOG.setFieldActive("contact_tel_otro",0)
               CALL DIALOG.setFieldActive("contact_saldo_cap",0)
               CALL DIALOG.setFieldActive("contact_cap_vencido",0)
               CALL DIALOG.setFieldActive("contact_int_vencido",0)
               CALL DIALOG.setFieldActive("contact_saldo_imo",0)
               CALL DIALOG.setFieldActive("contact_otros",0)
               CALL DIALOG.setFieldActive("contact_total",0)
               CALL DIALOG.setFieldActive("contact_porc_serv",0)
               CALL DIALOG.setFieldActive("contact_total_cobrar",0)
               CALL DIALOG.setFieldActive("contact_fec_ult_pago",0)
               CALL DIALOG.setFieldActive("contact_cuotas_mora",0)
               CALL DIALOG.setFieldActive("contact_fec_mora",0)
               CALL DIALOG.setFieldActive("contact_nom_fia1",0)
               CALL DIALOG.setFieldActive("contact_dir_fia1",0)
               CALL DIALOG.setFieldActive("contact_nom_fia2",0)
               CALL DIALOG.setFieldActive("contact_dir_fia2",0)
               CALL DIALOG.setFieldActive("contact_nom_fia3",0)
               CALL DIALOG.setFieldActive("contact_dir_fia3",0)
               CALL DIALOG.setFieldActive("contact_rec_muser",0)
               CALL DIALOG.setFieldActive("contact_rec_mtime",0)
               CALL DIALOG.setFieldActive("contact_rec_mstat",0)
               CALL DIALOG.setFieldActive("contact_when",0)
               --CALL DIALOG.setFieldActive("contact_tosync",0)
               IF grupo = "SUPERVISORES" THEN 
                  LET qry = "SELECT user_id, user_name FROM users WHERE city_num = ", gsede, " ORDER BY 2"
                  CALL combo_din2("contact1.contact_user",qry)
               END IF 

            BEFORE ROW 
               LET id = arr_curr()
               IF id > 0 THEN 
                  LET g_reg.* = reg_det[id].*
                  --CALL displayEnc()
                  --CALL displayDetArr()
                  
                  CALL setAttr(id)
                  CALL DIALOG.setCellAttributes(reg_det_attr)
               END IF 
            
         END INPUT 
         IF NOT int_flag THEN
            FOR i = 1 TO reg_det.getLength()
               IF reg_det[i].contact_user <> ga_asigna[i].contact_user THEN
                  UPDATE contact SET contact_user = reg_det[i].contact_user
                  WHERE contact_num = reg_det[i].contact_num
               END IF 
            END FOR 
         END IF 
         
         
      
      {ON ACTION reporte
         CALL repPuesto()}
      ON ACTION mensaje
         LET runcmd = "./runmensaje.sh"
         RUN runcmd

      ON ACTION repcobro
         IF grupo = 'ADMINISTRADORES' OR usuario = "root" THEN
            LET runcmd = "./runcobro.sh"
            RUN runcmd
         END IF 
         
      ON ACTION notasmasivas
         LET runcmd = "fglrun aprop0706.42r ", usuario
         RUN runcmd 
         
         --Refresh de la pantalla
         LET cuantos = consulta(false)
         IF cuantos > 0 THEN 
            CALL dialog.setCurrentRow("sdet",1)
            LET g_reg.* = reg_det[1].*
            CALL displayEnc()
            CALL displayDetArr()
            LET cuantos2 = displayPhone(g_reg.contact_num) 
            IF cuantos2 > 0 THEN 
               DISPLAY ARRAY grDetTel TO sDet3.*
                  BEFORE DISPLAY
                     EXIT DISPLAY
               END DISPLAY 
            END IF 
            CALL setAttr(1)
            CALL DIALOG.setCellAttributes(reg_det_attr)
            DISPLAY BY NAME cuantos
         END IF 
         CALL encabezado("")
         
      ON ACTION nota
         LET runcmd = "fglrun aprom0702.42r ", g_reg.contact_num, " ", usuario
         RUN runcmd
         LET cuantos = consulta(false)
         IF cuantos > 0 THEN 
            LET g_reg.* = reg_det[1].*
            CALL displayEnc()
            CALL displayDetArr()
            LET cuantos2 = displayPhone(g_reg.contact_num) 
            IF cuantos2 > 0 THEN 
               DISPLAY ARRAY grDetTel TO sDet3.*
                  BEFORE DISPLAY
                     EXIT DISPLAY
               END DISPLAY 
            END IF 
            CALL setAttr(1)
            CALL DIALOG.setCellAttributes(reg_det_attr)
            DISPLAY BY NAME cuantos
         END IF 
         CALL encabezado("")
      
      
      ON ACTION informe
         LET runcmd = "./runreporte.sh", " ", usuario, " ", grupo
         RUN runcmd 

      ON ACTION menrepdiarioimg
         LET runcmd = "fglrun apror0716.42r"
         RUN runcmd

      ON ACTION menrepdiariodoc
         DISPLAY "entre"
         LET runcmd = "fglrun apror0717.42r"
         RUN runcmd

      ON ACTION estados
         LET runcmd = "./runrepestados.sh", " ", usuario, " ", grupo
         RUN runcmd

      {ON ACTION liqabogados
         LET runcmd = "./runliqabog.sh", " ", usuario, " ", grupo
         RUN runcmd}
         
      ON ACTION salir
         EXIT DISPLAY 
         
   END DISPLAY 
END FUNCTION

FUNCTION combo_init()
   DEFINE where_clause STRING 
   DEFINE sede LIKE users.city_num
   DEFINE sede_alt LIKE users.city_num
   DEFINE lsede STRING 

   IF grupo != "ADMINISTRADORES" THEN
      --DISPLAY "Grupo lleva ", grupo 
      --Sede
      LET sede = 0 LET sede_alt = 0
      
      SELECT city_num      INTO sede     FROM users WHERE user_id = usuario
      SELECT city_num_alt  INTO sede_alt FROM users WHERE user_id = usuario

      LET lsede = NULL 
      IF sede > 0 AND sede_alt > 0 THEN 
         LET lsede = " WHERE city_num IN (", sede, ",", sede_alt, ")"
      ELSE 
         IF sede > 0 THEN 
            LET lsede = " WHERE city_num = ", sede
         ELSE 
            IF sede_alt > 0 THEN 
               LET lsede = " WHERE city_num = ", sede_alt
            END IF 
         END IF 
      END IF 
      LET where_clause = "SELECT city_num, TRIM(city_name)||' '||TRIM(city_country) FROM city ", lsede
   ELSE 
      DISPLAY "Entre al else"
      LET where_clause = "SELECT city_num, TRIM(city_name)||' '||TRIM(city_country) FROM city "
   END IF 
   CALL combo_din2("contact1.contact_city", "SELECT city_num, TRIM(city_name)||' '||TRIM(city_country) FROM city")
   --DISPLAY "where_clause =========================================", where_clause
   CALL combo_din2("fcontact_city", where_clause)  
   CALL combo_din2("contact1.contact_user","SELECT user_id, user_name FROM users ORDER BY 2")  
   CALL combo_din2("fcontact_user","SELECT user_id, user_name FROM users ORDER BY 2")  
   --CALL combo_din2("cmbjefe",'SELECT id_commempl, TRIM(nombre) || " "|| TRIM(apellido) FROM commempl WHERE id_commempl > 0')
   --CALL combo_din2("cmbpuesto","SELECT * FROM commpue WHERE id_commpue > 0")
   --CALL combo_din2("cmbtipempaque","SELECT idtipempaque, nomtipempaque FROM pemmtipemp ")
END FUNCTION 

FUNCTION encabezado(gtit_enc)
DEFINE gtit_enc STRING 

   DISPLAY BY NAME  gtit_enc
END FUNCTION 

FUNCTION displayDetArr()
DEFINE i SMALLINT 
  CALL reg_detArr.clear()
  DECLARE detArrCur CURSOR FOR
  SELECT contnote_num, contnote_rec_muser, contnote_rec_mtime,
      contnote_contact, contnote_when, contnote_text
      FROM contnote
  WHERE contnote_contact = g_reg.contact_num
  ORDER BY contnote_num
  LET i = 1
  FOREACH detArrCur INTO reg_detArr[i].*
     LET i = i + 1
  END FOREACH 
  CALL reg_detArr.deleteElement(reg_detArr.getLength())
  DISPLAY ARRAY reg_detArr TO sDet2.*
     BEFORE DISPLAY  EXIT DISPLAY 
  END DISPLAY 
END FUNCTION 

FUNCTION displayEnc()
   DISPLAY g_reg.contact_num,
      g_reg.contact_cod_region, g_reg.contact_city, g_reg.contact_user,
      g_reg.contact_estado_caso,
      g_reg.contact_operacion, g_reg.contact_producto, g_reg.contact_garantia,
      g_reg.contact_estado_op,
      --g_reg.contact_tosync, 
      g_reg.contact_dpi_cliente, g_reg.contact_name, g_reg.contact_street, 
      g_reg.contact_tel_cliente, g_reg.contact_tel_casa, g_reg.contact_tel_cel,
      g_reg.contact_tel_trab, g_reg.contact_tel_otro, 
      g_reg.contact_saldo_cap, g_reg.contact_cap_vencido,
      g_reg.contact_int_vencido, g_reg.contact_saldo_imo, g_reg.contact_otros,
      g_reg.contact_total, g_reg.contact_porc_serv, g_reg.contact_total_cobrar,
      g_reg.contact_fec_ult_pago, g_reg.contact_cuotas_mora, g_reg.contact_fec_mora,
      g_reg.contact_nom_fia1, g_reg.contact_dir_fia1, g_reg.contact_nom_fia2,
      g_reg.contact_dir_fia2, g_reg.contact_nom_fia3, g_reg.contact_dir_fia3,
      g_reg.contact_rec_muser, g_reg.contact_rec_mtime, g_reg.contact_rec_mstat,
      g_reg.contact_when
      
   TO fcontact_num,
      fcontact_cod_region, fcontact_city, fcontact_user,
      fcontact_estado_caso,
      fcontact_operacion, fcontact_producto, fcontact_garantia,
      fcontact_estado_op, 
      --fcontact_tosync,
      fcontact_dpi_cliente, fcontact_name, fcontact_street, 
      fcontact_tel_cliente, fcontact_tel_casa, fcontact_tel_cel,
      fcontact_tel_trab, fcontact_tel_otro, 
      fcontact_saldo_cap, fcontact_cap_vencido,
      fcontact_int_vencido, fcontact_saldo_imo, fcontact_otros,
      fcontact_total, fcontact_porc_serv, fcontact_total_cobrar,
      fcontact_fec_ult_pago, fcontact_cuotas_mora, fcontact_fec_mora,
      fcontact_nom_fia1, fcontact_dir_fia1, fcontact_nom_fia2,
      fcontact_dir_fia2, fcontact_nom_fia3, fcontact_dir_fia3,
      fcontact_rec_muser, fcontact_rec_mtime, fcontact_rec_mstat,
      fcontact_when
   
END FUNCTION 

FUNCTION setAttr(id)
DEFINE  i, id  SMALLINT

   FOR i=1 TO reg_det.getLength()
      LET reg_det_attr[i].contact_num           = "black"
      LET reg_det_attr[i].contact_cod_region    = "black"
      LET reg_det_attr[i].contact_city          = "black"
      LET reg_det_attr[i].contact_user          = "black"
      LET reg_det_attr[i].contact_estado_caso   = "black"
      LET reg_det_attr[i].contact_operacion     = "black"
      LET reg_det_attr[i].contact_producto      = "black"
      LET reg_det_attr[i].contact_garantia      = "black"
      LET reg_det_attr[i].contact_estado_op     = "black"
      LET reg_det_attr[i].contact_dpi_cliente   = "black"
      LET reg_det_attr[i].contact_name          = "black"
      LET reg_det_attr[i].contact_street        = "black"
      LET reg_det_attr[i].contact_tel_cliente   = "black"
      --LET reg_det_attr[i].contact_tosync        = "black"
   END FOR  

   LET reg_det_attr[id].contact_num           = "blue reverse"
   LET reg_det_attr[id].contact_cod_region    = "blue reverse"
   LET reg_det_attr[id].contact_city          = "blue reverse"
   LET reg_det_attr[id].contact_user          = "blue reverse"
   LET reg_det_attr[id].contact_estado_caso   = "blue reverse"
   LET reg_det_attr[id].contact_operacion     = "blue reverse"
   LET reg_det_attr[id].contact_producto      = "blue reverse"
   LET reg_det_attr[id].contact_garantia      = "blue reverse"
   LET reg_det_attr[id].contact_estado_op     = "blue reverse"
   LET reg_det_attr[id].contact_dpi_cliente   = "blue reverse"
   LET reg_det_attr[id].contact_name          = "blue reverse"
   LET reg_det_attr[id].contact_street        = "blue reverse"
   LET reg_det_attr[id].contact_tel_cliente   = "blue reverse"
   --LET reg_det_attr[id].contact_tosync        = "blue reverse"
   
END FUNCTION 
            
PRIVATE FUNCTION set_title()
    DEFINE w ui.Window,
           title, nom_user, nom_sede STRING
    --LET title = SFMT("%1 / %2", %"contacts.title", parameters.user_id )
    SELECT user_name INTO nom_user FROM users WHERE user_id = usuario
    SELECT trim (city_name) ||' ' ||trim(city_country) INTO nom_sede FROM city c,users u 
      WHERE c.city_num = u.city_num AND u.user_id = usuario    
    LET title = SFMT("%1 / SEDE: %2 ", nom_user, nom_sede )
    --LET title = fgl_getenv("LANG")
    LET w = ui.Window.getCurrent()
    CALL w.setText( title )
    CALL ui.Interface.setText( title )
END FUNCTION

{FUNCTION setEnvForm()
DEFINE vUniMed VARCHAR(50) 

SELECT  valchr 
INTO    vUniMed
FROM    glb_paramtrs 
WHERE nompar = 'NOMBRE UNIDAD DE MEDIDA POR DEFECTO EN BASCULA'

DISPLAY vUniMed TO unidadMedida
END FUNCTION} 


FUNCTION editphone(d)
   DEFINE d ui.Dialog
   DEFINE r,s,touched,reorder,i INT 
   
   LET grDetTelUpd.* = grDetTel.*
   
   INPUT ARRAY grDetTel FROM sDet3.* ATTRIBUTE(WITHOUT DEFAULTS)

      BEFORE INSERT   
         CALL phone_reorder(DIALOG)
            
      AFTER INSERT
         LET reorder=1

      AFTER DELETE
         LET reorder=1

      AFTER ROW
         IF FIELD_TOUCHED(sDet3.*) THEN
            LET touched=TRUE
         END IF
         IF reorder THEN
            CALL phone_reorder(DIALOG)
            LET reorder=0
         END IF
   END INPUT 
   
   IF INT_FLAG THEN 
      LET grDetTel.* = grDetTelUpd.*
      DISPLAY ARRAY grDetTel TO sDet3.* BEFORE DISPLAY EXIT DISPLAY END display
      RETURN 
   END IF 

   DELETE FROM contphone WHERE contact_num = g_reg.contact_num
   FOR i = 1 TO grDetTel.getLength()
     INSERT INTO contphone(contact_num, contphone_num, contphone_phone)
         VALUES(g_reg.contact_num, grDetTel[i].contphone_num, grDetTel[i].contphone_phone)
   END FOR 
   
END FUNCTION 

FUNCTION phone_reorder(d)
    DEFINE d ui.Dialog
    DEFINE i INTEGER
    
    FOR i=1 TO d.getArrayLength("sdet3")
        LET grDetTel[i].contphone_num = i
    END FOR
END FUNCTION
