################################################################################
# Funcion     : %M%
# param_id : Funcion para consulta de umdores  
# Funciones   : umd_qry_init()    -> Prepara la seleccion
#               query_umd()       -> Ingreso de criterios de busqueda
#               fetch_umd()       -> Navega sobre seleccion de datos
#               clean_up()        -> Limpia el cursor de trabajo
#               display_umd()     -> Despliega el registro en pantalla
#               inicia_null()     -> inicia param_ides a nulos
#               display_estado()  -> Despliega el estado del umdor
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        param_id de la modificacion
#
################################################################################
GLOBALS "aprom0700_glob.4gl"

################################################################################
# Visualización de datos:
#                     Todo     Sede     Propietario
#                    ------   ------     ------
# Administradores      X      
# Supervisores                  X
# Oficina central                           X
#
# Opciones adicionales:
# 
#                    Cargar   Mensajes Notas-Masiv
#                    ------   ------   ------
# Administradores      X        X        X      
# Supervisores                  
# Oficina central                           
#
#
################################################################################

FUNCTION consulta(flagConsulta)
DEFINE flagConsulta BOOLEAN 
DEFINE sede LIKE contact.contact_city
DEFINE sede_alt LIKE contact.contact_city
DEFINE lsede STRING 

DEFINE 
   rDet tDet,
   x INTEGER,
   consulta STRING  
   
   IF flagConsulta THEN 
      CALL encabezado("Consulta")
      --CALL info_usuario()
      --Creación de la consulta
      CONSTRUCT condicion 
         ON contact_num, 
            contact_cod_region, contact_city, contact_user, contact_estado_caso,
            contact_operacion, contact_producto, contact_garantia, contact_estado_op,
            --contact_tosync,
            contact_dpi_cliente, contact_name, contact_street, contact_tel_cliente, 
            contact_tel_casa, contact_tel_cel, contact_tel_trab, contact_tel_otro,
            contact_saldo_cap, contact_cap_vencido, contact_int_vencido, contact_saldo_imo,
            contact_otros, contact_total, contact_porc_serv, contact_total_cobrar,
            contact_fec_ult_pago, contact_cuotas_mora, contact_fec_mora,
            contact_nom_fia1, contact_dir_fia1, contact_nom_fia2, contact_dir_fia2,
            contact_nom_fia3, contact_dir_fia3,
            contact_rec_muser, contact_rec_mtime, contact_rec_mstat,
            contact_when
            
         FROM fcontact_num, 
            fcontact_cod_region, fcontact_city, fcontact_user, fcontact_estado_caso,
            fcontact_operacion, fcontact_producto, fcontact_garantia, fcontact_estado_op,
            --fcontact_tosync,
            fcontact_dpi_cliente, fcontact_name, fcontact_street, fcontact_tel_cliente,
            fcontact_tel_casa, fcontact_tel_cel, fcontact_tel_trab, fcontact_tel_otro, 
            fcontact_saldo_cap, fcontact_cap_vencido, fcontact_int_vencido, fcontact_saldo_imo,
            fcontact_otros, fcontact_total, fcontact_porc_serv, fcontact_total_cobrar,
            fcontact_fec_ult_pago, fcontact_cuotas_mora, fcontact_fec_mora,
            fcontact_nom_fia1, fcontact_dir_fia1, fcontact_nom_fia2, fcontact_dir_fia2,
            fcontact_nom_fia3, fcontact_dir_fia3,
            fcontact_rec_muser, fcontact_rec_mtime, fcontact_rec_mstat,
            fcontact_when
         
         ON ACTION ACCEPT
            EXIT CONSTRUCT 
         ON ACTION CANCEL 
            CALL reg_det.clear()
            RETURN 0
         
      END CONSTRUCT
   ELSE
      --IF sede = 1 THEN
      LET condicion = " 1=1 "
      {ELSE 
         LET condicion = " contact_city = (select city_num from users where user_id = '", usuario CLIPPED, "')"
      END IF }   
   END IF
   
   --Sede
   SELECT city_num INTO sede     FROM users WHERE user_id = usuario

   LET sede_alt = 0
   SELECT city_num_alt INTO sede_alt FROM users WHERE user_id = usuario

   IF sede_alt IS NULL OR sede_alt = 0  THEN
      LET lsede = " AND contact_city = ", sede
   ELSE 
      LET lsede = " AND contact_city IN (", sede, ",", sede_alt, ")"
   END IF 
   --Grupo -> variable: grupo
   
   CASE 
      WHEN grupo = "SEDES" OR grupo = "OFICINA"
         LET condicion = condicion CLIPPED, 
            " AND contact_user = '", usuario, "'"
      WHEN grupo = "SUPERVISORES"
         LET condicion = condicion CLIPPED, lsede CLIPPED  
            --" AND contact_city = (select city_num from users where user_id = '", usuario CLIPPED, "')"
            --" AND contact_city = ", sede CLIPPED
            
   END CASE
   --Armar la consulta
   LET consulta = 
      " SELECT FIRST 100 contact_num, 
            contact_cod_region, contact_city, contact_user, contact_estado_caso,
            contact_operacion, contact_producto, contact_garantia, contact_estado_op, ",
            --contact_tosync,
         " contact_dpi_cliente, contact_name, contact_street, contact_tel_cliente,
            contact_tel_casa, contact_tel_cel, contact_tel_trab, contact_tel_otro, 
            contact_saldo_cap, contact_cap_vencido, contact_int_vencido, contact_saldo_imo,
            contact_otros, contact_total, contact_porc_serv, contact_total_cobrar,
            contact_fec_ult_pago, contact_cuotas_mora, contact_fec_mora,
            contact_nom_fia1, contact_dir_fia1, contact_nom_fia2, contact_dir_fia2,
            contact_nom_fia3, contact_dir_fia3,
            contact_rec_muser, contact_rec_mtime, contact_rec_mstat,
            contact_when ",
      " FROM contact ",
      " WHERE  ", condicion,
      " ORDER BY 1"
 --DISPLAY "consulta -> ", consulta
   --definir cursor con consulta de BD
   DECLARE curDet CURSOR FROM consulta
   CALL reg_det.clear()
   LET x = 0
   --Llenar el arreglo con el resultado de la consulta
   FOREACH curDet INTO rDet.*
      LET x = x + 1 
      LET reg_det [x].* = rDet.*  
   END FOREACH

   RETURN x --Cantidad de registros
END FUNCTION

FUNCTION displayPhone(lcontact_num)
   DEFINE lcontact_num LIKE contact.contact_num
   DEFINE i SMALLINT 

   DECLARE ccurTel CURSOR FOR 
      SELECT contphone_num, contact_num, contphone_phone FROM contphone c WHERE c.contact_num = lcontact_num

   LET i = 1
   FOREACH ccurTel INTO grDetTel[i].contphone_num, grDetTel[i].contact_num, grDetTel[i].contphone_phone
      LET i = i + 1
   END FOREACH 
   CALL grDetTel.deleteElement(grDetTel.getLength())
   --DISPLAY grDetTel.* TO sDet3
   RETURN i-1
END FUNCTION 
