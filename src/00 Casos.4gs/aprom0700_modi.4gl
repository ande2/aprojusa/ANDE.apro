################################################################################
# Funcion     : %M%
# Descripcion : Funcion para modificar registros
# Funciones   : updel_init()
#	             delete_umd() 
#               update_umd()
#               pk_estado()
#               valida_modi()
#               verifica_modi()
#               existe_datos()
#               existe_dpro()
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################
GLOBALS "aprom0700_glob.4gl"

FUNCTION update_init()
DEFINE strSql STRING 

   LET strSql = 
      "UPDATE contact ",
      "SET ",
      " contact_total       = ?, ",
      " contact_cuotas_mora = ?, ",
      " contact_tel_casa    = ?, ",
      " contact_tel_cel     = ?, ",
      " contact_tel_trab    = ?, ",
      " contact_tel_otro    = ?  ", --, ",
      --" contact_tosync      = ?  ",
      " WHERE contact_num   = ? "
      
   PREPARE st_modificar FROM strSql

   LET strSql =
      "INSERT INTO contnote ( ",
        " contnote_num, contnote_rec_muser, contnote_rec_mtime, contnote_rec_mstat, contnote_contact, contnote_when, contnote_text ",
        "   ) ",
      " VALUES (?,?,?,?,?,?,?)"

   PREPARE st_insertarDetMod FROM strSql

END FUNCTION 

FUNCTION modifica()
   CALL encabezado("Modificar")
   --CALL info_usuario()
   
   IF captura_datos('M') THEN
      RETURN actualizar()
   ELSE 
      RETURN FALSE 
   END IF 
END FUNCTION 

FUNCTION delete_init()
DEFINE strSql STRING 

   LET strSql =
      "DELETE FROM contact ",
      "WHERE contact_num = ? "
      
   PREPARE st_delete FROM strSql

   LET strSql =
      "DELETE FROM contnote ",
      "WHERE contnote_contact = ? "
      
   PREPARE st_deleteDet FROM strSql


END FUNCTION 

FUNCTION actualizar()
DEFINE i SMALLINT 

   {IF NOT can_delete("pemMBoleta", "idOrdPro", g_reg.idOrdPro) THEN
      CALL msg("No se puede eliminar, ya existe en boletas de ingreso")
      RETURN FALSE 
   END IF}
   WHENEVER ERROR CONTINUE 
   BEGIN WORK 
   EXECUTE IMMEDIATE "SET CONSTRAINTS ALL DEFERRED"
   --Modifica encabezado
   TRY
      EXECUTE st_modificar USING 
        g_reg.contact_total, g_reg.contact_cuotas_mora, 
        g_reg.contact_tel_casa, g_reg.contact_tel_cel,
        g_reg.contact_tel_trab,
        g_reg.contact_tel_otro, 
        --g_reg.contact_tosync,
        u_reg.contact_num 
      --Si todo bien elimina el detalle
      --CS TRY
         --CS EXECUTE st_deleteDet USING 
           --CS g_reg.contact_num 
         --Si todo bien inserta nuevo detalle
         --CS TRY 
            --CS FOR i= 1 TO reg_detArr.getLength()
               --LET reg_detArr[i].idOrdPro = g_reg.idOrdPro
               --EXECUTE st_insertarDetMod USING 
                 --i, reg_detArr[i].contnote_rec_muser,
                --CS INSERT INTO contnote ( contnote_num, contnote_rec_muser, contnote_rec_mtime, 
                  --CS contnote_rec_mstat, contnote_contact, contnote_when, contnote_text )
                  --CS VALUES (contnote_seq.NEXTVAL, reg_detArr[i].contnote_rec_muser, reg_detArr[i].contnote_rec_mtime, 
                  --CS 'V', g_reg.contact_num, reg_detArr[i].contnote_when, reg_detArr[i].contnote_text) 
            --CS END FOR 
         --CS CATCH 
            --CS ROLLBACK WORK 
            --CS WHENEVER ERROR STOP
            --CS CALL msgError(sqlca.sqlcode,"Grabar Detalle de Registro")
            --CS RETURN FALSE 
         --CS END TRY
      --CS CATCH 
         --CS ROLLBACK WORK
         --CS WHENEVER ERROR STOP 
         --CS CALL msgError(sqlca.sqlcode,"Modificar Detalle")
         --CS RETURN FALSE 
      --CS END TRY
   CATCH 
      WHENEVER ERROR STOP 
      CALL msgError(sqlca.sqlcode,"Modificar Registro")
      ROLLBACK WORK
      RETURN FALSE 
   END TRY
   COMMIT WORK 
   WHENEVER ERROR STOP 
   CALL msg ("Registro actualizado")
   RETURN TRUE 
END FUNCTION 

FUNCTION anular()
   IF NOT box_confirma("Esta seguro de anular el registro") THEN
      RETURN FALSE
   END IF 
   WHENEVER ERROR CONTINUE 
   BEGIN WORK 
   --Eliminar detalle
   TRY
      EXECUTE st_deleteDet USING 
           g_reg.contact_num 
      --Si todo bien elimina encabezado
      TRY 
         EXECUTE st_delete USING g_reg.contact_num
      CATCH 
         ROLLBACK WORK
         WHENEVER ERROR STOP 
         CALL msgError(sqlca.sqlcode,"Eliminar Registro")
         RETURN FALSE 
      END TRY
   CATCH 
      ROLLBACK WORK
      WHENEVER ERROR STOP 
      CALL msgError(sqlca.sqlcode,"Eliminar detalle de registro")
      RETURN FALSE 
   END TRY
   COMMIT WORK 
   WHENEVER ERROR STOP 
   CALL msg("Registro eliminado")
   RETURN TRUE 
END FUNCTION 