DATABASE "aprojusa"

MAIN

   MENU 
      ON ACTION reporte
         CALL freporte()
      ON ACTION salir 
         EXIT MENU 
   END MENU 

END MAIN 


FUNCTION freporte()

DEFINE handler om.SaxDocumentHandler

	If fgl_report_loadCurrentSettings("Empleados.4rp") THEN
      --CALL fgl_report_selectDevice("PDF")
      CALL fgl_report_selectDevice("SVG")
		LET handler = fgl_report_commitCurrentSettings()
	ELSE
		EXIT PROGRAM
	END IF

	IF handler IS NOT NULL THEN
		CALL run_report1_to_handler(handler)
	END IF
   
END FUNCTION 