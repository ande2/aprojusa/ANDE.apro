DATABASE aprojusa


MAIN
   OPTIONS 
      INPUT WRAP 
      
   DEFER INTERRUPT 

   MENU
         
      ON ACTION word
         CALL freporte("RTF")
   
      ON ACTION pdf
         CALL freporte("PDF") --CALL freporte("PDF")
   
      ON ACTION salir
         EXIT MENU 
         
   END MENU

END MAIN 

FUNCTION freporte(dst)
   DEFINE g_reg RECORD 
      contact_num    LIKE contnotedocs.contact_num,
      contnote_num   LIKE contnotedocs.contnote_num,
      contdocs_id    LIKE contnotedocs.contdocs_id --,
      --contdocs_tipo  LIKE contnotedocs.contdocs_tipo,
      --contdocs_desc  LIKE contnotedocs.contdocs_desc
   END RECORD 

   DEFINE myHandler om.SaxDocumentHandler
   DEFINE dst  STRING
   
   IF fgl_report_loadCurrentSettings("apror0714_report.4rp") THEN
      CALL fgl_report_selectDevice(dst)
      LET myHandler = fgl_report_commitCurrentSettings()
   ELSE
      EXIT PROGRAM
   END IF

   DECLARE curdoc CURSOR FOR  
      SELECT contact_num, contnote_num, contdocs_id --, contdocs_tipo, contdocs_desc
         --INTO g_reg.contact_num, g_reg.contnote_num, g_reg.contdocs_id, g_reg.contdocs_tipo, g_reg.contdocs_desc
         FROM contnotedocs
         WHERE contact_num = 17162 
         AND   contnote_num = 25662
      GROUP BY contact_num, contnote_num, contdocs_id
         --AND contdocs_id   = 1
      START REPORT pro_det4gl_rep TO XML HANDLER myHandler

      FOREACH curdoc INTO g_reg.contact_num, g_reg.contnote_num, g_reg.contdocs_id --, g_reg.contdocs_tipo, g_reg.contdocs_desc
         
         OUTPUT TO REPORT pro_det4gl_rep(g_reg.*)

      END FOREACH 

   FINISH REPORT pro_det4gl_rep

END FUNCTION 

REPORT pro_det4gl_rep(l_reg)
---- Variables de regitro por tablas
DEFINE l_reg     RECORD 
   contact_num    LIKE contnotedocs.contact_num,
   contnote_num   LIKE contnotedocs.contnote_num,
   contdocs_id    LIKE contnotedocs.contdocs_id --,
   --contdocs_tipo  LIKE contnotedocs.contdocs_tipo,
   --contdocs_desc  LIKE contnotedocs.contdocs_desc
END RECORD
DEFINE foto1, foto2, foto3, foto4, foto5 RECORD 
   doc_id      LIKE contnotedocs.contdocs_id,
   doc_doc     LIKE contnotedocs.contdocs_doc
END RECORD
DEFINE i SMALLINT  

FORMAT 
   BEFORE GROUP OF l_reg.contact_num
      PRINTX l_reg.contact_num
   BEFORE GROUP OF l_reg.contnote_num
      PRINTX l_reg.contnote_num
      
   ON EVERY ROW
      --doc1
      {LOCATE foto1.doc_doc IN FILE "doc1"
      SELECT contdocs_id, contdocs_doc
         INTO foto1.doc_id, foto1.doc_doc 
         FROM contnotedocs
         WHERE contact_num = l_reg.contact_num
         AND contnote_num  = l_reg.contnote_num
         AND contdocs_id   = 1
      IF sqlca.sqlcode = 0 THEN DISPLAY "imagen 1 ok ", foto1.doc_id END IF  

      --doc2
      LOCATE foto2.doc_doc IN FILE "doc2"
      SELECT contdocs_id, contdocs_doc
         INTO foto2.doc_id, foto2.doc_doc 
         FROM contnotedocs
         WHERE contact_num = l_reg.contact_num
         AND contnote_num  = l_reg.contnote_num
         AND contdocs_id   = 2
      IF sqlca.sqlcode = 0 THEN DISPLAY "imagen 2 ok ", foto2.doc_id END IF} 
         
      PRINTX   l_reg.contdocs_id --, 
               --foto1.*
      {SKIP TO TOP OF PAGE 
      PRINTX foto2.*}
      
      
END REPORT 