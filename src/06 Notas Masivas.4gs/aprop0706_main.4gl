--Carlos Santizo jun/2018
SCHEMA aprojusa 

GLOBALS 
   DEFINE g_note RECORD LIKE contnote.*

   DEFINE g_reg RECORD LIKE city.*
END GLOBALS 

MAIN

DEFINE sql STRING
DEFINE filename STRING
DEFINE header BOOLEAN
DEFINE preview BOOLEAN
DEFINE result BOOLEAN

    DEFER INTERRUPT
    DEFER QUIT
    OPTIONS INPUT WRAP
    OPTIONS FIELD ORDER FORM

    CONNECT TO "aprojusa"
    --CALL populate()
    
    --OPEN WINDOW w WITH FORM "apror0704_form"
    OPEN WINDOW w WITH FORM "aprop0706_form"

    CALL fgl_settitle("Registro de envío de mensajes por Sede")
    CALL combo_din2("city_num","SELECT * FROM city")
    -- Default values
    LET sql = "SELECT * FROM city"
    --LET filename = "fgl_excel_generic_test.xlsx"
    LET filename = "IMPORTAR-CONTACTOS.csv"
    LET header = TRUE
    LET preview = TRUE
    
    --INPUT BY NAME sql, filename, header, preview ATTRIBUTES(UNBUFFERED, WITHOUT DEFAULTS=TRUE, ACCEPT=FALSE, CANCEL=FALSE)
    INPUT BY NAME g_reg.city_num --sql, filename, header, preview 
      ATTRIBUTES(UNBUFFERED, WITHOUT DEFAULTS=TRUE, ACCEPT=FALSE, CANCEL=FALSE)

         ON ACTION generar
            CALL generaMsj()
        {ON ACTION excel ATTRIBUTES(TEXT="Generate Excel", IMAGE="fa-file-excel-o")
        
            LET sql = prepsql(g_reg.city_num)
      
            IF sql_to_excel(sql, filename, header) THEN
                IF preview THEN
                    CALL fgl_putfile(filename, filename)
                    CALL ui.Interface.frontCall("standard","shellExec", filename, result)
                ELSE
                    MESSAGE "Spreadsheet created"
                END IF
            ELSE
                ERROR "Something went wrong"
            END IF
            EXIT INPUT} 
        ON ACTION salir
            EXIT INPUT 
        ON ACTION close
            EXIT INPUT

    END INPUT
END MAIN

FUNCTION generaMsj()
DEFINE i SMALLINT 
DEFINE mensaje STRING  

   LET i = 0
   
   DECLARE ccur CURSOR FOR 
      SELECT contact_num FROM contact 
         WHERE contact_city = g_reg.city_num
         AND contact_estado_caso IN ("VIGENTE", "PAGO PARCIAL")

   FOREACH ccur INTO g_note.contnote_contact
      LET g_note.contnote_text = creaNotaMsjTxt(g_note.contnote_contact)
      --DISPLAY "g_note.contnote_text ", g_note.contnote_text
      LET g_note.contnote_rec_mtime       = CURRENT 
      --LET g_note.contnote_text            = "Se envió mensaje al cliente"
      LET g_note.contnote_cod_tipologia   = 67
      LET g_note.contnote_rec_muser       = arg_val(1)
      LET g_note.contnote_rec_mstat       = "V"
      LET g_note.contnote_when            = CURRENT 
      LET g_note.contnote_estado          = "A"
      
      WHENEVER ERROR CONTINUE
      INSERT INTO contnote (contnote_contact, contnote_num, contnote_rec_mtime,
         contnote_text, contnote_cod_tipologia, contnote_rec_muser,
         contnote_rec_mstat, contnote_when, contnote_estado)
         VALUES (g_note.contnote_contact, contnote_seq.NEXTVAL, 
         g_note.contnote_rec_mtime,
         g_note.contnote_text, g_note.contnote_cod_tipologia, g_note.contnote_rec_muser,
         g_note.contnote_rec_mstat, g_note.contnote_when, g_note.contnote_estado)
      WHENEVER ERROR STOP 
      
      IF sqlca.sqlcode != 0 THEN
         ERROR "Error ", sqlca.sqlcode, " al insertar nota para el caso ", g_note.contnote_contact
      ELSE 
         LET i = i + 1
      END IF 
   END FOREACH 
   LET mensaje = "Total de mensajes generados:",i
   CALL msg(mensaje)
   
END FUNCTION 

FUNCTION creaNotaMsjTxt(idCaso)
DEFINE idCaso        LIKE contnote.contnote_contact
DEFINE c_name        LIKE contact.contact_name
DEFINE c_operacion   LIKE contact.contact_operacion
DEFINE c_total       LIKE contact.contact_total
DEFINE c_porc        LIKE contact.contact_porc_serv
DEFINE city_cta      LIKE city.city_cuenta
DEFINE city_tel      LIKE city.city_telefono
DEFINE n_total, n_porc  STRING 

DEFINE txtMsj STRING

   SELECT trim(contact_name), trim(contact_operacion), 
   trim(contact_total), trim(contact_porc_serv), 
   trim(city_cuenta), trim(city_telefono)
   INTO c_name, c_operacion, c_total, c_porc, city_cta, city_tel
   FROM contact, city
   WHERE contact_city = city_num 
   AND contact_num = idCaso

   LET c_total = formatoNum(c_total)
   LET c_porc = formatoNum(c_porc)
   
   LET txtMsj = "Buen día ", c_name CLIPPED, 
   " le saludamos del Bufete Jurídico APROJUSA requiriendole el pago del Crédito No. ", c_operacion CLIPPED,
   " otorgado por BANRURAL, el monto de las cuotas atrasadas actualmente es de Q.", c_total CLIPPED,
   ", para el desbloqueo de su crédito depositar Q.", c_porc CLIPPED,
   " a la cuenta No. ", city_cta CLIPPED, 
   ". Para consultas comunicarse al Teléfono ", city_tel CLIPPED
 
RETURN txtMsj
END FUNCTION
 