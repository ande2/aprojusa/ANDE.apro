DATABASE aprojusa

DEFINE g_reg RECORD
   suc_id            LIKE suc_enca.suc_id,
   suc_al_nom        LIKE suc_enca.suc_al_nom,
   suc_al_tit        LIKE suc_enca.suc_al_tit,
   suc_emp_nom       LIKE suc_enca.suc_emp_nom,
   suc_emp_dir       LIKE suc_enca.suc_emp_dir,
   suc_emp_registro  LIKE suc_enca.suc_emp_registro,
   suc_pla_fecha     LIKE suc_enca.suc_pla_fecha
END RECORD 
DEFINE usuario LIKE users.user_id

MAIN
   OPTIONS 
      INPUT WRAP 
      
   DEFER INTERRUPT 

   LET usuario = arg_val(1)

   OPEN WINDOW w1 WITH FORM "suc_form1"

   MENU
      BEFORE MENU 
         SELECT suc_id, suc_al_nom, suc_al_tit, suc_emp_nom,
            suc_emp_dir, suc_emp_registro, suc_pla_fecha
            INTO g_reg.suc_id, g_reg.suc_al_nom, g_reg.suc_al_tit, g_reg.suc_emp_nom,
            g_reg.suc_emp_dir, g_reg.suc_emp_registro, g_reg.suc_pla_fecha
            FROM suc_enca
            WHERE suc_usuario = usuario
            
         DISPLAY BY NAME g_reg.suc_id, g_reg.suc_al_nom, g_reg.suc_al_tit, g_reg.suc_emp_nom,
            g_reg.suc_emp_dir, g_reg.suc_emp_registro, g_reg.suc_pla_fecha


      ON ACTION modificar
         CALL fmodifica()
         
      ON ACTION informe
         MENU 
            ON ACTION word
               CALL freporte("RTF")
               EXIT MENU 
            ON ACTION pdf
               CALL freporte("PDF")
               EXIT MENU 
         END MENU 
      ON ACTION salir
         EXIT MENU 
         
   END MENU

END MAIN 

FUNCTION fmodifica()

   INPUT BY NAME g_reg.suc_id, g_reg.suc_al_nom, g_reg.suc_al_tit, g_reg.suc_emp_nom,
      g_reg.suc_emp_dir, g_reg.suc_emp_registro, g_reg.suc_pla_fecha
      WITHOUT DEFAULTS
      ATTRIBUTES(UNBUFFERED)

   IF int_flag THEN 
      CALL msg("Procedimiento cancelado")
      RETURN
   END IF 

   UPDATE suc_enca SET (suc_al_nom, suc_al_tit, suc_emp_nom,
      suc_emp_dir, suc_emp_registro, suc_pla_fecha) =
      (g_reg.suc_al_nom, g_reg.suc_al_tit, g_reg.suc_emp_nom,
      g_reg.suc_emp_dir, g_reg.suc_emp_registro, g_reg.suc_pla_fecha)
      WHERE suc_id = g_reg.suc_id

   IF sqlca.sqlcode = 0 THEN 
      CALL msg("Registro guardado")
   ELSE 
      CALL msg("Error al insertar")
   END IF 
END FUNCTION 

FUNCTION freporte(dst)

   DEFINE myHandler om.SaxDocumentHandler
   DEFINE dst  STRING

   SELECT suc_id, suc_al_nom, suc_al_tit, suc_emp_nom,
            suc_emp_dir, suc_emp_registro, suc_pla_fecha
            INTO g_reg.suc_id, g_reg.suc_al_nom, g_reg.suc_al_tit, g_reg.suc_emp_nom,
            g_reg.suc_emp_dir, g_reg.suc_emp_registro, g_reg.suc_pla_fecha
            FROM suc_enca
            WHERE suc_usuario = usuario
            
   IF fgl_report_loadCurrentSettings("suc_report1.4rp") THEN
      CALL fgl_report_selectDevice(dst)
      LET myHandler = fgl_report_commitCurrentSettings()
   ELSE
      EXIT PROGRAM
   END IF
    
   START REPORT pro_det4gl_rep TO XML HANDLER myHandler

      OUTPUT TO REPORT pro_det4gl_rep(g_reg.*)

   FINISH REPORT pro_det4gl_rep

END FUNCTION 

REPORT pro_det4gl_rep(l_reg)
---- Variables de regitro por tablas
DEFINE l_reg     RECORD
   suc_id            LIKE suc_enca.suc_id,
   suc_al_nom        LIKE suc_enca.suc_al_nom,
   suc_al_tit        LIKE suc_enca.suc_al_tit,
   suc_emp_nom       LIKE suc_enca.suc_emp_nom,
   suc_emp_dir       LIKE suc_enca.suc_emp_dir,
   suc_emp_registro  LIKE suc_enca.suc_emp_registro,
   suc_pla_fecha     LIKE suc_enca.suc_pla_fecha
END RECORD 

FORMAT 
   ON EVERY ROW
      PRINTX l_reg.*
END REPORT 