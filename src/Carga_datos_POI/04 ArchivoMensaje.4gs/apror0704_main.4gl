IMPORT FGL fgl_excel

SCHEMA aprojusa 

MAIN
DEFINE g_reg RECORD LIKE city.*
DEFINE sql STRING
DEFINE filename STRING
DEFINE header BOOLEAN
DEFINE preview BOOLEAN
DEFINE result BOOLEAN

    DEFER INTERRUPT
    DEFER QUIT
    OPTIONS INPUT WRAP
    OPTIONS FIELD ORDER FORM

    -- Create and populate test database
    --CONNECT TO ":memory:+driver='dbmsqt'"
    CONNECT TO "aprojusa"
    --CALL populate()
    
    --OPEN WINDOW w WITH FORM "apror0704_form"
    OPEN WINDOW w WITH FORM "apror0704_form2"

    CALL fgl_settitle("Generacion de Archivo Excel para Mensajes por Sede")
    CALL combo_din2("city_num","SELECT * FROM city")
    -- Default values
    LET sql = "SELECT * FROM city"
    --LET filename = "fgl_excel_generic_test.xlsx"
    LET filename = "IMPORTAR-CONTACTOS.csv"
    LET header = TRUE
    LET preview = TRUE
    
    --INPUT BY NAME sql, filename, header, preview ATTRIBUTES(UNBUFFERED, WITHOUT DEFAULTS=TRUE, ACCEPT=FALSE, CANCEL=FALSE)
    INPUT BY NAME g_reg.city_num --sql, filename, header, preview 
      ATTRIBUTES(UNBUFFERED, WITHOUT DEFAULTS=TRUE, ACCEPT=FALSE, CANCEL=FALSE)
        ON ACTION excel ATTRIBUTES(TEXT="Generate Excel", IMAGE="fa-file-excel-o")
        
            LET sql = prepsql(g_reg.city_num)
      --DISPLAY "sql ", sql
            --IF sql_to_excel(sql, filename, header) THEN
               IF exporta_datos(sql, filename) THEN
                IF preview THEN
                    CALL fgl_putfile(filename, filename)
                    CALL ui.Interface.frontCall("standard","shellExec", filename, result)
                ELSE
                    MESSAGE "Spreadsheet created"
                END IF
            ELSE
                ERROR "Something went wrong"
            END IF
            
            EXIT INPUT 
        ON ACTION salir
            EXIT INPUT 
        ON ACTION close
            EXIT INPUT

    END INPUT
END MAIN

FUNCTION exporta_datos(sql_stmt, nomfile)
   DEFINE sql_stmt, nomfile, prepsql STRING

   UNLOAD TO 'IMPORTAR-CONTACTOS.csv' DELIMITER ','

      SELECT number,  cliente, credito, monto, porcentaje,  cuenta, bufete  
      FROM tmp_rep_excel
   --LET prepsql = "UNLOAD TO '", nomfile CLIPPED, "' ", sql_stmt
   --DISPLAY "prepsql ", prepsql
   --PREPARE ex_stmt FROM prepsql
   --EXECUTE ex_stmt 
   IF sqlca.sqlcode = 0 THEN 
      RETURN TRUE
   ELSE 
      RETURN FALSE
   END IF 
END FUNCTION 

FUNCTION sql_to_excel(sql, filename, header)
DEFINE hdl base.SqlHandle
DEFINE sql STRING
DEFINE filename STRING
DEFINE header BOOLEAN
DEFINE row_idx, col_idx INTEGER 

DEFINE workbook     fgl_excel.workbookType 
DEFINE sheet        fgl_excel.sheetType  
DEFINE row          fgl_excel.rowType  
DEFINE cell         fgl_excel.cellType 
DEFINE header_style fgl_excel.cellStyleType
DEFINE header_font  fgl_excel.fontType

DEFINE datatype STRING
    
    LET hdl = base.SqlHandle.create()
    TRY
        CALL hdl.prepare(sql)
        CALL hdl.open()
    CATCH
        RETURN FALSE
    END TRY

    CALL fgl_excel.workbook_create() RETURNING workbook

    -- create a worksheet
    CALL fgl_excel.workbook_createsheet(workbook) RETURNING sheet

    -- create data rows
    LET row_idx = 0 
    
    WHILE TRUE
        CALL hdl.fetch()
        IF STATUS=NOTFOUND THEN
            EXIT WHILE
        END IF
        LET row_idx = row_idx + 1

        IF row_idx = 1 AND header THEN
            -- create a font, will be used in header
            CALL fgl_excel.font_create(workbook) RETURNING header_font
            CALL fgl_excel.font_set(header_font, "weight", "bold")

            -- create a style, will be used in header
            CALL fgl_excel.style_create(workbook) RETURNING header_style
            CALL fgl_excel.style_set(header_style, "alignment","center")
            CALL fgl_excel.style_font_set(header_style, header_font)
   
            -- Add column headers
            CALL fgl_excel.sheet_createrow(sheet, 0) RETURNING row
            FOR col_idx = 1 TO hdl.getResultCount()
                CALL fgl_excel.row_createcell(row, col_idx-1) RETURNING cell
                CALL fgl_excel.cell_value_set(cell, hdl.getResultName(col_idx))
                CALL fgl_excel.cell_style_set(cell, header_style)
            END FOR
        END IF
        CALL fgl_excel.sheet_createrow(sheet, IIF(header,row_idx, row_idx-1)) RETURNING row

        FOR col_idx = 1 TO hdl.getResultCount()
            CALL fgl_excel.row_createcell(row, col_idx-1) RETURNING cell
            LET datatype = hdl.getResultType(col_idx) 
            CASE 
                WHEN datatype =  "INTEGER" -- TODO check logic
                  OR datatype MATCHES "DECIMAL*"
                  OR datatype MATCHES "FLOAT*"
                  OR datatype MATCHES "*INT*"
                    CALL fgl_excel.cell_number_set(cell, hdl.getResultValue(col_idx))
                OTHERWISE
                    CALL fgl_excel.cell_value_set(cell, hdl.getResultValue(col_idx))
            END CASE
        END FOR
    END WHILE

    -- TODO this code should automatically size the columns
    -- However it is very very slow for reasons I can't determine
    -- Uncomment and test at your leisure
    IF hdl IS NOT NULL THEN
        FOR col_idx = 1 TO hdl.getResultCount()
            CALL fgl_excel.sheet_autosizecolumn(sheet, col_idx-1)
        END FOR
    END IF

    -- Write to File
    CALL fgl_excel.workbook_writeToFile(workbook, filename)

    RETURN TRUE   
END FUNCTION



FUNCTION populate()
DEFINE idx INTEGER
DEFINE rec RECORD
    integer_type INTEGER,
    date_type DATE,
    char_type CHAR(20),
    float_type FLOAT
END RECORD

    CREATE TEMP TABLE test_data 
        (integer_type INTEGER,
         date_type DATE,
         char_type VARCHAR(20),
         float_type FLOAT)

    FOR idx = 1 TO 26
        LET rec.integer_type = idx
        LET rec.date_type = TODAY+idx
        LET rec.char_type = ASCII(64+idx)
        LET rec.float_type = 1/idx
        
        INSERT INTO test_data VALUES(rec.*)
    END FOR
END FUNCTION

FUNCTION prepsql(idSede)
   DEFINE idSede LIKE city.city_num
   DEFINE sql_stmt STRING 
   
   CALL creatmp()
   CALL carga_datos(idSede)
   LET sql_stmt = "SELECT number, ", 
      " cliente, credito, monto, porcentaje, ",
      " cuenta, bufete ",
      " FROM tmp_rep_excel "
   RETURN sql_stmt
END FUNCTION

FUNCTION creatmp()
   CREATE TEMP TABLE tmp_rep_excel(
      credito     CHAR(20), 
      cliente     VARCHAR(100), 
      monto       VARCHAR (50), --DECIMAL(20,2), --VARCHAR(20), 
      porcentaje  VARCHAR(50), --DECIMAL(20,2), --VARCHAR(20),
      cuenta      CHAR(20),     
      bufete      CHAR(20),
      number      CHAR(8)
   )
   INSERT INTO tmp_rep_excel (number, cliente, credito, monto, porcentaje, cuenta, bufete) 
      VALUES ('number',  'cliente', 'credito', 'monto', 'porcentaje',  'cuenta', 'bufete  ')
      
END FUNCTION  

FUNCTION carga_datos(sede)

   DEFINE sede LIKE city.city_num
   
   TYPE t_reg RECORD
      contact_operacion    LIKE contact.contact_operacion, 
      contac_name          LIKE contact.contact_name, 
      --contact_total        LIKE contact.contact_total, 
      --contact_porc_serv    LIKE contact.contact_porc_serv,
      
      contact_tel_cliente  STRING,
      contact_tel_casa     LIKE contact.contact_tel_casa,
      contact_tel_cel      LIKE contact.contact_tel_cel,
      contact_tel_trab     LIKE contact.contact_tel_trab --,
      
      --contact_tel_bufete   LIKE city.city_telefono,
      --contact_cta_bufete   LIKE city.city_cuenta
   END RECORD

   DEFINE g_reg t_reg
   DEFINE ga_reg DYNAMIC ARRAY OF t_reg
   DEFINE i    SMALLINT 
   
   DEFINE cuenta           LIKE city.city_cuenta
   DEFINE telefono         LIKE city.city_telefono

   DEFINE monto, porcentaje DECIMAL(20,2)

   --para cuenta y telefono de bufete
   SELECT city_cuenta, city_telefono
      INTO cuenta, telefono
      FROM city 
      WHERE city_num=sede
   
   DECLARE ccur CURSOR FOR 
      SELECT contact_operacion, contact_name, 
         contact_total, contact_porc_serv, 
         contact_tel_casa, contact_tel_cel, contact_tel_trab
      FROM contact
      WHERE contact_city = sede

   FOREACH ccur INTO g_reg.contact_operacion, g_reg.contac_name, 
      monto, porcentaje, 
      g_reg.contact_tel_casa, g_reg.contact_tel_cel, g_reg.contact_tel_trab 

      CALL ga_reg.CLEAR()
      
      --Para telefono
      LET i = 0 
      IF LENGTH(g_reg.contact_tel_cel) = 8 THEN
         LET g_reg.contact_tel_cliente = g_reg.contact_tel_cel
         LET i = i + 1
         LET ga_reg[i].* = g_reg.*
      END IF    
      IF LENGTH(g_reg.contact_tel_casa) = 8 THEN
         IF g_reg.contact_tel_casa != g_reg.contact_tel_cel THEN 
            LET g_reg.contact_tel_cliente = g_reg.contact_tel_casa
            LET i = i + 1
            LET ga_reg[i].* = g_reg.*
         END IF    
      END IF            
      IF LENGTH(g_reg.contact_tel_trab) = 8 THEN
         IF g_reg.contact_tel_trab != g_reg.contact_tel_cel THEN
            IF g_reg.contact_tel_trab != g_reg.contact_tel_casa THEN
               LET g_reg.contact_tel_cliente = g_reg.contact_tel_trab
               LET i = i + 1
               LET ga_reg[i].* = g_reg.*
            END IF 
         END IF    
      END IF 
      IF i = 0 THEN 
         CONTINUE FOREACH 
      END IF 

      FOR i = 1 TO ga_reg.getLength()
         INSERT INTO tmp_rep_excel ( credito, cliente, monto,
            porcentaje, cuenta, bufete, number)
           --VALUES (g_reg.contact_operacion, g_reg.contac_name, g_reg.contact_total,
           -- g_reg.contact_porc_serv, cuenta, telefono, g_reg.contact_tel_cliente) 
           VALUES (ga_reg[i].contact_operacion, ga_reg[i].contac_name, monto,
            porcentaje, cuenta, telefono, ga_reg[i].contact_tel_cliente)
      END FOR       
   END FOREACH 

END FUNCTION 