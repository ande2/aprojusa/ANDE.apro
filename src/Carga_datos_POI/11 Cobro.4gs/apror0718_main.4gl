################################################################################
# Usa la plantilla "LIQUIDACION_ABOGADOS.xlsx"
# Al final hace un "Guardar como " usando  CALL fgl_excel.workbook_writeToFile(workbook, filename)
# Y lo guarda como "LIQUIDACION_ABOGADOS.xls" para que sea compatible con versiones anteriores de Excel
# Carlos Santizo
#
################################################################################



IMPORT FGL fgl_excel

SCHEMA aprojusa

DEFINE luser   LIKE users.user_id
DEFINE lgrupo  LIKE grupo.grpnombre
DEFINE lsede   LIKE city.city_num
DEFINE lfec_ini, lfec_fin DATE  
 
MAIN
DEFINE g_reg RECORD LIKE city.*
DEFINE sql STRING
DEFINE filename STRING
DEFINE header BOOLEAN
DEFINE preview BOOLEAN
DEFINE result BOOLEAN
DEFINE fecha DATE
DEFINE fec_ini, fec_fin DATE  
DEFINE fechahora STRING 

DEFINE sql_stmt   STRING
DEFINE where_clause  STRING  


    DEFER INTERRUPT
    DEFER QUIT
    OPTIONS INPUT WRAP
    OPTIONS FIELD ORDER FORM

    -- Create and populate test database
    --CONNECT TO ":memory:+driver='dbmsqt'"
    CONNECT TO "aprojusa"
    --CALL populate()
    CALL fgl_setenv("DBDATE","dmy2-")
    --OPEN WINDOW w WITH FORM "apror0704_form"
    OPEN WINDOW w WITH FORM "apror0718_form"

    CALL fgl_settitle("Reporte de Cobros Realizados")
    
    {CS LET luser     = arg_val(1)
    LET lgrupo    = arg_val(2)
    LET lsede     = arg_val(3)
    LET lfec_ini  = arg_val(4)
    LET lfec_fin  = arg_val(5)}
    
    --DISPLAY "usuario lleva ", luser

    {CS LET where_clause = " city_num <> 1000 "
    IF lgrupo = "SEDES" OR lgrupo = "OFICINA" OR lgrupo = "SUPERVISORES" THEN  
       LET where_clause = where_clause CLIPPED, " AND city_num = ", lsede
            --" AND contact_user = '", usuario, "'"
    END IF}  
    {LET where_clause = " 1=1 "
    LET sql_stmt     = "SELECT * FROM city WHERE ", where_clause CLIPPED} 
    --DISPLAY "sql_stmt ", sql_stmt
    
    --CALL combo_din2("city_num", sql_stmt)

    
    -- Default values
    --LET sql = "SELECT * FROM contnote"

     -- Creando nombre archivo de errores a copiar a pc cliente 
    {LET fechahora      = "_"||TODAY||"_"||CURRENT HOUR TO MINUTE||"." 
    LET fechahora      = librut001_replace(fechahora,":","",40)  
    LET fechahora      = librut001_replace(fechahora,"/","",40)  
    LET fechahora      = librut001_replace(fechahora,"-","",40)}  
    
    --LET filename = "fgl_excel_generic_test.xlsx"
    --LET filename = "REPORTE-DIARIO",fechahora,".xls"
    LET filename = "COBROS.xlsx"
    
    --LET header = TRUE
    LET preview = TRUE
    
    --INPUT BY NAME sql, filename, header, preview ATTRIBUTES(UNBUFFERED, WITHOUT DEFAULTS=TRUE, ACCEPT=FALSE, CANCEL=FALSE)
    INPUT BY NAME fec_ini, fec_fin 
      {g_reg.city_num --sql, filename, header, preview} 
      ATTRIBUTES(UNBUFFERED, WITHOUT DEFAULTS=TRUE, ACCEPT=FALSE, CANCEL=FALSE)

         BEFORE INPUT
            LET fec_fin = TODAY 
            LET fec_ini = TODAY 
            DISPLAY BY NAME fec_ini, fec_fin
            {LET sql_stmt = "SELECT FIRST 1 city_num FROM city WHERE ", where_clause CLIPPED, " ORDER BY 1 " 
            PREPARE ex_stmt FROM sql_stmt
            EXECUTE ex_stmt INTO g_reg.city_num 
            DISPLAY BY NAME g_reg.city_num}
      
        ON ACTION excel ATTRIBUTES(TEXT="Generate Excel", IMAGE="mars_excel")
        
            LET sql = prepsql(fec_ini, fec_fin)
      
            IF sql_to_excel(sql, filename, header) THEN
                IF preview THEN
                    CALL fgl_putfile(filename, filename)
                    CALL ui.Interface.frontCall("standard","shellExec", filename, result)
                ELSE
                    MESSAGE "Spreadsheet created"
                END IF
            ELSE
                ERROR "Something went wrong"
            END IF
            EXIT INPUT 
        ON ACTION salir
            EXIT INPUT 
        ON ACTION close
            EXIT INPUT

    END INPUT
END MAIN


FUNCTION sql_to_excel(sql, filename, header)
DEFINE hdl base.SqlHandle
DEFINE sql STRING
DEFINE filename STRING
DEFINE header BOOLEAN
DEFINE row_idx, col_idx INTEGER 

DEFINE workbook     fgl_excel.workbookType 
DEFINE sheet        fgl_excel.sheetType  
DEFINE row          fgl_excel.rowType  
DEFINE cell         fgl_excel.cellType 
DEFINE header_style fgl_excel.cellStyleType
DEFINE header_font  fgl_excel.fontType
DEFINE trailer_font fgl_excel.fontType

DEFINE datatype STRING
DEFINE texto STRING 
    
    LET hdl = base.SqlHandle.create()
    TRY
        CALL hdl.prepare(sql)
        CALL hdl.open()
    CATCH
        RETURN FALSE
    END TRY

    --CALL fgl_excel.workbook_create() RETURNING workbook
    CALL fgl_excel.workbook_open("REPORTE_COBROS.xlsx") RETURNING workbook
    -- create a worksheet
    --CS CALL fgl_excel.workbook_createsheet(workbook) RETURNING sheet
    CALL fgl_excel.workbook_opensheet(workbook) RETURNING sheet
    
    -- create data rows
    --cs LET row_idx = 0 
    LET row_idx = 3 
    
    WHILE TRUE
        CALL hdl.fetch()
        IF STATUS=NOTFOUND THEN
            EXIT WHILE
        END IF
        LET row_idx = row_idx + 1

        IF row_idx = 1 AND header THEN
            -- create a font, will be used in header
            CALL fgl_excel.font_create(workbook) RETURNING header_font
            CALL fgl_excel.font_set(header_font, "weight", "bold")

            -- create a style, will be used in header
            CALL fgl_excel.style_create(workbook) RETURNING header_style
            CALL fgl_excel.style_set(header_style, "alignment","center")
            CALL fgl_excel.style_font_set(header_style, header_font)
   
            -- Add column headers
            CALL fgl_excel.sheet_createrow(sheet, 0) RETURNING row
            FOR col_idx = 1 TO hdl.getResultCount()
                CALL fgl_excel.row_createcell(row, col_idx-1) RETURNING cell
                CALL fgl_excel.cell_value_set(cell, hdl.getResultName(col_idx))
                CALL fgl_excel.cell_style_set(cell, header_style)
            END FOR
        END IF
        CALL fgl_excel.sheet_createrow(sheet, IIF(header,row_idx, row_idx-1)) RETURNING row

        FOR col_idx = 1 TO hdl.getResultCount()
            CALL fgl_excel.row_createcell(row, col_idx-1) RETURNING cell
            LET datatype = hdl.getResultType(col_idx) 
            CASE 
                WHEN datatype =  "INTEGER" -- TODO check logic
                  OR datatype MATCHES "DECIMAL*"
                  OR datatype MATCHES "FLOAT*"
                  OR datatype MATCHES "*INT*"
                    CALL fgl_excel.cell_number_set(cell, hdl.getResultValue(col_idx))
                OTHERWISE
                    CALL fgl_excel.cell_value_set(cell, hdl.getResultValue(col_idx))
            END CASE
        END FOR
        
    END WHILE
   
   CALL fgl_excel.sheet_createrow(sheet, IIF(header,row_idx, row_idx+3)) RETURNING ROW
   CALL fgl_excel.row_createcell(row, 9) RETURNING cell
   
   CALL fgl_excel.font_create(workbook) RETURNING header_font
   CALL fgl_excel.font_set(header_font, "weight", "bold")

   CALL fgl_excel.style_create(workbook) RETURNING header_style
   CALL fgl_excel.style_set(header_style, "alignment","center")
   
   CALL fgl_excel.style_font_set(header_style, header_font)

   --LET texto =  "Usuario: ", arg_val(1)
   --CALL fgl_excel.cell_value_set(cell, texto)
   
   CALL fgl_excel.cell_style_set(cell, header_style)
   
    -- TODO this code should automatically size the columns
    -- However it is very very slow for reasons I can't determine
    -- Uncomment and test at your leisure
    {IF hdl IS NOT NULL THEN
        FOR col_idx = 1 TO hdl.getResultCount()
            CALL fgl_excel.sheet_autosizecolumn(sheet, col_idx-1)
        END FOR
    END IF}

    -- Write to File
    CALL fgl_excel.workbook_writeToFile(workbook, filename)

    RETURN TRUE   
END FUNCTION



FUNCTION populate()
DEFINE idx INTEGER
DEFINE rec RECORD
    integer_type INTEGER,
    date_type DATE,
    char_type CHAR(20),
    float_type FLOAT
END RECORD

    CREATE TEMP TABLE test_data 
        (integer_type INTEGER,
         date_type DATE,
         char_type VARCHAR(20),
         float_type FLOAT)

    FOR idx = 1 TO 26
        LET rec.integer_type = idx
        LET rec.date_type = TODAY+idx
        LET rec.char_type = ASCII(64+idx)
        LET rec.float_type = 1/idx
        
        INSERT INTO test_data VALUES(rec.*)
    END FOR
END FUNCTION

FUNCTION prepsql(lfec_ini, lfec_fin)
   DEFINE lfec_ini, lfec_fin DATE 
   DEFINE lfec DATE 
   DEFINE sql_stmt STRING 
   
   CALL creatmp()
   CALL carga_datos(lfec_ini, lfec_fin)
   
   LET sql_stmt = " SELECT fecha, operacion, tipologia, monto_promesa, ",
                  " contnote_boleta_monto, contnote_boleta_numero, contnote_boleta_fecha ",
      " FROM tmp_rep_excel "
   RETURN sql_stmt
END FUNCTION

FUNCTION creatmp()
   CREATE TEMP TABLE tmp_rep_excel(
      fecha                   DATE,
      operacion               CHAR(20),  
      tipologia               CHAR(40), 
      monto_promesa           VARCHAR(100,10),
      contnote_boleta_monto   DECIMAL(10,2), 
      contnote_boleta_numero  CHAR(25),  
      contnote_boleta_fecha   DATE 
   )
   
END FUNCTION  

FUNCTION carga_datos(dfec_ini, dfec_fin)

   DEFINE dfec_ini, dfec_fin DATE 
   DEFINE lfecha DATE 
   
   TYPE t_reg RECORD
      fecha                   DATE,
      operacion       CHAR(20),  
      tipologia               CHAR(40), 
      monto_promesa           VARCHAR(100,10),
      contnote_boleta_monto   DECIMAL(10,2), 
      contnote_boleta_numero  CHAR(25),  
      contnote_boleta_fecha   DATE 
   END RECORD
   DEFINE comentario          CHAR(2000)
   DEFINE loperacion          LIKE contact.contact_operacion
   DEFINE g_reg, g_new t_reg
   DEFINE g_ord RECORD
      contnote_num            LIKE contnote.contnote_contact, 
      contnote_cod_tipologia  LIKE contnote.contnote_cod_tipologia, 
      orden                   LIKE tipologia.orden
   END RECORD 
   DEFINE ga_reg DYNAMIC ARRAY OF t_reg
   DEFINE i    SMALLINT 
   DEFINE where_clause, sql_stmt STRING 
   DEFINE hay_boleta    CHAR(1)
   DEFINE ladm, llic CHAR(1)
   

   {IF lgrupo != 'ADMINISTRADORES' THEN --informe completo
      LET where_clause = " AND c.contact_user = '", luser CLIPPED, "'"
   END IF} 
   
   LET sql_stmt = " SELECT DATE(n.contnote_rec_mtime), c.contact_operacion, ", 
                  " n.contnote_cod_tipologia || '-' || t.des_tipologia, ", 
                  " NVL(n.contnote_monto_promesa, 0), NVL(n.contnote_boleta_monto,0), ", 
                  " n.contnote_boleta_numero, n.contnote_boleta_fecha ", 
                  " FROM contnote n, contact c, tipologia t ",
                  " WHERE c.contact_num = n.contnote_contact ", 
                  " AND n.contnote_cod_tipologia = t.cod_tipologia ",
                  " AND n.contnote_cod_tipologia in ",
                  " ('4','6','9','11','12','13','15','16','18','23','24','31','34','39','40','41','56','59','73','F4','83','86','D8') ",
                  " AND DATE(n.contnote_rec_mtime) >= '", dfec_ini CLIPPED, "'",
                  " AND DATE(n.contnote_rec_mtime) <= '", dfec_fin CLIPPED, "'"
      --" AND n.contnote_cod_tipologia = 18 ",
      --" AND ", 
      --where_clause CLIPPED 
      --where_clause CLIPPED 
      DISPLAY "sql_stmt -> ", sql_stmt
   PREPARE ex_stmt FROM sql_stmt
   DECLARE ccur CURSOR FOR ex_stmt
      {SELECT n.contnote_contact, n.contnote_num, 
         c.contact_operacion, n.contnote_text, 
         n.contnote_cod_tipologia, t.des_tipologia, 
         n.contnote_fecha_promesa, n.contnote_monto_promesa
         --n.contnote_rec_mtime, day(n.contnote_rec_mtime)||'/'|| month(n.contnote_rec_mtime)||'/'|| year(n.contnote_rec_mtime)
      FROM contnote n, contact c,tipologia t 
      WHERE n.contnote_contact = c.contact_num
      AND n.contnote_cod_tipologia = t.cod_tipologia
      AND DATE(contnote_rec_mtime) = TODAY 
      AND c.contact_city = sede}

   FOREACH ccur INTO g_reg.fecha, g_reg.operacion, g_reg.tipologia, g_reg.monto_promesa,
         g_reg.contnote_boleta_monto, g_reg.contnote_boleta_numero, g_reg.contnote_boleta_fecha

           
      INSERT INTO tmp_rep_excel ( fecha, operacion, tipologia, monto_promesa,
         contnote_boleta_monto, contnote_boleta_numero, contnote_boleta_fecha )
        VALUES (g_reg.fecha, g_reg.operacion, g_reg.tipologia, g_reg.monto_promesa,
         g_reg.contnote_boleta_monto, g_reg.contnote_boleta_numero, g_reg.contnote_boleta_fecha
         )
   
   END FOREACH 
   --SELECT COUNT(*) INTO i FROM tmp_rep_excel -- WHERE operacion = '7063266326'
   --DISPLAY "Cuando carga ", i 
   --Consolidando
   {DECLARE ccur2 CURSOR FOR 
      SELECT operacion
      FROM tmp_rep_excel
      GROUP BY operacion
      HAVING COUNT(*) > 1

   FOREACH ccur2 INTO loperacion

      DECLARE ccur3 CURSOR FOR 
         SELECT r.contnote_num, t.orden
            FROM tmp_rep_excel r, tipologia t
            WHERE r.cod_tipologia = t.cod_tipologia 
            AND r.operacion = loperacion
            ORDER BY 2

      LET i = 0
      FOREACH ccur3 INTO g_ord.contnote_num, g_ord.orden
      
         SELECT t.contnote_contact, t.contnote_num, t.operacion, t.comentario, 
            t.cod_tipologia, t.des_tipologia, t.fecha_promesa, t.monto_promesa
         INTO g_reg.contnote_contact, g_reg.contnote_num, 
            g_reg.contact_operacion, g_reg.contnote_text, 
            g_reg.contnote_cod_tipologia, g_reg.des_tipologia, 
            g_reg.contnote_fecha_promesa, g_reg.contnote_monto_promesa
         FROM tmp_rep_excel t
         WHERE t.contnote_num = g_ord.contnote_num
         
         IF i = 0 THEN
            LET i = 1
            LET g_new.contnote_contact       = g_reg.contnote_contact
            LET g_new.contnote_num           = g_reg.contnote_num 
            LET g_new.contact_operacion      = g_reg.contact_operacion 
            LET g_new.contnote_text          = g_reg.contnote_text
            LET g_new.contnote_cod_tipologia = g_reg.contnote_cod_tipologia
            LET g_new.des_tipologia          = g_reg.des_tipologia 
            LET g_new.contnote_fecha_promesa = g_reg.contnote_fecha_promesa
            LET g_new.contnote_monto_promesa = g_reg.contnote_monto_promesa
         ELSE 
            LET g_new.contnote_text          = g_new.contnote_text CLIPPED, "\n", g_reg.contnote_text
            LET g_new.contnote_text          = g_new.contnote_text CLIPPED, "\n", g_reg.contnote_cod_tipologia CLIPPED, ")"
            LET g_new.contnote_text          = g_new.contnote_text CLIPPED, " ", g_reg.des_tipologia CLIPPED, "\n"
         END IF 
      END FOREACH
      DELETE FROM tmp_rep_excel WHERE operacion = loperacion
      --SELECT COUNT(*) INTO i FROM tmp_rep_excel WHERE operacion = '7063266326'
      --DISPLAY "Despues del delete ", i
      INSERT INTO tmp_rep_excel ( contnote_contact, contnote_num,
         operacion, comentario, cod_tipologia, des_tipologia,
         fecha_promesa, monto_promesa )
         
        VALUES (g_new.contnote_contact, g_new.contnote_num, 
         g_new.contact_operacion, g_new.contnote_text, 
         g_new.contnote_cod_tipologia, g_new.des_tipologia, 
         g_new.contnote_fecha_promesa, g_new.contnote_monto_promesa) 
   END FOREACH} 
   --SELECT COUNT(*) INTO i FROM tmp_rep_excel WHERE operacion = '7063266326'
      --DISPLAY "Al terminar ", i
END FUNCTION 