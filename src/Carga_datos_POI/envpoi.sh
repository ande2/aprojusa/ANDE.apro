. /opt/informix/ifmx.sh
. /opt/4js/gst310/dev/envgenero

POI_HOME=/app/poi/java/poi-3.10.1
export POI_HOME

CLASSPATH=$POI_HOME/poi-3.10.1-20140818.jar:$POI_HOME/poi-ooxml-3.10.1-20140818.jar:$POI_HOME/poi-ooxml-schemas-3.10.1-20140818.jar:$POI_HOME/ooxml-lib/dom4j-1.6.1.jar:$POI_HOME/ooxml-lib/xmlbeans-2.6.0.jar:$CLASSPATH
export CLASSPATH

JRE_HOME=/app/poi/java/jdk-10.0.1
export JRE_HOME

PATH=$JRE_HOME/bin:$PATH
export PATH

LD_LIBRARY_PATH=$JRE_HOME/lib/server:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH
