{
Carga datos de un archivo excel 
Mayo 2018  
}

IMPORT FGL fgl_excel
IMPORT util
IMPORT os 
SCHEMA aprojusa 
-- Definicion de constantes y variables globales 
CONSTANT DirectorioBase = "c:\\\\"
CONSTANT ExtensionFiles = "*.xlsx *.xls" 
DEFINE   username       VARCHAR(15) 
DEFINE   nrows,ncols    INT  

-- Campos del arreglo 
DEFINE fields DYNAMIC ARRAY OF RECORD
    name      STRING, 
    type      STRING 
END RECORD

-- Filtros del arreglo 
DEFINE filter DYNAMIC ARRAY OF RECORD
    name      STRING,
    value     STRING
END RECORD

-- Subrutina principal
MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT
 
 -- Definiendo archivo de errores
   CALL startlog("errlog")

   CALL connect_db()
 
  -- Seleccionado y cargando plantilla
  CALL CargarPlantilla()
END MAIN 

-- Subrutina para seleccionar y cargar la plantilla

FUNCTION CargarPlantilla()
 DEFINE rrow            fgl_excel.RowType 
 DEFINE rsheet          fgl_excel.sheetType
 DEFINE rworkbook       fgl_excel.workbookType 
 DEFINE celda           fgl_excel.cell
 DEFINE colchar         CHAR(50) 
 DEFINE colname         STRING  
 DEFINE filename        STRING 
 DEFINE NombreArchivo   STRING 
 DEFINE ArchivoDestino  STRING 
 DEFINE ArchivoOrigen   STRING 
 DEFINE cellValue       STRING 
 DEFINE strtable        STRING
 DEFINE strregistros    STRING
 DEFINE idx,r,c,i,lg,j  INT 
 DEFINE numero          BIGINT 
 DEFINE deciml          DEC(12,2) 

 -- Obteniendo usuario
 LET username = FGL_GETENV("LOGNAME") 

 -- Cargando plantilla
 TRY 
  -- Seleccionando el archivo en excel 
  LET filename = winopenfile(DirectorioBase,
                            "Archivos",
                            ExtensionFiles,
                            "Seleccion del Plantilla a Cargar")

  IF (LENGTH(filename)>0) THEN 
    -- Abriendo archivo 
    LET ArchivoOrigen  = librut001_replace(filename,"\/","\\",40) 
    LET ArchivoDestino = "./"||os.Path.basename(filename) 
    LET NombreArchivo  = os.Path.basename(filename) 
    LET ArchivoDestino = librut001_replace(ArchivoDestino," ","",40) 

    -- Copiando el archivo del front end al back end 
    CALL librut001_getfile(ArchivoOrigen,ArchivoDestino)
  ELSE
    CALL fgl_winmessage(
    "Atencion","Carga de plantilla cancelada.","stop")
    RETURN 
  END IF 

  -- Abriendo libro de excel
  LET rworkbook = fgl_excel.workbook_open(ArchivoDestino.trim())
 
  -- Onteniendo hoja uno del libro de excel
  LET rsheet = rworkbook.getSheetAt(0) 

  -- Obteniendo numero de filas del libro de excel 
  LET nrows = rsheet.getPhysicalNumberOfRows()

  -- Obteniendo columnas de la primera fila 
  LET rrow  = rsheet.getRow(0)
  LET ncols = rrow.getLastCellNum() 

  -- Creando tabla temporal en base al numero de columnas 
  LET strtable = "CREATE TEMP TABLE tmp_excel (" 
  LET c = 0
  FOR i = 0 TO (ncols-1)
   LET c = c+1 

   -- Obteniendo valores de la celda 
   LET colname = rrow.getCell(i)
   LET colname = librut001_replace(colname," ","",100) 
   LET colname = UPSHIFT(colname) 
   LET colchar = colname 
   FOR j = 1 TO LENGTH(colchar) 
    IF colchar[j,j] MATCHES "*[A-Z]*" OR
       colchar[j,j] MATCHES "*[0-9]*" THEN
    ELSE 
       LET colchar[j,j] = " " 
    END IF 
   END FOR 
   LET colchar = librut001_replace(colchar," ","",100) 
   IF LENGTH(colchar)<=0 THEN 
      LET colchar = "COLUMNA"||c
   END IF 
   IF colchar NOT MATCHES "*[A-Z]*" THEN
      LET colchar = "COLUMNA"||c
   END IF
   LET colname = colchar 

   IF (i<(ncols-1)) THEN 
     LET strtable = strtable.trim()||colname.trim()||" VARCHAR(50)," 
   ELSE 
     LET strtable = strtable.trim()||colname.trim()||" VARCHAR(50))" 
   END IF 
  END FOR 
  IF (ncols>0) THEN 
    -- Creando tabla temporal 
    PREPARE s1 FROM strtable
    EXECUTE s1
    FREE s1 
  END IF 

  -- Cargando informacion del libro de excel y poblando tabla temporal
  -- Recorriendo filas 
  FOR r = 1 TO nrows 
   LET rrow = rsheet.getRow(r)
   IF rrow IS NULL THEN 
      CONTINUE FOR   
   END IF

   -- Recorriendo columnas 
   LET strregistros = "INSERT INTO tmp_excel VALUES (" 
   LET ncols = rrow.getLastCellNum() 
   FOR c = 0 TO (ncols-1)
    LET Celda = rrow.getCell(c)
    LET cellValue = Celda 

    -- Verificando tipo de campo 
    CASE (c)
     WHEN  0 LET numero = cellValue LET cellValue = numero 
     WHEN  1 LET numero = cellValue LET cellValue = numero 
     WHEN  3 LET numero = cellValue LET cellValue = numero 
     WHEN  5 LET deciml = cellValue LET cellValue = deciml 
     WHEN  6 LET deciml = cellValue LET cellValue = deciml 
     WHEN  7 LET deciml = cellValue LET cellValue = deciml 
     WHEN  8 LET deciml = cellValue LET cellValue = deciml 
     WHEN  9 LET deciml = cellValue LET cellValue = deciml 
     WHEN 10 LET deciml = cellValue LET cellValue = deciml 
     WHEN 11 LET deciml = cellValue LET cellValue = deciml 
     WHEN 12 LET deciml = cellValue LET cellValue = deciml 
    END CASE 

    IF cellValue IS NULL THEN 
       LET cellValue = "" 
    END IF

    -- Creando registros 
    IF (c<(ncols-1)) THEN 
       LET strregistros = strregistros.trim(),"'",cellValue,"'," 
    ELSE
       LET strregistros = strregistros.trim(),"'",cellValue,"')" 
    END IF 
   END FOR    
   IF (ncols>0) THEN 
     -- Insertando registros 
     PREPARE s2 FROM strregistros 
     EXECUTE s2
     FREE s2 
   END IF 
  END FOR    

  -- Desplegando y navegando la plantilla cargada de excel por medio de la 
  -- tabla temporal 
  CALL NavegarPlantilla(
  "tmp_excel","Plantilla [ "||NombreArchivo||" ] Registros ["||(nrows-1)||"]")

  -- Eliminando tabla temporal
  DROP TABLE tmp_excel 
 
 CATCH 
  CALL fgl_winmessage(
  "Atencion","Error al cargar la plantilla de excel.","stop")
 END TRY 
END FUNCTION 

-- Desplegando y navengando la plantilla cargada 

FUNCTION NavegarPlantilla(tabname,textName)
 DEFINE tabname  STRING
 DEFINE textName STRING
 DEFINE sql      STRING
 DEFINE d        ui.Dialog
 DEFINE done     BOOLEAN
 DEFINE nr       INT 
 DEFINE haycarga BOOLEAN  

 -- Creando campos del arreglo
 CALL CamposArray(tabName)
 CALL filter.clear()

 -- Abriendo ventana 
 OPEN WINDOW w WITH 1 ROWS, 1 COLUMNS

  -- Creando arreglo 
  CALL CrearArregloForma(tabName,textName)
  LET sql = "select * from ", tabName
  LET done = FALSE
  WHILE NOT done
   LET d = ui.Dialog.createDisplayArrayTo(fields, tabName)
   CALL d.addTrigger("ON ACTION Regresar")
   CALL d.addTrigger("ON ACTION Cargar")
   CALL d.addTrigger("ON ACTION Filtros")

   -- Llenando arreglo 
   CALL LlenarArreglo(d, sql, tabName)

   WHILE TRUE -- Evaluando acciones 
    CASE d.nextEvent()
     WHEN "ON ACTION Regresar"
          LET done = TRUE
          EXIT WHILE
     WHEN "ON ACTION Cargar"
          -- Cargando informacion a tabla de datos 
          IF librut001_YesOrNot(
           "Confirmacion",
           "Desea cargar plantilla? \n"||"Registros a cargar ("||(nrows-1)||")",
           "Si","No","question") THEN

           CALL CargarDatosTabla(tabName)
           RETURNING haycarga,nr 
           IF (nr>0) THEN 
             CALL fgl_winmessage(
             "Atencion","Plantilla cargada. ("||nr||") registros.","information") 
             LET done = TRUE
             EXIT WHILE
           ELSE 
             IF haycarga THEN
              CALL fgl_winmessage(
              "Atencion","Plantilla con todos sus datos ya registrados.","stop")
              CONTINUE WHILE 
             ELSE
              CALL fgl_winmessage(
              "Atencion","Plantilla no puede cargarse, error en datos.","stop")
              CONTINUE WHILE 
             END IF 
           END IF 
          END IF 
     WHEN "ON ACTION Filtros"
          LET sql = CreandoFiltros(tabName)
          EXIT WHILE
     WHEN "AFTER DISPLAY"
          EXIT WHILE
    END CASE
   END WHILE
   CALL d.close()
  END WHILE
 CLOSE WINDOW w
END FUNCTION

-- Creando campos del arreglo en base a los campos de la tabla temporal 
-- Campos de la plantilla 

FUNCTION CamposArray(tabName)
 DEFINE tabName STRING
 DEFINE h       base.SqlHandle
 DEFINE i       INT

 -- Creando campos 
 LET h = base.SqlHandle.create()
 CALL h.prepare("select * from " || tabName)
 CALL h.open()
 CALL fields.clear()
 FOR i = 1 TO h.getResultCount()
  LET fields[i].name = h.getResultName(i)
  LET fields[i].type = h.getResultType(i)
 END FOR
 CALL h.close()
END FUNCTION

-- Subrutina para llenar el arreglo con los datos de una tabla 
-- Datos de la plantilla 

FUNCTION LlenarArreglo(d, sql, tabName)
 DEFINE d       ui.Dialog
 DEFINE tabName STRING
 DEFINE sql     STRING
 DEFINE h       base.SqlHandle
 DEFINE i,j     INT

 -- Llenando arreglo 
 LET h = base.SqlHandle.create()
 CALL h.prepare(sql)
 CALL h.open()
 CALL h.fetch()
 LET j = 0

 WHILE status == 0
  LET j = j + 1
  CALL d.setCurrentRow(tabName, j) 
  FOR i = 1 TO h.getResultCount()
   CALL d.setFieldValue(h.getResultName(i), h.getResultValue(i))
  END FOR
  CALL h.fetch()
  END WHILE
 CALL d.setCurrentRow(tabName, 1)
 CALL h.close()
END FUNCTION

-- Subrutina para desplegar la forma con el arreglo

FUNCTION CrearArregloForma(tabName,textName)
 DEFINE tabName                                     STRING
 DEFINE textName                                    STRING 
 DEFINE colName, colType                            STRING
 DEFINE f                                           ui.Form
 DEFINE w                                           ui.Window
 DEFINE window, form, grid, table, formfield, edit  om.DomNode
 DEFINE i                                           INT

 -- Creando forma 
 LET w = ui.Window.getCurrent()
 LET f = w.createForm("test")
 LET form = f.getNode()

 -- Creando atributos de la forma 
 LET window = form.getParent()
 CALL window.setAttribute("text", textName)
 LET grid = form.createChild("Grid")
 CALL grid.setAttribute("width", 1)
 CALL grid.setAttribute("height", 1)
 LET table = grid.createChild("Table")
 CALL table.setAttribute("doubleClick", "update")
 CALL table.setAttribute("tabName", tabName)
 CALL table.setAttribute("pageSize", 10)
 CALL table.setAttribute("gridWidth", 1)
 CALL table.setAttribute("gridHeight", 1)

 -- Creando columnas de la tabla 
 FOR i = 1 TO fields.getLength()
  LET formfield = table.createChild("TableColumn")
  LET colName = fields[i].name
  LET colType = fields[i].type
  CALL formfield.setAttribute("text", colName)
  CALL formfield.setAttribute("colName", colName)
  CALL formfield.setAttribute("name", tabName || "." || colName)
  CALL formfield.setAttribute("sqlType", colType)
  CALL formfield.setAttribute("tabIndex", i + 1)
  LET edit = formfield.createChild("Edit")
  CALL edit.setAttribute("width", MejorAncho(colType))
 END FOR
END FUNCTION

-- Subrutina para ajustar el mejor archo de una columna para setear el atributo
-- en la forma 

FUNCTION MejorAncho(t)
 DEFINE t STRING
 DEFINE i, j, len INT

 IF (i := t.getIndexOf('(', 1)) > 0 THEN
  IF (j := t.getIndexOf(',', i + 1)) == 0 THEN
   LET j = t.getIndexOf(')', i + 1)
  END IF
  LET len = t.subString(i + 1, j - 1)
  LET t = t.subString(1, i - 1)
 END IF

 -- Verificando tipo de dato 
 CASE t
  WHEN "BOOLEAN"    RETURN 1
  WHEN "TINYINT"    RETURN 4
  WHEN "SMALLINT"   RETURN 6
  WHEN "INTEGER"    RETURN 11
  WHEN "BIGINT"     RETURN 20
  WHEN "SMALLFLOAT" RETURN 14
  WHEN "FLOAT"      RETURN 14
  WHEN "STRING"     RETURN 20
  WHEN "DECIMAL"    RETURN IIF(len IS NULL, 16, LEN + 2)
  WHEN "MONEY"      RETURN IIF(len IS NULL, 16, LEN + 2)
  WHEN "CHAR"       RETURN IIF(len IS NULL, 1, IIF (len > 20, 20, len))
  WHEN "VARCHAR"    RETURN IIF(len IS NULL, 1, IIF (len > 20, 20, len))
  WHEN "DATE"       RETURN 10
  OTHERWISE         RETURN 20
 END CASE
END FUNCTION

-- Subrutina para reemplazar una parte de una cadeca de caracteres 

FUNCTION librut001_replace(hilera,pattern,replace,ntimes)
 DEFINE buf                    base.StringBuffer,
        ntimes                 SMALLINT,
        hilera,replace,pattern STRING

 -- Creando buffer
 LET buf = base.StringBuffer.create()

 -- Agregando hilera al buffer
 CALL buf.append(hilera)

 -- Reemplazando string buscado x nuevo string en la hilera x n veces
 CALL buf.replace(pattern,replace,ntimes)

 -- Returnando el string
 RETURN buf.toString()
END FUNCTION

-- Subrutina para transferir un archivo del front-end al server

FUNCTION librut001_getfile(fs,ft)
 DEFINE fs,ft STRING

 -- Obteniendo el archivo origen
 CALL fgl_getfile(fs,ft)
END FUNCTION

-- Subrutina para cargar los datos a la table 

FUNCTION CargarDatosTabla(tabName)
 DEFINE nr              RECORD LIKE contact.* 
 DEFINE r               RECORD 
         Codigo_Region	CHAR(100), 
         Operacion	   CHAR(100),
         Producto	      CHAR(100),
         Cuotas_Mora	   CHAR(100),
         Fecha_Mora	   CHAR(100),
         Saldo_Cap 	   CHAR(100),
         Cap_Vencido 	CHAR(100),
         Int_Vencido 	CHAR(100),
         Saldo_Imo 	   CHAR(100),
         Otros 	      CHAR(100),
         Total          CHAR(100),
         Porcentaje     CHAR(100),
         Total_Cobrar   CHAR(100),
         Estado_Op      CHAR(100),
         Cliente	      CHAR(100),
         Direccion_Cli  CHAR(240),
         Des_Garantia   CHAR(100),
         Tel_Cliente    CHAR(240),
         Fecha_ultpago  CHAR(100),
         DPI_Cliente    CHAR(100),
         Nom_Fiador1    CHAR(240),
         Dir_Fiador1    CHAR(240),
         Nom_Fiador2    CHAR(240),
         Dir_Fiador2    CHAR(240),
         Nom_Fiador3    CHAR(240),
         Dir_Fiador3    CHAR(240)
        END RECORD 

 DEFINE sql             STRING 
 DEFINE tabName         STRING
 DEFINE n,conteo        INT 
 DEFINE mensaje STRING 

 -- Seleccionando datos 
 TRY
  --WHENEVER ERROR CONTINUE 
  -- Preparando busqueda 
  LET sql = "SELECT x.* FROM ",tabname CLIPPED," x ORDER BY rowid "
  PREPARE p1 FROM sql 
  DECLARE c1 CURSOR FOR p1 

  LET n=0
  MESSAGE "Cargando Prestamo ..." 
  FOREACH c1 INTO r.* 
   -- Llenando campos de la tabla contact
   INITIALIZE nr.* TO NULL 
   
   -- Asignando llave primaria
   LET nr.contact_operacion = r.Operacion 
   
   -- Chequeando que no exista ya el registro
   SELECT COUNT(*)
    INTO  conteo 
    FROM  contact a
    WHERE a.contact_operacion = nr.contact_operacion  
    IF conteo>0 THEN
       CONTINUE FOREACH
    END IF 
    LET n=n+1 
    MESSAGE "Cargando Registro #"||n||" Prestamo ( "||nr.contact_operacion||")"

   -- Obteniendo numero maximo
   SELECT (NVL(MAX(a.contact_num),0)+1) 
    INTO  nr.contact_num 
    FROM  contact a 

   LET nr.contact_rec_muser                = username 
   LET nr.contact_rec_mtime                = CURRENT
   LET nr.contact_rec_mstat                = "01"                 -- Verificar
   LET nr.contact_name                     = r.Cliente 
   LET nr.contact_producto                 = r.Producto 
   LET nr.contact_cuotas_mora              = r.Cuotas_Mora 
   LET nr.contact_fec_mora                 = r.Fecha_Mora   
   LET nr.contact_saldo_cap                = r.Saldo_Cap 
   LET nr.contact_cap_vencido              = r.Cap_Vencido  
   LET nr.contact_int_vencido              = r.Int_Vencido 
   LET nr.contact_saldo_imo                = r.Saldo_Imo 
   LET nr.contact_otros                    = r.Otros 
   LET nr.contact_total                    = r.Total 
   LET nr.contact_porc_serv                = r.porcentaje 
   LET nr.contact_total_cobrar             = r.Total_Cobrar 
   LET nr.contact_estado_op                = r.Estado_Op  
   LET nr.contact_garantia                 = r.Des_Garantia
   LET nr.contact_tel_cliente              = r.Tel_Cliente
   LET nr.contact_fec_ult_pago             = r.Fecha_UltPago 
   LET nr.contact_dpi_cliente              = r.DPI_Cliente 
   LET nr.contact_nom_fia1                 = r.Nom_Fiador1
   LET nr.contact_dir_fia1                 = r.Dir_Fiador1
   LET nr.contact_nom_fia2                 = r.Nom_Fiador2
   LET nr.contact_dir_fia2                 = r.Dir_Fiador2
   LET nr.contact_nom_fia3                 = r.Nom_Fiador3
   LET nr.contact_dir_fia3                 = r.Dir_Fiador3
   LET nr.contact_valid                    = "1"             -- Verificar valor 
   LET nr.contact_street                   = r.Direccion_Cli
   LET nr.contact_city                     = 1               -- Verificar valor
   LET nr.contact_num_m                    = NULL 
   LET nr.contact_num_w                    = NULL 
   LET nr.contact_num_h                    = NULL 
   LET nr.contact_user                     = "user"          -- Verificar valor
   LET nr.contact_loc_lon                  = 0               -- Verificar valor
   LET nr.contact_loc_lat                  = 0               -- Verificar valor
   LET nr.contact_photo_mtime              = NULL
   --LET nr.contact_photo byte               = 0 
   LET nr.contact_cod_region               = r.Codigo_Region 
   LET nr.contact_saldo_imo                = NULL
   LET nr.contact_estado_caso              = NULL
   LET nr.contact_when                     = CURRENT 
 
   -- Insertanto registro 
   INSERT INTO contact (
    contact_num,
    contact_rec_muser,
    contact_rec_mtime,
    contact_rec_mstat,
    contact_name,
    contact_operacion,
    contact_producto,
    contact_cuotas_mora,
    contact_fec_mora,
    contact_saldo_cap,
    contact_cap_vencido,
    contact_int_vencido,
    contact_saldo_imo,
    contact_otros,
    contact_total,
    contact_porc_serv,
    contact_total_cobrar,
    contact_estado_op,
    contact_garantia,
    contact_tel_cliente,
    contact_fec_ult_pago,
    contact_dpi_cliente,
    contact_nom_fia1,
    contact_dir_fia1,
    contact_nom_fia2,
    contact_dir_fia2,
    contact_nom_fia3,
    contact_dir_fia3,
    contact_valid,
    contact_street,
    contact_city,
    contact_num_m,
    contact_num_w,
    contact_num_h,
    contact_user,
    contact_loc_lon,
    contact_loc_lat,
    contact_photo_mtime,
    contact_cod_region,
    contact_estado_caso,
    contact_when) 

   VALUES (
    nr.contact_num,
    nr.contact_rec_muser,
    nr.contact_rec_mtime,
    nr.contact_rec_mstat,
    nr.contact_name,
    nr.contact_operacion,
    nr.contact_producto,
    nr.contact_cuotas_mora,
    nr.contact_fec_mora,
    nr.contact_saldo_cap,
    nr.contact_cap_vencido,
    nr.contact_int_vencido,
    nr.contact_saldo_imo,
    nr.contact_otros,
    nr.contact_total,
    nr.contact_porc_serv,
    nr.contact_total_cobrar,
    nr.contact_estado_op,
    nr.contact_garantia,
    nr.contact_tel_cliente,
    nr.contact_fec_ult_pago,
    nr.contact_dpi_cliente,
    nr.contact_nom_fia1,
    nr.contact_dir_fia1,
    nr.contact_nom_fia2,
    nr.contact_dir_fia2,
    nr.contact_nom_fia3,
    nr.contact_dir_fia3,
    nr.contact_valid,
    nr.contact_street,
    nr.contact_city,
    nr.contact_num_m,
    nr.contact_num_w,
    nr.contact_num_h,
    nr.contact_user,
    nr.contact_loc_lon,
    nr.contact_loc_lat,
    nr.contact_photo_mtime,
    nr.contact_cod_region,
    nr.contact_estado_caso,
    nr.contact_when) 

   -- INSERT INTO contact VALUES (nr.*) 
  END FOREACH  
  CLOSE c1 
  FREE  c1 
  MESSAGE "" 
  RETURN TRUE,n 

 CATCH

  -- Mostrando error
   --LET mensaje = "Error ", sqlca.sqlcode, nr.*
  CALL fgl_winmessage(
  "Atencion",err_get(status),"stop") 
  --"Atencion",mensaje,"stop") 
  RETURN FALSE,0 

 END TRY
END FUNCTION

-- Subrutina para el menu de confirmacion a una pregunta 

FUNCTION librut001_yesornot(title,msg,opc1,opc2,icon)
 DEFINE ope  SMALLINT,
        title,
        msg,
        opc1,
        opc2,
        icon STRING

  MENU title
  ATTRIBUTE(STYLE="dialog",COMMENT=msg,IMAGE=icon)
  COMMAND opc1
   LET ope = 1
  COMMAND opc2
   LET ope = 0
  END MENU
  RETURN ope
END FUNCTION

-- Subrutina para crear filtros sobre la plantilla cargada 

FUNCTION CreandoFiltros(tabName)
 DEFINE tabName   STRING
 DEFINE d         ui.Dialog
 DEFINE i, j      INT
 DEFINE s, q, sql STRING

 -- Abirendo ventana de filtros 
 OPEN WINDOW modify WITH 1 ROWS, 1 COLUMNS
  CALL CrearDetalleForma(tabName, "Filtros", TRUE)

  LET d = ui.Dialog.createConstructByName(fields)
  -- Restaurando filtros originales  
  FOR i = 1 TO filter.getLength()
   CALL d.setFieldValue(filter[i].name, filter[i].value)
  END FOR

  -- Creando acciones 
  CALL d.addTrigger("ON ACTION accept")
  WHILE TRUE
   CASE d.nextEvent()
    WHEN "ON ACTION accept"
        CALL d.accept()
    WHEN "AFTER CONSTRUCT"
        EXIT WHILE
   END CASE
  END WHILE

  -- Creando busqueda  
  CALL filter.clear()
  FOR i = 1 TO fields.getLength()
   LET s = d.getQueryFromField(fields[i].name)
   IF s IS NOT NULL THEN
      LET j = j + 1

      -- Salvando el filtro 
      LET filter[j].name = fields[i].name
      LET filter[j].value = d.getFieldValue(fields[i].name)
      
      IF q IS NOT NULL THEN
         LET q = q, " AND "
      END IF
      LET q = q, s CLIPPED
   END IF
  END FOR
  CALL d.close()
 CLOSE WINDOW modify

 LET sql = "select * from ", tabName
 IF q IS NOT NULL THEN
    LET sql = sql, " where ", q
 END IF
 RETURN (sql)
END FUNCTION

-- Subrutina para crear el detalle de la forma con el arreglo

FUNCTION CrearDetalleForma(tabName, title, forConstruct)
 DEFINE tabName                                      STRING
 DEFINE title                                        STRING
 DEFINE colName, colType                             STRING
 DEFINE forConstruct                                 BOOLEAN
 DEFINE f                                            ui.Form
 DEFINE w                                            ui.Window
 DEFINE i, width                                     INT
 DEFINE window, form, grid, label, formfield, edit   om.DomNode
 DEFINE screenRecord, link                           om.DomNode

 -- Crando ventana y forma 
 LET w = ui.Window.getCurrent()
 LET f = w.createForm("modify")
 LET form = f.getNode()

 -- Creando atributos de la ventana 
 LET window = form.getParent()
 CALL window.setAttribute("text", title || " Plantilla")

 -- Creando grid  
 LET grid = form.createChild("Grid")
 CALL grid.setAttribute("width", 2)
 CALL grid.setAttribute("height", fields.getLength())
 LET screenRecord = form.createChild("RecordView")
 CALL screenRecord.setAttribute("tabName", tabName)

 -- Creando campos  
 FOR i = 1 TO fields.getLength()
  LET colName = fields[i].name
  LET colType = fields[i].type

  -- Creando atributos 
  LET label = grid.createChild("Label")
  CALL label.setAttribute("posX", 0)
  CALL label.setAttribute("posY", i - 1)
  CALL label.setAttribute("gridWidth", 1)
  CALL label.setAttribute("text", colName)
  LET formfield = grid.createChild("FormField")
  CALL formfield.setAttribute("colName", colName)
  CALL formfield.setAttribute("name", tabName || "." || colName)
  CALL formfield.setAttribute("sqlType", colType)
  CALL formfield.setAttribute("fieldId", i)
  CALL formfield.setAttribute("tabIndex", i + 1)
  LET edit = formfield.createChild(IIF(colType=="DATE", "DateEdit", "Edit"))
  CALL edit.setAttribute("posX", 1)
  CALL edit.setAttribute("posY", i - 1)
  LET width = IIF(forConstruct, 20, MejorAncho(colType))
  CALL edit.setAttribute("gridWidth", width)
  CALL edit.setAttribute("width", width)
  LET link = screenRecord.createChild("Link")
  CALL link.setAttribute("colName", colName)
  CALL link.setAttribute("fieldIdRef", i)
 END FOR
 CALL form.writeXml("test.42f")
END FUNCTION

FUNCTION connect_db()
   DATABASE aprojusa
END FUNCTION 