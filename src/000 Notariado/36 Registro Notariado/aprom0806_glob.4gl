-- Definicion de variables globales

SCHEMA aprojusa

GLOBALS 
 CONSTANT prog_name              = "aprom0806"
 CONSTANT titulo1                = "Registro Notariado"
 CONSTANT AdmonNotariado         = "ADMINISTRADOR-NOTARIADO" 
 CONSTANT ValorMaxTimbreNotarial = 300
 CONSTANT DosPorMillar           = 0.002 
 CONSTANT PorcenIVA              = 0.12
 CONSTANT ValorInscripcionFinca  = 50
 CONSTANT PuntoCincoPorMillar    = 0.0015
 CONSTANT ValorBaseExcedenteRPG  = 10000
 CONSTANT ValorInsFincaRPG       = 50
 CONSTANT ValorConsultasElectro  = 10 
 CONSTANT ValorHojaProtocolo     = 20
 CONSTANT ValorDiaRechazo        = 25
  
 TYPE 
  tDet                RECORD
   numexp             LIKE notariado.numexp,
   codpro             LIKE notariado.codpro,
   numcre             LIKE notariado.numcre,
   tipdoc             LIKE notariado.tipdoc,
   city_num           LIKE notariado.city_num,
   numage             LIKE notariado.numage,
   numres             LIKE notariado.numres,
   fecres             LIKE notariado.fecres,
   codasn             LIKE notariado.codasn,
   fecasi             LIKE notariado.fecasi,
   nomcli             LIKE notariado.nomcli,
   valcre             LIKE notariado.valcre,
   codabo             LIKE notariado.codabo,
   numesc             LIKE notariado.numesc,
   fecesc             LIKE notariado.fecesc,
   hojini             LIKE notariado.hojini,
   hojfin             LIKE notariado.hojfin, 
   hojin2             LIKE notariado.hojin2,
   hojfi2             LIKE notariado.hojfi2, 
   todipt             LIKE notariado.todipt,
   todies             LIKE notariado.todies,
   fecini             LIKE notariado.fecini,
   usrini             LIKE notariado.usrini,
   horini             LIKE notariado.horini,
   feesct             LIKE notariado.feesct,
   usresc             LIKE notariado.usresc,
   horesc             LIKE notariado.horesc,
   nofase             LIKE notariado.nofase,
   estado             LIKE notariado.estado,
   vtcoba             LIKE notariado.vtcoba,
   vtocob             LIKE notariado.vtocob,
   fecdsg             LIKE notariado.fecdsg,
   usrdsg             LIKE notariado.usrdsg,
   hordsg             LIKE notariado.hordsg,
   fevfir             LIKE notariado.fevfir,
   frefir             LIKE notariado.frefir,
   tdifir             LIKE notariado.tdifir,
   finrpg             LIKE notariado.finrpg,
   fegrpg             LIKE notariado.fegrpg,
   nrerpg             LIKE notariado.nrerpg,
   ndbrpg             LIKE notariado.ndbrpg,
   toparp             LIKE notariado.toparp,
   topaco             LIKE notariado.topaco,
   codrgo             LIKE notariado.codrgo,
   codarp             LIKE notariado.codarp,
   todifo             LIKE notariado.todifo,
   fecfor             LIKE notariado.fecfor,
   usrfor             LIKE notariado.usrfor,
   horfor             LIKE notariado.horfor,
   canrch             LIKE notariado.canrch,
   totrch             LIKE notariado.totrch,
   ferech             LIKE notariado.ferech,
   fecrei             LIKE notariado.fecrei,
   nrecrg             LIKE notariado.nrecrg,
   ndocrg             LIKE notariado.ndocrg,
   fecdev             LIKE notariado.fecdev,
   todidv             LIKE notariado.todidv,
   todich             LIKE notariado.todich,
   fecrch             LIKE notariado.fecrch,
   usrrch             LIKE notariado.usrrch,
   horrch             LIKE notariado.horrch,
   totpag             LIKE notariado.totpag,
   numbol             LIKE notariado.numbol,
   fecbol             LIKE notariado.fecbol,
   numdep             LIKE notariado.numdep,
   fecdap             LIKE notariado.fecdap,
   usrdap             LIKE notariado.usrdap,
   hordap             LIKE notariado.hordap,
   fecdeb             LIKE notariado.fecdeb,
   nuboho             LIKE notariado.nuboho,
   modeho             LIKE notariado.modeho,
   nuboga             LIKE notariado.nuboga,
   modega             LIKE notariado.modega,
   nufade             LIKE notariado.nufade,
   fecest             LIKE notariado.fecest,
   usrest             LIKE notariado.usrest,
   horest             LIKE notariado.horest,
   lugenv             LIKE notariado.lugenv,
   fecenv             LIKE notariado.fecenv,
   fecrep             LIKE notariado.fecrep,
   nucoev             LIKE notariado.nucoev,
   todiev             LIKE notariado.todiev,
   diapro             LIKE notariado.diapro,
   fecpcl             LIKE notariado.fecpcl,
   usrpcl             LIKE notariado.usrpcl,
   horpcl             LIKE notariado.horpcl,
   numeta             LIKE notariado.numeta,
   fpagas             LIKE notariado.fpagas,
   mpagno             LIKE notariado.mpagno,
   observ             LIKE notariado.observ,
   coment             LIKE notariado.coment,
   fecdfi             LIKE notariado.fecdfi,
   usrdfi             LIKE notariado.usrdfi,
   hordfi             LIKE notariado.hordfi,
   codgru             LIKE notariado.codgru 
 END RECORD

 TYPE 
  fDet                RECORD 
   fnumexp            LIKE notariado.numexp,
   fnumesc            LIKE notariado.numesc,
   fcodpro            LIKE notariado.codpro,
   fnompro            LIKE programas_not.nombre,
   fnumcre            LIKE notariado.numcre,
   ftipdoc            LIKE notariado.tipdoc,
   fnomdoc            LIKE tiposdocumento.nombre,
   fnomsed            CHAR(40), 
   fnumres            LIKE notariado.numres,
   fnomcli            LIKE notariado.nomcli,
   fvalcre            LIKE notariado.valcre,
   fnometa            LIKE etapas_not.nometa
 END RECORD

 TYPE
  cDet RECORD
   tipcob             LIKE notariado_cob.tipcob, 
   nomcob             LIKE tiposcobro.nomcob, 
   ingcan             LIKE notariado_cob.ingcan, 
   sumsub             LIKE notariado_cob.sumsub, 
   mulcx1             LIKE notariado_cob.mulcx1, 
   caliva             LIKE notariado_cob.caliva, 
   valsum             LIKE notariado_cob.valsum,
   valrst             LIKE notariado_cob.valres, 
   curcob             CHAR(1),
   cancob             LIKE notariado_cob.cancob, 
   valcob             LIKE notariado_cob.valcob,     
   totcob             LIKE notariado_cob.totcob
 END RECORD

 DEFINE reg_det_attr  DYNAMIC ARRAY OF RECORD
   fnumexp            STRING,
   fnumesc            STRING,
   fcodpro            STRING,
   fnompro            STRING,
   fnumcre            STRING,
   ftipdoc            STRING,
   fnomdoc            STRING,
   fnomsed            STRING,   
   fnumres            STRING,
   fnomcli            STRING,
   fvalcre            STRING,
   fnometa            STRING
 END RECORD

 DEFINE v_cobros_attr DYNAMIC ARRAY OF RECORD
   tipcob             STRING,
   nomcob             STRING,
   ingcan             STRING,
   sumsub             STRING,
   mulcx1             STRING,
   caliva             STRING,
   valsum             STRING, 
   valrst             STRING, 
   curcob             STRING,
   cancob             STRING,
   valcob             STRING,
   totcob             STRING
 END RECORD
 
 DEFINE g_reg,u_reg   tDet
 DEFINE reg_det       DYNAMIC ARRAY OF fDet
 DEFINE v_cobros      DYNAMIC ARRAY OF cDet 
 DEFINE dbname        STRING
 DEFINE condicion     STRING
 DEFINE usuario       STRING
 DEFINE GrupoUsuario  LIKE grupo.grpnombre
 DEFINE SedeUsuario   LIKE users.city_num
 DEFINE SedeAlterna   LIKE users.city_num
 DEFINE filename  STRING 
 DEFINE filebase  STRING
END GLOBALS
