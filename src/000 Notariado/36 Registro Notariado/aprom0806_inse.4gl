-- Insertando datos registro notariado

GLOBALS "aprom0806_glob.4gl"

FUNCTION insert_init()
 DEFINE strSql STRING 

 LET strSql =
  "INSERT INTO notariado (",
          "numexp,   codpro,", "numcre,   tipdoc,",
          "city_num, numage,", "numres,   fecres,",
          "codasn,   fecasi,", "nomcli,   valcre,",
          "fecini,   usrini,", "horini,   feesct,",
          "usresc,   horesc,", "nofase,   estado,",
          "todipt,   todies,", "vtcoba,   vtocob,",
          "fecdsg,   usrdsg,", "hordsg,",
          "fevfir,   frefir,", "tdifir,   finrpg,",
          "fegrpg,   nrerpg,", "ndbrpg,   toparp,",
          "topaco,   codrgo,", "codarp,   todifo,",
          "fecfor,   usrfor,", "horfor,   canrch,",
          "totrch,   ferech,", "fecrei,   nrecrg,",
          "ndocrg,   fecdev,", "todidv,   todich,",
          "fecrch,   usrrch,", "horrch,   totpag,",
          "numbol,   fecbol,", "numdep,   fecdap,",
          "usrdap,   hordap,", "fecdeb,   nuboho,",
          "modeho,   nuboga,", "modega,   nufade,",
          "fecest,   usrest,", "horest,   lugenv,",
          "fecenv,   fecrep,", "nucoev,   todiev,",
          "diapro,   fecpcl,", "usrpcl,   horpcl,", 
          "numeta,   fpagas,", "mpagno,   observ,",
          "coment,   fecdfi,", "usrdfi,   hordfi,",
          "codgru) ", 
  "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,",
          "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,",
          "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,",
          "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,",
          "?,?,?,?,?,?,?,?)" 

 PREPARE st_insertar FROM strSql
END FUNCTION 

FUNCTION ingreso(nofase SMALLINT)
 -- CALL encabezado("Ingresar")
 IF captura_datos('I',nofase) THEN
    RETURN grabar()
 ELSE 
    RETURN FALSE 
 END IF 
END FUNCTION 

FUNCTION grabar() 
 -- Inicializando datos
 LET g_reg.numexp = 0
 LET g_reg.fecini = CURRENT
 LET g_reg.usrini = usuario
 LET g_reg.horini = CURRENT
 LET g_reg.nofase = 1
 LET g_reg.estado = 1
 LET g_reg.todies = 0
 LET g_reg.todipt = 0
 LET g_reg.vtcoba = 0    
 LET g_reg.vtocob = 0 
 LET g_reg.tdifir = 0
 LET g_reg.toparp = 0
 LET g_reg.topaco = 0 
 LET g_reg.todifo = 0 
 LET g_reg.totrch = 0
 LET g_reg.todidv = 0
 LET g_reg.todich = 0
 LET g_reg.totpag = 0
 LET g_reg.modeho = 0
 LET g_reg.modega = 0 
 LET g_reg.todiev = 0 
 LET g_reg.diapro = 0 
 
 TRY 
  -- Insertando datos iniciales 
  EXECUTE st_insertar 
  USING   g_reg.numexp,   g_reg.codpro, g_reg.numcre, g_reg.tipdoc,
          g_reg.city_num, g_reg.numage, g_reg.numres, g_reg.fecres,
          g_reg.codasn,   g_reg.fecasi, g_reg.nomcli, g_reg.valcre,
          g_reg.fecini,   g_reg.usrini, g_reg.horini, g_reg.feesct,
          g_reg.usresc,   g_reg.horesc, g_reg.nofase, g_reg.estado, 
          g_reg.todipt,   g_reg.todies, g_reg.vtcoba, g_reg.vtocob,   
          g_reg.fecdsg,   g_reg.usrdsg, g_reg.hordsg, g_reg.fevfir,   
          g_reg.frefir,   g_reg.tdifir, g_reg.finrpg, g_reg.fegrpg,   
          g_reg.nrerpg,   g_reg.ndbrpg, g_reg.toparp, g_reg.topaco,   
          g_reg.codrgo,   g_reg.codarp, g_reg.todifo, g_reg.fecfor,   
          g_reg.usrfor,   g_reg.horfor, g_reg.canrch, g_reg.totrch,   
          g_reg.ferech,   g_reg.fecrei, g_reg.nrecrg, g_reg.ndocrg,   
          g_reg.fecdev,   g_reg.todidv, g_reg.todich, g_reg.fecrch,   
          g_reg.usrrch,   g_reg.horrch, g_reg.totpag, g_reg.numbol,   
          g_reg.fecbol,   g_reg.numdep, g_reg.fecdap, g_reg.usrdap,   
          g_reg.hordap,   g_reg.fecdeb, g_reg.nuboho, g_reg.modeho,   
          g_reg.nuboga,   g_reg.modega, g_reg.nufade, g_reg.fecest,   
          g_reg.usrest,   g_reg.horest, g_reg.lugenv, g_reg.fecenv,   
          g_reg.fecrep,   g_reg.nucoev, g_reg.todiev, g_reg.diapro,   
          g_reg.fecpcl,   g_reg.usrpcl, g_reg.horpcl, g_reg.numeta,   
          g_reg.fpagas,   g_reg.mpagno, g_reg.observ, g_reg.coment,   
          g_reg.fecdfi,   g_reg.usrdfi, g_reg.hordfi, g_reg.codgru 

  -- Calculando y grabando datos iniciales del desglose 
  LET g_reg.numexp = SQLCA.SQLERRD[2] 
  CALL CalculaCobros(1,1,1)

 CATCH 
  CALL msgError(sqlca.sqlcode,"Grabar Registro.")
  RETURN FALSE 
 END TRY
 DISPLAY BY NAME g_reg.*
 CALL box_valdato ("Registro agregado.")
 RETURN TRUE
END FUNCTION 
