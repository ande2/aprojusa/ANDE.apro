-- Modificando datos registro notariado

GLOBALS "aprom0806_glob.4gl"

FUNCTION update_init()
 DEFINE strSql STRING 

 LET strSql = 
 "UPDATE notariado ",
 "SET ","codpro   = ?,", "numcre   = ?,", "tipdoc   = ?,", "city_num = ?,",
        "numage   = ?,", "numres   = ?,", "fecres   = ?,", "codasn   = ?,",
        "fecasi   = ?,", "nomcli   = ?,", "valcre   = ?,", "codabo   = ?,",
        "numesc   = ?,", "fecesc   = ?,", "hojini   = ?,", "hojfin   = ?,",
        "hojin2   = ?,", "hojfi2   = ?,",
        "todipt   = ?,", "todies   = ?,", "fecini   = ?,", "usrini   = ?,",
        "horini   = ?,", "feesct   = ?,", "usresc   = ?,", "horesc   = ?,",
        "nofase   = ?,", "estado   = ?,", "vtcoba   = ?,",
        "vtocob   = ?,", "fecdsg   = ?,", "usrdsg   = ?,", "hordsg   = ?,",
        "fevfir   = ?,", "frefir   = ?,", "tdifir   = ?,", "finrpg   = ?,",
        "fegrpg   = ?,", "nrerpg   = ?,", "ndbrpg   = ?,", "toparp   = ?,",
        "topaco   = ?,", "codrgo   = ?,", "codarp   = ?,", "todifo   = ?,",
        "fecfor   = ?,", "usrfor   = ?,", "horfor   = ?,", "canrch   = ?,",
        "totrch   = ?,", "ferech   = ?,", "fecrei   = ?,", "nrecrg   = ?,",
        "ndocrg   = ?,", "fecdev   = ?,", "todidv   = ?,", "todich   = ?,",
        "fecrch   = ?,", "usrrch   = ?,", "horrch   = ?,", "totpag   = ?,", 
        "numbol   = ?,", "fecbol   = ?,", "numdep   = ?,", "fecdap   = ?,", 
        "usrdap   = ?,", "hordap   = ?,", "fecdeb   = ?,", "nuboho   = ?,", 
        "modeho   = ?,", "nuboga   = ?,", "modega   = ?,", "nufade   = ?,", 
        "fecest   = ?,", "usrest   = ?,", "horest   = ?,", "lugenv   = ?,",
        "fecenv   = ?,", "fecrep   = ?,", "nucoev   = ?,", "todiev   = ?,",
        "diapro   = ?,", "fecpcl   = ?,", "usrpcl   = ?,", "horpcl   = ?,", 
        "numeta   = ?,", "fpagas   = ?,", "mpagno   = ?,", "observ   = ?,", 
        "coment   = ?,", "fecdfi   = ?,", "usrdfi   = ?,", "hordfi   = ?,",
        "codgru   = ? ",
 " WHERE numexp   = ? " 

 PREPARE st_modificar FROM strSql
END FUNCTION 

FUNCTION modifica(nofase smallint)
 --CALL encabezado("Modificar")
   
 IF captura_datos('M',nofase) THEN
    RETURN actualizar(1)
 ELSE 
    RETURN FALSE 
 END IF 
END FUNCTION 

FUNCTION actualizar(mensaje SMALLINT)
 TRY
  EXECUTE st_modificar 
  USING   g_reg.codpro, g_reg.numcre, g_reg.tipdoc, g_reg.city_num, 
          g_reg.numage, g_reg.numres, g_reg.fecres, g_reg.codasn,
          g_reg.fecasi, g_reg.nomcli, g_reg.valcre, g_reg.codabo,
          g_reg.numesc, g_reg.fecesc, g_reg.hojini, g_reg.hojfin,
          g_reg.hojin2, g_reg.hojfi2,
          g_reg.todipt, g_reg.todies, g_reg.fecini, g_reg.usrini, 
          g_reg.horini, g_reg.feesct, g_reg.usresc, g_reg.horesc, 
          g_reg.nofase, g_reg.estado, g_reg.vtcoba, g_reg.vtocob, 
          g_reg.fecdsg, g_reg.usrdsg, g_reg.hordsg, 
          g_reg.fevfir, g_reg.frefir, g_reg.tdifir, g_reg.finrpg, 
          g_reg.fegrpg, g_reg.nrerpg, g_reg.ndbrpg, g_reg.toparp, 
          g_reg.topaco, g_reg.codrgo, g_reg.codarp, g_reg.todifo, 
          g_reg.fecfor, g_reg.usrfor, g_reg.horfor, g_reg.canrch,
          g_reg.totrch, g_reg.ferech, g_reg.fecrei, g_reg.nrecrg,
          g_reg.ndocrg, g_reg.fecdev, g_reg.todidv, g_reg.todich,
          g_reg.fecrch, g_reg.usrrch, g_reg.horrch, g_reg.totpag,
          g_reg.numbol, g_reg.fecbol, g_reg.numdep, g_reg.fecdap,
          g_reg.usrdap, g_reg.hordap, g_reg.fecdeb, g_reg.nuboho,  
          g_reg.modeho, g_reg.nuboga, g_reg.modega, g_reg.nufade,
          g_reg.fecest, g_reg.usrest, g_reg.horest, g_reg.lugenv, 
          g_reg.fecenv, g_reg.fecrep, g_reg.nucoev, g_reg.todiev,
          g_reg.diapro, g_reg.fecpcl, g_reg.usrpcl, g_reg.horpcl, 
          g_reg.numeta, g_reg.fpagas, g_reg.mpagno, g_reg.observ,
          g_reg.coment, g_reg.fecdfi, g_reg.usrdfi, g_reg.hordfi, 
          g_reg.codgru, g_reg.numexp
          
 CATCH 
  CALL msgError(sqlca.sqlcode,"Modificar Registro.")
  RETURN FALSE 
 END TRY

 -- Verificando si se despliega el mensaje 
 IF mensaje THEN 
    CALL msg("Registro actualizado.")
 END IF 

 RETURN TRUE 
END FUNCTION 

FUNCTION delete_init()
 DEFINE strSql STRING 

 LET strSql =
  "DELETE FROM notariado ",
  "WHERE numexp = ? "
      
 PREPARE st_delete FROM strSql
END FUNCTION 

FUNCTION eliminar()
 IF NOT box_confirma("Esta seguro de eliminar el registro") THEN
    RETURN FALSE
 END IF 

 TRY 
  EXECUTE st_delete USING g_reg.numexp
  
 CATCH 
  CALL msgError(sqlca.sqlcode,"Eliminar Registro.")
  RETURN FALSE 
 END TRY
 CALL msg("Registro eliminado.")
 RETURN TRUE 
END FUNCTION 
