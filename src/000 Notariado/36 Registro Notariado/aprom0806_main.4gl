-- Mantenimiento de Registro Notariado

IMPORT FGL fgl_excel

GLOBALS "aprom0806_glob.4gl"
DEFINE result    INTEGER
DEFINE SQL       STRING 
 
MAIN
 DEFINE n_param 	SMALLINT,
	prog_name2 	STRING
		
 DEFER INTERRUPT

 OPTIONS INPUT  WRAP,
		 HELP KEY CONTROL-W,
		 COMMENT LINE OFF,
		 PROMPT LINE LAST - 2,
		 MESSAGE LINE LAST - 1,
		 ERROR LINE LAST

 LET n_param = num_args()

 CONNECT TO "aprojusa"

 LET prog_name2 = prog_name||".log"   
 CALL STARTLOG(prog_name2)
 CALL main_init()
END MAIN

FUNCTION main_init()
 DEFINE nom_forma   STRING,
        w           ui.WINDOW,
        f           ui.FORM
	  
 INITIALIZE u_reg.* TO NULL
 LET g_reg.* = u_reg.*

 CALL ui.Interface.loadActionDefaults("actiondefaults")
 CALL ui.Interface.loadStyles("styles_is")
 LET nom_forma = prog_name CLIPPED, "_form"
 CLOSE WINDOW SCREEN 

 OPEN WINDOW w1 WITH FORM nom_forma

 LET w = ui.WINDOW.getcurrent()
 LET f = w.getForm()

 -- Obteniendo usuario
 LET usuario = ARG_VAL(1) 

 -- Obteniendo grupo del usuario 
 LET GrupoUsuario = grpusu(usuario)

 -- Obteniendo sede del usuario primaria y alterna
 (SELECT a.city_num,a.city_num_alt INTO SedeUsuario,SedeAlterna FROM users a 
  WHERE a.user_id = usuario)

 -- Verificando que usuario tenga sede registrada 
 IF (SedeUsuario IS NULL) THEN
    CALL msg("Usuario sin sede registrada.\nDebe tener sede para acceder a la opcion de notariado.")
 END IF

 -- Verificando si hay sede alterna sino asigna cero 
 IF (SedeAlterna IS NULL) THEN
    LET SedeAlterna = 0
 END IF
  
 CALL fgl_settitle("ANDE - "||titulo1 CLIPPED||" - "||usuario||" - "||GrupoUsuario)

 -- Creando tabla temporal
 CREATE TEMP TABLE tmp_expedientes
 --CREATE TABLE tmp_expedientes
  (numexp INTEGER) 

 -- Iniializando datos 
 CALL insert_init()
 CALL update_init()
 CALL delete_init()
 CALL combo_init()
 CALL main_menu()

 -- Dropeando tabla temporal
 DROP TABLE tmp_expedientes 
END FUNCTION                                                                    

FUNCTION main_menu()                               
 DEFINE  cuantos,id,ids,i SMALLINT,
         cnt,resultado 	  SMALLINT,
         f                ui.FORM

 -- Inicializando contador
 LET cnt = 1
 DISPLAY UPSHIFT(titulo1) TO gtit_enc
  
 -- Asignando datos de informe a excel
 LET filebase = "ExcelNotariadoBase.xlsx"
 LET filename = "ExcelNotariado.xlsx"

 -- Desplegando datos
 DISPLAY ARRAY reg_det TO sDet.*
  ATTRIBUTE(ACCEPT=FALSE,CANCEL=FALSE,UNBUFFERED)
   
  BEFORE DISPLAY
   -- Seleccionando datos 
   LET cuantos = consulta(FALSE)
   IF cuantos > 0 THEN 
     CALL dialog.setCurrentRow("sdet",1)
     -- Desplegando datos
     CALL desplegar_datos(reg_det[1].fnumexp)
     --CALL setAttr(1)
     --CALL DIALOG.setCellAttributes(reg_det_attr)
   END IF 
   CALL encabezado(cuantos)
         
  BEFORE ROW 
   -- Despleando datos 
   LET id = arr_curr()
   IF id > 0 THEN 
      -- Desplegando datos
      CALL desplegar_datos(reg_det[id].fnumexp)
      --CALL setAttr(id)
      --CALL DIALOG.setCellAttributes(reg_det_attr)
   END IF 
     
  ON ACTION buscar
   -- Buscar datos 
   LET cuantos = consulta(TRUE)
   IF cuantos > 0 THEN 
      -- Desplegando datos
      CALL desplegar_datos(reg_det[id].fnumexp)
   END IF 
   CALL encabezado(cuantos)

  ON ACTION agregar
   -- Agregar registro
   IF ingreso(1) THEN 
      LET cuantos = consulta(FALSE)
      CALL fgl_set_arr_curr( arr_count() + 1 )

      --Refrescar Pantalla
      DISPLAY ARRAY reg_det TO sDet.*
       BEFORE DISPLAY  EXIT DISPLAY 
      END DISPLAY 
   END IF
   CALL encabezado(cuantos)
   
  ON ACTION modificar
   -- Modificar registro
   LET id = arr_curr()
   LET ids = scr_line()
   IF id > 0 THEN 
    LET resultado = modifica(1)

    -- Desplegando datos
    CALL desplegar_datos(reg_det[id].fnumexp)
    LET reg_det[id].fcodpro = g_reg.codpro

    -- Obteniendo nombre del programa de notariado
    (SELECT a.nombre INTO reg_det[id].fnompro FROM programas_not a
      WHERE a.codpro = g_reg.codpro)
       
    LET reg_det[id].fnumcre = g_reg.numcre
    LET reg_det[id].ftipdoc = g_reg.tipdoc

    -- Obteniendo nombre del tipo de documento
    (SELECT a.nombre INTO reg_det[id].fnomdoc FROM tiposdocumento a
      WHERE a.tipdoc = g_reg.tipdoc)
     
    -- Obteniendo nombre de la sede               
    (SELECT a.city_name INTO reg_det[id].fnomsed FROM city a
      WHERE a.city_num = g_reg.city_num)
      
    LET reg_det[id].fnumres = g_reg.numres
    LET reg_det[id].fnomcli = g_reg.nomcli
    LET reg_det[id].fvalcre = g_reg.valcre

    -- Obteniendo nombre de la etapa
    (SELECT a.nometa INTO reg_det[id].fnometa FROM etapas_not a
      WHERE a.numeta = g_reg.numeta)
     
    DISPLAY reg_det[id].* TO sDet[ids].*
   END IF 
   
 { ON ACTION eliminar
   -- Eliminar registro
   LET id = arr_curr()
   LET ids = scr_line()         
   IF id > 0 THEN 
      IF eliminar() THEN
         CALL DIALOG.deleteRow("sdet", id)
         IF id = arr_count() THEN 
            LET id = id - 1
         END IF 
         IF id > 0 THEN 
            -- Desplegando datos
            CALL desplegar_datos(reg_det[id].fnumexp)
         ELSE 
            INITIALIZE g_reg.* TO NULL
         END IF 
         DISPLAY BY NAME g_reg.*
      END IF   
   END IF }

  ON ACTION escrituras ATTRIBUTE (TEXT="Escrituracion")
   -- Registrar datos de escrituracion
   LET id = arr_curr()
   LET ids = scr_line()
   IF id > 0 THEN 
    IF modifica(2) THEN
      -- Desplegando datos
      CALL desplegar_datos(reg_det[id].fnumexp)
    END IF

    -- Regresando a datos generales
    LET f = DIALOG.getForm()
    CALL f.ensureFieldVisible("notariado.numexp")
   END IF 

  ON ACTION desglose ATTRIBUTE (TEXT="Desglose")
   -- Registrar desglose de cobro
   LET id = arr_curr()
   LET ids = scr_line()
   IF id > 0 THEN 
    -- Registrando desglose 
    CALL CapturaDesglose(1) 

    -- Desplegando datos
    CALL desplegar_datos(reg_det[id].fnumexp)

    -- Regresando a datos generales
    LET f = DIALOG.getForm()
    CALL f.ensureFieldVisible("notariado.numexp")
   END IF 

  ON ACTION proformal ATTRIBUTE (TEXT="Formalizacion")
   -- Registrar proceso de formalizacion
   LET id = arr_curr()
   LET ids = scr_line()
   IF id > 0 THEN 
    IF modifica(4) THEN
      -- Desplegando datos
      CALL desplegar_datos(reg_det[id].fnumexp)
    END IF

     -- Regresando a datos generales
    LET f = DIALOG.getForm()
    CALL f.ensureFieldVisible("notariado.numexp")
   END IF 

  ON ACTION reingreso ATTRIBUTE (TEXT="Reingreso RGP")
   -- Registrar datos de reingreso
   LET id = arr_curr()
   LET ids = scr_line()
   IF id > 0 THEN 
    IF modifica(5) THEN
      -- Desplegando datos
      CALL desplegar_datos(reg_det[id].fnumexp)
    END IF

     -- Regresando a datos generales
    LET f = DIALOG.getForm()
    CALL f.ensureFieldVisible("notariado.numexp")
   END IF 

  ON ACTION datospago ATTRIBUTE (TEXT="Datos-Pago")
   -- Registrar datos de pago 
   LET id = arr_curr()
   LET ids = scr_line()
   IF id > 0 THEN 
    IF modifica(6) THEN
      -- Desplegando datos
      CALL desplegar_datos(reg_det[id].fnumexp)
    END IF

     -- Regresando a datos generales
    LET f = DIALOG.getForm()
    CALL f.ensureFieldVisible("notariado.numexp")
   END IF 

  ON ACTION estadocta ATTRIBUTE (TEXT="Estado-Cuenta")
   -- Registrar datos estado de cuenta
   LET id = arr_curr()
   LET ids = scr_line()
   IF id > 0 THEN 
    IF modifica(7) THEN
      -- Desplegando datos
      CALL desplegar_datos(reg_det[id].fnumexp)
    END IF

     -- Regresando a datos generales
    LET f = DIALOG.getForm()
    CALL f.ensureFieldVisible("notariado.numexp")
   END IF 

  ON ACTION enviopcl ATTRIBUTE (TEXT="Envio-PCL")
   -- Registrar datos envio pcl
   LET id = arr_curr()
   LET ids = scr_line()
   IF id > 0 THEN 
    IF modifica(8) THEN
      -- Desplegando datos
      CALL desplegar_datos(reg_det[id].fnumexp)
    END IF

     -- Regresando a datos generales
    LET f = DIALOG.getForm()
    CALL f.ensureFieldVisible("notariado.numexp")
   END IF 

  ON ACTION finales ATTRIBUTE (TEXT="Finales")
   -- Registrar datos envio pcl
   LET id = arr_curr()
   LET ids = scr_line()
   IF id > 0 THEN 
    IF modifica(9) THEN
      -- Desplegando datos
      CALL desplegar_datos(reg_det[id].fnumexp)

      -- Obteniendo nombre de la etapa
      (SELECT a.nometa INTO reg_det[id].fnometa FROM etapas_not a
        WHERE a.numeta = g_reg.numeta)
      
      DISPLAY reg_det[id].fnometa TO sDet[ids].fnometa
    END IF

     -- Regresando a datos generales
    LET f = DIALOG.getForm()
    CALL f.ensureFieldVisible("notariado.numexp")
   END IF 
   
  ON ACTION excel ATTRIBUTE (TEXT="Excel")
   -- Borrando registros
   DELETE FROM tmp_expedientes

   -- Llenando table temporal
   FOR i = 1 TO cuantos
    INSERT INTO tmp_expedientes
    VALUES (reg_det[i].fnumexp)
   END FOR 

   -- Exportando a excel
   LET sql = prepsql()
   IF sql_to_excel(sql) THEN
      CALL fgl_putfile(filename,filename)
      CALL ui.Interface.frontCall("standard","shellExec", filename, result)
   ELSE
      ERROR "Something went wrong"
   END IF 
   
   

  ON ACTION salir
   -- Salida 
   EXIT DISPLAY 
 END DISPLAY 
END FUNCTION

FUNCTION Desplegar_Datos(wnumexp INT)
 DEFINE nomdocdes LIKE tiposdocumento.nombre 

 -- Seleccionando datos maestro notariado 
 SELECT a.numexp,   a.codpro,
        a.numcre,   a.tipdoc,
        a.city_num, a.numage,
        a.numres,   a.fecres, 
        a.codasn,   a.fecasi, 
        a.nomcli,   a.valcre,
        a.codabo,   a.numesc,
        a.fecesc,   a.hojini,
        a.hojfin,   a.hojin2,
        a.hojfi2,   a.todipt,
        a.todies,
        a.fecini,   a.usrini,
        a.horini,   a.feesct, 
        a.usresc,   a.horesc,
        a.nofase,   a.estado,
        a.vtcoba,   
        a.vtocob,   a.fecdsg,
        a.usrdsg,   a.hordsg,
        a.fevfir,   a.frefir,   
        a.tdifir,   a.finrpg,   
        a.fegrpg,   a.nrerpg,   
        a.ndbrpg,   a.toparp,
        a.topaco,   a.codrgo,
        a.codarp,   a.todifo,
        a.fecfor,   a.usrfor,
        a.horfor,   a.canrch,
        a.totrch,   a.ferech,
        a.fecrei,   a.nrecrg,
        a.ndocrg,   a.fecdev,
        a.todidv,   a.todich,
        a.fecrch,   a.usrrch,
        a.horrch,   a.totpag,
        a.numbol,   a.fecbol,
        a.numdep,   a.fecdap,
        a.usrdap,   a.hordap, 
        a.fecdeb,   a.nuboho,  
        a.modeho,   a.nuboga,
        a.modega,   a.nufade,
        a.fecest,   a.usrest,
        a.horest,   a.lugenv,
        a.fecenv,   a.fecrep,
        a.nucoev,   a.todiev,
        a.diapro,   a.fecpcl,
        a.usrpcl,   a.horpcl,
        a.numeta,   a.fpagas, 
        a.mpagno,   a.observ,
        a.coment,   a.fecdfi,   
        a.usrdfi,   a.hordfi,
        a.codgru 
  INTO  g_reg.* 
  FROM  notariado a
  WHERE a.numexp = wnumexp
  IF (status!=NOTFOUND) THEN
      -- Obteniendo nombre del tipo de documento
      (SELECT a.nombre INTO nomdocdes FROM tiposdocumento a
        WHERE a.tipdoc = g_reg.tipdoc)

      DISPLAY BY NAME g_reg.*
      DISPLAY g_reg.valcre TO valcredes 
      DISPLAY BY NAME nomdocdes
  END IF 

 -- Desplegando datos del desglose  
 CALL DesplegarDatosDesglose() 
END FUNCTION

FUNCTION DesplegarDatosDesglose() 
 DEFINE g_tco cDet,
        x     SMALLINT 

 -- Seleccionado datos desglose de cobros 
 CALL v_cobros.CLEAR()
 DECLARE c_cobros CURSOR FOR
 SELECT a.tipcob, 
        a.nomcob,
        a.ingcan,
        a.sumsub,
        a.mulcx1,
        a.caliva, 
        a.valsum,
        a.valres, 
        " ", 
        a.cancob,
        a.valcob,
        a.totcob,
        a.numcor 
  FROM  notariado_cob a
  WHERE a.numexp = g_reg.numexp 
  ORDER BY a.numcor

  LET x = 0
  FOREACH c_cobros INTO g_tco.*
   LET x = x+1 
   LET v_cobros[x].* = g_tco.*
  END FOREACH
  CLOSE c_cobros
  FREE  c_cobros

 -- Desplegando tipos de cobro 
 DISPLAY ARRAY v_cobros TO sCobros.*
  BEFORE DISPLAY 
   EXIT DISPLAY
 END DISPLAY
END FUNCTION 
 
FUNCTION encabezado(registros)
 DEFINE registros STRING 
 CALL fgl_settitle(
 "ANDE - "||titulo1 CLIPPED||" - "||
 usuario||" - "||GrupoUsuario||" - # Expedientes "||registros)
END FUNCTION 

{FUNCTION setAttr(id)
 DEFINE  i,id SMALLINT

 FOR i=1 TO reg_det.getLength()
  LET reg_det_attr[i].fnumexp = "black"
  LET reg_det_attr[i].fcodpro = "black"
  LET reg_det_attr[i].fnompro = "black"
  LET reg_det_attr[i].fnumcre = "black"
  LET reg_det_attr[i].ftipdoc = "black"
  LET reg_det_attr[i].fnomdoc = "black"
  LET reg_det_attr[i].fnumres = "black"
  LET reg_det_attr[i].fnomsed = "black"
  LET reg_det_attr[i].fnomcli = "black"
  LET reg_det_attr[i].fvalcre = "black"
  LET reg_det_attr[i].fnometa = "black"
 END FOR  

 LET reg_det_attr[id].fnumexp = "blue reverse"
 LET reg_det_attr[id].fcodpro = "blue reverse"
 LET reg_det_attr[id].fnompro = "blue reverse"
 LET reg_det_attr[id].fnumcre = "blue reverse"
 LET reg_det_attr[id].ftipdoc = "blue reverse"
 LET reg_det_attr[id].fnomdoc = "blue reverse"
 LET reg_det_attr[id].fnomsed = "blue reverse"
 LET reg_det_attr[id].fnumres = "blue reverse"
 LET reg_det_attr[id].fnomcli = "blue reverse"
 LET reg_det_attr[id].fvalcre = "blue reverse"
 LET reg_det_attr[id].fnometa = "blue reverse"
END FUNCTION }

FUNCTION combo_init()
 DEFINE wcondicionsede,wcondicionsede1,strcon STRING
 
 -- Cargando combobox
 CALL combo_din2("notariado.codpro","SELECT a.codpro, a.nombre FROM programas_not a ORDER BY a.codpro")
 CALL combo_din2("notariado.tipdoc","SELECT a.tipdoc, a.nombre FROM tiposdocumento a ORDER BY a.tipdoc")

 -- Verificando grupo de usuario para asignar sede 
 LET wcondicionsede = NULL
 IF GrupoUsuario!=AdmonNotariado THEN
    LET wcondicionsede  = "WHERE a.city_num IN (",SedeUsuario,",",SedeAlterna,")"
    LET wcondicionsede1 = " AND  a.city_num IN (",SedeUsuario,",",SedeAlterna,")"
 ELSE
 END IF 

 -- Sedes
 LET strcon = "SELECT a.city_num,a.city_name||' - '||",
              "a.city_country FROM city a ",
              wcondicionsede|| 
              " ORDER BY a.city_num"  

 CALL combo_din2("notariado.city_num",strcon)

 -- Agencias banco x sede
 LET strcon = "SELECT a.numage,a.numage||' - '||a.nombre||' - '||",
              "a.nomlug FROM agencias_bco a ",
              wcondicionsede||
              " ORDER BY a.numage"

 CALL combo_din2("notariado.numage",strcon) 

 -- Asistentes de notario x sede
 LET strcon = "SELECT a.codasn,a.nombre FROM asistentes_nt a ",
              --wcondicionsede||
              " ORDER BY a.nombre"

 CALL combo_din2("notariado.codasn",strcon)

 -- Notarios x sede
 LET strcon = "SELECT a.usuid,a.user_name FROM users a ",
              " WHERE a.user_mod_notar = '1' ",
              wcondicionsede1||
              " ORDER BY a.user_name"

 CALL combo_din2("notariado.codabo",strcon) 

 -- Registro que operan
 CALL combo_din2("notariado.codrgo","SELECT a.codrgo,a.nombre FROM registros_ope a ORDER BY a.codrgo")

 -- Asistentes registro 
 CALL combo_din2("notariado.codarp","SELECT a.codarp,a.nombre FROM asistentes_rp a ORDER BY a.codarp")

 -- Lugares de envio
 CALL combo_din2("notariado.lugenv","SELECT a.lugenv,a.nomlug FROM lugarenvio_not a ORDER BY a.lugenv")

 -- Etapas 
 CALL combo_din2("notariado.numeta","SELECT a.numeta,a.nometa FROM etapas_not a ORDER BY a.numeta")

END FUNCTION 



