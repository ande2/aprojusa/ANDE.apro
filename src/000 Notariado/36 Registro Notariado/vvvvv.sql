
SELECT
  a.nomdoc,a.numexp,a.codpro,a.numcre,a.nomreg,a.numres,a.fecres,
  a.numage,a.nomage,a.nomasi,a.fecasi,a.nomnot,a.numesc,a.fecesc,
  a.hojini,a.hojfin,a.todipt,a.todies,a.nomcli,a.nombre,a.valcre,

  a.honora,a.totiva,a.timnot,a.prites,a.tesesp,a.ivacom,a.honbas,
  a.lugfin,a.valfin,a.honexe,a.redhon,a.canfin,a.finext,a.gastot,
  a.gasmun,a.difcob,a.toacob,a.canele,a.valele,a.gascer,a.hojpro,
  a.docext,a.totcob,a.tototr,

  a.fevfir,a.frefir,a.tdifir,a.finrpg,a.fegrpg,a.nrerpg,a.ndbrpg,
  a.toparp,a.topaco,a.codrgo,a.codarp,a.todifo,a.canrch,a.totrch,
  a.ferech,a.fecrei,a.nrecrg,a.ndocrg,a.fecdev,a.coment,a.todidv,
  a.todich,a.totpag,a.numbol,a.fecbol,a.numdep,a.fecdeb,a.nuboho,
  a.modeho,a.nuboga,a.modega,a.nufade,a.lugenv,a.fecenv,a.fecrep,
  a.nucoev,a.todiev,a.diapro,a.numeta,a.fpagas,a.mpagno,a.observ

  FROM  visexcelnotariado a
