-- Ingresando registro notariado

GLOBALS "aprom0806_glob.4gl"
DEFINE totiva DEC(10,2) 

FUNCTION captura_datos(operacion,nofase)
 DEFINE operacion CHAR (1)
 DEFINE resultado BOOLEAN
 DEFINE nofase    SMALLINT

 LET resultado = FALSE 
 LET u_reg.* = g_reg.*
 IF operacion = 'I' THEN 
    INITIALIZE g_reg.* TO NULL
    DISPLAY BY NAME g_reg.*
 END IF

 -- Ingresando datos 
 DIALOG ATTRIBUTES(UNBUFFERED)
   INPUT BY NAME g_reg.numexp,   g_reg.codpro,
                 g_reg.numcre,   g_reg.tipdoc,
                 g_reg.city_num, g_reg.numage,
                 g_reg.numres,   g_reg.fecres, 
                 g_reg.codasn,   g_reg.fecasi, 
                 g_reg.nomcli,   g_reg.valcre,
                 g_reg.codabo,   g_reg.numesc,
                 g_reg.fecesc,   g_reg.hojini, 
                 g_reg.hojfin,   g_reg.hojin2,  
                 g_reg.hojfi2,   g_reg.todies,
                 g_reg.todipt, 
                 g_reg.fevfir,   g_reg.frefir,   
                 g_reg.tdifir,   g_reg.finrpg,  
                 g_reg.fegrpg,   g_reg.nrerpg,   
                 g_reg.ndbrpg,   g_reg.toparp,   
                 g_reg.topaco,   g_reg.codrgo,
                 g_reg.codarp,   g_reg.todifo,
                 g_reg.canrch,   g_reg.totrch,
                 g_reg.ferech,   g_reg.fecrei,
                 g_reg.nrecrg,   g_reg.ndocrg,
                 g_reg.fecdev,   g_reg.todidv, 
                 g_reg.todich,   g_reg.totpag,
                 g_reg.numbol,   g_reg.fecbol,
                 g_reg.numdep,   g_reg.fecdeb,
                 g_reg.nuboho,   g_reg.modeho,
                 g_reg.nuboga,   g_reg.modega,
                 g_reg.nufade,   g_reg.lugenv,
                 g_reg.fecenv,   g_reg.fecrep,
                 g_reg.nucoev,   g_reg.todiev,
                 g_reg.diapro,   g_reg.numeta,
                 g_reg.fpagas,   g_reg.mpagno,
                 g_reg.observ,   g_reg.coment 
    ATTRIBUTES (WITHOUT DEFAULTS)

    BEFORE INPUT
     CALL DIALOG.setActionHidden("close",TRUE)

     -- Verificando grupo de usuario 
     IF GrupoUsuario!=AdmonNotariado THEN
        IF (operacion="I") THEN
           LET g_reg.city_num = SedeUsuario
        END IF 
        CALL Dialog.setFieldActive("city_num",0)
     ELSE
        CALL Dialog.setFieldActive("city_num",1)
     END IF

     -- Verificando fase 
     IF nofase=1 THEN -- Datos iniciales
        -- Deshabilita datos escrituracion 
        CALL Dialog.SetFieldActive("codabo",0)
        CALL Dialog.SetFieldActive("numesc",0)
        CALL Dialog.SetFieldActive("fecesc",0)
        CALL Dialog.SetFieldActive("hojini",0)
        CALL Dialog.SetFieldActive("hojfin",0)
        CALL Dialog.SetFieldActive("hojin2",0)
        CALL Dialog.SetFieldActive("hojfi2",0)
        CALL Dialog.SetFieldActive("todipt",0)
        CALL Dialog.SetFieldActive("todies",0)
        -- Deshabilita datos formalizacion  
        CALL Dialog.SetFieldActive("fevfir",0)
        CALL Dialog.SetFieldActive("frefir",0)
        CALL Dialog.SetFieldActive("tdifir",0)
        CALL Dialog.SetFieldActive("finrpg",0)
        CALL Dialog.SetFieldActive("fegrpg",0)
        CALL Dialog.SetFieldActive("nrerpg",0)
        CALL Dialog.SetFieldActive("ndbrpg",0)
        CALL Dialog.SetFieldActive("toparp",0)
        CALL Dialog.SetFieldActive("topaco",0)
        CALL Dialog.SetFieldActive("codrgo",0)
        CALL Dialog.SetFieldActive("codarp",0)
        CALL Dialog.SetFieldActive("todifo",0)
        -- Desabilita datos reingreso rpg 
        CALL Dialog.SetFieldActive("canrch",0)
        CALL Dialog.SetFieldActive("totrch",0)
        CALL Dialog.SetFieldActive("ferech",0)
        CALL Dialog.SetFieldActive("fecrei",0)
        CALL Dialog.SetFieldActive("nrecrg",0)
        CALL Dialog.SetFieldActive("ndocrg",0)
        CALL Dialog.SetFieldActive("fecdev",0)
        CALL Dialog.SetFieldActive("todidv",0)
        CALL Dialog.SetFieldActive("todich",0)
        -- Desabilita datos de pago 
        CALL Dialog.SetFieldActive("totpag",0)
        CALL Dialog.SetFieldActive("numbol",0)
        CALL Dialog.SetFieldActive("fecbol",0)
        CALL Dialog.SetFieldActive("numdep",0)
        -- Deshabilitado datos estado cuenta 
        CALL Dialog.SetFieldActive("fecdeb",0)
        CALL Dialog.SetFieldActive("nuboho",0)
        CALL Dialog.SetFieldActive("modeho",0)
        CALL Dialog.SetFieldActive("nuboga",0)
        CALL Dialog.SetFieldActive("modega",0)
        CALL Dialog.SetFieldActive("nufade",0)
        -- Deshabilitado datos envio pcl 
        CALL Dialog.SetFieldActive("lugenv",0)
        CALL Dialog.SetFieldActive("fecenv",0)
        CALL Dialog.SetFieldActive("fecrep",0)
        CALL Dialog.SetFieldActive("nucoev",0)
        CALL Dialog.SetFieldActive("todiev",0)
        CALL Dialog.SetFieldActive("diapro",0)
        -- Deshabilitado datos finales
        CALL Dialog.SetFieldActive("numeta",0)
        CALL Dialog.SetFieldActive("fpagas",0)
        CALL Dialog.SetFieldActive("mpagno",0)
        CALL Dialog.SetFieldActive("observ",0)
        CALL Dialog.SetFieldActive("coment",0)
     END IF 
     
     IF nofase=2 THEN -- Datos escrituracion
        -- Deshabilita datos iniciales  
        CALL Dialog.SetFieldActive("numexp",0)
        CALL Dialog.SetFieldActive("codpro",0)
        CALL Dialog.SetFieldActive("numcre",0)
        CALL Dialog.SetFieldActive("tipdoc",0)
        CALL Dialog.SetFieldActive("city_num",0)
        CALL Dialog.SetFieldActive("numage",0)
        CALL Dialog.SetFieldActive("numres",0)
        CALL Dialog.SetFieldActive("fecres",0)
        CALL Dialog.SetFieldActive("codasn",0)
        CALL Dialog.SetFieldActive("fecasi",0)
        CALL Dialog.SetFieldActive("nomcli",0)
        CALL Dialog.SetFieldActive("valcre",0)
        -- Deshabilita datos formalizacion  
        CALL Dialog.SetFieldActive("fevfir",0)
        CALL Dialog.SetFieldActive("frefir",0)
        CALL Dialog.SetFieldActive("tdifir",0)
        CALL Dialog.SetFieldActive("finrpg",0)
        CALL Dialog.SetFieldActive("fegrpg",0)
        CALL Dialog.SetFieldActive("nrerpg",0)
        CALL Dialog.SetFieldActive("ndbrpg",0)
        CALL Dialog.SetFieldActive("toparp",0)
        CALL Dialog.SetFieldActive("topaco",0)
        CALL Dialog.SetFieldActive("codrgo",0)
        CALL Dialog.SetFieldActive("codarp",0)
        CALL Dialog.SetFieldActive("todifo",0) 
        -- Desabilita datos reingreso rpg 
        CALL Dialog.SetFieldActive("canrch",0)
        CALL Dialog.SetFieldActive("totrch",0)
        CALL Dialog.SetFieldActive("ferech",0)
        CALL Dialog.SetFieldActive("fecrei",0)
        CALL Dialog.SetFieldActive("nrecrg",0)
        CALL Dialog.SetFieldActive("ndocrg",0)
        CALL Dialog.SetFieldActive("fecdev",0)
        CALL Dialog.SetFieldActive("todidv",0)
        CALL Dialog.SetFieldActive("todich",0)
        -- Desabilita datos de pago 
        CALL Dialog.SetFieldActive("totpag",0)
        CALL Dialog.SetFieldActive("numbol",0)
        CALL Dialog.SetFieldActive("fecbol",0)
        CALL Dialog.SetFieldActive("numdep",0)
        -- Deshabilitado datos estado cuenta 
        CALL Dialog.SetFieldActive("fecdeb",0)
        CALL Dialog.SetFieldActive("nuboho",0)
        CALL Dialog.SetFieldActive("modeho",0)
        CALL Dialog.SetFieldActive("nuboga",0)
        CALL Dialog.SetFieldActive("modega",0)
        CALL Dialog.SetFieldActive("nufade",0)
        -- Deshabilitado datos envio pcl 
        CALL Dialog.SetFieldActive("lugenv",0)
        CALL Dialog.SetFieldActive("fecenv",0)
        CALL Dialog.SetFieldActive("fecrep",0)
        CALL Dialog.SetFieldActive("nucoev",0)
        CALL Dialog.SetFieldActive("todiev",0)
        CALL Dialog.SetFieldActive("diapro",0)
        -- Deshabilitado datos finales
        CALL Dialog.SetFieldActive("numeta",0)
        CALL Dialog.SetFieldActive("fpagas",0)
        CALL Dialog.SetFieldActive("mpagno",0)
        CALL Dialog.SetFieldActive("observ",0)
        CALL Dialog.SetFieldActive("coment",0)
     END IF 

     IF nofase=3 THEN -- Desglose cobro         
        -- Deshabilita datos iniciales  
        CALL Dialog.SetFieldActive("numexp",0)
        CALL Dialog.SetFieldActive("codpro",0)
        CALL Dialog.SetFieldActive("numcre",0)
        CALL Dialog.SetFieldActive("tipdoc",0)
        CALL Dialog.SetFieldActive("city_num",0)
        CALL Dialog.SetFieldActive("numage",0)
        CALL Dialog.SetFieldActive("numres",0)
        CALL Dialog.SetFieldActive("fecres",0)
        CALL Dialog.SetFieldActive("codasn",0)
        CALL Dialog.SetFieldActive("fecasi",0)
        CALL Dialog.SetFieldActive("nomcli",0)
        CALL Dialog.SetFieldActive("valcre",0)
        -- Deshabilita datos escrituracion 
        CALL Dialog.SetFieldActive("codabo",0)
        CALL Dialog.SetFieldActive("numesc",0)
        CALL Dialog.SetFieldActive("fecesc",0)
        CALL Dialog.SetFieldActive("hojini",0)
        CALL Dialog.SetFieldActive("hojfin",0)
        CALL Dialog.SetFieldActive("hojin2",0)
        CALL Dialog.SetFieldActive("hojfi2",0)
        CALL Dialog.SetFieldActive("todies",0)
        -- Deshabilita datos formalizacion  
        CALL Dialog.SetFieldActive("fevfir",0)
        CALL Dialog.SetFieldActive("frefir",0)
        CALL Dialog.SetFieldActive("tdifir",0)
        CALL Dialog.SetFieldActive("finrpg",0)
        CALL Dialog.SetFieldActive("fegrpg",0)
        CALL Dialog.SetFieldActive("nrerpg",0)
        CALL Dialog.SetFieldActive("ndbrpg",0)
        CALL Dialog.SetFieldActive("toparp",0)
        CALL Dialog.SetFieldActive("topaco",0)
        CALL Dialog.SetFieldActive("codrgo",0)
        CALL Dialog.SetFieldActive("codarp",0)
        CALL Dialog.SetFieldActive("todifo",0)
        -- Desabilita datos reingreso rpg 
        CALL Dialog.SetFieldActive("canrch",0)
        CALL Dialog.SetFieldActive("totrch",0)
        CALL Dialog.SetFieldActive("ferech",0)
        CALL Dialog.SetFieldActive("fecrei",0)
        CALL Dialog.SetFieldActive("nrecrg",0)
        CALL Dialog.SetFieldActive("ndocrg",0)
        CALL Dialog.SetFieldActive("fecdev",0)
        CALL Dialog.SetFieldActive("todidv",0)
        CALL Dialog.SetFieldActive("todich",0)
        -- Desabilita datos de pago 
        CALL Dialog.SetFieldActive("totpag",0)
        CALL Dialog.SetFieldActive("numbol",0)
        CALL Dialog.SetFieldActive("fecbol",0)
        CALL Dialog.SetFieldActive("numdep",0)
        -- Deshabilitado datos estado cuenta 
        CALL Dialog.SetFieldActive("fecdeb",0)
        CALL Dialog.SetFieldActive("nuboho",0)
        CALL Dialog.SetFieldActive("modeho",0)
        CALL Dialog.SetFieldActive("nuboga",0)
        CALL Dialog.SetFieldActive("modega",0)
        CALL Dialog.SetFieldActive("nufade",0)
        -- Deshabilitado datos envio pcl 
        CALL Dialog.SetFieldActive("lugenv",0)
        CALL Dialog.SetFieldActive("fecenv",0)
        CALL Dialog.SetFieldActive("fecrep",0)
        CALL Dialog.SetFieldActive("nucoev",0)
        CALL Dialog.SetFieldActive("todiev",0)
        CALL Dialog.SetFieldActive("diapro",0)
        -- Deshabilitado datos finales
        CALL Dialog.SetFieldActive("numeta",0)
        CALL Dialog.SetFieldActive("fpagas",0)
        CALL Dialog.SetFieldActive("mpagno",0)
        CALL Dialog.SetFieldActive("observ",0)
        CALL Dialog.SetFieldActive("coment",0)

        NEXT FIELD tipcob 
     END IF 

     IF nofase=4 THEN -- Datos formalizacion 
        -- Deshabilita datos iniciales  
        CALL Dialog.SetFieldActive("numexp",0)
        CALL Dialog.SetFieldActive("codpro",0)
        CALL Dialog.SetFieldActive("numcre",0)
        CALL Dialog.SetFieldActive("tipdoc",0)
        CALL Dialog.SetFieldActive("city_num",0)
        CALL Dialog.SetFieldActive("numage",0)
        CALL Dialog.SetFieldActive("numres",0)
        CALL Dialog.SetFieldActive("fecres",0)
        CALL Dialog.SetFieldActive("codasn",0)
        CALL Dialog.SetFieldActive("fecasi",0)
        CALL Dialog.SetFieldActive("nomcli",0)
        CALL Dialog.SetFieldActive("valcre",0)
        -- Deshabilita datos escrituracion 
        CALL Dialog.SetFieldActive("codabo",0)
        CALL Dialog.SetFieldActive("numesc",0)
        CALL Dialog.SetFieldActive("fecesc",0)
        CALL Dialog.SetFieldActive("hojini",0)
        CALL Dialog.SetFieldActive("hojfin",0)
        CALL Dialog.SetFieldActive("hojin2",0)
        CALL Dialog.SetFieldActive("hojfi2",0)
        CALL Dialog.SetFieldActive("todies",0)
        -- Desabilita datos reingreso rpg 
        CALL Dialog.SetFieldActive("canrch",0)
        CALL Dialog.SetFieldActive("totrch",0)
        CALL Dialog.SetFieldActive("ferech",0)
        CALL Dialog.SetFieldActive("fecrei",0)
        CALL Dialog.SetFieldActive("nrecrg",0)
        CALL Dialog.SetFieldActive("ndocrg",0)
        CALL Dialog.SetFieldActive("fecdev",0)
        CALL Dialog.SetFieldActive("todidv",0)
        CALL Dialog.SetFieldActive("todich",0)
        -- Desabilita datos de pago 
        CALL Dialog.SetFieldActive("totpag",0)
        CALL Dialog.SetFieldActive("numbol",0)
        CALL Dialog.SetFieldActive("fecbol",0)
        CALL Dialog.SetFieldActive("numdep",0)
        -- Deshabilitado datos estado cuenta 
        CALL Dialog.SetFieldActive("fecdeb",0)
        CALL Dialog.SetFieldActive("nuboho",0)
        CALL Dialog.SetFieldActive("modeho",0)
        CALL Dialog.SetFieldActive("nuboga",0)
        CALL Dialog.SetFieldActive("modega",0)
        CALL Dialog.SetFieldActive("nufade",0)
        -- Deshabilitado datos envio pcl 
        CALL Dialog.SetFieldActive("lugenv",0)
        CALL Dialog.SetFieldActive("fecenv",0)
        CALL Dialog.SetFieldActive("fecrep",0)
        CALL Dialog.SetFieldActive("nucoev",0)
        CALL Dialog.SetFieldActive("todiev",0)
        CALL Dialog.SetFieldActive("diapro",0)
        -- Deshabilitado datos finales
        CALL Dialog.SetFieldActive("numeta",0)
        CALL Dialog.SetFieldActive("fpagas",0)
        CALL Dialog.SetFieldActive("mpagno",0)
        CALL Dialog.SetFieldActive("observ",0)
        CALL Dialog.SetFieldActive("coment",0)
     END IF 

     IF nofase=5 THEN -- Reingreso RPG       
        -- Deshabilita datos iniciales  
        CALL Dialog.SetFieldActive("numexp",0)
        CALL Dialog.SetFieldActive("codpro",0)
        CALL Dialog.SetFieldActive("numcre",0)
        CALL Dialog.SetFieldActive("tipdoc",0)
        CALL Dialog.SetFieldActive("city_num",0)
        CALL Dialog.SetFieldActive("numage",0)
        CALL Dialog.SetFieldActive("numres",0)
        CALL Dialog.SetFieldActive("fecres",0)
        CALL Dialog.SetFieldActive("codasn",0)
        CALL Dialog.SetFieldActive("fecasi",0)
        CALL Dialog.SetFieldActive("nomcli",0)
        CALL Dialog.SetFieldActive("valcre",0)
        -- Deshabilita datos escrituracion 
        CALL Dialog.SetFieldActive("codabo",0)
        CALL Dialog.SetFieldActive("numesc",0)
        CALL Dialog.SetFieldActive("fecesc",0)
        CALL Dialog.SetFieldActive("hojini",0)
        CALL Dialog.SetFieldActive("hojfin",0)
        CALL Dialog.SetFieldActive("hojin2",0)
        CALL Dialog.SetFieldActive("hojfi2",0)
        CALL Dialog.SetFieldActive("todies",0)
        -- Deshabilita datos formalizacion  
        CALL Dialog.SetFieldActive("fevfir",0)
        CALL Dialog.SetFieldActive("frefir",0)
        CALL Dialog.SetFieldActive("tdifir",0)
        CALL Dialog.SetFieldActive("finrpg",0)
        CALL Dialog.SetFieldActive("fegrpg",0)
        CALL Dialog.SetFieldActive("nrerpg",0)
        CALL Dialog.SetFieldActive("ndbrpg",0)
        CALL Dialog.SetFieldActive("toparp",0)
        CALL Dialog.SetFieldActive("topaco",0)
        CALL Dialog.SetFieldActive("codrgo",0)
        CALL Dialog.SetFieldActive("codarp",0)
        CALL Dialog.SetFieldActive("todifo",0)
        -- Desabilita datos de pago 
        CALL Dialog.SetFieldActive("totpag",0)
        CALL Dialog.SetFieldActive("numbol",0)
        CALL Dialog.SetFieldActive("fecbol",0)
        CALL Dialog.SetFieldActive("numdep",0)
        -- Deshabilitado datos estado cuenta 
        CALL Dialog.SetFieldActive("fecdeb",0)
        CALL Dialog.SetFieldActive("nuboho",0)
        CALL Dialog.SetFieldActive("modeho",0)
        CALL Dialog.SetFieldActive("nuboga",0)
        CALL Dialog.SetFieldActive("modega",0)
        CALL Dialog.SetFieldActive("nufade",0)
        -- Deshabilitado datos envio pcl 
        CALL Dialog.SetFieldActive("lugenv",0)
        CALL Dialog.SetFieldActive("fecenv",0)
        CALL Dialog.SetFieldActive("fecrep",0)
        CALL Dialog.SetFieldActive("nucoev",0)
        CALL Dialog.SetFieldActive("todiev",0)
        CALL Dialog.SetFieldActive("diapro",0)
        -- Deshabilitado datos finales
        CALL Dialog.SetFieldActive("numeta",0)
        CALL Dialog.SetFieldActive("fpagas",0)
        CALL Dialog.SetFieldActive("mpagno",0)
        CALL Dialog.SetFieldActive("observ",0)
        CALL Dialog.SetFieldActive("coment",0)
     END IF 

     IF nofase=6 THEN -- Datos de pago y facturacion
        -- Deshabilita datos iniciales  
        CALL Dialog.SetFieldActive("numexp",0)
        CALL Dialog.SetFieldActive("codpro",0)
        CALL Dialog.SetFieldActive("numcre",0)
        CALL Dialog.SetFieldActive("tipdoc",0)
        CALL Dialog.SetFieldActive("city_num",0)
        CALL Dialog.SetFieldActive("numage",0)
        CALL Dialog.SetFieldActive("numres",0)
        CALL Dialog.SetFieldActive("fecres",0)
        CALL Dialog.SetFieldActive("codasn",0)
        CALL Dialog.SetFieldActive("fecasi",0)
        CALL Dialog.SetFieldActive("nomcli",0)
        CALL Dialog.SetFieldActive("valcre",0)
        -- Deshabilita datos escrituracion 
        CALL Dialog.SetFieldActive("codabo",0)
        CALL Dialog.SetFieldActive("numesc",0)
        CALL Dialog.SetFieldActive("fecesc",0)
        CALL Dialog.SetFieldActive("hojini",0)
        CALL Dialog.SetFieldActive("hojfin",0)
        CALL Dialog.SetFieldActive("hojin2",0)
        CALL Dialog.SetFieldActive("hojfi2",0)
        CALL Dialog.SetFieldActive("todies",0)
        -- Deshabilita datos formalizacion  
        CALL Dialog.SetFieldActive("fevfir",0)
        CALL Dialog.SetFieldActive("frefir",0)
        CALL Dialog.SetFieldActive("tdifir",0)
        CALL Dialog.SetFieldActive("finrpg",0)
        CALL Dialog.SetFieldActive("fegrpg",0)
        CALL Dialog.SetFieldActive("nrerpg",0)
        CALL Dialog.SetFieldActive("ndbrpg",0)
        CALL Dialog.SetFieldActive("toparp",0)
        CALL Dialog.SetFieldActive("topaco",0)
        CALL Dialog.SetFieldActive("codrgo",0)
        CALL Dialog.SetFieldActive("codarp",0)
        CALL Dialog.SetFieldActive("todifo",0)
         -- Desabilita datos reingreso rpg 
        CALL Dialog.SetFieldActive("canrch",0)
        CALL Dialog.SetFieldActive("totrch",0)
        CALL Dialog.SetFieldActive("ferech",0)
        CALL Dialog.SetFieldActive("fecrei",0)
        CALL Dialog.SetFieldActive("nrecrg",0)
        CALL Dialog.SetFieldActive("ndocrg",0)
        CALL Dialog.SetFieldActive("fecdev",0)
        CALL Dialog.SetFieldActive("todidv",0)
        CALL Dialog.SetFieldActive("todich",0)
        -- Deshabilitado datos estado cuenta 
        CALL Dialog.SetFieldActive("fecdeb",0)
        CALL Dialog.SetFieldActive("nuboho",0)
        CALL Dialog.SetFieldActive("modeho",0)
        CALL Dialog.SetFieldActive("nuboga",0)
        CALL Dialog.SetFieldActive("modega",0)
        CALL Dialog.SetFieldActive("nufade",0)
        -- Deshabilitado datos envio pcl 
        CALL Dialog.SetFieldActive("lugenv",0)
        CALL Dialog.SetFieldActive("fecenv",0)
        CALL Dialog.SetFieldActive("fecrep",0)
        CALL Dialog.SetFieldActive("nucoev",0)
        CALL Dialog.SetFieldActive("todiev",0)
        CALL Dialog.SetFieldActive("diapro",0)
        -- Deshabilitado datos finales
        CALL Dialog.SetFieldActive("numeta",0)
        CALL Dialog.SetFieldActive("fpagas",0)
        CALL Dialog.SetFieldActive("mpagno",0)
        CALL Dialog.SetFieldActive("observ",0)
        CALL Dialog.SetFieldActive("coment",0)
     END IF 

     IF nofase=7 THEN -- Datos estado cuenta 
        -- Deshabilita datos iniciales  
        CALL Dialog.SetFieldActive("numexp",0)
        CALL Dialog.SetFieldActive("codpro",0)
        CALL Dialog.SetFieldActive("numcre",0)
        CALL Dialog.SetFieldActive("tipdoc",0)
        CALL Dialog.SetFieldActive("city_num",0)
        CALL Dialog.SetFieldActive("numage",0)
        CALL Dialog.SetFieldActive("numres",0)
        CALL Dialog.SetFieldActive("fecres",0)
        CALL Dialog.SetFieldActive("codasn",0)
        CALL Dialog.SetFieldActive("fecasi",0)
        CALL Dialog.SetFieldActive("nomcli",0)
        CALL Dialog.SetFieldActive("valcre",0)
        -- Deshabilita datos escrituracion 
        CALL Dialog.SetFieldActive("codabo",0)
        CALL Dialog.SetFieldActive("numesc",0)
        CALL Dialog.SetFieldActive("fecesc",0)
        CALL Dialog.SetFieldActive("hojini",0)
        CALL Dialog.SetFieldActive("hojfin",0)
        CALL Dialog.SetFieldActive("hojin2",0)
        CALL Dialog.SetFieldActive("hojfi2",0)
        CALL Dialog.SetFieldActive("todies",0)
        -- Deshabilita datos formalizacion  
        CALL Dialog.SetFieldActive("fevfir",0)
        CALL Dialog.SetFieldActive("frefir",0)
        CALL Dialog.SetFieldActive("tdifir",0)
        CALL Dialog.SetFieldActive("finrpg",0)
        CALL Dialog.SetFieldActive("fegrpg",0)
        CALL Dialog.SetFieldActive("nrerpg",0)
        CALL Dialog.SetFieldActive("ndbrpg",0)
        CALL Dialog.SetFieldActive("toparp",0)
        CALL Dialog.SetFieldActive("topaco",0)
        CALL Dialog.SetFieldActive("codrgo",0)
        CALL Dialog.SetFieldActive("codarp",0)
        CALL Dialog.SetFieldActive("todifo",0)
         -- Desabilita datos reingreso rpg 
        CALL Dialog.SetFieldActive("canrch",0)
        CALL Dialog.SetFieldActive("totrch",0)
        CALL Dialog.SetFieldActive("ferech",0)
        CALL Dialog.SetFieldActive("fecrei",0)
        CALL Dialog.SetFieldActive("nrecrg",0)
        CALL Dialog.SetFieldActive("ndocrg",0)
        CALL Dialog.SetFieldActive("fecdev",0)
        CALL Dialog.SetFieldActive("todidv",0)
        CALL Dialog.SetFieldActive("todich",0)
        -- Desabilita datos de pago 
        CALL Dialog.SetFieldActive("totpag",0)
        CALL Dialog.SetFieldActive("numbol",0)
        CALL Dialog.SetFieldActive("fecbol",0)
        CALL Dialog.SetFieldActive("numdep",0)
        -- Deshabilitado datos envio pcl 
        CALL Dialog.SetFieldActive("lugenv",0)
        CALL Dialog.SetFieldActive("fecenv",0)
        CALL Dialog.SetFieldActive("fecrep",0)
        CALL Dialog.SetFieldActive("nucoev",0)
        CALL Dialog.SetFieldActive("todiev",0)
        CALL Dialog.SetFieldActive("diapro",0)
        -- Deshabilitado datos finales
        CALL Dialog.SetFieldActive("numeta",0)
        CALL Dialog.SetFieldActive("fpagas",0)
        CALL Dialog.SetFieldActive("mpagno",0)
        CALL Dialog.SetFieldActive("observ",0)
        CALL Dialog.SetFieldActive("coment",0)
     END IF 

     IF nofase=8 THEN -- Datos envio pcl     
        -- Deshabilita datos iniciales  
        CALL Dialog.SetFieldActive("numexp",0)
        CALL Dialog.SetFieldActive("codpro",0)
        CALL Dialog.SetFieldActive("numcre",0)
        CALL Dialog.SetFieldActive("tipdoc",0)
        CALL Dialog.SetFieldActive("city_num",0)
        CALL Dialog.SetFieldActive("numage",0)
        CALL Dialog.SetFieldActive("numres",0)
        CALL Dialog.SetFieldActive("fecres",0)
        CALL Dialog.SetFieldActive("codasn",0)
        CALL Dialog.SetFieldActive("fecasi",0)
        CALL Dialog.SetFieldActive("nomcli",0)
        CALL Dialog.SetFieldActive("valcre",0)
        -- Deshabilita datos escrituracion 
        CALL Dialog.SetFieldActive("codabo",0)
        CALL Dialog.SetFieldActive("numesc",0)
        CALL Dialog.SetFieldActive("fecesc",0)
        CALL Dialog.SetFieldActive("hojini",0)
        CALL Dialog.SetFieldActive("hojfin",0)
        CALL Dialog.SetFieldActive("hojin2",0)
        CALL Dialog.SetFieldActive("hojfi2",0)
        CALL Dialog.SetFieldActive("todies",0)
        -- Deshabilita datos formalizacion  
        CALL Dialog.SetFieldActive("fevfir",0)
        CALL Dialog.SetFieldActive("frefir",0)
        CALL Dialog.SetFieldActive("tdifir",0)
        CALL Dialog.SetFieldActive("finrpg",0)
        CALL Dialog.SetFieldActive("fegrpg",0)
        CALL Dialog.SetFieldActive("nrerpg",0)
        CALL Dialog.SetFieldActive("ndbrpg",0)
        CALL Dialog.SetFieldActive("toparp",0)
        CALL Dialog.SetFieldActive("topaco",0)
        CALL Dialog.SetFieldActive("codrgo",0)
        CALL Dialog.SetFieldActive("codarp",0)
        CALL Dialog.SetFieldActive("todifo",0)
         -- Desabilita datos reingreso rpg 
        CALL Dialog.SetFieldActive("canrch",0)
        CALL Dialog.SetFieldActive("totrch",0)
        CALL Dialog.SetFieldActive("ferech",0)
        CALL Dialog.SetFieldActive("fecrei",0)
        CALL Dialog.SetFieldActive("nrecrg",0)
        CALL Dialog.SetFieldActive("ndocrg",0)
        CALL Dialog.SetFieldActive("fecdev",0)
        CALL Dialog.SetFieldActive("todidv",0)
        CALL Dialog.SetFieldActive("todich",0)
        -- Desabilita datos de pago 
        CALL Dialog.SetFieldActive("totpag",0)
        CALL Dialog.SetFieldActive("numbol",0)
        CALL Dialog.SetFieldActive("fecbol",0)
        CALL Dialog.SetFieldActive("numdep",0)
        -- Deshabilitado datos estado cuenta 
        CALL Dialog.SetFieldActive("fecdeb",0)
        CALL Dialog.SetFieldActive("nuboho",0)
        CALL Dialog.SetFieldActive("modeho",0)
        CALL Dialog.SetFieldActive("nuboga",0)
        CALL Dialog.SetFieldActive("modega",0)
        CALL Dialog.SetFieldActive("nufade",0)
        -- Deshabilitado datos finales
        CALL Dialog.SetFieldActive("numeta",0)
        CALL Dialog.SetFieldActive("fpagas",0)
        CALL Dialog.SetFieldActive("mpagno",0)
        CALL Dialog.SetFieldActive("observ",0)
        CALL Dialog.SetFieldActive("coment",0)
     END IF 

     IF nofase=9 THEN -- Datos finales 
        -- Deshabilita datos iniciales  
        CALL Dialog.SetFieldActive("numexp",0)
        CALL Dialog.SetFieldActive("codpro",0)
        CALL Dialog.SetFieldActive("numcre",0)
        CALL Dialog.SetFieldActive("tipdoc",0)
        CALL Dialog.SetFieldActive("city_num",0)
        CALL Dialog.SetFieldActive("numage",0)
        CALL Dialog.SetFieldActive("numres",0)
        CALL Dialog.SetFieldActive("fecres",0)
        CALL Dialog.SetFieldActive("codasn",0)
        CALL Dialog.SetFieldActive("fecasi",0)
        CALL Dialog.SetFieldActive("nomcli",0)
        CALL Dialog.SetFieldActive("valcre",0)
        -- Deshabilita datos escrituracion 
        CALL Dialog.SetFieldActive("codabo",0)
        CALL Dialog.SetFieldActive("numesc",0)
        CALL Dialog.SetFieldActive("fecesc",0)
        CALL Dialog.SetFieldActive("hojini",0)
        CALL Dialog.SetFieldActive("hojfin",0)
        CALL Dialog.SetFieldActive("hojin2",0)
        CALL Dialog.SetFieldActive("hojfi2",0)
        CALL Dialog.SetFieldActive("todies",0)
        -- Deshabilita datos formalizacion  
        CALL Dialog.SetFieldActive("fevfir",0)
        CALL Dialog.SetFieldActive("frefir",0)
        CALL Dialog.SetFieldActive("tdifir",0)
        CALL Dialog.SetFieldActive("finrpg",0)
        CALL Dialog.SetFieldActive("fegrpg",0)
        CALL Dialog.SetFieldActive("nrerpg",0)
        CALL Dialog.SetFieldActive("ndbrpg",0)
        CALL Dialog.SetFieldActive("toparp",0)
        CALL Dialog.SetFieldActive("topaco",0)
        CALL Dialog.SetFieldActive("codrgo",0)
        CALL Dialog.SetFieldActive("codarp",0)
        CALL Dialog.SetFieldActive("todifo",0)
         -- Desabilita datos reingreso rpg 
        CALL Dialog.SetFieldActive("canrch",0)
        CALL Dialog.SetFieldActive("totrch",0)
        CALL Dialog.SetFieldActive("ferech",0)
        CALL Dialog.SetFieldActive("fecrei",0)
        CALL Dialog.SetFieldActive("nrecrg",0)
        CALL Dialog.SetFieldActive("ndocrg",0)
        CALL Dialog.SetFieldActive("fecdev",0)
        CALL Dialog.SetFieldActive("todidv",0)
        CALL Dialog.SetFieldActive("todich",0)
        -- Desabilita datos de pago 
        CALL Dialog.SetFieldActive("totpag",0)
        CALL Dialog.SetFieldActive("numbol",0)
        CALL Dialog.SetFieldActive("fecbol",0)
        CALL Dialog.SetFieldActive("numdep",0)
        -- Deshabilitado datos estado cuenta 
        CALL Dialog.SetFieldActive("fecdeb",0)
        CALL Dialog.SetFieldActive("nuboho",0)
        CALL Dialog.SetFieldActive("modeho",0)
        CALL Dialog.SetFieldActive("nuboga",0)
        CALL Dialog.SetFieldActive("modega",0)
        CALL Dialog.SetFieldActive("nufade",0)
        -- Deshabilitado datos envio pcl 
        CALL Dialog.SetFieldActive("lugenv",0)
        CALL Dialog.SetFieldActive("fecenv",0)
        CALL Dialog.SetFieldActive("fecrep",0)
        CALL Dialog.SetFieldActive("nucoev",0)
        CALL Dialog.SetFieldActive("todiev",0)
        CALL Dialog.SetFieldActive("diapro",0)
     END IF 

    ON CHANGE city_num
     -- Cargando combobox agencias por sede
     -- Verificando grupo de usuario 
     IF GrupoUsuario=AdmonNotariado THEN
        CALL combo_din2("notariado.numage","SELECT a.numage,a.numage||' - '||a.nombre||' - '||a.nomlug FROM agencias_bco a WHERE a.city_num = "||g_reg.city_num||" ORDER BY a.numage")
     END IF

    ON CHANGE tipdoc
     -- Calculando datos
     CALL CalculaCobros(1,1,0) 

    ON CHANGE hojini
     -- Calculando datos
     CALL CalculaCobros(2,0,0)

    ON CHANGE hojfin
     -- Calculando datos
     CALL CalculaCobros(2,0,0)

    ON CHANGE hojin2
     -- Calculando datos
     CALL CalculaCobros(2,0,0)

    ON CHANGE hojfi2
     -- Calculando datos
     CALL CalculaCobros(2,0,0)

    ON CHANGE valcre 
     -- Calculando datos
     CALL CalculaCobros(1,1,0) 

    ON CHANGE fecesc
     -- Calculando datos
     CALL CalculaCobros(2,0,0)

    ON CHANGE fevfir
     -- Calculando datos
     CALL CalculaCobros(2,0,0)

    ON CHANGE frefir
     -- Calculando datos
     CALL CalculaCobros(2,0,0)

    ON CHANGE finrpg
     -- Calculando datos
     CALL CalculaCobros(2,0,0)

    ON CHANGE fegrpg
     -- Calculando datos
     CALL CalculaCobros(2,0,0)

    ON CHANGE toparp
     -- Calculando datos
     CALL CalculaCobros(2,0,0)

    ON CHANGE canrch
     -- Calculando datos
     CALL CalculaCobros(2,0,0)

    ON CHANGE ferech 
     -- Calculando datos
     CALL CalculaCobros(2,0,0)

    ON CHANGE fecrei
     -- Calculando datos
     CALL CalculaCobros(2,0,0)

    ON CHANGE fecdev
     -- Calculando datos
     CALL CalculaCobros(2,0,0)

    ON CHANGE fecenv
     -- Calculando datos
     CALL CalculaCobros(2,0,0)

    ON CHANGE fecrep
     -- Calculando datos
     CALL CalculaCobros(2,0,0)

   END INPUT 

   ON ACTION ACCEPT
    -- Fase datos iniciales
    IF (nofase=1) THEN 
     IF g_reg.codpro IS NULL THEN
       CALL msg("Ingrese programa.")
       NEXT FIELD codpro  
     END IF 
    
     IF g_reg.numcre IS NULL THEN
       CALL msg("Ingrese numero de programa.")
       NEXT FIELD numcre
     END IF 

     IF g_reg.tipdoc IS NULL THEN
       CALL msg("Ingrese tipo de documento.")
       NEXT FIELD tipdoc
     END IF 

     IF g_reg.city_num IS NULL THEN
       CALL msg("Ingrese sede.")
       NEXT FIELD city_num  
     END IF 

     IF g_reg.numage IS NULL THEN
       CALL msg("Ingrese numero de agencia.")
       NEXT FIELD numage  
     END IF  
    
     IF g_reg.numres IS NULL THEN
       CALL msg("Ingrese numero de resolucion.")
       NEXT FIELD numres  
     END IF

     IF g_reg.fecres IS NULL AND g_reg.codpro = 1 THEN
       CALL msg("Ingrese fecha de resolucion.")
       NEXT FIELD fecres 
     END IF  
    
     IF g_reg.fecres >TODAY THEN
       CALL msg("Fecha de resolucion no puede ser mayor a fecha del dia.")
       NEXT FIELD fecres 
     END IF  

     IF g_reg.codasn IS NULL THEN
       CALL msg("Ingrese asistente responsable.")
       NEXT FIELD codasn  
     END IF  

     IF g_reg.fecasi IS NULL THEN
       CALL msg("Ingrese fecha de asignacion.")
       NEXT FIELD fecasi  
     END IF  

     IF g_reg.fecasi >TODAY THEN
       CALL msg("Fecha de asignacion no puede ser mayor a fecha del dia.")
       NEXT FIELD fecasi 
     END IF  

     -- Validando fecha de asignacion
     IF g_reg.fecasi<g_reg.fecres THEN
       CALL msg("Fecha de asignacion menor a fecha de resolucion.")
       NEXT FIELD fecres
     END IF  
    
     IF g_reg.nomcli IS NULL THEN
       CALL msg("Ingrese nombre del cliente.")
       NEXT FIELD nomcli  
     END IF 

     IF g_reg.valcre IS NULL OR
       g_reg.valcre <0 THEN
       CALL msg("Valor del documento debe ser mayor o igual a cero.")
       NEXT FIELD valcre 
     END IF

     -- Asignando datos de registro
     LET g_reg.nofase = 1
    END IF 

    -- Fase datos escrituracion
    IF (nofase=2) THEN 
     IF g_reg.codabo IS NULL THEN
       CALL msg("Ingrese notario.")
       NEXT FIELD codabo
     END IF 
     
     IF g_reg.numesc IS NULL THEN
       CALL msg("Ingrese numero de escritura.")
       NEXT FIELD numesc
     END IF 

     IF g_reg.fecesc IS NULL THEN
       CALL msg("Ingrese fecha de escritura.")
       NEXT FIELD fecesc
     END IF 

     IF g_reg.fecesc >TODAY THEN
       CALL msg("Fecha de escritura no puede ser mayor a fecha del dia.")
       NEXT FIELD fecesc
     END IF  

     -- Validando fecha de escrituracion y asignacion 
     IF g_reg.fecesc<g_reg.fecasi THEN
        CALL msg("Fecha de escritura menor a fecha de asignacion.")
        NEXT FIELD fecesc 
     END IF

     IF g_reg.hojini IS NULL THEN 
        CALL msg("Ingrese hoja inicial del protocolo.")
        NEXT FIELD hojini
     END IF 

     IF g_reg.hojfin IS NULL THEN
        CALL msg("Ingrese hoja final del protocolo.")
        NEXT FIELD hojfin
     END IF 

     -- Validando hoja inicial y final del protocolo
     IF g_reg.hojfin<g_reg.hojini THEN
        CALL msg("Hoja final menor a hoja inicial.")
        NEXT FIELD hojini 
     END IF

     -- Validando hoja inicial 2 y final 2 del protocolo
     IF g_reg.hojin2 IS NOT NULL AND g_reg.hojfi2 IS NOT NULL THEN 
      IF g_reg.hojfi2<g_reg.hojin2 THEN
        CALL msg("Hoja final 2 menor a hoja inicial 2.")
        NEXT FIELD hojin2 
      END IF 
     END IF
     
     -- Calculando datos
     CALL CalculaCobros(2,0,0)

     -- Asignando datos de registro
     LET g_reg.feesct = CURRENT
     LET g_reg.usresc = usuario
     LET g_reg.horesc = CURRENT
     LET g_reg.nofase = 2
    END IF 

    -- Fase datos formalizacion 
    IF (nofase=4) THEN 
       -- Calculando datos
       CALL CalculaCobros(2,0,0)
     
     ---IF g_reg.fevfir IS NULL THEN
       ---CALL msg("Ingrese fecha de envio a firma.")
       ---NEXT FIELD fevfir   
     ---END IF  

        IF g_reg.fevfir >TODAY THEN
          CALL msg("Fecha de envio a firma no puede ser mayor a fecha del dia.")
          NEXT FIELD fevfir
        END IF  

     ---IF g_reg.frefir IS NULL THEN
       ---CALL msg("Ingrese fecha de regreso de firma.")
       ---NEXT FIELD frefir   
     ---END IF  

       IF g_reg.frefir >TODAY THEN
          CALL msg("Fecha de regreso de firma no puede ser mayor a fecha del dia.")
          NEXT FIELD fevfir
       END IF  

     -- Validando fecha de regreso de firma 
       IF g_reg.frefir<g_reg.fevfir THEN
        --CALL msg("Fecha de regreso de firma menor a fecha de envio a firma.")
        --NEXT FIELD frefir
       END IF

       -- Calculando datos
       --CALL CalculaCobros(2,0,0)

     ---IF g_reg.finrpg IS NULL THEN
       ---CALL msg("Ingrese fecha de ingreso a registro.")
       ---NEXT FIELD finrpg   
     ---END IF  

       IF g_reg.finrpg >TODAY THEN
         CALL msg("Fecha de ingreso a registro no puede ser mayor a fecha del dia.")
         NEXT FIELD finrpg
       END IF  

     ---IF g_reg.fegrpg IS NULL THEN
       ---CALL msg("Ingrese fecha de egreso del registro,")
       ---NEXT FIELD fegrpg   
     ---END IF  

       IF g_reg.fegrpg >TODAY THEN
         CALL msg("Fecha de egreso del registro no puede ser mayor a fecha del dia.")
         NEXT FIELD fegrpg
       END IF  

     -- Validando fecha de egreso de registro
       --IF g_reg.fegrpg<g_reg.finrpg THEN
        --CALL msg("Fecha de egreso de registro menor a fecha de ingreso de registro.")
        --NEXT FIELD fegrpg
       --END IF

     -- Calculando datos
     --CALL CalculaCobros(2,0,0)

     --IF g_reg.nrerpg IS NULL THEN
       --CALL msg("Ingrese el numero del recibo del RGP.")
       --NEXT FIELD nrerpg   
     --END IF 
     
     --IF g_reg.ndbrpg IS NULL THEN
       --CALL msg("Ingrese el numero del documento o boleta prenda del RGP.")
       --NEXT FIELD ndbrpg   
     --END IF 

       IF g_reg.toparp IS NULL OR 
          g_reg.toparp <0 THEN
          CALL msg("Ingrese el total pago en RGP.")
          NEXT FIELD toparp   
       END IF 

     -- Calculando datos
       CALL CalculaCobros(2,0,0)
     
     -- Asignando datos de registro
     LET g_reg.fecfor = CURRENT
     LET g_reg.usrfor = usuario
     LET g_reg.horfor = CURRENT
     LET g_reg.nofase = 4
    END IF

    -- Fase reingreso RGP
    IF (nofase=5) THEN 
     -- Calculando datos
     CALL CalculaCobros(2,0,0)
     
     IF g_reg.canrch IS NULL THEN
       CALL msg("Ingrese numero de rechazos.")
       NEXT FIELD canrch   
     END IF  

     -- Calculando datos
     CALL CalculaCobros(2,0,0)

     IF g_reg.ferech IS NULL THEN
       CALL msg("Ingrese fecha del rechazo.")
       NEXT FIELD ferech   
     END IF  

     IF g_reg.ferech >TODAY THEN
       CALL msg("Fecha del rechazo no puede ser mayor a fecha del dia.")
       NEXT FIELD ferech 
     END IF  

     -- Calculando datos
     CALL CalculaCobros(2,0,0)

     IF g_reg.fecrei IS NULL THEN
       CALL msg("Ingrese fecha de reingreso.")
       NEXT FIELD fecrei   
     END IF  

     IF g_reg.fecrei >TODAY THEN
       CALL msg("Fecha de reingreso no puede ser mayor a fecha del dia.")
       NEXT FIELD fecrei
     END IF  

     -- Validando fecha de reingreso 
     IF g_reg.fecrei<g_reg.ferech THEN
        CALL msg("Fecha de reingreso menor a fecha de rechazo.")
        NEXT FIELD fecrei 
     END IF

     -- Calculando datos
     CALL CalculaCobros(2,0,0)

     IF g_reg.nrecrg IS NULL THEN
       CALL msg("Ingrese el numero del recibo del RGP.")
       NEXT FIELD nrecrg   
     END IF 
     
     IF g_reg.ndocrg IS NULL THEN
       CALL msg("Ingrese el numero del documento del RGP.")
       NEXT FIELD ndocrg   
     END IF 

     {IF g_reg.fecdev IS NULL THEN
       CALL msg("Ingrese fecha de devolucion.")
       NEXT FIELD fecdev   
     END IF}  

     IF g_reg.fecdev >TODAY THEN
       CALL msg("Fecha de devolucion no puede ser mayor a fecha del dia.")
       NEXT FIELD fecdev
     END IF  

     -- Validando fecha de devolucion
     IF g_reg.fecdev<g_reg.fecrei THEN
        CALL msg("Fecha de devolucion menor a fecha de reingreso.")
        NEXT FIELD fecdev
     END IF

     -- Calculando datos
     CALL CalculaCobros(2,0,0)

     -- Asignando datos de registro
     LET g_reg.fecrch = CURRENT
     LET g_reg.usrrch = usuario
     LET g_reg.horrch = CURRENT
     LET g_reg.nofase = 5
    END IF

    -- Fase datos de pago y facturacion 
    IF (nofase=6) THEN 
     -- Calculando datos
     CALL CalculaCobros(2,0,0)
     
     IF g_reg.totpag IS NULL THEN
       CALL msg("Ingrese el total pagado.")
       NEXT FIELD totpag   
     END IF  

     IF g_reg.numbol IS NULL THEN
       CALL msg("Ingrese el numero de boleta.")
       NEXT FIELD numbol   
     END IF  

     IF g_reg.fecbol IS NULL THEN
       CALL msg("Ingrese fecha de la boleta.")
       NEXT FIELD fecbol   
     END IF  

     IF g_reg.fecbol >TODAY THEN
       CALL msg("Fecha de la boleta no puede ser mayor a fecha del dia.")
       NEXT FIELD fecbol
     END IF  

     -- Validando fecha de la boleta  
     IF g_reg.valcre>0 THEN
      IF g_reg.fecbol<g_reg.fegrpg THEN
        --CALL msg("Fecha de la boleta menor a fecha egreso del registro.")
        --NEXT FIELD fecbol 
      END IF 
     END IF

     IF g_reg.numdep IS NULL THEN
       CALL msg("Ingrese el numero del deposito electronico.")
       NEXT FIELD numdep   
     END IF 
     
     -- Calculando datos
     CALL CalculaCobros(2,0,0)

     -- Asignando datos de registro
     LET g_reg.fecdap = CURRENT
     LET g_reg.usrdap = usuario
     LET g_reg.hordap = CURRENT
     LET g_reg.nofase = 6
    END IF

    -- Fase datos estado cuenta
    IF (nofase=7) THEN 
     -- Calculando datos
     CALL CalculaCobros(2,0,0)
     
     IF g_reg.fecdeb IS NULL THEN
       CALL msg("Ingrese fecha del debito.")
       NEXT FIELD fecdeb   
     END IF  

     IF g_reg.fecdeb >TODAY THEN
       CALL msg("Fecha del debito no puede ser mayor a fecha del dia.")
       NEXT FIELD fecdeb
     END IF  

     -- Validando fecha del debito 
     IF g_reg.fecdeb<g_reg.fecbol THEN
        CALL msg("Fecha del debito menor a fecha de la boleta de pago.")
        NEXT FIELD fecdeb 
     END IF

     IF g_reg.nuboho IS NULL THEN
       CALL msg("Ingrese el numero de la boleta de honorarios.")
       NEXT FIELD nuboho   
     END IF 

     IF g_reg.modeho IS NULL THEN
       CALL msg("Ingrese el monto del debito de la boleta de honorarios.")
       NEXT FIELD modeho   
     END IF 

     IF g_reg.nuboga IS NULL THEN
       CALL msg("Ingrese el numero de la boleta de gastos.")
       NEXT FIELD nuboga   
     END IF 

     IF g_reg.modega IS NULL THEN
       CALL msg("Ingrese el monto del debito de la boleta de gastos.")
       NEXT FIELD modega   
     END IF 

     IF g_reg.nufade IS NULL THEN
       CALL msg("Ingrese el numero de factura electronica por el debito.")
       NEXT FIELD nufade   
     END IF 
     
     -- Calculando datos
     CALL CalculaCobros(2,0,0)

     -- Asignando datos de registro
     LET g_reg.fecest = CURRENT
     LET g_reg.usrest = usuario
     LET g_reg.horest = CURRENT
     LET g_reg.nofase = 7
    END IF

    -- Fase datos envio pcl 
    IF (nofase=8) THEN 
     -- Calculando datos
     CALL CalculaCobros(2,0,0)

     IF g_reg.lugenv IS NULL THEN
       CALL msg("Ingrese lugar de envio.")
       NEXT FIELD lugenv   
     END IF  

     IF g_reg.fecenv IS NULL THEN
       CALL msg("Ingrese fecha de envio.")
       NEXT FIELD fecenv   
     END IF  

     IF g_reg.fecenv >TODAY THEN
       CALL msg("Fecha de envio no puede ser mayor a fecha del dia.")
       NEXT FIELD fecenv
     END IF

     -- Validando fecha de envio
     IF (g_reg.fecenv<g_reg.fegrpg) THEN
        --CALL msg("Fecha de envio menor a fecha de egreso de registro.")
        --NEXT FIELD fecenv 
     END IF

     {IF g_reg.fecrep IS NULL THEN
       CALL msg("Ingrese fecha de recepcion.")
       NEXT FIELD fecrep   
     END IF}  

     IF g_reg.fecrep >TODAY THEN
       CALL msg("Fecha de recepcion no puede ser mayor a fecha del dia.")
       NEXT FIELD fecrep
     END IF  

     -- Validando fecha de recepcion
     IF g_reg.fecrep<g_reg.fecenv THEN
        --CALL msg("Fecha del recepcion menor a fecha de envio.")
        --NEXT FIELD fecrep 
     END IF

     IF g_reg.nucoev IS NULL THEN
       CALL msg("Ingrese el numero correlativo del documento de envio.")
       NEXT FIELD nucoev   
     END IF 
     
     -- Calculando datos
     CALL CalculaCobros(2,0,0)
     
     -- Asignando datos de registro
     LET g_reg.fecpcl = CURRENT
     LET g_reg.usrpcl = usuario
     LET g_reg.horpcl = CURRENT
     LET g_reg.nofase = 8
    END IF

    -- Fase datos finales
    IF (nofase=9) THEN 
     -- Calculando datos
     CALL CalculaCobros(2,0,0)
     
     -- Asignando datos de registro
     LET g_reg.fecdfi = CURRENT
     LET g_reg.usrdfi = usuario
     LET g_reg.hordfi = CURRENT
     LET g_reg.nofase = 9
    END IF
    
    -- Verificando cambios
    IF operacion = 'M' THEN
       IF g_reg.* = u_reg.* THEN
          CALL msg("No se efectuaron cambios.")
          EXIT DIALOG 
       ELSE
          -- Verificando cambio de tipo de documento u valor del documento
          IF (g_reg.tipdoc!=u_reg.tipdoc) OR
             (g_reg.valcre!=u_reg.valcre) THEN
             -- Calculando datos
             CALL CalculaCobros(1,1,1) 
          END IF 
       END IF 
    END IF

    -- Confirmando cambios
    CASE box_gradato("Seguro de grabar")
     WHEN "Si"
      LET resultado = TRUE
      EXIT DIALOG
     WHEN "No"
      EXIT DIALOG 
     OTHERWISE
      CONTINUE DIALOG 
    END CASE 
    LET resultado = TRUE
    EXIT DIALOG 

   ON ACTION CANCEL
    EXIT DIALOG
 END DIALOG
   
 IF NOT resultado THEN
    LET g_reg.* = u_reg.*
    DISPLAY BY NAME g_reg.* 
 END IF 
 RETURN resultado 
END FUNCTION

FUNCTION CapturaDesglose(ingreso)
 DEFINE scr,arr,i INTEGER, 
        grabar    SMALLINT, 
        ingreso   SMALLINT, 
        datosok   SMALLINT

 -- Verificando si hay ingreso de datos
IF ingreso THEN
   -- Desplegando datos del desglose
   CALL DesplegarDatosDesglose() 

   -- Ingresando datos del desglose 
   INPUT ARRAY v_cobros FROM sCobros.*
      ATTRIBUTE (WITHOUT DEFAULTS=TRUE,INSERT ROW=FALSE,DELETE ROW=FALSE,
             APPEND ROW=FALSE,ACCEPT=FALSE,CANCEL=FALSE) 

      ON ACTION ACCEPT 
         -- Confirmando cambios
         LET grabar = FALSE 
         CASE box_gradato("Seguro de grabar")
            WHEN "Si"
               LET grabar = TRUE
               EXIT INPUT 
            WHEN "No"
               EXIT INPUT 
            OTHERWISE
               CONTINUE INPUT 
            END CASE 

      ON ACTION CANCEL
         LET grabar = FALSE 
         EXIT INPUT

      --BEFORE INPUT   
         --LET arr = ARR_CURR() 
       --CALL setAttrDesglose(1)
       --CALL DIALOG.setCellAttributes(v_cobros_attr) 

      {BEFORE ROW 
       LET arr = ARR_CURR()} 
       --CALL setAttrDesglose(arr)

      {BEFORE FIELD curcob 
       LET arr = ARR_CURR()
       LET scr = SCR_LINE() 
       LET v_cobros[arr].curcob = ">" 
       DISPLAY v_cobros[arr].* TO sCobros[scr].*
       ATTRIBUTE(REVERSE,BLUE)} 

      {AFTER FIELD  curcob 
       LET arr = ARR_CURR()
       LET scr = SCR_LINE() 
       LET v_cobros[arr].curcob = " " 
       DISPLAY v_cobros[arr].* TO sCobros[scr].*

       -- Verificando si hay ingreso de cantidad
       IF v_cobros[arr].ingcan=1 THEN
          CALL Dialog.SetFieldActive("cancob",1) 
       ELSE
          CALL Dialog.SetFieldActive("cancob",0) 
       END IF }

       -- Verificando si hay ingreso de valor
       {IF v_cobros[arr].valsum=0 AND v_cobros[arr].valrst=0 THEN
          CALL Dialog.SetFieldActive("valcob",0) 
       ELSE}
          --CALL Dialog.SetFieldActive("valcob",1) 
       --END IF 

      ON CHANGE cancob 
         -- Validando cantidad 
         LET arr = ARR_CURR()
         LET scr = SCR_LINE() 

         -- Recalculando total si valor cobro es mayor que cero 
         IF v_cobros[arr].ingcan=1 THEN
            IF v_cobros[arr].cancob IS NULL THEN
               LET v_cobros[arr].cancob = 1
            END IF

            IF (v_cobros[arr].valcob>0) THEN 
               IF (v_cobros[arr].mulcx1=0) THEN 
                  LET v_cobros[arr].totcob = (v_cobros[arr].cancob*v_cobros[arr].valcob)
               ELSE
                  IF (v_cobros[arr].cancob>1) THEN 
                     LET v_cobros[arr].totcob = v_cobros[arr].valcob
                  ELSE
                     LET v_cobros[arr].totcob = (v_cobros[arr].cancob*v_cobros[arr].valcob)
                  END IF
               END IF 
            END IF 
         ELSE 
            LET v_cobros[arr].cancob = 1
         END IF 
         IF v_cobros[arr].cancob = 0 THEN LET v_cobros[arr].totcob = 0 END IF 
         DISPLAY v_cobros[arr].cancob TO sCobros[scr].cancob 
         DISPLAY v_cobros[arr].totcob TO sCobros[scr].totcob 

         -- Calculando totales
         CALL CalculoTotalesDesglose() 
    
      {AFTER FIELD cancob 
         -- Validando cantidad 
         LET arr = ARR_CURR()
         LET scr = SCR_LINE() 

         -- Recalculando total si valor cobro es mayor que cero 
         IF v_cobros[arr].ingcan=1 THEN
            IF v_cobros[arr].cancob IS NULL THEN
               LET v_cobros[arr].cancob = 1
            END IF

            IF (v_cobros[arr].valcob>0) THEN 
               IF (v_cobros[arr].mulcx1=0) THEN 
                  LET v_cobros[arr].totcob = (v_cobros[arr].cancob*v_cobros[arr].valcob)
               ELSE
                  IF (v_cobros[arr].cancob>1) THEN 
                     LET v_cobros[arr].totcob = v_cobros[arr].valcob
                  ELSE
                     LET v_cobros[arr].totcob = (v_cobros[arr].cancob*v_cobros[arr].valcob)
                  END IF
               END IF 
            END IF 
         ELSE 
            LET v_cobros[arr].cancob = 1
         END IF 
         DISPLAY v_cobros[arr].cancob TO sCobros[scr].cancob 
         DISPLAY v_cobros[arr].totcob TO sCobros[scr].totcob} 

         -- Calculando totales
         CALL CalculoTotalesDesglose() 
      
      ON CHANGE valcob 
         LET scr = SCR_LINE() 
         LET arr = ARR_CURR()
         -- Totalizando
         IF v_cobros[arr].valsum=1 AND v_cobros[arr].valrst=0 THEN
            LET v_cobros[arr].totcob = v_cobros[arr].valcob 
         END IF
         IF v_cobros[arr].valsum=0 AND v_cobros[arr].valrst=1 THEN
            LET v_cobros[arr].totcob = (v_cobros[arr].valcob*(-1)) 
         END IF
         IF v_cobros[arr].valsum=0 THEN --AND v_cobros[arr].valrst=0 THEN
            LET v_cobros[arr].totcob = v_cobros[arr].valcob 
         END IF
         DISPLAY v_cobros[arr].totcob TO sCobros[scr].totcob 

         -- Calculando totales
         CALL CalculoTotalesDesglose() 

      {AFTER FIELD valcob 
       -- Validando valor 
       LET arr = ARR_CURR()
       LET scr = SCR_LINE() 

       -- Verificando ingreso del valor
       IF v_cobros[arr].valcob IS NULL OR
          v_cobros[arr].valcob <0 THEN
          CALL msg("Valor debe ser mayor que cero.")
          LET v_cobros[arr].valcob = 0 
          DISPLAY v_cobros[arr].valcob TO sCobros[scr].valcob 
          NEXT FIELD valcob 
       END IF 

       -- Totalizando
       IF v_cobros[arr].valsum=1 AND v_cobros[arr].valrst=0 THEN
          LET v_cobros[arr].totcob = v_cobros[arr].valcob 
       END IF
       IF v_cobros[arr].valsum=0 AND v_cobros[arr].valrst=1 THEN
          LET v_cobros[arr].totcob = (v_cobros[arr].valcob*(-1)) 
       END IF
       DISPLAY v_cobros[arr].totcob TO sCobros[scr].totcob 

       -- Calculando totales
       CALL CalculoTotalesDesglose()} 
     END INPUT 
 ELSE
      -- No hay ingreso solo graba lo que ya esta cargado en el vector
      LET grabar = TRUE
 END IF 

 -- Grabando datos del desglose 
 IF grabar THEN
  IF g_reg.numexp IS NOT NULL THEN 
    -- Actualizando datos de la fase del desglose cobros
    -- Asignando datos de registro
    LET g_reg.fecdsg = CURRENT
    LET g_reg.usrdsg = usuario
    LET g_reg.hordsg = CURRENT
    LET g_reg.nofase = 3

    -- Actualizando datos
    LET datosok = actualizar(0)

    -- Borrando desglose antes de grabar
    DELETE FROM notariado_cob  
    WHERE notariado_cob.numexp = g_reg.numexp 

    -- Grabando desglose 
    FOR i = 1 TO v_cobros.getLength()
     IF v_cobros[i].tipcob IS NULL THEN
        CONTINUE FOR
     END IF 

     INSERT INTO notariado_cob
     VALUES (g_reg.numexp,  
             v_cobros[i].tipcob,
             v_cobros[i].nomcob,
             v_cobros[i].ingcan,
             v_cobros[i].sumsub,
             v_cobros[i].mulcx1,
             v_cobros[i].caliva,
             v_cobros[i].valsum,
             v_cobros[i].valrst,
             v_cobros[i].cancob,  
             v_cobros[i].valcob,
             v_cobros[i].totcob,
             i)
    END FOR 
  END IF 
 END IF 
END FUNCTION 

-- Subrutina para calcular el total de dias de escrituracion

FUNCTION CalculaCobros(calculo,inicializa,grabar)
 DEFINE g_tdc      RECORD  
         cancob    LIKE notariado_cob.cancob, 
         valcob    LIKE notariado_cob.valcob,
         totcob    LIKE notariado_cob.totcob 
        END RECORD, 
        g_cob      RECORD 
         tipcob    LIKE grupostiposdoc_cob.tipcob, 
         nomcob    LIKE tiposcobro.nomcob, 
         rangos    LIKE grupostiposdoc_cob.rangos, 
         valcob    LIKE grupostiposdoc_cob.valcob,
         porcen    LIKE grupostiposdoc_cob.porcen, 
         valmax    LIKE grupostiposdoc_cob.valmax, 
         ingcan    LIKE grupostiposdoc_cob.ingcan,
         sumsub    LIKE grupostiposdoc_cob.sumsub,
         mulcx1    LIKE grupostiposdoc_cob.mulcx1,
         caliva    LIKE grupostiposdoc_cob.caliva,
         valsum    LIKE grupostiposdoc_cob.valsum,
         valrst    LIKE grupostiposdoc_cob.valres,
         valini    LIKE grupostiposdoc_ran.valini, 
         valfin    LIKE grupostiposdoc_ran.valfin, 
         valran    LIKE grupostiposdoc_ran.valran,
         porran    LIKE grupostiposdoc_ran.porran, 
         valres    LIKE grupostiposdoc_ran.valres 
        END RECORD, 
        calculo    SMALLINT, 
        inicializa SMALLINT, 
        grabar     SMALLINT, 
        tc,conteo  INTEGER,
        desiva     CHAR(50) 

 -- Si tipo de calculo es inicial 
 IF (calculo=1) THEN 

  -- Verificando si ya existen datos registrados
  SELECT COUNT(*)
   INTO  conteo
   FROM  notariado_cob a
   WHERE a.numexp = g_reg.numexp

  -- Verificando si hay recalculo por cambio de tipo o valor del documento 
  IF (inicializa=1) THEN
     IF (conteo>0) THEN
        LET conteo=0
     END IF 
  END IF 

  IF (conteo=0) THEN -- Cargando datos parametrizados 
   -- Calculando datos fase 3

   -- Obteniendo grupo del tipo de documento
   SELECT NVL(a.codgru,0)
    INTO  g_reg.codgru 
    FROM  grupostiposdoc_det a 
    WHERE a.tipdoc = g_reg.tipdoc 
    IF (status=NOTFOUND) THEN
       IF NOT grabar THEN 
        CALL msg("Tipo de documento no asociado en ningun grupo."||
                 "\nDesglose de cobro se asignara con valor cero.") 
       END IF 
       LET g_reg.codgru = 0
    END IF 

   -- Inicializado vector de datos
   CALL v_cobros.Clear() 

   -- Obteniendo cobros asignados al tipo de documento
   DECLARE c_cobros CURSOR FOR
   SELECT x.tipcob,
          y.nomcob,
          x.rangos,
          x.valcob,
          x.porcen,
          x.valmax,
          x.ingcan,
          x.sumsub,
          x.mulcx1,
          x.caliva,
          x.valsum,
          x.valres, 
          0,
          0,
          0,
          0,
          0 
    FROM  grupostiposdoc_cob x,tiposcobro y,grupostiposdoc_det z
    WHERE x.codgru = z.codgru
      AND x.codgru = g_reg.codgru
      AND z.tipdoc = g_reg.tipdoc 
      AND y.tipcob = x.tipcob
   ORDER BY y.norden 

   LET tc     = 0 
   LET totiva = 0 
   FOREACH c_cobros INTO g_cob.* 
    -- Datos calculados
    -- Verificando condiciones y haciendo calculos
    IF g_cob.rangos=0 THEN 
       -- Seleccionando valores fijos 
       LET g_tdc.cancob = 1

       IF g_cob.valsum=0 AND g_cob.valrst=0 THEN
        IF g_cob.ingcan=1 THEN 
          IF g_cob.valcob>0 THEN
             LET g_tdc.valcob = g_cob.valcob 
          ELSE
             LET g_tdc.valcob = (g_reg.valcre*g_cob.porcen) 
             IF (g_cob.valmax>0) THEN 
              IF (g_tdc.valcob>g_cob.valmax) THEN
                LET g_tdc.valcob = g_cob.valmax
              END IF 
             END IF 
          END IF 
          LET g_tdc.totcob = g_tdc.valcob 
        ELSE
          IF g_cob.valcob>0 THEN
             LET g_tdc.totcob = g_cob.valcob 
          ELSE
             LET g_tdc.totcob = (g_reg.valcre*g_cob.porcen) 
             IF (g_cob.valmax>0) THEN 
              IF (g_tdc.totcob>g_cob.valmax) THEN
                LET g_tdc.totcob = g_cob.valmax
              END IF 
             END IF 
          END IF 
          LET g_tdc.valcob = g_tdc.totcob 
        END IF 
       ELSE
        LET g_tdc.valcob = 0 
        LET g_tdc.totcob = 0 
       END IF 
    ELSE
       -- Seleccionando valores por rangos 
       SELECT NVL(x.valini,0),
              NVL(x.valfin,0),
              NVL(x.valran,0),
              NVL(x.porran,0), 
              NVL(x.valres,0)
        INTO  g_cob.valini,
              g_cob.valfin, 
              g_cob.valran,
              g_cob.porran,
              g_cob.valres
        FROM  grupostiposdoc_ran x
        WHERE x.codgru = g_reg.codgru 
          AND x.tipcob = g_cob.tipcob 
          AND g_reg.valcre >= x.valini 
          AND g_reg.valcre <= x.valfin
       IF (status!=NOTFOUND) THEN
          LET g_tdc.cancob = 1
          LET g_tdc.totcob = (g_cob.valran+
                             ((g_reg.valcre-g_cob.valres)*g_cob.porran))
          LET g_tdc.valcob = g_tdc.totcob 
       ELSE 
          LET g_tdc.cancob = 0
          LET g_tdc.valcob = 0 
          LET g_tdc.totcob = 0 
       END IF 
    END IF 

    -- Verificando si se paga impuesto 
    IF g_cob.caliva=1 THEN 
       LET totiva = totiva+g_tdc.totcob
    END IF 

    -- Excluyendo tipos de cobro que tanto el valor cobro y el total cobro sean 0
    IF g_cob.valsum=0 AND g_cob.valrst=0 THEN
     IF (g_tdc.valcob=0) AND (g_tdc.totcob=0) THEN
        CONTINUE FOREACH
     END IF 
    END IF 

    -- Asignando datos
    LET tc = tc+1 
    LET v_cobros[tc].tipcob = g_cob.tipcob
    LET v_cobros[tc].nomcob = g_cob.nomcob 
    LET v_cobros[tc].ingcan = g_cob.ingcan 
    LET v_cobros[tc].sumsub = g_cob.sumsub 
    LET v_cobros[tc].mulcx1 = g_cob.mulcx1 
    LET v_cobros[tc].caliva = g_cob.caliva 
    LET v_cobros[tc].valsum = g_cob.valsum 
    LET v_cobros[tc].valrst = g_cob.valrst 
    LET v_cobros[tc].curcob = " " 
    LET v_cobros[tc].cancob = g_tdc.cancob
    LET v_cobros[tc].valcob = g_tdc.valcob
    LET v_cobros[tc].totcob = g_tdc.totcob 
   END FOREACH
   CLOSE c_cobros
   FREE  c_cobros

   -- Agregando linea de impuestos
   IF (totiva>0) THEN
    LET tc = tc+1 
    LET v_cobros[tc].tipcob = 0
    LET desiva              = "IVA (",(PorcenIVA*100) USING "<<&.&&"," %) ",
                              totiva USING "<<<,<<&.&&" 
    LET v_cobros[tc].nomcob = desiva 
    LET v_cobros[tc].ingcan = 0 
    LET v_cobros[tc].sumsub = 0 
    LET v_cobros[tc].mulcx1 = 0 
    LET v_cobros[tc].caliva = 0 
    LET v_cobros[tc].valsum = 0 
    LET v_cobros[tc].valrst = 0 
    LET v_cobros[tc].curcob = " "  
    LET v_cobros[tc].cancob = 1 
    LET v_cobros[tc].totcob = totiva*PorcenIVA 
    LET v_cobros[tc].valcob = v_cobros[tc].totcob 
   END IF 

   -- Desplegando datos
   DISPLAY ARRAY v_cobros TO sCobros.*
    BEFORE DISPLAY
     EXIT DISPLAY
   END DISPLAY 

   -- Calculando totales 
   CALL CalculoTotalesDesglose()  

   -- Verificando si hay recalculo por cambio de tipo o monto del documento 
   IF (grabar=1) THEN
      -- Grabando cambios 
      CALL CapturaDesglose(0)
   END IF
  ELSE
   -- Obtiene datos ya registrados 
   CALL DesplegarDatosDesglose() 

   -- Calculando totales 
   CALL CalculoTotalesDesglose()  
  END IF 
 END IF 

 -- Calculando datos fase 2
 -- Calculando total de hojas de protocolo
 LET g_reg.todipt = 0
 IF (g_reg.hojini>0 AND g_reg.hojfin>0) THEN 
    LET g_reg.todipt = (g_reg.hojfin-g_reg.hojini)+1
    IF (g_reg.todipt=0) THEN LET g_reg.todipt = 1 END IF
 END IF
 IF (g_reg.hojin2>0 AND g_reg.hojfi2>0) THEN 
    LET g_reg.todipt = (g_reg.hojfin-g_reg.hojini)+1 + (g_reg.hojfi2-g_reg.hojin2)+1
    IF (g_reg.todipt=0) THEN LET g_reg.todipt = 1 END IF
 END IF
 
 DISPLAY BY NAME g_reg.todipt

 -- Calculando total de dias escrituracion
 LET g_reg.todies = 0
 IF (g_reg.fecesc IS NOT NULL) AND (g_reg.fecasi IS NOT NULL) THEN
    LET g_reg.todies = (g_reg.fecesc-g_reg.fecasi)
    IF (g_reg.todies=0) THEN LET g_reg.todies = 1 END IF
 END IF
 DISPLAY BY NAME g_reg.todies

 -- Calculando datos fase 4
 -- Calculando total de dias firma
 LET g_reg.tdifir = 0
 IF (g_reg.fevfir IS NOT NULL) AND (g_reg.frefir IS NOT NULL) THEN
    LET g_reg.tdifir = (g_reg.frefir-g_reg.fevfir)
    IF (g_reg.tdifir=0) THEN LET g_reg.tdifir = 1 END IF
 END IF
 DISPLAY BY NAME g_reg.tdifir

 -- Calculando total pagado en registro vrs total cobrado registro de la propiedad base,
 -- excedente, finca adicionales.
 LET g_reg.topaco = 0
 IF (g_reg.vtcoba IS NOT NULL) AND (g_reg.toparp IS NOT NULL) THEN
    LET g_reg.topaco = (g_reg.vtcoba-g_reg.toparp)
 END IF 
 DISPLAY BY NAME g_reg.topaco

 -- Calculando total de dias registro
 LET g_reg.todifo = 0
 IF (g_reg.finrpg IS NOT NULL) AND (g_reg.fegrpg IS NOT NULL) THEN
    LET g_reg.todifo = (g_reg.fegrpg-g_reg.finrpg)
    IF (g_reg.todifo=0) THEN LET g_reg.todifo = 1 END IF
 END IF
 DISPLAY BY NAME g_reg.todifo
 
 -- Calculando datos fase 5
 -- Calculando total de dias rechazo
 LET g_reg.totrch = 0
 IF (g_reg.canrch IS NOT NULL) THEN
    LET g_reg.totrch = (g_reg.canrch*ValorDiaRechazo) 
 END IF 
 DISPLAY BY NAME g_reg.totrch 

 -- Calculando total de dias rechazo
 LET g_reg.todidv = 0
 IF (g_reg.ferech IS NOT NULL AND g_reg.fecdev IS NOT NULL) THEN 
    LET g_reg.todidv = (g_reg.fecdev-g_reg.ferech) 
    IF (g_reg.todidv=0) THEN LET g_reg.todidv = 1 END IF 
 END IF
 DISPLAY BY NAME g_reg.todidv

 -- Calculando total de dias reingreso
 LET g_reg.todich = 0
 IF (g_reg.todifo IS NOT NULL AND g_reg.todidv IS NOT NULL) THEN
    LET g_reg.todich = (g_reg.todifo+g_reg.todidv)
 END IF
 DISPLAY BY NAME g_reg.todich 

 -- Calculando datos fase 6
  -- No lleva calculos

 -- Calculando datos fase 7
  -- No lleva calculos

 -- Calculando datos fase 8
 -- Calculando numero de dias entre fecha de recepcion y fecha de envio
 LET g_reg.todiev = 0
 IF (g_reg.fecenv IS NOT NULL AND g_reg.fecrep IS NOT NULL) THEN
    LET g_reg.todiev = (g_reg.fecrep-g_reg.fecenv) 
    IF g_reg.todiev = 0 THEN LET g_reg.todiev = 1 END IF
 END IF 
 DISPLAY BY NAME g_reg.todiev

 -- Calculando total de dias del proceso
 LET g_reg.diapro = 0
 IF (g_reg.fecasi IS NOT NULL AND g_reg.fecrep IS NOT NULL) THEN
    LET g_reg.diapro = (g_reg.fecrep-g_reg.fecasi) 
    IF g_reg.diapro = 0 THEN LET g_reg.diapro = 1 END IF
 END IF 
 DISPLAY BY NAME g_reg.diapro

 -- Calculando datos fase 9
 -- No lleva calculos
END FUNCTION

FUNCTION CalculoTotalesDesglose()  
 DEFINE i,tc    SMALLINT,
        desiva  CHAR(50) 

 -- Calculando totales del desglose 

 -- Calculando iva 
 LET totiva = 0 
 FOR i = 1 TO v_cobros.getLength() 
  IF v_cobros[i].tipcob IS NULL OR
     v_cobros[i].caliva=0 THEN
     CONTINUE FOR
  END IF

  -- Totalizando 
  LET totiva = totiva+v_cobros[i].totcob
 END FOR 

 -- Agregando impuestos
 IF (totiva>0) THEN
    LET tc = v_cobros.getLength() 
    LET v_cobros[tc].tipcob = 0
    LET desiva              = "IVA (",(PorcenIVA*100) USING "<<&.&&"," %) ",
                              totiva USING "<<<,<<&.&&" 
    LET v_cobros[tc].nomcob = desiva 
    LET v_cobros[tc].ingcan = 0 
    LET v_cobros[tc].sumsub = 0 
    LET v_cobros[tc].mulcx1 = 0 
    LET v_cobros[tc].caliva = 0 
    LET v_cobros[tc].valsum = 0 
    LET v_cobros[tc].valrst = 0 
    LET v_cobros[tc].curcob = " "  
    LET v_cobros[tc].cancob = 1 
    LET v_cobros[tc].totcob = totiva*PorcenIVA 
    LET v_cobros[tc].valcob = v_cobros[tc].totcob 
    DISPLAY ARRAY v_cobros TO sCobros.* BEFORE DISPLAY EXIT DISPLAY END DISPLAY
 END IF 

 -- Calculando totales 
 LET g_reg.vtcoba = 0 
 LET g_reg.vtocob = 0 
 FOR i = 1 TO v_cobros.getLength() 
  IF v_cobros[i].tipcob IS NULL THEN
     CONTINUE FOR
  END IF

  -- Total a cobrar
  LET g_reg.vtocob = g_reg.vtocob+v_cobros[i].totcob 

  -- Subtotal cobrado 
  IF v_cobros[i].sumsub THEN
     LET g_reg.vtcoba = g_reg.vtcoba+v_cobros[i].totcob 
  END IF 
  
  IF g_reg.tipdoc = 15 THEN --Fiduciaria
      LET g_reg.vtcoba = NULL
  ELSE 
      IF g_reg.tipdoc = 13 OR g_reg.tipdoc = 14  THEN --Prendaria / Prendaria-Fiduciaria
         LET g_reg.vtcoba = 460
      END IF    
  END IF 
 END FOR 

 -- Desplegando totales 
 DISPLAY BY NAME g_reg.vtcoba,g_reg.vtocob 
END FUNCTION 

-- Subrutina para desplegar las lineas del vector tipos de documento

FUNCTION setAttrDesglose(id)
 DEFINE i,id SMALLINT

 FOR i=1 TO v_cobros.getLength()
  LET v_cobros_attr[i].tipcob = "black"
  LET v_cobros_attr[i].nomcob = "black"
  LET v_cobros_attr[i].ingcan = "black"
  LET v_cobros_attr[i].sumsub = "black"
  LET v_cobros_attr[i].mulcx1 = "black"
  LET v_cobros_attr[i].caliva = "black"
  LET v_cobros_attr[i].valsum = "black"
  LET v_cobros_attr[i].valrst = "black"
  LET v_cobros_attr[i].curcob = "black"
  LET v_cobros_attr[i].cancob = "black"
  LET v_cobros_attr[i].valcob = "black"
  LET v_cobros_attr[i].totcob = "black"
 END FOR

 LET v_cobros_attr[id].tipcob = "teal reverse"
 LET v_cobros_attr[id].nomcob = "teal reverse"
 LET v_cobros_attr[id].ingcan = "teal reverse"
 LET v_cobros_attr[id].sumsub = "teal reverse"
 LET v_cobros_attr[id].mulcx1 = "teal reverse"
 LET v_cobros_attr[id].caliva = "teal reverse"
 LET v_cobros_attr[id].valsum = "teal reverse"
 LET v_cobros_attr[id].valrst = "teal reverse"
 LET v_cobros_attr[id].curcob = "teal reverse"
 LET v_cobros_attr[id].cancob = "teal reverse"
 LET v_cobros_attr[id].valcob = "teal reverse"
 LET v_cobros_attr[id].totcob = "teal reverse"
END FUNCTION
