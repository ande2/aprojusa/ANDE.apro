  drop view visexcelnotariado;

  create view visexcelnotariado
  (nomdoc,numexp,codpro,numcre,nomreg,numres,fecres,numage,nomage,nomasi,fecasi,
   nomnot,numesc,fecesc,hojini,hojfin,todipt,todies,nomcli,nombre,valcre,

   honora,totiva,timnot,prites,tesesp,ivacom,honbas,lugfin,valfin,honexe,
   redhon,canfin,finext,gastot,gasmun,difcob,toacob,canele,valele,gascer,
   hojpro,docext,totcob,tototr,

   fevfir,frefir,tdifir,finrpg,fegrpg,nrerpg,ndbrpg,toparp,topaco,codrgo,codarp,
   todifo,canrch,totrch,ferech,fecrei,nrecrg,ndocrg,fecdev,coment,todidv,todich,
   totpag,numbol,fecbol,numdep,fecdeb,nuboho,modeho,nuboga,modega,nufade,lugenv,
   fecenv,fecrep,nucoev,todiev,diapro,numeta,fpagas,mpagno,observ)
  as
SELECT t.nombre,a.numexp,
       case (a.codpro)
        WHEN 1 THEN "GES"
        WHEN 2 THEN "FISICO"
       end case,
       a.numcre,
       r.city_name,
       a.numres,
       a.fecres,
       a.numage,
       g.nombre,
       s.nombre,
       a.fecasi,
       u.user_name,
       a.numesc,
       a.fecesc,
       a.hojini,
       a.hojfin,
       a.todipt,
       a.todies,
       a.nomcli,
       t.nombre,
       a.valcre,
       (SELECT NVL(y.totcob,0) FROM notariado_cob y
         WHERE y.numexp = a.numexp
           AND y.tipcob = 13),
       0,
       (SELECT NVL(y.totcob,0) FROM notariado_cob y
         WHERE y.numexp = a.numexp
           AND y.tipcob = 15),
       (SELECT NVL(y.totcob,0) FROM notariado_cob y
         WHERE y.numexp = a.numexp
           AND y.tipcob = 1),
       (SELECT NVL(y.totcob,0) FROM notariado_cob y
         WHERE y.numexp = a.numexp
           AND y.tipcob = 2),
       (SELECT NVL(y.totcob,0) FROM notariado_cob y
         WHERE y.numexp = a.numexp
           AND y.tipcob = 3),
       (SELECT NVL(y.totcob,0) FROM notariado_cob y
         WHERE y.numexp = a.numexp
           AND y.tipcob = 4),
       (SELECT NVL(y.totcob,0) FROM notariado_cob y
         WHERE y.numexp = a.numexp
           AND y.tipcob = 9),
       (SELECT NVL(y.totcob,0) FROM notariado_cob y
         WHERE y.numexp = a.numexp
           AND y.tipcob = 12),
       (SELECT NVL(y.totcob,0) FROM notariado_cob y
         WHERE y.numexp = a.numexp
           AND y.tipcob = 16),
       (SELECT ROUND(NVL(y.totcob,0),2) FROM notariado_cob y
         WHERE y.numexp = a.numexp
           AND y.tipcob = 16),
       0,
       0,
       (SELECT NVL(y.totcob,0) FROM notariado_cob y
         WHERE y.numexp = a.numexp
           AND y.tipcob = 5),
       (SELECT NVL(y.totcob,0) FROM notariado_cob y
         WHERE y.numexp = a.numexp
           AND y.tipcob = 6),
       (SELECT NVL(y.totcob,0) FROM notariado_cob y
         WHERE y.numexp = a.numexp
           AND y.tipcob = 17),

       a.toparp, 

       (SELECT NVL(y.cancob,0) FROM notariado_cob y
         WHERE y.numexp = a.numexp
           AND y.tipcob = 10),

       (SELECT NVL(y.totcob,0) FROM notariado_cob y
         WHERE y.numexp = a.numexp
           AND y.tipcob = 10),
       
        0,0,0,0,0,

       a.fevfir,
       a.frefir,
       a.tdifir,
       a.finrpg,
       a.fegrpg,
       a.nrerpg,
       a.ndbrpg,
       a.toparp,
       a.topaco,
       a.codrgo,
       a.codarp,
       a.todifo,
       a.canrch,
       a.totrch,
       a.ferech,
       a.fecrei,
       a.nrecrg,
       a.ndocrg,
       a.fecdev,
       a.coment,
       a.todidv,
       a.todich,
       a.totpag,
       a.numbol,
       a.fecbol,
       a.numdep,
       a.fecdeb,
       a.nuboho,
       a.modeho,
       a.nuboga,
       a.modega,
       a.nufade,
       a.lugenv,
       a.fecenv,
       a.fecrep,
       a.nucoev,
       a.todiev,
       a.diapro,
       a.numeta,
       a.fpagas,
       a.mpagno,
       a.observ
from notariado a,tiposdocumento t,city r,outer agencias_bco g,
     outer asistentes_nt s,outer users u
where t.tipdoc   = a.tipdoc
  and r.city_num = a.city_num
  and g.city_num = a.city_num
  and g.numage   = a.numage
  and s.codasn   = a.codasn
  and u.usuid    = a.codabo;

grant select on visexcelnotariado to public;
