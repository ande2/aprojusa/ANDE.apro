-- Consultando registro notariado

GLOBALS "aprom0806_glob.4gl"

FUNCTION consulta(flagConsulta)
 DEFINE flagConsulta        BOOLEAN 
 DEFINE PrimeraVez          BOOLEAN 
 DEFINE flagSede            BOOLEAN 
 DEFINE rDet                fDet
 DEFINE x                   INTEGER
 DEFINE consulta            STRING  
 DEFINE FiltroSede          STRING
 
 IF flagConsulta THEN 
    -- CALL encabezado("Consulta")

    -- Ingreso de datos de la consulta 
    CONSTRUCT BY NAME condicion 
     ON a.numexp,   a.codpro, a.numcre, a.tipdoc,
        a.city_num, a.numage,   a.numres, a.fecres,
        a.codasn,   a.fecasi, a.nomcli, a.valcre,
        a.fecini,   a.usrini, a.codabo, a.numesc,
        a.fecesc,   a.hojini, a.hojin2, a.hojfin, 
        a.hojfi2,   a.todipt,
        a.todies,   a.feesct, a.usresc, a.vtcoba,   
        a.vtocob,   a.fecdsg, a.usrdsg, a.fevfir,   
        a.frefir,   a.tdifir, a.finrpg, a.fegrpg, 
        a.nrerpg,   a.ndbrpg, a.toparp, a.topaco,   
        a.codrgo,   a.codarp, a.todifo, a.fecfor,   
        a.usrfor,   a.totpag, a.numbol, a.fecbol,
        a.numdep,   a.fecdap, a.usrdap, a.fecdeb,   
        a.nuboho,   a.modeho, a.nuboga, a.modega,   
        a.nufade,   a.fecest, a.usrest, a.lugenv,   
        a.fecenv,   a.fecrep, a.nucoev, a.todiev,   
        a.diapro,   a.fecpcl, a.usrpcl, a.numeta,   
        a.fpagas,   a.mpagno, a.observ, a.coment,   
        a.fecdfi,   a.usrdfi 
     ON ACTION ACCEPT
      EXIT CONSTRUCT 
     ON ACTION CANCEL 
      CALL reg_det.clear()
      RETURN 0
    END CONSTRUCT 
          
    -- Verificando grupo de usuario 
    LET FiltroSede = NULL
    IF GrupoUsuario!=AdmonNotariado THEN
       LET FiltroSede = " AND a.city_num IN (",SedeUsuario,",",SedeAlterna,")"
    END IF
 ELSE
    LET condicion = " 1=1 "
    -- Verificando grupo de usuario 
    LET FiltroSede = NULL
    IF GrupoUsuario!=AdmonNotariado THEN
       LET FiltroSede = " AND a.city_num IN (",SedeUsuario,",",SedeAlterna,")"
    END IF
 END IF

 --Armar la consulta
 LET consulta = 
    "SELECT a.numexp,a.numesc,a.codpro,p.nombre,a.numcre,a.tipdoc,t.nombre,",
           "trim(r.city_name)||'-'||trim(r.city_country),",
           "a.numres,a.nomcli,a.valcre,NVL(e.nometa,'NO DEFINIDA') ",
    " FROM  notariado a, programas_not p,tiposdocumento t,outer etapas_not e,",
           "city r ",
    " WHERE ", condicion,
    FiltroSede,
    "   AND p.codpro   = a.codpro ", 
    "   AND t.tipdoc   = a.tipdoc ",
    "   AND e.numeta   = a.numeta ",
    "   AND r.city_num = a.city_num ",
    " ORDER BY 8, YEAR(a.fecesc) DESC, a.numesc DESC " -- 7,1"
 display "Consutalta ========> ", consulta 
   
 --definir cursor con consulta de BD
 DECLARE curDet CURSOR FROM consulta
 CALL reg_det.clear()
 CLEAR FORM 
 LET x = 0

 --Llenar el arreglo con el resultado de la consulta
 FOREACH curDet INTO rDet.*
  LET x = x + 1 
  LET reg_det [x].* = rDet.*  
 END FOREACH
 RETURN x --Cantidad de registros
END FUNCTION
