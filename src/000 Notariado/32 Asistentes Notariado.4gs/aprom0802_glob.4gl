-- Definicion de variables globales

SCHEMA aprojusa

GLOBALS 
 CONSTANT prog_name = "aprom0802"
 CONSTANT titulo1    = "Asistentes Notariado" 
  
 TYPE 
  tDet RECORD
   grpId              LIKE asistentes_nt.codasn,
   grpNombre          LIKE asistentes_nt.nombre
 END RECORD

 DEFINE
  reg_det             DYNAMIC ARRAY OF tDet, 
  g_reg, u_reg tDet,
  dbname              STRING,
  condicion           STRING

 DEFINE reg_det_attr  DYNAMIC ARRAY OF RECORD 
  grpid               STRING,
  grpnombre           STRING
 END RECORD

END GLOBALS