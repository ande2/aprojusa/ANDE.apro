-- Insertando datos Asistentes Notariado 

GLOBALS "aprom0802_glob.4gl"

FUNCTION insert_init()
 DEFINE strSql STRING 

 LET strSql =
  "INSERT INTO asistentes_nt (",
  "codasn, nombre ",
  ") ",
  "VALUES (?,?)"

 PREPARE st_insertar FROM strSql
END FUNCTION 

FUNCTION ingreso()
 CALL encabezado("Ingresar")
 IF captura_datos('I') THEN
    RETURN grabar()
 ELSE 
    RETURN FALSE 
 END IF 
END FUNCTION 

FUNCTION grabar()
 LET g_reg.grpid = 0
 TRY 
  SELECT NVL(MAX(a.codasn),0)+1
   INTO  g_reg.grpid
   FROM  asistentes_nt a
   
  EXECUTE st_insertar USING g_reg.grpid, g_reg.grpNombre
 CATCH 
  CALL msgError(sqlca.sqlcode,"Grabar Registro")
  RETURN FALSE 
 END TRY
 DISPLAY BY NAME g_reg.*
 CALL box_valdato ("Registro agregado")
 RETURN TRUE
END FUNCTION 
