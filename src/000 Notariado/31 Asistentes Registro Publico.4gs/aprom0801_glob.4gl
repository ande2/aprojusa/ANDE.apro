-- Definicion de variables globales

SCHEMA aprojusa

GLOBALS 
 CONSTANT prog_name = "aprom0801"
 CONSTANT titulo1    = "Asistentes Registro Publico" 
  
 TYPE 
  tDet RECORD
   grpId              LIKE asistentes_rp.codarp,
   grpNombre          LIKE asistentes_rp.nombre
 END RECORD

 DEFINE
  reg_det             DYNAMIC ARRAY OF tDet, 
  g_reg, u_reg tDet,
  dbname              STRING,
  condicion           STRING

 DEFINE reg_det_attr  DYNAMIC ARRAY OF RECORD 
  grpid               STRING,
  grpnombre           STRING
 END RECORD

END GLOBALS