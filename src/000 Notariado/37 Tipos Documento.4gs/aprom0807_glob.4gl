-- Definicion de variables globales

SCHEMA aprojusa

GLOBALS 
 CONSTANT prog_name = "aprom0807"
 CONSTANT titulo1    = "Tipos Documento" 
  
 TYPE 
  tDet RECORD
   tipdoc             LIKE tiposdocumento.tipdoc,
   nombre             LIKE tiposdocumento.nombre
 END RECORD

 TYPE 
  fDet RECORD
   ftipdoc            LIKE tiposdocumento.tipdoc,
   fnombre            LIKE tiposdocumento.nombre
 END RECORD

 DEFINE reg_det_attr  DYNAMIC ARRAY OF RECORD 
  ftipdoc             STRING,
  fnombre             STRING
 END RECORD

 DEFINE g_reg,u_reg   tDet
 DEFINE reg_det       DYNAMIC ARRAY OF fDet 
 DEFINE dbname        STRING
 DEFINE condicion     STRING
END GLOBALS
