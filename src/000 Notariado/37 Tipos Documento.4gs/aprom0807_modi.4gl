-- Modificando datos Tipos Documento 

GLOBALS "aprom0807_glob.4gl"

FUNCTION update_init()
 DEFINE strSql STRING 

 LET strSql = 
 "UPDATE tiposdocumento ",
 "SET    nombre = ? ",
 "WHERE  tipdoc = ? " 

 PREPARE st_modificar FROM strSql
END FUNCTION 

FUNCTION modifica(nofase SMALLINT)
 CALL encabezado("Modificar")
   
 IF captura_datos('M',nofase) THEN
    RETURN actualizar(nofase)
 ELSE 
    RETURN FALSE 
 END IF 
END FUNCTION 

FUNCTION actualizar(nofase SMALLINT)
 DEFINE i   INTEGER
 
 TRY
  EXECUTE st_modificar 
    USING g_reg.nombre,
          g_reg.tipdoc 
 
 CATCH 
  CALL msgError(sqlca.sqlcode,"Modificar Registro.")
  RETURN FALSE 
 END TRY
 CALL msg("Registro actualizado.")
 RETURN TRUE 
END FUNCTION 

FUNCTION delete_init()
 DEFINE strSql STRING 

 LET strSql =
  "DELETE FROM tiposdocumento ",
  "WHERE tipdoc = ? "
      
 PREPARE st_delete FROM strSql
END FUNCTION 

FUNCTION eliminar()
 DEFINE conteo INTEGER

 -- Chequeando integridad
 SELECT COUNT(*)
  INTO  conteo
  FROM  notariado a
  WHERE a.tipdoc = g_reg.tipdoc 
  IF (conteo>0) THEN
     CALL msg(
     "Tipo de documento ya tiene registros en notariado.\nNo puede eliminarse.")
     RETURN FALSE
  END IF

 -- Confirmando eliminacion 
 IF NOT box_confirma("Esta seguro de eliminar el registro") THEN
    RETURN FALSE
 END IF 

 TRY 
  EXECUTE st_delete USING g_reg.tipdoc 
 CATCH 
  CALL msgError(sqlca.sqlcode,"Eliminar Registro.")
  RETURN FALSE 
 END TRY
 CALL msg("Registro eliminado.")
 RETURN TRUE 
END FUNCTION 
