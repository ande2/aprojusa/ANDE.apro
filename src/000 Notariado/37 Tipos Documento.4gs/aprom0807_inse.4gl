-- Insertando datos Tipos Documento 

GLOBALS "aprom0807_glob.4gl"

FUNCTION insert_init()
 DEFINE strSql STRING 

 LET strSql =
  "INSERT INTO tiposdocumento (",
  "tipdoc,nombre) ",
  "VALUES (?,?)"

 PREPARE st_insertar FROM strSql
END FUNCTION 

FUNCTION ingreso(nofase SMALLINT)
 CALL encabezado("Ingresar")
 IF captura_datos('I',nofase) THEN
    RETURN grabar()
 ELSE 
    RETURN FALSE 
 END IF 
END FUNCTION 

FUNCTION grabar()
 LET g_reg.tipdoc = 0 
 TRY 
  SELECT NVL(MAX(a.tipdoc),0)+1
   INTO  g_reg.tipdoc
   FROM  tiposdocumento a
   
  EXECUTE st_insertar 
  USING g_reg.tipdoc,
        g_reg.nombre   

 CATCH 
  CALL msgError(sqlca.sqlcode,"Grabar Registro.")
  RETURN FALSE 
 END TRY
 DISPLAY BY NAME g_reg.*
 CALL box_valdato ("Registro agregado.")
 RETURN TRUE
END FUNCTION 
