-- Consultando datos Tipos Documento 

GLOBALS "aprom0807_glob.4gl"

FUNCTION consulta(flagConsulta)
 DEFINE flagConsulta  BOOLEAN 
 DEFINE x             INTEGER
 DEFINE consulta      STRING  
 DEFINE rDet          fDet 

 IF flagConsulta THEN 
    CALL encabezado("Consulta")

    -- Ingreso de datos de la consulta 
    CONSTRUCT BY NAME condicion 
     ON a.tipdoc,a.nombre
     ON ACTION ACCEPT
      EXIT CONSTRUCT 
     ON ACTION CANCEL 
      CALL reg_det.clear()
      RETURN 0
    END CONSTRUCT 
 ELSE
    LET condicion = " 1=1 "
 END IF

 --Armar la consulta
 LET consulta = 
    "SELECT a.tipdoc,a.nombre ",
    " FROM  tiposdocumento a ",
    " WHERE ", condicion,
    " ORDER BY 1"
   
 --Definir cursor con consulta de BD
 DECLARE curDet CURSOR FROM consulta
 CALL reg_det.clear()
 LET x = 0

 --Llenar el arreglo con el resultado de la consulta
 FOREACH curDet INTO rDet.*
  LET x = x + 1 
  LET reg_det[x].* = rDet.*  
 END FOREACH
 RETURN x --Cantidad de registros
END FUNCTION
