-- Definicion de variables globales

SCHEMA aprojusa

GLOBALS 
 CONSTANT prog_name = "aprom0804"
 CONSTANT titulo1    = "Programas Notariado" 
  
 TYPE 
  tDet RECORD
   grpId              LIKE programas_not.codpro,
   grpNombre          LIKE programas_not.nombre
 END RECORD

 DEFINE
  reg_det             DYNAMIC ARRAY OF tDet, 
  g_reg, u_reg tDet,
  dbname              STRING,
  condicion           STRING

 DEFINE reg_det_attr  DYNAMIC ARRAY OF RECORD 
  grpid               STRING,
  grpnombre           STRING
 END RECORD

END GLOBALS