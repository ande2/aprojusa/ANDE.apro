-- Insertando datos Tipos Cobro 

GLOBALS "aprom0809_glob.4gl"

FUNCTION insert_init()
 DEFINE strSql STRING 

 LET strSql =
  "INSERT INTO tiposcobro (",
  "tipcob,nomcob,norden) ",
  "VALUES (?,?,?)"

 PREPARE st_insertar FROM strSql
END FUNCTION 

FUNCTION ingreso(nofase SMALLINT)
 CALL encabezado("Ingresar")
 IF captura_datos('I',nofase) THEN
    RETURN grabar()
 ELSE 
    RETURN FALSE 
 END IF 
END FUNCTION 

FUNCTION grabar()
 LET g_reg.tipcob = 0 
 TRY 
  SELECT NVL(MAX(a.tipcob),0)+1
   INTO  g_reg.tipcob
   FROM  tiposcobro a
   
  EXECUTE st_insertar 
  USING g_reg.tipcob,
        g_reg.nomcob,  
        g_reg.norden   

 CATCH 
  CALL msgError(sqlca.sqlcode,"Grabar Registro.")
  RETURN FALSE 
 END TRY
 DISPLAY BY NAME g_reg.*
 CALL box_valdato ("Registro agregado.")
 RETURN TRUE
END FUNCTION 
