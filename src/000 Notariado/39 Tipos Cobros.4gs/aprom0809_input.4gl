-- Ingresando datos Tipos Cobro 

GLOBALS "aprom0809_glob.4gl"

-- Subrutina para ingresar los datos del tipo de cobro 

FUNCTION captura_datos(operacion,nofase)
 DEFINE operacion CHAR (1)
 DEFINE resultado BOOLEAN
 DEFINE nofase    SMALLINT 

 LET resultado = FALSE 
 LET u_reg.* = g_reg.*
 IF operacion = 'I' THEN 
    INITIALIZE g_reg.* TO NULL
    DISPLAY BY NAME g_reg.*
 END IF

 -- Ingresando datos 
 DIALOG ATTRIBUTES(UNBUFFERED)
  INPUT BY NAME g_reg.tipcob,g_reg.nomcob,g_reg.norden 
   ATTRIBUTES (WITHOUT DEFAULTS)

   BEFORE INPUT
    CALL DIALOG.setActionHidden("close",TRUE)
  END INPUT 

  ON ACTION ACCEPT
   -- Verificando nomcob
   IF g_reg.nomcob IS NULL THEN
      CALL msg("Ingrese nombre.")
      NEXT FIELD CURRENT 
   END IF

   -- Verificando norden
   IF g_reg.norden IS NULL THEN
      CALL msg("Ingrese orden.")
      NEXT FIELD CURRENT 
   END IF

   -- Verificando duplicados
   LET g_reg.nomcob = g_reg.nomcob CLIPPED  
   SELECT nomcob FROM tiposcobro
    WHERE tipcob != g_reg.tipcob 
      AND UPPER(nomcob) = UPPER(g_reg.nomcob) 
    IF sqlca.sqlcode = 0 THEN
       CALL msg("Ya existe un registro con este mismo nombre.")
       NEXT FIELD CURRENT 
    END IF 

   -- Verificando cambios
   IF operacion = 'M' AND g_reg.* = u_reg.* THEN
      CALL msg("No se efectuaron cambios.")
      EXIT DIALOG
   END IF 

   -- Confirmando grabacion
   CASE box_gradato("Seguro de grabar")
    WHEN "Si"
     LET resultado = TRUE
     EXIT DIALOG
    WHEN "No"
     EXIT DIALOG 
    OTHERWISE
     CONTINUE DIALOG 
   END CASE 
   LET resultado = TRUE
   EXIT DIALOG 

  ON ACTION CANCEL
   EXIT DIALOG
 END DIALOG
   
 IF NOT resultado THEN
    LET g_reg.* = u_reg.*
    DISPLAY BY NAME g_reg.* 
 END IF 
 RETURN resultado 
END FUNCTION
