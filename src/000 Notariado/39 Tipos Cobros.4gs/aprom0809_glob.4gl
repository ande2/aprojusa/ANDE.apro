-- Definicion de variables globales

SCHEMA aprojusa

GLOBALS 
 CONSTANT prog_name = "aprom0809"
 CONSTANT titulo1    = "Tipos de Cobro" 
  
 TYPE 
  tDet RECORD
   tipcob             LIKE tiposcobro.tipcob,
   nomcob             LIKE tiposcobro.nomcob,
   norden             LIKE tiposcobro.norden 
 END RECORD

 TYPE 
  fDet RECORD
   ftipcob            LIKE tiposcobro.tipcob,
   fnomcob            LIKE tiposcobro.nomcob,
   fnorden            LIKE tiposcobro.norden 
 END RECORD

 DEFINE reg_det_attr  DYNAMIC ARRAY OF RECORD 
  ftipcob             STRING,
  fnomcob             STRING,
  fnorden             STRING
 END RECORD

 DEFINE g_reg,u_reg   tDet
 DEFINE reg_det       DYNAMIC ARRAY OF fDet 
 DEFINE dbname        STRING
 DEFINE condicion     STRING
END GLOBALS
