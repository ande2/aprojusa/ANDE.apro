-- Definicion de variables globales

SCHEMA aprojusa

GLOBALS 
 CONSTANT prog_name = "aprom0803"
 CONSTANT titulo1    = "Registros Que Operan" 
  
 TYPE 
  tDet RECORD
   grpId              LIKE registros_ope.codrgo,
   grpNombre          LIKE registros_ope.nombre
 END RECORD

 DEFINE
  reg_det             DYNAMIC ARRAY OF tDet, 
  g_reg, u_reg tDet,
  dbname              STRING,
  condicion           STRING

 DEFINE reg_det_attr  DYNAMIC ARRAY OF RECORD 
  grpid               STRING,
  grpnombre           STRING
 END RECORD

END GLOBALS
