-- Ingresando datos Asistentes Registro Publico

GLOBALS "aprom0805_glob.4gl"

FUNCTION captura_datos(operacion)
 DEFINE operacion CHAR (1)
 DEFINE resultado BOOLEAN

 LET resultado = FALSE 
 LET u_reg.* = g_reg.*
 IF operacion = 'I' THEN 
    INITIALIZE g_reg.* TO NULL
    DISPLAY BY NAME g_reg.*
 END IF

 -- Ingresando datos 
 DIALOG ATTRIBUTES(UNBUFFERED)
  INPUT BY NAME g_reg.city_num, g_reg.numage, g_reg.nombre, g_reg.nomLug 
   ATTRIBUTES (WITHOUT DEFAULTS)

   BEFORE INPUT
    CALL DIALOG.setActionHidden("close",TRUE)

   AFTER FIELD city_num
    IF g_reg.city_num IS NULL THEN
       CALL msg("Ingrese región")
       NEXT FIELD CURRENT  
    END IF 

       
  END INPUT 

  ON ACTION ACCEPT
   -- Verificando nombre
   IF g_reg.city_num IS NULL THEN
      CALL msg("Ingrese región")
      NEXT FIELD CURRENT 
   END IF

   -- Verificando duplicados
   {LET g_reg.grpnombre = g_reg.grpnombre CLIPPED  
   SELECT nombre FROM asistentes_rp
    WHERE UPPER(nombre) = UPPER(g_reg.grpnombre) 
    IF sqlca.sqlcode = 0 THEN
       CALL msg("Ya existe un registro con este mismo nombre")
       NEXT FIELD CURRENT 
     END IF 
}
   -- Verificando cambios
   IF operacion = 'M' AND g_reg.* = u_reg.* THEN
      CALL msg("No se efectuaron cambios")
      EXIT DIALOG 
   END IF

   --  Confirmando grabacion
   CASE box_gradato("Seguro de grabar")
    WHEN "Si"
     LET resultado = TRUE
     EXIT DIALOG
    WHEN "No"
     EXIT DIALOG 
    OTHERWISE
     CONTINUE DIALOG 
   END CASE 
   LET resultado = TRUE
   EXIT DIALOG 

  ON ACTION CANCEL
   EXIT DIALOG
 END DIALOG
   
 IF NOT resultado THEN
    LET g_reg.* = u_reg.*
    DISPLAY BY NAME g_reg.* 
  END IF 
  RETURN resultado 
END FUNCTION