-- Modificando datos Asistentes Registro Publico

GLOBALS "aprom0805_glob.4gl"

FUNCTION update_init()
 DEFINE strSql STRING 

 LET strSql = 
 "UPDATE agencias_bco ",
 "SET ",
 " nombre    = ?, ",
 " nomlug    = ? ",
 " WHERE city_num = ? ",
 " AND numage = ? " 

 PREPARE st_modificar FROM strSql
END FUNCTION 

FUNCTION modifica()
 CALL encabezado("Modificar")
   
 IF captura_datos('M') THEN
    RETURN actualizar()
 ELSE 
    RETURN FALSE 
 END IF 
END FUNCTION 

FUNCTION actualizar()
 TRY
  EXECUTE st_modificar USING g_reg.nombre, g_reg.nomlug, u_reg.city_num, u_reg.numAge
 CATCH 
  CALL msgError(sqlca.sqlcode,"Modificar Registro")
  RETURN FALSE 
 END TRY
 CALL msg("Registro actualizado")
 RETURN TRUE 
END FUNCTION 

FUNCTION delete_init()
 DEFINE strSql STRING 

 LET strSql =
  "DELETE FROM agencias_bco ",
 " WHERE city_num = ? ",
 " AND numage = ? " 
     
 PREPARE st_delete FROM strSql
END FUNCTION 

FUNCTION eliminar()
 IF NOT box_confirma("Esta seguro de eliminar el registro") THEN
    RETURN FALSE
 END IF 

 TRY 
  EXECUTE st_delete USING g_reg.city_num, g_reg.numAge
 CATCH 
  CALL msgError(sqlca.sqlcode,"Eliminar Registro")
  RETURN FALSE 
 END TRY
 CALL msg("Registro eliminado")
 RETURN TRUE 
END FUNCTION 
