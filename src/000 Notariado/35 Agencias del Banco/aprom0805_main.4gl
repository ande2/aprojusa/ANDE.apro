-- Mantenimiento de Asistentes Registro Publico

GLOBALS "aprom0805_glob.4gl"
MAIN
 DEFINE n_param 	SMALLINT,
		prog_name2 	STRING
		
 DEFER INTERRUPT

 OPTIONS INPUT  WRAP,
		 HELP KEY CONTROL-W,
		 COMMENT LINE OFF,
		 PROMPT LINE LAST - 2,
		 MESSAGE LINE LAST - 1,
		 ERROR LINE LAST

 LET n_param = num_args()

 CONNECT TO "aprojusa"

 LET prog_name2 = prog_name||".log"   
 CALL STARTLOG(prog_name2)
 CALL main_init()
END MAIN

FUNCTION main_init()
 DEFINE nom_forma 	STRING,
        w           ui.WINDOW,
        f           ui.FORM
	  
 INITIALIZE u_reg.* TO NULL
 LET g_reg.* = u_reg.*

 CALL ui.Interface.loadActionDefaults("actiondefaults")
 CALL ui.Interface.loadStyles("styles_sc")
 LET nom_forma = prog_name CLIPPED, "_form"
 CLOSE WINDOW SCREEN 

 OPEN WINDOW w1 WITH FORM nom_forma
 CALL fgl_settitle("ANDE - "||titulo1 CLIPPED)

 LET w = ui.WINDOW.getcurrent()
 LET f = w.getForm()
	
 CALL insert_init()
 CALL update_init()
 CALL delete_init()
 CALL combo_init()
 CALL main_menu()
END FUNCTION                                                                    

FUNCTION main_menu()                               
 DEFINE  cuantos,id,ids	   SMALLINT,
         cnt 		       SMALLINT  
    
 LET cnt = 1
 DISPLAY UPSHIFT(titulo1) TO gtit_enc
    
 DISPLAY ARRAY reg_det TO sDet.*
  ATTRIBUTE(ACCEPT=FALSE,CANCEL=FALSE,UNBUFFERED)
   
  BEFORE DISPLAY
   LET cuantos = consulta(FALSE)
   IF cuantos > 0 THEN 
     CALL dialog.setCurrentRow("sdet",1)
     LET g_reg.* = reg_det[1].*
     DISPLAY BY NAME g_reg.*
     CALL setAttr(1)
     CALL DIALOG.setCellAttributes(reg_det_attr)
   END IF 
   CALL encabezado("")
         
  BEFORE ROW 
   LET id = arr_curr()
   IF id > 0 THEN 
      LET g_reg.* = reg_det[id].*
      DISPLAY BY NAME g_reg.*
      CALL setAttr(id)
      CALL DIALOG.setCellAttributes(reg_det_attr)
   END IF 
      
  ON ACTION buscar
   LET cuantos = consulta(TRUE)
   IF cuantos > 0 THEN 
      LET g_reg.* = reg_det[1].*
      DISPLAY BY NAME g_reg.*
   END IF 
   CALL encabezado("")

  ON ACTION agregar
   IF ingreso() THEN 
      LET cuantos = consulta(FALSE)
      CALL fgl_set_arr_curr( arr_count() + 1 )

      --Refrescar Pantalla
      DISPLAY ARRAY reg_det TO sDet.*
       BEFORE DISPLAY  EXIT DISPLAY 
      END DISPLAY 
   END IF
   CALL encabezado("")
   
  ON ACTION modificar
   LET id = arr_curr()
   LET ids = scr_line()
   IF id > 0 THEN 
    IF modifica() THEN
       LET reg_det[id].* = g_reg.*
       DISPLAY reg_det[id].* TO sDet[ids].*
    END IF   
   END IF 
   CALL encabezado("")
   
  ON ACTION eliminar
   LET id = arr_curr()
   LET ids = scr_line()         
   IF id > 0 THEN 
      IF eliminar() THEN
         CALL DIALOG.deleteRow("sdet", id)
         IF id = arr_count() THEN 
            LET id = id - 1
         END IF 
         IF id > 0 THEN 
            LET g_reg.* = reg_det[id].*
         ELSE 
            INITIALIZE g_reg.* TO NULL
         END IF 
         DISPLAY BY NAME g_reg.*
      END IF   
   END IF 

  ON ACTION salir
   EXIT DISPLAY 
 END DISPLAY 
END FUNCTION

FUNCTION encabezado(gtit_enc)
 DEFINE gtit_enc STRING 

 DISPLAY BY NAME  gtit_enc
END FUNCTION 

FUNCTION setAttr(id)
 DEFINE  i,id SMALLINT

 FOR i=1 TO reg_det.getLength()
  LET reg_det_attr[i].city_num   = "black"
  LET reg_det_attr[i].numage     = "black"
  LET reg_det_attr[i].nombre     = "black"
  LET reg_det_attr[i].nomlug     = "black"
 END FOR  

 LET reg_det_attr[id].city_num   = "blue reverse"
 LET reg_det_attr[id].numage     = "blue reverse"
 LET reg_det_attr[id].nombre     = "blue reverse"
 LET reg_det_attr[id].nomlug     = "blue reverse"
END FUNCTION

FUNCTION combo_init() 
   CALL combo_din2("city_num","SELECT city_num, city_name FROM city")
   CALL combo_din2("cmbcity","SELECT city_num, city_name FROM city")
END FUNCTION   