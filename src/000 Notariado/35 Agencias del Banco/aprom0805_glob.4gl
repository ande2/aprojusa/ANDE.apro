-- Definicion de variables globales

SCHEMA aprojusa

GLOBALS 
 CONSTANT prog_name = "aprom0805"
 CONSTANT titulo1    = "Agencias" 
  
 TYPE 
  tDet RECORD
   city_num          LIKE agencias_bco.city_num,
   numAge            LIKE agencias_bco.numage,
   nombre            LIKE agencias_bco.nombre,
   nomLug            LIKE agencias_bco.nomlug
 END RECORD

 DEFINE
  reg_det             DYNAMIC ARRAY OF tDet, 
  g_reg, u_reg tDet,
  dbname              STRING,
  condicion           STRING

 DEFINE reg_det_attr  DYNAMIC ARRAY OF RECORD 
  city_num           STRING,
  numAge             STRING,
  nombre             STRING,
  nomLug             STRING
 END RECORD

END GLOBALS