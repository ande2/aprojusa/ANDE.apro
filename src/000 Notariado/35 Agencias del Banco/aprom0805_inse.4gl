-- Insertando datos Asistentes Registro Publico

GLOBALS "aprom0805_glob.4gl"

FUNCTION insert_init()
 DEFINE strSql STRING 

 LET strSql =
  "INSERT INTO agencias_bco (",
  "city_num, numage, nombre, nomlug ",
  ") ",
  "VALUES (?,?,?,?)"

 PREPARE st_insertar FROM strSql
END FUNCTION 

FUNCTION ingreso()
 CALL encabezado("Ingresar")
 IF captura_datos('I') THEN
    RETURN grabar()
 ELSE 
    RETURN FALSE 
 END IF 
END FUNCTION 

FUNCTION grabar()
 --LET g_reg.grpid = 0
 TRY 
  {SELECT NVL(MAX(a.codarp),0)+1
   INTO  g_reg.grpid
   FROM  asistentes_rp a}
   
  EXECUTE st_insertar USING g_reg.city_num, g_reg.numage, g_reg.nombre, g_reg.nomLug
 CATCH 
  CALL msgError(sqlca.sqlcode,"Grabar Registro")
  RETURN FALSE 
 END TRY
 DISPLAY BY NAME g_reg.*
 CALL box_valdato ("Registro agregado")
 RETURN TRUE
END FUNCTION 