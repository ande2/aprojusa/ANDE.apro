-- Ingresando datos de Grupos de Tipos de Documento 

GLOBALS "aprom0808_glob.4gl"

-- Subrutina para ingresar los datos del grupo de tipos de documento 

FUNCTION captura_datos(operacion,nofase)
 DEFINE operacion CHAR (1)
 DEFINE resultado BOOLEAN
 DEFINE nofase    SMALLINT 

 LET resultado = FALSE 
 LET u_reg.* = g_reg.*
 IF operacion = 'I' THEN 
    INITIALIZE g_reg.* TO NULL
    DISPLAY BY NAME g_reg.*
 END IF

 -- Ingresando datos 
 DIALOG ATTRIBUTES(UNBUFFERED)
  INPUT BY NAME g_reg.codgru,g_reg.nombre
   ATTRIBUTES (WITHOUT DEFAULTS)

   BEFORE INPUT
    CALL DIALOG.setActionHidden("close",TRUE)
  END INPUT 

  ON ACTION ACCEPT
   -- Verificando nombre
   IF g_reg.nombre IS NULL THEN
      CALL msg("Ingrese nombre")
      NEXT FIELD CURRENT 
   END IF

   -- Verificando duplicados
   LET g_reg.nombre = g_reg.nombre CLIPPED  
   SELECT nombre FROM grupostiposdoc
    WHERE codgru != g_reg.codgru 
      AND UPPER(nombre) = UPPER(g_reg.nombre) 
    IF sqlca.sqlcode = 0 THEN
       CALL msg("Ya existe un registro con este mismo nombre.")
       NEXT FIELD CURRENT 
    END IF 

   -- Verificando cambios
   IF operacion = 'M' AND g_reg.* = u_reg.* THEN
      CALL msg("No se efectuaron cambios.")
      EXIT DIALOG
   END IF 

   -- Confirmando grabacion
   CASE box_gradato("Seguro de grabar")
    WHEN "Si"
     LET resultado = TRUE
     EXIT DIALOG
    WHEN "No"
     EXIT DIALOG 
    OTHERWISE
     CONTINUE DIALOG 
   END CASE 
   LET resultado = TRUE
   EXIT DIALOG 

  ON ACTION CANCEL
   EXIT DIALOG
 END DIALOG
   
 IF NOT resultado THEN
    LET g_reg.* = u_reg.*
    DISPLAY BY NAME g_reg.* 
 END IF 
 RETURN resultado 
END FUNCTION

-- Subrutina para ingresar los tipos de documento del grupo  

FUNCTION CapturaTiposDocumento()
 DEFINE gtipdct   LIKE tiposdocumento.tipdoc,
        grabar,i  SMALLINT,
        loop      SMALLINT,
        conteo    INTEGER  

 -- Ingresando datos
 LET loop = TRUE 
 WHILE loop
  -- Inicializando datos
  LET grabar  = FALSE 
  LET gtipdct = NULL

  -- Ingresando tipos de documento 
  INPUT BY NAME gtipdct WITHOUT DEFAULTS
   ON ACTION accept ATTRIBUTE (TEXT="Grabar",IMAGE="grabar") 
    -- Verificando si hay tipos de documento 
    IF v_tipdocs.getLength()<=0 THEN 
       CALL msg("No existen ningun tipo de documento ingresado.")
       CONTINUE INPUT 
    END IF

    -- Confirmando grabacion
    LET grabar = FALSE 
    CASE box_gradato("Seguro de grabar")
     WHEN "Si"
      LET loop   = FALSE 
      LET grabar = TRUE
      EXIT INPUT 
     WHEN "No"
      LET loop   = FALSE 
      EXIT INPUT  
     OTHERWISE
      CONTINUE INPUT  
    END CASE 

   ON ACTION cancel
    LET loop = FALSE 
    EXIT INPUT 
 
   ON ACTION modificar ATTRIBUTE(TEXT="Modificar")    
    -- Verificando si hay tipos de documento
    IF v_tipdocs.getLength()<=0 THEN 
       CALL msg("No existe ningun tipo de documento registrado.")
       CONTINUE INPUT 
    END IF

    -- Modificando tipos de documento
    CALL ModificaTiposDocumento() 

   AFTER FIELD gtipdct 
    -- Verificando ingreso de valores 
    IF gtipdct IS NULL THEN
       CALL msg("Seleccionar tipo de documento.") 
       NEXT FIELD gtipdct 
    END IF

    -- Verificando si tipo de documento ya existe asociado a otro grupo
    SELECT COUNT(*)
     INTO  conteo
     FROM  grupostiposdoc_det a
     WHERE a.codgru != g_reg.codgru
       AND a.tipdoc  = gtipdct 
    IF (conteo>0) THEN
       CALL msg("Tipo de documento ya asociado a otro grupo."||
                "\nAsociacion no permitida.")
       NEXT FIELD gtipdct 
    END IF 

    -- Verificando si ya existe tipo de documento 
    IF BuscaTipoDocumento(gtipdct) THEN
       CALL msg("Tipo de documento ya existe registrado.")
       NEXT FIELD gtipdct 
    END IF

    -- Llenando lista
    CALL LlenaListaTiposDocumento(gtipdct)
    EXIT INPUT 
  END INPUT 
 END WHILE 

 -- Verificando si se graban tipos de documento
 IF grabar THEN
    -- Borrando antes de grabar
    DELETE FROM grupostiposdoc_det 
    WHERE grupostiposdoc_det.codgru = g_reg.codgru 

    -- Grabando tipos de documento 
    FOR i = 1 TO v_tipdocs.getLength()
     IF v_tipdocs[i].gcodgru IS NULL OR 
        v_tipdocs[i].gtipdoc IS NULL OR
        v_tipdocs[i].gnomdoc IS NULL THEN
        CONTINUE FOR
     END IF 

     -- Grabando
     INSERT INTO grupostiposdoc_det
     VALUES (v_tipdocs[i].gcodgru,
             v_tipdocs[i].gtipdoc,
             i) 
    END FOR 
 END IF 
END FUNCTION 

-- Subrutina para buscar si ya existe en el vector el tipo de documento 

FUNCTION BuscaTipoDocumento(gtipdoc) 
 DEFINE gtipdoc   LIKE tiposdocumento.tipdoc, 
        i,existe  SMALLINT

 LET existe = FALSE 
 FOR i = 1 TO v_tipdocs.getLength()
  IF v_tipdocs[i].gtipdoc IS NULL THEN
     CONTINUE FOR
  END IF

  IF (v_tipdocs[i].gtipdoc=gtipdoc) THEN
     LET existe = TRUE
     EXIT FOR
  END IF 
 END FOR
 RETURN existe 
END FUNCTION 

-- Subrutina para llenar el vector de tipos de documento

FUNCTION LlenaListaTiposDocumento(gtipdoc) 
 DEFINE gtipdoc   LIKE tiposdocumento.tipdoc, 
        idx       SMALLINT

 LET idx = v_tipdocs.getLength()+1 
 LET v_tipdocs[idx].gtipdoc = gtipdoc 
 LET v_tipdocs[idx].gcodgru = g_reg.codgru 

 -- Obteniendo nombre del tipo de cobro 
 SELECT NVL(a.nombre,"NO REGISTRADO") 
  INTO v_tipdocs[idx].gnomdoc 
  FROM tiposdocumento a 
  WHERE a.tipdoc = gtipdoc 

 -- Desplegando datos 
 DISPLAY ARRAY v_tipdocs TO sTipdocs.* 
  BEFORE DISPLAY
   EXIT DISPLAY
 END DISPLAY 
END FUNCTION 

-- Subrutina para modificar los tipos de documento 

FUNCTION ModificaTiposDocumento() 
 DEFINE arr INTEGER 

 -- Modificando datos del tipo de documento
 DISPLAY ARRAY v_tipdocs TO sTipdocs.*
  ATTRIBUTE (ACCEPT=FALSE,CANCEL=FALSE) 

  ON ACTION regresar ATTRIBUTE (TEXT="Regresar",IMAGE="cancel") 
   -- Salida 
   EXIT DISPLAY 

  ON ACTION delete 
   -- Eliminando tipo de documento 
   LET arr = ARR_CURR() 

   IF box_confirma("Esta seguro de eliminar la linea.") THEN
      CALL v_tipdocs.deleteElement(arr)
   END IF 

  BEFORE DISPLAY 
   LET arr = ARR_CURR() 
   CALL setAttrTiposDocumento(1)
   CALL DIALOG.setCellAttributes(v_tipdoc_attr) 

  BEFORE ROW 
   LET arr = ARR_CURR() 
   CALL setAttrTiposDocumento(arr)
   CALL DIALOG.setCellAttributes(v_tipdoc_attr) 
 END DISPLAY 
END FUNCTION 

-- Subrutina para desplegar las lineas del vector tipos de documento 

FUNCTION setAttrTiposDocumento(id)
 DEFINE i,id SMALLINT

 FOR i=1 TO v_tipdocs.getLength()
  LET v_tipdoc_attr[i].gcodgru = "black"
  LET v_tipdoc_attr[i].gtipdoc = "black"
  LET v_tipdoc_attr[i].gnomdoc = "black"
 END FOR

 LET v_tipdoc_attr[id].gcodgru = "teal reverse"
 LET v_tipdoc_attr[id].gtipdoc = "teal reverse"
 LET v_tipdoc_attr[id].gnomdoc = "teal reverse"
END FUNCTION
