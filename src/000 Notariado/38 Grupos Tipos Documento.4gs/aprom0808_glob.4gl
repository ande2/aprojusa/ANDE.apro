-- Definicion de variables globales

SCHEMA aprojusa

GLOBALS 
 CONSTANT prog_name    = "aprom0808"
 CONSTANT titulo1      = "Grupos de Tipos de Documento" 
 CONSTANT TIPVALHONORS = 1
 CONSTANT TIPVALGASTOS = 2
  
 TYPE 
  tDet RECORD
   codgru             LIKE grupostiposdoc.codgru,
   nombre             LIKE grupostiposdoc.nombre
 END RECORD

 TYPE 
  fDet RECORD
   fcodgru            LIKE grupostiposdoc.codgru,
   fnombre            LIKE grupostiposdoc.nombre
 END RECORD 

 DEFINE reg_det_attr  DYNAMIC ARRAY OF RECORD 
  fcodgru             STRING,
  fnombre             STRING
 END RECORD

 TYPE
  dDet RECORD
   gcodgru            LIKE grupostiposdoc.codgru, 
   gtipdoc            LIKE tiposdocumento.tipdoc,
   gnomdoc            LIKE tiposdocumento.nombre 
 END RECORD 

 DEFINE v_tipdoc_attr DYNAMIC ARRAY OF RECORD 
  gcodgru             STRING,
  gtipdoc             STRING,
  gnomdoc             STRING
 END RECORD

 TYPE
  cDet RECORD
   hotipcob           LIKE grupostiposdoc_cob.tipcob,
   honomcob           LIKE tiposcobro.nomcob, 
   horangos           LIKE grupostiposdoc_cob.rangos,
   hovalfij           LIKE grupostiposdoc_cob.valfij, 
   hovalcob           LIKE grupostiposdoc_cob.valcob,
   hoporcen           LIKE grupostiposdoc_cob.porcen,
   hovalmax           LIKE grupostiposdoc_cob.valmax,
   hoingcan           LIKE grupostiposdoc_cob.ingcan,
   hosumsub           LIKE grupostiposdoc_cob.sumsub,
   homulcx1           LIKE grupostiposdoc_cob.mulcx1,
   hocaliva           LIKE grupostiposdoc_cob.caliva,
   hovalsum           LIKE grupostiposdoc_cob.valsum,
   hovalres           LIKE grupostiposdoc_cob.valres 
 END RECORD

 TYPE
  cDetga RECORD
   gatipcob           LIKE grupostiposdoc_cob.tipcob,
   ganomcob           LIKE tiposcobro.nomcob, 
   garangos           LIKE grupostiposdoc_cob.rangos,
   gavalfij           LIKE grupostiposdoc_cob.valfij, 
   gavalcob           LIKE grupostiposdoc_cob.valcob,
   gaporcen           LIKE grupostiposdoc_cob.porcen,
   gavalmax           LIKE grupostiposdoc_cob.valmax,
   gaingcan           LIKE grupostiposdoc_cob.ingcan,
   gasumsub           LIKE grupostiposdoc_cob.sumsub,
   gamulcx1           LIKE grupostiposdoc_cob.mulcx1,
   gacaliva           LIKE grupostiposdoc_cob.caliva,
   gavalsum           LIKE grupostiposdoc_cob.valsum,
   gavalres           LIKE grupostiposdoc_cob.valres 
 END RECORD

 TYPE
  cDetfa RECORD
   fanofase           LIKE grupostiposdoc_fas.nofase,
   fanomfas           LIKE fases_not.nomfas, 
   fadiamin           LIKE grupostiposdoc_fas.diamin,
   fadiarea           LIKE grupostiposdoc_fas.diarea, 
   fadiamax           LIKE grupostiposdoc_fas.diamax
 END RECORD

 TYPE
  hDet RECORD
   htcodgru           LIKE grupostiposdoc_ran.codgru,
   httipval           LIKE grupostiposdoc_ran.tipval,
   httipcob           LIKE grupostiposdoc_ran.tipcob,
   htvalini           LIKE grupostiposdoc_ran.valini,
   htvalfin           LIKE grupostiposdoc_ran.valfin,
   htvalran           LIKE grupostiposdoc_ran.valran,
   htporran           LIKE grupostiposdoc_ran.porran,
   htvalres           LIKE grupostiposdoc_ran.valres,
   htnumcor           LIKE grupostiposdoc_ran.numcor 
 END RECORD

 TYPE
  gDet RECORD
   gtcodgru           LIKE grupostiposdoc_ran.codgru,
   gttipval           LIKE grupostiposdoc_ran.tipval,
   gttipcob           LIKE grupostiposdoc_ran.tipcob,
   gtvalini           LIKE grupostiposdoc_ran.valini,
   gtvalfin           LIKE grupostiposdoc_ran.valfin,
   gtvalran           LIKE grupostiposdoc_ran.valran,
   gtporran           LIKE grupostiposdoc_ran.porran,
   gtvalres           LIKE grupostiposdoc_ran.valres,
   gtnumcor           LIKE grupostiposdoc_ran.numcor 
 END RECORD

 TYPE
  hoDet RECORD
   htipcob            LIKE grupostiposdoc_cob.tipcob,
   hnomcob            LIKE tiposcobro.nomcob, 
   hrangos            LIKE grupostiposdoc_cob.rangos,
   hvalfij            LIKE grupostiposdoc_cob.valfij, 
   hvalcob            LIKE grupostiposdoc_cob.valcob,
   hporcen            LIKE grupostiposdoc_cob.porcen,
   hvalmax            LIKE grupostiposdoc_cob.valmax,
   hingcan            LIKE grupostiposdoc_cob.ingcan,
   hsumsub            LIKE grupostiposdoc_cob.sumsub,
   hmulcx1            LIKE grupostiposdoc_cob.mulcx1, 
   hcaliva            LIKE grupostiposdoc_cob.caliva,
   hvalsum            LIKE grupostiposdoc_cob.valsum,
   hvalres            LIKE grupostiposdoc_cob.valres 
 END RECORD

 TYPE
  gaDet RECORD
   gtipcob            LIKE grupostiposdoc_cob.tipcob,
   gnomcob            LIKE tiposcobro.nomcob, 
   grangos            LIKE grupostiposdoc_cob.rangos,
   gvalfij            LIKE grupostiposdoc_cob.valfij, 
   gvalcob            LIKE grupostiposdoc_cob.valcob,
   gporcen            LIKE grupostiposdoc_cob.porcen,
   gvalmax            LIKE grupostiposdoc_cob.valmax,
   gingcan            LIKE grupostiposdoc_cob.ingcan,
   gsumsub            LIKE grupostiposdoc_cob.sumsub,
   gmulcx1            LIKE grupostiposdoc_cob.mulcx1, 
   gcaliva            LIKE grupostiposdoc_cob.caliva,
   gvalsum            LIKE grupostiposdoc_cob.valsum,
   gvalres            LIKE grupostiposdoc_cob.valres 
 END RECORD

 TYPE
  faDet RECORD
   fnofase            LIKE grupostiposdoc_fas.nofase,
   fnomfas            LIKE fases_not.nomfas,  
   fdiamin            LIKE grupostiposdoc_fas.diamin,
   fdiarea            LIKE grupostiposdoc_fas.diarea, 
   fdiamax            LIKE grupostiposdoc_fas.diamax 
 END RECORD

 DEFINE vho_cobros_attr DYNAMIC ARRAY OF RECORD
  hotipcob            STRING,
  honomcob            STRING,
  horangos            STRING,
  hovalfij            STRING,
  hovalcob            STRING,
  hoporcen            STRING,
  hovalmax            STRING,
  hoingcan            STRING,
  hosumsub            STRING,
  homulcx1            STRING, 
  hocaliva            STRING,
  hovalsum            STRING,
  hovalres            STRING 
 END RECORD 

 DEFINE vga_cobros_attr DYNAMIC ARRAY OF RECORD
  gatipcob            STRING,
  ganomcob            STRING,
  garangos            STRING,
  gavalfij            STRING,
  gavalcob            STRING,
  gaporcen            STRING,
  gavalmax            STRING,
  gaingcan            STRING,
  gasumsub            STRING,
  gamulcx1            STRING, 
  gacaliva            STRING,
  gavalsum            STRING,
  gavalres            STRING 
 END RECORD 

 DEFINE vga_fases_attr DYNAMIC ARRAY OF RECORD
  fanofase            STRING,
  fanomfas            STRING,
  fadiamin            STRING,
  fadiarea            STRING,
  fadiamax            STRING
 END RECORD 

 DEFINE g_reg,u_reg   tDet
 DEFINE reg_det       DYNAMIC ARRAY OF fDet 
 DEFINE vho_cobros    DYNAMIC ARRAY OF cDet 
 DEFINE v_honors      DYNAMIC ARRAY OF hDet 
 DEFINE vga_cobros    DYNAMIC ARRAY OF cDetga 
 DEFINE v_gastos      DYNAMIC ARRAY OF gDet 
 DEFINE v_tipdocs     DYNAMIC ARRAY OF dDet 
 DEFINE vga_fases     DYNAMIC ARRAY OF cDetfa 
 DEFINE dbname        STRING
 DEFINE condicion     STRING
END GLOBALS
