-- Mantenimiento de Grupos de Tipos de Documento 

GLOBALS "aprom0808_glob.4gl"
MAIN
 DEFINE n_param    SMALLINT,
	prog_name2 STRING
		
 DEFER INTERRUPT

 OPTIONS INPUT  WRAP,
		 HELP KEY CONTROL-W,
		 COMMENT LINE OFF,
		 PROMPT LINE LAST - 2,
		 MESSAGE LINE LAST - 1,
		 ERROR LINE LAST

 LET n_param = num_args()

 CONNECT TO "aprojusa"

 LET prog_name2 = prog_name||".log"   
 CALL STARTLOG(prog_name2)
 CALL main_init()
END MAIN

FUNCTION main_init()
 DEFINE nom_forma   STRING,
        w           ui.WINDOW,
        f           ui.FORM
	  
 INITIALIZE u_reg.* TO NULL
 LET g_reg.* = u_reg.*

 CALL ui.Interface.loadActionDefaults("actiondefaults")
 CALL ui.Interface.loadStyles("styles_is")
 LET nom_forma = prog_name CLIPPED, "_form"
 CLOSE WINDOW SCREEN 

 OPEN WINDOW w1 WITH FORM nom_forma
 CALL fgl_settitle("ANDE - "||titulo1 CLIPPED)

 LET w = ui.WINDOW.getcurrent()
 LET f = w.getForm()
	
 CALL insert_init()
 CALL update_init()
 CALL delete_init()
 CALL combo_init()
 CALL main_menu()
END FUNCTION                                                                    

FUNCTION main_menu()                               
 DEFINE  xcodigo              LIKE tiposdocumento.tipdoc,
         xnombre              LIKE tiposdocumento.nombre,
         ho_ppd               hoDet,  
         ga_ppd               gaDet,  
         cuantos,id,ids	      SMALLINT,
         cnt,regreso,arr,scr  SMALLINT, 
         aceptar              SMALLINT,
         arrt,arrh,arrg,arrf  INTEGER, 
         w                    ui.WINDOW,
         f                    ui.FORM

 LET w = ui.WINDOW.getcurrent()
 LET f = w.getForm()

 LET cnt = 1
 DISPLAY UPSHIFT(titulo1) TO gtit_enc

 -- Creando tabla temporal
 CREATE TEMP TABLE tmp_ranhon
 (
  codgru SMALLINT,
  tipval SMALLINT, 
  tipcob SMALLINT,
  valini DEC(10,2), 
  valfin DEC(10,2), 
  valran DEC(10,2), 
  porran DEC(9,6),
  valres DEC(10,2),
  numcor SMALLINT 
 ) 

 -- Desplegando datos
 DIALOG ATTRIBUTES(UNBUFFERED,FIELD ORDER FORM)

  -- Desplegando movimientos
  DISPLAY ARRAY reg_det TO sDet.*
   
   BEFORE DISPLAY
    LET cuantos = consulta(FALSE)
    IF cuantos>0 THEN 
     CALL dialog.setCurrentRow("sdet",1)
     -- Desplegando datos
     CALL desplegar_datos(reg_det[1].fcodgru)
     CALL setAttr(1)
     CALL DIALOG.setCellAttributes(reg_det_attr)
    END IF 
    CALL encabezado("")
         
   BEFORE ROW 
    LET id = arr_curr()
    IF id>0 THEN 
      -- Desplegando datos
      CALL desplegar_datos(reg_det[id].fcodgru)
      CALL setAttr(id)
      CALL DIALOG.setCellAttributes(reg_det_attr)
    END IF 
      
   ON ACTION buscar
    LET cuantos = consulta(TRUE)
    IF cuantos>0 THEN 
      -- Desplegando datos
      CALL desplegar_datos(reg_det[id].fcodgru)
    END IF 
    CALL encabezado("")

   ON ACTION agregar
    IF ingreso(1) THEN 
      LET cuantos = consulta(FALSE)
      CALL fgl_set_arr_curr(arr_count()+1)

      -- Refrescar Pantalla
      DISPLAY ARRAY reg_det TO sDet.*
       BEFORE DISPLAY EXIT DISPLAY 
      END DISPLAY 
    END IF
    CALL encabezado("")
   
   ON ACTION modificar
    LET id = arr_curr()
    LET ids = scr_line()
    IF id>0 THEN 
     IF modifica(1) THEN
       -- Desplegando datos
       CALL desplegar_datos(reg_det[id].fcodgru)
       LET reg_det[id].fcodgru = g_reg.codgru
       LET reg_det[id].fnombre = g_reg.nombre
       DISPLAY reg_det[id].* TO sDet[ids].*
     END IF   
    END IF 
    CALL encabezado("")

   ON ACTION eliminar
    LET id = arr_curr()
    LET ids = scr_line()         
    IF id>0 THEN 
      IF eliminar() THEN
         CALL DIALOG.deleteRow("sdet",id)
         IF id = arr_count() THEN 
            LET id = id-1
         END IF 
         IF id>0 THEN 
            -- Desplegando datos
            CALL desplegar_datos(reg_det[id].fcodgru)
         ELSE 
            INITIALIZE g_reg.* TO NULL
         END IF 
         DISPLAY BY NAME g_reg.*
      END IF   

      -- Regresando a datos generales
      LET f = DIALOG.getForm()
      CALL f.ensureFieldVisible("grupostiposdoc.codgru")
    END IF 

  {ON ACTION tiposdoc ATTRIBUTE(TEXT="Tipos-Documento") 
   IF id>0 THEN 
     -- Ingresando tipos de documento  
     CALL CapturaTiposDocumento()

     -- Desplegando datos
     CALL desplegar_datos(reg_det[id].fcodgru)

     -- Regresando a datos generales
     LET f = DIALOG.getForm()
     CALL f.ensureFieldVisible("grupostiposdoc.codgru")
   END IF 
   CALL encabezado("")} 

   {ON ACTION honorarios ATTRIBUTE(TEXT="Honorarios") 
    IF id>0 THEN 
     -- Ingresando honorarios 
     CALL CapturaHonorarios() 

     -- Desplegando datos
     CALL desplegar_datos(reg_det[id].fcodgru)

     -- Regresando a datos generales
     LET f = DIALOG.getForm()
     CALL f.ensureFieldVisible("grupostiposdoc.codgru")
    END IF 
    CALL encabezado("")} 

   {ON ACTION gastos ATTRIBUTE(TEXT="Gastos") 
    IF id>0 THEN 
     -- Ingresando gastos 
     CALL CapturaGastos()       

     -- Desplegando datos
     CALL desplegar_datos(reg_det[id].fcodgru)

     -- Regresando a datos generales
     LET f = DIALOG.getForm()
     CALL f.ensureFieldVisible("grupostiposdoc.codgru")
    END IF 
    CALL encabezado("")} 

   {ON ACTION fases ATTRIBUTE(TEXT="Fases") 
    IF id>0 THEN 
     -- Ingresando fases  
     CALL CapturaFases()       

     -- Desplegando datos
     CALL desplegar_datos(reg_det[id].fcodgru)

     -- Regresando a datos generales
     LET f = DIALOG.getForm()
     CALL f.ensureFieldVisible("grupostiposdoc.codgru")
    END IF 
   CALL encabezado("")}

   ON ACTION salir
    EXIT DIALOG  
  END DISPLAY 

  -- Desplegando tipos de documento
  DISPLAY ARRAY v_tipdocs TO sTipdocs.*
   BEFORE DISPLAY
    LET arrt = ARR_CURR()
    CALL setAttrTiposDocumento(1)
    CALL DIALOG.setCellAttributes(v_tipdoc_attr)

   BEFORE ROW
    LET arrt = ARR_CURR()
    CALL setAttrTiposDocumento(arrt)
    CALL DIALOG.setCellAttributes(v_tipdoc_attr)
    
   ON ACTION addtipdoc ATTRIBUTE (TEXT="Agregar",IMAGE="mars_mas")
    -- Agregando
    LET arr = ARR_COUNT()+1
    CALL picklist_2("Lista de Tipos de Documento",
                    "Tipo",
                    "Nombre del Tipo",
                    "x.tipdoc",
                    "x.nombre",
                    "tiposdocumento x",
                    "1=1 AND NOT EXISTS "||
                    "(SELECT y.tipdoc FROM grupostiposdoc_det y "||
                    "WHERE y.tipdoc = x.tipdoc)",
                    2,
                    1) 
    RETURNING xcodigo,xnombre,regreso
    IF xcodigo IS NOT NULL THEN
       LET v_tipdocs[arr].gcodgru = g_reg.codgru
       LET v_tipdocs[arr].gtipdoc = xcodigo 
       LET v_tipdocs[arr].gnomdoc = xnombre 

       -- Grabando tipo de documento
       INSERT INTO grupostiposdoc_det
       VALUES (v_tipdocs[arr].gcodgru,
               v_tipdocs[arr].gtipdoc,
               arr)
    END IF 

    DISPLAY ARRAY v_tipdocs TO sTipdocs.*
    BEFORE DISPLAY EXIT DISPLAY END DISPLAY 

   ON ACTION delete ATTRIBUTE(TEXT="Eliminar",IMAGE="mars_eliminar")
    -- Eliminando
    IF box_confirma("Esta seguro de eliminar la linea.") THEN
       LET arrt = ARR_CURR()
       LET xcodigo = v_tipdocs[arrt].gtipdoc 
       CALL v_tipdocs.deleteElement(arrt)

       -- Borrando tipo de documento
       DELETE FROM grupostiposdoc_det
       WHERE grupostiposdoc_det.codgru = g_reg.codgru
         AND grupostiposdoc_det.tipdoc = xcodigo 
    END IF
  END DISPLAY

  -- Desplegando honorarios 
  DISPLAY ARRAY vho_cobros TO shoCobros.*
   BEFORE DISPLAY
    LET arrh = ARR_CURR()
    CALL setAttrHonorarios(1)
    CALL DIALOG.setCellAttributes(vho_cobros_attr)

   BEFORE ROW
    LET arrh = ARR_CURR()
    CALL setAttrHonorarios(arrh)
    CALL DIALOG.setCellAttributes(vho_cobros_attr)

    -- Desplegando datos del honorario
    IF (vho_cobros[arrh].hotipcob IS NOT NULL) THEN
       CALL DesplegarDatosHonorario(arrh)
    END IF

   ON ACTION addhonorarios ATTRIBUTE (TEXT="Agregar",IMAGE="mars_mas")
    -- Agregando
    LET arr = vho_cobros.getLength()+1 
    CALL picklist_2("Lista de Tipos de Honorarios",
                    "Tipo de Honorario",
                    "Nombre del Tipo de Honorario",
                    "x.tipcob",
                    "x.nomcob",
                    "tiposcobro x",
                    "1=1 AND NOT EXISTS "||
                    "(SELECT y.tipcob FROM grupostiposdoc_cob y "||
                    "WHERE y.codgru = "||g_reg.codgru||" AND y.tipcob = x.tipcob)",
                    2,
                    1) 
    RETURNING xcodigo,xnombre,regreso
    IF xcodigo IS NOT NULL THEN
       -- Inicializando datos
       CALL v_honors.clear() 
       LET ho_ppd.htipcob = xcodigo
       LET ho_ppd.hnomcob = xnombre 
       LET ho_ppd.hrangos = 0
       LET ho_ppd.hvalfij = 0
       LET ho_ppd.hvalcob = 0
       LET ho_ppd.hporcen = 0
       LET ho_ppd.hvalmax = 0
       LET ho_ppd.hingcan = 0
       LET ho_ppd.hsumsub = 0
       LET ho_ppd.hmulcx1 = 0
       LET ho_ppd.hcaliva = 0
       LET ho_ppd.hvalsum = 0
       LET ho_ppd.hvalres = 0

       -- Desplegando datos 
       DISPLAY ho_ppd.htipcob TO hxtipcob 
       DISPLAY ARRAY v_honors TO sHonors.* 
       BEFORE DISPLAY EXIT DISPLAY END DISPLAY 

       -- Ingresando datos del honorario
       CALL CapturarDatosHonorario(ho_ppd.*)
       RETURNING ho_ppd.*,aceptar
       IF aceptar THEN
          -- Asignando datos
          LET vho_cobros[arr].hotipcob = ho_ppd.htipcob
          LET vho_cobros[arr].honomcob = ho_ppd.hnomcob 
          LET vho_cobros[arr].horangos = ho_ppd.hrangos
          LET vho_cobros[arr].hovalfij = ho_ppd.hvalfij 
          LET vho_cobros[arr].hovalcob = ho_ppd.hvalcob
          LET vho_cobros[arr].hoporcen = ho_ppd.hporcen
          LET vho_cobros[arr].hovalmax = ho_ppd.hvalmax 
          LET vho_cobros[arr].hoingcan = ho_ppd.hingcan
          LET vho_cobros[arr].hosumsub = ho_ppd.hsumsub
          LET vho_cobros[arr].homulcx1 = ho_ppd.hmulcx1
          LET vho_cobros[arr].hocaliva = ho_ppd.hcaliva 
          LET vho_cobros[arr].hovalsum = ho_ppd.hvalsum 
          LET vho_cobros[arr].hovalres = ho_ppd.hvalres 

          -- Grabando honorario 
          CALL GrabarHonorarios(arr)

          -- Desplegando datos 
          CALL DesplegarDatosHonorario(arr)
       END IF
    END IF 

    -- Desplegando datos 
    DISPLAY ARRAY vho_cobros TO shoCobros.*
    BEFORE DISPLAY EXIT DISPLAY END DISPLAY 
    CALL DesplegarDatosHonorario(arrh)

   ON ACTION edithonorarios ATTRIBUTE (TEXT="Editar",IMAGE="edit")
    IF (vho_cobros[arrh].hotipcob IS NOT NULL) THEN
       CALL CapturarDatosHonorario(vho_cobros[arrh].*)
       RETURNING ho_ppd.*,aceptar
       IF aceptar THEN
          -- Asignando datos 
          LET vho_cobros[arrh].horangos = ho_ppd.hrangos
          LET vho_cobros[arrh].hovalfij = ho_ppd.hvalfij 
          LET vho_cobros[arrh].hovalcob = ho_ppd.hvalcob
          LET vho_cobros[arrh].hoporcen = ho_ppd.hporcen
          LET vho_cobros[arrh].hovalmax = ho_ppd.hvalmax
          LET vho_cobros[arrh].hoingcan = ho_ppd.hingcan
          LET vho_cobros[arrh].hosumsub = ho_ppd.hsumsub
          LET vho_cobros[arrh].homulcx1 = ho_ppd.hmulcx1
          LET vho_cobros[arrh].hocaliva = ho_ppd.hcaliva 
          LET vho_cobros[arrh].hovalsum = ho_ppd.hvalsum 
          LET vho_cobros[arrh].hovalres = ho_ppd.hvalres 

          -- Grabando honorario 
          CALL GrabarHonorarios(arrh)
       END IF 
    END IF 

   ON ACTION delhonorarios ATTRIBUTE(TEXT="Eliminar",IMAGE="mars_eliminar")
    -- Eliminando
    IF box_confirma("Esta seguro de eliminar la linea.") THEN
       LET arrh = ARR_CURR()
       LET xcodigo = vho_cobros[arrh].hotipcob 
       CALL vho_cobros.deleteElement(arrh)

       -- Borrando honorario 
       DELETE FROM grupostiposdoc_cob
       WHERE grupostiposdoc_cob.codgru = g_reg.codgru
         AND grupostiposdoc_cob.tipval = TIPVALHONORS 
         AND grupostiposdoc_cob.tipcob = xcodigo 

       -- Desplegando datos
       IF (arrh=ARR_COUNT()) THEN 
          LET arr = arrh-1
       END IF 
       IF (arr>0) THEN 
          CALL DesplegarDatosHonorario(arr)
       ELSE 
       END IF 
    END IF 

    -- Desplegando datos 
    DISPLAY ARRAY vho_cobros TO shoCobros.*
    BEFORE DISPLAY EXIT DISPLAY END DISPLAY 
  END DISPLAY

  -- Desplegando gastos 
  DISPLAY ARRAY vga_cobros TO sgaCobros.*
   BEFORE DISPLAY
    LET arrg = ARR_CURR()
    CALL setAttrGastos(1)
    CALL DIALOG.setCellAttributes(vga_cobros_attr)

   BEFORE ROW
    LET arrg = ARR_CURR()
    CALL setAttrGastos(arrg)
    CALL DIALOG.setCellAttributes(vga_cobros_attr)

    -- Desplegando datos del gasto 
    IF (vga_cobros[arrg].gatipcob IS NOT NULL) THEN 
       CALL DesplegarDatosGasto(arrg)
    END IF 

   ON ACTION addgastos ATTRIBUTE (TEXT="Agregar",IMAGE="mars_mas")
    -- Agregando
    LET arr = ARR_COUNT()+1
    CALL picklist_2("Lista de Tipos de Gastos",
                    "Tipo de Gasto",
                    "Nombre del Tipo de Gasto",
                    "x.tipcob",
                    "x.nomcob",
                    "tiposcobro x",
                    "1=1 AND NOT EXISTS "||
                    "(SELECT y.tipcob FROM grupostiposdoc_cob y "||
                    "WHERE y.codgru = "||g_reg.codgru||" AND y.tipcob = x.tipcob)",
                    2,
                    1) 
    RETURNING xcodigo,xnombre,regreso
    IF xcodigo IS NOT NULL THEN
       LET vga_cobros[arr].gatipcob = xcodigo 
       LET vga_cobros[arr].ganomcob = xnombre 

       -- Grabando honorarios 
       {INSERT INTO grupostiposdoc_cob
       VALUES (v_tipdocs[arr].gcodgru,
               v_tipdocs[arr].gtipdoc,
               arr)}
    END IF 

    DISPLAY ARRAY vga_cobros TO sgaCobros.*
    BEFORE DISPLAY EXIT DISPLAY END DISPLAY 

   ON ACTION editgastos ATTRIBUTE (TEXT="Editar",IMAGE="edit")
    IF (vga_cobros[arrg].gatipcob IS NOT NULL) THEN
       CALL CapturarDatosGasto(vga_cobros[arrg].*)
       RETURNING ga_ppd.*,aceptar
       IF aceptar THEN
          -- Asignando datos 
          LET vga_cobros[arrg].garangos = ga_ppd.grangos
          LET vga_cobros[arrg].gavalfij = ga_ppd.gvalfij 
          LET vga_cobros[arrg].gavalcob = ga_ppd.gvalcob
          LET vga_cobros[arrg].gaporcen = ga_ppd.gporcen
          LET vga_cobros[arrg].gavalmax = ga_ppd.gvalmax
          LET vga_cobros[arrg].gaingcan = ga_ppd.gingcan
          LET vga_cobros[arrg].gasumsub = ga_ppd.gsumsub
          LET vga_cobros[arrg].gamulcx1 = ga_ppd.gmulcx1
          LET vga_cobros[arrg].gacaliva = ga_ppd.gcaliva 
          LET vga_cobros[arrg].gavalsum = ga_ppd.gvalsum 
          LET vga_cobros[arrg].gavalres = ga_ppd.gvalres 

          -- Grabando gasto 
          CALL GrabarGastos(arrg)
       END IF 
    END IF 

   ON ACTION delete ATTRIBUTE(TEXT="Eliminar",IMAGE="mars_eliminar")
    -- Eliminando
    IF box_confirma("Esta seguro de eliminar la linea.") THEN
       LET arrg = ARR_CURR()
       LET xcodigo = vga_cobros[arrh].gatipcob 
       CALL vga_cobros.deleteElement(arrg)

       -- Borrando gasto 
       DELETE FROM grupostiposdoc_cob
       WHERE grupostiposdoc_cob.codgru = g_reg.codgru
         AND grupostiposdoc_cob.tipval = TIPVALGASTOS 
         AND grupostiposdoc_cob.tipcob = xcodigo 
    END IF 
  END DISPLAY

  -- Desplegando fases 
  DISPLAY ARRAY vga_fases TO sgaFases.*
   BEFORE DISPLAY
    LET arrf = ARR_CURR()
    CALL setAttrFases(1)
    CALL DIALOG.setCellAttributes(vga_fases_attr)

   BEFORE ROW
    LET arrf = ARR_CURR()
    CALL setAttrFases(arrf)
    CALL DIALOG.setCellAttributes(vga_fases_attr)
  END DISPLAY
 END DIALOG 

 -- Dropeando tabla temporal
 DROP TABLE tmp_ranhon
END FUNCTION

FUNCTION Desplegar_Datos(wcodgru SMALLINT)
 DEFINE g_tco cDet 
 DEFINE g_tga cDetga 
 DEFINE g_tdc dDet 
 DEFINE g_tfa cDetfa 
 DEFINE x     INT
 
 -- Seleccionando datos del grupo de tipos de documento 
 -- Generales y valores
 SELECT a.codgru,a.nombre
  INTO  g_reg.* 
  FROM  grupostiposdoc a
  WHERE a.codgru = wcodgru
  IF (status!=NOTFOUND) THEN
      DISPLAY BY NAME g_reg.*
  END IF 

 -- Inicializando table temporal
 DELETE FROM tmp_ranhon 
 CALL v_honors.clear()
 DISPLAY ARRAY v_honors TO sHonors.* 
 BEFORE DISPLAY EXIT DISPLAY END DISPLAY

 -- Seleccionando honorarios 
 CALL vho_cobros.CLEAR()
 DECLARE c_honors CURSOR FOR
 SELECT a.tipcob,
        y.nomcob,
        a.rangos,
        a.valfij,
        a.valcob,
        a.porcen,
        a.valmax, 
        a.ingcan,
        a.sumsub,
        a.mulcx1,
        a.caliva,
        a.valsum,
        a.valres 
  FROM  grupostiposdoc_cob a,tiposcobro y
  WHERE a.codgru = g_reg.codgru
    AND a.tipval = TIPVALHONORS 
    AND y.tipcob = a.tipcob 
  ORDER BY a.numcor

  LET x = 0
  FOREACH c_honors INTO g_tco.*
   LET x = x+1 
   LET vho_cobros[x].* = g_tco.*
  END FOREACH
  CLOSE c_honors
  FREE  c_honors

 -- Seleccionando rangos de los honorarios 
 INSERT INTO tmp_ranhon 
 SELECT a.* FROM grupostiposdoc_ran a
  WHERE a.codgru = g_reg.codgru 
    AND a.tipval = TIPVALHONORS 

 -- Seleccionando gastos 
 CALL vga_cobros.CLEAR()
 DECLARE c_gastos CURSOR FOR
 SELECT a.tipcob,
        y.nomcob,
        a.rangos,
        a.valfij,
        a.valcob,
        a.porcen,
        a.valmax, 
        a.ingcan,
        a.sumsub,
        a.mulcx1,
        a.caliva,
        a.valsum,
        a.valres 
  FROM  grupostiposdoc_cob a,tiposcobro y
  WHERE a.codgru = g_reg.codgru
    AND a.tipval = TIPVALGASTOS 
    AND y.tipcob = a.tipcob 
  ORDER BY a.numcor

  LET x = 0
  FOREACH c_gastos INTO g_tga.*
   LET x = x+1 
   LET vga_cobros[x].* = g_tga.*
  END FOREACH
  CLOSE c_gastos
  FREE  c_gastos

 -- Seleccionando rangos de los gastos 
 INSERT INTO tmp_ranhon 
 SELECT a.* FROM grupostiposdoc_ran a
  WHERE a.codgru = g_reg.codgru 
    AND a.tipval = TIPVALGASTOS 

 -- Seleccionando tipos de documento 
 CALL v_tipdocs.CLEAR()
 DECLARE c_tipdocs CURSOR FOR
 SELECT a.codgru,
        a.tipdoc,
        y.nombre
  FROM  grupostiposdoc_det a,tiposdocumento y
  WHERE a.codgru = g_reg.codgru
    AND y.tipdoc = a.tipdoc 
  ORDER BY a.numcor

  LET x = 0
  FOREACH c_tipdocs INTO g_tdc.*
   LET x = x+1 
   LET v_tipdocs[x].* = g_tdc.*
  END FOREACH
  CLOSE c_tipdocs
  FREE  c_tipdocs

 -- Seleccionando fases 
 CALL vga_fases.CLEAR()
 DECLARE c_fases CURSOR FOR
 SELECT a.nofase,
        y.nomfas,
        a.diamin,
        a.diarea, 
        a.diamax
  FROM  grupostiposdoc_fas a,fases_not y
  WHERE a.codgru = g_reg.codgru
    AND y.nofase = a.nofase 
  ORDER BY a.numcor

  LET x = 0
  FOREACH c_fases INTO g_tfa.*
   LET x = x+1 
   LET vga_fases[x].* = g_tfa.*
  END FOREACH
  CLOSE c_fases   
  FREE  c_fases
END FUNCTION

FUNCTION encabezado(gtit_enc)
 DEFINE gtit_enc STRING 

 DISPLAY BY NAME  gtit_enc
END FUNCTION 

FUNCTION setAttr(id)
 DEFINE  i,id SMALLINT

 FOR i=1 TO reg_det.getLength()
  LET reg_det_attr[i].fcodgru = "black"
  LET reg_det_attr[i].fnombre = "black"
 END FOR  

 LET reg_det_attr[id].fcodgru = "blue reverse"
 LET reg_det_attr[id].fnombre = "blue reverse"
END FUNCTION 

FUNCTION combo_init()
 -- Cargando combobox de tipos de documento
 CALL combo_din2("gtipdct","SELECT a.tipdoc,a.nombre FROM tiposdocumento a ORDER BY a.nombre")
 -- Cargando combobox de honorarios 
 CALL combo_din2("hxtipcob","SELECT a.tipcob,a.nomcob FROM tiposcobro a ORDER BY a.nomcob")
 -- Cargando combobox de gastos 
 CALL combo_din2("gxtipcob","SELECT a.tipcob,a.nomcob FROM tiposcobro a ORDER BY a.nomcob")
 -- Cargando combobox de fases  
 CALL combo_din2("fxnofase","SELECT a.nofase,a.nomfas FROM fases_not a ORDER BY a.nofase")
END FUNCTION 
