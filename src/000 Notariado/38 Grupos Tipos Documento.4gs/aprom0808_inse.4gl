-- Insertando datos de Grupos de Tipos de Documento 

GLOBALS "aprom0808_glob.4gl"

FUNCTION insert_init()
 DEFINE strSql STRING 

 LET strSql =
  "INSERT INTO grupostiposdoc (",
  "codgru,nombre) ",
  "VALUES (?,?)"

 PREPARE st_insertar FROM strSql
END FUNCTION 

FUNCTION ingreso(nofase SMALLINT)
 CALL encabezado("Ingresar")
 IF captura_datos('I',nofase) THEN
    RETURN grabar()
 ELSE 
    RETURN FALSE 
 END IF 
END FUNCTION 

FUNCTION grabar()
 LET g_reg.codgru = 0 
 TRY 
  SELECT NVL(MAX(a.codgru),0)+1
   INTO  g_reg.codgru
   FROM  grupostiposdoc a
   
  EXECUTE st_insertar 
  USING g_reg.codgru,
        g_reg.nombre   

 CATCH 
  CALL msgError(sqlca.sqlcode,"Grabar Registro.")
  RETURN FALSE 
 END TRY
 DISPLAY BY NAME g_reg.*
 CALL box_valdato ("Registro agregado.")
 RETURN TRUE
END FUNCTION 
