-- Ingresando datos de las fases del grupo de tipos de documento 

GLOBALS "aprom0808_glob.4gl"

-- Subrutina para capturar el fase         

FUNCTION CapturaFases()              
 DEFINE fxnofase  LIKE grupostiposdoc_fas.nofase, 
        fa_ppd    faDet,  
        scr,arr,i INTEGER, 
        aceptar   SMALLINT,
        loop      SMALLINT 

 -- Ingresando datos
 LET loop = TRUE 
 WHILE loop
  LET fxnofase       = NULL
  LET fa_ppd.fnofase = NULL 
  LET fa_ppd.fnomfas = NULL 
  LET fa_ppd.fdiamin = 0
  LET fa_ppd.fdiarea = 0
  LET fa_ppd.fdiamax = 0
  DISPLAY BY NAME fa_ppd.fdiamin,fa_ppd.fdiarea,fa_ppd.fdiamax

  -- Ingresando datos del fase 
  DIALOG ATTRIBUTES(UNBUFFERED)
   INPUT BY NAME fxnofase 
    BEFORE INPUT
     CALL DIALOG.setActionHidden("close",TRUE)

    AFTER FIELD fxnofase
     -- Verificando que no sea nulo
     IF fxnofase IS NULL THEN
        CALL msg("Seleccionar fase.")
        NEXT FIELD fxnofase
     END IF 

     -- Verificando si ya existe fase 
     IF BuscaFase(fxnofase) THEN
        CALL msg("Fase ya existe registrada.")
        NEXT FIELD fxnofase
     END IF

     -- Ingresando datos del fase 
     LET fa_ppd.fnofase = fxnofase 
     CALL CapturarDatosFase(fa_ppd.*)
     RETURNING fa_ppd.*,aceptar
     IF aceptar THEN 
        -- Llenando lista
        CALL LlenaListaFases(fa_ppd.*)
     END IF 
     EXIT DIALOG 
   END INPUT 

   ON ACTION accept ATTRIBUTE (TEXT="Grabar",IMAGE="grabar") 
    -- Aceptar   
    -- Verificando si hay fases    
    IF vga_fases.getLength()<=0 THEN 
       CALL msg("No existen fases ingresadas.")
       CONTINUE DIALOG 
    END IF

    -- Confirmando grabacion
    CASE box_gradato("Seguro de grabar")
     WHEN "Si"
      LET loop = FALSE 

      -- Grabando fases 
      CALL GrabarFases() 
      EXIT DIALOG
     WHEN "No"
      LET loop = FALSE 
      EXIT DIALOG 
     OTHERWISE
      CONTINUE DIALOG 
    END CASE 

   ON ACTION cancel
    -- Cancelar y salir 
    LET loop = FALSE 
    EXIT DIALOG 

   ON ACTION modificar ATTRIBUTE(TEXT="Modificar")    
    -- Verificando si fases
    IF vga_fases.getLength()<=0 THEN 
       CALL msg("No existen fases registradas.")
       CONTINUE DIALOG 
    END IF

    -- Modificando fases 
    CALL ModificaFases() 
    EXIT DIALOG 
  END DIALOG 
 END WHILE 
END FUNCTION 

-- Subrutina para modificar los fases 

FUNCTION ModificaFases()
 DEFINE fa_ppd    faDet,  
        scr,arr   INTEGER, 
        aceptar   SMALLINT 

 -- Modificando datos del fase 
 DISPLAY ARRAY vga_fases TO sgaFases.*
  ATTRIBUTE (ACCEPT=FALSE,CANCEL=FALSE) 

  ON ACTION regresar ATTRIBUTE (TEXT="Regresar",IMAGE="cancel") 
   -- Salida 
   EXIT DISPLAY 

  ON ACTION editar ATTRIBUTE(TEXT="Editar",IMAGE="edit") 
   -- Editando propiedades del fase 
   LET arr = ARR_CURR() 
   LET scr = SCR_LINE() 

   IF (vga_fases[arr].fanofase IS NOT NULL) THEN 
    CALL CapturarDatosFase(vga_fases[arr].*) 
    RETURNING fa_ppd.*,aceptar 
    IF aceptar THEN 
      -- Asignando datos 
      LET vga_fases[arr].fadiamin = fa_ppd.fdiamin 
      LET vga_fases[arr].fadiarea = fa_ppd.fdiarea 
      LET vga_fases[arr].fadiamax = fa_ppd.fdiamax

      -- Desplegando datos del fase 
      CALL DesplegarDatosFase(arr)
    END IF 
   END IF 

  ON ACTION delete 
   -- Eliminando fase 
   LET arr = ARR_CURR() 

   IF box_confirma("Esta seguro de eliminar el fase.") THEN
      CALL vga_fases.deleteElement(arr)

      -- Desplegando datos del fase
      LET arr = ARR_CURR() 
      CALL DesplegarDatosFase(arr)
   END IF 

  BEFORE DISPLAY 
   LET arr = ARR_CURR() 
   CALL setAttrFases(1)
   CALL DIALOG.setCellAttributes(vga_fases_attr) 

  BEFORE ROW 
   LET arr = ARR_CURR() 
   CALL setAttrFases(arr)
   CALL DIALOG.setCellAttributes(vga_fases_attr) 

   -- Desplegando datos del fase 
   IF (vga_fases[arr].fanofase IS NOT NULL) THEN 
      CALL DesplegarDatosFase(arr)
   END IF 
 END DISPLAY 
END FUNCTION 

-- Subrutina para buscar si ya existe en el vector el fase                

FUNCTION BuscaFase(fxnofase) 
 DEFINE fxnofase   LIKE grupostiposdoc_fas.nofase, 
        i,existe  SMALLINT

 LET existe = FALSE 
 FOR i = 1 TO vga_fases.getLength()
  IF vga_fases[i].fanofase IS NULL THEN
     CONTINUE FOR
  END IF

  IF (vga_fases[i].fanofase=fxnofase) THEN
     LET existe = TRUE
     EXIT FOR
  END IF 
 END FOR
 RETURN existe 
END FUNCTION 

-- Subrutina para llenar el vector de fases

FUNCTION LlenaListaFases(fa_ppd)
 DEFINE fa_ppd    faDet,  
        idx       SMALLINT

 LET idx = vga_fases.getLength()+1 
 LET vga_fases[idx].fanofase = fa_ppd.fnofase
 LET vga_fases[idx].fadiamin = fa_ppd.fdiamin
 LET vga_fases[idx].fadiarea = fa_ppd.fdiarea 
 LET vga_fases[idx].fadiamax = fa_ppd.fdiamax 

 -- Obteniendo nombre del fase
 SELECT NVL(a.nomfas,"NO REGISTRADA") 
  INTO vga_fases[idx].fanomfas 
  FROM fases_not a  
  WHERE a.nofase = fa_ppd.fnofase 

 -- Desplegando datos 
 DISPLAY ARRAY vga_fases TO sgaFases.* 
  BEFORE DISPLAY
   EXIT DISPLAY
 END DISPLAY 
END FUNCTION 

-- Subrutina para capturar los datos del fase 

FUNCTION CapturarDatosFase(fa_ppd) 
 DEFINE fa_ppd     faDet,  
        aceptar,i  SMALLINT

 -- Capturando datos 
 INPUT BY NAME fa_ppd.fdiamin,
               fa_ppd.fdiarea,
               fa_ppd.fdiamax
  ATTRIBUTES (WITHOUT DEFAULTS,UNBUFFERED)

  ON ACTION accept
   -- Verificando datos 
   IF fa_ppd.fdiamin IS NULL OR fa_ppd.fdiamin<0 THEN 
      CALL msg("Dias antes de tiempo debe ser mayor o igual a cero.")
      NEXT FIELD fdiamin 
   END IF 
   IF fa_ppd.fdiarea IS NULL OR fa_ppd.fdiarea<0 THEN 
      CALL msg("Dias en tiempo debe ser mayor o igual a cero.")
      NEXT FIELD fdiarea 
   END IF 
   IF fa_ppd.fdiamax IS NULL OR fa_ppd.fdiamax<0 THEN 
      CALL msg("Dias fuera de tiempo debe ser mayor o igual a cero.")
      NEXT FIELD fdiamax 
   END IF 
 
   -- Aceptando datos 
   LET aceptar = TRUE 
   EXIT INPUT

  ON ACTION cancel 
   -- Salida 
   LET aceptar = FALSE 
   EXIT INPUT
 END INPUT 

 RETURN fa_ppd.*,aceptar 
END FUNCTION  

-- Subrutina para desplegar los datos del fase                 

FUNCTION DesplegarDatosFase(idx SMALLINT)
 -- Desplegando datos 
 DISPLAY vga_fases[idx].fanofase TO fxnofase 
 DISPLAY vga_fases[idx].fadiamin TO fdiamin
 DISPLAY vga_fases[idx].fadiarea TO fdiarea 
 DISPLAY vga_fases[idx].fadiamax TO fdiamax 
END FUNCTION 

-- Subrutina para desplegar las lineas del vector fases 

FUNCTION setAttrFases(id)
 DEFINE i,id SMALLINT

 FOR i=1 TO vga_fases.getLength()
  LET vga_fases_attr[i].fanofase = "black"
  LET vga_fases_attr[i].fanomfas = "black"
  LET vga_fases_attr[i].fadiamin = "black"
  LET vga_fases_attr[i].fadiarea = "black"
  LET vga_fases_attr[i].fadiamax = "black"
 END FOR

 LET vga_fases_attr[id].fanofase = "lightRed reverse"
 LET vga_fases_attr[id].fanomfas = "lightRed reverse"
 LET vga_fases_attr[id].fadiamin = "lightRed reverse"
 LET vga_fases_attr[id].fadiarea = "lightRed reverse"
 LET vga_fases_attr[id].fadiamax = "lightRed reverse"
END FUNCTION

-- Grabar fases del grupo de tipos de documento 

FUNCTION GrabarFases()    
 DEFINE i SMALLINT

 -- Iniciando transaccion
 BEGIN WORK 

  -- Borrando antes de guardar
  DELETE FROM grupostiposdoc_fas
  WHERE grupostiposdoc_fas.codgru = g_reg.codgru 

  -- Grabando fases 
  FOR i = 1 TO vga_fases.getLength()
   IF vga_fases[i].fanofase IS NULL OR 
      vga_fases[i].fadiamin IS NULL OR 
      vga_fases[i].fadiarea IS NULL OR 
      vga_fases[i].fadiamax IS NULL THEN 
      CONTINUE FOR
   END IF 

   -- Datos fases 
   INSERT INTO grupostiposdoc_fas 
   VALUES (g_reg.codgru, 
           vga_fases[i].fanofase,
           vga_fases[i].fadiamin,
           vga_fases[i].fadiarea, 
           vga_fases[i].fadiamax,
           i) 
  END FOR 

 -- Finalizando la transaccion
 COMMIT WORK 
END FUNCTION 
