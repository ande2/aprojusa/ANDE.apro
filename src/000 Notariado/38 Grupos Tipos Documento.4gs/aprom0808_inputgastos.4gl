-- Ingresando datos de los gastos del grupo de tipos de documento 

GLOBALS "aprom0808_glob.4gl"

-- Subrutina para capturar el gasto         

FUNCTION CapturaGastos()              
 DEFINE gxtipcob  LIKE grupostiposdoc_cob.tipcob, 
        ga_ppd    gaDet,  
        scr,arr,i INTEGER, 
        conteo    INTEGER, 
        aceptar   SMALLINT,
        loop      SMALLINT 


 -- Creando tabla temporal
 CREATE TEMP TABLE tmp_rangas
 (
  codgru SMALLINT,
  tipval SMALLINT, 
  tipcob SMALLINT,
  valini DEC(10,2), 
  valfin DEC(10,2), 
  valran DEC(10,2), 
  porran DEC(9,6),
  valres DEC(10,2),
  numcor SMALLINT 
 ) 

 -- Cargando rangos por el grupo de tipo de documento 
 INSERT INTO tmp_rangas 
 SELECT a.* FROM grupostiposdoc_ran a
  WHERE a.codgru = g_reg.codgru 
    AND a.tipval = TIPVALGASTOS 

 -- Ingresando datos
 LET loop = TRUE 
 WHILE loop
  -- Inicializando datos
  CALL v_gastos.clear() 
  DISPLAY ARRAY v_gastos TO sGastos.* BEFORE DISPLAY EXIT DISPLAY END DISPLAY 

  LET gxtipcob       = NULL
  LET ga_ppd.gtipcob = NULL 
  LET ga_ppd.gnomcob = NULL 
  LET ga_ppd.grangos = 0
  LET ga_ppd.gvalfij = 0
  LET ga_ppd.gvalcob = 0
  LET ga_ppd.gporcen = 0
  LET ga_ppd.gvalmax = 0
  LET ga_ppd.gingcan = 0
  LET ga_ppd.gsumsub = 0
  LET ga_ppd.gmulcx1 = 0
  LET ga_ppd.gcaliva = 0
  DISPLAY BY NAME ga_ppd.grangos,ga_ppd.gvalfij,ga_ppd.gvalcob,ga_ppd.gporcen,
                  ga_ppd.gvalmax,ga_ppd.gingcan,ga_ppd.gsumsub,ga_ppd.gmulcx1,
                  ga_ppd.gcaliva 

  -- Ingresando datos del gasto 
  DIALOG ATTRIBUTES(UNBUFFERED)
   INPUT BY NAME gxtipcob 
    BEFORE INPUT
     CALL DIALOG.setActionHidden("close",TRUE)

    AFTER FIELD gxtipcob
     -- Verificando que no sea nulo
     IF gxtipcob IS NULL THEN
        CALL msg("Seleccionar gasto.")
        NEXT FIELD gxtipcob
     END IF 

     -- Verificando si ya existe gasto 
     IF BuscaGasto(gxtipcob) THEN
        CALL msg("Gasto ya existe registrado.")
        NEXT FIELD gxtipcob
     END IF

     -- Verificando si tipo de cobro gasto ya fue grabado como honorario
     SELECT COUNT(*)
      INTO  conteo
      FROM  grupostiposdoc_cob a 
      WHERE a.codgru = g_reg.codgru 
        AND a.tipval = TIPVALHONORS 
        AND a.tipcob = gxtipcob 
     IF (conteo>0) THEN
        CALL msg("Tipo de cobro ya registrado como honorario.")
        NEXT FIELD gxtipcob
     END IF 

     -- Ingresando datos del gasto 
     LET ga_ppd.gtipcob = gxtipcob 
     CALL CapturarDatosGasto(ga_ppd.*)
     RETURNING ga_ppd.*,aceptar
     IF aceptar THEN 
        -- Llenando lista
        CALL LlenaListaGastos(ga_ppd.*)
     END IF 
     EXIT DIALOG 
   END INPUT 

   ON ACTION accept ATTRIBUTE (TEXT="Grabar",IMAGE="grabar") 
    -- Aceptar   
    -- Verificando si hay gastis 
    IF vga_cobros.getLength()<=0 THEN 
       CALL msg("No existen gastos ingresados.")
       CONTINUE DIALOG 
    END IF

    -- Confirmando grabacion
    CASE box_gradato("Seguro de grabar")
     WHEN "Si"
      LET loop = FALSE 

      -- Grabando gastos 
      -- CALL GrabarGastos() 
      EXIT DIALOG
     WHEN "No"
      LET loop = FALSE 
      EXIT DIALOG 
     OTHERWISE
      CONTINUE DIALOG 
    END CASE 

   ON ACTION cancel
    -- Cancelar y salir 
    LET loop = FALSE 
    EXIT DIALOG 

   ON ACTION modificar ATTRIBUTE(TEXT="Modificar")    
    -- Verificando si gastos
    IF vga_cobros.getLength()<=0 THEN 
       CALL msg("No existen gastos registrados.")
       CONTINUE DIALOG 
    END IF

    -- Modificando gastos 
    CALL ModificaGastos() 
    EXIT DIALOG 
  END DIALOG 
 END WHILE 

 -- Dropeando tabla temporal
 DROP TABLE tmp_rangas
END FUNCTION 

-- Subrutina para modificar los gastos 

FUNCTION ModificaGastos()
 DEFINE ga_ppd    gaDet,  
        scr,arr   INTEGER, 
        aceptar   SMALLINT 

 -- Modificando datos del gasto 
 DISPLAY ARRAY vga_cobros TO sgaCobros.*
  ATTRIBUTE (ACCEPT=FALSE,CANCEL=FALSE) 

  ON ACTION regresar ATTRIBUTE (TEXT="Regresar",IMAGE="cancel") 
   -- Salida 
   EXIT DISPLAY 

  ON ACTION editar ATTRIBUTE(TEXT="Editar",IMAGE="edit") 
   -- Editando propiedades del gasto 
   LET arr = ARR_CURR() 
   LET scr = SCR_LINE() 

   IF (vga_cobros[arr].gatipcob IS NOT NULL) THEN 
    CALL CapturarDatosGasto(vga_cobros[arr].*) 
    RETURNING ga_ppd.*,aceptar 
    IF aceptar THEN 
      -- Asignando datos 
      LET vga_cobros[arr].garangos = ga_ppd.grangos
      LET vga_cobros[arr].gavalfij = ga_ppd.gvalfij 
      LET vga_cobros[arr].gavalcob = ga_ppd.gvalcob
      LET vga_cobros[arr].gaporcen = ga_ppd.gporcen
      LET vga_cobros[arr].gavalmax = ga_ppd.gvalmax
      LET vga_cobros[arr].gaingcan = ga_ppd.gingcan
      LET vga_cobros[arr].gasumsub = ga_ppd.gsumsub
      LET vga_cobros[arr].gamulcx1 = ga_ppd.gmulcx1
      LET vga_cobros[arr].gacaliva = ga_ppd.gcaliva 

      -- Desplegando datos del gasto 
      CALL DesplegarDatosGasto(arr)
    END IF 
   END IF 

  ON ACTION delete 
   -- Eliminando gasto 
   LET arr = ARR_CURR() 

   IF box_confirma("Esta seguro de eliminar el gasto.") THEN
      CALL vga_cobros.deleteElement(arr)

      -- Desplegando datos del gasto
      LET arr = ARR_CURR() 
      CALL DesplegarDatosGasto(arr)
   END IF 

  BEFORE DISPLAY 
   LET arr = ARR_CURR() 
   CALL setAttrGastos(1)
   CALL DIALOG.setCellAttributes(vga_cobros_attr) 

  BEFORE ROW 
   LET arr = ARR_CURR() 
   CALL setAttrGastos(arr)
   CALL DIALOG.setCellAttributes(vga_cobros_attr) 

   -- Desplegando datos del gasto 
   IF (vga_cobros[arr].gatipcob IS NOT NULL) THEN 
      CALL DesplegarDatosGasto(arr)
   END IF 
 END DISPLAY 
END FUNCTION 

-- Subrutina para buscar si ya existe en el vector el gasto                

FUNCTION BuscaGasto(gxtipcob) 
 DEFINE gxtipcob   LIKE grupostiposdoc_cob.tipcob, 
        i,existe  SMALLINT

 LET existe = FALSE 
 FOR i = 1 TO vga_cobros.getLength()
  IF vga_cobros[i].gatipcob IS NULL THEN
     CONTINUE FOR
  END IF

  IF (vga_cobros[i].gatipcob=gxtipcob) THEN
     LET existe = TRUE
     EXIT FOR
  END IF 
 END FOR
 RETURN existe 
END FUNCTION 

-- Subrutina para llenar el vector de gastos

FUNCTION LlenaListaGastos(ga_ppd)
 DEFINE ga_ppd    gaDet,  
        idx       SMALLINT

 LET idx = vga_cobros.getLength()+1 
 LET vga_cobros[idx].gatipcob = ga_ppd.gtipcob
 LET vga_cobros[idx].garangos = ga_ppd.grangos
 LET vga_cobros[idx].gavalfij = ga_ppd.gvalfij 
 LET vga_cobros[idx].gavalcob = ga_ppd.gvalcob
 LET vga_cobros[idx].gaporcen = ga_ppd.gporcen
 LET vga_cobros[idx].gavalmax = ga_ppd.gvalmax 
 LET vga_cobros[idx].gaingcan = ga_ppd.gingcan
 LET vga_cobros[idx].gasumsub = ga_ppd.gsumsub
 LET vga_cobros[idx].gamulcx1 = ga_ppd.gmulcx1
 LET vga_cobros[idx].gacaliva = ga_ppd.gcaliva 

 -- Obteniendo nombre del gasto
 SELECT NVL(a.nomcob,"NO REGISTRADO") 
  INTO vga_cobros[idx].ganomcob 
  FROM tiposcobro a 
  WHERE a.tipcob = ga_ppd.gtipcob 

 -- Desplegando datos 
 DISPLAY ARRAY vga_cobros TO sgaCobros.* 
  BEFORE DISPLAY
   EXIT DISPLAY
 END DISPLAY 
END FUNCTION 

-- Subrutina para capturar los datos del gasto 

FUNCTION CapturarDatosGasto(ga_ppd) 
 DEFINE ga_ppd     gaDet,  
        aceptar,i  SMALLINT

 -- Capturando datos 
 INPUT BY NAME ga_ppd.grangos,
               ga_ppd.gvalcob,
               ga_ppd.gporcen,
               ga_ppd.gvalmax,
               ga_ppd.gingcan,
               ga_ppd.gsumsub,
               ga_ppd.gmulcx1,
               ga_ppd.gcaliva
  ATTRIBUTES (WITHOUT DEFAULTS,UNBUFFERED)

  ON ACTION accept
   -- Verificando datos fijos 
   IF ga_ppd.gvalfij=1 THEN
      IF ga_ppd.gvalcob IS NULL OR ga_ppd.gvalcob<0 THEN 
         CALL msg("Valor fijo debe ser mayor o igual a cero.")
         NEXT FIELD gvalcob 
      END IF 
      IF ga_ppd.gporcen IS NULL OR ga_ppd.gporcen<0 THEN 
         CALL msg("Porcentaje fijo debe ser mayor o igual a cero.")
         NEXT FIELD gporcen
      END IF 
      IF ga_ppd.gvalmax IS NULL OR ga_ppd.gvalmax<0 THEN 
         CALL msg("Valor maximo debe ser mayor o igual a cero.")
         NEXT FIELD gvalmax 
      END IF 
      IF ga_ppd.gvalcob=0 AND ga_ppd.gporcen=0 THEN
         CALL msg("Debe ingresarse un valor o un porcentaje.")
         NEXT FIELD gvalcob 
      END IF 
   END IF 
 
   -- Verificando valores rangos  
   IF ga_ppd.grangos=1 THEN 
      IF v_gastos.getLength()<=0 THEN
         CALL msg("Debe ingresarse al menos un valor por rangos.")
         NEXT FIELD grangos 
      END IF 
   END IF 

   -- Aceptando datos 
   LET aceptar = TRUE 
   EXIT INPUT

  ON ACTION cancel 
   -- Salida 
   LET aceptar = FALSE 
   EXIT INPUT

  ON ACTION editrangas 
   -- Ingresando rangos del gasto
   CALL CapturarRangosGasto()
  
  BEFORE INPUT           
   -- Verificando rangos
   IF ga_ppd.grangos=1 THEN 
      -- Habilitando campos
      CALL Dialog.SetActionActive("editrangas",1) 
      CALL Dialog.SetFieldActive("gvalcob",0) 
      CALL Dialog.SetFieldActive("gporcen",0) 
      CALL Dialog.SetFieldActive("gvalmax",0) 
      LET ga_ppd.gvalfij = 0
      LET ga_ppd.gvalcob = 0 
      LET ga_ppd.gporcen = 0 
      LET ga_ppd.gvalmax = 0 
      DISPLAY BY NAME ga_ppd.gvalfij 
   ELSE   
      -- Deshabilitando campos
      CALL Dialog.SetActionActive("editrangas",0) 
      CALL Dialog.SetFieldActive("gvalcob",1) 
      CALL Dialog.SetFieldActive("gporcen",1) 
      CALL Dialog.SetFieldActive("gvalmax",1) 
      LET ga_ppd.gvalfij = 1 
      DISPLAY BY NAME ga_ppd.gvalfij 
   END IF 

  ON CHANGE grangos 
   -- Verificando rangos
   IF ga_ppd.grangos=1 THEN 
      -- Habilitando campos
      CALL Dialog.SetActionActive("editrangas",1) 
      CALL Dialog.SetFieldActive("gvalcob",0) 
      CALL Dialog.SetFieldActive("gporcen",0) 
      CALL Dialog.SetFieldActive("gvalmax",0) 
      LET ga_ppd.gvalfij = 0 
      LET ga_ppd.gvalcob = 0 
      LET ga_ppd.gporcen = 0 
      LET ga_ppd.gvalmax = 0 
      DISPLAY BY NAME ga_ppd.gvalfij 

      -- Ingresando rangos del gasto
      CALL CapturarRangosGasto()
   ELSE 
      -- Deshabilitando campos
      CALL Dialog.SetActionActive("editrangas",0) 
      CALL Dialog.SetFieldActive("gvalcob",1) 
      CALL Dialog.SetFieldActive("gporcen",1) 
      CALL Dialog.SetFieldActive("gvalmax",1) 
      LET ga_ppd.gvalfij = 1 
      DISPLAY BY NAME ga_ppd.gvalfij 
   END IF 

  AFTER FIELD grangos 
   -- Verificando rangos 
   IF ga_ppd.grangos=1 THEN 
      -- Habilitando campos
      CALL Dialog.SetFieldActive("gvalcob",0) 
      CALL Dialog.SetFieldActive("gporcen",0) 
      CALL Dialog.SetFieldActive("gvalmax",0) 
      LET ga_ppd.gvalfij = 0 
      LET ga_ppd.gvalcob = 0 
      LET ga_ppd.gporcen = 0 
      LET ga_ppd.gvalmax = 0 
      DISPLAY BY NAME ga_ppd.gvalfij 
   ELSE 
      -- Deshabilitando campos 
      CALL Dialog.SetFieldActive("gvalcob",1) 
      CALL Dialog.SetFieldActive("gporcen",1) 
      CALL Dialog.SetFieldActive("gvalmax",1) 
      LET ga_ppd.gvalfij = 1 
      DISPLAY BY NAME ga_ppd.gvalfij 
   END IF 
 END INPUT 

 -- Verificando si se aceptaron los datos
 IF aceptar THEN
  -- Verificando si valores por rango para guardarlos en tabla temporal
  IF v_gastos.getLength()>0 THEN
   -- Borrando antes de grabar
   DELETE FROM tmp_rangas
   WHERE tmp_rangas.codgru = g_reg.codgru
     AND tmp_rangas.tipval = TIPVALGASTOS 
     AND tmp_rangas.tipcob = ga_ppd.gtipcob

   -- Grabando rangos
   FOR i = 1 TO v_gastos.getLength()
    IF v_gastos[i].gtvalini IS NULL THEN
       CONTINUE FOR
    END IF 

    -- Asignando datos
    LET v_gastos[i].gtcodgru = g_reg.codgru 
    LET v_gastos[i].gttipval = TIPVALGASTOS 
    LET v_gastos[i].gttipcob = ga_ppd.gtipcob 
    LET v_gastos[i].gtnumcor = i 

    -- Grabando 
    INSERT INTO tmp_rangas
    VALUES (v_gastos[i].*) 
   END FOR 
  END IF
 END IF 

 RETURN ga_ppd.*,aceptar 
END FUNCTION  

-- Subrutina para ingresar los rangos del gasto

FUNCTION CapturarRangosGasto()
 -- Ingresando rangos
 INPUT ARRAY v_gastos FROM sGastos.*
  ATTRIBUTE (WITHOUT DEFAULTS=TRUE,INSERT ROW=FALSE)

  BEFORE INPUT
   CALL DIALOG.setActionHidden("append",TRUE)
 END INPUT
END FUNCTION 

-- Subrutina para desplegar los datos del gasto                 

FUNCTION DesplegarDatosGasto(idx SMALLINT)
 DEFINE g_ran   gDet, 
        totran  SMALLINT 

 -- Desplegando datos 
 DISPLAY vga_cobros[idx].gatipcob TO gxtipcob 
 DISPLAY vga_cobros[idx].garangos TO grangos
 DISPLAY vga_cobros[idx].gavalfij TO gvalfij 
 DISPLAY vga_cobros[idx].gavalcob TO gvalcob
 DISPLAY vga_cobros[idx].gaporcen TO gporcen
 DISPLAY vga_cobros[idx].gavalmax TO gvalmax 
 DISPLAY vga_cobros[idx].gaingcan TO gingcan
 DISPLAY vga_cobros[idx].gasumsub TO gsumsub
 DISPLAY vga_cobros[idx].gamulcx1 TO gmulcx1
 DISPLAY vga_cobros[idx].gacaliva TO gcaliva 

 -- Obteniendo datos de los rangos 
 CALL v_gastos.clear() 
 IF (vga_cobros[idx].garangos>0) THEN 
    DECLARE ctmp CURSOR FOR
    SELECT a.*
     FROM  tmp_rangas a
     WHERE a.codgru = g_reg.codgru
       AND a.tipval = TIPVALGASTOS 
       AND a.tipcob = vga_cobros[idx].gatipcob 
     ORDER BY a.numcor

     LET totran = 0
     FOREACH ctmp INTO g_ran.*
      LET totran = totran+1
      LET v_gastos[totran].* = g_ran.*
     END FOREACH
     CLOSE ctmp
     FREE  ctmp 
 END IF 

 -- Desplegando rangos 
 DISPLAY ARRAY v_gastos TO sGastos.* 
  BEFORE DISPLAY
   EXIT DISPLAY
 END DISPLAY 
END FUNCTION 

-- Subrutina para desplegar las lineas del vector gastos 

FUNCTION setAttrGastos(id)
 DEFINE i,id SMALLINT

 FOR i=1 TO vga_cobros.getLength()
  LET vga_cobros_attr[i].gatipcob = "black"
  LET vga_cobros_attr[i].ganomcob = "black"
  LET vga_cobros_attr[i].garangos = "black"
  LET vga_cobros_attr[i].gavalfij = "black"
  LET vga_cobros_attr[i].gavalcob = "black"
  LET vga_cobros_attr[i].gaporcen = "black"
  LET vga_cobros_attr[i].gavalmax = "black"
  LET vga_cobros_attr[i].gaingcan = "black"
  LET vga_cobros_attr[i].gasumsub = "black"
  LET vga_cobros_attr[i].gamulcx1 = "black"
  LET vga_cobros_attr[i].gacaliva = "black"
 END FOR

 LET vga_cobros_attr[id].gatipcob = "gray reverse"
 LET vga_cobros_attr[id].ganomcob = "gray reverse"
 LET vga_cobros_attr[id].garangos = "gray reverse"
 LET vga_cobros_attr[id].gavalfij = "gray reverse"
 LET vga_cobros_attr[id].gavalcob = "gray reverse"
 LET vga_cobros_attr[id].gaporcen = "gray reverse"
 LET vga_cobros_attr[id].gavalmax = "gray reverse"
 LET vga_cobros_attr[id].gaingcan = "gray reverse"
 LET vga_cobros_attr[id].gasumsub = "gray reverse"
 LET vga_cobros_attr[id].gamulcx1 = "gray reverse"
 LET vga_cobros_attr[id].gacaliva = "gray reverse"
END FUNCTION

-- Grabar gastos del grupo de tipos de documento 

FUNCTION GrabarGastos(i)    
 DEFINE g_ran gDet,
        i     SMALLINT

 -- Iniciando transaccion
 BEGIN WORK 

  -- Borrando antes de guardar
  DELETE FROM grupostiposdoc_cob
  WHERE grupostiposdoc_cob.codgru = g_reg.codgru 
    AND grupostiposdoc_cob.tipval = TIPVALGASTOS 
    AND grupostiposdoc_cob.tipcob = vga_cobros[i].gatipcob 

  -- Grabando gasto               
  INSERT INTO grupostiposdoc_cob 
  VALUES (g_reg.codgru, 
          TIPVALGASTOS,
          vga_cobros[i].gatipcob,
          vga_cobros[i].garangos,
          vga_cobros[i].gavalfij, 
          vga_cobros[i].gavalcob,
          vga_cobros[i].gaporcen,
          vga_cobros[i].gavalmax,
          vga_cobros[i].gaingcan,
          vga_cobros[i].gasumsub,
          vga_cobros[i].gamulcx1,
          vga_cobros[i].gacaliva,
          i) 

  -- Grabando rangos 
  IF vga_cobros[i].garangos=1 THEN 
    -- Borrando antes de grabar
    DELETE FROM grupostiposdoc_ran 
    WHERE grupostiposdoc_ran.codgru = g_reg.codgru
      AND grupostiposdoc_ran.tipval = TIPVALGASTOS 
      AND grupostiposdoc_ran.tipcob = vga_cobros[i].gatipcob

    -- Grabando 
    DECLARE crangas CURSOR FOR
    SELECT a.*
     FROM  tmp_rangas a
     WHERE a.codgru = g_reg.codgru 
       AND a.tipcob = vga_cobros[i].gatipcob 
     ORDER BY a.numcor  
    FOREACH crangas INTO g_ran.*
     INSERT INTO grupostiposdoc_ran
     VALUES (g_ran.*) 
    END FOREACH 
  END IF 

 -- Finalizando la transaccion
 COMMIT WORK 
END FUNCTION 
