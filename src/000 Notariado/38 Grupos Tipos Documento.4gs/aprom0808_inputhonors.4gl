-- Ingresando datos de los honorarios del grupo de tipos de documento 

GLOBALS "aprom0808_glob.4gl"

-- Subrutina para capturar el honorario         

FUNCTION CapturaHonorarios()              
 DEFINE hxtipcob  LIKE grupostiposdoc_cob.tipcob, 
        ho_ppd    hoDet,  
        scr,arr,i INTEGER, 
        conteo    INTEGER, 
        aceptar   SMALLINT,
        loop      SMALLINT 

 -- Creando tabla temporal
 CREATE TEMP TABLE tmp_ranhon
 (
  codgru SMALLINT,
  tipval SMALLINT, 
  tipcob SMALLINT,
  valini DEC(10,2), 
  valfin DEC(10,2), 
  valran DEC(10,2), 
  porran DEC(9,6),
  valres DEC(10,2),
  numcor SMALLINT 
 ) 

 -- Cargando rangos por el grupo de tipo de documento 
 INSERT INTO tmp_ranhon 
 SELECT a.* FROM grupostiposdoc_ran a
  WHERE a.codgru = g_reg.codgru 
    AND a.tipval = TIPVALHONORS 

 -- Ingresando datos
 LET loop = TRUE 
 WHILE loop
  -- Inicializando datos
  CALL v_honors.clear() 
  DISPLAY ARRAY v_honors TO sHonors.* BEFORE DISPLAY EXIT DISPLAY END DISPLAY 

  LET hxtipcob       = NULL
  LET ho_ppd.htipcob = NULL 
  LET ho_ppd.hnomcob = NULL 
  LET ho_ppd.hrangos = 0
  LET ho_ppd.hvalfij = 0
  LET ho_ppd.hvalcob = 0
  LET ho_ppd.hporcen = 0
  LET ho_ppd.hvalmax = 0
  LET ho_ppd.hingcan = 0
  LET ho_ppd.hsumsub = 0
  LET ho_ppd.hmulcx1 = 0
  LET ho_ppd.hcaliva = 0
  LET ho_ppd.hvalsum = 0
  LET ho_ppd.hvalres = 0 
  DISPLAY BY NAME ho_ppd.hrangos,ho_ppd.hvalfij,ho_ppd.hvalcob,ho_ppd.hporcen,
                  ho_ppd.hvalmax,ho_ppd.hingcan,ho_ppd.hsumsub,ho_ppd.hmulcx1,
                  ho_ppd.hcaliva,ho_ppd.hvalsum,ho_ppd.hvalres 

  -- Ingresando datos del honorario 
  DIALOG ATTRIBUTES(UNBUFFERED)
   INPUT BY NAME hxtipcob 
    BEFORE INPUT
     CALL DIALOG.setActionHidden("close",TRUE)

    AFTER FIELD hxtipcob
     -- Verificando que no sea nulo
     IF hxtipcob IS NULL THEN
        CALL msg("Seleccionar honorario.")
        NEXT FIELD hxtipcob
     END IF 

     -- Verificando si ya existe honorario 
     IF BuscaHonorario(hxtipcob) THEN
        CALL msg("Honorario ya existe registrado.")
        NEXT FIELD hxtipcob
     END IF

     -- Verificando si tipo de cobro honorario ya fue grabado como gasto
     SELECT COUNT(*)
      INTO  conteo
      FROM  grupostiposdoc_cob a
      WHERE a.codgru = g_reg.codgru
        AND a.tipval = TIPVALGASTOS 
        AND a.tipcob = hxtipcob
     IF (conteo>0) THEN
        CALL msg("Tipo de cobro ya registrado como gasto.")
        NEXT FIELD hxtipcob
     END IF

     -- Ingresando datos del honorario 
     LET ho_ppd.htipcob = hxtipcob 
     CALL CapturarDatosHonorario(ho_ppd.*)
     RETURNING ho_ppd.*,aceptar
     IF aceptar THEN 
        -- Llenando lista
        CALL LlenaListaHonorarios(ho_ppd.*)
     END IF 
     EXIT DIALOG 
   END INPUT 

   ON ACTION accept ATTRIBUTE (TEXT="Grabar",IMAGE="grabar") 
    -- Aceptar   
    -- Verificando si hay honorarios 
    IF vho_cobros.getLength()<=0 THEN 
       CALL msg("No existen honorarios ingresados.")
       CONTINUE DIALOG 
    END IF

    -- Confirmando grabacion
    CASE box_gradato("Seguro de grabar")
     WHEN "Si"
      LET loop = FALSE 

      -- Grabando honorarios 
      --CALL GrabarHonorarios() 
      EXIT DIALOG
     WHEN "No"
      LET loop = FALSE 
      EXIT DIALOG 
     OTHERWISE
      CONTINUE DIALOG 
    END CASE 

   ON ACTION cancel
    -- Cancelar y salir 
    LET loop = FALSE 
    EXIT DIALOG 

   ON ACTION modificar ATTRIBUTE(TEXT="Modificar")    
    -- Verificando si honorarios
    IF vho_cobros.getLength()<=0 THEN 
       CALL msg("No existen honorarios registrados.")
       CONTINUE DIALOG 
    END IF

    -- Modificando honorarios 
    CALL ModificaHonorarios() 
    EXIT DIALOG 
  END DIALOG 
 END WHILE 

 -- Dropeando tabla temporal
 DROP TABLE tmp_ranhon
END FUNCTION 

-- Subrutina para modificar los honorarios 

FUNCTION ModificaHonorarios()
 DEFINE ho_ppd    hoDet,  
        scr,arr   INTEGER, 
        aceptar   SMALLINT 

 -- Modificando datos del honorario 
 DISPLAY ARRAY vho_cobros TO shoCobros.*
  ATTRIBUTE (ACCEPT=FALSE,CANCEL=FALSE) 

  ON ACTION regresar ATTRIBUTE (TEXT="Regresar",IMAGE="cancel") 
   -- Salida 
   EXIT DISPLAY 

  ON ACTION editar ATTRIBUTE(TEXT="Editar",IMAGE="edit") 
   -- Editando propiedades del honorario 
   LET arr = ARR_CURR() 
   LET scr = SCR_LINE() 

   IF (vho_cobros[arr].hotipcob IS NOT NULL) THEN 
    CALL CapturarDatosHonorario(vho_cobros[arr].*) 
    RETURNING ho_ppd.*,aceptar 
    IF aceptar THEN 
      -- Asignando datos 
      LET vho_cobros[arr].horangos = ho_ppd.hrangos
      LET vho_cobros[arr].hovalfij = ho_ppd.hvalfij 
      LET vho_cobros[arr].hovalcob = ho_ppd.hvalcob
      LET vho_cobros[arr].hoporcen = ho_ppd.hporcen
      LET vho_cobros[arr].hovalmax = ho_ppd.hvalmax
      LET vho_cobros[arr].hoingcan = ho_ppd.hingcan
      LET vho_cobros[arr].hosumsub = ho_ppd.hsumsub
      LET vho_cobros[arr].homulcx1 = ho_ppd.hmulcx1
      LET vho_cobros[arr].hocaliva = ho_ppd.hcaliva 
      LET vho_cobros[arr].hovalsum = ho_ppd.hvalsum 
      LET vho_cobros[arr].hovalres = ho_ppd.hvalres 

      -- Desplegando datos del honorario 
      CALL DesplegarDatosHonorario(arr)
    END IF 
   END IF 

  ON ACTION delete 
   -- Eliminando honorario 
   LET arr = ARR_CURR() 

   IF box_confirma("Esta seguro de eliminar el honorario.") THEN
      CALL vho_cobros.deleteElement(arr)

      -- Desplegando datos del honorario
      LET arr = ARR_CURR() 
      CALL DesplegarDatosHonorario(arr)
   END IF 

  BEFORE DISPLAY 
   LET arr = ARR_CURR() 
   CALL setAttrHonorarios(1)
   CALL DIALOG.setCellAttributes(vho_cobros_attr) 

  BEFORE ROW 
   LET arr = ARR_CURR() 
   CALL setAttrHonorarios(arr)
   CALL DIALOG.setCellAttributes(vho_cobros_attr) 

   -- Desplegando datos del honorario 
   IF (vho_cobros[arr].hotipcob IS NOT NULL) THEN 
      CALL DesplegarDatosHonorario(arr)
   END IF 
 END DISPLAY 
END FUNCTION 

-- Subrutina para buscar si ya existe en el vector el honorario                

FUNCTION BuscaHonorario(hxtipcob) 
 DEFINE hxtipcob   LIKE grupostiposdoc_cob.tipcob, 
        i,existe  SMALLINT

 LET existe = FALSE 
 FOR i = 1 TO vho_cobros.getLength()
  IF vho_cobros[i].hotipcob IS NULL THEN
     CONTINUE FOR
  END IF

  IF (vho_cobros[i].hotipcob=hxtipcob) THEN
     LET existe = TRUE
     EXIT FOR
  END IF 
 END FOR
 RETURN existe 
END FUNCTION 

-- Subrutina para llenar el vector de honorarios

FUNCTION LlenaListaHonorarios(ho_ppd)
 DEFINE ho_ppd    hoDet,  
        idx       SMALLINT

 LET idx = vho_cobros.getLength()+1 
 LET vho_cobros[idx].hotipcob = ho_ppd.htipcob
 LET vho_cobros[idx].horangos = ho_ppd.hrangos
 LET vho_cobros[idx].hovalfij = ho_ppd.hvalfij 
 LET vho_cobros[idx].hovalcob = ho_ppd.hvalcob
 LET vho_cobros[idx].hoporcen = ho_ppd.hporcen
 LET vho_cobros[idx].hovalmax = ho_ppd.hvalmax 
 LET vho_cobros[idx].hoingcan = ho_ppd.hingcan
 LET vho_cobros[idx].hosumsub = ho_ppd.hsumsub
 LET vho_cobros[idx].homulcx1 = ho_ppd.hmulcx1
 LET vho_cobros[idx].hocaliva = ho_ppd.hcaliva 
 LET vho_cobros[idx].hovalsum = ho_ppd.hvalsum 
 LET vho_cobros[idx].hovalres = ho_ppd.hvalres 

 -- Obteniendo nombre del honorario
 SELECT NVL(a.nomcob,"NO REGISTRADO") 
  INTO vho_cobros[idx].honomcob 
  FROM tiposcobro a 
  WHERE a.tipcob = ho_ppd.htipcob 

 -- Desplegando datos 
 DISPLAY ARRAY vho_cobros TO shoCobros.* 
  BEFORE DISPLAY
   EXIT DISPLAY
 END DISPLAY 
END FUNCTION 

-- Subrutina para capturar los datos del honorario 

FUNCTION CapturarDatosHonorario(ho_ppd) 
 DEFINE ho_ppd     hoDet,  
        aceptar,i  SMALLINT

 -- Capturando datos 
 INPUT BY NAME ho_ppd.hrangos,
               ho_ppd.hvalcob,
               ho_ppd.hporcen,
               ho_ppd.hvalmax,
               ho_ppd.hingcan,
               ho_ppd.hsumsub,
               ho_ppd.hmulcx1,
               ho_ppd.hcaliva,
               ho_ppd.hvalsum,
               ho_ppd.hvalres 
  ATTRIBUTES (WITHOUT DEFAULTS,UNBUFFERED)

  ON ACTION accept
   -- Verificando datos fijos 
   IF ho_ppd.hvalfij=1 THEN
      IF ho_ppd.hvalcob IS NULL OR ho_ppd.hvalcob<0 THEN 
         CALL msg("Valor fijo debe ser mayor o igual a cero.")
         NEXT FIELD hvalcob 
      END IF 
      IF ho_ppd.hporcen IS NULL OR ho_ppd.hporcen<0 THEN 
         CALL msg("Porcentaje fijo debe ser mayor o igual a cero.")
         NEXT FIELD hporcen
      END IF 
      IF ho_ppd.hvalmax IS NULL OR ho_ppd.hvalmax<0 THEN 
         CALL msg("Valor maximo debe ser mayor o igual a cero.")
         NEXT FIELD hvalmax 
      END IF 
     
      IF ho_ppd.hvalsum=0 AND ho_ppd.hvalres=0 THEN
       IF ho_ppd.hvalcob=0 AND ho_ppd.hporcen=0 THEN
          CALL msg("Debe ingresarse un valor o un porcentaje.")
          NEXT FIELD hvalcob 
       END IF 
      ELSE
       LET ho_ppd.hvalcob = 0
       LET ho_ppd.hporcen = 0
       LET ho_ppd.hvalmax = 0 
       DISPLAY BY NAME ho_ppd.hvalcob,ho_ppd.hporcen,ho_ppd.hvalmax 
      END IF 
   END IF 
 
   -- Verificando valores rangos  
   IF ho_ppd.hrangos=1 THEN 
      IF v_honors.getLength()<=0 THEN
         CALL msg("Debe ingresarse al menos un valor por rangos.")
         NEXT FIELD hrangos 
      END IF 
   END IF 

   -- Aceptando datos 
   LET aceptar = TRUE 
   EXIT INPUT

  ON ACTION cancel 
   -- Salida 
   LET aceptar = FALSE 
   EXIT INPUT

  ON ACTION editranhon 
   -- Ingresando rangos del honorario
   CALL CapturarRangosHonorario()
  
  BEFORE INPUT           
   -- Verificando rangos
   IF ho_ppd.hrangos=1 THEN 
      -- Habilitando campos
      CALL Dialog.SetActionActive("editranhon",1) 
      CALL Dialog.SetFieldActive("hvalcob",0) 
      CALL Dialog.SetFieldActive("hporcen",0) 
      CALL Dialog.SetFieldActive("hvalmax",0) 
      CALL Dialog.SetFieldActive("hvalsum",0) 
      CALL Dialog.SetFieldActive("hvalres",0) 
      LET ho_ppd.hvalfij = 0
      LET ho_ppd.hvalcob = 0 
      LET ho_ppd.hporcen = 0 
      LET ho_ppd.hvalmax = 0 
      LET ho_ppd.hvalsum = 0
      LET ho_ppd.hvalres = 0 
      DISPLAY BY NAME ho_ppd.hvalfij 
   ELSE   
      -- Deshabilitando campos
      CALL Dialog.SetActionActive("editranhon",0) 
      CALL Dialog.SetFieldActive("hvalcob",1) 
      CALL Dialog.SetFieldActive("hporcen",1) 
      CALL Dialog.SetFieldActive("hvalmax",1) 
      CALL Dialog.SetFieldActive("hvalsum",1) 
      CALL Dialog.SetFieldActive("hvalres",1) 
      LET ho_ppd.hvalfij = 1 
      DISPLAY BY NAME ho_ppd.hvalfij 
   END IF 

  ON CHANGE hrangos 
   -- Verificando rangos
   IF ho_ppd.hrangos=1 THEN 
      -- Habilitando campos
      CALL Dialog.SetActionActive("editranhon",1) 
      CALL Dialog.SetFieldActive("hvalcob",0) 
      CALL Dialog.SetFieldActive("hporcen",0) 
      CALL Dialog.SetFieldActive("hvalmax",0) 
      CALL Dialog.SetFieldActive("hvalsum",0) 
      CALL Dialog.SetFieldActive("hvalres",0) 
      LET ho_ppd.hvalfij = 0 
      LET ho_ppd.hvalcob = 0 
      LET ho_ppd.hporcen = 0 
      LET ho_ppd.hvalmax = 0 
      LET ho_ppd.hvalsum = 0
      LET ho_ppd.hvalres = 0 
      DISPLAY BY NAME ho_ppd.hvalfij 

      -- Ingresando rangos del honorario
      CALL CapturarRangosHonorario()
   ELSE 
      -- Deshabilitando campos
      CALL Dialog.SetActionActive("editranhon",0) 
      CALL Dialog.SetFieldActive("hvalcob",1) 
      CALL Dialog.SetFieldActive("hporcen",1) 
      CALL Dialog.SetFieldActive("hvalmax",1) 
      CALL Dialog.SetFieldActive("hvalsum",1) 
      CALL Dialog.SetFieldActive("hvalres",1) 
      LET ho_ppd.hvalfij = 1 
      DISPLAY BY NAME ho_ppd.hvalfij 
   END IF 

  AFTER FIELD hrangos 
   -- Verificando rangos 
   IF ho_ppd.hrangos=1 THEN 
      -- Habilitando campos
      CALL Dialog.SetFieldActive("hvalcob",0) 
      CALL Dialog.SetFieldActive("hporcen",0) 
      CALL Dialog.SetFieldActive("hvalmax",0) 
      CALL Dialog.SetFieldActive("hvalsum",0) 
      CALL Dialog.SetFieldActive("hvalres",0) 
      LET ho_ppd.hvalfij = 0 
      LET ho_ppd.hvalcob = 0 
      LET ho_ppd.hporcen = 0 
      LET ho_ppd.hvalmax = 0 
      LET ho_ppd.hvalsum = 0
      LET ho_ppd.hvalres = 0 
      DISPLAY BY NAME ho_ppd.hvalfij 
   ELSE 
      -- Deshabilitando campos 
      CALL Dialog.SetFieldActive("hvalcob",1) 
      CALL Dialog.SetFieldActive("hporcen",1) 
      CALL Dialog.SetFieldActive("hvalmax",1) 
      CALL Dialog.SetFieldActive("hvalsum",1) 
      CALL Dialog.SetFieldActive("hvalres",1) 
      LET ho_ppd.hvalfij = 1 
      DISPLAY BY NAME ho_ppd.hvalfij 
   END IF 
 END INPUT 

 -- Verificando si se aceptaron los datos
 IF aceptar THEN
  -- Verificando si valores por rango para guardarlos en tabla temporal
  IF v_honors.getLength()>0 THEN
   -- Borrando antes de grabar
   DELETE FROM tmp_ranhon
   WHERE tmp_ranhon.codgru = g_reg.codgru
     AND tmp_ranhon.tipval = TIPVALHONORS 
     AND tmp_ranhon.tipcob = ho_ppd.htipcob

   -- Grabando rangos
   FOR i = 1 TO v_honors.getLength()
    IF v_honors[i].htvalini IS NULL THEN
       CONTINUE FOR
    END IF 

    -- Asignando datos
    LET v_honors[i].htcodgru = g_reg.codgru 
    LET v_honors[i].httipval = TIPVALHONORS 
    LET v_honors[i].httipcob = ho_ppd.htipcob 
    LET v_honors[i].htnumcor = i 

    -- Grabando 
    INSERT INTO tmp_ranhon
    VALUES (v_honors[i].*) 
   END FOR 
  END IF
 END IF 

 RETURN ho_ppd.*,aceptar 
END FUNCTION  

-- Subrutina para ingresar los rangos del honorario

FUNCTION CapturarRangosHonorario()
 -- Ingresando rangos
 INPUT ARRAY v_honors FROM sHonors.*
  ATTRIBUTE (WITHOUT DEFAULTS=TRUE,INSERT ROW=FALSE)

  BEFORE INPUT
   CALL DIALOG.setActionHidden("append",TRUE)
 END INPUT
END FUNCTION 

-- Subrutina para desplegar los datos del honorario                 

FUNCTION DesplegarDatosHonorario(idx SMALLINT)
 DEFINE g_ran   hDet, 
        totran  SMALLINT 

 -- Desplegando datos 
 DISPLAY vho_cobros[idx].hotipcob TO hxtipcob 
 DISPLAY vho_cobros[idx].horangos TO hrangos
 DISPLAY vho_cobros[idx].hovalfij TO hvalfij 
 DISPLAY vho_cobros[idx].hovalcob TO hvalcob
 DISPLAY vho_cobros[idx].hoporcen TO hporcen
 DISPLAY vho_cobros[idx].hovalmax TO hvalmax 
 DISPLAY vho_cobros[idx].hoingcan TO hingcan
 DISPLAY vho_cobros[idx].hosumsub TO hsumsub
 DISPLAY vho_cobros[idx].homulcx1 TO hmulcx1
 DISPLAY vho_cobros[idx].hocaliva TO hcaliva 
 DISPLAY vho_cobros[idx].hovalsum TO hvalsum 
 DISPLAY vho_cobros[idx].hovalres TO hvalres 

 -- Obteniendo datos de los rangos 
 CALL v_honors.clear() 
 IF (vho_cobros[idx].horangos>0) THEN 
    DECLARE ctmp CURSOR FOR
    SELECT a.*
     FROM  tmp_ranhon a
     WHERE a.codgru = g_reg.codgru
       AND a.tipval = TIPVALHONORS 
       AND a.tipcob = vho_cobros[idx].hotipcob 
     ORDER BY a.numcor

     LET totran = 0
     FOREACH ctmp INTO g_ran.*
      LET totran = totran+1
      LET v_honors[totran].* = g_ran.*
     END FOREACH
     CLOSE ctmp
     FREE  ctmp 
 END IF 

 -- Desplegando rangos 
 DISPLAY ARRAY v_honors TO sHonors.* 
  BEFORE DISPLAY
   EXIT DISPLAY
 END DISPLAY 
END FUNCTION 

-- Subrutina para desplegar las lineas del vector honorarios 

FUNCTION setAttrHonorarios(id)
 DEFINE i,id SMALLINT

 FOR i=1 TO vho_cobros.getLength()
  LET vho_cobros_attr[i].hotipcob = "black"
  LET vho_cobros_attr[i].honomcob = "black"
  LET vho_cobros_attr[i].horangos = "black"
  LET vho_cobros_attr[i].hovalfij = "black"
  LET vho_cobros_attr[i].hovalcob = "black"
  LET vho_cobros_attr[i].hoporcen = "black"
  LET vho_cobros_attr[i].hovalmax = "black"
  LET vho_cobros_attr[i].hoingcan = "black"
  LET vho_cobros_attr[i].hosumsub = "black"
  LET vho_cobros_attr[i].homulcx1 = "black"
  LET vho_cobros_attr[i].hocaliva = "black"
  LET vho_cobros_attr[i].hovalsum = "black"
  LET vho_cobros_attr[i].hovalres = "black"
 END FOR

 LET vho_cobros_attr[id].hotipcob = "yellow reverse"
 LET vho_cobros_attr[id].honomcob = "yellow reverse"
 LET vho_cobros_attr[id].horangos = "yellow reverse"
 LET vho_cobros_attr[id].hovalfij = "yellow reverse"
 LET vho_cobros_attr[id].hovalcob = "yellow reverse"
 LET vho_cobros_attr[id].hoporcen = "yellow reverse"
 LET vho_cobros_attr[id].hovalmax = "yellow reverse"
 LET vho_cobros_attr[id].hoingcan = "yellow reverse"
 LET vho_cobros_attr[id].hosumsub = "yellow reverse"
 LET vho_cobros_attr[id].homulcx1 = "yellow reverse"
 LET vho_cobros_attr[id].hocaliva = "yellow reverse"
 LET vho_cobros_attr[id].hovalsum = "yellow reverse"
 LET vho_cobros_attr[id].hovalres = "yellow reverse"
END FUNCTION

-- Grabar honorarios del grupo de tipos de documento 

FUNCTION GrabarHonorarios(i)    
 DEFINE g_ran hDet,
        i     SMALLINT

 -- Iniciando transaccion
 BEGIN WORK 

  -- Borrando antes de guardar
  DELETE FROM grupostiposdoc_cob
  WHERE grupostiposdoc_cob.codgru = g_reg.codgru 
    AND grupostiposdoc_cob.tipval = TIPVALHONORS 
    AND grupostiposdoc_cob.tipcob = vho_cobros[i].hotipcob 

  -- Datos cobro 
  INSERT INTO grupostiposdoc_cob 
  VALUES (g_reg.codgru, 
          TIPVALHONORS,
          vho_cobros[i].hotipcob,
          vho_cobros[i].horangos,
          vho_cobros[i].hovalfij, 
          vho_cobros[i].hovalcob,
          vho_cobros[i].hoporcen,
          vho_cobros[i].hovalmax,
          vho_cobros[i].hoingcan,
          vho_cobros[i].hosumsub,
          vho_cobros[i].homulcx1,
          vho_cobros[i].hocaliva,
          vho_cobros[i].hovalsum,
          vho_cobros[i].hovalres,
          i) 

  -- Grabando rangos 
  IF vho_cobros[i].horangos=1 THEN 
    -- Borrando antes de grabar
    DELETE FROM grupostiposdoc_ran 
    WHERE grupostiposdoc_ran.codgru = g_reg.codgru
      AND grupostiposdoc_ran.tipval = TIPVALHONORS 
      AND grupostiposdoc_ran.tipcob = vho_cobros[i].hotipcob

    -- Grabando 
    DECLARE crangas CURSOR FOR
    SELECT a.*
     FROM  tmp_ranhon a
     WHERE a.codgru = g_reg.codgru 
       AND a.tipcob = vho_cobros[i].hotipcob 
     ORDER BY a.numcor  
    FOREACH crangas INTO g_ran.*
     INSERT INTO grupostiposdoc_ran
     VALUES (g_ran.*) 
    END FOREACH 
  END IF 

 -- Finalizando la transaccion
 COMMIT WORK 
END FUNCTION 
