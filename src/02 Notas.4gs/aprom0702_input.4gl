
GLOBALS "aprom0702_glob.4gl"

FUNCTION captura_datos(operacion)
DEFINE operacion CHAR (1)
DEFINE resultado BOOLEAN
DEFINE w ui.Window
DEFINE f ui.FORM
DEFINE i INT 

DEFINE lpath_remote, lpath_local    STRING

   LET resultado = FALSE 
   LET u_reg.* = g_reg.*
   IF operacion = 'I' THEN 
      INITIALIZE g_reg.* TO NULL
      DISPLAY BY NAME g_reg.*
      CALL ga_pago.CLEAR()
      DISPLAY "pase init ", g_reg.contnote_num
      CALL ga_doc.CLEAR()
   END IF
   DIALOG ATTRIBUTES(UNBUFFERED)
     INPUT 
      g_reg.contact_estado_caso, g_reg.contnote_text, g_reg.contnote_cod_tipologia, 
      g_reg.contnote_fecha_promesa, g_reg.contnote_monto_promesa,
      g_reg.contnote_boleta_numero, g_reg.contnote_boleta_fecha, g_reg.contnote_boleta_monto,
      g_reg.contnote_cuenta, g_reg.contnote_boleta_copia, g_reg.contnote_justificacion,  
      g_reg.contnote_estado
      FROM fcontact_estado_caso, fcontnote_text, fcontnote_cod_tipologia, 
      fcontnote_fecha_promesa, fcontnote_monto_promesa,
      fcontnote_boleta_numero, fcontnote_boleta_fecha, fcontnote_boleta_monto, fcontnote_cuenta,
      fcontnote_boleta_copia, fcontnote_justificacion,  
      fcontnote_estado
      ATTRIBUTES (WITHOUT DEFAULTS)

      BEFORE INPUT
         CALL DIALOG.setActionHidden("close",TRUE)
         IF operacion = 'I' THEN 
            --Caso
            LET g_reg.contnote_contact = arg_val(1)
            --Fecha de la nota
            LET g_reg.contnote_when = CURRENT
            --Fecha de registro
            LET g_reg.contnote_rec_mtime = CURRENT
            --Estado
            LET g_reg.contnote_rec_mstat = "V"
            --Usuario
            LET g_reg.contnote_rec_muser = usuario -- fgl_getenv("LOGNAME")
            LET g_reg.contnote_num = NULL 

            --Estado Caso
            SELECT contact_estado_caso INTO g_reg.contact_estado_caso
               FROM contact
               WHERE contact_num = g_reg.contnote_contact
               
            DISPLAY g_reg.contact_estado_caso, g_reg.contnote_contact, g_reg.contnote_num, 
               g_reg.contnote_when, 
               g_reg.contnote_rec_muser, g_reg.contnote_rec_mtime, g_reg.contnote_rec_mstat
               TO fcontact_estado_caso, fcontnote_contact, fcontnote_num, 
               fcontnote_when, 
               fcontnote_rec_muser, fcontnote_rec_mtime, fcontnote_rec_mstat
         END IF 

      ON CHANGE fcontact_estado_caso
         CASE g_reg.contact_estado_caso 
            WHEN "RECUPERADO" 
               LET g_reg.contnote_cod_tipologia = "18"
            WHEN "REC-PARCIAL"
               LET g_reg.contnote_cod_tipologia = "86"
         END CASE
         SELECT des_tipologia INTO g_reg.des_tipologia FROM tipologia
            WHERE cod_tipologia = g_reg.contnote_cod_tipologia 
         DISPLAY g_reg.des_tipologia TO fdes_tipologia
         NEXT FIELD fcontnote_text 

      {ON ACTION documentos
         CALL f_documentos()}
         
      ON ACTION buscar
          --LABEL opcionBusca:
         CASE  
            WHEN INFIELD (fcontnote_cod_tipologia)
                  CALL picklist_2("Tipología", "Código","Descripción","cod_tipologia", "des_tipologia", "tipologia", "estado ='V'", "1", 1)
                  RETURNING g_reg.contnote_cod_tipologia, g_reg.des_tipologia, INT_FLAG
               --Que no sea nulo
               IF g_reg.contnote_cod_tipologia IS NULL THEN
                  CALL msg("Debe ingresar código de tipología")
                  NEXT FIELD fcontnote_cod_tipologia
               ELSE
                  --LET g_reg.nombre1=vdescripcion
                  SELECT com_tipologia INTO g_reg.contnote_text FROM tipologia WHERE cod_tipologia = g_reg.contnote_cod_tipologia
                  DISPLAY g_reg.des_tipologia TO fdes_tipologia
                  DISPLAY g_reg.contnote_text TO fcontnote_text
                  
               END IF
         END CASE 

      ON CHANGE fcontnote_cod_tipologia
        SELECT des_tipologia, com_tipologia INTO g_reg.des_tipologia, g_reg.contnote_text FROM tipologia
        WHERE cod_tipologia = g_reg.contnote_cod_tipologia 
        IF sqlca.sqlcode != 0 THEN
           CALL msg("Tipología no existe, ingrese de nuevo")
           NEXT FIELD CURRENT 
        ELSE 
            DISPLAY g_reg.des_tipologia TO fdes_tipologia
            DISPLAY g_reg.contnote_text TO fcontnote_text
        END IF 
      
   END INPUT 

   INPUT ARRAY ga_pago FROM sDetPago.* ATTRIBUTES (INSERT ROW=FALSE )
      BEFORE FIELD contpago_bol_numero 
         IF ga_pago[arr_curr()].contpago_num IS NULL THEN
            LET ga_pago[arr_curr()].contpago_num = arr_curr()
            DISPLAY BY NAME ga_pago[arr_curr()].contpago_num 
         END IF 
   END INPUT 
   
   INPUT ARRAY ga_doc FROM sDetDoc.* ATTRIBUTES (INSERT ROW=FALSE ) --DELETE ROW=FALSE, )
      BEFORE FIELD contdocs_tipo   
         LET ga_doc[arr_curr()].contdocs_id = arr_curr()
         --DISPLAY ga_doc[arr_curr()].contdocs_id 

      {BEFORE FIELD contdocs_desc
         IF ga_doc[arr_curr()].contdocs_tipo IS NULL AND  THEN
            ERROR "Seleccione tipo de imagen"
            NEXT FIELD contdocs_tipo
         END IF}

      AFTER DELETE
         FOR i=1 TO ga_doc.getLength()
            LET ga_doc[arr_curr()].contdocs_id = arr_curr() 
         END FOR 

      AFTER ROW 
         IF ga_doc[arr_curr()].contdocs_doc IS NULL THEN
            IF ga_doc[arr_curr()].contdocs_tipo IS NOT NULL OR ga_doc[arr_curr()].contdocs_desc IS NOT NULL THEN 
               ERROR "Debe cargar una imagen"
               NEXT FIELD contdocs_desc
            END IF 
         END IF 
         
      ON ACTION documadd
         IF ga_doc[arr_curr()].contdocs_tipo IS NULL THEN
            ERROR "Seleccione tipo de imagen"
            NEXT FIELD contdocs_tipo
         END IF 
         IF ga_doc[arr_curr()].contdocs_desc IS NULL THEN
            ERROR "Ingrese descripción de la imagen"
            NEXT FIELD contdocs_desc
         END IF 
         LET lpath_local = "./file", arr_curr() USING "&&"
         CALL ui.Interface.frontCall("standard", "openFile", "",lpath_remote)
         DISPLAY "nombre remoto -> ", lpath_remote
         IF lpath_remote IS NOT NULL THEN 
            CALL fgl_getfile(lpath_remote, lpath_local)
            LOCATE ga_doc[arr_curr()].contdocs_doc IN FILE lpath_local
            LET ga_doc[arr_curr()].load_doc = TRUE 
            LET ga_doc[arr_curr()].contdocs_name = lpath_remote
         END IF 
         
 --[result])
   END INPUT 

   ON ACTION ACCEPT
      FOR i = 1 TO ga_doc.getLength()
         IF ga_doc[i].contdocs_doc IS NULL THEN
            IF ga_doc[i].contdocs_tipo IS NOT NULL OR ga_doc[i].contdocs_desc IS NOT NULL THEN 
               ERROR "Debe cargar una imagen"
               NEXT FIELD contdocs_desc
            END IF 
         END IF
      END FOR 
      IF g_reg.contact_estado_caso = "RECUPERADO" THEN 
         IF g_reg.contnote_boleta_numero IS NULL OR LENGTH(g_reg.contnote_boleta_numero) = 0 THEN
            CALL msg("Numero de boleta no puede ser nula")
            NEXT FIELD fcontnote_boleta_numero
         END IF 
         IF g_reg.contnote_boleta_fecha IS NULL THEN
            CALL msg("Fecha de boleta no puede ser nula")
            NEXT FIELD fcontnote_boleta_fecha
         END IF 
         IF g_reg.contnote_boleta_copia IS NULL OR LENGTH(g_reg.contnote_boleta_copia) = 0 THEN
            CALL msg("Debe indicar si entrego copia de boleta")
            NEXT FIELD fcontnote_boleta_copia
         END IF 
         IF g_reg.contnote_boleta_monto IS NULL THEN
            CALL msg("Ingrese monto de depósito")
            NEXT FIELD fcontnote_boleta_monto
         END IF 
         IF g_reg.contnote_cuenta IS NULL THEN
            CALL msg("Ingrese monto de depósito")
            NEXT FIELD fcontnote_boleta_monto
         END IF 
      END IF

      IF g_reg.contnote_boleta_copia = "N" THEN
         IF g_reg.contnote_justificacion IS NULL OR LENGTH(g_reg.contnote_justificacion)=0 THEN 
            CALL msg("Debe ingresar justificación")
            NEXT FIELD fcontnote_justificacion
         END IF 
      END IF 

        IF g_reg.contnote_text IS NULL THEN
           CALL msg("Ingrese nota")
           NEXT FIELD fcontnote_text  
        END IF

      IF operacion = 'M' AND g_reg.* = u_reg.* THEN
         CALL msg("No se efectuaron cambios")
         EXIT DIALOG 
      END IF
      CASE box_gradato("Seguro de grabar")
         WHEN "Si"
            LET resultado = TRUE
            EXIT DIALOG
         WHEN "No"
            EXIT DIALOG 
         OTHERWISE
            CONTINUE DIALOG 
      END CASE 
      LET resultado = TRUE
      EXIT DIALOG 
      
     ON ACTION CANCEL 
        CALL ga_pago.CLEAR() 
        EXIT dialog
   END DIALOG
   
   IF NOT resultado THEN
      LET g_reg.* = u_reg.*
      CALL display_reg()
      --DISPLAY BY NAME g_reg.* 
   END IF 
   RETURN resultado 
END FUNCTION

FUNCTION f_documentos()

   OPEN WINDOW wdocs WITH FORM "aprom0702_form02"
   
      MENU
        { ON ACTION ver 
            CALL showdoc()}
         
         ON ACTION agregar
            CALL f_adddoc()

         ON ACTION eliminar
         
         ON ACTION regresar
            EXIT MENU 
      END MENU
   CLOSE WINDOW wdocs
END FUNCTION 

FUNCTION f_adddoc()
   INPUT ARRAY det_doc FROM sDoc.*
      
END FUNCTION 

