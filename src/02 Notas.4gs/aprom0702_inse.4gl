################################################################################
# Funcion     : %M%
# Descripcion : Ingreso de un nuevo umdor 	
# Funciones   : input_umd() funcion para insertar datos
#              
#              
#               
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Anderson Garcia 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################

GLOBALS "aprom0702_glob.4gl"

FUNCTION insert_init()
DEFINE strSql STRING 

   {LET strSql =
      "INSERT INTO contnote ( ",
         " contnote_contact, contnote_num, ",
         " contnote_rec_mtime, contnote_text, ",
         " contnote_cod_tipologia, contnote_fecha_promesa, contnote_monto_promesa, ", 
         " contnote_rec_muser, contnote_rec_mstat,  contnote_when ",
         "   ) ",
         " VALUES (?,contnote_seq.NEXTVAL,?,?,?,?,?,?,?,?)"

   PREPARE st_insertar FROM strSql}

END FUNCTION 

FUNCTION ingreso()
   CALL encabezado("Ingresar")
   --CALL info_usuario()
   IF captura_datos('I') THEN
      RETURN grabar()
   ELSE 
      RETURN FALSE 
   END IF 
END FUNCTION 

FUNCTION grabar()
DEFINE i SMALLINT 
   --LET g_reg.idpais = 0
   SELECT contnote_seq.NEXTVAL INTO g_reg.contnote_num
      FROM systables WHERE tabid = 1
   TRY 
      INSERT INTO contnote ( 
         contnote_contact, contnote_num, 
         contnote_rec_mtime, contnote_text, 
         contnote_cod_tipologia, contnote_fecha_promesa, contnote_monto_promesa,
         contnote_boleta_numero, contnote_boleta_fecha, contnote_boleta_monto, 
         contnote_boleta_copia, contnote_cuenta, contnote_justificacion, 
         contnote_rec_muser, contnote_rec_mstat, contnote_when, contnote_estado 
           ) 
         VALUES (g_reg.contnote_contact, g_reg.contnote_num, -- contnote_seq.NEXTVAL, 
            g_reg.contnote_rec_mtime, g_reg.contnote_text, 
            g_reg.contnote_cod_tipologia, 
            g_reg.contnote_fecha_promesa, g_reg.contnote_monto_promesa,
            g_reg.contnote_boleta_numero, g_reg.contnote_boleta_fecha, g_reg.contnote_boleta_monto, 
            g_reg.contnote_boleta_copia, g_reg.contnote_cuenta, g_reg.contnote_justificacion,
            --g_reg.contnote_rec_muser,
            usuario, g_reg.contnote_rec_mstat, g_reg.contnote_when, g_reg.contnote_estado)   
            
      UPDATE contact SET contact_estado_caso = g_reg.contact_estado_caso WHERE contact_num = g_reg.contnote_contact  

      FOR i = 1 TO ga_pago.getLength()
         IF ga_pago[i].contpago_bol_monto IS NOT NULL THEN  
            INSERT INTO contnotepagos (contnote_num, contpago_num, contpago_bol_numero,
                  contpago_bol_monto, contpago_bol_fecha, contpago_bol_copia,
                  contpago_justificacion) 
               VALUES (g_reg.contnote_num, ga_pago[i].contpago_num, ga_pago[i].contpago_bol_numero, 
                  ga_pago[i].contpago_bol_monto, ga_pago[i].contpago_bol_fecha, ga_pago[i].contpago_bol_copia,
                  ga_pago[i].contpago_justificacion)
         END IF 
      END FOR 
      
      FOR i = 1 TO ga_doc.getLength()
         IF ga_doc[i].contdocs_tipo IS NOT NULL OR ga_doc[i].contdocs_desc IS NOT NULL THEN
            INSERT INTO contnotedocs (contnote_num, contact_num, 
               contdocs_id, contdocs_tipo, contdocs_desc, 
               contdocs_doc, contdocs_name) 
               VALUES (g_reg.contnote_num, g_reg.contnote_contact, 
                  ga_doc[i].contdocs_id, ga_doc[i].contdocs_tipo, ga_doc[i].contdocs_desc, 
                  ga_doc[i].contdocs_doc, ga_doc[i].contdocs_name)
         END IF 
      END FOR 
      --CS agregarlo cuando se agregue el ID 
      --LET g_reg.idpais = SQLCA.sqlerrd[2]
   CATCH 
      CALL msgError(sqlca.sqlcode,"Grabar Registro")
      RETURN FALSE 
   END TRY
   DISPLAY BY NAME g_reg.*
   CALL box_valdato ("Registro agregado")
   --CALL combo_din2("cmbdepartamento","SELECT * FROM commdep WHERE id_commdep > 0")
   RETURN TRUE 
END FUNCTION 