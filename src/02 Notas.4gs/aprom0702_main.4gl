################################################################################
# Funcion     : %M%
# Descripcion : Modulo Principal para umdores 
# Funciones   : main_init(dbname)
#               main_menu()                               
#               dummy() 
#               
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo  
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################

GLOBALS "aprom0702_glob.4gl"
MAIN
	DEFINE 
		n_param 	SMALLINT,
		prog_name2 	STRING
		
	DEFER INTERRUPT

	OPTIONS
		INPUT  WRAP,
		HELP KEY CONTROL-W,
		COMMENT LINE OFF,
		PROMPT LINE LAST - 2,
		MESSAGE LINE LAST - 1,
		ERROR LINE LAST

   LET n_param = num_args()

   CONNECT TO "aprojusa"
   
	IF n_param = 0 THEN
		RETURN
	ELSE
      LET g_reg.contnote_contact = arg_val(1) 
      LET usuario = arg_val(2)
      LET grupo = grpusu(usuario)
      DISPLAY "grupo ", grupo 
        --LET dbname = "ande"
		--CONNECT TO dbname
	END IF
   
	LET prog_name2 = prog_name||".log"   -- El progr_name es definido como constante en el arch. globals
    
	CALL STARTLOG(prog_name2)
	CALL main_init()
   
END MAIN

FUNCTION main_init()
DEFINE 
	nom_forma 	STRING,
    w           ui.Window,
    f           ui.FORM
DEFINE titulo STRING     
	  
	INITIALIZE u_reg.* TO NULL
	LET g_reg.* = u_reg.*
   
   --CALL trae_usuario() returning g_usuario.*
   --CALL trae_empresa(dbname) returning g_empresa.*
   --CALL trae_programa(prog_name) returning g_programa.*
   --IF g_programa.prog_id = 0 OR 
      --g_programa.prog_id IS NULL THEN
      --CALL box_valdato("Programa no existente, contacte a sistemas")
      --RETURN 
   --END IF 
    CALL ui.Interface.loadActionDefaults("actiondefaults")
   --CALL ui.Interface.loadStyles("default")
   --CALL ui.Interface.loadToolbar("toolbar")

	LET nom_forma = prog_name CLIPPED, "_form"
    CLOSE WINDOW SCREEN 

	OPEN WINDOW w1 WITH FORM nom_forma
   CALL fgl_settitle("Notas por Caso")
   LET g_reg.contnote_contact = arg_val(1)
   SELECT contact_operacion INTO titulo FROM contact WHERE contact_num = g_reg.contnote_contact 
   CALL encabezado(titulo)

	LET w = ui.WINDOW.getcurrent()
	LET f = w.getForm()
	--CALL fgl_settitle(g_programa.prog_des)
    
	CALL insert_init()
   CALL update_init()
   CALL delete_init()
   CALL combo_init()
	CALL main_menu()
   
END FUNCTION                                                                    

FUNCTION main_menu()                               
DEFINE  
    cuantos, 
    id, ids	   SMALLINT,
    vopciones   CHAR(255), 
    cnt 		   SMALLINT,  
    tol ARRAY[15] OF RECORD
      acc      CHAR(15),
      des_cod  SMALLINT
    END RECORD,
    aui, tb, tbi, tbs om.DomNode
   DEFINE titulo     STRING 
   DEFINE txtmsg     STRING 
   DEFINE numdocs    INTEGER  
   DEFINE numpagos   INTEGER  
   DEFINE runcmd     STRING 

   LET cnt = 1

   LET g_reg.contnote_contact = arg_val(1)
   SELECT trim(contact_operacion)||"-"||contact_name INTO titulo FROM contact WHERE contact_num = g_reg.contnote_contact 
   DISPLAY titulo TO uno
    --MENU "test" ON ACTION salir EXIT MENU END MENU 
	DISPLAY ARRAY reg_det TO sDet.*
      ATTRIBUTE(ACCEPT=FALSE,CANCEL=FALSE,UNBUFFERED)
   
      BEFORE DISPLAY
         LET cuantos = consulta(false)
         IF cuantos > 0 THEN 
            CALL dialog.setCurrentRow("sdet",1)
            LET g_reg.* = reg_det[1].*
            CALL display_reg()
            --CALL setAttr(1)
            --CALL DIALOG.setCellAttributes(reg_det_attr)
            LET numpagos = 0
            CALL display_pagos(g_reg.contnote_num) RETURNING numpagos
            LET numdocs = 0
            CALL display_docs(g_reg.contnote_num) RETURNING numdocs
            --DISPLAY BY NAME g_reg.*
         END IF 
         CALL encabezado("")
         
         
      BEFORE ROW 
         LET id = arr_curr()
         IF id > 0 THEN 
            LET g_reg.* = reg_det[id].*
            CALL display_reg()
            --CALL setAttr(id)
            --CALL DIALOG.setCellAttributes(reg_det_attr)
            LET numpagos = 0
            CALL display_pagos(g_reg.contnote_num) RETURNING numpagos
            LET numdocs = 0
            CALL display_docs(g_reg.contnote_num) RETURNING numdocs
            --DISPLAY BY NAME g_reg.*
         END IF 
         IF g_reg.contnote_estado = "C" THEN
            CALL dialog.setActionHidden("abrir",0)
            CALL dialog.setActionHidden("cerrar",1)
         ELSE 
            CALL dialog.setActionHidden("abrir",1)
            CALL dialog.setActionHidden("cerrar",0)
         END IF

      ON ACTION ver
         LET id = arr_curr()
         IF id > 0 THEN
            IF numdocs > 0 THEN 
               LET g_reg.* = reg_det[id].*
               CALL sverdocs() -- fverdocs(g_reg.contnote_num)
            END IF    
         END IF 
         
      ON ACTION agregar
         IF ingreso() THEN 
            LET cuantos = consulta(FALSE)
            CALL fgl_set_arr_curr( arr_count() + 1 )
            --Refrescar Pantalla
            DISPLAY ARRAY reg_det TO sDet.*
               BEFORE DISPLAY  EXIT DISPLAY 
            END DISPLAY 
         END IF
         CALL encabezado("")
         --CALL setAttr(1)
         --CALL DIALOG.setCellAttributes(reg_det_attr)
         --CALL info_usuario()
         
      ON ACTION modificar
         DISPLAY "pase upd"
         IF g_reg.contnote_estado = "A" THEN
            LET id = arr_curr()
            LET ids = scr_line()
            IF id > 0 THEN 
               IF modifica() THEN
                  LET reg_det[id].* = g_reg.*
                  DISPLAY reg_det[id].* TO sDet[ids].*
               END IF   
            END IF 
            CALL encabezado("")
            --CALL setAttr(id)
            --CALL DIALOG.setCellAttributes(reg_det_attr)
         END IF    
         --CALL info_usuario()
         
      ON ACTION anular
         LET id = arr_curr()
         LET ids = scr_line()         
         IF id > 0 THEN 
            IF anular() THEN
               --Si es anulacion
               --LET reg_det[id].* = g_reg.*
               --DISPLAY reg_det[id].* TO sDet[ids].*
               --Si es eliminacion
               CALL DIALOG.deleteRow("sdet", id)
               IF id = arr_count() THEN 
                  LET id = id - 1
               END IF 
               IF id > 0 THEN 
                  LET g_reg.* = reg_det[id].*
               ELSE 
                  INITIALIZE g_reg.* TO NULL
               END IF 
               DISPLAY BY NAME g_reg.*
               LET numpagos = 0
               CALL display_pagos(g_reg.contnote_num) RETURNING numpagos
               LET numdocs = 0
               CALL display_docs(g_reg.contnote_num) RETURNING numdocs
               --CALL setAttr(id)
               --CALL DIALOG.setCellAttributes(reg_det_attr)
            END IF   
         END IF 

      ON ACTION abrir
         LET id = arr_curr()
         IF g_reg.contnote_estado = "C" THEN 
            LET txtmsg = "Desea abrir la nota No. ", g_reg.contnote_num
            IF confirma(txtmsg) THEN
               LET g_reg.contnote_estado = "A"
               UPDATE contnote SET contnote_estado = g_reg.contnote_estado WHERE contnote_num = g_reg.contnote_num
               LET cuantos = consulta(false)
               IF cuantos > 0 THEN 
                  CALL dialog.setCurrentRow("sdet",1)
                  LET g_reg.* = reg_det[id].*
                  CALL display_reg()
                  --CALL setAttr(id)
                  --CALL DIALOG.setCellAttributes(reg_det_attr)
                  --DISPLAY BY NAME g_reg.*
               END IF 
               CALL encabezado("")
               CALL dialog.setActionHidden ("abrir",1)
               CALL dialog.setActionHidden("cerrar",0)
            ELSE 
               LET txtmsg = "Operación cancelada"
               CALL msg(txtmsg)
            END IF
         END IF 
            

      ON ACTION reportedoc
         LET id = arr_curr()
         LET g_reg.* = reg_det[id].*
         LET runcmd = "fglrun apror0715.42r", g_reg.contnote_contact, g_reg.contnote_num, "NOTAS" 
         RUN runcmd  

         
      ON ACTION cerrar
         LET id = arr_curr()
         IF g_reg.contnote_estado = "A" THEN 
            LET txtmsg = "Desea cerrar la nota No. ", g_reg.contnote_num
            IF confirma(txtmsg) THEN
               LET g_reg.contnote_estado = "C"
               UPDATE contnote SET contnote_estado = g_reg.contnote_estado WHERE contnote_num = g_reg.contnote_num
               LET cuantos = consulta(false)
               IF cuantos > 0 THEN 
                  CALL dialog.setCurrentRow("sdet",1)
                  LET g_reg.* = reg_det[id].*
                  CALL display_reg()
                  --CALL setAttr(id)
                  --CALL DIALOG.setCellAttributes(reg_det_attr)
                  --DISPLAY BY NAME g_reg.*
               END IF 
               CALL encabezado("")
               CALL dialog.setActionHidden ("abrir",0)
               CALL dialog.setActionHidden("cerrar",1)
            ELSE 
               LET txtmsg = "Operación cancelada"
               CALL msg(txtmsg)
            END IF
         END IF
      
      --ON ACTION reporte
         --CALL PrintReport()
      ON ACTION salir
         EXIT DISPLAY 
   END DISPLAY 
END FUNCTION

FUNCTION combo_init()
   --CALL combo_din2("contdocs_tipo","SELECT * FROM glbmfam")
   --CALL combo_din2("id_commpue","SELECT * FROM commpue WHERE id_commpue > 0")
   --CALL combo_din2("jefe",'SELECT id_commempl, TRIM(nombre) || " "|| TRIM(apellido) FROM commempl WHERE id_commempl > 0')
   --CALL combo_din2("cmbjefe",'SELECT id_commempl, TRIM(nombre) || " "|| TRIM(apellido) FROM commempl WHERE id_commempl > 0')
   --CALL combo_din2("cmbpuesto","SELECT * FROM commpue WHERE id_commpue > 0")
   --CALL combo_din2("cmbfamilia","SELECT * FROM glbmfam")
END FUNCTION 

FUNCTION encabezado(gtit_enc)
DEFINE gtit_enc STRING 

   --DISPLAY gtit_enc TO uno
END FUNCTION 

FUNCTION display_reg()

   DISPLAY g_reg.contnote_contact, g_reg.contnote_num, g_reg.contact_operacion,
      g_reg.contnote_rec_mtime, g_reg.contact_estado_caso, 
      g_reg.contnote_text, g_reg.contnote_cod_tipologia, g_reg.des_tipologia,
      g_reg.contnote_fecha_promesa, g_reg.contnote_monto_promesa,
      
      g_reg.contnote_boleta_numero, g_reg.contnote_boleta_fecha,
      g_reg.contnote_boleta_monto, g_reg.contnote_cuenta, 
      g_reg.contnote_boleta_copia, g_reg.contnote_justificacion,
      
      g_reg.contnote_rec_muser, g_reg.contnote_rec_mstat, g_reg.contnote_when,
      g_reg.contnote_estado
      TO fcontnote_contact, fcontnote_num, fcontact_operacion,
      fcontnote_rec_mtime, fcontact_estado_caso, 
      fcontnote_text, fcontnote_cod_tipologia, 
      fdes_tipologia, fcontnote_fecha_promesa, fcontnote_monto_promesa,

      fcontnote_boleta_numero, fcontnote_boleta_fecha,
      fcontnote_boleta_monto, fcontnote_cuenta, 
      fcontnote_boleta_copia, fcontnote_justificacion,
      
      fcontnote_rec_muser, fcontnote_rec_mstat, fcontnote_when,
      fcontnote_estado
       

END FUNCTION 

FUNCTION setAttr(id)
DEFINE  i, id  SMALLINT
     
   FOR i=1 TO reg_det.getLength()
      LET reg_det_attr[i].contnote_contact         = "black"
      LET reg_det_attr[i].contnote_num             = "black"
      LET reg_det_attr[i].contact_operacion        = "black"
      LET reg_det_attr[i].contnote_rec_mtime       = "black"
      LET reg_det_attr[i].contact_estado_caso      = "black"
      LET reg_det_attr[i].contnote_text            = "black"
      LET reg_det_attr[i].contnote_cod_tipologia   = "black"
      LET reg_det_attr[i].des_tipologia            = "black"
      LET reg_det_attr[i].contnote_fecha_promesa   = "black"
      LET reg_det_attr[i].contnote_monto_promesa   = "black"

      LET reg_det_attr[i].contnote_boleta_numero   = "black"
      LET reg_det_attr[i].contnote_boleta_fecha    = "black"
      LET reg_det_attr[i].contnote_boleta_monto    = "black"
      LET reg_det_attr[i].contnote_cuenta          = "black"
      LET reg_det_attr[i].contnote_boleta_copia    = "black"
      LET reg_det_attr[i].contnote_justificacion   = "black"
      
      LET reg_det_attr[i].contnote_rec_muser       = "black"
      LET reg_det_attr[i].contnote_rec_mstat       = "black"
      LET reg_det_attr[i].contnote_when            = "black"
      LET reg_det_attr[i].contnote_estado          = "black"
   END FOR  

   LET reg_det_attr[id].contnote_contact        = "blue reverse"
   LET reg_det_attr[id].contnote_num            = "blue reverse"
   LET reg_det_attr[id].contact_operacion       = "blue reverse"
   LET reg_det_attr[id].contnote_rec_mtime      = "blue reverse"
   LET reg_det_attr[id].contact_estado_caso     = "blue reverse"
   LET reg_det_attr[id].contnote_text           = "blue reverse"
   LET reg_det_attr[id].contnote_cod_tipologia  = "blue reverse"
   LET reg_det_attr[id].des_tipologia           = "blue reverse"
   LET reg_det_attr[id].contnote_fecha_promesa  = "blue reverse"
   LET reg_det_attr[id].contnote_monto_promesa  = "blue reverse"

   LET reg_det_attr[id].contnote_boleta_numero   = "blue reverse"
   LET reg_det_attr[id].contnote_boleta_fecha    = "blue reverse"
   LET reg_det_attr[id].contnote_boleta_monto    = "blue reverse"
   LET reg_det_attr[id].contnote_cuenta          = "blue reverse"
   LET reg_det_attr[id].contnote_boleta_copia    = "blue reverse"
   LET reg_det_attr[id].contnote_justificacion   = "blue reverse"
   
   LET reg_det_attr[id].contnote_rec_muser      = "blue reverse"
   LET reg_det_attr[id].contnote_rec_mstat      = "blue reverse"
   LET reg_det_attr[id].contnote_when           = "blue reverse"
   LET reg_det_attr[id].contnote_estado         = "blue reverse"
   
END FUNCTION 

--Muestra los documentos en el arreglo de pantalla con toda la información
FUNCTION display_docs(lcontnote_num)
   DEFINE lcontnote_num LIKE contnote.contnote_num
   DEFINE pos           SMALLINT 

   CALL det_doc.CLEAR()
   DECLARE ccurdocs CURSOR FOR 
      SELECT contact_num, 
             contnote_num, 
             contdocs_id, contdocs_tipo, contdocs_desc
             --contdocs_doc
         FROM contnotedocs
         WHERE contnote_num = lcontnote_num
   LET pos = 1
   --LOCATE det_doc[pos].contdocs_doc IN FILE 
   FOREACH ccurdocs INTO det_doc[pos].contact_num, det_doc[pos].contnote_num,
      det_doc[pos].contdocs_id, det_doc[pos].contdocs_tipo, det_doc[pos].contdocs_desc

      --Para combobox "hay documento"
      SELECT contdocs_doc FROM contnotedocs 
         WHERE contact_num    = det_doc[pos].contact_num
         AND   contnote_num   = det_doc[pos].contnote_num
         AND   contdocs_id    = det_doc[pos].contdocs_id
      IF sqlca.sqlcode = 0 THEN 
         LET det_doc[pos].load_doc = TRUE 
      ELSE 
         LET det_doc[pos].load_doc = FALSE  
      END IF  
      
      LET pos = pos+1
      
   END FOREACH
   CALL det_doc.deleteElement(pos)

   DISPLAY ARRAY det_doc TO sDetDoc.*
      BEFORE DISPLAY 
         EXIT DISPLAY
   END DISPLAY
   RETURN pos - 1
END FUNCTION 

--Muestra el detalle de pagos en el arreglo de pantalla con toda la información
FUNCTION display_pagos(lcontnote_num)
   DEFINE lcontnote_num LIKE contnote.contnote_num
   DEFINE pos           SMALLINT 

   CALL det_pago.CLEAR()
   DECLARE ccurpagos CURSOR FOR 
      SELECT contnote_num, contpago_num, 
            contpago_bol_numero, contpago_bol_monto, 
            contpago_bol_fecha, contpago_bol_copia, 
            contpago_justificacion
         FROM contnotepagos
         WHERE contnote_num = lcontnote_num
   LET pos = 1
   
   FOREACH ccurpagos INTO det_pago[pos].contnote_num, det_pago[pos].contpago_num,
      det_pago[pos].contpago_bol_numero, det_pago[pos].contpago_bol_monto, 
      det_pago[pos].contpago_bol_fecha, det_pago[pos].contpago_bol_copia,
      det_pago[pos].contpago_justificacion

      LET pos = pos+1
      
   END FOREACH
   CALL det_pago.deleteElement(pos)

   DISPLAY ARRAY det_pago TO sDetPago.*
      BEFORE DISPLAY 
         EXIT DISPLAY
   END DISPLAY
   RETURN pos - 1
END FUNCTION 

FUNCTION sverdocs()
   
   DISPLAY ARRAY det_doc TO sDetDoc.*

      ON ACTION showdoc
         CALL showdoc(det_doc[arr_curr()].contact_num,det_doc[arr_curr()].contnote_num,det_doc[arr_curr()].contdocs_id)
      
      {ON ACTION regreso 
         EXIT DISPLAY}
         
   END DISPLAY
   
END FUNCTION 

--Muestra una ventana con los documentos
FUNCTION fverdocs(lcontnote_num)
   --lmodo 1=Ver  2=Editar
   DEFINE lcontnote_num LIKE contnote.contnote_num
   DEFINE pos           SMALLINT  

   --OPEN WINDOW wdocs WITH FORM "aprom0702_form02"

   CALL ga_doc.CLEAR()
   
   DECLARE ccurdocs2 CURSOR FOR 
      SELECT contact_num, 
             contnote_num, 
             contdocs_id, contdocs_tipo, contdocs_desc,
             contdocs_doc
         FROM contnotedocs
         WHERE contnote_num = lcontnote_num         
   LET pos = 1
   LOCATE ga_doc[pos].contdocs_doc IN FILE 
   FOREACH ccurdocs2 INTO ga_doc[pos].contact_num, ga_doc[pos].contnote_num,
      ga_doc[pos].contdocs_id, ga_doc[pos].contdocs_tipo, ga_doc[pos].contdocs_desc,
      ga_doc[pos].contdocs_doc
         LOCATE ga_doc[pos:=pos+1].contdocs_doc IN FILE 
   END FOREACH
   CALL ga_doc.deleteElement(pos)

   DISPLAY ARRAY ga_doc TO sDoc.*
      
      ON ACTION verdoc
         LET pos = arr_curr()
         OPEN WINDOW w5 WITH FORM "aprom0702_form03"
            MENU 
               BEFORE MENU
                  DISPLAY ga_doc[pos].contdocs_doc TO imagen1

               ON ACTION regresar
                  EXIT MENU 
            END MENU 
         CLOSE WINDOW w5
   
   END DISPLAY 
      
   CLOSE WINDOW wdocs
   
END FUNCTION  

FUNCTION showdoc(caso,nota,id)
   DEFINE caso LIKE contnotedocs.contact_num,
         nota  LIKE contnotedocs.contnote_num,
         id    LIKE contnotedocs.contdocs_id,
         doc   LIKE contnotedocs.contdocs_doc,
         tipo  LIKE contnotedocs.contdocs_tipo,
         lfilepath, rfilepath    STRING,
      filename  CHAR (20)

   LET filename = caso USING "<<<<<<<", nota USING "<<<<<<<", id USING "<<"
   
   LET rfilepath = NULL 
   SELECT contdocs_tipo, contdocs_name INTO tipo, rfilepath FROM contnotedocs d
      WHERE d.contact_num  = caso
      AND d.contnote_num   = nota
      AND d.contdocs_id    = id
   IF rfilepath IS NULL THEN
      IF tipo = "I" OR tipo = "F" THEN 
         LET rfilepath = filename CLIPPED, ".jpg"
      ELSE 
         LET rfilepath = filename CLIPPED, ".pdf"
      END IF 
   END IF 
   IF tipo = 'I' THEN 
      LOCATE doc IN FILE 
   ELSE
      LET lfilepath = rfilepath
      LOCATE doc IN FILE lfilepath  
   END IF 

   SELECT contdocs_doc INTO doc FROM contnotedocs d
      WHERE d.contact_num  = caso
      AND d.contnote_num   = nota
      AND d.contdocs_id    = id

   IF tipo = 'I' THEN 
      OPEN WINDOW w5 WITH FORM "aprom0702_form03"
         DISPLAY doc TO imagen1

         MENU 
            ON ACTION regresar
            EXIT MENU 
         END MENU 
      CLOSE WINDOW w5
   ELSE 
      DISPLAY "lfilepath ", lfilepath, " - rfilepath ", rfilepath
      CALL fgl_putfile(lfilepath, rfilepath)
   END IF 

END FUNCTION 