################################################################################
# Funcion     : %M%
# param_id : Funcion para consulta de umdores  
# Funciones   : umd_qry_init()    -> Prepara la seleccion
#               query_umd()       -> Ingreso de criterios de busqueda
#               fetch_umd()       -> Navega sobre seleccion de datos
#               clean_up()        -> Limpia el cursor de trabajo
#               display_umd()     -> Despliega el registro en pantalla
#               inicia_null()     -> inicia param_ides a nulos
#               display_estado()  -> Despliega el estado del umdor
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        param_id de la modificacion
#
################################################################################

GLOBALS "aprom0702_glob.4gl"

FUNCTION consulta(flagConsulta)
DEFINE flagConsulta BOOLEAN 
DEFINE 
   rDet tDet,
   x,i,vcon INTEGER,
   consulta STRING  

   LET g_reg.contnote_contact = arg_val(1)
   {SELECT MAX(NVL(contnote_num,0)+1) INTO g_reg.contnote_num FROM contnote
      WHERE contnote_contact = g_reg.contnote_contact
   DISPLAY BY NAME g_reg.contnote_contact, g_reg.contnote_num}

   IF flagConsulta THEN 
      CALL encabezado("Consulta")
      --CALL info_usuario()
      --Creación de la consulta
      CONSTRUCT condicion 
         ON contact_estado_caso, contnote_text, contnote_cod_tipologia, contnote_fecha_promesa, contnote_monto_promesa,
            contnote_boleta_numero, contnote_boleta_fecha, contnote_boleta_monto, contnote_cuenta, contnote_boleta_copia,
            contnote_justificacion 
         FROM f_contact_estado_caso, fcontnote_text, fcontnote_cod_tipologia, fcontnote_fecha_promesa, fcontnote_monto_promesa,
              fcontnote_boleta_numero, fcontnote_boleta_fecha, fcontnote_boleta_monto, fcontnote_cuenta, fcontnote_boleta_copia,
              fcontnote_justificacion   

         ON ACTION ACCEPT
            EXIT CONSTRUCT 
         ON ACTION CANCEL 
            CALL reg_det.clear()
            RETURN 0
             
      END CONSTRUCT 
   ELSE
      --LET condicion = " 1=1 "
      LET condicion = " contnote_contact = ", g_reg.contnote_contact
   END IF
   --Armar la consulta
   LET consulta = 
      "SELECT n.contnote_contact, n.contnote_num, c.contact_operacion, ",
      " n.contnote_rec_mtime, c.contact_estado_caso, n.contnote_text, ",
      " n.contnote_cod_tipologia, t.des_tipologia, n.contnote_fecha_promesa, ",
      " n.contnote_monto_promesa, ",
      " n.contnote_boleta_numero, n.contnote_boleta_fecha, n.contnote_boleta_monto, ",
      " n.contnote_cuenta, n.contnote_boleta_copia, n.contnote_justificacion, ", 
      " n.contnote_rec_muser, n.contnote_rec_mstat, n.contnote_when, ",
      " n.contnote_estado ",
      " FROM contnote n, outer(contact c), outer(tipologia t) ",
      " WHERE n.contnote_contact = c.contact_num AND n.contnote_cod_tipologia = t.cod_tipologia AND ", condicion,
      " ORDER BY 1"
   --DISPLAY "consulta ", consulta 
   --definir cursor con consulta de BD
   DECLARE curDet CURSOR FROM consulta
   CALL reg_det.clear()
   LET x = 0
   --Llenar el arreglo con el resultado de la consulta
   FOREACH curDet INTO rDet.contnote_contact, rDet.contnote_num, rDet.contact_operacion, 
      rDet.contnote_rec_mtime, rDet.contact_estado_caso, rDet.contnote_text, 
      rDet.contnote_cod_tipologia, rDet.des_tipologia, rDet.contnote_fecha_promesa,
      rDet.contnote_monto_promesa,
      rDet.contnote_boleta_numero, rDet.contnote_boleta_fecha, rDet.contnote_boleta_monto, 
      rDet.contnote_cuenta, rDet.contnote_boleta_copia, rDet.contnote_justificacion, 
      rDet.contnote_rec_muser, rDet.contnote_rec_mstat, rDet.contnote_when,
      rDet.contnote_estado
      LET x = x + 1 
      LET reg_det [x].* = rDet.*  
   END FOREACH
   RETURN x --Cantidad de registros
END FUNCTION
