DATABASE aprojusa

#########################################################################
## Function  : GLOBALS
##
## Parameters: none
##
## Returnings: none
##
## Comments  : Declaracion de funciones globales
#########################################################################
GLOBALS
DEFINE gUsuario   RECORD 
       usuId     LIKE users.usuid,
       usuLogin  LIKE users.user_id,
       usuPwd    LIKE users.user_auth,
       usuNombre VARCHAR(30),
       usuGrpId  LIKE users.usugrpid
END RECORD --LIKE usuario.*
CONSTANT cRaiz = -1
CONSTANT cErr = "Error. La operación no fue procesada."
CONSTANT cAddOK = "Registro agregado exitosamente"
CONSTANT cDelOK = "Registro eliminado"
CONSTANT cUpdOK = "Registro actualizado"
CONSTANT cPerOK = "Permisos actualizados"
CONSTANT prog_name = "smenu_dina"
END GLOBALS