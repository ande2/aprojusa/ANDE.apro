DATABASE aprojusa

DEFINE g_reg RECORD
   contnote_contact     LIKE contnote.contnote_contact, 
   contnote_num         LIKE contnote.contnote_num,
   contdocs_id          LIKE contnotedocs.contdocs_id, 
   contdocs_name        LIKE contnotedocs.contdocs_name,
   contdocs_doc         LIKE contnotedocs.contdocs_doc 
   
END RECORD 

DEFINE nzipcaso, nzipgral      STRING
DEFINE runcmdcaso, runcmdgral  STRING 


MAIN

   CALL fgl_settitle("Informe Diario de Documentos")
   CALL ui.Interface.loadActionDefaults("actiondefaults")

   MENU
      ON ACTION menrepdiariodoc
         CALL freporte()
      ON ACTION salir 
         EXIT MENU 
   END MENU 
   
END MAIN 

FUNCTION freporte()
DEFINE nomfile STRING 

   LET nzipgral   = "Reporte_Diario_Documentos" --, g_reg.contnote_contact USING "<<<<<<<<"
   LET nzipgral   = nzipgral CLIPPED, ".zip"
      
   LET runcmdgral = "rm ",nzipgral, "; ", "zip -r ", nzipgral 

   DECLARE cur CURSOR FOR

       SELECT DISTINCT n.contnote_contact --, n.contnote_num,
            --d.contdocs_id, d.contdocs_name 
         FROM contnote n, contnotedocs d
         WHERE n.contnote_num = d.contnote_num
         AND   d.contdocs_doc IS NOT NULL
         AND   d.contdocs_tipo = 'D'
         AND   DATE(n.contnote_rec_mtime) = TODAY
         ORDER BY 1 --,2,3

   FOREACH cur INTO g_reg.contnote_contact
      LET nzipcaso   = "Caso_", g_reg.contnote_contact USING "<<<<<<<<"
      LET nzipcaso   = nzipcaso CLIPPED, ".zip"
      
      LET runcmdcaso = "zip -r ", nzipcaso 

      DECLARE curdocs CURSOR FOR 
         SELECT   d.contnote_num, d.contdocs_id, d.contdocs_name 
            FROM  contnote n, contnotedocs d
            WHERE n.contnote_num = d.contnote_num
            AND   d.contact_num = g_reg.contnote_contact
            AND   d.contdocs_doc IS NOT NULL
            AND   d.contdocs_tipo = 'D'
            AND   DATE(n.contnote_rec_mtime) = TODAY
            ORDER BY 1, 2

      FOREACH curdocs INTO g_reg.contnote_num,
         g_reg.contdocs_id, g_reg.contdocs_name

         LOCATE g_reg.contdocs_doc IN FILE g_reg.contdocs_name
         SELECT contdocs_doc INTO g_reg.contdocs_doc
            FROM contnotedocs d 
            WHERE d.contact_num  = g_reg.contnote_contact
            AND d.contnote_num   = g_reg.contnote_num
            AND d.contdocs_id    = g_reg.contdocs_id
            
         LET nomFile = namefile2unix(g_reg.contdocs_name)
         LET runcmdcaso = runcmdcaso CLIPPED, " ", nomFile 
         
      END FOREACH
      
      --DISPLAY "runcmdcaso ", runcmdcaso
      RUN runcmdcaso
      LET runcmdgral = runcmdgral CLIPPED, " ", nzipcaso
      
   END FOREACH 
   --DISPLAY "runcmdgral final ", runcmdgral
   RUN runcmdgral
   CALL fgl_putfile(nzipgral,nzipgral)
   LET runcmdgral = "rm ",nzipgral
   RUN runcmdgral
END FUNCTION 

FUNCTION namefile2unix(oriname)
DEFINE oriname, newname STRING
DEFINE i INT  

   FOR i=1 TO oriname.getLength()
      IF oriname.getCharAt(i) = " " THEN
         LET newname = newname, "\\ "
      ELSE 
         LET newname = newname, oriname.getCharAt(i)
      END IF 
   END FOR 

   RETURN newname
   
END FUNCTION 