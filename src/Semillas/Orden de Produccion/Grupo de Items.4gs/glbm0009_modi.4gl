################################################################################
# Funcion     : %M%
# Descripcion : Funcion para modificar registros
# Funciones   : updel_init()
#	             delete_umd() 
#               update_umd()
#               pk_estado()
#               valida_modi()
#               verifica_modi()
#               existe_datos()
#               existe_dpro()
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################
GLOBALS "glbm0009_glob.4gl"

FUNCTION update_init()
DEFINE strSql STRING 

   LET strSql = 
      "UPDATE glbMItems ",
      "SET ",
      " idfamilia       = ?, ",
      " idtipo          = ?, ",
      " desItemCt       = ?, ",
      " desItemMd       = ?, ",
      " desItemLg       = ?, ",
      " variedad        = ?, ",
      " calidad         = ?, ",
      " mercado         = ?  ",
      " WHERE idItem    = ?"

   PREPARE st_modificar FROM strSql

   LET strSql = 
      "DELETE itemsxgrupo ",
      " WHERE iditem = ? "

   PREPARE st_delitemsxgrupo FROM strSql

   LET strSql =
      "INSERT INTO itemsxgrupo ( ",
        " idItem, idItemSAP, categoria, nombre, peso ",
        "   ) ",
      " VALUES (?,?,?,?,?)"

   PREPARE st_insitemsxgrupo FROM strSql
   
END FUNCTION 

FUNCTION modifica()
   CALL encabezado("Modificar")
   --CALL info_usuario()
   
   IF captura_datos('M') THEN
      RETURN actualizar()
   ELSE 
      RETURN FALSE 
   END IF 
END FUNCTION 

FUNCTION actualizar()
DEFINE i SMALLINT 

   {EXECUTE IMMEDIATE "BEGIN WORK"
   EXECUTE IMMEDIATE "SET CONSTRAINTS ALL DEFERRED"}

   --TRY
      BEGIN WORK 
      EXECUTE st_modificar USING 
      g_reg.lidFamilia, g_reg.lidTipo, g_reg.desItemCt, g_reg.desItemMd, 
      g_reg.ldesItemLg, g_reg.variedad, g_reg.calidad, g_reg.mercado,
      u_reg.lidItem
      IF sqlca.sqlcode <> 0 THEN 
         ROLLBACK WORK  
         CALL msgError(sqlca.sqlcode,"Modificar Registro")
         RETURN FALSE
      END IF 
      
      --Borrando Items x grupo
      --TRY
        --EXECUTE st_delitemsxgrupo USING g_reg.lidItem
        --TRY 
        FOR i = 1 TO gr_itemsExp.getLength()
           IF gr_itemsExp[i].idItemSAP IS NULL AND gr_itemsExp[i].nombre IS NULL THEN 
              EXIT FOR 
           END IF
           --si existe lo actualiza de lo contrario lo agrega
           SELECT * FROM itemsxgrupo t1 WHERE t1.lnkitem = gr_itemsExp[i].lnkItem 
           IF sqlca.sqlcode = 0 THEN 
              UPDATE itemsxgrupo SET (idItem, idItemSAP, categoria, nombre, peso,
                       iditemfac, iditemSAPfac,nombrefac, pesofac, 
                       fleteCaja, costoDirecto, porcentaje) =
                       (u_reg.lidItem, gr_itemsExp[i].idItemSAP, 
                       1, gr_itemsExp[i].nombre, gr_itemsExp[i].peso,
                       gr_itemsExp[i].idItemfac,gr_itemsExp[i].idItemSAPfac,
                       gr_itemsExp[i].nombrefac, gr_itemsExp[i].pesofac,
                       gr_itemsExp[i].fleteCaja, gr_itemsExp[i].costoDirecto,
                       gr_itemsExp[i].porcentaje)
               WHERE lnkitem = gr_itemsExp[i].lnkItem  
               IF sqlca.sqlcode < 0 THEN
                  ROLLBACK WORK CALL msgError(sqlca.sqlcode,"Grabar Items de Exportación") RETURN FALSE
               END IF       
           ELSE 
              IF sqlca.sqlcode = 100 THEN
                 IF gr_itemsExp[i].lnkItem IS NULL OR gr_itemsExp[i].lnkItem < 0 THEN
                    SELECT MAX (lnkItem) + 1 INTO gr_itemsExp[i].lnkItem FROM itemsxgrupo
                 END IF 
                 INSERT INTO itemsxgrupo(lnkItem, idItem, idItemSAP, categoria, nombre, peso,
                             iditemfac, iditemSAPfac,nombrefac, pesofac, 
                             fleteCaja, costoDirecto, porcentaje) 
                     VALUES (gr_itemsExp[i].lnkItem, u_reg.lidItem, gr_itemsExp[i].idItemSAP, 
                             1, gr_itemsExp[i].nombre, gr_itemsExp[i].peso,
                             gr_itemsExp[i].idItemfac,gr_itemsExp[i].idItemSAPfac,
                             gr_itemsExp[i].nombrefac, gr_itemsExp[i].pesofac,
                             gr_itemsExp[i].fleteCaja, gr_itemsExp[i].costoDirecto,
                             gr_itemsExp[i].porcentaje)   
                 IF sqlca.sqlcode < 0 THEN
                    ROLLBACK WORK CALL msgError(sqlca.sqlcode,"Grabar Items de Exportación") RETURN FALSE
                 END IF            
              ELSE 
                 IF sqlca.sqlcode < 0 THEN
                    ROLLBACK WORK CALL msgError(sqlca.sqlcode,"Grabar Items de Exportación") RETURN FALSE
                 END IF                
              END IF 
           END IF    
           
        END FOR 
        --TRY 
           
        FOR i = 1 TO gr_itemsLoc.getLength()
           IF gr_itemsLoc[i].idItemSAP IS NULL AND gr_itemsLoc[i].nombre IS NULL THEN 
              EXIT FOR 
           END IF
           --si existe lo actualiza de lo contrario lo agrega
           SELECT * FROM itemsxgrupo t1 WHERE t1.lnkitem = gr_itemsLoc[i].lnkItem
           IF sqlca.sqlcode = 0 THEN 
              UPDATE itemsxgrupo SET (idItem, idItemSAP, categoria, nombre, peso,
                       iditemfac, iditemSAPfac,nombrefac, pesofac, 
                       fleteCaja, costoDirecto, porcentaje) =
                       (u_reg.lidItem, gr_itemsLoc[i].idItemSAP, 
                       2, gr_itemsLoc[i].nombre, gr_itemsLoc[i].peso,
                       gr_itemsLoc[i].idItemfac, gr_itemsLoc[i].idItemSAPfac,
                       gr_itemsLoc[i].nombrefac, gr_itemsLoc[i].pesofac,
                       gr_itemsLoc[i].fleteCaja, gr_itemsLoc[i].costoDirecto,
                       gr_itemsLoc[i].porcentaje)
               WHERE lnkitem = gr_itemsLoc[i].lnkItem  
               IF sqlca.sqlcode < 0 THEN
                  ROLLBACK WORK CALL msgError(sqlca.sqlcode,"Grabar Items Locales") RETURN FALSE
               END IF       
           ELSE 
              IF sqlca.sqlcode = 100 THEN
                 IF gr_itemsLoc[i].lnkItem IS NULL OR gr_itemsLoc[i].lnkItem < 0 THEN
                    SELECT MAX (lnkItem) + 1 INTO gr_itemsLoc[i].lnkItem FROM itemsxgrupo
                 END IF 
                 INSERT INTO itemsxgrupo(lnkItem, idItem, idItemSAP, categoria, nombre, peso,
                             iditemfac, iditemSAPfac,nombrefac, pesofac,
                             fleteCaja, costoDirecto, porcentaje) 
                     VALUES (gr_itemsLoc[i].lnkItem, u_reg.lidItem, gr_itemsLoc[i].idItemSAP, 
                             2, gr_itemsLoc[i].nombre, gr_itemsLoc[i].peso,
                             gr_itemsLoc[i].idItemfac,gr_itemsLoc[i].idItemSAPfac,
                             gr_itemsLoc[i].nombrefac, gr_itemsLoc[i].pesofac,
                             gr_itemsLoc[i].fleteCaja, gr_itemsLoc[i].costoDirecto,
                             gr_itemsLoc[i].porcentaje)
                 IF sqlca.sqlcode < 0 THEN
                    ROLLBACK WORK CALL msgError(sqlca.sqlcode,"Grabar Items Locales") RETURN FALSE
                 END IF            
              ELSE 
                 IF sqlca.sqlcode < 0 THEN
                    ROLLBACK WORK CALL msgError(sqlca.sqlcode,"Grabar Items Locales") RETURN FALSE
                 END IF                
              END IF 
            END IF   
          END FOR 
          {EXECUTE IMMEDIATE "COMMIT WORK"}
        {CATCH 
          CALL msgError(sqlca.sqlcode,"Grabar Items Locales")
          RETURN FALSE 
        END TRY}  
      {CATCH 
        CALL msgError(sqlca.sqlcode,"Grabar Items de Exportación")
        RETURN FALSE 
      END TRY } 
      {CATCH 
         CALL msgError(sqlca.sqlcode,"Eliminar items de exportación")
         RETURN FALSE 
      END TRY}  
   {CATCH 
      CALL msgError(sqlca.sqlcode,"Modificar Registro")
      RETURN FALSE 
   END TRY}
   COMMIT WORK 
   CALL msg ("Registro actualizado")
   RETURN TRUE 
END FUNCTION 

FUNCTION delete_init()
DEFINE strSql STRING 

   LET strSql =
      "DELETE FROM glbmItems ",
      "WHERE idItem = ? "
      
   PREPARE st_delete FROM strSql

END FUNCTION 

FUNCTION anular()
   IF NOT can_delete("pemDOrdPro", "idItem", g_reg.lidItem) THEN
      CALL msg("No se puede eliminar, ya existe en orden de producción")
      RETURN FALSE 
   END IF

   IF NOT box_confirma("Esta seguro de anular el registro") THEN
      RETURN FALSE
   END IF 
   TRY 
      DELETE FROM itemsxgrupo WHERE idItem = g_reg.lidItem
      EXECUTE st_delete USING g_reg.lidItem
   CATCH 
      CALL msgError(sqlca.sqlcode,"Eliminar Registro")
      RETURN FALSE 
   END TRY
   CALL msg("Registro eliminado")
   RETURN TRUE 
END FUNCTION 