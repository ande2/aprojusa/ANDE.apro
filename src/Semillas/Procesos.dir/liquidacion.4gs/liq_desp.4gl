GLOBALS "liq_glob.4gl"


FUNCTION fbusca()
DEFINE sql_query STRING


  CALL finput("B")
  LET cuantos = 0 
  LET sql_query = "SELECT COUNT (*) FROM pemmliquidacion WHERE 1=1"

  IF gr_reg.idliq IS NOT NULL THEN
    LET sql_query = sql_query CLIPPED,
    " AND idliq = ",gr_reg.idliq USING "<<<<<<<"
  END IF
  IF gr_reg.idcliente IS NOT NULL THEN
    LET sql_query = sql_query CLIPPED,
    " AND idcliente = ",gr_reg.idcliente USING "<<<<<<<"
  END IF
  IF gr_reg.anio IS NOT NULL THEN
    LET sql_query = sql_query CLIPPED,
    " AND liqanio = ",gr_reg.anio USING "<<<<"
  END IF
  IF gr_reg.semanaliq IS NOT NULL THEN
    LET sql_query = sql_query CLIPPED,
    " AND liqsemliq = ",gr_reg.semanaliq USING "<<<<"
  END IF

  PREPARE count_query1 FROM sql_query
  EXECUTE count_query1 INTO cuantos
  IF cuantos > 0 THEN 
LET sql_query = "SELECT a.idliq, a.idcliente, a.liqanio, a.liqsemliq ",
                " FROM pemmliquidacion a WHERE 1=1"

  IF gr_reg.idliq IS NOT NULL THEN
    LET sql_query = sql_query CLIPPED,
    " AND a.idliq = ",gr_reg.idliq USING "<<<<<<<"
  END IF
  IF gr_reg.idcliente IS NOT NULL THEN
    LET sql_query = sql_query CLIPPED,
    " AND a.idcliente = ",gr_reg.idcliente USING "<<<<<<<"
  END IF
  IF gr_reg.anio IS NOT NULL THEN
    LET sql_query = sql_query CLIPPED,
    " AND a.liqanio = ",gr_reg.anio USING "<<<<"
  END IF
  IF gr_reg.semanaliq IS NOT NULL THEN
    LET sql_query = sql_query CLIPPED,
    " AND a.liqsemliq = ",gr_reg.semanaliq USING "<<<<"
  END IF
  PREPARE datos_query2 FROM sql_query
     DECLARE curBusca SCROLL CURSOR FOR datos_query2
       
  ELSE 
     RETURN FALSE   
  END IF 
  OPEN curBusca
  FETCH FIRST curBusca INTO gr_reg.idliq, gr_reg.idcliente, gr_reg.anio, gr_reg.semanaliq
  DISPLAY BY NAME gr_reg.*
  CALL dispdet1()
  CALL dispdet2()
  
  
  RETURN TRUE 
  
END FUNCTION 

FUNCTION sigAnt(fcur)
DEFINE fcur SMALLINT 
  CALL cleanAllDet()
  FETCH RELATIVE fcur curBusca INTO gr_reg.idliq, gr_reg.idcliente, gr_reg.anio, gr_reg.semanaliq
  DISPLAY BY NAME gr_reg.*
  CALL dispdet1()
  CALL dispdet2()
 
END FUNCTION 

FUNCTION dispdet1()
DEFINE cnt SMALLINT
 

    DECLARE cur05 CURSOR FOR
    SELECT b.linkfac, b.liqfacser, b.liqfacnum, b.liqfacfec,1,b.liqfacfecrec, SUM(b.liqdesemp),SUM(b.liqdesrecl),
    SUM(b.liqdesemp+b.liqdesrecl), SUM(b.liqtotalsap)
    FROM pemdliquidacion b
    WHERE b.idliq = gr_reg.idliq
    GROUP BY 1,2,3,4,5,6
    
    LET cnt = 1
    FOREACH cur05 INTO gr_det1[cnt].*
    
    LET cnt = cnt+ 1
    END FOREACH

    DISPLAY ARRAY gr_det1 TO sDet1.* ATTRIBUTES(COUNT=gr_det1.getLength())
        BEFORE DISPLAY 
        EXIT DISPLAY 
    END DISPLAY
    
END FUNCTION


FUNCTION dispdet2()
DEFINE cnt SMALLINT
 

    DECLARE cur06 CURSOR FOR
    SELECT c.linkfac, YEAR(c.liqfacfec), c.liqfacsemfec, c.liqfacser, c.liqfacnum, c.liqfacfec, c.liqfacfecrec, c.liqfacsemrec,
    c.lnkItem, 0, c.liqcodigosap, "", c.liqcajassap, c.liqpreciosap, c.liqtotalsap,c.liqcajasing, c.liqprecioing, c.liqtotaling, 
    c.liqpreciocliente, c.liqtotalcliente,
    c.liqdesemp, c.liqdesrecl, c.liqprecondes, c.liqtotalcondes, c.liqdifenprecio, c.liqdiftotalenprecio,
    c.liqnotacnotad, c.liqnumncnd 
    FROM pemdliquidacion c
    WHERE c.idliq = gr_reg.idliq

    LET cnt = 1
    FOREACH cur06 INTO gr_det2[cnt].*
        CALL obtPresentacion(cnt) RETURNING gr_det2[cnt].presentacion2, gr_det2[cnt].lnkItem, gr_det2[cnt].peso
        LET cnt = cnt+ 1
    END FOREACH

    DISPLAY ARRAY gr_det2 TO sDet2.* ATTRIBUTES(COUNT=gr_det2.getLength())
        BEFORE DISPLAY 
        EXIT DISPLAY 
    END DISPLAY
    
END FUNCTION
