SCHEMA db0001  
GLOBALS
  DEFINE gr_reg RECORD
    idliq INTEGER,
    idcliente   INTEGER,
    nombre      VARCHAR(50,1),
    anio        SMALLINT,
    semanaliq   SMALLINT
    
  END RECORD
  

  DEFINE gr_det1 DYNAMIC ARRAY OF RECORD
    linkfac     INTEGER,
    fac_ser     VARCHAR(30,1),
    fac_num     VARCHAR(30,1),
    fac_fec     DATE,
    fac_sel     SMALLINT,
    fecharec    DATE,
    des_emp     DECIMAL(18,2),
    des_rec     DECIMAL(18,2),
    tot_des     DECIMAL(18,2),
    tot_facturas    DECIMAL(18,2)
  END RECORD 

  DEFINE gr_det2 DYNAMIC ARRAY OF RECORD
   linkfac2         INTEGER,
   anio2            SMALLINT,
   semanaliq2       SMALLINT,
   facser2          VARCHAR(30,1),
   factura2         VARCHAR(50,1),
   fechafac2        DATE,
   fecharec2        DATE,
   semanarec2       SMALLINT,
   lnkItem          INTEGER,
   peso             DECIMAL(6,2),
   producto2        VARCHAR(50,1),
   presentacion2    VARCHAR(50,1),
   cajassap2           SMALLINT,
   preciosap2       DECIMAL(18,2),
   totalsap2           DECIMAL(18,2),
   cajasing2           SMALLINT,
   precioing2       DECIMAL(18,2),
   totaling2           DECIMAL(18,2),
   preciocliente2   DECIMAL(18,2),
   totalcliente2    DECIMAL(18,2),
   descuentoempaque2 DECIMAL(18,2),
   descuentoreclamo2 DECIMAL(18,2),
   preciocondescuento2 DECIMAL(6,2),
   totalcondescuento2 DECIMAL(18,2),
   difenprecio2 DECIMAL(18,2),
   diftotalprecio2 DECIMAL(18,2),
   notacnotad2  VARCHAR(30,1),
   nunotacnotad2       VARCHAR(30,1)
   
   
  END RECORD 

  
  
  DEFINE resultado BOOLEAN
  DEFINE ltipo, i, j, idx, idx2, det2, insdet2 SMALLINT 
  DEFINE lidordpro, liddordpro, lproductor, litem, vpesototal INTEGER 
  DEFINE vEstructura, vProductor, vItem VARCHAR (100) 
  DEFINE vdescripcion, lcondi VARCHAR (100)
  DEFINE lPesoCaja, lPesoTotal, lSumPeso, lPesoSaldo, vpeso LIKE pemmboleta.pesototal
  DEFINE cuantos SMALLINT 
END GLOBALS
