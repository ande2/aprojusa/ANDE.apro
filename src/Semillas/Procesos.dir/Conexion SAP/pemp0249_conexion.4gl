DATABASE db0001

DEFINE runcmd STRING 
DEFINE res SMALLINT
DEFINE cnt1, scr1 SMALLINT
DEFINE reg RECORD
  comando           STRING ,
  tipoOperacion     TINYINT ,
  servidorBD        STRING ,
  usuarioBD         STRING ,
  claveBD           STRING ,
  servidorLicencias STRING ,
  nombreBD          STRING ,
  usuarioSAP        STRING ,
  claveSAP          STRING ,

  numeroBoleta      STRING ,
  
  numOrdenPro       STRING ,
  fechaContab       STRING ,
  serieReciboPro    STRING ,
  almacenRecibo     STRING ,
  
  serieSalidaMer    STRING ,
  
  serieEntradaMer   STRING ,
  almacenEntrada    STRING ,
  
  numItemsExp       TINYINT   


END RECORD 

DEFINE det DYNAMIC ARRAY OF RECORD 
  codigoItem        STRING ,
  descripcion       STRING ,
  costo             DECIMAL(10,2),
  cantidad          DECIMAL(10,2),
  ctaconsalida      STRING ,
  ctaconentrada     STRING 
END RECORD 

DEFINE result RECORD
  numReciboPro      STRING ,
  numSalidaInv      STRING ,
  numEntradaInv     STRING ,
  numEntMerExp      STRING ,
  numEntMerLoc      STRING ,
  ctaConSalida      STRING ,
  ctaConEntrada     STRING 
END RECORD 

MAIN
CALL ui.Interface.loadStyles("styles_sc")
  --detalle tdetac azul arial 7pt
OPEN WINDOW w1 WITH FORM "pemp0248_form1"
  MENU
  #1 = Validaci�n de costo
  #2 = Recibo/Salida/Entrada
  #3 = Entrada por proveedores
  #4 = Solo Salida/Entrada
  #5 = Solo Entrada
  #** El resto de campos se env�an igual

  --ON ACTION ValidaCosto
  ON ACTION Local
    CALL fInput(2)
    CALL ejecutaIngreso()
    CALL leeResultado()

  ON ACTION Productores
    CALL fInput(3)
    CALL ejecutaIngreso()
    CALL leeResultado()

  ON ACTION LocalSalidaEntrada
    CALL fInput(4)
    CALL ejecutaIngreso()
    CALL leeResultado()

  ON ACTION LocalEntrada
    CALL fInput(5)
    CALL ejecutaIngreso()
    CALL leeResultado()

  ON ACTION envio
     LET runcmd = 'java -jar C:\\\\tmp\\\\dist\\\\sc_conector_sap.jar 2 192.168.0.2 sa Kmaleon2 192.168.0.2 "WPruebas Semillas" manager 0313231719 5453 21-04-2014 195 196 195 1 RC-T100 140.042.005 "CHILE BLOCKY (LIBRA) FINCAS CM" 2.22 200 SR-0400 "_SYS00000001152" 305.240.003 "TOMATE CHERRY (LB) EXPORTACI�N" 5.85 100 PH-0200'
     CALL ui.interface.frontcall("standard","shellexec",[runcmd],[res])
     DISPLAY "Resultado ", res
     DISPLAY runcmd

  ON ACTION salir 
    EXIT MENU

END MENU 

END MAIN 

FUNCTION fInput(operacion)
  DEFINE operacion STRING 
  DEFINE lcategoria STRING

	IF operacion = 2 THEN
		LET lCategoria = " AND a.categoria = 1 "
	ELSE
		LET lCategoria = Null
	END IF
  DIALOG 

	  INPUT BY NAME reg.comando ,           
						 reg.tipoOperacion ,     reg.servidorBD ,
						 reg.usuarioBD ,         reg.claveBD ,
						 reg.servidorLicencias , reg.nombreBD ,
						 reg.usuarioSAP ,        reg.claveSAP ,
						 reg.numeroBoleta

		 BEFORE INPUT
			--datos de conexi�n
			LET reg.comando       = "java -jar C:\\\\tmp\\\\dist\\\\sc_conector_sap.jar"
			LET reg.tipoOperacion = 2
			LET reg.servidorBD    = "192.168.0.2"
			LET reg.usuarioBD     = "sa"
			LET reg.claveBD       = "Kmaleon2"
			LET reg.servidorLicencias = "192.168.0.2"
			LET reg.nombreBD      = "WPruebas Semillas"
			LET reg.usuarioSAP    = "manager"
			LET reg.claveSAP      = "0313231719"

			--datos de la boleta
			LET reg.numeroBoleta  = "RC-T100"

			--Datos de Recibo de producci�nD      
			LET reg.numOrdenPro       = "5453"
			LET reg.fechaContab       = "21-04-2014"
			LET reg.serieReciboPro    = "195"
			LET reg.almacenRecibo     = "SR-0400"

			--Datos de Salida de Mercanc�a
			LET reg.serieSalidaMer    = "196"

			--Datos de Entrada de Mercanc�a
			LET reg.serieEntradaMer   = "195"
			LET reg.almacenEntrada    = "PH-0200"

			--Datos de items
			LET reg.numItemsExp   = 1
			
			DISPLAY BY NAME reg.numeroBoleta, reg.comando,    reg.tipoOperacion, 
								 reg.servidorBD,   reg.usuarioBD,  reg.usuarioBD, 
								 reg.claveBD,      reg.servidorLicencias,  
								 reg.nombreBD,     reg.usuarioSAP, reg.claveSAP ,
								 reg.numeroBoleta, 
								 reg.numOrdenPro,  reg.fechaContab,reg.serieReciboPro,
								 reg.almacenRecibo,
								 reg.serieSalidaMer,
								 reg.serieEntradaMer, reg.almacenEntrada,
								 reg.numItemsExp

			NEXT FIELD numeroBoleta

	  END INPUT   

	  INPUT ARRAY det FROM sdet.*

			BEFORE ROW
				LET cnt1 = ARR_CURR()
				LET scr1 = SCR_LINE()

	  END INPUT

		ON ACTION envio
			LET reg.tipoOperacion = GET_FLDBUF(tipoOperacion)
			CALL ejecutaIngreso()

		ON ACTION buscar
			CASE 
				WHEN INFIELD(codigoitem)
					CALL picklist_2("Busqueda de items","Codigo","Descripcion","a.iditemsap","a.nombre","itemsxgrupo a"," 1=1 "||lcategoria,1,1)
					RETURNING det[cnt1].codigoItem , det[cnt1].descripcion , INT_FLAG
					DISPLAY det[cnt1].* TO sdet[scr1].*
			END CASE

		ON ACTION salir
			EXIT DIALOG

  END DIALOG 

END FUNCTION 

FUNCTION ejecutaIngreso()
DEFINE cmd STRING
DEFINE i INTEGER
DEFINE lTipoOperacion INTEGER
    IF reg.tipoOperacion = 1 THEN
        LET lTipoOperacion = 6
    ELSE
        LET lTipoOperacion = reg.tipoOperacion
    END IF
	LET cmd = reg.comando," ",lTipoOperacion," ",reg.servidorBD," ",reg.usuarioBD," ",
						reg.claveBD," ",reg.servidorLicencias," ",reg.nombreBD," ",reg.usuarioSAP," ",
						reg.numOrdenPro," ",reg.fechaContab," ",reg.serieReciboPro," ",reg.serieSalidaMer," ",
						reg.numItemsExp," ",reg.numeroBoleta

	FOR i = 1 TO det.getLength()
		IF det[i].codigoItem IS NULL THEN
			CONTINUE FOR
		END IF

		LET cmd = cmd," ",det[i].codigoItem,'"',det[i].descripcion CLIPPED,'" ',
							det[i].costo USING "<<<<<<##.##"," ",
							det[i].cantidad USING "<<<<<<##.##"," ", reg.almacenRecibo

    END FOR

    CALL ui.interface.frontcall("standard","shellexec",[cmd],[res])
	DISPLAY "Resultado ", res

	CALL leeResultado()
END FUNCTION 

FUNCTION leeResultado()
DEFINE PathWin , PathUnix STRING
DEFINE texto STRING
DEFINE accion DYNAMIC ARRAY OF CHAR(3)
DEFINE i INTEGER

	CALL accion.clear()

	CASE reg.tipoOperacion
		WHEN 1
			LET accion[6] = "-06"
		WHEN 2
			LET accion[6] = "-06"
			LET accion[1] = "-01"
			LET accion[2] = "-02"
			LET accion[3] = "-03"
		WHEN 3
			LET accion[4] = "-04"
		WHEN 4
			LET accion[2] = "-02"
			LET accion[3] = "-03"
		WHEN 5
			LET accion[5] = "-05"
		OTHERWISE
			LET accion = NULL
	END CASE

	FOR i = 1 TO accion.getLength()
		IF accion[i] IS NULL THEN
			CONTINUE FOR
		END IF
		LET PathWin = "C:\\tmp\\dist\\",reg.numeroBoleta CLIPPED,accion[i] CLIPPED,".txt"
		LET PathUnix = FGL_GETENV("HOME")
		LET PathUnix = PathUnix CLIPPED,"/",reg.numeroBoleta CLIPPED,accion[i] CLIPPED,".txt"
		CALL librut001_getfile(PathWin,PathUnix)

		LET texto = librut001_readfile(PathUnix,1)

		DISPLAY "texto: ",texto

		CASE i
			WHEN 1
				LET result.numReciboPro = texto
			WHEN 2
				LET result.numSalidaInv = texto
			WHEN 3
				LET result.numEntradaInv = texto
			WHEN 4
				LET result.numReciboPro = texto
			WHEN 5
				LET result.numReciboPro = texto
            WHEN 6
				DISPLAY texto
		END CASE
        CALL box_valdato(texto||' '||i)
	END FOR
{
	result RECORD
		numReciboPro      STRING ,
		numSalidaInv      STRING ,
		numEntradaInv     STRING ,
		numEntMerExp      STRING ,
		numEntMerLoc      STRING ,
		ctaConSalida      STRING ,
		ctaConEntrada     STRING
	}

END FUNCTION