GLOBALS "cliq_glob.4gl"

FUNCTION finput(accion)
  DEFINE accion CHAR (1)
  DEFINE msgTxt, sqlTxt STRING 
  DEFINE pesoFalta SMALLINT 
  DEFINE scr_idx2, cnt,i SMALLINT
  DEFINE vdesc_emp DECIMAL(18,2)
  DEFINE vdesc_rec DECIMAL(18,2)
  

  IF accion = "I" OR accion = "B" THEN 
     CALL inicializatmp()
  ELSE 
     --CALL llenatmpitems()  
     ----SELECT SUM (pesoTotal) INTO lPesoTotal FROM tmpdet2
     --SELECT SUM (pesoTotal) INTO lPesoTotal FROM pemmbo 
     --LET lPesoSaldo = lPesoTotal
  END IF 
  LET insdet2 = 0

  CALL llenatmp()
  --CALL dialog.setactionactive("close",0)
    IF accion = "I" OR accion = "B" THEN 
        INITIALIZE gr_reg.* TO NULL
        LET gr_reg.anio = YEAR(TODAY)
    
    END IF  
   
           
           
   DIALOG ATTRIBUTES(UNBUFFERED)
     
     INPUT BY NAME gr_reg.idconliq, gr_reg.anio, gr_reg.semanaliq
       ATTRIBUTES  (WITHOUT DEFAULTS)
        
        BEFORE INPUT 
           CALL dialog.setActionHidden("close",1)

        ON ACTION buscar
          LABEL opcionBusca:
          

        BEFORE FIELD idconliq
            IF accion = "I" THEN
                LET gr_reg.idconliq = 0
                DISPLAY BY NAME gr_reg.idconliq
                NEXT FIELD NEXT
            END IF
            
        BEFORE FIELD anio
        
            CALL cleandet1()
            CALL mostrarDet1()

            
        AFTER INPUT 
           IF accion = "B" THEN
              RETURN 
           ELSE

                IF gr_reg.anio IS NULL THEN
                    CALL box_valdato("Debe especificar el anio")
                    NEXT FIELD anio
                END IF
                IF gr_reg.semanaliq IS NULL THEN
                    CALL box_valdato("Debe especificar la semana de liquidacion")
                    NEXT FIELD semanaliq
                END IF
           END IF
     END INPUT 
     
     
     INPUT ARRAY gr_det1 FROM sDet1.* ATTRIBUTES (WITHOUT DEFAULTS, COUNT=gr_det1.getLength() )

       BEFORE INPUT 
         LABEL labelDet1:
         LET idx2 = arr_curr()
         

       BEFORE ROW 
         LET idx2 = arr_curr()  

        ON CHANGE liq_sel
            CALL mostrarDet2()

        
         
       AFTER ROW 
          LET idx2 = arr_curr()
        
        AFTER INPUT
            CALL mostrarDet2()
     END INPUT

     
     INPUT ARRAY gr_det2 FROM sDet2.* ATTRIBUTES (COUNT=gr_det2.getLength())

         
       
     END INPUT 
     
     ON ACTION ACCEPT

     IF accion = "B" THEN
              RETURN 
     ELSE
        IF gr_reg.anio IS NULL THEN
            CALL box_valdato("Debe especificar el anio")
            NEXT FIELD anio
        END IF
        IF gr_reg.semanaliq IS NULL THEN
            CALL box_valdato("Debe especificar la semana de liquidacion")
            NEXT FIELD semanaliq
        END IF
        LET cnt = 0
        FOR i = 1 TO gr_det1.getLength()
            IF gr_det1[i].liq_sel = 1 THEN
                LET cnt = cnt+1
            END IF
        END FOR

        IF cnt = 0 THEN
            CALL msg("Debe especificar liquidaciones a consolidar")
            NEXT FIELD anio
        END IF
    
        EXIT DIALOG 

     END IF
      
     ON ACTION CANCEL
        CLEAR FORM 
        
        --LET gr_reg.cliente  = NULL 
        RETURN --EXIT dialog
   END DIALOG
   --IF INT_FLAG THEN 
      --RETURN 
   --END IF 

   
   BEGIN WORK 
     --insertando inforamcion de la boleta de exportacion

    LET vdesc_emp = 0
    LET vdesc_rec = 0
     
     IF accion = "I" THEN 
        INSERT INTO pemmconsolidadoliq (idconliq, 
                                   cliqsemana, cliqanio)
          VALUES (0, gr_reg.semanaliq, gr_reg.anio) 
                
                  
     ELSE 
        IF accion = "U" THEN 
           UPDATE pemmconsolidadoliq SET ( liqanio, liqsemliq
                                      ) =
                  ( gr_reg.anio, gr_reg.semanaliq)
            WHERE pemmconsolidadoliq.idconliq = gr_reg.idconliq
        END IF 
     END IF  
                  
   DISPLAY "insert en detalle de clientes"
   IF sqlca.sqlcode = 0 THEN 
      IF accion = "I" THEN 
         LET gr_reg.idconliq = sqlca.sqlerrd[2]
         DISPLAY BY NAME gr_reg.idconliq
      ELSE 
         DELETE FROM pemdconsolidadoliq WHERE idconliq = gr_reg.idconliq
         DELETE FROM pemrelmconliq WHERE idconliq = gr_reg.idconliq
      END IF 

      FOR i = 1 TO gr_det1.getLength()
        IF gr_det1[i].liq_sel = 1 THEN
            INSERT INTO pemrelmconliq(idconliq,idliq)
            VALUES(gr_reg.idconliq, gr_det1[i].idliq)
        END IF
      END FOR 
    
      --Insertando en clientes
      FOR i = 1 TO gr_det2.getLength()
        IF gr_det2[i].producto IS NOT NULL THEN 
          INSERT INTO pemdconsolidadoliq (idconliq, cliqcor, cliqcodigosap, lnkitem,
          cliqtotalcajas, cliqtotalvalor, cliqprecio, cliqdesemp, cliqdescal,
          cliqcostoporcaja, cliqflete, cliqcosdirecto, cliqpordes, cliqpreciocajaprod,
          cliqpreciolibraprod)
          VALUES(gr_reg.idconliq, i, gr_det2[i].producto, gr_det2[i].lnkItem, gr_det2[i].cajasing,
            gr_det2[i].totaling, gr_det2[i].precioing, gr_det2[i].descuentoempaque, gr_det2[i].descuentoreclamo, 
            gr_det2[i].preciocondescuento, gr_det2[i].totalflete, gr_det2[i].totalcostodirecto,
            gr_det2[i].total6porc, gr_det2[i].precioporcaja, gr_det2[i].precioporlibra
            )
          
         END IF 
      END FOR 
    END IF   

      IF sqlca.sqlcode = 0 THEN
         CALL msg("Informacion grabada")
         --ROLLBACK WORK
         COMMIT WORK
      ELSE 
         CALL msg("Error al grabar información 2")
         ROLLBACK WORK 
      END IF 
   
END FUNCTION 

####### Encabezado ##############

FUNCTION creatmp()


END FUNCTION 

FUNCTION inicializatmp()
{
  DELETE FROM tmpfacturas
  DELETE FROM tmpdetfacturas
  
   FOR i = 1 TO gr_det1.getLength()
      INITIALIZE gr_det1[i].* TO NULL
   END FOR 
   FOR i = 1 TO gr_det2.getLength()
      INITIALIZE gr_det2[i].* TO NULL
   END FOR
   }
   CLEAR FORM 
END FUNCTION

FUNCTION llenatmp()
 

   
   
END FUNCTION 


FUNCTION cleandet1()
  FOR i = 1 TO gr_det1.getLength()
      INITIALIZE gr_det1[i].* TO NULL
   END FOR 
   FOR i = gr_det1.getLength() TO 1 STEP -1
     CALL gr_det1.deleteElement(i)
   END FOR
     
END FUNCTION

FUNCTION mostrarDet1()
DEFINE cnt SMALLINT

DECLARE cur_0001 CURSOR FOR
SELECT a.idliq, a.idcliente, b.nombre, a.liqsemliq,0
FROM pemmliquidacion a, pemmcli b
WHERE a.id_estado = 5
--AND a.liqsemliq = gr_reg.semanaliq
AND b.idcliente = a.idcliente


LET cnt = 1
FOREACH cur_0001 INTO gr_det1[cnt].idliq, gr_det1[cnt].idcliente, gr_det1[cnt].nombre, gr_det1[cnt].idsemliq, gr_det1[cnt].liq_sel
 LET cnt = cnt+ 1
END FOREACH

 DISPLAY ARRAY gr_det1 TO sDet1.* ATTRIBUTES (COUNT=gr_det1.getLength())
    BEFORE DISPLAY 
      EXIT DISPLAY 
  END DISPLAY

END FUNCTION

##### Detalle 2 ##########

FUNCTION mostrarDet2()
DEFINE cnt, i SMALLINT
DEFINE vliquidacion STRING
DEFINE sqlquery STRING

  CALL cleandet2()

  LET vliquidacion = "0"
  FOR i = 1 TO gr_det1.getLength()
    IF gr_det1[i].liq_sel = 1 THEN
        LET vliquidacion = vliquidacion CLIPPED,",",gr_det1[i].idliq USING "<<<<<<<" 
    END IF
  END FOR
  
  LET sqlquery = "SELECT a.liqcodigosap, SUM(a.liqcajasing), SUM(a.liqtotaling), ",
               " SUM(a.liqdesemp), SUM(a.liqdesrecl), SUM(a.liqtotalcondes) ",
               " FROM pemdliquidacion a",
               " WHERE a.idliq IN (",vliquidacion CLIPPED,") GROUP BY 1 ORDER BY 1"

  PREPARE pre_fac FROM sqlquery
  DECLARE cur003 CURSOR FOR pre_fac
    
  LET cnt = 1
  FOREACH cur003 INTO gr_det2[cnt].producto, gr_det2[cnt].cajasing, gr_det2[cnt].totaling,
  gr_det2[cnt].descuentoempaque, gr_det2[cnt].descuentoreclamo, gr_det2[cnt].totalcondescuento
  
    CALL obtPresentacion(cnt) RETURNING gr_det2[cnt].presentacion, gr_det2[cnt].lnkItem, 
                                        gr_det2[cnt].peso, gr_det2[cnt].totalflete, gr_det2[cnt].totalcostodirecto
                                         
    LET gr_det2[cnt].precioing = ObtPrecioing(cnt)
    LET gr_det2[cnt].preciocondescuento = ObtPrecioConDescuento(cnt)
    LET gr_det2[cnt].total6porc = ObtDescuento6porc(cnt)
    LET gr_det2[cnt].precioporcaja = ObtPrecioxCaja(cnt)
    LET gr_det2[cnt].precioporlibra = ObtPrecioxLibra(cnt)
    
    
    LET cnt = cnt + 1

  END FOREACH
  
  DISPLAY ARRAY gr_det2 TO sDet2.* ATTRIBUTES (COUNT=gr_det2.getLength())
    BEFORE DISPLAY 
      EXIT DISPLAY 
  END DISPLAY  
END FUNCTION 



FUNCTION cleandet2()
  FOR i = 1 TO gr_det2.getLength()
      INITIALIZE gr_det2[i].* TO NULL
   END FOR 
   FOR i = gr_det2.getLength() TO 1 STEP -1
     CALL gr_det2.deleteElement(i)
   END FOR 
   
END FUNCTION 





FUNCTION cleanAllDet()
  CALL cleanDet2()
  CALL cleanDet1()

END FUNCTION 

FUNCTION validaLinea(idx)
DEFINE i, idx SMALLINT 

  FOR i = 1 TO gr_det2.getLength()
    
  END FOR 
  
  RETURN FALSE  
END FUNCTION

