GLOBALS "liq_glob.4gl"

FUNCTION finput(accion)
  DEFINE accion CHAR (1)
  DEFINE msgTxt, sqlTxt STRING 
  DEFINE pesoFalta SMALLINT 
  DEFINE scr_idx2, cnt,i SMALLINT
  DEFINE vdesc_emp DECIMAL(18,2)
  DEFINE vdesc_rec DECIMAL(18,2)
  

  IF accion = "I" OR accion = "B" THEN 
     CALL inicializatmp()
  ELSE 
     --CALL llenatmpitems()  
     ----SELECT SUM (pesoTotal) INTO lPesoTotal FROM tmpdet2
     --SELECT SUM (pesoTotal) INTO lPesoTotal FROM pemmbo 
     --LET lPesoSaldo = lPesoTotal
  END IF 
  LET insdet2 = 0

  CALL llenatmp()
  --CALL dialog.setactionactive("close",0)
    IF accion = "I" OR accion = "B" THEN 
        INITIALIZE gr_reg.* TO NULL
        LET gr_reg.anio = YEAR(TODAY)
    
    END IF  
   
           
           
   DIALOG ATTRIBUTES(UNBUFFERED)
     
     INPUT BY NAME gr_reg.idliq, gr_reg.idcliente, gr_reg.anio, gr_reg.semanaliq
       ATTRIBUTES  (WITHOUT DEFAULTS)
        
        BEFORE INPUT 
           CALL dialog.setActionHidden("close",1)

        ON ACTION buscar
          LABEL opcionBusca:
          
          CALL picklist_2("Clientes", "Codigo","Descripción","idcliente", "nombre", "pemmcli", "1=1", "1", 0)
            RETURNING gr_reg.idcliente, vdescripcion, INT_FLAG
          --Que no sea nulo
           IF gr_reg.idcliente IS NULL THEN
              CALL msg("Debe ingresar un cliente valido")
              NEXT FIELD idcliente
            ELSE
                LET gr_reg.nombre=vdescripcion
                DISPLAY BY NAME gr_reg.nombre
           END IF

        BEFORE FIELD idliq
            IF accion = "I" THEN
                LET gr_reg.idliq = 0
                DISPLAY BY NAME gr_reg.idliq
                NEXT FIELD NEXT
            END IF
            
        ON CHANGE idcliente
            SELECT nombre
            INTO gr_reg.nombre
            FROM pemmcli
            WHERE pemmcli.idcliente = gr_reg.idcliente

            IF SQLCA.sqlcode = NOTFOUND THEN
                CALL box_valdato("Codigo de cliente no existe")
                LET gr_reg.nombre = NULL
                LET gr_reg.idcliente = NULL
                DISPLAY BY NAME gr_reg.nombre, gr_reg.idcliente
                NEXT FIELD idcliente
            END IF
            
            DISPLAY BY NAME gr_reg.nombre
            CALL cleandet1()
            CALL mostrarDet1()

            
        AFTER INPUT 
           IF accion = "B" THEN
              RETURN 
           ELSE

                IF gr_reg.idcliente IS NULL THEN
                    CALL box_valdato("Debe especificar un cliente")
                    NEXT FIELD idcliente
                END IF
                IF gr_reg.anio IS NULL THEN
                    CALL box_valdato("Debe especificar el anio")
                    NEXT FIELD anio
                END IF
                IF gr_reg.semanaliq IS NULL THEN
                    CALL box_valdato("Debe especificar la semana de liquidacion")
                    NEXT FIELD semanaliq
                END IF
           END IF
     END INPUT 
     
     
     INPUT ARRAY gr_det1 FROM sDet1.* ATTRIBUTES (WITHOUT DEFAULTS, COUNT=gr_det1.getLength() )

       BEFORE INPUT 
         LABEL labelDet1:
         LET idx2 = arr_curr()
         

       BEFORE ROW 
         LET idx2 = arr_curr()  

        ON CHANGE fac_sel
            CALL mostrarDet2()

        ON CHANGE des_emp
            LET idx2 = arr_curr()
            LET scr_idx2 = scr_line()
            LET gr_det1[idx2].tot_des = NVL(gr_det1[idx2].des_emp,0) + NVL(gr_det1[idx2].des_rec,0)
            DISPLAY gr_det1[idx2].tot_des TO sDet1[scr_idx2].tot_des
            CALL mostrarDet2()

       ON CHANGE des_rec
            LET idx2 = arr_curr()
            LET scr_idx2 = scr_line()
            LET gr_det1[idx2].tot_des = NVL(gr_det1[idx2].des_emp,0) + NVL(gr_det1[idx2].des_rec,0)
            DISPLAY gr_det1[idx2].tot_des TO sDet1[scr_idx2].tot_des
            CALL mostrarDet2()
 
        
         
       AFTER ROW 
          LET idx2 = arr_curr()
        
        AFTER INPUT
            CALL mostrarDet2()
     END INPUT

     
     INPUT ARRAY gr_det2 FROM sDet2.* ATTRIBUTES (COUNT=gr_det2.getLength())

         
       
     END INPUT 
     
     ON ACTION ACCEPT

     IF accion = "B" THEN
              RETURN 
     ELSE
        IF gr_reg.idcliente IS NULL THEN
            CALL box_valdato("Debe especificar un cliente")
            NEXT FIELD idcliente
        END IF
        IF gr_reg.anio IS NULL THEN
            CALL box_valdato("Debe especificar el anio")
            NEXT FIELD anio
        END IF
        IF gr_reg.semanaliq IS NULL THEN
            CALL box_valdato("Debe especificar la semana de liquidacion")
            NEXT FIELD semanaliq
        END IF
        LET cnt = 0
        FOR i = 1 TO gr_det1.getLength()
            IF gr_det1[i].fac_sel = 1 THEN
                LET cnt = cnt+1
            END IF
        END FOR

        IF cnt = 0 THEN
            CALL msg("Debe especificar facturas a liquidar")
            NEXT FIELD idcliente
        END IF
    
        EXIT DIALOG 

     END IF
      
     ON ACTION CANCEL
        CLEAR FORM 
        
        --LET gr_reg.cliente  = NULL 
        RETURN --EXIT dialog
   END DIALOG
   --IF INT_FLAG THEN 
      --RETURN 
   --END IF 

   
   BEGIN WORK 
     --insertando inforamcion de la boleta de exportacion

    LET vdesc_emp = 0
    LET vdesc_rec = 0
     
     IF accion = "I" THEN 
        INSERT INTO pemmliquidacion (idliq, idcliente, liqanio,
                                   liqsemliq, id_estado)
          VALUES (0, gr_reg.idcliente, gr_reg.anio,gr_reg.semanaliq, 
                  1)
                  
     ELSE 
        IF accion = "U" THEN 
           UPDATE pemmliquidacion SET (idcliente, liqanio, liqsemliq
                                      ) =
                  (gr_reg.idcliente, gr_reg.anio, gr_reg.semanaliq)
            WHERE pemmliquidacion.idliq = gr_reg.idliq
        END IF 
     END IF  
                  
   DISPLAY "insert en detalle de clientes"
   IF sqlca.sqlcode = 0 THEN 
      IF accion = "I" THEN 
         LET gr_reg.idliq = sqlca.sqlerrd[2]
         DISPLAY BY NAME gr_reg.idliq
      ELSE 
         DELETE FROM pemdliquidacion WHERE idliq = gr_reg.idliq
      END IF 

    
      --Insertando en clientes
      FOR i = 1 TO gr_det2.getLength()
        IF gr_det2[i].linkfac2 IS NOT NULL THEN 
          INSERT INTO pemdliquidacion (
          idliq,liqcor,linkfac,liqfacser,liqfacnum,liqfacfec,liqfacfecrec,liqfacsemfec, 
          liqfacsemrec,liqcodigosap,lnkitem,liqcajassap,liqpreciosap,liqtotalsap,liqcajasing,       
          liqprecioing,liqtotaling,liqpreciocliente,liqtotalcliente,liqdesemp,liqdesrecl,        
          liqprecondes,liqtotalcondes,liqdifenprecio,liqdiftotalenprecio,liqnotacnotad,liqnumncnd        
          )
          VALUES (gr_reg.idliq, i, gr_det2[i].linkfac2, gr_det2[i].facser2, gr_det2[i].factura2, gr_det2[i].fechafac2,
            gr_det2[i].fecharec2, gr_det2[i].semanarec2, gr_det2[i].semanarec2, gr_det2[i].producto2, gr_det2[i].lnkItem, gr_det2[i].cajassap2,
            gr_det2[i].preciosap2, gr_det2[i].totalsap2,gr_det2[i].cajasing2,gr_det2[i].precioing2, gr_det2[i].totaling2, 
            gr_det2[i].preciocliente2, gr_det2[i].totalcliente2, gr_det2[i].descuentoempaque2,
            gr_det2[i].descuentoreclamo2, gr_det2[i].preciocondescuento2, gr_det2[i].totalcondescuento2,gr_det2[i].difenprecio2,
            gr_det2[i].diftotalprecio2, gr_det2[i].notacnotad2, gr_det2[i].nunotacnotad2
            )
          
         END IF 
      END FOR 
    END IF   

      IF sqlca.sqlcode = 0 THEN
         CALL msg("Informacion grabada")
         --ROLLBACK WORK
         COMMIT WORK
      ELSE 
         CALL msg("Error al grabar información 2")
         ROLLBACK WORK 
      END IF 
   
END FUNCTION 

####### Encabezado ##############

FUNCTION creatmp()

    CREATE TEMP TABLE tmpfacturas
  (correlativo  SMALLINT, 
   serie        VARCHAR(30,1),
   numero_factura VARCHAR(30,1), 
   fecha        DATETIME YEAR TO FRACTION,
   codigo_item  VARCHAR(30,1),
   cantidad     SMALLINT,
   unidad_medida VARCHAR(30,1),
   precio         DECIMAL(18,2),
   descuento_linea DECIMAL(18,2),
   total_factura   DECIMAL(18,2),
   descuento_documento DECIMAL(18,2)
   ) WITH NO LOG

     CREATE TEMP TABLE tmpdetfacturas
  (correlativo  SMALLINT, 
   serie        VARCHAR(30,1),
   factura VARCHAR(30,1), 
   fecha        DATE,
   codigo_item  VARCHAR(30,1),
   cantidad     SMALLINT,
   unidad_medida VARCHAR(30,1),
   precio         DECIMAL(18,2),
   descuento_linea DECIMAL(18,2),
   total_factura   DECIMAL(18,2),
   descuento_documento DECIMAL(18,2)
   ) WITH NO LOG

END FUNCTION 

FUNCTION inicializatmp()

  DELETE FROM tmpfacturas
  DELETE FROM tmpdetfacturas
  
   FOR i = 1 TO gr_det1.getLength()
      INITIALIZE gr_det1[i].* TO NULL
   END FOR 
   FOR i = 1 TO gr_det2.getLength()
      INITIALIZE gr_det2[i].* TO NULL
   END FOR
   CLEAR FORM 
END FUNCTION

FUNCTION llenatmp()
 

  --LOAD FROM "/app/ANDE/ANDE.sem/src/Semillas/Procesos.dir/liquidacion.4gs/DEU-9999.txt"
  LOAD FROM "/app/test/SVN/ANDE.sem/src/Semillas/Procesos.dir/liquidacion.4gs/DEU-9999.txt"
  INSERT INTO tmpfacturas

  INSERT INTO tmpdetfacturas
  SELECT correlativo,
   serie,
   numero_factura,
   DATE(fecha),
   codigo_item,
   cantidad,
   unidad_medida,
   precio,
   descuento_linea,
   total_factura,
   descuento_documento
   FROM tmpfacturas
   
END FUNCTION 


FUNCTION cleandet1()
  FOR i = 1 TO gr_det1.getLength()
      INITIALIZE gr_det1[i].* TO NULL
   END FOR 
   FOR i = gr_det1.getLength() TO 1 STEP -1
     CALL gr_det1.deleteElement(i)
   END FOR
  
   
END FUNCTION

FUNCTION mostrarDet1()
DEFINE cnt SMALLINT

DECLARE cur_0001 CURSOR FOR
SELECT tmpdetfacturas.correlativo, tmpdetfacturas.serie, tmpdetfacturas.factura, tmpdetfacturas.fecha,0
,SUM(tmpdetfacturas.total_factura)
FROM tmpdetfacturas
GROUP BY 1,2,3,4,5

LET cnt = 1
FOREACH cur_0001 INTO gr_det1[cnt].linkfac, gr_det1[cnt].fac_ser, gr_det1[cnt].fac_num, gr_det1[cnt].fac_fec, gr_det1[cnt].fac_sel,
gr_det1[cnt].tot_facturas
 LET cnt = cnt+ 1
END FOREACH

 DISPLAY ARRAY gr_det1 TO sDet1.* ATTRIBUTES (COUNT=gr_det1.getLength())
    BEFORE DISPLAY 
      EXIT DISPLAY 
  END DISPLAY

END FUNCTION

##### Detalle 2 ##########

FUNCTION mostrarDet2()
DEFINE cnt, i SMALLINT
DEFINE vfacturas STRING
DEFINE sqlquery STRING

  CALL cleandet2()

  LET vfacturas = "0"
  FOR i = 1 TO gr_det1.getLength()
    IF gr_det1[i].fac_sel = 1 THEN
        LET vfacturas = vfacturas CLIPPED,",",gr_det1[i].linkfac USING "<<<<<<<" 
    END IF
  END FOR
  
  LET sqlquery = "SELECT a.correlativo,YEAR(a.fecha), a.serie, a.factura, a.fecha, a.codigo_item, a.cantidad, ",
               " (a.total_factura/a.cantidad), a.total_factura",
               " FROM tmpdetfacturas a",
               " WHERE a.correlativo IN (",vfacturas CLIPPED,") ORDER BY 1,2,3,4,5"

  PREPARE pre_fac FROM sqlquery
  DECLARE cur003 CURSOR FOR pre_fac
    
  LET cnt = 1
  FOREACH cur003 INTO gr_det2[cnt].linkfac2, gr_det2[cnt].anio2, gr_det2[cnt].facser2, gr_det2[cnt].factura2, gr_det2[cnt].fechafac2,  gr_det2[cnt].producto2, gr_det2[cnt].cajassap2, gr_det2[cnt].preciosap2, 
  gr_det2[cnt].totalsap2
  
    LET gr_det2[cnt].semanaliq2 = ObtSemanaliq(cnt)
--    LET gr_det2[cnt].fecharec2 = ObtFechaRec(cnt)
    LET gr_det2[cnt].semanarec2 = ObtSemanaRec(cnt)
    CALL obtPresentacion(cnt) RETURNING gr_det2[cnt].presentacion2, gr_det2[cnt].lnkItem, gr_det2[cnt].peso
    LET gr_det2[cnt].preciocliente2 = ObtPrecioCliente(cnt)
    LET gr_det2[cnt].totalcliente2 = ObtPrecioClienteSinDescuentoPorLibra(cnt) 
    LET gr_det2[cnt].descuentoempaque2 = ObtDescuentoEmpaque(cnt)
    LET gr_det2[cnt].descuentoreclamo2 = ObtDescuentoReclamo(cnt)
    LET gr_det2[cnt].preciocondescuento2 = ObtPrecioConDescuento(cnt)
    LET gr_det2[cnt].totalcondescuento2 = ObtPrecioConDescuentoDeCalidadPorLibra(cnt)
    LET gr_det2[cnt].difenprecio2 = ObtDifEnPrecio(cnt)
    LET gr_det2[cnt].diftotalprecio2 = ObtDifTotalEnPrecio(cnt)
    
    
    LET cnt = cnt + 1

  END FOREACH
  
  DISPLAY ARRAY gr_det2 TO sDet2.* ATTRIBUTES (COUNT=gr_det2.getLength())
    BEFORE DISPLAY 
      EXIT DISPLAY 
  END DISPLAY  
END FUNCTION 



FUNCTION cleandet2()
  FOR i = 1 TO gr_det2.getLength()
      INITIALIZE gr_det2[i].* TO NULL
   END FOR 
   FOR i = gr_det2.getLength() TO 1 STEP -1
     CALL gr_det2.deleteElement(i)
   END FOR 
   
END FUNCTION 





FUNCTION cleanAllDet()
  CALL cleanDet2()
  CALL cleanDet1()

END FUNCTION 

FUNCTION validaLinea(idx)
DEFINE i, idx SMALLINT 

  FOR i = 1 TO gr_det2.getLength()
    
  END FOR 
  
  RETURN FALSE  
END FUNCTION

