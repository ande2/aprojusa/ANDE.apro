DATABASE db0001

DEFINE runcmd STRING 
DEFINE res SMALLINT

DEFINE reg RECORD
  comando           STRING ,
  tipoOperacion     TINYINT ,
  servidorBD        STRING ,
  usuarioBD         STRING ,
  claveBD           STRING ,
  servidorLicencias STRING ,
  nombreBD          STRING ,
  usuarioSAP        STRING ,
  claveSAP          STRING ,

  numeroBoleta      STRING ,
  
  numOrdenPro       STRING ,
  fechaContab       STRING ,
  serieReciboPro    STRING ,
  almacenRecibo     STRING ,
  
  serieSalidaMer    STRING ,
  
  serieEntradaMer   STRING ,
  almacenEntrada    STRING ,
  
  numItemsExp       TINYINT   
   
  
END RECORD 

DEFINE det DYNAMIC ARRAY OF RECORD 
  codigoItem        STRING ,
  descripcion       STRING ,
  costo             STRING ,
  cantidad          STRING ,
  ctaconsalida      STRING ,
  ctaconentrada     STRING 
END RECORD 

DEFINE result RECORD
  numReciboPro      STRING ,
  numSalidaInv      STRING ,
  numEntradaInv     STRING ,
  numEntMerExp      STRING ,
  numEntMerLoc      STRING ,
  ctaConSalida      STRING ,
  ctaConEntrada     STRING 
END RECORD 

MAIN
CALL ui.Interface.loadStyles("styles_sc")
  --detalle tdetac azul arial 7pt
OPEN WINDOW w1 WITH FORM "pemp0248_form1"
  MENU
  #1 = Validaci�n de costo
  #2 = Recibo/Salida/Entrada
  #3 = Entrada por proveedores
  #4 = Solo Salida/Entrada
  #5 = Solo Entrada
  #** El resto de campos se env�an igual

  --ON ACTION ValidaCosto
  ON ACTION Local
    CALL fInput(2)
    CALL ejecutaIngreso()
    CALL leeResultado()
    
  ON ACTION Productores
    CALL fInput(3)
    CALL ejecutaIngreso()
    CALL leeResultado()
    
  ON ACTION LocalSalidaEntrada
    CALL fInput(4)
    CALL ejecutaIngreso()
    CALL leeResultado()
    
  ON ACTION LocalEntrada
    CALL fInput(5)
    CALL ejecutaIngreso()
    CALL leeResultado()
    
  ON ACTION envio
     LET runcmd = 'java -jar C:\\\\tmp\\\\dist\\\\sc_conector_sap.jar 2 192.168.0.2 sa Kmaleon2 192.168.0.2 "WPruebas Semillas" manager 0313231719 5453 21-04-2014 195 196 195 1 RC-T100 140.042.005 "CHILE BLOCKY (LIBRA) FINCAS CM" 2.22 200 SR-0400 "_SYS00000001152" 305.240.003 "TOMATE CHERRY (LB) EXPORTACI�N" 5.85 100 PH-0200'
     CALL ui.interface.frontcall("standard","shellexec",[runcmd],[res])
     DISPLAY "Resultado ", res 
     DISPLAY runcmd

  ON ACTION salir 
    EXIT MENU

END MENU 

END MAIN 

FUNCTION fInput(operacion)
  DEFINE operacion STRING 

  DIALOG 

  INPUT BY NAME reg.comando ,           
                reg.tipoOperacion ,     reg.servidorBD ,
                reg.usuarioBD ,         reg.claveBD ,
                reg.servidorLicencias , reg.nombreBD ,
                reg.usuarioSAP ,        reg.claveSAP ,
                reg.numeroBoleta
                
    BEFORE INPUT 
      --datos de conexi�n
      LET reg.comando       = "java -jar C:\\\\tmp\\\\dist\\\\sc_conector_sap.jar"
      LET reg.tipoOperacion = 2
      LET reg.servidorBD    = "192.168.0.2"
      LET reg.usuarioBD     = "sa"
      LET reg.claveBD       = "Kmaleon2"
      LET reg.servidorLicencias = "192.168.0.2"
      LET reg.nombreBD      = "WPruebas Semillas"
      LET reg.usuarioSAP    = "manager"
      LET reg.claveSAP      = "0313231719"

      --datos de la boleta
      LET reg.numeroBoleta  = "RC-T100"

      --Datos de Recibo de producci�n      
      LET reg.numOrdenPro       = "5453"
      LET reg.fechaContab       = "21-04-2014"
      LET reg.serieReciboPro    = "195"
      LET reg.almacenRecibo     = "SR-0400"

      --Datos de Salida de Mercanc�a
      LET reg.serieSalidaMer    = "196"
      
      --Datos de Entrada de Mercanc�a
      LET reg.serieEntradaMer   = "195"
      LET reg.almacenEntrada    = "PH-0200"

      --Datos de items
      LET reg.numItemsExp   = 1
      
      DISPLAY BY NAME reg.numeroBoleta, reg.comando,    reg.tipoOperacion, 
                      reg.servidorBD,   reg.usuarioBD,  reg.usuarioBD, 
                      reg.claveBD,      reg.servidorLicencias,  
                      reg.nombreBD,     reg.usuarioSAP, reg.claveSAP ,
                      reg.numeroBoleta, 
                      reg.numOrdenPro,  reg.fechaContab,reg.serieReciboPro,
                      reg.almacenRecibo,
                      reg.serieSalidaMer,
                      reg.serieEntradaMer, reg.almacenEntrada,
                      reg.numItemsExp

      NEXT FIELD numeroBoleta

  END INPUT   

  INPUT ARRAY det FROM sdet.*
  END INPUT 

  END DIALOG 

END FUNCTION 

FUNCTION ejecutaIngreso()
END FUNCTION 

FUNCTION leeResultado()
END FUNCTION