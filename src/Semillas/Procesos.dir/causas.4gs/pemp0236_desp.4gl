GLOBALS "pemp0236_glob.4gl"


FUNCTION fbusca()

  CALL finput("B")

  SELECT idBoletaCau
  INTO gr_reg.idBoletaCau
  FROM pemmboletacau
  WHERE idBoletaIng = gr_reg.idBoletaIng

  IF sqlca.sqlcode = 100 THEN
     RETURN FALSE  
  END IF 

  CALL displayEnca()
  CALL displayDet1()
  {SELECT pesoTotal INTO lPesoTotal FROM pemmBoleta WHERE idBoletaIng = gr_reg.idBoletaIng
  SELECT pesoTotal INTO lSumPeso   FROM pemmBoletaExp WHERE idBoletaExp = gr_reg.idBoletaExp
  LET lPesoSaldo = lPesoTotal - lSumPeso 
  DISPLAY lPesoSaldo TO fPesoSaldo}
  RETURN TRUE 
  
END FUNCTION 

FUNCTION displayEnca()
DEFINE idx4 SMALLINT 
DEFINE sqlTxt STRING 
 
  CALL cleanAllDet()
  CALL llenatmp()
 
  LET sqlTxt = " SELECT tipoboleta, idordpro, iddordpro, productor, ",
               "        iditem, pesoTotal ",
               " FROM pemmboleta WHERE idboletaing = ", gr_reg.idBoletaIng
               --" AND ", lcondi 
  PREPARE ex_stmt FROM sqlTxt 
  EXECUTE ex_stmt INTO ltipo, lidordpro, liddordpro, lproductor, litem, 
          lPesoTotal  

  --Para datos de finca/productor
  IF ltipo = 1 THEN
     --Para estructura
     SELECT fincaNomCt || '-'||nomEstructura || '-'|| NVL(nomValvula,'') || '-' || desItemLg 
     INTO vEstructura
     FROM pemdordpro a, pemmfinca b, mestructura c, OUTER(pemMValvula d), glbMItems e 
     WHERE a.idfinca = b.idfinca 
     AND a.idestructura = c.idEstructura 
     AND a.idValvula = d.idValvula 
     AND a.idItem = e.idItem 
     AND a.idOrdPro = lidordpro 
     AND a.iddordpro = liddordpro

     DISPLAY vEstructura TO estructura
     --limpia productor
     LET vProductor = NULL 
     LET vItem = NULL 
     SELECT b.nombre||" "|| a.desItemLg
     INTO vItem 
     FROM glbMItems a, glbmtip b, 
          pemmboleta c, pemdordpro d
     WHERE c.idBoletaIng = gr_reg.idBoletaIng
     AND  a.idtipo = b.idtipo
     AND  c.idordpro = d.idordpro
     AND  c.iddordpro = d.iddordpro
     AND  d.iditem = a.iditem
     DISPLAY vProductor TO productor
     DISPLAY vItem TO producto
  ELSE 
     --Para productor
     SELECT trim(nombre)
     INTO vProductor 
     FROM commemp 
     WHERE id_commemp = lproductor
     DISPLAY vProductor TO productor

     --Para producto / item
     SELECT b.nombre||" "|| a.desItemLg
     INTO vItem 
     FROM glbMItems a, glbmtip b, 
          pemmboleta c -- pemDOrdPro b, 
     WHERE c.idBoletaIng = gr_reg.idBoletaIng
     AND  a.idtipo = b.idtipo
     AND  c.idItem = a.idItem
     DISPLAY vItem TO producto
     
     --Limpia estructura
     LET vEstructura = NULL 
     DISPLAY vEstructura TO estructura
  END IF 
  CALL allPesos() 

           
  --Para fecha
  SELECT fecha INTO gr_reg.fecha FROM pemmboleta WHERE idboletaing = gr_reg.idBoletaIng
  IF sqlca.sqlcode = 0 THEN  
     DISPLAY gr_reg.fecha TO fecha
  END IF 
  CALL combo_init()

END FUNCTION

FUNCTION displayDet1()
DEFINE idx6 SMALLINT
DEFINE lbol, lcli, i INTEGER 

  CALL cleandet2()
  DECLARE cur06 CURSOR FOR 
    SELECT idcausa, porcentaje, observaciones 
    FROM pemDBoletaCau
    WHERE idBoletaCau = gr_reg.idBoletaCau
    
  LET i = 1
  FOREACH cur06 INTO gr_det[i].idcausa, gr_det[i].porcentaje, gr_det[i].obsevaciones
    {SELECT nombre INTO gr_det[i].nombre FROM itemsxgrupo
           WHERE lnkItem = gr_det[i].lnkItem}
    LET i = i + 1
  END FOREACH
  DISPLAY ARRAY gr_det TO sDet.*
    BEFORE DISPLAY 
      EXIT DISPLAY 
  END DISPLAY  

END FUNCTION 