GLOBALS "pemp0236_glob.4gl"

DEFINE fCerrar SMALLINT 

FUNCTION finput(accion)
  DEFINE accion CHAR (1)
  DEFINE msgTxt, sqlTxt STRING 
  DEFINE pesoFalta SMALLINT 
  DEFINE preCantCajas, fpesomanual SMALLINT

  --IF accion = "I" OR accion = "B" THEN 
     --CALL inicializatmp()
  --ELSE 
     --CALL llenatmpitems()  
     --SELECT SUM (pesoTotal) INTO lPesoTotal FROM tmpdet2
     --SELECT SUM (pesoTotal) INTO lPesoTotal FROM pemmbo 
     --LET lPesoSaldo = lPesoTotal
  --END IF 
   DIALOG ATTRIBUTES(UNBUFFERED)
     
     INPUT gr_reg.idBoletaIng
       FROM idboletaing ATTRIBUTES  (WITHOUT DEFAULTS)
        
        BEFORE INPUT 
           CALL dialog.setactionactive("close",0)
           IF accion = "I" OR accion = "B" THEN 
              INITIALIZE gr_reg.* TO NULL
           END IF  
   
           CALL dialog.setActionHidden("close",1)

        BEFORE FIELD idboletaing
          IF gAccion = "U" THEN
             CALL allPesos()
             DISPLAY "voy a modificar"
             GOTO opcionDet1  
          END IF 
        
        AFTER FIELD idBoletaIng
          IF accion = "I" THEN
             IF gr_reg.idBoletaIng IS NULL THEN 
                CALL msg("Debe ingresar numero de boleta")
                GOTO opcionBusca
                --NEXT FIELD idBoletaIng
             END IF 
          END IF    
          
        ON CHANGE idBoletaIng
           CALL cleanAllDet()
           CALL llenatmp()
           CALL displayEnca()
           CALL displayDet1()
           --Que no sea nulo
           IF gr_reg.idBoletaing IS NULL THEN
              CALL msg("Debe ingresar orden de producci�n")
              NEXT FIELD idboletaing
           ELSE 
              IF accion = "I" THEN 
                 SELECT * FROM ordenes WHERE idOrden = gr_reg.idBoletaIng
                 IF sqlca.sqlcode = 100 THEN 
                    CALL msg("Boleta no existe o no esta completa")
                    LET gr_reg.idBoletaIng = NULL 
                    NEXT FIELD idboletaing
                 END IF   
              END IF  
           END IF

           {CASE                
             WHEN accion = "I" LET lcondi = " estado = 1"
             WHEN accion = "B" LET lcondi = " estado IN (1,2)"
           END CASE }

           LET sqlTxt = " SELECT tipoboleta, idordpro, iddordpro, productor, ",
                        "        iditem, pesoTotal ",
                        " FROM pemmboleta WHERE idboletaing = ", gr_reg.idBoletaIng
                        --" AND ", lcondi 
           PREPARE ex_stmt FROM sqlTxt 
           EXECUTE ex_stmt INTO ltipo, lidordpro, liddordpro, lproductor, litem, 
                   lPesoTotal
              --Si boleta no existe
              {CASE 
                WHEN accion = "I" LET msgTxt = "Boleta no existe o ya tiene informaci�n de exportaci�n" 
                WHEN accion = "B" LET msgTxt = "Boleta no exite"
              END CASE 
              IF sqlca.sqlcode = 100 THEN 
                 CALL msg(msgTxt)
                 NEXT FIELD idboletaing
              END IF }
           

           --Para datos de finca/productor
           IF ltipo = 1 THEN
              --Para estructura
              SELECT fincaNomCt || '-'||nomEstructura || '-'|| NVL(nomValvula,'') || '-' || desItemLg 
              INTO vEstructura
              FROM pemdordpro a, pemmfinca b, mestructura c, OUTER(pemMValvula d), glbMItems e 
              WHERE a.idfinca = b.idfinca 
              AND a.idestructura = c.idEstructura 
              AND a.idValvula = d.idValvula 
              AND a.idItem = e.idItem 
              AND a.idOrdPro = lidordpro 
              AND a.iddordpro = liddordpro

              DISPLAY vEstructura TO estructura
              --limpia productor
              LET vProductor = NULL 
              LET vItem = NULL 
              SELECT b.nombre||" "|| a.desItemLg
              INTO vItem 
              FROM glbMItems a, glbmtip b, 
                   pemmboleta c, pemdordpro d
              WHERE c.idBoletaIng = gr_reg.idBoletaIng
              AND  a.idtipo = b.idtipo
              AND  c.idordpro = d.idordpro
              AND  c.iddordpro = d.iddordpro
              AND  d.iditem = a.iditem
              DISPLAY vProductor TO productor
              DISPLAY vItem TO producto
           ELSE 
              --Para productor
              SELECT trim(nombre)
              INTO vProductor 
              FROM commemp 
              WHERE id_commemp = lproductor
              DISPLAY vProductor TO productor

              --Para producto / item
              SELECT b.nombre||" "|| a.desItemLg
              INTO vItem 
              FROM glbMItems a, glbmtip b, 
                   pemmboleta c -- pemDOrdPro b, 
              WHERE c.idBoletaIng = gr_reg.idBoletaIng
              AND  a.idtipo = b.idtipo
              AND  c.idItem = a.idItem
              DISPLAY vItem TO producto
              
              --Limpia estructura
              LET vEstructura = NULL 
              DISPLAY vEstructura TO estructura
           END IF 
           CALL allPesos() 
           IF fCerrar = 0 THEN 
              RETURN 
           END IF 
           --UPDATE pemmboleta SET estado = 0 WHERE idBoletaIng = gr_reg.idBoletaIng  
           --Para pesos
           --DISPLAY lPesoTotal TO fpesototal
           --CALL calculaPesoSaldo()           
           --DISPLAY lPesoSaldo TO fPesoSaldo

           --CALL dispcliente()
           
           --Para fecha
           SELECT fecha INTO gr_reg.fecha FROM pemmboleta WHERE idboletaing = gr_reg.idBoletaIng
           IF sqlca.sqlcode <> 0 THEN 
              CALL msg("Numero de boleta no existe") NEXT FIELD idboletaing
           ELSE 
              DISPLAY gr_reg.fecha TO fecha
           END IF 
           CALL combo_init()
           
        ON ACTION buscar
          LABEL opcionBusca:
          CALL llenatmp()
          CALL displayEnca()
          CALL displayDet1() 
          CALL picklist_2("Boletas", "Boleta","Descripci�n","idorden", "descripcion", "ordenes", "1=1", "1", 0)
            RETURNING gr_reg.idBoletaIng, vDescripcion, INT_FLAG
          --Que no sea nulo
           IF gr_reg.idBoletaing IS NULL THEN
              CALL msg("Debe ingresar orden de producci�n")
              NEXT FIELD idboletaing
           END IF

           LET sqlTxt = " SELECT tipoboleta, idordpro, iddordpro, productor, ",
                        " iditem, pesoTotal ",
                        " FROM pemmboleta WHERE idboletaing = ", gr_reg.idBoletaIng

                        
           PREPARE ex_st1 FROM sqlTxt
           EXECUTE ex_st1 INTO ltipo, lidordpro, liddordpro, lproductor, 
                               litem, lPesoTotal 
           --Si boleta no existe
           IF sqlca.sqlcode = 100 THEN 
              CALL msg(msgTxt)
              NEXT FIELD idboletaing
           END IF 

           --Para datos de finca/productor
           IF ltipo = 1 THEN
              --Para estructura
              SELECT fincaNomCt || '-'||nomEstructura || '-'|| NVL(nomValvula,'') || '-' || desItemLg 
              INTO vEstructura
              FROM pemdordpro a, pemmfinca b, mestructura c, OUTER(pemMValvula d), glbMItems e 
              WHERE a.idfinca = b.idfinca 
              AND a.idestructura = c.idEstructura 
              AND a.idValvula = d.idValvula 
              AND a.idItem = e.idItem 
              AND a.idOrdPro = lidordpro 
              AND a.iddordpro = liddordpro

              DISPLAY vEstructura TO estructura
              --limpia productor
              LET vProductor = NULL 
              LET vItem = NULL 
              SELECT b.nombre||" "|| a.desItemLg
              INTO vItem 
              FROM glbMItems a, glbmtip b, 
                   pemmboleta c, pemdordpro d
              WHERE c.idBoletaIng = gr_reg.idBoletaIng
              AND  a.idtipo = b.idtipo
              AND  c.idordpro = d.idordpro
              AND  c.iddordpro = d.iddordpro
              AND  d.iditem = a.iditem
              DISPLAY vProductor TO productor
              DISPLAY vItem TO producto
           ELSE 
              --Para productor
              SELECT trim(nombre)
              INTO vProductor 
              FROM commemp 
              WHERE id_commemp = lproductor
              DISPLAY vProductor TO productor
              
              --Para producto / item
              SELECT b.nombre||" "|| a.desItemLg
              INTO vItem 
              FROM glbMItems a, glbmtip b, 
                   pemmboleta c -- pemDOrdPro b, 
              WHERE c.idBoletaIng = gr_reg.idBoletaIng
              AND  a.idtipo = b.idtipo
              AND  c.idItem = a.idItem
              DISPLAY vItem TO producto

              --Limpia estructura
              LET vEstructura = NULL 
              DISPLAY vEstructura TO estructura
           END IF 
           CALL allPesos() 
           
           --Para fecha
           SELECT fecha INTO gr_reg.fecha FROM pemmboleta WHERE idboletaing = gr_reg.idBoletaIng
           IF sqlca.sqlcode <> 0 THEN 
              CALL msg("Numero de boleta no existe") NEXT FIELD idboletaing
           ELSE 
              DISPLAY gr_reg.fecha TO fecha
           END IF 

           CALL combo_init()
           --Para pesos
           --DISPLAY lPesoTotal TO fpesototal
           --CALL calculaPesoSaldo() 
           --DISPLAY lPesoSaldo TO fPesoSaldo

        AFTER INPUT 
           
           IF accion = "B" THEN
              RETURN 
           END IF
     END INPUT 
     
     INPUT ARRAY gr_det FROM sDet.* ATTRIBUTES (INSERT ROW = FALSE )

       BEFORE INPUT 
         --CALL DIALOG.setActionS( "append", FALSE )
         LABEL opcionDet1:
         LET insdet2 = 0
         LABEL labelIS:
         LET idx = gr_det.getLength()
         IF idx > 1 THEN
            FOR i = idx TO 1 STEP -1
               IF i = 1 THEN EXIT FOR END IF 
               IF gr_det[i].idcausa IS NULL AND gr_det[i].porcentaje IS NULL THEN    
                  CALL gr_det.deleteElement(i)
               END IF  
            END FOR  
         END IF 
         NEXT FIELD idcausa
          
       ON CHANGE idcausa
         LET idx = arr_curr()
         IF lineaRepetida(DIALOG) THEN
            CALL msg("Causa de rechazo ya fue ingresada")
            LET gr_det[idx].idcausa = NULL 
            NEXT FIELD idcausa 
         END IF 

       BEFORE FIELD porcentaje
         LET idx = arr_curr()
         IF gr_det[idx].idcausa IS NULL THEN
            CALL msg("Ingrese causa de rechazo")
            NEXT FIELD idcausa 
         END IF   

       BEFORE FIELD observaciones
         LET idx = arr_curr()
         IF gr_det[idx].idcausa IS NULL THEN
            CALL msg("Ingrese causa de rechazo")
            NEXT FIELD idcausa 
         END IF 
         IF gr_det[idx].porcentaje IS NULL THEN  
            CALL msg("Ingrese porcentaje de rechazo")
            NEXT FIELD porcentaje 
         END IF   

       {AFTER FIELD observaciones
         LET idx = arr_curr()
         IF gr_det[idx].obsevaciones IS NULL THEN
            CALL msg("Ingrese observaciones")
            NEXT FIELD idcausa 
         END IF   }

       ON CHANGE porcentaje
         LET idx = arr_curr()
         IF gr_det[idx].porcentaje <=0 OR gr_det[idx].porcentaje > 100 THEN
            CALL msg("Debe ingresar un valor entre 1-100")
            NEXT FIELD porcentaje   
         END IF 
         IF porcentajeMayor(DIALOG) THEN
            CALL msg("La suma de porcentajes no puede ser mayor a 100%")
            NEXT FIELD porcentaje 
         END IF 
         
       AFTER FIELD porcentaje  
         LET idx = arr_curr()
         IF gr_det[idx].porcentaje IS NULL THEN
            CALL msg("Debe ingresar porcentaje")
            NEXT FIELD porcentaje   
         END IF
         IF porcentajeMayor(DIALOG) THEN
            CALL msg("La suma de porcentajes no puede ser mayor a 100%")
            NEXT FIELD porcentaje 
         END IF 
         
       
       AFTER INSERT
         LET idx = arr_curr()
          
         IF gr_det[arr_curr()].idcausa IS NULL 
            OR gr_det[arr_curr()].porcentaje IS NULL THEN
            CANCEL INSERT
         END IF

       {AFTER DELETE
          CALL calculaPesos()}
          
       AFTER INPUT 
         --CALL insertaDet2()
         LET insdet2 = 1
         
       
     END INPUT 
     
     ON ACTION ACCEPT
        LET idx = arr_curr()
        IF gr_reg.idBoletaIng IS NULL THEN
           CALL msg("Debe seleccionar boleta")
           GOTO opcionBusca
           --NEXT FIELD idBoletaIng  
        END IF 
        IF gr_det.getLength() = 1 AND gr_det[idx].idcausa IS NULL THEN
           CALL msg("Debe seleccionar causa de rechazo")
           NEXT FIELD idcausa 
        END IF 
        IF porcentajeMayor(DIALOG) THEN
            CALL msg("La suma de porcentajes no puede ser mayor a 100%")
            NEXT FIELD porcentaje 
         END IF
         IF gAccion <> "B" THEN 
            IF porcentajeMenor(DIALOG) THEN  
               CALL msg("La suma de porcentajes debe ser 100%")
               NEXT FIELD porcentaje 
            END IF
         END IF 
         
        IF accion = "B" THEN
           CALL llenatmp()
           CALL displayEnca()
           CALL displayDet1()
                      --Para datos de finca/productor
           IF ltipo = 1 THEN
              --Para estructura
              SELECT fincaNomCt || '-'||nomEstructura || '-'|| NVL(nomValvula,'') || '-' || desItemLg 
              INTO vEstructura
              FROM pemdordpro a, pemmfinca b, mestructura c, OUTER(pemMValvula d), glbMItems e 
              WHERE a.idfinca = b.idfinca 
              AND a.idestructura = c.idEstructura 
              AND a.idValvula = d.idValvula 
              AND a.idItem = e.idItem 
              AND a.idOrdPro = lidordpro 
              AND a.iddordpro = liddordpro

              DISPLAY vEstructura TO estructura
              --limpia productor
              LET vProductor = NULL 
              LET vItem = NULL 
              SELECT b.nombre||" "|| a.desItemLg
              INTO vItem 
              FROM glbMItems a, glbmtip b, 
                   pemmboleta c, pemdordpro d
              WHERE c.idBoletaIng = gr_reg.idBoletaIng
              AND  a.idtipo = b.idtipo
              AND  c.idordpro = d.idordpro
              AND  c.iddordpro = d.iddordpro
              AND  d.iditem = a.iditem
              DISPLAY vProductor TO productor
              DISPLAY vItem TO producto
           ELSE 
              --Para productor
              SELECT trim(nombre)
              INTO vProductor 
              FROM commemp 
              WHERE id_commemp = lproductor
              DISPLAY vProductor TO productor
              
              --Para producto / item
              SELECT b.nombre||" "|| a.desItemLg
              INTO vItem 
              FROM glbMItems a, glbmtip b, 
                   pemmboleta c -- pemDOrdPro b, 
              WHERE c.idBoletaIng = gr_reg.idBoletaIng
              AND  a.idtipo = b.idtipo
              AND  c.idItem = a.idItem
              DISPLAY vItem TO producto

              --Limpia estructura
              LET vEstructura = NULL 
              DISPLAY vEstructura TO estructura
           END IF 
           CALL allPesos()
           RETURN 
        END IF
        EXIT DIALOG
        
        {IF gr_det[idx].lnkItem IS NULL THEN
           CALL msg("Ingrese Item")
           NEXT FIELD lnkItem 
        END IF
        
        IF gr_det[idx].cantCajas IS NULL THEN  
           CALL msg("Debe ingresar n�mero de cajas")
           NEXT FIELD cantCajas
        END IF

        IF gr_det[idx].pesoNeto IS NULL THEN  
           CALL msg("Debe leer peso")
           NEXT FIELD cantCajas
        END IF
        
           IF insdet2 = 0 THEN
              CALL insertaDet2() 
           END IF

          IF gr_det[idx].calidad IS NULL THEN
             CALL msg("Debe ingresar calidad")
             NEXT FIELD calidad
          END IF 
        EXIT DIALOG }

    {       CASE box_gradato("Seguro de grabar")
         WHEN "Si"
            LET resultado = TRUE
            EXIT DIALOG
         WHEN "No"
            EXIT DIALOG 
         OTHERWISE
            CONTINUE DIALOG 
      END CASE 
      LET resultado = TRUE
      EXIT DIALOG }
      
     ON ACTION CANCEL
        CLEAR FORM 
        --CALL gr_det.clear()

        DISPLAY "INGRESO DE CAUSAS DE RECHAZO" TO pdescripcion

        LET gr_reg.idBoletaIng = NULL
        --LET gr_reg.cliente  = NULL 
        RETURN --EXIT dialog
   END DIALOG
   --IF INT_FLAG THEN 
      --RETURN 
   --END IF 

  -- DISPLAY "insert en encabezado"
   IF gAccion = "I" THEN 
   LET gr_reg.idBoletaCau = 0 
   --DISPLAY "Boleta cau", gr_reg.idBoletacau
   END IF  
   --DISPLAY "Boleta ing", gr_reg.idBoletaIng
   --DISPLAY "Cliente ", gr_reg.cliente
   --DISPLAY "pesototal ", vpesototal
   --DISPLAY "calidadproduccion ", 1
   LET gr_reg.fectran = CURRENT 
   --DISPLAY "fectran ", gr_reg.fectran
   LET gr_reg.usuOpero = 1 --Validar usuarios
   --DISPLAY "usuopero", gr_reg.usuOpero
   
   BEGIN WORK 
     --insertando inforamcion de la boleta local
     IF accion = "I" THEN 
        INSERT INTO pemmboletacau (idboletacau, idboletaing, merma,
                                   fectran, usuopero)
          VALUES (0, gr_reg.idBoletaIng, gr_reg.pesomerma,
                  gr_reg.fectran, gr_reg.usuOpero )
     ELSE 
        IF accion = "U" THEN 
           UPDATE pemmboletacau SET (merma,
                                   fectran, usuopero)
                                  = (gr_reg.pesomerma,
                                     gr_reg.fectran, gr_reg.usuOpero )
           WHERE idboletacau = gr_reg.idBoletacau                          
        END IF 
     END IF  
                  
   DISPLAY "insert en detalle "
   IF sqlca.sqlcode = 0 THEN 
      IF accion = "I" THEN 
         LET gr_reg.idBoletacau = sqlca.sqlerrd[2]
      ELSE 
         DELETE FROM pemdboletacau WHERE idboletacau = gr_reg.idBoletacau
         --DELETE FROM pemcboletaexp WHERE idboletaexp = gr_reg.idBoletaExp}
      END IF 
         
      --Insertando en detalle
      FOR i = 1 TO gr_det.getLength()
        IF gr_det[i].idcausa IS NOT NULL THEN 
          INSERT INTO pemDBoletaCau (idBoletaCau, idCausa, porcentaje, observaciones)
            VALUES (gr_reg.idBoletaCau, gr_det[i].idcausa, gr_det[i].porcentaje,
                    gr_det[i].obsevaciones)
        END IF 
      END FOR 
    END IF   

      IF sqlca.sqlcode = 0 THEN
         DISPLAY "actualizando estado de la boleta"
         --DISPLAY "update pemmboleta set estado = 2 where idboletaing = ", gr_reg.idBoletaIng
         UPDATE pemmboleta SET estado = 0 WHERE idboletaing = gr_reg.idBoletaIng
         IF sqlca.sqlcode = 0 THEN
            CALL msg("Informacion grabada")
            --ROLLBACK WORK
            COMMIT WORK
         ELSE 
            CALL msg("Error al grabar informaci�n 3")
            ROLLBACK WORK 
         END IF  
      ELSE 
         CALL msg("Error al grabar informaci�n 2")
         ROLLBACK WORK 
      END IF 
END FUNCTION 


FUNCTION allPesos()
DEFINE porcMerma DECIMAL (5,2) 
DEFINE iMerma, maxMerma INTEGER 
 
--Peso de la boleta
SELECT pesoTotal INTO gr_reg.pesorecepcion
FROM pemmboleta WHERE idBoletaIng = gr_reg.idBoletaIng
DISPLAY gr_reg.pesorecepcion TO pesorecepcion

--Para aprovechamiento exportaci�n
SELECT SUM (pesoTotal) INTO gr_reg.pesoaprovexp
FROM pemMBoletaExp WHERE idBoletaIng = gr_reg.idBoletaIng
GROUP BY idBoletaIng
DISPLAY gr_reg.pesoaprovexp TO pesoaprovexp

--Para aprovechamiento local
SELECT SUM (a.pesoNeto) INTO gr_reg.pesoaprovloc
FROM pemdboletaloc a, pemMBoletaLoc b, pemMBoleta c
WHERE a.idBoletaLoc = b.idBoletaLoc
AND b.idBoletaIng = c.idBoletaIng
AND c.idBoletaIng = gr_reg.idBoletaIng
AND a.calidad = 1

DISPLAY gr_reg.pesoaprovloc TO pesoaprovloc
--GROUP BY calidad

--Para mediano 
SELECT SUM (a.pesoNeto) INTO gr_reg.pesomediano
FROM pemdboletaloc a, pemMBoletaLoc b, pemMBoleta c
WHERE a.idBoletaLoc = b.idBoletaLoc
AND b.idBoletaIng = c.idBoletaIng
AND c.idBoletaIng = gr_reg.idBoletaIng
AND a.calidad = 2

DISPLAY gr_reg.pesomediano TO pesomediano

--Para devoluci�n
SELECT SUM (a.pesoNeto) INTO gr_reg.pesodevolucion
FROM pemdboletaloc a, pemMBoletaLoc b, pemMBoleta c
WHERE a.idBoletaLoc = b.idBoletaLoc
AND b.idBoletaIng = c.idBoletaIng
AND c.idBoletaIng = gr_reg.idBoletaIng
AND a.calidad = 3

DISPLAY gr_reg.pesodevolucion TO pesodevolucion

--Para merma
-- 1. Calcular merma en lbs
IF gr_reg.pesorecepcion  IS NULL THEN LET gr_reg.pesorecepcion  = 0 END IF
IF gr_reg.pesoaprovexp   IS NULL THEN LET gr_reg.pesoaprovexp   = 0 END IF 
IF gr_reg.pesoaprovloc   IS NULL THEN LET gr_reg.pesoaprovloc   = 0 END IF
IF gr_reg.pesomediano    IS NULL THEN LET gr_reg.pesomediano    = 0 END IF
IF gr_reg.pesodevolucion IS NULL THEN LET gr_reg.pesodevolucion = 0 END IF
IF gr_reg.pesomerma      IS NULL THEN LET gr_reg.pesomerma      = 0 END IF

LET gr_reg.pesomerma = gr_reg.pesorecepcion - (gr_reg.pesoaprovexp + 
    gr_reg.pesoaprovloc + gr_reg.pesomediano + gr_reg.pesodevolucion)
DISPLAY gr_reg.pesomerma TO pesomerma

-- 2. Calcular que % es la merma del peso total ingresado

LET porcMerma = gr_reg.pesomerma * 100 / gr_reg.pesorecepcion
LET iMerma = porcMerma USING "##"
--LET porcMerma = ROUND (porcMerma)
--LET gMsg = "Porcentaje de merma es: ", porcMerma USING "##"
LET gMsg = "Porcentaje de merma es: ", iMerma 
--CALL msg(gMsg)

-- 3. Obtener par�metro de merma valida (es %)
LET maxMerma = getValParam("MERMA")

LET gMsg = "Porcentaje maximo de merma es: ", maxMerma 
--CALL msg(gMsg)


-- 4. Si la merma es superior a lo permitido, da mensaje de error y no permite ingresar causas
--DISPLAY "iMerma ", iMerma, " maxMerma ", maxMerma
IF iMerma > maxMerma THEN
   LET gMsg = "Boleta NO se puede cerrar\n",
              "Merma en esta boleta es de: ", gr_reg.pesomerma USING "<<<,<<&", " Lbs (", iMerma USING "##","%)\n",
              "el porcentaje m�ximo permitido es: ", maxMerma USING "<<", "%\n"
   CALL msg(gMsg) 
   LET fCerrar = 0
ELSE
   IF gr_reg.pesorecepcion - (gr_reg.pesoaprovexp + gr_reg.pesomerma + 
      gr_reg.pesoaprovloc + gr_reg.pesomediano + gr_reg.pesodevolucion) < 0 THEN
      LET gMsg = "Boleta NO se puede cerrar\n", 
                 "La Suma de Exportaci�n (", gr_reg.pesoaprovexp + gr_reg.pesomerma USING "<<<,<<&", ")\n",
                 "           Local       (", gr_reg.pesoaprovloc + gr_reg.pesomediano USING "<<<,<<&", ")\n",  
                 "           Devoluci�n  (", gr_reg.pesodevolucion USING "<<<,<<&", ")\n",
                 "Es mayor a las libras ingresadas en recepci�n (", gr_reg.pesorecepcion USING "<<<,<<&", ")"
      CALL msg(gMsg) 
      LET fCerrar = 0 
   ELSE
      {LET gMsg = "Boleta SI se puede cerrar\n", 
                 "La Suma de Exportaci�n (", gr_reg.pesoaprovexp + gr_reg.pesomerma USING "<<<,<<&", ")\n",
                 "           Local       (", gr_reg.pesoaprovloc + gr_reg.pesomediano USING "<<<,<<&", ")\n",  
                 "           Devoluci�n  (", gr_reg.pesodevolucion USING "<<<,<<&", ")\n",
                 "Es mayor a las libras ingresadas en recepci�n (", gr_reg.pesorecepcion USING "<<<,<<&", ")"
      CALL msg(gMsg) }
      LET fCerrar = 1  
   END IF  
   
END IF 


-- 5. Si es posible cerrar la boleta, la cierra y permite el ingreso de las causas


-- 6. Despliega suma de pesos
LET sumaPesos = gr_reg.pesoaprovexp + gr_reg.pesoaprovloc + gr_reg.pesomediano + 
                gr_reg.pesodevolucion + gr_reg.pesomerma
DISPLAY BY NAME sumapesos                 
  
END FUNCTION 

####### Encabezado ##############

FUNCTION creatmp()

    CREATE TEMP TABLE tmpdet2
  (idcliente    INTEGER, 
   lnkItem      INTEGER ,
   cantcajas    SMALLINT , 
   peso         SMALLINT,
   pesoTotal    SMALLINT,
   PRIMARY KEY (idcliente, lnkitem) )


  CREATE TEMP TABLE ordenes
  (
  idorden   INTEGER ,
  descripcion   VARCHAR (100)
  )
  CREATE INDEX ix_orden ON ordenes(idorden)
END FUNCTION 

{FUNCTION inicializatmp()
  DROP TABLE tmpdet2
  CREATE TEMP TABLE tmpdet2
  (idcliente    INTEGER, 
   lnkItem      INTEGER ,
   cantcajas    SMALLINT , 
   peso         SMALLINT,
   pesoTotal    SMALLINT,
   PRIMARY KEY (idcliente, lnkitem)  )

   FOR i = 1 TO gr_cli.getLength()
      INITIALIZE gr_cli[i].* TO NULL
   END FOR 
   FOR i = 1 TO gr_det.getLength()
      INITIALIZE gr_det[i].* TO NULL
   END FOR
   CLEAR FORM 
END FUNCTION}

FUNCTION llenatmp()
  DEFINE laccion CHAR (1) 
  DEFINE sqlTxt STRING 
  DEFINE vBol, vordpro, vdordpro, vproductor, vitem, vlPeso1, vlPeso2 INTEGER
  DEFINE vestructura, cproductor, citem, vdes VARCHAR (100)
  DEFINE pesoSaldo SMALLINT  

  DELETE FROM ordenes WHERE 1=1
  --Para boletas locales
   
  LET sqlTxt = " SELECT idboletaing, idordpro, iddordpro ",
               " FROM pemmboleta ",
               " WHERE tipoBoleta = 1 and estado <> 0"
  CASE 
    WHEN gAccion = "I"
       LET sqlTxt = sqlTxt || " AND estadoDevolucion = 1 AND estado <> 0 "
    WHEN gAccion = "U"
       LET sqlTxt = sqlTxt || " AND estadoDevolucion = 1 AND estado <> 0 "
  END CASE             

  PREPARE ex_bolLoc FROM sqlTxt  
  DECLARE cur01 CURSOR FOR ex_bolLoc

    FOREACH cur01 INTO vBol, vordpro, vdordpro
      SELECT fincaNomCt || '-'||nomEstructura || '-'|| NVL(nomValvula,'') || '-' || desItemLg
        INTO vestructura
        FROM pemdordpro a, pemmfinca b, mestructura c, OUTER(pemMValvula d), glbMItems e 
       WHERE a.idfinca = b.idfinca 
         AND a.idestructura = c.idEstructura 
         AND a.idValvula = d.idValvula 
         AND a.idItem = e.idItem 
         AND a.idOrdPro = vordpro 
         AND a.iddordpro = vdordpro
         INSERT INTO ordenes VALUES (vBol, vestructura)
    END FOREACH 
DISPLAY "primer foreach"
    --Para productor
    LET sqlTxt = " SELECT idboletaing, productor, iditem ",
               " FROM pemmboleta ",
               " WHERE tipoBoleta = 2 ",
               " AND estadoDevolucion = 1 AND estado <> 0 "
               
  {CASE 
    WHEN laccion = "I" --Ingreso
      LET sqlTxt = sqlTxt || " AND estado = 1 "
    WHEN laccion = "B" --Busqueda
      LET sqlTxt = sqlTxt || " AND estado IN (1,2) "
  END CASE}
  PREPARE ex_bolProd FROM sqlTxt
    DECLARE cur02 CURSOR FOR ex_bolProd

    FOREACH cur02 INTO vBol, vproductor, vitem
      SELECT trim(nombre)
        INTO cProductor 
        FROM commemp 
       WHERE id_commemp = vproductor
              --DISPLAY vProductor TO productor
    
      --Para producto / item
      SELECT desItemLg
        INTO cItem 
        FROM glbMItems
       WHERE idItem = vitem
              --DISPLAY vItem TO producto
      LET vdes = cproductor CLIPPED, "-",citem
      INSERT INTO ordenes VALUES (vBol, vdes)
    END FOREACH 
--DISPLAY "segundo foreach"
    --Borrando las boletas que tengan ingreso en exportacion y ya no tengan saldo
    DECLARE cur03 CURSOR FOR SELECT idOrden FROM ordenes
    FOREACH cur03 INTO vBol
      SELECT FIRST 1 idBoletaing FROM pemmboletaexp WHERE idboletaing = vBol
      IF sqlca.sqlcode = 0 THEN
         SELECT SUM (pesoTotal) INTO vlPeso1
         FROM pemmboletaexp
         WHERE idboletaing = vBol
         GROUP BY idBoletaIng
         SELECT a.pesoTotal INTO vlPeso2 
         FROM pemmboleta a
         WHERE a.idboletaing = vBol
         LET pesoSaldo = vlPeso2 - vlPeso1
         IF pesoSaldo <= 0 THEN
            DELETE FROM ordenes WHERE idOrden = vBol
            IF sqlca.sqlcode = 0 THEN
               UPDATE pemmboleta SET estado = 0 WHERE idboletaing = vBol --gr_reg.idBoletaIng 
               --DISPLAY "borre ", vBol 
            END IF  
         END IF  
      END IF 
    END FOREACH 
   --DISPLAY "tercer foreach" 
END FUNCTION 

FUNCTION calculaPesoSaldo() 

  LET lSumPeso = 0
  SELECT SUM (pesoTotal) INTO lSumPeso FROM pemmboletaExp
  WHERE idboletaing = gr_reg.idBoletaIng
  GROUP BY idboletaing 
  IF lSumPeso > 0 THEN
     LET lPesoSaldo = lPesoTotal - lSumPeso
  ELSE 
     LET lPesoSaldo = lPesoTotal - lSumPeso
  END IF 
 
END FUNCTION

####### Detalle 1 #############


FUNCTION clirep(nline)
DEFINE nline SMALLINT 
{
  FOR i = 1 TO gr_cli.getLength()
    IF i <> nline THEN
     IF gr_cli[i].idcliente = gr_cli[nline].idcliente THEN  
       RETURN TRUE 
     END IF 
    END IF 
  END FOR }
  RETURN FALSE 
END FUNCTION 

FUNCTION cliExDet2(lcli)
DEFINE lcli INTEGER 

  SELECT FIRST 1 idcliente FROM tmpdet2
  WHERE idcliente = lcli
     IF sqlca.sqlcode = 0 THEN 
        RETURN TRUE  
     ELSE 
        RETURN FALSE 
     END IF 

END FUNCTION 

FUNCTION cleandet1()
 { FOR i = 1 TO gr_cli.getLength()
      INITIALIZE gr_cli[i].* TO NULL
   END FOR 
   FOR i = gr_cli.getLength() TO 1 STEP -1
     CALL gr_cli.deleteElement(i)
   END FOR }
   
END FUNCTION

##### Detalle 2 ##########

FUNCTION mostrarDet2(lcli)
DEFINE lcli, i INTEGER 
{
  CALL cleandet2()
  SELECT COUNT (*) INTO j FROM tmpdet2 WHERE idcliente = lcli
  DISPLAY "en tmp van ", j
  DECLARE cur003 CURSOR FOR 
    SELECT idcliente, lnkItem, cantCajas, peso, pesoTotal
    FROM tmpdet2
    WHERE idcliente = lcli
  LET i = 1
  FOREACH cur003 INTO gr_det[i].idcliente, gr_det[i].lnkItem, 
    gr_det[i].cantcajas, gr_det[i].peso, gr_det[i].pesoTotal
    SELECT nombre INTO gr_det[i].nombre FROM itemsxgrupo
           WHERE lnkItem = gr_det[i].lnkItem
    LET i = i + 1
    DISPLAY "pase foreach ", i
  END FOREACH
  DISPLAY "arreglo tiene lineas ", gr_det.getLength()
  DISPLAY ARRAY gr_det TO sDet.*
    BEFORE DISPLAY 
      EXIT DISPLAY 
  END DISPLAY } 
END FUNCTION 

FUNCTION fitem(numBol)
DEFINE numbol, numitem, tipBol INTEGER 

SELECT tipoboleta INTO tipBol FROM pemmboleta WHERE idboletaing = numbol
IF tipBol = 1 THEN 
  SELECT a.iditem 
  INTO numitem 
  FROM pemdordpro a, pemmboleta b
  WHERE a.idordpro = b.idordpro
  AND a.iddordpro = b.iddordpro
  AND b.idboletaing = numBol
ELSE 
  SELECT iditem INTO numitem FROM pemmboleta WHERE idboletaing = numBol
END IF 

RETURN numitem
END FUNCTION 

FUNCTION cleandet2()
  FOR i = 1 TO gr_det.getLength()
      INITIALIZE gr_det[i].* TO NULL
   END FOR 
   FOR i = gr_det.getLength() TO 1 STEP -1
     CALL gr_det.deleteElement(i)
   END FOR 
   
END FUNCTION 

FUNCTION insertaDet2()
 { DELETE FROM tmpdet2 WHERE idcliente = gr_cli[idx2].idcliente
  FOR i = 1 TO gr_det.getLength()
     IF gr_det[i].lnkItem IS NOT NULL AND gr_det[i].cantcajas IS NOT NULL THEN 
        INSERT INTO tmpdet2(idcliente, lnkItem, cantcajas, peso, pesoTotal)
          VALUES (gr_cli[idx2].idcliente, gr_det[i].lnkItem, 
          gr_det[i].cantcajas, gr_det[i].peso, gr_det[i].pesoTotal)
     END IF      
  END FOR 
  SELECT SUM (pesoTotal) INTO gr_cli[idx2].pesocli FROM tmpdet2 WHERE idcliente = gr_cli[idx2].idcliente
  LET vpeso = 0 
  FOR i = 1 TO gr_cli.getLength()
     IF gr_cli[i].pesocli IS NOT NULL THEN 
        LET vpeso = vpeso + gr_cli[i].pesocli
     END IF 
  END FOR 
  LET lpesosaldo = lpesototal - vpeso
  
  DISPLAY lpesosaldo TO fpesosaldo }
END FUNCTION 

FUNCTION cleanAllDet()
  CALL cleanDet2()
  CALL cleanDet1()
END FUNCTION 

FUNCTION validaLinea(idx)
DEFINE i, idx SMALLINT 

{  FOR i = 1 TO gr_det.getLength()
    IF i <> idx THEN
       IF gr_det[i].lnkItem = gr_det[idx].lnkitem THEN 
          RETURN TRUE  
       END IF 
    END IF 
  END FOR }
  
  RETURN FALSE  
END FUNCTION

FUNCTION pesoExcede()
DEFINE pesoActual, i, npeso, npeso2 SMALLINT 
 {  
  SELECT SUM (pesoTotal) INTO pesoActual FROM pemMBoletaExp
  WHERE idBoletaIng = gr_reg.idBoletaIng
  IF pesoActual > 0 THEN 
  ELSE 
     LET pesoActual = 0
  END IF 
  FOR i = 1 TO gr_det.getLength()
     IF gr_det[i].pesoNeto > 0 THEN 
        LET pesoActual = pesoActual + gr_det[i].pesoNeto
     END IF    
  END FOR
  
  SELECT pesoTotal INTO nPeso FROM pemmboleta
  WHERE idBoletaing = gr_reg.idboletaing

  IF pesoActual > npeso THEN 
     LET gMsg = "Suma de peso es superior a saldo disponible, ingrese de nuevo" 
     RETURN TRUE 
  ELSE
    RETURN FALSE   
  END IF 
  }
  RETURN FALSE
END FUNCTION 

FUNCTION llenatmpitems()
DEFINE lcliente, litem  INTEGER 
DEFINE lcajas, lpeso, lpesototal SMALLINT 
{
  DECLARE cur10 CURSOR FOR
    SELECT idCliente, lnkItem, cantCajas, peso, pesoTotal
    FROM pemDBoletaExp 
    WHERE idBoletaLoc = gr_reg.idBoletaLoc

  FOREACH cur10 INTO lcliente, litem, lcajas, lpeso, lpesototal
    INSERT INTO tmpdet2 (idCliente, lnkItem, cantCajas, peso, pesoTotal)
      VALUES (lcliente, litem, lcajas, lpeso, lpesototal)
  END FOREACH 
}
END FUNCTION 

FUNCTION getPeso()
  DEFINE pesoManual SMALLINT
  DEFINE fn, fs, ft STRING 
  DEFINE cPeso STRING 
  DEFINE result SMALLINT, --,opc,activo   
         peso   DEC(9,2)

  LET pesoManual = getValParam("LEER PESO")       
  --LET pesoManual = 1

  IF pesomanual = 1 THEN 
     RETURN 3000
  ELSE 
     -- Obteniendo peso de bascula
     LET fn = "C:\\\\Basculas\\\\SipLafarge.exe"
     LET fs = "C:\\\\SCALEWORKDIR\\\\pesos.prn"
     LET ft = "/tmp/file.txt"
     
     --LET fn = "bascula" --||w_mae_psj.numbas 
     CALL librut001_leerbascula(fn,fs,ft,"G")
     RETURNING result,peso
     DISPLAY "result ", result, " peso ", peso 
     IF NOT result THEN
        DISPLAY "entre if"
        RETURN -1  
     ELSE 
        DISPLAY "entre al else"
        RETURN peso
     END IF
     
  END IF  
END FUNCTION 

FUNCTION calculaPesos() --fArray que arreglo esta pesando 1 local 2 productor
DEFINE pesoTarima, pesoCaja, pesoCajaT DECIMAL(9,2)
DEFINE idx, i, fArray SMALLINT 
{
  LET idx = arr_curr()
  --obtiene peso de tarima
  IF gr_reg.tieneTarima THEN
     LET pesoTarima = getValParam("RECEPCION DE FRUTA - PESO DE TARIMA")
  ELSE 
     LET pesoTarima = 0.0
  END IF  
  
  --obtiene peso segun tipo de caja
  SELECT pesoCatEmpaque INTO pesoCaja
  FROM pemMcatEmp
  WHERE idCatEmpaque = gr_reg.tipoCaja
  
  --Calcula Tara (multiplica cantCajas * pesoCaja) + pesoTarima
     LET pesoCajaT =  gr_det[idx].cantCajas * pesoCaja

     LET gr_det[idx].pesoTara = pesoTarima + pesoCajaT
     DISPLAY "Tara lleva: ", gr_det[idx].pesoTara 
  
  --Para peso Neto
     LET gr_det[idx].pesoNeto = gr_det[idx].pesoBruto -
                                    gr_det[idx].pesoTara
     DISPLAY gr_det[arr_curr()].pesoBruto,
             gr_det[arr_curr()].pesoTara,
             gr_det[arr_curr()].pesoNeto
          TO sdet[scr_line()].sPesoBruto,
             sdet[scr_line()].sPesoTara,
             sdet[scr_line()].sPesoNeto
    DISPLAY "Peso Neto lleva: ", gr_det[idx].pesoNeto
 
          
  --Para pesoTotal
  IF idx = 1 THEN  
     LET gr_reg.pesoCli = gr_det[idx].pesoNeto
  ELSE  
     LET gr_reg.pesoCli = 0.0
     FOR i = 1 TO gr_det.getLength()
        IF gr_det[i].pesoNeto > 0 THEN 
           LET gr_reg.pesoCli = gr_reg.pesoCli + gr_det[i].pesoNeto 
        END IF    
     END FOR 
  END IF  
  
  DISPLAY gr_reg.pesoCli TO fPesoTotal1
}
  --retorna total
  --RETURN taraTotal
END FUNCTION 

FUNCTION lineaRepetida(d)
  DEFINE d ui.Dialog 

  LET idx = arr_curr()
  FOR i = 1 TO gr_det.getLength()
     IF i <> idx THEN
        IF gr_det[i].idcausa = gr_det[idx].idcausa THEN 
           RETURN TRUE 
        END IF 
     END IF 
  END FOR 
  RETURN FALSE  
END FUNCTION 

FUNCTION porcentajeMayor(d)
  DEFINE d ui.Dialog 
  DEFINE totPorcentaje SMALLINT 

  LET idx = arr_curr()
  LET totPorcentaje = 0 
  FOR i = 1 TO gr_det.getLength()
     IF gr_det[i].porcentaje IS NOT NULL THEN
     
        LET totPorcentaje = totPorcentaje + gr_det[i].porcentaje
     END IF 
  END FOR 
  IF totPorcentaje > 100 THEN 
     RETURN TRUE 
  ELSE 
     RETURN FALSE 
  END IF 
  
END FUNCTION 

FUNCTION porcentajeMenor(d)
  DEFINE d ui.Dialog 
  DEFINE totPorcentaje SMALLINT 

  LET idx = arr_curr()
  LET totPorcentaje = 0 
  FOR i = 1 TO gr_det.getLength()
     IF gr_det[i].porcentaje IS NOT NULL THEN
        LET totPorcentaje = totPorcentaje + gr_det[i].porcentaje
     END IF 
  END FOR 
  IF totPorcentaje <> 100 THEN 
     RETURN TRUE 
  ELSE 
     RETURN FALSE 
  END IF 
  
END FUNCTION 
