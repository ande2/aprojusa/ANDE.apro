--Librer�a empacada para interfaces a SAP-Semillas
--Todos los documentos crean la trazabilidad, log y partidas 
--contables exactamente igual a si se hicieran dentro de SAP B1
--IMPORT JAVA java.util.regex.Pattern
--IMPORT JAVA java.lang.StringBuffer
IMPORT JAVA sc_sap.conectorSAP
--IMPORT JAVA com.sap.smb.sbo.api.*



DATABASE db0001

--Registro para conexion a SAP
DEFINE gr_con RECORD    
  ipserver      VARCHAR (15) ,
  userDB        VARCHAR (80) ,
  passDB        VARCHAR (80) ,
  userSap       VARCHAR (80) ,
  passSap       VARCHAR (80)
END RECORD

--Registro de trabajo
DEFINE gr_reg RECORD 
    idSapPro              LIKE sapProducirEnc.idsappro ,
	docNum                LIKE sapProducirEnc.docnum  ,
	docType               LIKE sapProducirEnc.doctype  ,
	docDate               LIKE sapProducirEnc.docdate  ,
	docDueDate            LIKE sapProducirEnc.docduedate  ,
	docCurrency           LIKE sapProducirEnc.doccurrency  ,
	Reference1            LIKE sapProducirEnc.reference1  ,
	Comments              LIKE sapProducirEnc.comments  ,
	TaxDate               LIKE sapProducirEnc.taxdate  ,
	Series                LIKE sapProducirEnc.series  ,
	idBoletaIng           LIKE sapProducirEnc.idboletaing   ,
	estado                LIKE sapProducirEnc.estado  ,
	fectran               LIKE sapProducirEnc.fectran   ,
	usuOpero              LIKE sapProducirEnc.usuopero
END RECORD 

MAIN

  MENU 
    BEFORE MENU
      CALL DIALOG.setActionActive( "costo", FALSE )
      CALL DIALOG.setActionActive( "reciboprod", FALSE )
      CALL DIALOG.setActionActive( "salidainv", FALSE )
      CALL DIALOG.setActionActive( "entradainv", FALSE )
      CALL DIALOG.setActionActive( "entradamerc", FALSE )  
      
    ON ACTION conectar
      IF fConnect() THEN 
         CALL DIALOG.setActionActive( "costo", TRUE )
         CALL DIALOG.setActionActive( "reciboprod", TRUE )
         CALL DIALOG.setActionActive( "salidainv", TRUE )
         CALL DIALOG.setActionActive( "entradainv", TRUE )
         CALL DIALOG.setActionActive( "entradamerc", TRUE )
      ELSE   
         CALL DIALOG.setActionActive( "costo", FALSE ) 
         CALL DIALOG.setActionActive( "reciboprod", FALSE )
         CALL DIALOG.setActionActive( "salidainv", FALSE )
         CALL DIALOG.setActionActive( "entradainv", FALSE )
         CALL DIALOG.setActionActive( "entradamerc", FALSE )
      END IF 
      
    ON ACTION costo
       CALL fCost()

    ON ACTION reciboProd
       CALL fReciboProd()

    ON ACTION salidaInv
       CALL fSalidaInv() 

    ON ACTION entradaInv
       CALL fEntradaInv() 
    
    ON ACTION entradaMerc
       CALL fEntradaMerc() 

    ON ACTION testJava
       CALL fTestJava()   
                
    ON ACTION salir 
      EXIT MENU 
  END MENU 

END MAIN

FUNCTION fConnect()
--Paso 1 Conector SAP:
--Funci�n que recibe los par�metros de conexi�n al servidor 
--Parametros: IP del servidor, usuario DB, contrase�a DB, Usuario SAP, 
--contrase�a SAP, etc), esta clase se puede conectar a una empresa que se env�e 
--como par�metro y no crear� ning�na ventana de logeo, solo recibir� los datos 
--como par�metros de funci�n, la funci�n devuelve un INT indicando el resultado 
--de la operaci�n

 --a. Obtener parametros para conexion
 LET gr_con.ipserver = getValParam("SAP - IP SERVIDOR")
 LET gr_con.userDB   = getValParam("SAP - USUARIO DB")
 LET gr_con.passDB   = getValParam("SAP - CONTRASE�A DB")
 LET gr_con.userSap  = getValParam("SAP - USUARIO SAP")
 LET gr_con.passSap  = getValParam("SAP - CONTRASE�A SAP")
 
 ERROR  "IP        ", gr_con.ipserver, 
   "UserDB    ", gr_con.userDB, 
   "PassDB    ", gr_con.passDB, 
   "UserSap   ", gr_con.userSap, 
   "PassSap   ", gr_con.passSap

 --Con los parametros realiza la llamada a la funcion de conexion
 {IF NOT conectorSap(gr_con.ipserver, gr_con.userDB, gr_con.passDB, 
                gr_con.userSap, gr_con.passSap ) THEN
    CALL msg ("No se puede realizar la conexi�n a SAP")
    RETURN FALSE 
 ELSE 
    RETURN TRUE 
 END IF }

 RETURN TRUE  
    
END FUNCTION 

FUNCTION fCost()
--Paso 2 Validaci�n de costos: 
--Funci�n interna que verificar� en donde sea necesario que el costo del producto 
--que se est� enviando como par�metro est� acorde al que tiene el sistema
--Par�metros: 
--Valores que retorna: Res (INT) indica el resultado de la operaci�n
  ERROR  "Funci�n Costo"
  
END FUNCTION 


FUNCTION fReciboProd()
--Paso 3 Funci�n crearReciboProducci�n: 
--Funci�n que crear� un recibo de producci�n. 
--Par�metros: N�mero de orden de producci�n, fecha de contabilizaci�n, serie,
--un array de c�digos de productos, 
--un array con las descripciones de los productos, 
--un array con las cantidades de los productos, 
--un array con los costos de los productos -> El costo no se calcula durante el ingreso?
-->Puede ser un solo array con todo?
--Valores que retorna: Res (INT) indica el resultado de la operaci�n
DEFINE 
  OrdPro    LIKE pemmordpro.numOrdPro,
  fechaCon  DATE , 
  serie     VARCHAR (50)
  
DEFINE gr_items DYNAMIC ARRAY OF RECORD  
   idItem   LIKE itemsxgrupo.iditemsap
END RECORD      

DEFINE gr_itemsNom DYNAMIC ARRAY OF RECORD  
   nombre   LIKE itemsxgrupo.nombre
END RECORD 

DEFINE gr_itemsCant DYNAMIC ARRAY OF RECORD  
   nombre   INTEGER 
END RECORD 

DEFINE gr_itemsCost DYNAMIC ARRAY OF RECORD  
   costo   DECIMAL (9,2) 
END RECORD 

DEFINE res TINYINT 

--Cargando los valores

  ERROR  "Funci�n Recibo de Producci�n"
  
END FUNCTION 

FUNCTION fSalidaInv()
--Paso 4 Funci�n crearSalidaInventario: 
--Funci�n que crea una salida de inventario. 
--Par�metros: fecha de contabilizaci�n, serie, array de cantidades de productos, 
--array de descripci�n de productos, array de costo de productos.
--Valores que retorna: Res (INT) indica el resultado de la operaci�n
  ERROR  "Funci�n Salida de Inventario"

END FUNCTION 

FUNCTION fEntradaInv()
--Paso 5 Funci�n crearEntradaInventario: 
--Funci�n que crea una entrada de inventario
--Par�metros: fecha de contabilizaci�n, serie, array de cantidades de productos, 
--array de descripci�n de productos, array de costo de productos, 
--array de cuentas de inventario.
--Valores que retorna: Res (INT) indica el resultado de la operaci�n
  ERROR  "Funci�n Entrada a Inventario"

END FUNCTION 

FUNCTION fEntradaMerc() 
--Paso 6 Funci�n crearEntradaMercancia: 
--Funci�n que crea una entrada de mercancia.
--Par�metros: fecha de contabilizaci�n, serie, c�digo de socio de negocio, 
--array de cantidades de productos, array de descripci�n de productos, 
--array de costo de productos.
--Valores que retorna: Res (INT) indica el resultado de la operaci�n.
  ERROR  "Funci�n Entrada de Mercanc�a"

END FUNCTION 

FUNCTION varios()


--Llenar tabla de encabezado con datos para SAP solamente para las boletas locales
--Produccion / Recibo de produccion

DECLARE cur01 CURSOR FOR 
SELECT idboletaing 
FROM pemmboleta 
WHERE tipoBoleta = 1 --locales
AND estado = 0       --Cerradas
AND idboletaing NOT IN 
    (SELECT idboletaing FROM sapProducirEnc) --no han sido procesadas
FOREACH cur01 INTO gr_reg.idBoletaIng
  DISPLAY gr_reg.idBoletaIng
  --Generar archivo para envio
    --idSapPro
  LET gr_reg.idSapPro = 0 --serial
  --docNum
  SELECT idOrdPro INTO gr_reg.docNum FROM pemMBoleta 
  WHERE idboletaing = gr_reg.idBoletaIng
  --docType
  --LET gr_reg.docType = ??  
	{docDate            
	docDueDate         
	docCurrency        
	Reference1         
	Comments           
	TaxDate            
	Series             
	idBoletaIng        
	estado             
	fectran            
	usuOpero           
  SELECT }
END FOREACH  

--Ejecutar proceso 

--Actualizar estado de 
END FUNCTION 



FUNCTION fTestJava()
DEFINE res INT 
--DEFINE p Pattern
--DEFINE x java.lang.StringBuffer
DEFINE a sc_sap.conectorSAP

  --LET p = Pattern.compile("[,\\s]+")
   --LET x = StringBuffer.create()
   --CALL change(x)
   --ERROR  x.toString() , 
     -- "Java p: ", p

      --conectorSAP(String DBServer,String DBUser,String DBPass, String LicenseServer, String SAPUser, String SAPPass)
   LET a =  sc_sap.conectorSAP.create("DBServer", "DBUser", "DBPass", "LicenseServer", "SAPUser", "SAPPass")   
   --CALL conectorSAP("DBServer", "DBUser", "DBPass", "LicenseServer", "SAPUser", "SAPPass")
   --LET res = isconected()
   DISPLAY "Resultado ", res   

END FUNCTION 

{FUNCTION change(sb)
   DEFINE sb java.lang.StringBuffer
   CALL sb.append("abc")
END FUNCTION}
