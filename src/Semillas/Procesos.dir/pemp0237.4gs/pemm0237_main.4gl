DATABASE db0001 

--Registro para conexion a SAP
DEFINE gr_con RECORD    
  ipserver      VARCHAR (15) ,
  userDB        VARCHAR (80) ,
  passDB        VARCHAR (80) ,
  userSap       VARCHAR (80) ,
  passSap       VARCHAR (80)
END RECORD

--Registro de trabajo
DEFINE gr_reg RECORD 
    idSapPro              LIKE sapProducirEnc.idsappro ,
	docNum                LIKE sapProducirEnc.docnum  ,
	docType               LIKE sapProducirEnc.doctype  ,
	docDate               LIKE sapProducirEnc.docdate  ,
	docDueDate            LIKE sapProducirEnc.docduedate  ,
	docCurrency           LIKE sapProducirEnc.doccurrency  ,
	Reference1            LIKE sapProducirEnc.reference1  ,
	Comments              LIKE sapProducirEnc.comments  ,
	TaxDate               LIKE sapProducirEnc.taxdate  ,
	Series                LIKE sapProducirEnc.series  ,
	idBoletaIng           LIKE sapProducirEnc.idboletaing   ,
	estado                LIKE sapProducirEnc.estado  ,
	fectran               LIKE sapProducirEnc.fectran   ,
	usuOpero              LIKE sapProducirEnc.usuopero
END RECORD 

MAIN

--Librer�a empacada

--Paso 1 Conector SAP:
--Funci�n que recibe los par�metros de conexi�n al servidor 
--Parametros: IP del servidor, usuario DB, contrase�a DB, Usuario SAP, 
--contrase�a SAP, etc), esta clase se puede conectar a una empresa que se env�e 
--como par�metro y no crear� ning�na ventana de logeo, solo recibir� los datos 
--como par�metros de funci�n, la funci�n devuelve un INT indicando el resultado 
--de la operaci�n

 --a. Obtener parametros para conexion
 LET gr_con.ipserver = getValParam("SAP - IP SERVIDOR")
 LET gr_con.userDB   = getValParam("SAP - USUARIO DB")
 LET gr_con.passDB   = getValParam("SAP - CONTRASE�A DB")
 LET gr_con.userSap  = getValParam("SAP - USUARIO SAP")
 LET gr_con.passSap  = getValParam("SAP - CONTRASE�A SAP")
 
 DISPLAY "IP        ", gr_con.ipserver
 DISPLAY "UserDB    ", gr_con.userDB
 DISPLAY "PassDB    ", gr_con.passDB
 DISPLAY "UserSap   ", gr_con.userSap
 DISPLAY "PassSap   ", gr_con.passSap

--Paso 2 Validaci�n de costos: 
--Funci�n interna que verificar� en donde sea necesario que el costo del producto 
--que se est� enviando como par�metro est� acorde al que tiene el sistema
--Par�metros: 
--Valores que retorna: Res (INT) indica el resultado de la operaci�n

--Paso 3 Funci�n crearReciboProducci�n: 
--Funci�n que crear� un recibo de producci�n. 
--Par�metros: N�mero de orden de producci�n, fecha de contabilizaci�n, 
--serie, un array de c�digos de productos, un array con las descripciones de 
--los productos, un array con las cantidades de los productos, 
--un array con los costos de los productos.
--Valores que retorna: Res (INT) indica el resultado de la operaci�n

--Paso 4 Funci�n crearSalidaInventario: 
--Funci�n que crea una salida de inventario. 
--Par�metros: fecha de contabilizaci�n, serie, array de cantidades de productos, 
--array de descripci�n de productos, array de costo de productos.
--Valores que retorna: Res (INT) indica el resultado de la operaci�n

--Paso 5 Funci�n crearEntradaInventario: 
--Funci�n que crea una entrada de inventario
--Par�metros: fecha de contabilizaci�n, serie, array de cantidades de productos, 
--array de descripci�n de productos, array de costo de productos, 
--array de cuentas de inventario.
--Valores que retorna: Res (INT) indica el resultado de la operaci�n

--Paso 6 Funci�n crearEntradaMercancia: 
--Funci�n que crea una entrada de mercancia.
--Par�metros: fecha de contabilizaci�n, serie, c�digo de socio de negocio, 
--array de cantidades de productos, array de descripci�n de productos, 
--array de costo de productos.
--Valores que retorna: Res (INT) indica el resultado de la operaci�n.

--Todos los documentos anteriores crear�n la trazabilidad, log y partidas 
--contables exactamente igual a si se hicieran dentro de SAP B1

--Fin

--Llenar tabla de encabezado con datos para SAP solamente para las boletas locales
--Produccion / Recibo de produccion

DECLARE cur01 CURSOR FOR 
SELECT idboletaing 
FROM pemmboleta 
WHERE tipoBoleta = 1 --locales
AND estado = 0       --Cerradas
AND idboletaing NOT IN 
    (SELECT idboletaing FROM sapProducirEnc) --no han sido procesadas
FOREACH cur01 INTO gr_reg.idBoletaIng
  DISPLAY gr_reg.idBoletaIng
  --Generar archivo para envio
    --idSapPro
  LET gr_reg.idSapPro = 0 --serial
  --docNum
  SELECT idOrdPro INTO gr_reg.docNum FROM pemMBoleta 
  WHERE idboletaing = gr_reg.idBoletaIng
  --docType
  --LET gr_reg.docType = ??  
	{docDate            
	docDueDate         
	docCurrency        
	Reference1         
	Comments           
	TaxDate            
	Series             
	idBoletaIng        
	estado             
	fectran            
	usuOpero           
  SELECT }
END FOREACH  




--Ejecutar proceso 

--Actualizar estado de 

END MAIN
