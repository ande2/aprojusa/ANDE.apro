################################################################################
# Funcion     : %M%
# Descripcion : Ingreso de un registro nuevo 	
# Funciones   : input_umd() funcion para insertar datos
#              
#              
#               
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Anderson Garcia 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################

GLOBALS "pemp0207_glob.4gl"

FUNCTION insert_init()
DEFINE strSql STRING 

   LET strSql =
      "INSERT INTO pemMBoleta ( ",
        " idBoletaIng, fecha, esdirecto, tipoBoleta, estado, ",
        " idOrdPro, idDOrdPro, emplEntrego, docPropioSerie, docPropio, ",
        " productor, idItem, nomEntrego, docProductor, ",
        " tipoCaja, tieneTarima, pesoTotal, ",
        " emplRecibio, fecTran ",
        "   ) ",
      " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"

   PREPARE st_insertar FROM strSql

   LET strSql = 
     "INSERT INTO pemDBoleta ( ",
     " idBoletaIng, idLinea, ",
     " cantCajas, pesoBruto, pesoTara, pesoNeto ",
     " ) ",
     " VALUES (?,?,?,?,?,?) "
   PREPARE st_insertarDet FROM strSql

END FUNCTION 

FUNCTION ingreso()
   CALL encabezado("Ingresar")
   --CALL info_usuario()
   IF captura_datos('I') THEN
      RETURN grabar()
   ELSE 
      RETURN FALSE 
   END IF 
END FUNCTION 

FUNCTION grabar()
DEFINE i SMALLINT 

   LET g_reg.idBoletaIng = 0
   
   LET g_reg.fecTran = CURRENT
   IF g_reg.esDirecto = 1 THEN 
      LET g_reg.estado = 4
   END IF 

   WHENEVER ERROR CONTINUE
   BEGIN WORK 
   TRY 
      EXECUTE st_insertar USING 
        g_reg.idBoletaIng, g_reg.fecha, g_reg.esDirecto, g_reg.tipoBoleta, 
        g_reg.estado, 
        g_reg.idOrdPro, g_reg.idDOrdPro, g_reg.emplEntrego, 
        g_reg.docPropioSerie, g_reg.docPropio, 
        g_reg.productor, g_reg.idItem, g_reg.nomEntrego, g_reg.docProductor,
        g_reg.tipoCaja, g_reg.tieneTarima, g_reg.pesoTotal,
        g_reg.emplRecibio, g_reg.fecTran  
        
      --Para ID 
      LET g_reg.idBoletaIng = SQLCA.sqlerrd[2]
      IF g_reg.tipoBoleta = 1 THEN 
         --Finca
         TRY 
            FOR i= 1 TO reg_detArr.getLength()
               --LET reg_detArr[i].idOrdPro = g_reg.idOrdPro
               EXECUTE st_insertarDet USING 
                 g_reg.idBoletaIng, i,
                 reg_detArr[i].cantCajas, reg_detArr[i].pesoBruto,
                 reg_detArr[i].pesoTara, reg_detArr[i].pesoNeto 
            END FOR 
         CATCH 
            ROLLBACK WORK 
            WHENEVER ERROR STOP
            CALL msgError(sqlca.sqlcode,"Grabar Detalle de Registro")
            RETURN FALSE 
         END TRY
      ELSE 
         --Productor
         TRY 
            FOR i= 1 TO reg_detArr2.getLength()
               --LET reg_detArr[i].idOrdPro = g_reg.idOrdPro
               EXECUTE st_insertarDet USING 
                 g_reg.idBoletaIng, i,
                 reg_detArr2[i].cantCajas, reg_detArr2[i].pesoBruto,
                 reg_detArr2[i].pesoTara, reg_detArr2[i].pesoNeto 
            END FOR 
         CATCH 
            ROLLBACK WORK 
            WHENEVER ERROR STOP
            CALL msgError(sqlca.sqlcode,"Grabar Detalle de Registro")
            RETURN FALSE 
         END TRY
      END IF 
      
   CATCH 
      CALL msgError(sqlca.sqlcode,"Grabar Registro")
      RETURN FALSE 
   END TRY
   COMMIT WORK 
   WHENEVER ERROR STOP
   CALL dispReg()
   CALL box_valdato ("Registro agregado")
   --CALL combo_din2("cmbdepartamento","SELECT * FROM commdep WHERE id_commdep > 0")
   RETURN TRUE 
END FUNCTION 

 