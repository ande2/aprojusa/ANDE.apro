################################################################################
# Funcion     : %M%
# Descripcion : Funcion para modificar registros
# Funciones   : updel_init()
#	             delete_umd() 
#               update_umd()
#               pk_estado()
#               valida_modi()
#               verifica_modi()
#               existe_datos()
#               existe_dpro()
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################
GLOBALS "pemp0207_glob.4gl"

FUNCTION update_init()
DEFINE strSql STRING 

   LET strSql = 
      "UPDATE pemMBoleta ",
      "SET ",
      " fecha       = ?, ",
      " esdirecto   = ?, ",
      " tipoBoleta  = ?, ",
      " estado      = ?, ",
      " idOrdPro    = ?, ",
      " idDOrdPro   = ?, ",
      " emplEntrego = ?, ",
      " docPropioSerie = ?, ",
      " docPropio   = ?, ",
      " productor   = ?, ",
      " idItem      = ?, ",
      " nomEntrego  = ?, ",
      " docProductor= ?, ",
      " tipoCaja    = ?, ",
      " tieneTarima = ?, ",
      " pesoTotal   = ?, ",
      " emplRecibio = ?, ",
      " fecTran     = ?  ",
      " WHERE idBoletaIng  = ?"
      
   PREPARE st_modificar FROM strSql

   LET strSql =
      "INSERT INTO pemDBoleta ( ",
        " idBoletaIng, idLinea, cantCajas, pesoBruto, pesoTara, pesoNeto ",
        "   ) ",
      " VALUES (?,?,?,?,?,?)"

   PREPARE st_insertarDetMod FROM strSql
   
END FUNCTION 

FUNCTION modifica()
   CALL encabezado("Modificar")
   --CALL info_usuario()
   
   IF captura_datos('M') THEN
      RETURN actualizar()
   ELSE 
      RETURN FALSE 
   END IF 
END FUNCTION 

FUNCTION delete_init()
DEFINE strSql STRING 

   LET strSql =
      "DELETE FROM pemMBoleta ",
      "WHERE idBoletaIng = ? "
      
   PREPARE st_delete FROM strSql

   LET strSql =
      "DELETE FROM pemDBoleta ",
      "WHERE idBoletaIng = ? "
      
   PREPARE st_deleteDet FROM strSql

END FUNCTION 

FUNCTION actualizar()
DEFINE i SMALLINT 

   WHENEVER ERROR CONTINUE 
   BEGIN WORK 
   --Modifica encabezado
   TRY
      EXECUTE st_modificar USING 
        g_reg.fecha, g_reg.esDirecto, g_reg.tipoBoleta, g_reg.estado,
        g_reg.idOrdPro, g_reg.idDOrdPro, g_reg.emplEntrego, 
        g_reg.docPropioSerie, g_reg.docPropio,
        g_reg.productor, g_reg.idItem, g_reg.nomEntrego, g_reg.docProductor, 
        g_reg.tipoCaja, g_reg.tieneTarima,  g_reg.pesoTotal,
        g_reg.emplRecibio, g_reg.fecTran,
        u_reg.idBoletaIng  

      --Si todo bien elimina el detalle
      TRY
         EXECUTE st_deleteDet USING 
           g_reg.idBoletaIng 
         --Si todo bien inserta nuevo detalle
         TRY 
            FOR i= 1 TO reg_detArr.getLength()
               --LET reg_detArr[i].idOrdPro = g_reg.idOrdPro
               EXECUTE st_insertarDetMod USING 
                 g_reg.idBoletaIng, i, 
                 reg_detArr[i].cantCajas, reg_detArr[i].pesoBruto,
                 reg_detArr[i].pesoTara, reg_detArr[i].pesoNeto 
            END FOR 
         CATCH 
            ROLLBACK WORK 
            WHENEVER ERROR STOP
            CALL msgError(sqlca.sqlcode,"Grabar Detalle de Registro")
            RETURN FALSE 
         END TRY
      CATCH 
         ROLLBACK WORK
         WHENEVER ERROR STOP 
         CALL msgError(sqlca.sqlcode,"Modificar Detalle")
         RETURN FALSE 
      END TRY
   CATCH 
      ROLLBACK WORK
      WHENEVER ERROR STOP 
      CALL msgError(sqlca.sqlcode,"Modificar Registro")
      RETURN FALSE 
   END TRY
   COMMIT WORK 
   WHENEVER ERROR STOP 
   CALL msg ("Registro actualizado")
   RETURN TRUE 
   
END FUNCTION 


FUNCTION anular()
   IF NOT box_confirma("Esta seguro de anular el registro") THEN
      RETURN FALSE
   END IF 
   WHENEVER ERROR CONTINUE 
   --BEGIN WORK 
   --Eliminar detalle
   TRY
      UPDATE pemmboleta SET estado = -1
      WHERE idboletaing = g_reg.idBoletaIng
      {EXECUTE st_deleteDet USING 
           g_reg.idBoletaIng}
      --Si todo bien elimina encabezado
      {TRY 
         --UPDATE pemmboleta SET 
         EXECUTE st_delete USING g_reg.idBoletaIng
      CATCH 
         CALL msgError(sqlca.sqlcode,"Eliminar Registro")
         ROLLBACK WORK
         WHENEVER ERROR STOP 
         RETURN FALSE 
      END TRY}
   CATCH 
      --ROLLBACK WORK
      WHENEVER ERROR STOP 
      CALL msgError(sqlca.sqlcode,"Anular registro")
      RETURN FALSE 
   END TRY
   --COMMIT WORK 
   WHENEVER ERROR STOP 
   CALL msg("Registro anulado")
   RETURN TRUE
   
END FUNCTION 