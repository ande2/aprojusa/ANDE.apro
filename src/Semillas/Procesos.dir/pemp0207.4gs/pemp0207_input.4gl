
GLOBALS "pemp0207_glob.4gl"

DEFINE esPropia SMALLINT 
DEFINE idx9 SMALLINT 

FUNCTION captura_datos(operacion)
DEFINE operacion CHAR (1)
DEFINE resultado BOOLEAN
DEFINE w ui.Window
DEFINE f ui.Form
DEFINE sql_stmt STRING 
DEFINE preCantCajas SMALLINT 
DEFINE vusuario VARCHAR (30) 


DEFINE sqlTxt STRING 

   LET resultado = FALSE 
   LET u_reg.* = g_reg.*
   IF operacion = 'I' THEN 
      CALL cleanAll()
      --INITIALIZE g_reg.* TO NULL
      CALL dispReg()
      
   END IF
   DIALOG ATTRIBUTES(UNBUFFERED)
     INPUT --BY NAME  
      --g_reg.idBoletaIng, g_reg.fecha, g_reg.tipoBoleta, g_reg.estado 
      g_reg.esDirecto,
      g_reg.idOrdPro, g_reg.idDOrdPro, g_reg.emplEntrego, 
      g_reg.docPropioSerie, g_reg.docPropio,
      --CS g_reg.productor, g_reg.idItem, g_reg.nomEntrego, g_reg.docProductor,
      g_reg.tipoCaja, g_reg.tieneTarima
      --, g_reg.pesoTotal
      --g_reg.emplRecibio, g_reg.fecTran,
     FROM
      --fIdBoletaIng, fFecha, fTipoBoleta, fEstado, 
      fesdirecto,
      fIdOrdPro, fIdDOrdPro, fEmplEntrego,
      fdocpropioserie, fdocpropio,
      --fproductor, fIdItem, fNomEntrego, fdocproductor,
      --fEmplRecibio, fFecTran, 
      fTipoCaja, fTieneTarima
      --, fPesoTotal
      ATTRIBUTES (WITHOUT DEFAULTS)
      
      BEFORE INPUT
         CALL DIALOG.setActionHidden("close",true)
         --Para id de boleta
         IF operacion = 'I' THEN
            LET g_reg.idBoletaIng = 0 
            LET g_reg.esDirecto = 0
         
            DISPLAY g_reg.idBoletaIng TO fidboletaing
            DISPLAY g_reg.esDirecto TO fesdirecto
         
            LET vusuario = ARG_VAL(3)
            DISPLAY "vusuario ", vusuario
         
            --Para fecha
            LET g_reg.fecha = TODAY 
            DISPLAY g_reg.fecha TO fFecha

            --Para estado
            LET g_reg.estado = 1
            DISPLAY g_reg.estado TO festado

            LET g_reg.tieneTarima = 1
            LET g_reg.pesoTotal = 0.0

            --Para usuario que recibio - LOGNAME
            LET sql_stmt = "SELECT id_commempl ",
                           " FROM commempl WHERE usuLogin = '", vusuario, "'" --get_AndeVarLog("LOGNAME"), "'"
            PREPARE ex_emplRecibio FROM sql_stmt
            EXECUTE ex_emplRecibio INTO g_reg.emplRecibio
            IF g_reg.emplRecibio IS NULL THEN
               LET g_reg.emplRecibio = 100
            END IF
            DISPLAY g_reg.emplRecibio TO femplrecibio1  
            DISPLAY g_reg.emplRecibio TO femplrecibio  
          END IF 
          
      ON CHANGE fIdOrdPro
         LET sql_stmt = 
             "SELECT idDOrdPro, fincaNomCt || '-'||nomEstructura || '-'|| NVL(nomValvula,'') || '-' || desItemLg ",
             " FROM pemdordpro a, pemmfinca b, mestructura c, OUTER(pemMValvula d), glbMItems e ",
             " WHERE a.idfinca = b.idfinca ",
             " AND a.idestructura = c.idEstructura ",
             " AND a.idValvula = d.idValvula ",
             " AND a.idItem = e.idItem ",
             " AND a.idOrdPro = ", g_reg.idOrdPro
         CALL combo_din2("fiddordpro", sql_stmt)
         CALL cleanProductor()

      ON CHANGE fIdDOrdPro
         CALL cleanProductor()  

      ON CHANGE fEmplEntrego 
        CALL cleanProductor() 
        IF g_reg.emplEntrego IS NULL THEN
           CALL msg("Debe ingresar emplEntrego")
           NEXT FIELD CURRENT
        END IF  
         
      BEFORE FIELD fdocpropio
        IF g_reg.docPropio IS NULL THEN
           SELECT serieEnvio || '-'
           INTO g_reg.docPropioSerie
            FROM pemmfinca a, pemDOrdPro b   
            WHERE b.idfinca = a.idfinca
            AND b.idordpro = g_reg.idOrdPro
            AND b.iddordpro = g_reg.idDOrdPro
        END IF
        DISPLAY g_reg.docPropioSerie TO fdocpropioserie
        
      ON CHANGE fdocpropio
         SELECT FIRST 1 docPropio FROM pemMBoleta 
         WHERE docPropioSerie = g_reg.docPropioSerie 
         AND docPropio = g_reg.docPropio
         IF SQLca.sqlcode = 0 THEN
            CALL msg("N�mero de documento ya existe")
            NEXT FIELD CURRENT
         END IF 
      
      ON CHANGE fTipoCaja 
        IF g_reg.tipoCaja IS NULL THEN
           CALL msg("Debe ingresar tipoCaja")
           NEXT FIELD CURRENT
        END IF 
      
   END INPUT 

   INPUT ARRAY reg_detArr FROM sDet2.*
      ATTRIBUTES ( WITHOUT DEFAULTS, INSERT ROW = FALSE) --, DELETE ROW = FALSE)

      BEFORE INPUT
        CALL fgl_dialog_setkeylabel("Append", "")
    
      --BEFORE ROW 
      BEFORE FIELD scantcajas
         IF arr_curr() <> arr_count() THEN 
            CALL DIALOG.setCurrentRow("sdet2", arr_count())
         END IF 
         IF g_reg.tipoBoleta = 1 THEN
            IF g_reg.idOrdPro IS NULL THEN
               CALL msg("Debe ingresar orden de producci�n")
               NEXT FIELD fIdOrdPro
            END IF 
            IF g_reg.idDOrdPro IS NULL THEN
               CALL msg("Debe ingresar estructura")
               NEXT FIELD fIdDOrdPro
            END IF 
            IF g_reg.emplEntrego IS NULL THEN
               CALL msg("Debe seleccionar empleado que entrego")
               NEXT FIELD fEmplEntrego
            END IF
            IF g_reg.docPropio IS NULL THEN
               CALL msg("Debe ingresar numero de documento de entrega")
               NEXT FIELD fdocpropio
            END IF
         ELSE 
            IF g_reg.idItem IS NULL THEN
               CALL msg("Debe ingresar Item")
               NEXT FIELD fIdItem
            END IF  
            IF g_reg.nomEntrego IS NULL THEN
               CALL msg("Debe ingresar nombre de qui�n entreg� el producto")
               NEXT FIELD fNomEntrego
            END IF  
            
         END IF 

         IF g_reg.tipoCaja IS NULL THEN
            CALL msg("Debe ingresar tipoCaja")
            NEXT FIELD fTipoCaja
         END IF
         --Para numero de linea
         LET reg_detArr[arr_curr()].idLinea = arr_curr()
         DISPLAY reg_detArr[arr_curr()].idLInea TO sdet2[scr_line()].sidlinea

      ON CHANGE scantcajas
         LET reg_detArr[arr_curr()].pesoBruto = NULL 
         LET reg_detArr[arr_curr()].pesoTara = NULL 
         LET reg_detArr[arr_curr()].pesoNeto = NULL 
         DISPLAY reg_detArr[arr_curr()].pesoBruto TO sdet2[scr_line()].spesobruto
         DISPLAY reg_detArr[arr_curr()].pesoTara TO sdet2[scr_line()].spesotara
         DISPLAY reg_detArr[arr_curr()].pesoNeto TO sdet2[scr_line()].spesoneto 
      
      ON ACTION leerPeso
        -- Desactivando opcion de grabar
        --CALL Dialog.SetActionActive("accept",FALSE)
        
        IF reg_detArr[arr_curr()].cantCajas IS NULL THEN 
           CALL msg("Debe ingresar # de Cajas")
           NEXT FIELD scantcajas
        ELSE 
           LET reg_detArr[arr_curr()].pesoBruto = getPeso()
           CALL calculaPesos(1)
           IF reg_detArr[arr_curr()].pesoNeto <= 0 THEN
              CALL msg("Peso Neto NO puede ser menor o igual a 0, intente de nuevo")
              NEXT FIELD scantcajas 
           END IF 
           
           LET preCantCajas = reg_detArr[arr_curr()].cantCajas
           NEXT FIELD NEXT 
        END IF

      AFTER INSERT
         IF reg_detArr[arr_curr()].cantCajas IS NULL 
            OR reg_detArr[arr_curr()].pesoBruto IS NULL
            OR reg_detArr[arr_curr()].pesoTara IS NULL
            OR reg_detArr[arr_curr()].pesoTara IS NULL THEN
            CANCEL INSERT
         END IF
         --Revisa que la linea no se duplique 
         {IF repLine(arr_curr()) THEN 
            CALL msg("L�nea repetida")
            NEXT FIELD sIdFinca
         END IF 

      AFTER INSERT
         IF d_gru[idx1] IS NULL THEN
            CANCEL INSERT
         END IF

      ON ACTION DELETE
         LET idx1 = arr_curr()
         CALL DIALOG.deleteRow("sdet1", idx1)}
      
   END INPUT 

   INPUT --BY NAME  
       g_reg.productor, g_reg.idItem, g_reg.nomEntrego, g_reg.docProductor,
       g_reg.tipoCaja, g_reg.tieneTarima
      --, g_reg.pesoTotal
      --g_reg.emplRecibio, g_reg.fecTran,
     FROM
      fproductor, fIdItem, fNomEntrego, fdocproductor,
      --fEmplRecibio, fFecTran, 
      fTipoCaja1, fTieneTarima1
      --, fPesoTotal
      ATTRIBUTES (WITHOUT DEFAULTS)
   --g_reg.productor, g_reg.idItem, g_reg.nomEntrego, g_reg.docProductor,
      --g_reg.tipoCaja, g_reg.tieneTarima

      ON CHANGE fproductor
         CALL cleanPropia() 
            --LET sql_stmt = ' SELECT a.idItem, trim (b.nombre)||" "|| trim( c.nombre)|| " " || a.desItemLg ', 
                  --' FROM glbMItems a, glbmfam b, glbmtip c ',
                  --' WHERE a.idfamilia = b.idfamilia ', 
                  --' AND a.idtipo = c.idtipo '
         LET sqlTxt = ' SELECT idItem, trim (b.nombre)||" "|| trim( c.nombre)|| " " || a.desItemLg  ',
                      ' FROM glbMItems a, glbmfam b, glbmtip c ',
                      ' WHERE a.idfamilia = b.idfamilia ',
                      ' AND a.idtipo = c.idtipo ',
                      ' AND a.idItem in (select iditem from itemsxempresa where id_commemp =', g_reg.productor, ')'
         CALL combo_din2("fiditem", sqlTxt) 

    ON CHANGE fIdItem
         CALL cleanPropia()  
              
      ON CHANGE fNomEntrego 
        CALL cleanPropia()
        IF g_reg.nomEntrego IS NULL THEN
           CALL msg("Debe ingresar nomEntrego")
           NEXT FIELD CURRENT
        END IF  
    END INPUT 

    INPUT ARRAY reg_detArr2 FROM sDet3.*
      ATTRIBUTES ( WITHOUT DEFAULTS, INSERT ROW = FALSE) --, DELETE ROW = FALSE)

      BEFORE INPUT
        CALL fgl_dialog_setkeylabel("Append", "")
    
      --BEFORE ROW 
      BEFORE FIELD scantcajas1
         IF arr_curr() <> arr_count() THEN 
            CALL DIALOG.setCurrentRow("sdet3", arr_count())
         END IF 
         IF g_reg.tipoBoleta = 1 THEN
            IF g_reg.idOrdPro IS NULL THEN
               CALL msg("Debe ingresar orden de producci�n")
               NEXT FIELD fIdOrdPro
            END IF 
            IF g_reg.idDOrdPro IS NULL THEN
               CALL msg("Debe ingresar estructura")
               NEXT FIELD fIdDOrdPro
            END IF 
            IF g_reg.emplEntrego IS NULL THEN
               CALL msg("Debe seleccionar empleado que entrego")
               NEXT FIELD fEmplEntrego
            END IF
            IF g_reg.docPropio IS NULL THEN
               CALL msg("Debe ingresar numero de documento de entrega")
               NEXT FIELD fdocpropio
            END IF
         ELSE 
            IF g_reg.idItem IS NULL THEN
               CALL msg("Debe ingresar Item")
               NEXT FIELD fIdItem
            END IF  
            IF g_reg.nomEntrego IS NULL THEN
               CALL msg("Debe ingresar nombre de qui�n entreg� el producto")
               NEXT FIELD fNomEntrego
            END IF  
            
         END IF 

         IF g_reg.tipoCaja IS NULL THEN
            CALL msg("Debe ingresar tipoCaja")
            NEXT FIELD fTipoCaja1
         END IF
         --Para numero de linea
         LET reg_detArr2[arr_curr()].idLinea = arr_curr()
         DISPLAY reg_detArr2[arr_curr()].idLInea TO sdet3[scr_line()].sidlinea1

      ON CHANGE scantcajas1
         LET reg_detArr2[arr_curr()].pesoBruto = NULL 
         LET reg_detArr2[arr_curr()].pesoTara = NULL 
         LET reg_detArr2[arr_curr()].pesoNeto = NULL 
         DISPLAY reg_detArr2[arr_curr()].pesoBruto TO sdet3[scr_line()].spesobruto1
         DISPLAY reg_detArr2[arr_curr()].pesoTara TO sdet3[scr_line()].spesotara1
         DISPLAY reg_detArr2[arr_curr()].pesoNeto TO sdet3[scr_line()].spesoneto1 
      
      ON ACTION leerPeso
        IF reg_detArr2[arr_curr()].cantCajas IS NULL THEN 
           CALL msg("Debe ingresar # de Cajas")
           NEXT FIELD scantcajas1
        ELSE 
           LET reg_detArr2[arr_curr()].pesoBruto = getPeso()
           CALL calculaPesos(2)
           IF reg_detArr2[arr_curr()].pesoNeto <= 0 THEN
              CALL msg("Peso Neto NO puede ser menor o igual a 0, intente de nuevo")
              NEXT FIELD scantcajas 
           END IF 
           
           LET preCantCajas = reg_detArr2[arr_curr()].cantCajas
           NEXT FIELD NEXT 
        END IF
        
      AFTER INSERT
         IF reg_detArr2[arr_curr()].cantCajas IS NULL 
            OR reg_detArr2[arr_curr()].pesoBruto IS NULL
            OR reg_detArr2[arr_curr()].pesoTara IS NULL
            OR reg_detArr2[arr_curr()].pesoTara IS NULL THEN
            CANCEL INSERT
         END IF
         --Revisa que la linea no se duplique 
         {IF repLine(arr_curr()) THEN 
            CALL msg("L�nea repetida")
            NEXT FIELD sIdFinca
         END IF 

      AFTER INSERT
         IF d_gru[idx1] IS NULL THEN
            CANCEL INSERT
         END IF

      ON ACTION DELETE
         LET idx1 = arr_curr()
         CALL DIALOG.deleteRow("sdet1", idx1)}
      
   END INPUT 
      
   ON ACTION ACCEPT
        
        IF g_reg.tipoBoleta = 1 THEN
           IF g_reg.idOrdPro IS NULL THEN
              CALL msg("Debe ingresar orden de producci�n")
              NEXT FIELD fIdOrdPro
           END IF 
           IF g_reg.idDOrdPro IS NULL THEN
              CALL msg("Debe ingresar estructura")
              NEXT FIELD fIdDOrdPro
           END IF 
           IF g_reg.emplEntrego IS NULL THEN
              CALL msg("Debe seleccionar empleado que entrego")
              NEXT FIELD fEmplEntrego
           END IF
           IF g_reg.docPropio IS NULL THEN
              CALL msg("Debe ingresar numero de documento de entrega")
              NEXT FIELD fdocpropio
           END IF
           --inicio
           IF reg_detArr.getLength() > 0 THEN 
              --Verifica si el usuario cambio la cantidad de cajas cambio luego de leer el peso
              IF reg_detArr[arr_curr()].pesoNeto IS NOT NULL THEN 
                 IF preCantCajas > 0 THEN 
                    DISPLAY "entre precantcajas ", precantcajas
                    IF preCantCajas <>  reg_detArr[arr_curr()].cantCajas THEN 
                       CALL msg("Cambio cantidad de cajas, debe leer nuevamente el peso")
                       CONTINUE DIALOG
                    END IF
                 END IF    
              ELSE 
                 IF reg_detArr[reg_detArr.getLength()].pesoNeto IS NULL THEN
                    CALL DIALOG.deleteRow("sdet2", reg_detArr.getLength())
                 END IF 
              END IF 
           ELSE 
              CALL msg("Debe ingresar # de cajas y leer el peso")
              CONTINUE DIALOG
           END IF  
           IF g_reg.tipoCaja IS NULL THEN
              CALL msg("Debe ingresar tipo Caja")
              NEXT FIELD fTipoCaja
           END IF 
           IF arr_curr() = 1 THEN  
              IF reg_detArr[arr_curr()].cantCajas IS NULL 
                  OR reg_detArr[arr_curr()].pesoBruto IS NULL
                  OR reg_detArr[arr_curr()].pesoTara IS NULL
                  OR reg_detArr[arr_curr()].pesoTara IS NULL THEN
                  CALL msg("Debe ingresar # de cajas y leer el peso")
                  CONTINUE DIALOG
               ELSE 
                  CALL calculaPesos(1)
                  IF reg_detArr[arr_curr()].pesoNeto <= 0 THEN
                    CALL msg("Peso Neto NO puede ser menor o igual a 0, intente de nuevo")
                    NEXT FIELD scantcajas 
                 END IF 
               END IF
           END IF
           --fin 
        ELSE 
           IF g_reg.idItem IS NULL THEN
              CALL msg("Debe ingresar Item")
              NEXT FIELD fIdItem
           END IF  
           IF g_reg.nomEntrego IS NULL THEN
              CALL msg("Debe ingresar nombre de qui�n entreg� el producto")
              NEXT FIELD fNomEntrego
           END IF 
           --inicio
           IF reg_detArr2.getLength() > 0 THEN 
              --Verifica si el usuario cambio la cantidad de cajas cambio luego de leer el peso
              IF reg_detArr2[arr_curr()].pesoNeto IS NOT NULL THEN 
                 IF preCantCajas <> reg_detArr2[arr_curr()].cantCajas THEN 
                    CALL msg("Cambio cantidad de cajas, debe leer nuevamente el peso")
                    CONTINUE DIALOG
                 END IF
              ELSE 
                 IF reg_detArr2[reg_detArr2.getLength()].pesoNeto IS NULL THEN
                    CALL DIALOG.deleteRow("sdet3", reg_detArr2.getLength())
                 END IF 
              END IF 
           ELSE 
              CALL msg("Debe ingresar # de cajas y leer el peso")
              CONTINUE DIALOG
           END IF  
           IF g_reg.tipoCaja IS NULL THEN
              CALL msg("Debe ingresar tipo Caja")
              NEXT FIELD fTipoCaja1
           END IF 
           IF arr_curr() = 1 THEN  
              IF reg_detArr2[arr_curr()].cantCajas IS NULL 
                  OR reg_detArr2[arr_curr()].pesoBruto IS NULL
                  OR reg_detArr2[arr_curr()].pesoTara IS NULL
                  OR reg_detArr2[arr_curr()].pesoTara IS NULL THEN
                  CALL msg("Debe ingresar # de cajas y leer el peso")
                  CONTINUE DIALOG
               ELSE 
                  CALL calculaPesos(2)
                  IF reg_detArr2[arr_curr()].pesoNeto <= 0 THEN
                    CALL msg("Peso Neto NO puede ser menor o igual a 0, intente de nuevo")
                    NEXT FIELD scantcajas 
                 END IF 
               END IF
           END IF
           --fin 
        END IF 
        
      IF operacion = 'M' AND g_reg.* = u_reg.* THEN
         CALL msg("No se efectuaron cambios")
         EXIT DIALOG 
      END IF
      CALL infoCajas()
      CASE box_gradato("Seguro de grabar")
         WHEN "Si"
            LET resultado = TRUE
            EXIT DIALOG
         WHEN "No"
            EXIT DIALOG 
         OTHERWISE
            CONTINUE DIALOG 
      END CASE 
      LET resultado = TRUE
      EXIT DIALOG 
      
     ON ACTION CANCEL
        EXIT dialog
   END DIALOG
   
   IF NOT resultado THEN
      LET g_reg.* = u_reg.*
      CALL displayDetArr() 
   END IF 
   RETURN resultado 
END FUNCTION

FUNCTION cleanProductor()
  LET g_reg.productor = NULL 
  LET g_reg.idItem = NULL 
  LET g_reg.nomEntrego = NULL 
  LET g_reg.docProductor = NULL 
  DISPLAY g_reg.productor, g_reg.idItem, g_reg.nomEntrego, g_reg.docProductor
       TO fproductor, fiditem, fnomentrego, fdocproductor
  --LET esPropia = 1
  LET g_reg.tipoBoleta = 1
END FUNCTION 

FUNCTION cleanPropia()
  LET g_reg.idOrdPro = NULL 
  LET g_reg.idDOrdPro = NULL 
  LET g_reg.emplEntrego = NULL 
  LET g_reg.docPropio = NULL
  DISPLAY g_reg.idOrdPro, g_reg.idDOrdPro, g_reg.emplEntrego, g_reg.docPropio
       TO fidordpro, fiddordpro, femplentrego, fdocpropio
  --LET esPropia = 0
  LET g_reg.tipoBoleta = 2
END FUNCTION 

FUNCTION getPeso()
  DEFINE pesoManual SMALLINT
  DEFINE fn, fs, ft STRING 
  DEFINE cPeso STRING 
  DEFINE result SMALLINT, --,opc,activo   
         peso, resp   DEC(9,2)

  LET pesoManual = getValParam("LEER PESO")       
  --LET pesoManual = 1

  IF pesomanual = 1 THEN 
     PROMPT "Ingrese perso " FOR resp
     RETURN resp
  ELSE 
     -- Obteniendo peso de bascula
     LET fn = "C:\\\\Basculas\\\\SipLafarge.exe"
     LET fs = "C:\\\\SCALEWORKDIR\\\\pesos.prn"
     LET ft = "/tmp/file.txt"
     
     --LET fn = "bascula" --||w_mae_psj.numbas 
     CALL librut001_leerbascula(fn,fs,ft,"G")
     RETURNING result,peso
     DISPLAY "result ", result, " peso ", peso 
     IF NOT result THEN
        DISPLAY "entre if"
        RETURN -1  
     ELSE 
        DISPLAY "entre al else"
        RETURN peso
     END IF
     
  END IF  
END FUNCTION 

FUNCTION calculaPesos(fArray) --fArray qye arreglo esta pesando 1 local 2 productor
DEFINE pesoTarima, pesoCaja, pesoCajaT DECIMAL(5,2)
DEFINE idx, i, fArray SMALLINT 

  LET idx = arr_curr()
  --obtiene peso de tarima
  IF g_reg.tieneTarima THEN
     LET pesoTarima = getValParam("RECEPCION DE FRUTA - PESO DE TARIMA")
  ELSE 
     LET pesoTarima = 0.0
  END IF  
  
  --obtiene peso segun tipo de caja
  SELECT pesoCatEmpaque INTO pesoCaja
  FROM pemMcatEmp
  WHERE idCatEmpaque = g_reg.tipoCaja
  
  --Calcula Tara (multiplica cantCajas * pesoCaja) + pesoTarima
  IF fArray = 1 THEN 
     LET pesoCajaT =  reg_detArr[idx].cantCajas * pesoCaja

     LET reg_detArr[idx].pesoTara = pesoTarima + pesoCajaT
     DISPLAY "Tara lleva: ", reg_detArr[idx].pesoTara
  ELSE
     LET pesoCajaT =  reg_detArr2[idx].cantCajas * pesoCaja

     LET reg_detArr2[idx].pesoTara = pesoTarima + pesoCajaT
     DISPLAY "Tara lleva: ", reg_detArr2[idx].pesoTara
  END IF 
  
  --Para peso Neto
  IF fArray = 1 THEN 
     LET reg_detArr[idx].pesoNeto = reg_detArr[idx].pesoBruto -
                                    reg_detArr[idx].pesoTara
     DISPLAY reg_detArr[arr_curr()].pesoBruto,
             reg_detArr[arr_curr()].pesoTara,
             reg_detArr[arr_curr()].pesoNeto
          TO sdet2[scr_line()].sPesoBruto,
             sdet2[scr_line()].sPesoTara,
             sdet2[scr_line()].sPesoNeto
    DISPLAY "Peso Neto lleva: ", reg_detArr[idx].pesoNeto
  ELSE 
     LET reg_detArr2[idx].pesoNeto = reg_detArr2[idx].pesoBruto -
                                     reg_detArr2[idx].pesoTara
     DISPLAY reg_detArr2[arr_curr()].pesoBruto,
             reg_detArr2[arr_curr()].pesoTara,
             reg_detArr2[arr_curr()].pesoNeto
          TO sdet3[scr_line()].sPesoBruto1,
             sdet3[scr_line()].sPesoTara1,
             sdet3[scr_line()].sPesoNeto1
    DISPLAY "Peso Neto lleva: ", reg_detArr2[idx].pesoNeto
  END IF 
          
  --Para pesoTotal
  {IF idx = 1 THEN 
     IF fArray = 1 THEN 
        LET g_reg.pesoTotal = reg_detArr[idx].pesoNeto
     ELSE 
        LET g_reg.pesoTotal = reg_detArr2[idx].pesoNeto
     END IF 
  ELSE }
     IF fArray = 1 THEN 
        LET g_reg.pesoTotal = 0.0
        FOR i = 1 TO reg_detArr.getLength()
          IF reg_detArr[i].pesoNeto IS NOT NULL THEN 
            LET g_reg.pesoTotal = g_reg.pesoTotal + reg_detArr[i].pesoNeto 
          END IF   
        END FOR
     ELSE 
       LET g_reg.pesoTotal = 0.0
       FOR i = 1 TO reg_detArr2.getLength()
         IF reg_detArr2[i].pesoNeto IS NOT NULL THEN  
           LET g_reg.pesoTotal = g_reg.pesoTotal + reg_detArr2[i].pesoNeto 
         END IF    
       END FOR
     END IF 
  --END IF  

  
  IF fArray = 1 THEN 
     DISPLAY g_reg.pesoTotal TO fPesoTotal
  ELSE
     DISPLAY g_reg.pesoTotal TO fPesoTotal1
  END IF 
  --retorna total
  --RETURN taraTotal
END FUNCTION 

FUNCTION cleanAll()
   INITIALIZE g_reg.* TO NULL
   CALL reg_detArr.clear()
END FUNCTION

FUNCTION infoCajas()
  DEFINE sqlTxt STRING 
  
  OPEN WINDOW wCaja WITH FORM "pemp0207_form2"
  --Para tipo caja en control de cajas
   LET sqlTxt = "SELECT idCatEmpaque, nomCatEmpaque FROM pemMCatEmp WHERE IdTipEmpaque = ", 
                   getValParam("RECEPCION DE FRUTA - ID CATEGORIA CAJAS")
   CALL combo_din2("idcatempaque",sqlTxt)
  INPUT ARRAY d_cajaemp FROM sDet9.* --.idCatEmpaque, d_cajaemp.prestamo, d_cajaemp.devolucion
    ATTRIBUTES ( WITHOUT DEFAULTS, INSERT ROW = FALSE)
    
    AFTER FIELD idcatempaque
      LET idx9 = arr_curr()
      IF repitecat(idx9) THEN 
         CALL msg("Caja repetida")
         NEXT FIELD idcatempaque
      END IF
      
    AFTER FIELD prestamo
      LET idx9 = arr_curr() 
      IF d_cajaemp[idx9].prestamo IS NOT NULL THEN 
         LET d_cajaemp[idx9].devolucion = NULL 
      END IF  

    AFTER FIELD devolucion
      LET idx9 = arr_curr() 
      IF d_cajaemp[idx9].devolucion IS NOT NULL THEN 
         LET d_cajaemp[idx9].prestamo = NULL 
      END IF   
    
  END INPUT 
  CLOSE WINDOW wCaja
END FUNCTION

FUNCTION repitecat(lid)
  DEFINE i, lid SMALLINT 
  
  FOR i = 1 TO d_cajaemp.getLength()
     IF i <> lid THEN
        IF d_cajaemp[i].idCatEmpaque = d_cajaemp[lid].idCatEmpaque THEN 
           RETURN TRUE
        END IF  
     END IF 
  END FOR 
  RETURN FALSE  
END FUNCTION 
