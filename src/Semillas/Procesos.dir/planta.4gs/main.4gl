SCHEMA db0000   

MAIN
  DEFINE msgTxt STRING 
  CLOSE WINDOW SCREEN
  OPEN WINDOW w1 WITH FORM "form01"
  MENU 
    ON ACTION boleta
       LET msgTxt = "Se selecciona de la lisa de recepciones abiertas, las que no tienen datos de exportación, automáticamente despliega Productor/Estructura y Producto"
       CALL msg(msgTxt)
    ON ACTION cliente
       LET msgTxt = "Lista de clientes de exportación"
       CALL msg(msgTxt)  
    ON ACTION items
       LET msgTxt = "Lista de items SAP, muestra todas las posibles opciones según el producto ingresado en la Recepción"
       CALL msg(msgTxt)  
    ON ACTION salir
       EXIT MENU 
  END MENU  

END MAIN 