GLOBALS "datexp_glob.4gl"

FUNCTION finput(accion)
  DEFINE accion CHAR (1)
  DEFINE msgTxt, sqlTxt STRING 
  DEFINE pesoFalta SMALLINT 

  IF accion = "I" OR accion = "B" THEN 
     CALL inicializatmp()
  ELSE 
     CALL llenatmpitems()  
     --SELECT SUM (pesoTotal) INTO lPesoTotal FROM tmpdet2
     --SELECT SUM (pesoTotal) INTO lPesoTotal FROM pemmbo 
     --LET lPesoSaldo = lPesoTotal
  END IF 
  LET insdet2 = 0
   DIALOG ATTRIBUTES(UNBUFFERED)
     
     INPUT gr_reg.idBoletaIng, gr_reg.estadoExportacion 
       FROM idboletaing, estadoexportacion ATTRIBUTES  (WITHOUT DEFAULTS)
        
        BEFORE INPUT 
           --CALL dialog.setactionactive("close",0)
           IF accion = "I" OR accion = "B" THEN 
              INITIALIZE gr_reg.* TO NULL
           END IF  
   
           CALL dialog.setActionHidden("close",1)

        AFTER FIELD idBoletaIng
          IF accion = "I" THEN
             IF gr_reg.idBoletaIng IS NULL THEN 
                CALL msg("Debe ingresar numero de boleta")
                GOTO opcionBusca
                --NEXT FIELD idBoletaIng
             END IF 
          END IF    
          
        ON CHANGE idBoletaIng
           
           CALL cleanAllDet()
           CALL llenatmp()
           --Que no sea nulo
           IF gr_reg.idBoletaing IS NULL THEN
              CALL msg("Debe ingresar orden de producci�n")
              NEXT FIELD idboletaing
           ELSE 
              IF accion = "I" THEN 
                 SELECT * FROM ordenes WHERE idOrden = gr_reg.idBoletaIng
                 IF sqlca.sqlcode = 100 THEN 
                    CALL msg("Boleta no existe o ya tiene informaci�n de exportaci�n")
                    NEXT FIELD idboletaing
                 END IF    
              END IF 
           END IF

           {CASE                
             WHEN accion = "I" LET lcondi = " estado = 1"
             WHEN accion = "B" LET lcondi = " estado IN (1,2)"
           END CASE }

           LET sqlTxt = " SELECT tipoboleta, idordpro, iddordpro, productor, ",
                        "        iditem, pesoTotal ",
                        " FROM pemmboleta WHERE idboletaing = ", gr_reg.idBoletaIng
                        --" AND ", lcondi 
           PREPARE ex_stmt FROM sqlTxt 
           EXECUTE ex_stmt INTO ltipo, lidordpro, liddordpro, lproductor, litem, 
                   lPesoTotal
           
           --Para datos de finca/productor
           IF ltipo = 1 THEN
              --Para estructura
              SELECT fincaNomCt || '-'||nomEstructura || '-'|| NVL(nomValvula,'') || '-' || desItemLg 
              INTO vEstructura
              FROM pemdordpro a, pemmfinca b, mestructura c, OUTER(pemMValvula d), glbMItems e 
              WHERE a.idfinca = b.idfinca 
              AND a.idestructura = c.idEstructura 
              AND a.idValvula = d.idValvula 
              AND a.idItem = e.idItem 
              AND a.idOrdPro = lidordpro 
              AND a.iddordpro = liddordpro

              DISPLAY vEstructura TO estructura
              --limpia productor
              LET vProductor = NULL 
              LET vItem = NULL 
              DISPLAY vProductor TO productor
              DISPLAY vItem TO producto

              --Para producto / item
              SELECT TRIM (nombre) || " " || a.desItemLg
              INTO vItem 
              FROM glbMItems a, pemdordpro c, glbmtip b, pemmboleta d
              WHERE d.idBoletaIng = gr_reg.idBoletaIng
              AND  d.idOrdPro = c.idOrdPro
              AND  d.idDOrdPro = c.idDOrdPro
              AND  c.idItem = a.idItem
              AND  a.idtipo = b.idtipo  
              DISPLAY vItem TO producto
           ELSE 
              --Para productor
              SELECT trim(nombre)
              INTO vProductor 
              FROM commemp 
              WHERE id_commemp = lproductor
              DISPLAY vProductor TO productor
              
              --Para producto / item
              SELECT TRIM (nombre) || " " || a.desItemLg
              INTO vItem 
              FROM glbMItems a, pemmboleta c, glbmtip b
              WHERE c.idBoletaIng = gr_reg.idBoletaIng
              AND a.idtipo = b.idtipo  
              --AND  c.idOrdPro = b.idOrdPro
              --AND  c.idDOrdPro = b.idDOrdPro
              AND  a.idItem = c.idItem
              DISPLAY vItem TO producto

              --Limpia estructura
              LET vEstructura = NULL 
              DISPLAY vEstructura TO estructura
           END IF 

           --Para pesos
           DISPLAY lPesoTotal TO fpesototal
           CALL calculaPesoSaldo()           
           DISPLAY lPesoSaldo TO fPesoSaldo

           --CALL dispcliente()

        ON ACTION buscar
          LABEL opcionBusca:
          CALL llenatmp()
          CALL picklist_2("Boletas", "Boleta","Descripci�n","idorden", "descripcion", "ordenes", "1=1", "1", 0)
            RETURNING gr_reg.idBoletaIng, vDescripcion, INT_FLAG
          --Que no sea nulo
           IF gr_reg.idBoletaing IS NULL THEN
              CALL msg("Debe ingresar orden de producci�n")
              NEXT FIELD idboletaing
           END IF

           LET sqlTxt = " SELECT tipoboleta, idordpro, iddordpro, productor, ",
                        " iditem, pesoTotal ",
                        " FROM pemmboleta WHERE idboletaing = ", gr_reg.idBoletaIng

           {CASE 
              WHEN accion = "I" 
                 LET sqlTxt = sqlTxt || " AND estado = 1 "
                 LET msgTxt = "Boleta no existe o ya tiene informaci�n de exportaci�n"
              WHEN accion = "B" 
                 LET sqlTxt = sqlTxt || " AND estado IN(1,2) "
                 LET msgTxt = "Error al seleccionar boleta"
           END CASE }
                        
           PREPARE ex_st1 FROM sqlTxt
           EXECUTE ex_st1 INTO ltipo, lidordpro, liddordpro, lproductor, 
                               litem, lPesoTotal 
           --Si boleta no existe
           IF sqlca.sqlcode = 100 THEN 
              CALL msg(msgTxt)
              NEXT FIELD idboletaing
           END IF 

           --Para datos de finca/productor
           IF ltipo = 1 THEN
              --Para estructura
              SELECT fincaNomCt || '-'||nomEstructura || '-'|| NVL(nomValvula,'') || '-' || desItemLg 
              INTO vEstructura
              FROM pemdordpro a, pemmfinca b, mestructura c, OUTER(pemMValvula d), glbMItems e 
              WHERE a.idfinca = b.idfinca 
              AND a.idestructura = c.idEstructura 
              AND a.idValvula = d.idValvula 
              AND a.idItem = e.idItem 
              AND a.idOrdPro = lidordpro 
              AND a.iddordpro = liddordpro

              DISPLAY vEstructura TO estructura
              --limpia productor
              LET vProductor = NULL 
              LET vItem = NULL 
              DISPLAY vProductor TO productor
              DISPLAY vItem TO producto

              --Para producto / item
              SELECT TRIM (nombre) || " " || a.desItemLg
              INTO vItem 
              FROM glbMItems a, pemdordpro c, glbmtip b, pemmboleta d
              WHERE d.idBoletaIng = gr_reg.idBoletaIng
              AND  d.idOrdPro = c.idOrdPro
              AND  d.idDOrdPro = c.idDOrdPro
              AND  c.idItem = a.idItem
              AND  a.idtipo = b.idtipo  
              DISPLAY vItem TO producto
           ELSE 
              --Para productor
              SELECT trim(nombre)
              INTO vProductor 
              FROM commemp 
              WHERE id_commemp = lproductor
              DISPLAY vProductor TO productor
              
              --Para producto / item
              SELECT TRIM (nombre) || " " || a.desItemLg
              INTO vItem 
              FROM glbMItems a, pemmboleta c, glbmtip b
              WHERE c.idBoletaIng = gr_reg.idBoletaIng
              AND a.idtipo = b.idtipo  
              --AND  c.idOrdPro = b.idOrdPro
              --AND  c.idDOrdPro = b.idDOrdPro
              AND  a.idItem = c.idItem
              DISPLAY vItem TO producto

              --Limpia estructura
              LET vEstructura = NULL 
              DISPLAY vEstructura TO estructura
           END IF 

           --Para pesos
           DISPLAY lPesoTotal TO fpesototal
           CALL calculaPesoSaldo() 
           DISPLAY lPesoSaldo TO fPesoSaldo

        AFTER INPUT 
           IF accion = "B" THEN
              RETURN 
           END IF
     END INPUT 
     
     
     INPUT ARRAY gr_cli FROM sDet2.* ATTRIBUTES (WITHOUT DEFAULTS )

       BEFORE INPUT 
         LABEL labelDet1:
         LET i = arr_curr()
         
       ON ACTION buscar
          LET idx2 = arr_curr()
          CALL picklist_2("Clientes", "ID Cliente","Nombre","idCliente", "nombre", "pemmcli", "tipo=1", "2", 0)
            RETURNING gr_cli[idx2].idcliente, gr_cli[idx2].nombre, INT_FLAG
            
       ON CHANGE idcliente 
         LET idx2 = arr_curr()
         
         --Cliente no repetido
         IF clirep(idx2) THEN
            CALL msg("Error: Cliente ya ingresado, ingrese de nuevo")
            NEXT FIELD idcliente 
         END IF 
         
         SELECT nombre INTO gr_cli[idx2].nombre FROM pemmcli WHERE idcliente = gr_cli[idx2].idcliente
         IF sqlca.sqlcode <> 0 THEN
            CALL msg("Error: ID cliente no exite, ingrese de nuevo")
            NEXT FIELD idcliente 
         END IF 
         
       AFTER ROW 
          LET idx2 = arr_curr()
        
          IF NOT cliExDet2(gr_cli[idx2].idcliente) AND gr_cli[idx2].nombre IS NOT NULL THEN  
             GOTO labelIS
          ELSE 
             CALL mostrarDet2(gr_cli[idx2].idcliente)
          END IF  
         
       BEFORE FIELD idcliente
         LET idx2 = arr_curr()
         IF idx2 > 1 THEN 
            IF (gr_cli[idx2].idcliente IS NULL ) AND --OR length(gr_cli[idx2].idcliente) < 1) AND
               (gr_cli[idx2-1].idcliente IS NULL) THEN -- OR length(gr_cli[idx2-1].idcliente)) THEN 
               CALL gr_cli.deleteElement(idx2)
            END IF 
         END IF 
         
       BEFORE ROW 
         LET idx2 = arr_curr()  
         IF gr_cli[idx2].idcliente IS NOT NULL THEN
            IF NOT cliExDet2(gr_cli[idx2].idcliente) THEN
               CALL cleandet2()  
               GOTO labelIS
            ELSE 
               CALL mostrarDet2(gr_cli[idx2].idcliente)
            END IF
         ELSE 
            CALL cleandet2()  
         END IF 
          
       --BEFORE INSERT 
         --LET idx2 = arr_curr() 
         --IF idx2 > 1 THEN 
            --IF gr_cli[idx-1].idcliente IS NULL THEN CANCEL INSERT END IF 
         --END IF 
         --
         
       ON CHANGE pesocli
         CALL calculaPesoSaldo()
         
     END INPUT 
     
     INPUT ARRAY gr_det FROM sDet.*

        BEFORE INPUT 
         LET insdet2 = 0
         --
         {CALL fitem(gr_reg.idBoletaIng) RETURNING litem
         DECLARE cur20 CURSOR FOR 
           SELECT lnkItem, iditemsap||"- "|| nombre|| " - "||peso||" Lbs"
             FROM itemsxgrupo
             WHERE categoria=1 AND iditem = litem
           LET i = 1  
           FOREACH cur20 INTO gr_det[i].lnkItem, gr_det[i].nombre
             LET i = i + 1
           END FOREACH }
         --
         
         LABEL labelIS:
           LET idx = gr_det.getLength()
           IF idx > 1 THEN
              FOR i = idx TO 1 STEP -1
                 IF i = 1 THEN EXIT FOR END IF 
                 IF gr_det[i].lnkItem IS NULL AND gr_det[i].cantcajas IS NULL THEN
                    CALL gr_det.deleteElement(i)
                 END IF  
              END FOR  
           END IF 
    
           NEXT FIELD lnkItem

       BEFORE FIELD lnkItem
         LET idx = arr_curr()
         LET gr_det[idx].idcliente = gr_cli[idx2].idcliente
         
       ON CHANGE lnkItem
         LET idx = arr_curr()
         --Existe ItemSAP?
         CALL fitem(gr_reg.idBoletaIng) RETURNING litem
         SELECT iditemsap||"- "|| nombre|| " - "||peso||" Lbs", peso 
         INTO gr_det[idx].nombre, gr_det[idx].peso FROM itemsxgrupo
           --WHERE lnkItem = gr_det[idx].lnkItem
           WHERE lnkItem = gr_det[idx].lnkItem
           AND categoria=1 AND iditem = litem
           
         IF sqlca.sqlcode <> 0 THEN
            CALL msg("Error: IDItemSAP no existe, ingrese de nuevo")
            NEXT FIELD lnkItem 
         END IF 
         IF validaLinea(idx) THEN 
            CALL msg("Item no puede repetirse en el mismo cliente")
            NEXT FIELD lnkItem
         END IF 

       ON CHANGE cantcajas
          LET idx = arr_curr()
          LET gr_det[idx].pesoTotal = 0
          
          IF gr_det[idx].cantcajas IS NOT NULL THEN
             SELECT peso INTO gr_det[idx].peso FROM itemsxgrupo 
             WHERE lnkItem=gr_det[idx].lnkItem
             --LET gr_det[idx].peso = gr_det[idx].cantcajas * gr_det[idx].peso 
             --DISPLAY gr_det[idx].peso TO sDet[scr_line()].peso
             LET gr_det[idx].pesoTotal = gr_det[idx].cantcajas * gr_det[idx].peso
             IF pesoExcede() THEN
                CALL msg("Suma de peso en exportaci�n es superior a peso en recepci�n, ingrese de nuevo")
                NEXT FIELD cantcajas
             END IF 
          END IF 

       AFTER FIELD cantCajas 
         IF gr_det[idx].cantcajas IS NULL THEN  
            CALL msg("Debe ingresar n�mero de cajas")
            NEXT FIELD cantCajas
         ELSE  
            LET idx = arr_curr()
             SELECT peso INTO gr_det[idx].peso FROM itemsxgrupo 
             WHERE lnkItem=gr_det[idx].lnkItem
             --LET gr_det[idx].peso = gr_det[idx].cantcajas * gr_det[idx].peso 
             --DISPLAY gr_det[idx].peso TO sDet[scr_line()].peso
             LET gr_det[idx].pesoTotal = gr_det[idx].cantcajas * gr_det[idx].peso
             IF pesoExcede() THEN
                CALL msg("Suma de peso en exportaci�n es superior a peso en recepci�n, ingrese de nuevo")
                NEXT FIELD cantcajas
             END IF 
          END IF 
         
       ON ACTION buscar
          LET idx = arr_curr()
          CALL fitem(gr_reg.idBoletaIng) RETURNING litem
          LET lcondi = "categoria=1 AND iditem =", litem
          CALL picklist_2("Items", "ID item SAP","Descripci�n","lnkItem", 'iditemsap||"- "|| nombre|| " - "||peso||" Lbs"', "itemsxgrupo", lcondi, "1", 0)
            RETURNING gr_det[idx].lnkItem, gr_det[idx].nombre, INT_FLAG
          IF validalinea(idx) THEN 
            CALL msg("Item no puede repetirse en el mismo cliente")
            NEXT FIELD lnkItem
         END IF
          SELECT peso INTO gr_det[idx].peso FROM itemsxgrupo WHERE lnkItem = gr_det[idx].lnkItem

       AFTER FIELD lnkItem
          LET idx = arr_curr()
          IF validalinea(idx) THEN 
            CALL msg("Item no puede repetirse en el mismo cliente")
            NEXT FIELD lnkItem
          END IF

       BEFORE FIELD cantcajas 
          LET idx = arr_curr()   
          IF gr_det[idx].lnkItem IS NULL AND gr_det[idx].cantcajas IS NULL THEN
             NEXT FIELD lnkItem 
          END IF 
         
       
       AFTER INSERT
         LET idx = arr_curr()
         IF gr_det[idx].lnkItem IS NULL AND gr_det[idx].cantcajas IS NULL THEN
            CANCEL INSERT 
         END IF 
         
       AFTER INPUT 
         CALL insertaDet2()
         LET insdet2 = 1
         
       
     END INPUT 
     
     ON ACTION ACCEPT
        IF gr_det[1].lnkItem IS NOT NULL AND gr_det[1].cantcajas IS NOT NULL THEN 
           CALL insertaDet2()
           LET insdet2 = 1
        END IF    
        IF gr_reg.idBoletaIng IS NULL THEN
           CALL msg("Debe seleccionar boleta")
           GOTO opcionBusca
           --NEXT FIELD idBoletaIng  
        END IF
          CALL llenatmp()
          LET sqlTxt = " SELECT tipoboleta, idordpro, iddordpro, productor, ",
                        "        iditem, pesoTotal ",
                        " FROM pemmboleta WHERE idboletaing = ", gr_reg.idBoletaIng
                        --" AND ", lcondi 
           PREPARE ex_stmt3 FROM sqlTxt 
           EXECUTE ex_stmt3 INTO ltipo, lidordpro, liddordpro, lproductor, litem, 
                   lPesoTotal
           --Para datos de finca/productor
           IF ltipo = 1 THEN
              --Para estructura
              SELECT fincaNomCt || '-'||nomEstructura || '-'|| NVL(nomValvula,'') || '-' || desItemLg 
              INTO vEstructura
              FROM pemdordpro a, pemmfinca b, mestructura c, OUTER(pemMValvula d), glbMItems e 
              WHERE a.idfinca = b.idfinca 
              AND a.idestructura = c.idEstructura 
              AND a.idValvula = d.idValvula 
              AND a.idItem = e.idItem 
              AND a.idOrdPro = lidordpro 
              AND a.iddordpro = liddordpro

              DISPLAY vEstructura TO estructura
              --limpia productor
              LET vProductor = NULL 
              LET vItem = NULL 
              DISPLAY vProductor TO productor
              DISPLAY vItem TO producto

              --Para producto / item
              SELECT TRIM (nombre) || " " || a.desItemLg
              INTO vItem 
              FROM glbMItems a, pemdordpro c, glbmtip b, pemmboleta d
              WHERE d.idBoletaIng = gr_reg.idBoletaIng
              AND  d.idOrdPro = c.idOrdPro
              AND  d.idDOrdPro = c.idDOrdPro
              AND  c.idItem = a.idItem
              AND  a.idtipo = b.idtipo  
              DISPLAY vItem TO producto
           ELSE 
              --Para productor
              SELECT trim(nombre)
              INTO vProductor 
              FROM commemp 
              WHERE id_commemp = lproductor
              DISPLAY vProductor TO productor
              
              --Para producto / item
              SELECT TRIM (nombre) || " " || a.desItemLg
              INTO vItem 
              FROM glbMItems a, pemmboleta c, glbmtip b
              WHERE c.idBoletaIng = gr_reg.idBoletaIng
              AND a.idtipo = b.idtipo  
              --AND  c.idOrdPro = b.idOrdPro
              --AND  c.idDOrdPro = b.idDOrdPro
              AND  a.idItem = c.idItem
              DISPLAY vItem TO producto

              --Limpia estructura
              LET vEstructura = NULL 
              DISPLAY vEstructura TO estructura
           END IF 
        IF accion = "B" THEN
           RETURN 
        END IF
        IF insdet2 = 0 THEN
           GOTO labeldet1 
        END IF 
        IF insdet2 = 1 THEN 
           IF idx = 0 THEN LET idx = 1 END IF 
           IF gr_det[idx].lnkItem IS NOT NULL AND gr_det[idx].cantcajas IS NULL THEN  
              CALL msg("Debe ingresar n�mero de cajas")
              NEXT FIELD cantCajas
           END IF
        END IF   
        --IF accion = "I" THEN  
           IF insdet2 = 0 THEN
              LET idx2 = arr_curr()
              CALL insertaDet2() 
           END IF
        --END IF    
        IF gr_reg.idBoletaIng IS NULL THEN
           CALL msg("Debe ingresar n�mero de Recepci�n")
           NEXT FIELD idBoletaIng
        END IF
        --IF gr_reg.cliente IS NULL THEN
           --CALL msg("Debe seleccionar Cliente")
           --NEXT FIELD cmbcliente
        --END IF
        EXIT DIALOG 

    {          CASE box_gradato("Seguro de grabar")
         WHEN "Si"
            LET resultado = TRUE
            EXIT DIALOG
         WHEN "No"
            EXIT DIALOG 
         OTHERWISE
            CONTINUE DIALOG 
      END CASE 
      LET resultado = TRUE
      EXIT DIALOG }
      
     ON ACTION CANCEL
        CLEAR FORM 
        LET gr_reg.idBoletaIng = NULL
        --LET gr_reg.cliente  = NULL 
        RETURN --EXIT dialog
   END DIALOG
   --IF INT_FLAG THEN 
      --RETURN 
   --END IF 

   DISPLAY "insert en encabezado"
   DISPLAY "Boleta exp", 0
   DISPLAY "Boleta ing", gr_reg.idBoletaIng
   --DISPLAY "Cliente ", gr_reg.cliente
   DISPLAY "pesototal ", vpesototal
   DISPLAY "calidadproduccion ", 1
   DISPLAY "fectran ", CURRENT 
   DISPLAY "usuopero", "Carlos"
   BEGIN WORK 
     --insertando inforamcion de la boleta de exportacion
     IF accion = "I" THEN 
        INSERT INTO pemmboletaexp (idboletaexp, idboletaing, pesototal,
                                   calidadproduccion, fectran, usuopero)
          VALUES (0, gr_reg.idBoletaIng, vpeso, 1,
                  CURRENT, 1)
     ELSE 
        IF accion = "U" THEN 
           UPDATE pemmboletaexp SET (idboletaing, pesototal,
                                      calidadproduccion, fectran, usuopero) =
                  (gr_reg.idBoletaIng, vpeso, 1, CURRENT, 1)
            WHERE idboletaexp = gr_reg.idBoletaExp
        END IF 
     END IF  
                  
   DISPLAY "insert en detalle de clientes"
   IF sqlca.sqlcode = 0 THEN 
      IF accion = "I" THEN 
         LET gr_reg.idBoletaExp = sqlca.sqlerrd[2]
      ELSE 
         DELETE FROM pemdboletaexp WHERE idboletaexp = gr_reg.idBoletaExp
         DELETE FROM pemcboletaexp WHERE idboletaexp = gr_reg.idBoletaExp
      END IF 
         
      --Insertando en clientes
      FOR i = 1 TO gr_cli.getLength()
        IF gr_cli[i].idcliente IS NOT NULL THEN 
          INSERT INTO pemCBoletaExp (idBoletaExp, idCliente, peso)
            VALUES (gr_reg.idBoletaExp, gr_cli[i].idcliente, gr_cli[i].pesocli)
          --insertando items x cliente
          IF sqlca.sqlcode = 0 THEN
            DECLARE cur04 CURSOR FOR 
              SELECT lnkItem, cantCajas, peso, pesoTotal
                FROM tmpdet2
               WHERE idCliente = gr_cli[i].idcliente
            LET j = 1
            FOREACH cur04 INTO gr_det[j].lnkItem, gr_det[j].cantcajas, 
              gr_det[j].peso, gr_det[j].pesoTotal
              IF gr_det[j].lnkItem IS NOT NULL AND gr_det[j].cantcajas IS NOT NULL THEN 
                INSERT INTO pemDBoletaExp (lnkItem, idBoletaExp, idCliente, 
                  cantCajas, peso, pesoTotal)
                  VALUES (gr_det[j].lnkItem, gr_reg.idBoletaExp, gr_cli[i].idcliente,
                    gr_det[j].cantcajas, gr_det[j].peso, gr_det[j].pesoTotal)
                LET j = j + 1
              END IF   
            END FOREACH 
          END IF 
        END IF 
      END FOR 
    END IF   

      IF sqlca.sqlcode = 0 THEN
         DISPLAY "actualizando estado de la boleta"
         --DISPLAY "update pemmboleta set estado = 2 where idboletaing = ", gr_reg.idBoletaIng
         UPDATE pemmboleta SET estado = 2, estadoExportacion = gr_reg.estadoExportacion WHERE idboletaing = gr_reg.idBoletaIng
         IF sqlca.sqlcode = 0 THEN
            CALL msg("Informacion grabada")
            --ROLLBACK WORK
            COMMIT WORK
         ELSE 
            CALL msg("Error al grabar informaci�n 3")
            ROLLBACK WORK 
         END IF  
      ELSE 
         CALL msg("Error al grabar informaci�n 2")
         ROLLBACK WORK 
      END IF 
   
END FUNCTION 

####### Encabezado ##############

FUNCTION creatmp()

    CREATE TEMP TABLE tmpdet2
  (idcliente    INTEGER, 
   lnkItem      INTEGER ,
   cantcajas    SMALLINT , 
   peso         SMALLINT,
   pesoTotal    SMALLINT,
   PRIMARY KEY (idcliente, lnkitem) )


  CREATE TEMP TABLE ordenes
  (
  idorden   INTEGER ,
  descripcion   VARCHAR (100)
  )
END FUNCTION 

FUNCTION inicializatmp()
  DROP TABLE tmpdet2
  CREATE TEMP TABLE tmpdet2
  (idcliente    INTEGER, 
   lnkItem      INTEGER ,
   cantcajas    SMALLINT , 
   peso         SMALLINT,
   pesoTotal    SMALLINT,
   PRIMARY KEY (idcliente, lnkitem)  )

   FOR i = 1 TO gr_cli.getLength()
      INITIALIZE gr_cli[i].* TO NULL
   END FOR 
   FOR i = 1 TO gr_det.getLength()
      INITIALIZE gr_det[i].* TO NULL
   END FOR
   CLEAR FORM 
END FUNCTION

FUNCTION llenatmp()
  DEFINE laccion CHAR (1) 
  DEFINE sqlTxt STRING 
  DEFINE vBol, vordpro, vdordpro, vproductor, vitem, vlPeso1, vlPeso2 INTEGER
  DEFINE vestructura, cproductor, citem, vdes VARCHAR (100)
  DEFINE pesoSaldo SMALLINT  

  DELETE FROM ordenes WHERE 1=1
  --Para boletas locales
  LET sqlTxt = " SELECT idboletaing, idordpro, iddordpro ",
               " FROM pemmboleta ",
               " WHERE tipoBoleta = 1 ",
               " AND estado IN (1,2,3) ",
               " AND estadoExportacion is null "


  PREPARE ex_bolLoc FROM sqlTxt  
  DECLARE cur01 CURSOR FOR ex_bolLoc

    FOREACH cur01 INTO vBol, vordpro, vdordpro
      SELECT fincaNomCt || '-'||nomEstructura || '-' || NVL(nomValvula,'') || '-' || desItemLg
        INTO vestructura
        FROM pemdordpro a, pemmfinca b, mestructura c, OUTER(pemMValvula d), glbMItems e 
       WHERE a.idfinca = b.idfinca 
         AND a.idestructura = c.idEstructura 
         AND a.idValvula = d.idValvula 
         AND a.idItem = e.idItem 
         AND a.idOrdPro = vordpro 
         AND a.iddordpro = vdordpro
         INSERT INTO ordenes VALUES (vBol, vestructura)
    END FOREACH 

    --Para productor
    LET sqlTxt = " SELECT idboletaing, productor, iditem ",
               " FROM pemmboleta ",
               " WHERE tipoBoleta = 2 ",
               " AND estado IN (1,2,3) ",
               " AND estadoExportacion is null "
               
  PREPARE ex_bolProd FROM sqlTxt
    DECLARE cur02 CURSOR FOR ex_bolProd

    FOREACH cur02 INTO vBol, vproductor, vitem
      SELECT trim(nombre)
        INTO cProductor 
        FROM commemp 
       WHERE id_commemp = vproductor
              --DISPLAY vProductor TO productor
    
      --Para producto / item
      SELECT desItemLg
        INTO cItem 
        FROM glbMItems
       WHERE idItem = vitem
              --DISPLAY vItem TO producto
      LET vdes = cproductor CLIPPED, "-",citem
      INSERT INTO ordenes VALUES (vBol, vdes)
    END FOREACH 

    --Borrando las boletas que tengan ingreso en exportacion y ya no tengan saldo
    DECLARE cur03 CURSOR FOR SELECT idOrden FROM ordenes
    FOREACH cur03 INTO vBol
      SELECT FIRST 1 idBoletaing FROM pemmboletaexp WHERE idboletaing = vBol
      IF sqlca.sqlcode = 0 THEN
         SELECT SUM (pesoTotal) INTO vlPeso1
         FROM pemmboletaexp
         WHERE idboletaing = vBol
         GROUP BY idBoletaIng
         SELECT a.pesoTotal INTO vlPeso2 
         FROM pemmboleta a
         WHERE a.idboletaing = vBol
         LET pesoSaldo = vlPeso2 - vlPeso1
         IF pesoSaldo <= 0 THEN
            DELETE FROM ordenes WHERE idOrden = vBol
            IF sqlca.sqlcode = 0 THEN
               DISPLAY "borre ", vBol 
            END IF  
         END IF  
      END IF 
    END FOREACH  
END FUNCTION 

{FUNCTION calculaPesoSaldo()
DEFINE i SMALLINT 

  LET lSumPeso = 0
  SELECT SUM (pesoTotal) INTO lSumPeso FROM pemmboletaexp
  WHERE idboletaing = gr_reg.idBoletaIng
  GROUP BY idboletaing 
  IF lSumPeso > 0 THEN
  ELSE 
     LET lSumPeso = 0
  END IF 
  
  FOR i = 1 TO gr_cli.getLength()
    IF gr_det[i].peso IS NOT NULL THEN
       LET lSumPeso = lSumPeso +  gr_cli[idx2].pesocli
    END IF 
  END FOR 
  LET lPesoSaldo = lPesoTotal - lSumPeso
  
END FUNCTION}

FUNCTION calculaPesoSaldo() 
DEFINE pesoExportacion, pesoLocal LIKE pemmboleta.pesototal
  LET lSumPeso = 0
  --Peso total de ingreso en lPesoTotal
  --Para peso de exportacion
  SELECT SUM (pesototal) INTO pesoExportacion FROM pemmboletaexp
  WHERE idboletaing = gr_reg.idBoletaIng
  GROUP BY idboletaing

  --Para peso Local (Carreta, Cenma y Devolucion)
  SELECT SUM (pesocli) INTO pesoLocal FROM pemmboletaloc
  WHERE idboletaing = gr_reg.idBoletaIng 
  GROUP BY idboletaing

  LET lSumPeso = pesoExportacion + pesoLocal
  {SELECT SUM (pesoTotal) INTO lSumPeso FROM pemmboletaExp
  WHERE idboletaing = gr_reg.idBoletaIng
  GROUP BY idboletaing }
  IF lSumPeso > 0 THEN
     LET lPesoSaldo = lPesoTotal - lSumPeso
  ELSE 
     LET lPesoSaldo = lPesoTotal - lSumPeso
  END IF 
 
END FUNCTION

####### Detalle 1 #############


FUNCTION clirep(nline)
DEFINE nline SMALLINT 

  FOR i = 1 TO gr_cli.getLength()
    IF i <> nline THEN
     IF gr_cli[i].idcliente = gr_cli[nline].idcliente THEN  
       RETURN TRUE 
     END IF 
    END IF 
  END FOR 
  RETURN FALSE 
END FUNCTION 

FUNCTION cliExDet2(lcli)
DEFINE lcli INTEGER 

  SELECT FIRST 1 idcliente FROM tmpdet2
  WHERE idcliente = lcli
     IF sqlca.sqlcode = 0 THEN 
        RETURN TRUE  
     ELSE 
        RETURN FALSE 
     END IF 

END FUNCTION 

FUNCTION cleandet1()
  FOR i = 1 TO gr_cli.getLength()
      INITIALIZE gr_cli[i].* TO NULL
   END FOR 
   FOR i = gr_cli.getLength() TO 1 STEP -1
     CALL gr_cli.deleteElement(i)
   END FOR 
   
END FUNCTION

##### Detalle 2 ##########

FUNCTION mostrarDet2(lcli)
DEFINE lcli, i INTEGER 

  CALL cleandet2()
  SELECT COUNT (*) INTO j FROM tmpdet2 WHERE idcliente = lcli
  DISPLAY "en tmp van ", j
  DECLARE cur003 CURSOR FOR 
    SELECT idcliente, lnkItem, cantCajas, peso, pesoTotal
    FROM tmpdet2
    WHERE idcliente = lcli
  LET i = 1
  FOREACH cur003 INTO gr_det[i].idcliente, gr_det[i].lnkItem, 
    gr_det[i].cantcajas, gr_det[i].peso, gr_det[i].pesoTotal
    SELECT nombre INTO gr_det[i].nombre FROM itemsxgrupo
           WHERE lnkItem = gr_det[i].lnkItem
    LET i = i + 1
    DISPLAY "pase foreach ", i
  END FOREACH
  DISPLAY "arreglo tiene lineas ", gr_det.getLength()
  DISPLAY ARRAY gr_det TO sDet.*
    BEFORE DISPLAY 
      EXIT DISPLAY 
  END DISPLAY  
END FUNCTION 

FUNCTION fitem(numBol)
DEFINE numbol, numitem, tipBol INTEGER 

SELECT tipoboleta INTO tipBol FROM pemmboleta WHERE idboletaing = numbol
IF tipBol = 1 THEN 
  SELECT a.iditem 
  INTO numitem 
  FROM pemdordpro a, pemmboleta b
  WHERE a.idordpro = b.idordpro
  AND a.iddordpro = b.iddordpro
  AND b.idboletaing = numBol
ELSE 
  SELECT iditem INTO numitem FROM pemmboleta WHERE idboletaing = numBol
END IF 

RETURN numitem
END FUNCTION 

FUNCTION cleandet2()
  FOR i = 1 TO gr_det.getLength()
      INITIALIZE gr_det[i].* TO NULL
   END FOR 
   FOR i = gr_det.getLength() TO 1 STEP -1
     CALL gr_det.deleteElement(i)
   END FOR 
   
END FUNCTION 

FUNCTION insertaDet2()
  DELETE FROM tmpdet2 WHERE idcliente = gr_cli[idx2].idcliente
  FOR i = 1 TO gr_det.getLength() - 1
     IF gr_det[i].lnkItem IS NOT NULL AND gr_det[i].cantcajas IS NOT NULL THEN
    DISPLAY  "cliente ", gr_cli[idx2].idcliente
    DISPLAY  "item ", gr_det[i].lnkItem
        INSERT INTO tmpdet2(idcliente, lnkItem, cantcajas, peso, pesoTotal)
          VALUES (gr_cli[idx2].idcliente, gr_det[i].lnkItem, 
          gr_det[i].cantcajas, gr_det[i].peso, gr_det[i].pesoTotal)
     END IF      
  END FOR 
  SELECT SUM (pesoTotal) INTO gr_cli[idx2].pesocli FROM tmpdet2 WHERE idcliente = gr_cli[idx2].idcliente
  LET vpeso = 0 
  FOR i = 1 TO gr_cli.getLength()
     IF gr_cli[i].pesocli IS NOT NULL THEN 
        LET vpeso = vpeso + gr_cli[i].pesocli
     END IF 
  END FOR 
  LET lpesosaldo = lpesototal - vpeso
  
  DISPLAY lpesosaldo TO fpesosaldo 
END FUNCTION 

FUNCTION cleanAllDet()
  CALL cleanDet2()
  CALL cleanDet1()
END FUNCTION 

FUNCTION validaLinea(idx)
DEFINE i, idx SMALLINT 

  FOR i = 1 TO gr_det.getLength()
    IF i <> idx THEN
       IF gr_det[i].lnkItem = gr_det[idx].lnkitem THEN 
          RETURN TRUE  
       END IF 
    END IF 
  END FOR 
  
  RETURN FALSE  
END FUNCTION

FUNCTION pesoExcede()
DEFINE pesoActual, i, npeso, npeso2 SMALLINT 

  LET pesoActual = 0 
  SELECT SUM (pesoTotal) INTO pesoActual FROM tmpdet2
  IF pesoActual IS NULL OR pesoActual = 0 THEN
     FOR i = 1 TO gr_det.getLength()
         IF gr_det[i].pesoTotal IS NOT NULL THEN
            IF i = 1 THEN 
               LET pesoActual = gr_det[i].pesoTotal 
            ELSE 
               LET pesoActual = pesoActual + gr_det[i].pesoTotal
            END IF 
             
         END IF 
     END FOR 
  END IF 
  SELECT SUM (pesoTotal) INTO nPeso FROM pemmboletaexp 
  WHERE idBoletaing = gr_reg.idboletaing
  IF nPeso > 0 THEN 
     LET npeso2 = lPesototal - npeso
  ELSE    
     LET npeso2 = lPesototal
  END IF 
  DISPLAY "actual ", pesoActual, " total ", lpesoTotal, "npeso", npeso2
  IF pesoActual > npeso2 THEN 
     
     RETURN TRUE 
  ELSE
     RETURN FALSE   
  END IF 
  
END FUNCTION 

FUNCTION llenatmpitems()
DEFINE lcliente, litem  INTEGER 
DEFINE lcajas, lpeso, lpesototal SMALLINT 

  DECLARE cur10 CURSOR FOR
    SELECT idCliente, lnkItem, cantCajas, peso, pesoTotal
    FROM pemDBoletaExp 
    WHERE idBoletaExp = gr_reg.idBoletaExp

  FOREACH cur10 INTO lcliente, litem, lcajas, lpeso, lpesototal
    INSERT INTO tmpdet2 (idCliente, lnkItem, cantCajas, peso, pesoTotal)
      VALUES (lcliente, litem, lcajas, lpeso, lpesototal)
  END FOREACH 

END FUNCTION 