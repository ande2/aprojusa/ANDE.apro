SCHEMA db0000  
GLOBALS
  DEFINE gr_reg RECORD
    idBoletaIng     INTEGER,
    --cliente         INTEGER,
    idBoletaExp     LIKE pemMBoletaExp.idBoletaExp
    --idLinea     LIKE pemMBoletaExp.idLinea,
    --productorEstructura LIKE pemMBoletaExp.productorEstructura,
    --producto    LIKE pemMBoletaExp.producto,
    --cliente     LIKE pemMBoletaExp.cliente,
    --fectran     LIKE pemMBoletaExp.fectran,
    --usuOpero    LIKE pemMBoletaExp.usuOpero
  END RECORD

  DEFINE gr_cli DYNAMIC ARRAY OF RECORD
    idboletaexp LIKE pemcboletaexp.idboletaexp,
    idcliente   LIKE pemcboletaexp.idcliente,
    nombre      LIKE pemmcli.nombre,
    pesocli     SMALLINT 
  END RECORD 

  DEFINE gr_det DYNAMIC ARRAY OF RECORD
   idboletaexp    LIKE pemdboletaexp.idboletaexp, 
   idcliente      LIKE pemdboletaexp.idcliente,
   iditemsap      LIKE pemdboletaexp.iditemsap,
   nombre         LIKE itemsxgrupo.nombre,
   cantcajas      LIKE pemdboletaexp.cantcajas,
   peso           SMALLINT 
  END RECORD 

  
  
  DEFINE resultado BOOLEAN
  DEFINE ltipo, i, j, idx, idx2, det2, insdet2 SMALLINT 
  DEFINE lidordpro, liddordpro, lproductor, litem, vpesototal INTEGER 
  DEFINE vEstructura, vProductor, vItem VARCHAR (100) 
  DEFINE vdescripcion, lcondi VARCHAR (100)
  DEFINE lPesoCaja, lPesoTotal, lSumPeso, lPesoSaldo, vpeso LIKE pemmboleta.pesototal
END GLOBALS
