################################################################################
# Funcion     : %M%
# Descripcion : Modulo Principal
# Funciones   : main_init(dbname)
#               main_menu()                               
#               dummy() 
#               
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo  
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################
GLOBALS "datexp_glob.4gl"
MAIN
	DEFINE 
		n_param 	SMALLINT,
		prog_name2 	STRING

    DEFINE f ui.Form
		
	DEFER INTERRUPT

	OPTIONS
		INPUT  WRAP,
		HELP KEY CONTROL-W,
		COMMENT LINE OFF,
		PROMPT LINE LAST - 2,
		MESSAGE LINE LAST - 1,
		ERROR LINE LAST

	LET n_param = num_args()

	--IF n_param = 0 THEN
		--RETURN
	--ELSE
        --LET vDbname = arg_val(2)
        --DISPLAY "vDbname ", vDbname
		--CONNECT TO vDbname
        CONNECT TO "db0001"
	--END IF

	LET prog_name2 = "datosexp.log"   -- El progr_name es definido como constante en el arch. globals
    
	CALL STARTLOG(prog_name2)
	CALL main_init()
END MAIN

FUNCTION main_init()
DEFINE 
	nom_forma 	STRING,
    w           ui.Window,
    f           ui.Form
	  
	--INITIALIZE u_reg.* TO NULL
	--LET g_reg.* = u_reg.*

    CALL ui.Interface.loadActionDefaults("actiondefaults")
    CALL ui.Interface.loadStyles("styles_sc")

    CLOSE WINDOW SCREEN
    OPEN WINDOW w1 WITH FORM "datexp_form1"

    CALL fgl_settitle("ANDE - Boleta de Ingreso de Fruta de Exportación")
    LET w = ui.WINDOW.getcurrent()
	LET f = w.getForm()

    --CALL insert_init()
    --CALL update_init()
    --CALL delete_init()
    CALL creatmp()
    CALL combo_init()
	CALL main_menu()

END FUNCTION 

FUNCTION main_menu()   

    DISPLAY "REGISTRO DE INGRESO DE FRUTA DE EXPORTACIÓN" TO gtit_enc

    MENU "Exportación"
      BEFORE MENU 
         CALL DIALOG.setActionActive("modificar",   FALSE)
         CALL DIALOG.setActionActive("siguiente",   FALSE)
         CALL DIALOG.setActionActive("anterior",   FALSE)
         
      ON ACTION agregar
         CALL finput("I")

      ON ACTION modificar
         CALL finput("U")

      ON ACTION buscar
         IF fbusca() THEN
            CALL DIALOG.setActionActive("modificar",   TRUE )
            IF cuantos > 1 THEN
               CALL DIALOG.setActionActive("siguiente",   TRUE )
               CALL DIALOG.setActionActive("anterior",   TRUE ) 
            END IF 
         ELSE   
            CALL DIALOG.setActionActive("modificar",   FALSE) 
            CALL DIALOG.setActionActive("siguiente",   TRUE )
            CALL DIALOG.setActionActive("anterior",   TRUE )
         END IF 

      ON ACTION siguiente
         CALL sigAnt(1)
         
         
      ON ACTION anterior 
         CALL sigAnt(-1)  
      ON ACTION salir
         EXIT MENU  
         
    END MENU 

END FUNCTION

FUNCTION combo_init()
   CALL combo_din2("cmbcliente", "SELECT idcliente, nombre FROM pemmcli WHERE tipo = 1 ")
END FUNCTION 