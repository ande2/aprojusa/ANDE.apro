GLOBALS "datexp_glob.4gl"


FUNCTION fbusca()

  CALL finput("B")
  LET cuantos = 0 
  SELECT COUNT (*) INTO cuantos FROM pemmboletaexp WHERE idBoletaIng = gr_reg.idBoletaIng
  IF cuantos > 0 THEN 
     DECLARE curBusca SCROLL CURSOR FOR 
       SELECT idBoletaExp
      --INTO gr_reg.idBoletaExp
        FROM pemmboletaexp
        WHERE idBoletaIng = gr_reg.idBoletaIng
  ELSE 
     RETURN FALSE   
  END IF 
  OPEN curBusca
  FETCH FIRST curBusca INTO gr_reg.idBoletaExp   

  CALL dispcliente()
  SELECT pesoTotal, estadoExportacion 
  INTO lPesoTotal, lEstadoExportacion 
  FROM pemmBoleta WHERE idBoletaIng = gr_reg.idBoletaIng

  {SELECT pesoTotal INTO lSumPeso   FROM pemmBoletaExp WHERE idBoletaExp = gr_reg.idBoletaExp
  LET lPesoSaldo = lPesoTotal - lSumPeso}

  CALL calculaPesoSaldo()

  DISPLAY lPesoTotal TO fPesoTotal
  DISPLAY lPesoSaldo TO fPesoSaldo
  DISPLAY lEstadoExportacion TO estadoexportacion
  RETURN TRUE 
  
END FUNCTION 

FUNCTION sigAnt(fcur)
DEFINE fcur SMALLINT 
  CALL cleanAllDet()
  FETCH RELATIVE fcur curBusca INTO gr_reg.idBoletaExp 
         CALL dispcliente()
         SELECT pesoTotal, estadoExportacion INTO lPesoTotal, lEstadoExportacion 
         FROM pemmBoleta WHERE idBoletaIng = gr_reg.idBoletaIng
         SELECT pesoTotal INTO lSumPeso   FROM pemmBoletaExp WHERE idBoletaExp = gr_reg.idBoletaExp
         LET lPesoSaldo = lPesoTotal - lSumPeso 
         DISPLAY lPesoSaldo TO fPesoSaldo
         DISPLAY lEstadoExportacion TO estadoexportacion
END FUNCTION 

FUNCTION dispcliente()
DEFINE idx4 SMALLINT 

DECLARE cur05 CURSOR FOR 
  SELECT idBoletaExp, idCliente, peso
  FROM pemCboletaExp
  WHERE idBoletaExp = gr_reg.idBoletaExp

  LET idx4 = 1
  FOREACH cur05 INTO gr_cli[idx4].idboletaexp, gr_cli[idx4].idcliente, 
          gr_cli[idx4].pesocli
    SELECT nombre INTO gr_cli[idx4].nombre FROM pemMcli 
    WHERE idCliente = gr_cli[idx4].idcliente
    IF idx4 = 1 THEN 
       CALL mostrarItem(gr_cli[idx4].idboletaexp, gr_cli[idx4].idcliente)
    END IF 
    LET idx4 = idx4 + 1
  END FOREACH 
  DISPLAY ARRAY gr_cli TO sDet2.*
    BEFORE DISPLAY 
      LET idx2 = idx4 - 1
      EXIT DISPLAY 
  END DISPLAY 
END FUNCTION

FUNCTION mostrarItem(lbol,lcli)
DEFINE idx6 SMALLINT
DEFINE lbol, lcli, i INTEGER 

  CALL cleandet2()
  DECLARE cur06 CURSOR FOR 
    SELECT lnkItem, cantCajas, peso, pesoTotal 
    FROM pemDBoletaExp
    WHERE idBoletaExp = lbol
    AND idCliente = lcli
    
  LET i = 1
  FOREACH cur06 INTO gr_det[i].lnkItem, gr_det[i].cantcajas, gr_det[i].peso, 
          gr_det[i].pesoTotal
    SELECT nombre INTO gr_det[i].nombre FROM itemsxgrupo
           WHERE lnkItem = gr_det[i].lnkItem
    LET i = i + 1
  END FOREACH
  DISPLAY ARRAY gr_det TO sDet.*
    BEFORE DISPLAY 
      EXIT DISPLAY 
  END DISPLAY  

END FUNCTION 

