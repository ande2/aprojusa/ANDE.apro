GLOBALS "datloc_glob.4gl"


FUNCTION fbusca()

  CALL finput("B")
  IF gr_reg.idBoletaIng IS NULL THEN
     CALL msg("Procedimiento cancelado")
     RETURN FALSE  
  END IF 
  LET sqlTxt = " SELECT idBoletaLoc, tipoCaja, tieneTarima, pesoCli ",
               " FROM pemmboletaloc ",
               " WHERE idBoletaIng = ", gr_reg.idBoletaIng,
               " AND idCliente = ",
               " (SELECT idCliente FROM pemmcli "
  CASE 
      WHEN gProcesa = 1 --La Carreta 
        LET sqlTxt = sqlTxt || 
            " WHERE nombre MATCHES '*CARRETA*')" 
      WHEN gProcesa = 2 --CENMA 
        LET sqlTxt = sqlTxt || 
            " WHERE nombre MATCHES '*CENMA*')"
      WHEN gProcesa = 3 --Devolución 
        LET sqlTxt = sqlTxt || 
            " WHERE nombre MATCHES '*DEVOLUC*')"  
    END CASE
    DISPLAY "sqlTxt ", sqlTxt 
  PREPARE ex_st1 FROM sqlTxt
  EXECUTE ex_st1 INTO gr_reg.idBoletaLoc, gr_reg.tipoCaja, gr_reg.tieneTarima,
                      gr_reg.pesoCli   
  DISPLAY "gr_reg.idboletaloc ", gr_reg.idBoletaLoc
  IF sqlca.sqlcode = 100 THEN
     RETURN FALSE  
  END IF 
  DISPLAY BY NAME gr_reg.tipoCaja, gr_reg.tieneTarima
  DISPLAY gr_reg.pesoCli TO fpesototal1
  --CALL dispcliente()
  --SELECT pesoTotal INTO lPesoTotal FROM pemmBoleta WHERE idBoletaIng = gr_reg.idBoletaIng
  --SELECT pesoTotal INTO lSumPeso   FROM pemmBoletaExp WHERE idBoletaExp = gr_reg.idBoletaExp
  --LET lPesoSaldo = lPesoTotal - lSumPeso 
  --DISPLAY lPesoSaldo TO fPesoSaldo}
  CALL mostrarItem()
  RETURN TRUE 
  
END FUNCTION 

{FUNCTION dispcliente()
DEFINE idx4 SMALLINT 

DECLARE cur05 CURSOR FOR 
  SELECT idBoletaLoc, idCliente, pesoCli
  FROM pemCboletaLoc
  WHERE idBoletaLoc = gr_reg.idBoletaLoc

  LET idx4 = 1
  FOREACH cur05 INTO gr_cli[idx4].idboletaexp, gr_cli[idx4].idcliente, 
          gr_cli[idx4].pesocli
    SELECT nombre INTO gr_cli[idx4].nombre FROM pemMcli 
    WHERE idCliente = gr_cli[idx4].idcliente
    IF idx4 = 1 THEN 
       CALL mostrarItem(gr_cli[idx4].idboletaexp, gr_cli[idx4].idcliente)
    END IF 
    LET idx4 = idx4 + 1
  END FOREACH 
  DISPLAY ARRAY gr_cli TO sDet2.*
    BEFORE DISPLAY 
      EXIT DISPLAY 
  END DISPLAY 
END FUNCTION}

FUNCTION mostrarItem()  --lbol,lcli)
DEFINE idx6 SMALLINT
DEFINE lbol, lcli, i INTEGER 

  CALL cleandet2()
  DECLARE cur06 CURSOR FOR 
    SELECT idLinea, lnkItem, cantCajas, pesoBruto, pesoTara, pesoNeto, calidad 
    FROM pemDBoletaLoc
    WHERE idBoletaLoc = gr_reg.idBoletaLoc
    --AND idCliente = lcli
    
  LET i = 1
  FOREACH cur06 INTO gr_det[i].idlinea, gr_det[i].lnkItem, gr_det[i].cantcajas, 
                     gr_det[i].pesoBruto, gr_det[i].pesoTara, gr_det[i].pesoNeto,
                     gr_det[i].calidad
    SELECT nombre INTO gr_det[i].nombre FROM itemsxgrupo
           WHERE lnkItem = gr_det[i].lnkItem
    LET i = i + 1
  END FOREACH
  DISPLAY ARRAY gr_det TO sDet.*
    BEFORE DISPLAY 
      EXIT DISPLAY 
  END DISPLAY  

END FUNCTION 