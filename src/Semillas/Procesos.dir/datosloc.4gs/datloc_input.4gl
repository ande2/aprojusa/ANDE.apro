GLOBALS "datloc_glob.4gl"

FUNCTION finput(accion)
  DEFINE accion CHAR (1)
  DEFINE msgTxt, sqlTxt STRING 
  DEFINE pesoFalta SMALLINT 
  DEFINE preCantCajas, fpesomanual SMALLINT

  IF accion = "I" OR accion = "B" THEN 
     CALL cleanAllDet()
     CLEAR FORM 
     --CALL inicializatmp()
  ELSE 
     CALL llenatmpitems()  
     --SELECT SUM (pesoTotal) INTO lPesoTotal FROM tmpdet2
     --SELECT SUM (pesoTotal) INTO lPesoTotal FROM pemmbo 
     --LET lPesoSaldo = lPesoTotal
  END IF 
   DIALOG ATTRIBUTES(UNBUFFERED)
     
     INPUT gr_reg.idBoletaIng, gr_reg.tipoCaja, gr_reg.tieneTarima 
       FROM idboletaing, tipocaja, tienetarima ATTRIBUTES  (WITHOUT DEFAULTS)
        
        BEFORE INPUT 
           --CALL dialog.setactionactive("close",0)
           DISPLAY "2. gr_reg.idboletaloc ", gr_reg.idBoletaLoc
           IF accion = "I" THEN --OR accion = "B" THEN
              LET gr_reg.idBoletaIng = NULL
              DISPLAY BY NAME gr_reg.idBoletaIng 
              --INITIALIZE gr_reg.* TO NULL
              --CALL cleanAllDet()
              --LET gr_reg.tieneTarima = 0 
           END IF  
           
           CALL dialog.setActionHidden("close",1)

        BEFORE FIELD idboletaing
             IF accion = "U" THEN
                NEXT FIELD NEXT  
             END IF    
             
        AFTER FIELD idBoletaIng
          IF accion = "I" THEN
             
             IF gr_reg.idBoletaIng IS NULL THEN 
                CALL msg("Debe ingresar numero de boleta")
                GOTO opcionBusca
                --NEXT FIELD idBoletaIng
             END IF 
          
          END IF    
          
        ON CHANGE idBoletaIng
           
           CALL cleanAllDet()
           CALL llenatmp(accion)
           --Que no sea nulo
           IF gr_reg.idBoletaing IS NULL THEN
              CALL msg("Debe ingresar orden de producción")
              NEXT FIELD idboletaing
           ELSE 
              SELECT * FROM ordenes WHERE idOrden = gr_reg.idBoletaIng
              IF sqlca.sqlcode = 100 THEN 
                 CALL msg("Boleta no existe o ya tiene información local")
                 LET gr_reg.idBoletaIng = NULL 
                 NEXT FIELD idboletaing
              END IF    
           END IF

           LET sqlTxt = " SELECT tipoboleta, idordpro, iddordpro, productor, ",
                        "        iditem, pesoTotal ",
                        " FROM pemmboleta WHERE idboletaing = ", gr_reg.idBoletaIng
                        --" AND ", lcondi 
           PREPARE ex_stmt FROM sqlTxt 
           EXECUTE ex_stmt INTO ltipo, lidordpro, liddordpro, lproductor, litem, 
                   lPesoTotal
           
           --Para datos de finca/productor
           IF ltipo = 1 THEN
              --Para estructura
              SELECT fincaNomCt || '-'||nomEstructura || '-'|| NVL(nomValvula,'') || '-' || desItemLg 
              INTO vEstructura
              FROM pemdordpro a, pemmfinca b, mestructura c, OUTER(pemMValvula d), glbMItems e 
              WHERE a.idfinca = b.idfinca 
              AND a.idestructura = c.idEstructura 
              AND a.idValvula = d.idValvula 
              AND a.idItem = e.idItem 
              AND a.idOrdPro = lidordpro 
              AND a.iddordpro = liddordpro

              DISPLAY vEstructura TO estructura
              --limpia productor
              LET vProductor = NULL 
              LET vItem = NULL 
              DISPLAY vProductor TO productor
              DISPLAY vItem TO producto

              --Para producto / item
              SELECT TRIM (nombre) || " " || a.desItemLg
              INTO vItem 
              FROM glbMItems a, pemdordpro c, glbmtip b, pemmboleta d
              WHERE d.idBoletaIng = gr_reg.idBoletaIng
              AND  d.idOrdPro = c.idOrdPro
              AND  d.idDOrdPro = c.idDOrdPro
              AND  c.idItem = a.idItem
              AND  a.idtipo = b.idtipo  
              DISPLAY vItem TO producto
           ELSE 
              --Para productor
              SELECT trim(nombre)
              INTO vProductor 
              FROM commemp 
              WHERE id_commemp = lproductor
              DISPLAY vProductor TO productor
              
              --Para producto / item
              SELECT TRIM (nombre) || " " || a.desItemLg
              INTO vItem 
              FROM glbMItems a, pemmboleta c, glbmtip b
              WHERE c.idBoletaIng = gr_reg.idBoletaIng
              AND a.idtipo = b.idtipo  
              --AND  c.idOrdPro = b.idOrdPro
              --AND  c.idDOrdPro = b.idDOrdPro
              AND  a.idItem = c.idItem
              DISPLAY vItem TO producto

              --Limpia estructura
              LET vEstructura = NULL 
              DISPLAY vEstructura TO estructura
           END IF 

           --Para pesos
           DISPLAY lPesoTotal TO fpesototal
           CALL calculaPesoSaldo()           
           DISPLAY lPesoSaldo TO fPesoSaldo

           IF accion = "B" THEN
              RETURN 
           END IF


        ON ACTION buscar
          LABEL opcionBusca:
          CALL llenatmp(accion)
          CALL picklist_2("Boletas", "Boleta","Descripción","idorden", "descripcion", "ordenes", "1=1", "1", 0)
            RETURNING gr_reg.idBoletaIng, vDescripcion, INT_FLAG
          --Que no sea nulo
           IF gr_reg.idBoletaing IS NULL THEN
              CALL msg("Debe ingresar orden de producción")
              NEXT FIELD idboletaing
           ELSE 
              DISPLAY BY NAME gr_reg.idBoletaIng   
           END IF

           LET sqlTxt = " SELECT tipoboleta, idordpro, iddordpro, productor, ",
                        " iditem, pesoTotal ",
                        " FROM pemmboleta WHERE idboletaing = ", gr_reg.idBoletaIng

           PREPARE ex_st1 FROM sqlTxt
           EXECUTE ex_st1 INTO ltipo, lidordpro, liddordpro, lproductor, 
                               litem, lPesoTotal 
           --Si boleta no existe
           IF sqlca.sqlcode = 100 THEN 
              CALL msg(msgTxt)
              NEXT FIELD idboletaing
           END IF 

           --Para datos de finca/productor
           IF ltipo = 1 THEN
              --Para estructura
              SELECT fincaNomCt || '-'||nomEstructura || '-'|| NVL(nomValvula,'') || '-' || desItemLg 
              INTO vEstructura
              FROM pemdordpro a, pemmfinca b, mestructura c, OUTER(pemMValvula d), glbMItems e 
              WHERE a.idfinca = b.idfinca 
              AND a.idestructura = c.idEstructura 
              AND a.idValvula = d.idValvula 
              AND a.idItem = e.idItem 
              AND a.idOrdPro = lidordpro 
              AND a.iddordpro = liddordpro

              DISPLAY vEstructura TO estructura
              --limpia productor
              LET vProductor = NULL 
              LET vItem = NULL 
              DISPLAY vProductor TO productor
              DISPLAY vItem TO producto
              --Para producto / item
              SELECT TRIM (nombre) || " " || a.desItemLg
              INTO vItem 
              FROM glbMItems a, pemdordpro c, glbmtip b, pemmboleta d
              WHERE d.idBoletaIng = gr_reg.idBoletaIng
              AND  d.idOrdPro = c.idOrdPro
              AND  d.idDOrdPro = c.idDOrdPro
              AND  c.idItem = a.idItem
              AND  a.idtipo = b.idtipo  
              DISPLAY vItem TO producto
           ELSE 
              --Para productor
              SELECT trim(nombre)
              INTO vProductor 
              FROM commemp 
              WHERE id_commemp = lproductor
              DISPLAY vProductor TO productor
              
              --Para producto / item
              SELECT TRIM (nombre) || " " || a.desItemLg
              INTO vItem 
              FROM glbMItems a, pemmboleta c, glbmtip b
              WHERE c.idBoletaIng = gr_reg.idBoletaIng
              AND a.idtipo = b.idtipo  
              --AND  c.idOrdPro = b.idOrdPro
              --AND  c.idDOrdPro = b.idDOrdPro
              AND  a.idItem = c.idItem
              DISPLAY vItem TO producto

              --Limpia estructura
              LET vEstructura = NULL 
              DISPLAY vEstructura TO estructura
           END IF 

           --Para pesos
           DISPLAY lPesoTotal TO fpesototal
           CALL calculaPesoSaldo() 
           DISPLAY lPesoSaldo TO fPesoSaldo
           IF accion = "B" THEN
              RETURN 
           END IF

        AFTER INPUT 
           IF accion = "B" THEN
              RETURN 
           END IF
     END INPUT 
     
     INPUT ARRAY gr_det FROM sDet.* ATTRIBUTES (INSERT ROW = FALSE )

       BEFORE INPUT 
         --CALL DIALOG.setActionS( "append", FALSE )
         LET insdet2 = 0
         LABEL labelIS:
         LET idx = gr_det.getLength()
         IF idx > 1 THEN
            FOR i = idx TO 1 STEP -1
               IF i = 1 THEN EXIT FOR END IF 
               IF gr_det[i].lnkItem IS NULL AND gr_det[i].cantcajas IS NULL THEN    
                  CALL gr_det.deleteElement(i)
               END IF  
            END FOR  
         END IF 
         NEXT FIELD lnkItem

       BEFORE FIELD lnkItem
         LET gr_det[arr_curr()].idlinea = arr_curr()
         DISPLAY gr_det[arr_curr()].idlinea TO sdet[scr_line()].idlinea
         
       ON CHANGE lnkItem
         LET idx = arr_curr()
         --Existe ItemSAP?
         CALL fitem(gr_reg.idBoletaIng) RETURNING litem

          CASE 
            WHEN gProcesa = 1 --carreta
              LET lcondi = " AND nombre MATCHES '*CARRETA*' "
            WHEN gProcesa = 2 --cenma
              LET lcondi = " AND nombre MATCHES '*CENMA*' "
            WHEN gProcesa = 3 --devolucion
              LET lcondi = " AND nombre MATCHES '*DEVOLUCI*' "
          END CASE
    
          LET sqlTxt = "SELECT nombre FROM itemsxgrupo ",
                      " WHERE lnkItem = ", gr_det[idx].lnkItem,
                      lcondi
          PREPARE ex_st5 FROM sqlTxt 
          EXECUTE ex_st5 INTO gr_det[idx].nombre   
           
         IF sqlca.sqlcode <> 0 THEN
            CALL msg("Error: IDItemSAP no existe, ingrese de nuevo")
            NEXT FIELD lnkItem 
         END IF 
         IF validaLinea(idx) THEN 
            CALL msg("Item no puede repetirse")
            NEXT FIELD lnkItem
         END IF

       {before  FIELD lnkItem
         LET idx = arr_curr()
         IF gr_det[idx].lnkItem IS NULL THEN
            CALL msg("Ingrese Item")
            NEXT FIELD lnkItem  
         END IF }
         
       ON CHANGE cantcajas
          LET idx = arr_curr()
          LET gr_reg.pesoCli = 0
          
          LET gr_det[arr_curr()].pesoBruto = NULL 
          LET gr_det[arr_curr()].pesoTara = NULL 
          LET gr_det[arr_curr()].pesoNeto = NULL 
          DISPLAY gr_det[arr_curr()].pesoBruto TO sdet[scr_line()].spesobruto
          DISPLAY gr_det[arr_curr()].pesoTara TO sdet[scr_line()].spesotara
          DISPLAY gr_det[arr_curr()].pesoNeto TO sdet[scr_line()].spesoneto 

       AFTER FIELD cantCajas 
         IF gr_det[idx].cantcajas IS NULL THEN  
            CALL msg("Debe ingresar número de cajas")
            NEXT FIELD cantCajas
         ELSE 
            IF gr_det[arr_curr()].pesoNeto > 0 THEN  
               LET idx = arr_curr()

                LET gr_reg.pesoCli = gr_det[idx].cantcajas * gr_det[idx].pesoNeto
                IF pesoExcede() THEN
                   CALL msg(gMsg)
                   NEXT FIELD cantcajas
                END IF 
            ELSE 
                IF fPesoManual = 0 THEN 
                   CALL msg("Debe leer el peso")
                   NEXT FIELD cantcajas  
                END IF    
            END IF 
         END IF    

        BEFORE ROW
          LET fPesoManual = 0 
          
        BEFORE FIELD spesoNeto
          IF fPesoManual = 0 THEN 
             NEXT FIELD calidad
          END IF  

        AFTER FIELD spesoneto 
          --DISPLAY "after field spesoneto"
          LET idx = arr_curr()
          IF fPesoManual = 0 THEN 
             NEXT FIELD calidad
          ELSE 
             IF gr_det[idx].pesoNeto IS NULL THEN 
                CALL msg("Debe ingresar peso")
                NEXT FIELD spesoNeto
             ELSE    
                --Para pesoTotal
                IF idx = 1 THEN  
                   LET gr_reg.pesoCli = gr_det[idx].pesoNeto
                ELSE  
                   LET gr_reg.pesoCli = 0.0
                   FOR i = 1 TO gr_det.getLength()
                      IF gr_det[i].pesoNeto > 0 THEN 
                         LET gr_reg.pesoCli = gr_reg.pesoCli + gr_det[i].pesoNeto 
                      END IF    
                   END FOR 
               END IF
               IF pesoExcede() THEN
                  CALL msg(gMsg)
                  NEXT FIELD spesoneto 
               END IF   
  
                 DISPLAY gr_reg.pesoCli TO fPesoTotal1
             END IF    
          END IF    

        BEFORE FIELD calidad
          IF gProcesa = 3 THEN --devolución
             LET idx = arr_curr()
             LET gr_det[idx].calidad = 3
          END IF 
          
        AFTER FIELD calidad
          LET idx = arr_curr()
          IF gr_det[idx].calidad IS NULL THEN
             CALL msg("Debe ingresar calidad")
             NEXT FIELD calidad 
          END IF 
          
       ON ACTION buscar
          LET idx = arr_curr()
          CALL fitem(gr_reg.idBoletaIng) RETURNING litem
          CASE 
            WHEN gProcesa = 1 --carreta
              LET lcondi = "nombre MATCHES '*CARRETA*' AND"
            WHEN gProcesa = 2 --cenma
              LET lcondi = "nombre MATCHES '*CENMA*' AND"
            WHEN gProcesa = 3 --devolucion
              LET lcondi = "nombre MATCHES '*DEVOLUCI*' AND"
          END CASE 
          LET lcondi = lcondi CLIPPED, " categoria=2 AND iditem =", litem --items locales
          CALL picklist_2("Items", "ID item SAP","Descripción","lnkItem", 'nombre', "itemsxgrupo", lcondi, "1", 0)
            RETURNING gr_det[idx].lnkItem, gr_det[idx].nombre, INT_FLAG
          IF validaLinea(idx) THEN 
            CALL msg("Item no puede repetirse")
            NEXT FIELD lnkItem
         END IF  
          

       ON ACTION leerPeso
         IF gr_det[arr_curr()].cantCajas IS NULL THEN 
           CALL msg("Debe ingresar # de Cajas")
           NEXT FIELD cantcajas
        ELSE 
           LET gr_det[arr_curr()].pesoBruto = getPeso()
           CALL calculaPesos()
           
           LET preCantCajas = gr_det[arr_curr()].cantCajas
           IF pesoExcede() THEN
              CALL msg(gMsg)
              NEXT FIELD cantcajas
           END IF
           NEXT FIELD NEXT 
        END IF

       ON ACTION pesoManual
         LET fPesoManual = 1
         NEXT FIELD sPesoNeto
       
       
       BEFORE FIELD cantcajas 
          LET idx = arr_curr()   
          IF gr_det[idx].lnkItem IS NULL AND gr_det[idx].cantcajas IS NULL THEN
             NEXT FIELD lnkItem 
          END IF 
          IF validaLinea(idx) THEN 
            CALL msg("Item no puede repetirse")
            NEXT FIELD lnkItem
         END IF
         
       
       AFTER INSERT
         LET idx = arr_curr()
          
         IF gr_det[arr_curr()].cantCajas IS NULL 
            --OR gr_det[arr_curr()].pesoBruto IS NULL
            --OR gr_det[arr_curr()].pesoTara IS NULL
            AND gr_det[arr_curr()].pesoNeto IS NULL THEN
            CANCEL INSERT
         END IF

       AFTER DELETE
          CALL calculaPesos()
          
       AFTER INPUT 
         CALL insertaDet2()
         LET insdet2 = 1
         
       
     END INPUT 
     
     ON ACTION ACCEPT
        DISPLAY "3. gr_reg.idboletaloc ", gr_reg.idBoletaLoc
        LET idx = arr_curr()
        IF gr_reg.idBoletaIng IS NULL THEN
           CALL msg("Debe seleccionar boleta")
           GOTO opcionBusca
           --NEXT FIELD idBoletaIng  
        END IF 
                  --Que no sea nulo
           IF gr_reg.idBoletaing IS NULL THEN
              CALL msg("Debe ingresar orden de producción")
              NEXT FIELD idboletaing
           ELSE 
              DISPLAY BY NAME gr_reg.idBoletaIng   
           END IF

           LET sqlTxt = " SELECT tipoboleta, idordpro, iddordpro, productor, ",
                        " iditem, pesoTotal ",
                        " FROM pemmboleta WHERE idboletaing = ", gr_reg.idBoletaIng

           PREPARE ex_st6 FROM sqlTxt
           EXECUTE ex_st6 INTO ltipo, lidordpro, liddordpro, lproductor, 
                               litem, lPesoTotal 
           --Si boleta no existe
           IF sqlca.sqlcode = 100 THEN 
              CALL msg(msgTxt)
              NEXT FIELD idboletaing
           END IF 

           --Para datos de finca/productor
           IF ltipo = 1 THEN
              --Para estructura
              SELECT fincaNomCt || '-'||nomEstructura || '-'|| NVL(nomValvula,'') || '-' || desItemLg 
              INTO vEstructura
              FROM pemdordpro a, pemmfinca b, mestructura c, OUTER(pemMValvula d), glbMItems e 
              WHERE a.idfinca = b.idfinca 
              AND a.idestructura = c.idEstructura 
              AND a.idValvula = d.idValvula 
              AND a.idItem = e.idItem 
              AND a.idOrdPro = lidordpro 
              AND a.iddordpro = liddordpro

              DISPLAY vEstructura TO estructura
              --limpia productor
              LET vProductor = NULL 
              LET vItem = NULL 
              DISPLAY vProductor TO productor
              DISPLAY vItem TO producto
              --Para producto / item
              SELECT TRIM (nombre) || " " || a.desItemLg
              INTO vItem 
              FROM glbMItems a, pemdordpro c, glbmtip b, pemmboleta d
              WHERE d.idBoletaIng = gr_reg.idBoletaIng
              AND  d.idOrdPro = c.idOrdPro
              AND  d.idDOrdPro = c.idDOrdPro
              AND  c.idItem = a.idItem
              AND  a.idtipo = b.idtipo  
              DISPLAY vItem TO producto
           ELSE 
              --Para productor
              SELECT trim(nombre)
              INTO vProductor 
              FROM commemp 
              WHERE id_commemp = lproductor
              DISPLAY vProductor TO productor
              
              --Para producto / item
              SELECT TRIM (nombre) || " " || a.desItemLg
              INTO vItem 
              FROM glbMItems a, pemmboleta c, glbmtip b
              WHERE c.idBoletaIng = gr_reg.idBoletaIng
              AND a.idtipo = b.idtipo  
              --AND  c.idOrdPro = b.idOrdPro
              --AND  c.idDOrdPro = b.idDOrdPro
              AND  a.idItem = c.idItem
              DISPLAY vItem TO producto

              --Limpia estructura
              LET vEstructura = NULL 
              DISPLAY vEstructura TO estructura
           END IF 

           --Para pesos
           DISPLAY lPesoTotal TO fpesototal
           CALL calculaPesoSaldo() 
           DISPLAY lPesoSaldo TO fPesoSaldo
        
        IF accion = "B" THEN
           RETURN 
        END IF
        
        IF gr_det[idx].lnkItem IS NULL AND gr_det[idx].cantCajas IS NOT NULL THEN
           CALL msg("Ingrese Item")
           NEXT FIELD lnkItem 
        END IF 
        IF gr_det[1].lnkItem IS NULL AND gr_det[1].cantCajas IS NULL THEN
           CALL msg("Ingrese Item")
           NEXT FIELD lnkItem 
        END IF 
        
        IF gr_det[idx].cantCajas IS NULL AND gr_det[idx].lnkItem IS NOT NULL THEN  
           CALL msg("Debe ingresar número de cajas")
           NEXT FIELD cantCajas
        END IF

        IF gr_det[idx].pesoNeto IS NULL AND gr_det[idx].lnkItem IS NOT NULL THEN  
           CALL msg("Debe leer peso")
           NEXT FIELD cantCajas
        END IF
        
           IF insdet2 = 0 THEN
              CALL insertaDet2() 
           END IF
        DISPLAY "5. gr_reg.idboletaloc ", gr_reg.idBoletaLoc    
          IF gr_det[idx].calidad IS NULL AND gr_det[idx].lnkItem IS NOT NULL THEN
             CALL msg("Debe ingresar calidad")
             NEXT FIELD calidad
          END IF 

        EXIT DIALOG 

    {       CASE box_gradato("Seguro de grabar")
         WHEN "Si"
            LET resultado = TRUE
            EXIT DIALOG
         WHEN "No"
            EXIT DIALOG 
         OTHERWISE
            CONTINUE DIALOG 
      END CASE 
      LET resultado = TRUE
      EXIT DIALOG }
      
     ON ACTION CANCEL
        CLEAR FORM 
        CALL gr_det.clear()
        CASE 
          WHEN gProcesa = 1 --La Carreta 
             DISPLAY "INGRESO DE DATOS LA CARRETA" TO pdescripcion
          WHEN gProcesa = 2 --CENMA 
             DISPLAY "INGRESO DE DATOS CENMA" TO pdescripcion
          WHEN gProcesa = 3 --Devolución 
             DISPLAY "INGRESO DE DATOS DEVOLUCIÓN" TO pdescripcion  
        END CASE
        LET gr_reg.idBoletaIng = NULL
        --LET gr_reg.cliente  = NULL 
        RETURN --EXIT dialog
   END DIALOG
   --IF INT_FLAG THEN 
      --RETURN 
   --END IF 
   DISPLAY "6. gr_reg.idboletaloc ", gr_reg.idBoletaLoc
   DISPLAY "insert en encabezado"
   IF accion = "I" THEN 
      LET gr_reg.idBoletaLoc = 0
   END IF    
   DISPLAY "Boleta loc", gr_reg.idBoletaLoc 
   
   DISPLAY "Boleta ing", gr_reg.idBoletaIng
   --DISPLAY "Cliente ", gr_reg.cliente
   --DISPLAY "pesototal ", vpesototal
   --DISPLAY "calidadproduccion ", 1
   LET gr_reg.fectran = CURRENT 
   DISPLAY "fectran ", gr_reg.fectran
   LET gr_reg.usuOpero = 10 --Validar usuarios
   DISPLAY "usuopero", gr_reg.usuOpero
   
   --Para id de cliente
   CASE 
     WHEN gProcesa = 1 --carreta
       SELECT idCliente INTO gr_reg.idCliente FROM pemMCli WHERE nombre MATCHES "*CARRETA*"
     WHEN gProcesa = 2 --cenma
       SELECT idCliente INTO gr_reg.idCliente FROM pemMCli WHERE nombre MATCHES "*CENMA*"
     WHEN gProcesa = 3 --devolucion
       SELECT idCliente INTO gr_reg.idCliente FROM pemMCli WHERE nombre MATCHES "*DEVOLUC*"
   END CASE 
   DISPLAY "7. gr_reg.idboletaloc ", gr_reg.idBoletaLoc 
   BEGIN WORK 
     --insertando inforamcion de la boleta local
     IF accion = "I" THEN 
        INSERT INTO pemmboletaloc (idboletaloc, idboletaing, idCliente,
                                   pesoCli, tieneTarima, TipoCaja, fectran, usuopero)
          VALUES (0, gr_reg.idBoletaIng, gr_reg.idCliente,
                  gr_reg.pesoCli, gr_reg.tieneTarima, gr_reg.tipoCaja,
                  gr_reg.fectran, gr_reg.usuOpero )
      ELSE 
        IF accion = "U" THEN 
           UPDATE pemMBoletaLoc SET (idboletaing, idCliente, pesoCli, 
                                     tieneTarima, TipoCaja, fectran, usuopero) =
                  (gr_reg.idBoletaIng, gr_reg.idCliente,
                  gr_reg.pesoCli, gr_reg.tieneTarima, gr_reg.tipoCaja,
                  gr_reg.fectran, gr_reg.usuOpero)
            WHERE idboletaloc = gr_reg.idBoletaLoc -- idBoletaExp
        END IF 
     END IF  
                  
   DISPLAY "insert en detalle de clientes"
   IF sqlca.sqlcode = 0 THEN 
      IF accion = "I" THEN 
         LET gr_reg.idBoletaLoc = sqlca.sqlerrd[2]
       ELSE 
         DELETE FROM pemdboletaloc WHERE idboletaloc = gr_reg.idBoletaLoc
         --DELETE FROM pemcboletaexp WHERE idboletaexp = gr_reg.idBoletaExp}
      END IF 
         
      --Insertando en clientes
      FOR i = 1 TO gr_det.getLength()
        IF gr_det[i].lnkItem IS NOT NULL THEN 
        INSERT INTO pemDBoletaLoc (idBoletaLoc, idLinea, lnkItem, cantCajas, 
                                     pesoBruto, pesoTara, pesoNeto, calidad)
            VALUES (gr_reg.idBoletaLoc, gr_det[i].idlinea, gr_det[i].lnkItem,
                    gr_det[i].cantCajas, gr_det[i].pesoBruto, gr_det[i].pesoTara,
                    gr_det[i].pesoNeto, gr_det[i].calidad)
        END IF 
      END FOR 
    END IF   

      IF sqlca.sqlcode = 0 THEN
         DISPLAY "actualizando estado de la boleta"
         --DISPLAY "update pemmboleta set estado = 2 where idboletaing = ", gr_reg.idBoletaIng
         IF gProcesa = 3 THEN --Devolucion
            UPDATE pemmboleta SET estadoDevolucion = 1 WHERE idboletaing = gr_reg.idBoletaIng
         END IF 
         UPDATE pemmboleta SET estado = 3 WHERE idboletaing = gr_reg.idBoletaIng
         IF sqlca.sqlcode = 0 THEN
            CALL msg("Informacion grabada")
            --ROLLBACK WORK
            COMMIT WORK
         ELSE 
            CALL msg("Error al grabar información 3")
            ROLLBACK WORK 
         END IF  
      ELSE 
         CALL msg("Error al grabar información 2")
         ROLLBACK WORK 
      END IF 
END FUNCTION 

####### Encabezado ##############

FUNCTION creatmp()

    CREATE TEMP TABLE tmpdet2
  (idcliente    INTEGER, 
   lnkItem      INTEGER ,
   cantcajas    SMALLINT , 
   peso         SMALLINT,
   pesoTotal    SMALLINT,
   PRIMARY KEY (idcliente, lnkitem) )


  CREATE TEMP TABLE ordenes
  (
  idorden   INTEGER ,
  descripcion   VARCHAR (100)
  )
END FUNCTION 

{FUNCTION inicializatmp()
  DROP TABLE tmpdet2
  CREATE TEMP TABLE tmpdet2
  (idcliente    INTEGER, 
   lnkItem      INTEGER ,
   cantcajas    SMALLINT , 
   peso         SMALLINT,
   pesoTotal    SMALLINT,
   PRIMARY KEY (idcliente, lnkitem)  )

   FOR i = 1 TO gr_cli.getLength()
      INITIALIZE gr_cli[i].* TO NULL
   END FOR 
   FOR i = 1 TO gr_det.getLength()
      INITIALIZE gr_det[i].* TO NULL
   END FOR
   CLEAR FORM 
END FUNCTION}

FUNCTION llenatmp(laccion)
  DEFINE laccion CHAR (1) 
  DEFINE sqlTxt STRING 
  DEFINE vBol, vordpro, vdordpro, vproductor, vitem, vlPeso1, vlPeso2 INTEGER
  DEFINE vestructura, cproductor, citem, vdes VARCHAR (100)
  DEFINE pesoSaldo SMALLINT 
  DEFINE localcond STRING  

  DELETE FROM ordenes WHERE 1=1
  LET localcond = ""
  CASE 
            WHEN gProcesa = 1 --carreta
              LET localcond = " AND b.idcliente = 6) "
            WHEN gProcesa = 2 --cenma
              LET localcond = " AND b.idcliente = 7) "
            WHEN gProcesa = 3 --devolucion
              LET localcond = " AND b.idcliente = 8) ",
                              " AND a.estadoDevolucion IS NULL "
  END CASE
  --Para boletas locales
  LET sqlTxt = " SELECT a.idboletaing, a.idordpro, a.iddordpro ",
               " FROM pemmboleta a ",
               " WHERE a.tipoBoleta = 1 ",
               " AND a.estado <> 0 ", --no cerrada IN (2,4) ", --ya tienen exportacion
               " AND (a.estadoExportacion = 1 OR a.esDirecto = 1 ) " --

               
               --" AND a.estadoDevolucion IS NULL "
               
   IF laccion = "I" THEN 
      LET sqlTxt = sqlTxt CLIPPED, 
                   " AND a.idboletaing NOT IN ",
                   " (SELECT b.idboletaing FROM pemmboletaloc b ",
                   " WHERE a.idboletaing = b.idboletaing ",
                   localcond  
   END IF    
   DISPLAY "laccion ", laccion
   DISPLAY "sqlTxt -> ", sqlTxt

               

  {CASE 
    WHEN laccion = "I" --Ingreso
      LET sqlTxt = sqlTxt || " AND estado = 1 "
    WHEN laccion = "B" --Busqueda
      LET sqlTxt = sqlTxt || " AND estado IN (1,2) "
  END CASE}
  PREPARE ex_bolLoc FROM sqlTxt  
  DECLARE cur01 CURSOR FOR ex_bolLoc

    FOREACH cur01 INTO vBol, vordpro, vdordpro
      SELECT fincaNomCt || '-'||nomEstructura || '-'|| NVL(nomValvula,'') || '-' || desItemLg
        INTO vestructura
        FROM pemdordpro a, pemmfinca b, mestructura c, OUTER(pemMValvula d), glbMItems e 
       WHERE a.idfinca = b.idfinca 
         AND a.idestructura = c.idEstructura 
         AND a.idValvula = d.idValvula 
         AND a.idItem = e.idItem 
         AND a.idOrdPro = vordpro 
         AND a.iddordpro = vdordpro
         INSERT INTO ordenes VALUES (vBol, vestructura)
    END FOREACH 

    --Para productor
    LET sqlTxt = " SELECT a.idboletaing, a.productor, a.iditem ",
               " FROM pemmboleta a ",
               " WHERE a.tipoBoleta = 2 ",
               --" AND estado IN (2,4) "
               " AND a.estado <> 0 ", --no cerrada
               " AND (a.estadoExportacion = 1 OR a.esDirecto = 1) ", --que ya tenga exportacion o sea directo
               " AND a.estadoDevolucion IS NULL "

   IF laccion = "I" THEN 
      LET sqlTxt = sqlTxt CLIPPED, 
                   " AND a.idboletaing NOT IN ",
                   " (SELECT b.idboletaing FROM pemmboletaloc b ",
                   " WHERE a.idboletaing = b.idboletaing ",
                   localcond        
   END IF              
   DISPLAY "sqlTxt -> ", sqlTxt            
  {CASE 
    WHEN laccion = "I" --Ingreso
      LET sqlTxt = sqlTxt || " AND estado = 1 "
    WHEN laccion = "B" --Busqueda
      LET sqlTxt = sqlTxt || " AND estado IN (1,2) "
  END CASE}
  PREPARE ex_bolProd FROM sqlTxt
    DECLARE cur02 CURSOR FOR ex_bolProd

    FOREACH cur02 INTO vBol, vproductor, vitem
      SELECT trim(nombre)
        INTO cProductor 
        FROM commemp 
       WHERE id_commemp = vproductor
              --DISPLAY vProductor TO productor
    
      --Para producto / item
      SELECT desItemLg
        INTO cItem 
        FROM glbMItems
       WHERE idItem = vitem
              --DISPLAY vItem TO producto
      LET vdes = cproductor CLIPPED, "-",citem
      INSERT INTO ordenes VALUES (vBol, vdes)
    END FOREACH 

    --Borrando las boletas que tengan ingreso en exportacion y ya no tengan saldo
    DECLARE cur03 CURSOR FOR SELECT idOrden FROM ordenes
    FOREACH cur03 INTO vBol
      SELECT FIRST 1 idBoletaing FROM pemmboletaexp WHERE idboletaing = vBol
      IF sqlca.sqlcode = 0 THEN
         SELECT SUM (pesoTotal) INTO vlPeso1
         FROM pemmboletaexp
         WHERE idboletaing = vBol
         GROUP BY idBoletaIng
         SELECT a.pesoTotal INTO vlPeso2 
         FROM pemmboleta a
         WHERE a.idboletaing = vBol
         LET pesoSaldo = vlPeso2 - vlPeso1
         IF pesoSaldo <= 0 THEN
            DELETE FROM ordenes WHERE idOrden = vBol
            IF sqlca.sqlcode = 0 THEN
               DISPLAY "borre ", vBol 
            END IF  
         END IF  
      END IF 
    END FOREACH  
END FUNCTION 

FUNCTION calculaPesoSaldo() 
DEFINE pesoExportacion, pesoLocal LIKE pemmboleta.pesototal
  LET lSumPeso = 0
  
  --Peso total de ingreso en lPesoTotal
  --Para peso de exportacion
  SELECT NVL (SUM (pesototal),0) INTO pesoExportacion FROM pemmboletaexp
  WHERE idboletaing = gr_reg.idBoletaIng
  GROUP BY idboletaing
  IF pesoExportacion IS NULL THEN LET pesoExportacion = 0 END IF 
  
  --Para peso Local (Carreta, Cenma y Devolucion)
  SELECT NVL (SUM (pesocli), 0) INTO pesoLocal FROM pemmboletaloc
  WHERE idboletaing = gr_reg.idBoletaIng 
  GROUP BY idboletaing
  IF pesoLocal IS NULL THEN LET pesoLocal = 0 END IF 
  LET lSumPeso = pesoExportacion + pesoLocal
  {SELECT SUM (pesoTotal) INTO lSumPeso FROM pemmboletaExp
  WHERE idboletaing = gr_reg.idBoletaIng
  GROUP BY idboletaing }
  IF lSumPeso > 0 THEN
     LET lPesoSaldo = lPesoTotal - lSumPeso
  ELSE 
     LET lPesoSaldo = lPesoTotal - lSumPeso
  END IF
   DISPLAY "pesoExportacion ", pesoExportacion
   DISPLAY "pesoLocal ", pesoLocal
   DISPLAY "lSumPeso ", lSumPeso 
   DISPLAY "lPesoTotal ", lPesoTotal
   DISPLAY "lPesoSaldo ", lPesoSaldo
END FUNCTION

####### Detalle 1 #############


FUNCTION clirep(nline)
DEFINE nline SMALLINT 
{
  FOR i = 1 TO gr_cli.getLength()
    IF i <> nline THEN
     IF gr_cli[i].idcliente = gr_cli[nline].idcliente THEN  
       RETURN TRUE 
     END IF 
    END IF 
  END FOR }
  RETURN FALSE 
END FUNCTION 

FUNCTION cliExDet2(lcli)
DEFINE lcli INTEGER 

  SELECT FIRST 1 idcliente FROM tmpdet2
  WHERE idcliente = lcli
     IF sqlca.sqlcode = 0 THEN 
        RETURN TRUE  
     ELSE 
        RETURN FALSE 
     END IF 

END FUNCTION 

FUNCTION cleandet1()
 { FOR i = 1 TO gr_cli.getLength()
      INITIALIZE gr_cli[i].* TO NULL
   END FOR 
   FOR i = gr_cli.getLength() TO 1 STEP -1
     CALL gr_cli.deleteElement(i)
   END FOR }
   
END FUNCTION

##### Detalle 2 ##########

FUNCTION mostrarDet2(lcli)
DEFINE lcli, i INTEGER 
{
  CALL cleandet2()
  SELECT COUNT (*) INTO j FROM tmpdet2 WHERE idcliente = lcli
  DISPLAY "en tmp van ", j
  DECLARE cur003 CURSOR FOR 
    SELECT idcliente, lnkItem, cantCajas, peso, pesoTotal
    FROM tmpdet2
    WHERE idcliente = lcli
  LET i = 1
  FOREACH cur003 INTO gr_det[i].idcliente, gr_det[i].lnkItem, 
    gr_det[i].cantcajas, gr_det[i].peso, gr_det[i].pesoTotal
    SELECT nombre INTO gr_det[i].nombre FROM itemsxgrupo
           WHERE lnkItem = gr_det[i].lnkItem
    LET i = i + 1
    DISPLAY "pase foreach ", i
  END FOREACH
  DISPLAY "arreglo tiene lineas ", gr_det.getLength()
  DISPLAY ARRAY gr_det TO sDet.*
    BEFORE DISPLAY 
      EXIT DISPLAY 
  END DISPLAY } 
END FUNCTION 

FUNCTION fitem(numBol)
DEFINE numbol, numitem, tipBol INTEGER 

SELECT tipoboleta INTO tipBol FROM pemmboleta WHERE idboletaing = numbol
IF tipBol = 1 THEN 
  SELECT a.iditem 
  INTO numitem 
  FROM pemdordpro a, pemmboleta b
  WHERE a.idordpro = b.idordpro
  AND a.iddordpro = b.iddordpro
  AND b.idboletaing = numBol
ELSE 
  SELECT iditem INTO numitem FROM pemmboleta WHERE idboletaing = numBol
END IF 

RETURN numitem
END FUNCTION 

FUNCTION cleandet2()
  FOR i = 1 TO gr_det.getLength()
      INITIALIZE gr_det[i].* TO NULL
   END FOR 
   FOR i = gr_det.getLength() TO 1 STEP -1
     CALL gr_det.deleteElement(i)
   END FOR 
   
END FUNCTION 

FUNCTION insertaDet2()
 { DELETE FROM tmpdet2 WHERE idcliente = gr_cli[idx2].idcliente
  FOR i = 1 TO gr_det.getLength()
     IF gr_det[i].lnkItem IS NOT NULL AND gr_det[i].cantcajas IS NOT NULL THEN 
        INSERT INTO tmpdet2(idcliente, lnkItem, cantcajas, peso, pesoTotal)
          VALUES (gr_cli[idx2].idcliente, gr_det[i].lnkItem, 
          gr_det[i].cantcajas, gr_det[i].peso, gr_det[i].pesoTotal)
     END IF      
  END FOR 
  SELECT SUM (pesoTotal) INTO gr_cli[idx2].pesocli FROM tmpdet2 WHERE idcliente = gr_cli[idx2].idcliente
  LET vpeso = 0 
  FOR i = 1 TO gr_cli.getLength()
     IF gr_cli[i].pesocli IS NOT NULL THEN 
        LET vpeso = vpeso + gr_cli[i].pesocli
     END IF 
  END FOR 
  LET lpesosaldo = lpesototal - vpeso
  
  DISPLAY lpesosaldo TO fpesosaldo }
END FUNCTION 

FUNCTION cleanAllDet()
  CALL cleanDet2()
  CALL cleanDet1()
END FUNCTION 

FUNCTION validaLinea(idx)
DEFINE i, idx SMALLINT 

  FOR i = 1 TO gr_det.getLength()
    IF i <> idx THEN
       IF gr_det[i].lnkItem = gr_det[idx].lnkitem THEN 
          RETURN TRUE  
       END IF 
    END IF 
  END FOR 
  
  RETURN FALSE  
END FUNCTION

FUNCTION pesoExcede()
DEFINE pesoActual, pesoLocal, i, npeso, npeso2,pesoSaldo, pesoExp SMALLINT 

  DISPLAY "Entre a pesoExcede()"
  SET LOCK MODE TO WAIT 3
  SELECT SUM (pesoTotal) INTO pesoExp FROM pemMBoletaExp
  WHERE idBoletaIng = gr_reg.idBoletaIng
  DISPLAY "Peso exportacion ", pesoActual
  IF pesoActual > 0 THEN 
  ELSE 
     LET pesoActual = 0
  END IF 
  {SELECT SUM (pesoCli) INTO pesoLocal FROM pemMBoletaLoc
  WHERE idBoletaIng = gr_reg.idBoletaIng}
  LET pesoLocal = 0
  FOR i = 1 TO gr_det.getLength()
    IF gr_det[i].pesoNeto > 0 THEN
       LET pesoLocal = pesoLocal + gr_det[i].pesoNeto 
       LET i = i + 1
    END IF 
  END FOR 
  DISPLAY "Peso Local ", pesoLocal
  {IF pesoLocal > 0 THEN 
  ELSE 
     LET pesoLocal = 0
  END IF 
  LET pesoActual = pesoActual + pesoLocal
  FOR i = 1 TO gr_det.getLength()
     IF gr_det[i].pesoNeto > 0 THEN 
        LET pesoActual = pesoActual + gr_det[i].pesoNeto
     END IF    
  END FOR}
  
  SELECT pesoTotal INTO nPeso FROM pemmboleta
  WHERE idBoletaing = gr_reg.idboletaing

  LET pesoSaldo = nPeso - pesoExp 
  IF pesoLocal > pesoSaldo THEN 
     LET gMsg = "Suma de peso es superior a saldo disponible, ingrese de nuevo" 
     RETURN TRUE 
  ELSE
    RETURN FALSE   
  END IF 
  
END FUNCTION 

FUNCTION llenatmpitems()
{
DEFINE lcliente, litem  INTEGER 
DEFINE lcajas, lpeso, lpesototal SMALLINT 

  DECLARE cur10 CURSOR FOR
    SELECT lnkItem, cantCajas, pesoBruto, pesoTara
    FROM pemDBoletaLoc 
    WHERE idBoletaLoc = gr_reg.idBoletaLoc

  FOREACH cur10 INTO litem, lcajas, lpeso, lpesototal
    INSERT INTO tmpdet2 (lnkItem, cantCajas, peso, pesoTotal)
      VALUES (litem, lcajas, lpeso, lpesototal)
  END FOREACH }

END FUNCTION 

FUNCTION getPeso()
  DEFINE pesoManual SMALLINT
  DEFINE fn, fs, ft STRING 
  DEFINE cPeso STRING 
  DEFINE result SMALLINT, --,opc,activo   
         peso   DEC(9,2)

  LET pesoManual = getValParam("LEER PESO")       
  --LET pesoManual = 1

  IF pesomanual = 1 THEN 
     PROMPT "Ingrese peso: " FOR peso 
     RETURN peso 
  ELSE 
     -- Obteniendo peso de bascula
     LET fn = "C:\\\\Basculas\\\\SipLafarge.exe"
     LET fs = "C:\\\\SCALEWORKDIR\\\\pesos.prn"
     LET ft = "/tmp/file.txt"
     
     --LET fn = "bascula" --||w_mae_psj.numbas 
     CALL librut001_leerbascula(fn,fs,ft,"G")
     RETURNING result,peso
     DISPLAY "result ", result, " peso ", peso 
     IF NOT result THEN
        DISPLAY "entre if"
        RETURN -1  
     ELSE 
        DISPLAY "entre al else"
        RETURN peso
     END IF
     
  END IF  
END FUNCTION 

FUNCTION calculaPesos() --fArray qye arreglo esta pesando 1 local 2 productor
DEFINE pesoTarima, pesoCaja, pesoCajaT DECIMAL(9,2)
DEFINE idx, i, fArray SMALLINT 

  LET idx = arr_curr()
  --obtiene peso de tarima
  IF gr_reg.tieneTarima THEN
     LET pesoTarima = getValParam("RECEPCION DE FRUTA - PESO DE TARIMA")
  ELSE 
     LET pesoTarima = 0.0
  END IF  
  
  --obtiene peso segun tipo de caja
  SELECT pesoCatEmpaque INTO pesoCaja
  FROM pemMcatEmp
  WHERE idCatEmpaque = gr_reg.tipoCaja
  
  --Calcula Tara (multiplica cantCajas * pesoCaja) + pesoTarima
     LET pesoCajaT =  gr_det[idx].cantCajas * pesoCaja

     LET gr_det[idx].pesoTara = pesoTarima + pesoCajaT
     DISPLAY "Tara lleva: ", gr_det[idx].pesoTara 
  
  --Para peso Neto
     LET gr_det[idx].pesoNeto = gr_det[idx].pesoBruto -
                                    gr_det[idx].pesoTara
     DISPLAY gr_det[arr_curr()].pesoBruto,
             gr_det[arr_curr()].pesoTara,
             gr_det[arr_curr()].pesoNeto
          TO sdet[scr_line()].sPesoBruto,
             sdet[scr_line()].sPesoTara,
             sdet[scr_line()].sPesoNeto
    DISPLAY "Peso Neto lleva: ", gr_det[idx].pesoNeto
 
          
  --Para pesoTotal
  IF idx = 1 THEN  
     LET gr_reg.pesoCli = gr_det[idx].pesoNeto
  ELSE  
     LET gr_reg.pesoCli = 0.0
     FOR i = 1 TO gr_det.getLength()
        IF gr_det[i].pesoNeto > 0 THEN 
           LET gr_reg.pesoCli = gr_reg.pesoCli + gr_det[i].pesoNeto 
        END IF    
     END FOR 
  END IF  
  
  DISPLAY gr_reg.pesoCli TO fPesoTotal1

  --retorna total
  --RETURN taraTotal
END FUNCTION 

