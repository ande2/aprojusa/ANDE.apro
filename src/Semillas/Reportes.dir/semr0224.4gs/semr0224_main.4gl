DATABASE db0001

DEFINE fecIni, fecFin DATE 
DEFINE gr_reg RECORD
  idboletaing LIKE pemmboleta.idboletaing,
  tipoBoleta  LIKE pemmboleta.tipoboleta,
  fecha        LIKE pemmboleta.fecha,
  tipoItem     CHAR (1),
  lnkitem      INTEGER ,
  idOrdPro    LIKE pemmboleta.idordpro,
  idDOrdPro   LIKE pemmboleta.iddordpro,
  productor   LIKE pemmboleta.productor,
  tipo         LIKE glbmtip.nombre ,
  proveedor    VARCHAR (200),
  finca        VARCHAR (100) , --LIKE pemmfinca.fincanomct,
  mercado      VARCHAR (100),
  producto     VARCHAR (100),
  calidad1     VARCHAR (100) ,
  calidad      VARCHAR (100) ,
  cantCajas        INTEGER ,
  librasxcaja  SMALLINT,
  totalLibras  INTEGER ,
  precioConLocal VARCHAR (100),     
  precioLocal     VARCHAR (100),    
  precioConDol    VARCHAR (100),    
  precioSinDescCalidad  VARCHAR (100), 
  precioConDescCalidad  VARCHAR (100), 
  totalFlete            VARCHAR (100), 
  costoxLibra           VARCHAR (100), 
  porcxLibra            VARCHAR (100), 
  numSemana             SMALLINT ,    
  anio                  SMALLINT ,    
  numEntrada            VARCHAR (100),
  estado                INTEGER   
   
END RECORD

DEFINE vCalidad SMALLINT 

MAIN
  DEFINE prog_name2 STRING 

  DEFER INTERRUPT 

  LET prog_name2 = "semr0224.log"   -- El progr_name es definido como constante en el arch. globals
    
  CALL STARTLOG(prog_name2)
  CALL main_init()

END MAIN 

FUNCTION main_init()

  CALL ui.Interface.loadActionDefaults("actiondefaults")
  CALL ui.Interface.loadStyles("styles_sc")

  CLOSE WINDOW SCREEN
  OPEN WINDOW w1 WITH FORM "semr0224_form1"

  CALL fgl_settitle("ANDE - Reporte de boletas de Recepci�n")
  
MENU "Boletas de Recepci�n"
  BEFORE MENU 
    CALL pideparam()
    EXIT MENU 
    
  ON ACTION generar
     CALL pideParam()
  ON ACTION salir
    EXIT MENU 
END MENU 

END FUNCTION 

FUNCTION pideParam()
  DEFINE sqlTxt STRING 
  DEFINE vestructura, cproductor, citem, citem1, vdes VARCHAR (100)
  DEFINE vBol, vordpro, vdordpro, vproductor, vitem INTEGER
  DEFINE myHandler om.SaxDocumentHandler
  
  INPUT BY NAME fecIni, fecFin
    {BEFORE INPUT 
      LET fecIni = "07032016"
      LET fecFin = "11032016"
      EXIT INPUT }
    AFTER FIELD fecIni
      IF fecIni IS NULL THEN
         CALL msg("Ingrese fecha de inicio")
         NEXT FIELD fecIni
      END IF

   AFTER FIELD fecFin
      IF fecFin IS NULL THEN
         CALL msg("Ingrese fecha final")
         NEXT FIELD fecFin
      END IF      
  END INPUT 
  
  IF INT_FLAG THEN 
     CALL msg("Reporte cancelado por el usuario")
     RETURN 
  END IF 
--SET EXPLAIN ON 
  --Boletas
  {LET sqlTxt = " SELECT t1.idboletaing, t1.tipoboleta, t1.fecha, 'E', t3.lnkitem, t3.cantCajas, t3.pesototal ",
               " FROM pemmboleta t1, pemmboletaexp t2, pemdboletaexp t3 ", 
               " WHERE t1.idboletaing = t2.idboletaing ",
               " AND t2.idboletaexp = t3.idboletaexp ",
               " AND t1.estado = 0 ",
               " AND fecha BETWEEN '", fecIni, "'", " AND '", fecFin, "'",
               " UNION ",
               " SELECT t1.idboletaing, t1.tipoboleta, t1.fecha, 'L', t3.lnkitem, t3.cantCajas, t3.pesoneto ",
               " FROM pemmboleta t1, pemmboletaloc t2, pemdboletaloc t3 ",
               " WHERE t1.idboletaing = t2.idboletaing ",
               " AND t2.idboletaloc = t3.idboletaloc ",
               " AND t1.estado = 0 ",
               " AND fecha BETWEEN '", fecIni, "'", " AND '", fecFin, "'",
               " ORDER BY 1 "}

  LET sqlTxt = " SELECT t1.idboletaing, t1.tipoboleta, t1.fecha, 'E', t3.lnkitem, t3.cantCajas, t3.pesototal, ' ', t1.estado ", 
               " FROM pemmboleta t1, OUTER (pemmboletaexp t2, OUTER (pemdboletaexp t3 )) ",
               " WHERE t1.idboletaing = t2.idboletaing ",
               " AND t2.idboletaexp = t3.idboletaexp ",
               " AND t1.estado IN (0,-1) ",
               " AND fecha BETWEEN '", fecIni, "'", " AND '", fecFin, "'",
               " UNION ",
               " SELECT t1.idboletaing, t1.tipoboleta, t1.fecha, 'L', t3.lnkitem, t3.cantCajas, t3.pesoneto, ' ', t1.estado ", 
               " FROM pemmboleta t1, OUTER (pemmboletaloc t2, OUTER (pemdboletaloc t3 )) ",
               " WHERE t1.idboletaing = t2.idboletaing ",
               " AND t2.idboletaloc = t3.idboletaloc ",
               " AND t1.estado IN (0,-1) ",
               " AND fecha BETWEEN '", fecIni, "'", " AND '", fecFin, "'",
               " UNION ",
               " SELECT b.idboletaing, b.tipoboleta, b.fecha, 'E', 1, 0, c.merma, a.nombre, b.estado ", 
               " FROM itemsxgrupo a, pemmboleta b, pemmboletacau c ",
               " WHERE a.nombre LIKE '%MERMA%' ",
               " AND a.iditem = b.iditem ",
               --" AND b.idboletaing = 1 ", lr_reg.idboletaing ,
               " AND b.idboletaing = c.idboletaing ",
               " AND b.fecha BETWEEN '", fecIni, "'", " AND '", fecFin, "'",
               " UNION ",
               " SELECT b.idboletaing, b.tipoboleta, b.fecha, 'E', 1, 0, c.merma, i.nombre, b.estado ", 
               " FROM itemsxgrupo i, pemmboleta b, pemdordpro d, pemmboletacau c ",
               " WHERE i.nombre LIKE '%MERMA%' ",
               " AND b.idordpro = d.idordpro ", 
               " AND b.iddordpro = d.iddordpro ",
               " AND d.iditem = i.iditem ",
               " AND b.fecha BETWEEN '", fecIni, "'", " AND '", fecFin, "'",
                --AND b.idboletaing = 1 --", lr_reg.idboletaing
               " AND b.idboletaing = c.idboletaing ",
               " ORDER BY 1 "
--DISPLAY sqlTxt

               
  {LET sqlTxt = " SELECT idboletaing, tipoBoleta, idOrdPro, idDOrdPro, productor, fecha ",
               " FROM pemmboleta ",
               " WHERE estado in (1,2) AND fecha BETWEEN '", fecIni, "'",
               " AND '", fecFin, "'"}
  PREPARE ex_sql1 FROM sqlTxt
  DECLARE cur01 CURSOR FOR ex_sql1
  LET myHandler = gral_reporte_all("unapagina","horizontal","XLS",132,"con_r_08")
  START REPORT repBolIng 
  
  FOREACH cur01 INTO gr_reg.idboletaing, gr_reg.tipoBoleta, gr_reg.fecha, 
          gr_reg.tipoItem, gr_reg.lnkitem, gr_reg.cantCajas, gr_reg.totalLibras, gr_reg.calidad1, gr_reg.estado -- gr_reg.tipoBoleta, gr_reg.idOrdPro,
                     --gr_reg.idDOrdPro, gr_reg.productor, gr_reg.fecha
    --DISPLAY "Boleta ", gr_reg.idboletaing, gr_reg.tipoBoleta, gr_reg.idOrdPro,
      --               gr_reg.idDOrdPro, gr_reg.productor
--DISPLAY "datos finca/productor ", CURRENT     

    --Para datos de finca/productor
    SELECT idordpro, iddordpro INTO gr_reg.idOrdPro, gr_reg.idDOrdPro FROM pemmboleta WHERE idboletaing = gr_reg.idboletaing
    IF gr_reg.tipoBoleta = 1 THEN
       --Para proveedor
       IF gr_reg.estado = -1 THEN
          LET gr_reg.proveedor = "ANULADA"
       ELSE
          SELECT fincaNomCt || '-'||nomEstructura || '-'|| NVL(nomValvula,'') || '-' || nombre || '-' || desItemLg 
          INTO gr_reg.proveedor
          FROM pemdordpro a, pemmfinca b, mestructura c, glbmtip f, OUTER(pemMValvula d), glbMItems e 
          WHERE a.idfinca = b.idfinca 
          AND a.idestructura = c.idEstructura 
          AND a.idValvula = d.idValvula 
          AND a.idItem = e.idItem 
          AND a.idOrdPro = gr_reg.idOrdPro 
          AND a.iddordpro = gr_reg.idDOrdPro
          AND e.idtipo = f.idtipo
       END IF

       --DISPLAY "vestructura ", vEstructura 
       --limpia productor
       LET vProductor = NULL 
       LET vItem = NULL 
       --DISPLAY "vProductor ", vProductor
       --DISPLAY "vItem ", vItem 
       
       --Para tipo
       SELECT t1.nombre
       INTO gr_reg.tipo
       FROM glbMTip t1, glbmitems t2, pemdordpro t3, pemmboleta t4
       WHERE t1.idtipo = t2.idtipo
       AND t2.iditem = t3.iditem
       AND t3.idordpro = t4.idordpro
       AND t3.iddordpro = t4.iddordpro
       AND t4.idboletaing = gr_reg.idboletaing

       --Para finca
       SELECT t1.fincanomct
         INTO gr_reg.finca
         FROM pemmfinca t1, pemdordpro t2, pemmboleta t3
         WHERE t1.idfinca = t2.idfinca
         AND t2.idordpro = t3.idordpro
         AND t2.iddordpro = t3.iddordpro
         AND t3.idboletaing = gr_reg.idboletaing
--DISPLAY "finca producto ", CURRENT 

       --Para producto
       SELECT trim (b.nombre)||" "|| trim( c.nombre)|| " " || a.desItemLg
         INTO gr_reg.producto 
         FROM glbMItems a, glbmfam b, glbmtip c  
        WHERE a.idfamilia = b.idfamilia 
          AND a.idtipo = c.idtipo
          AND a.iditem = 
      (SELECT e.iditem 
        FROM pemmboleta d, pemdordpro e  
       WHERE d.idordpro = e.idordpro 
         AND d.iddordpro = e.iddordpro
         AND d.idboletaing = gr_reg.idboletaing)

       --Para numero de entrada
       LET gr_reg.numEntrada = NULL 
       SELECT t1.numreciboprosap --t1.numentradainvsap
         INTO gr_reg.numEntrada
         FROM pemmdatossap t1 
         WHERE t1.idboletaing = gr_reg.idboletaing

       --Para precios
       {LET gr_reg.precioConLocal = NULL 
       LET gr_reg.precioLocal = NULL 
       SELECT t1.costoart INTO gr_reg.precioConDol FROM pemddatossap t1 
       WHERE t1.idboletaing = gr_reg.idboletaing AND t1.lnkitem = gr_reg.lnkitem}
    ELSE 
       --Para proveedor
       --DISPLAY "===> boleta ", gr_reg.idboletaing, " estado ", gr_reg.estado
       IF gr_reg.estado = -1 THEN
          LET gr_reg.proveedor = "ANULADA"
       ELSE
          SELECT trim(t1.nombre)
          INTO gr_reg.proveedor 
          FROM commemp t1, pemmboleta t2  
          WHERE t1.id_commemp = t2.productor
          AND t2.idboletaing = gr_reg.idboletaing
          --DISPLAY "Proveedor ", gr_reg.proveedor
       END IF 
              
       --Para producto / item
       SELECT nombre
       INTO cItem1
       FROM pemmboleta a, glbmitems b, glbmtip c
       WHERE c.idtipo = b.idtipo
       AND b.iditem = a.iditem
       AND a.idboletaing = gr_reg.idboletaing

       SELECT FIRST 1 desItemLg
       INTO cItem 
       FROM glbMItems a, pemmboleta c
       WHERE c.idItem = a.idItem 

       --DISPLAY "vItem ", cItem

       --Limpia estructura
       LET vEstructura = cItem1 CLIPPED, "-", cItem 
       --DISPLAY "vEstrctura ", vEstructura
--DISPLAY "productor tipo ", CURRENT 
       --Para tipo
       SELECT t1.nombre
       INTO gr_reg.tipo
       FROM glbMTip t1, glbmitems t2, pemmboleta t4
       WHERE t1.idtipo = t2.idtipo
       AND t2.iditem = t4.iditem
       AND t4.idboletaing = gr_reg.idboletaing

       --Para finca
       {SELECT t1.nombre
         INTO gr_reg.finca
         FROM commemp t1,pemmboleta t3
         WHERE t3.productor = t1.id_commemp
         AND t3.idboletaing = gr_reg.idboletaing}
       LET gr_reg.finca = "Productor"  

       --Para producto
       SELECT trim (b.nombre)||" "|| trim( c.nombre)|| " " || a.desItemLg
         INTO gr_reg.producto 
         FROM glbMItems a, glbmfam b, glbmtip c  
        WHERE a.idfamilia = b.idfamilia 
          AND a.idtipo = c.idtipo
          AND a.iditem = 
      (SELECT d.iditem 
        FROM pemmboleta d  
       WHERE d.idboletaing = gr_reg.idboletaing)

       --Para numero de entrada
       LET gr_reg.numEntrada = NULL
       SELECT t1.numentmercexpsap
         INTO gr_reg.numEntrada
         FROM pemmdatossap t1 
         WHERE t1.idboletaing = gr_reg.idboletaing
       IF gr_reg.numEntrada IS NULL THEN 
          SELECT t1.numentmerclocsap
         INTO gr_reg.numEntrada
         FROM pemmdatossap t1 
         WHERE t1.idboletaing = gr_reg.idboletaing
       END IF   

       --Para precios
       {LET gr_reg.precioConDol = NULL 
       LET gr_reg.precioConLocal = NULL 
       LET gr_reg.precioLocal = NULL 
       SELECT t1.costoart INTO gr_reg.precioConDol FROM pemddatossap t1 
       WHERE t1.idboletaing = gr_reg.idboletaing AND t1.lnkitem = gr_reg.lnkitem}  
    END IF
--DISPLAY "condiciones para items de exp y loc ", CURRENT 
    --Separa condiciones para Items de Exportaci�n y Locales
    IF gr_reg.tipoItem = "E" THEN --Exportacion
      --Para mercado
      LET gr_reg.mercado = "Semillas del Campo - Exportaci�n"
      --Para Calidad
      LET gr_reg.calidad = "Aprovechamiento"

    ELSE --Locales  
      --Para mercado
      LET gr_reg.mercado = "Semillas del Campo - Mercado Local"

      --Para Calidad
      SELECT a.calidad INTO vCalidad FROM pemdboletaloc a, pemmboletaloc b, pemmboleta c
      WHERE a.idboletaloc = b.idboletaloc AND b.idboletaing = c.idboletaing
      AND a.lnkitem = gr_reg.lnkitem
      AND c.idboletaing = gr_reg.idboletaing

      CASE vCalidad
        WHEN 1 LET gr_reg.calidad = "Aprovechamiento"
        WHEN 2 LET gr_reg.calidad = "Mediano"
        WHEN 3 LET gr_reg.calidad = "Devoluci�n"
      END CASE 
    END IF 

    --Para Calidad1
    --DISPLAY "calidad1 ", gr_reg.calidad1
       IF gr_reg.calidad1 = ' ' THEN 
          SELECT nombre, peso
          INTO gr_reg.calidad1, gr_reg.librasxcaja 
          FROM itemsxgrupo
          WHERE lnkitem = gr_reg.lnkitem
       END IF 
       IF gr_reg.calidad1 LIKE '%MERMA%' THEN LET gr_reg.librasxcaja = 0 END IF
--DISPLAY "boleta - item ", gr_reg.idboletaing, "-",gr_reg.lnkitem, "-",gr_reg.producto 
--DISPLAY "semana ", CURRENT 
    --Para semana
      SELECT tb1.sem_com --idsem 
      INTO gr_reg.numSemana
      FROM  dsemanacal tb1, msemanas tb2
      WHERE tb1.lnksem = tb2.lnksem
      AND tb2.periodoactivo = 1 
      AND gr_reg.fecha >= tb1.fec_ini
      AND  gr_reg.fecha <= tb1.fec_fin

   {Let gr_reg.numSemana = numSem(gr_reg.fecha)

    IF gr_reg.estado = -1 THEN
       LET gr_reg.cantCajas = 0
       LET gr_reg.librasxcaja = 0
          
    END IF} 
    
    OUTPUT TO REPORT repBolIng(gr_reg.*, vEstructura)
    
  END FOREACH   

  FINISH REPORT repBolIng  
  CALL fgl_report_stopGraphicalCompatibilityMode()
END FUNCTION 

REPORT repBolIng(lr_reg, lEstructura )
DEFINE lr_reg  RECORD
  idboletaing LIKE pemmboleta.idboletaing,
  tipoBoleta  LIKE pemmboleta.tipoboleta,
  fecha        LIKE pemmboleta.fecha,
  tipoItem     CHAR (1),
  lnkitem      INTEGER ,
  idOrdPro    LIKE pemmboleta.idordpro,
  idDOrdPro   LIKE pemmboleta.iddordpro,
  productor   LIKE pemmboleta.productor,
  tipo         LIKE glbmtip.nombre ,
  
  proveedor    VARCHAR (200),
  finca        VARCHAR (100), --LIKE pemmfinca.fincanomct,
  mercado      VARCHAR (100),
  producto     VARCHAR (100),
  calidad1     VARCHAR (100) ,
  calidad      VARCHAR (100) ,
  cantCajas        INTEGER,
  librasxcaja  SMALLINT,
  totalLibras  INTEGER ,
  precioConLocal VARCHAR (100),     
  precioLocal     VARCHAR (100),    
  precioConDol    VARCHAR (100),    
  precioSinDescCalidad  VARCHAR (100), 
  precioConDescCalidad  VARCHAR (100), 
  totalFlete            VARCHAR (100), 
  costoxLibra           VARCHAR (100), 
  porcxLibra            VARCHAR (100), 
  numSemana             SMALLINT ,    
  anio                  SMALLINT ,    
  numEntrada            VARCHAR (100),
  estado                INTEGER  
  
  {idboletaing LIKE pemmboleta.idboletaing,
  tipoBoleta  LIKE pemmboleta.tipoboleta,
  idOrdPro    LIKE pemmboleta.idordpro,
  idDOrdPro   LIKE pemmboleta.iddordpro,
  productor   LIKE pemmboleta.productor,
  tipo         LIKE glbmtip.nombre,
  fecha        LIKE pemmboleta.fecha,
  proveedor    VARCHAR (200),
  finca        LIKE pemmfinca.fincanomct} 
   
END RECORD
DEFINE lproductor LIKE pemmboleta.productor
DEFINE lpesototal LIKE pemmboleta.pesototal
DEFINE lfecha     LIKE pemmboleta.fecha
DEFINE lcantcajas INT 
DEFINE cproductor LIKE commemp.nombre
DEFINE lEstructura VARCHAR (100)

FORMAT 

FIRST PAGE HEADER 
  PRINT COLUMN 01, "RESUMEN BOLETAS DE RECEPCI�N"
  PRINT COLUMN 01, "Fecha inicial: ", fecini
  PRINT COLUMN 01, "Fecha final:   ", fecfin
  PRINT COLUMN 01, "."
  
  PRINT COLUMN 01, "Tipo",
        COLUMN 15, "Fecha", 
        COLUMN 20, "No. Recepci�n", 
        COLUMN 30, "Proveedor",
        COLUMN 65, "Finca",
        COLUMN 80, "Mercado",
        COLUMN 130, "Producto",
        COLUMN 160, "Calidad 1",
        COLUMN 200, "Calidad",
        COLUMN 220, "Cajas",
        COLUMN 230, "Libras por Caja",
        COLUMN 250, "Total Libras",
        COLUMN 270, "Pr Consig Loc",
        COLUMN 290, "Pr Local",
        COLUMN 310, "Pr Consig $",
        COLUMN 330, "Pr S/desc Cal $",
        COLUMN 350, "Pr C/desc Cal $",
        COLUMN 370, "Total Flete",
        COLUMN 390, "Costo x Libra",
        COLUMN 410, "6% x Libra",
        COLUMN 430, "Semana",
        COLUMN 450, "A�o",
        COLUMN 470, "Emo SAP"
        
        {COLUMN 50, "Cant Cajas",
        COLUMN 60, "Peso Lbs",
        COLUMN 70, "Estructura"}
        
ON EVERY ROW 
  --Productor, pesoLbs, fecha
  SELECT productor, pesoTotal
  INTO lproductor, lpesototal
  FROM pemmboleta
  WHERE idboletaing = lr_reg.idboletaing

  {SELECT nombre INTO cproductor 
  FROM commemp 
  WHERE id_commemp = lproductor}

  --cantCajas
  SELECT SUM (cantCajas) 
  INTO lcantcajas
  FROM pemmboleta a, pemdboleta b
  WHERE a.idboletaing = b.idboletaing
  AND a.idboletaing = lr_reg.idboletaing

  IF (lr_reg.totalLibras IS NULL OR lr_reg.totalLibras = 0) 
   AND lr_reg.proveedor <> "ANULADA" THEN 
   ELSE

   IF lr_reg.precioConLocal IS NULL THEN LET lr_reg.precioConLocal = "-" END IF 
   IF lr_reg.precioLocal IS NULL THEN LET lr_reg.precioLocal = "-" END IF 
   IF lr_reg.precioConDol IS NULL THEN LET lr_reg.precioConDol = "-" END IF 

    
  PRINT COLUMN 01, lr_reg.tipo , 
        COLUMN 15, lr_reg.fecha, 
        COLUMN 20, lr_reg.idboletaing USING "###,##&", 
        COLUMN 30, lr_reg.proveedor,
        COLUMN 65, lr_reg.finca,
        COLUMN 80, lr_reg.mercado,
        COLUMN 130, lr_reg.producto ,
        COLUMN 160, lr_reg.calidad1,
        COLUMN 200, lr_reg.calidad ,
        COLUMN 220, lr_reg.cantCajas,
        COLUMN 230, lr_reg.librasxcaja,
        COLUMN 250, lr_reg.totalLibras,
        COLUMN 270, lr_reg.precioConLocal, 
        COLUMN 290, lr_reg.precioLocal,
        COLUMN 310, lr_reg.precioConDol,
        COLUMN 330, "-",
        COLUMN 350, "-",
        COLUMN 370, "-",
        COLUMN 390, "-",
        COLUMN 410, "-",
        COLUMN 430, lr_reg.numSemana,
        COLUMN 450, YEAR (lr_reg.fecha) USING "####",
        COLUMN 470, lr_reg.numEntrada CLIPPED 
   END IF 
        {COLUMN 50, lcantcajas USING "##,##&", 
        COLUMN 60, lpesototal USING "##,##&", 
        COLUMN 70, lEstructura}
  LET lproductor = NULL 
  LET lpesototal = NULL 
  LET lfecha = NULL 
  LET cproductor = NULL 
  LET lcantcajas = NULL 
END REPORT 
