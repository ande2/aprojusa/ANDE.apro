DATABASE db0001

DEFINE fecIni, fecFin DATE 
DEFINE gr_reg DYNAMIC ARRAY OF RECORD
  idboletaing    LIKE pemmboleta.idboletaing,
  tipoBoleta     LIKE pemmboleta.tipoboleta,
  fecha          LIKE pemmboleta.fecha,
  tipoItem       CHAR (1),
  lnkitem        INTEGER ,
  idOrdPro       LIKE pemmboleta.idordpro,
  idDOrdPro      LIKE pemmboleta.iddordpro,
  productor      LIKE pemmboleta.productor,
  tipo           LIKE glbmtip.nombre ,
  proveedor      VARCHAR (200),
  finca          VARCHAR (100) , --LIKE pemmfinca.fincanomct,
  mercado        VARCHAR (100),
  producto       VARCHAR (100),
  calidad1       VARCHAR (100) ,
  calidad        VARCHAR (100) ,
  cantCajas      INTEGER ,
  librasxcaja    SMALLINT,
  totalLibras    INTEGER ,
  estado         INTEGER ,
  pos01          INTEGER ,
  pos02          INTEGER ,
  pos03          INTEGER ,
  pos04          INTEGER ,
  pos05          INTEGER ,
  pos06          INTEGER ,
  pos07          INTEGER ,
  pos08          INTEGER ,
  pos09          INTEGER ,
  pos10          INTEGER ,
  pos11          INTEGER ,
  pos12          INTEGER ,
  pos13          INTEGER ,
  pos14          INTEGER ,
  pos15          INTEGER ,
  pos16          INTEGER ,
  pos17          INTEGER ,
  pos18          INTEGER ,
  pos19          INTEGER ,
  pos20          INTEGER ,
  pos21          INTEGER ,
  pos22          INTEGER ,
  pos23          INTEGER ,
  pos24          INTEGER ,
  pos25          INTEGER ,
  pos26          INTEGER ,
  pos27          INTEGER ,
  pos28          INTEGER ,
  pos29          INTEGER ,
  pos30          INTEGER ,
  pos31          INTEGER ,
  pos32          INTEGER ,
  pos33          INTEGER ,
  pos34          INTEGER ,
  pos35          INTEGER ,
  pos36          INTEGER ,
  pos37          INTEGER ,
  pos38          INTEGER ,
  pos39          INTEGER ,
  pos40          INTEGER ,
  pos41          INTEGER ,
  pos42          INTEGER ,
  pos43          INTEGER ,
  pos44          INTEGER ,
  pos45          INTEGER ,
  pos46          INTEGER ,
  pos47          INTEGER ,
  pos48          INTEGER ,
  pos49          INTEGER ,
  pos50          INTEGER 
END RECORD 

DEFINE gr_cau DYNAMIC ARRAY OF RECORD 
  causa_nom    VARCHAR (100) ,
  causa_val    INTEGER 
END RECORD

DEFINE gr_tit RECORD
    tit01        VARCHAR (100),
    tit02        VARCHAR (100),
    tit03        VARCHAR (100),
    tit04        VARCHAR (100),
    tit05        VARCHAR (100),
    tit06        VARCHAR (100),
    tit07        VARCHAR (100),
    tit08        VARCHAR (100),
    tit09        VARCHAR (100),
    tit10        VARCHAR (100),
    tit11        VARCHAR (100),
    tit12        VARCHAR (100),
    tit13        VARCHAR (100),
    tit14        VARCHAR (100),
    tit15        VARCHAR (100),
    tit16        VARCHAR (100),
    tit17        VARCHAR (100),
    tit18        VARCHAR (100),
    tit19        VARCHAR (100),
    tit20        VARCHAR (100),
    tit21        VARCHAR (100),
    tit22        VARCHAR (100),
    tit23        VARCHAR (100),
    tit24        VARCHAR (100),
    tit25        VARCHAR (100),
    tit26        VARCHAR (100),
    tit27        VARCHAR (100),
    tit28        VARCHAR (100),
    tit29        VARCHAR (100),
    tit30        VARCHAR (100),
    tit31        VARCHAR (100),
    tit32        VARCHAR (100),
    tit33        VARCHAR (100),
    tit34        VARCHAR (100),
    tit35        VARCHAR (100),
    tit36        VARCHAR (100),
    tit37        VARCHAR (100),
    tit38        VARCHAR (100),
    tit39        VARCHAR (100),
    tit40        VARCHAR (100),
    tit41        VARCHAR (100),
    tit42        VARCHAR (100),
    tit43        VARCHAR (100),
    tit44        VARCHAR (100),
    tit45        VARCHAR (100),
    tit46        VARCHAR (100),
    tit47        VARCHAR (100),
    tit48        VARCHAR (100),
    tit49        VARCHAR (100),
    tit50        VARCHAR (100) 
END RECORD 
DEFINE lTit     VARCHAR (100)

DEFINE i, j SMALLINT 
DEFINE lcausa_nom VARCHAR (100) 
DEFINE lcausa_val SMALLINT 

DEFINE vCalidad SMALLINT 

MAIN
  DEFINE prog_name2 STRING 

  DEFER INTERRUPT 

  LET prog_name2 = "semr0324.log"   -- El progr_name es definido como constante en el arch. globals
    
  CALL STARTLOG(prog_name2)
  CALL main_init()

END MAIN 

FUNCTION main_init()

  CALL ui.Interface.loadActionDefaults("actiondefaults")
  CALL ui.Interface.loadStyles("styles_sc")

  CLOSE WINDOW SCREEN
  OPEN WINDOW w1 WITH FORM "semr0224_form1"

  CALL fgl_settitle("ANDE - Reporte de boletas de Recepci�n")
  
MENU "Boletas de Recepci�n"
  BEFORE MENU 
    CALL pideparam()
    EXIT MENU 
    
  ON ACTION generar
     CALL pideParam()
  ON ACTION salir
    EXIT MENU 
END MENU 

END FUNCTION 

FUNCTION pideParam()
  DEFINE sqlTxt STRING 
  DEFINE vestructura, cproductor, citem, citem1, vdes VARCHAR (100)
  DEFINE vBol, vordpro, vdordpro, vproductor, vitem INTEGER
  DEFINE myHandler om.SaxDocumentHandler
  
  INPUT BY NAME fecIni, fecFin
    {BEFORE INPUT 
      LET fecIni = "07032016"
      LET fecFin = "11032016"
      EXIT INPUT}
    AFTER FIELD fecIni
      IF fecIni IS NULL THEN
         CALL msg("Ingrese fecha de inicio")
         NEXT FIELD fecIni
      END IF

   AFTER FIELD fecFin
      IF fecFin IS NULL THEN
         CALL msg("Ingrese fecha final")
         NEXT FIELD fecFin
      END IF      
  END INPUT 
  
  IF INT_FLAG THEN 
     CALL msg("Reporte cancelado por el usuario")
     RETURN 
  END IF 
--SET EXPLAIN ON 
  --Boletas
  {LET sqlTxt = " SELECT t1.idboletaing, t1.tipoboleta, t1.fecha, 'E', t3.lnkitem, t3.cantCajas, t3.pesototal ",
               " FROM pemmboleta t1, pemmboletaexp t2, pemdboletaexp t3 ", 
               " WHERE t1.idboletaing = t2.idboletaing ",
               " AND t2.idboletaexp = t3.idboletaexp ",
               " AND t1.estado = 0 ",
               " AND fecha BETWEEN '", fecIni, "'", " AND '", fecFin, "'",
               " UNION ",
               " SELECT t1.idboletaing, t1.tipoboleta, t1.fecha, 'L', t3.lnkitem, t3.cantCajas, t3.pesoneto ",
               " FROM pemmboleta t1, pemmboletaloc t2, pemdboletaloc t3 ",
               " WHERE t1.idboletaing = t2.idboletaing ",
               " AND t2.idboletaloc = t3.idboletaloc ",
               " AND t1.estado = 0 ",
               " AND fecha BETWEEN '", fecIni, "'", " AND '", fecFin, "'",
               " ORDER BY 1 "}


  --Para titulos
    DECLARE curtit CURSOR FOR 
      SELECT DISTINCT m.nombre
        FROM pemmcausa m, pemdboletacau d, pemmboletacau e
        WHERE m.idcausa = d.idcausa
        AND d.idboletacau = e.idboletacau
        AND e.idboletaing IN
        (SELECT idboletaing 
           FROM pemmboleta 
           WHERE fecha BETWEEN fecIni AND fecFin)

    LET j = 1
    FOREACH curTit INTO lTit
        CASE j
          WHEN 1  LET gr_tit.tit01 = lTit
          WHEN 2  LET gr_tit.tit02 = lTit
          WHEN 3  LET gr_tit.tit03 = lTit
          WHEN 4  LET gr_tit.tit04 = lTit
          WHEN 5  LET gr_tit.tit05 = lTit
          WHEN 6  LET gr_tit.tit06 = lTit
          WHEN 7  LET gr_tit.tit07 = lTit
          WHEN 8  LET gr_tit.tit08 = lTit
          WHEN 9  LET gr_tit.tit09 = lTit
          WHEN 10 LET gr_tit.tit10 = lTit
          WHEN 11 LET gr_tit.tit11 = lTit
          WHEN 12 LET gr_tit.tit12 = lTit
          WHEN 13 LET gr_tit.tit13 = lTit
          WHEN 14 LET gr_tit.tit14 = lTit
          WHEN 15 LET gr_tit.tit15 = lTit
          WHEN 16 LET gr_tit.tit16 = lTit
          WHEN 17 LET gr_tit.tit17 = lTit
          WHEN 18 LET gr_tit.tit18 = lTit
          WHEN 19 LET gr_tit.tit19 = lTit
          WHEN 20 LET gr_tit.tit20 = lTit
          WHEN 21 LET gr_tit.tit21 = lTit
          WHEN 22 LET gr_tit.tit22 = lTit
          WHEN 23 LET gr_tit.tit23 = lTit
          WHEN 24 LET gr_tit.tit24 = lTit
          WHEN 25 LET gr_tit.tit25 = lTit
          WHEN 26 LET gr_tit.tit26 = lTit
          WHEN 27 LET gr_tit.tit27 = lTit
          WHEN 28 LET gr_tit.tit28 = lTit
          WHEN 29 LET gr_tit.tit29 = lTit
          WHEN 30 LET gr_tit.tit30 = lTit
          WHEN 31 LET gr_tit.tit31 = lTit
          WHEN 32 LET gr_tit.tit32 = lTit
          WHEN 33 LET gr_tit.tit33 = lTit
          WHEN 34 LET gr_tit.tit34 = lTit
          WHEN 35 LET gr_tit.tit35 = lTit
          WHEN 36 LET gr_tit.tit36 = lTit
          WHEN 37 LET gr_tit.tit37 = lTit
          WHEN 38 LET gr_tit.tit38 = lTit
          WHEN 39 LET gr_tit.tit39 = lTit
          WHEN 40 LET gr_tit.tit40 = lTit
          WHEN 41 LET gr_tit.tit41 = lTit
          WHEN 42 LET gr_tit.tit42 = lTit
          WHEN 43 LET gr_tit.tit43 = lTit
          WHEN 44 LET gr_tit.tit44 = lTit
          WHEN 45 LET gr_tit.tit45 = lTit
          WHEN 46 LET gr_tit.tit46 = lTit
          WHEN 47 LET gr_tit.tit47 = lTit
          WHEN 48 LET gr_tit.tit48 = lTit
          WHEN 49 LET gr_tit.tit49 = lTit
          WHEN 50 LET gr_tit.tit50 = lTit
        END CASE 
        LET j = j + 1
    END FOREACH 
     
               

  LET sqlTxt = " SELECT t1.idboletaing, t1.tipoboleta, t1.fecha, 'E', t3.lnkitem, t3.cantCajas, t3.pesototal, ' ', t1.estado ", 
               " FROM pemmboleta t1, OUTER (pemmboletaexp t2, OUTER (pemdboletaexp t3 )) ",
               " WHERE t1.idboletaing = t2.idboletaing ",
               " AND t2.idboletaexp = t3.idboletaexp ",
               " AND t1.estado IN (0,-1) ",
               " AND fecha BETWEEN '", fecIni, "'", " AND '", fecFin, "'",
        --" and t1.idboletaing >= 13133 and t1.idboletaing <= 13145 ",
               " UNION ",
               " SELECT t1.idboletaing, t1.tipoboleta, t1.fecha, 'L', t3.lnkitem, t3.cantCajas, t3.pesoneto, ' ', t1.estado ", 
               " FROM pemmboleta t1, OUTER (pemmboletaloc t2, OUTER (pemdboletaloc t3 )) ",
               " WHERE t1.idboletaing = t2.idboletaing ",
               " AND t2.idboletaloc = t3.idboletaloc ",
               " AND t1.estado IN (0,-1) ",
               " AND fecha BETWEEN '", fecIni, "'", " AND '", fecFin, "'",
        --" and t1.idboletaing >= 13133 and t1.idboletaing <= 13145 ",
               " UNION ",
               " SELECT b.idboletaing, b.tipoboleta, b.fecha, 'E', 1, 0, c.merma, a.nombre, b.estado ", 
               " FROM itemsxgrupo a, pemmboleta b, pemmboletacau c ",
               " WHERE a.nombre LIKE '%MERMA%' ",
               " AND a.iditem = b.iditem ",
               --" AND b.idboletaing = 1 ", lr_reg.idboletaing ,
               " AND b.idboletaing = c.idboletaing ",
               " AND b.fecha BETWEEN '", fecIni, "'", " AND '", fecFin, "'",
       --" and b.idboletaing >= 13133 and b.idboletaing <= 13145 ",        
               " UNION ",
               " SELECT b.idboletaing, b.tipoboleta, b.fecha, 'E', 1, 0, c.merma, i.nombre, b.estado ", 
               " FROM itemsxgrupo i, pemmboleta b, pemdordpro d, pemmboletacau c ",
               " WHERE i.nombre LIKE '%MERMA%' ",
               " AND b.idordpro = d.idordpro ", 
               " AND b.iddordpro = d.iddordpro ",
               " AND d.iditem = i.iditem ",
               " AND b.fecha BETWEEN '", fecIni, "'", " AND '", fecFin, "'",
                --AND b.idboletaing = 1 --", lr_reg.idboletaing
               " AND b.idboletaing = c.idboletaing ",
       --" and b.idboletaing >= 13133 and b.idboletaing <= 13145 ",
               " ORDER BY 1 "
--DISPLAY sqlTxt

               
  {LET sqlTxt = " SELECT idboletaing, tipoBoleta, idOrdPro, idDOrdPro, productor, fecha ",
               " FROM pemmboleta ",
               " WHERE estado in (1,2) AND fecha BETWEEN '", fecIni, "'",
               " AND '", fecFin, "'"}
  PREPARE ex_sql1 FROM sqlTxt
  DECLARE cur01 CURSOR FOR ex_sql1
  LET myHandler = gral_reporte_all("unapagina","horizontal","XLSX",132,"con_r_08")
  START REPORT repBolIng 

  LET i = 1
  FOREACH cur01 INTO gr_reg[i].idboletaing, gr_reg[i].tipoBoleta, gr_reg[i].fecha, 
          gr_reg[i].tipoItem, gr_reg[i].lnkitem, gr_reg[i].cantCajas, gr_reg[i].totalLibras, gr_reg[i].calidad1, gr_reg[i].estado -- gr_reg.tipoBoleta, gr_reg.idOrdPro,
                     --gr_reg.idDOrdPro, gr_reg.productor, gr_reg.fecha
    --DISPLAY "Boleta ", gr_reg.idboletaing, gr_reg.tipoBoleta, gr_reg.idOrdPro,
      --               gr_reg.idDOrdPro, gr_reg.productor
--DISPLAY "datos finca/productor ", CURRENT     
    --Para datos de finca/productor
    SELECT idordpro, iddordpro INTO gr_reg[i].idOrdPro, gr_reg[i].idDOrdPro FROM pemmboleta WHERE idboletaing = gr_reg[i].idboletaing
    IF gr_reg[i].tipoBoleta = 1 THEN
       --Para proveedor
       IF gr_reg[i].estado = -1 THEN
          LET gr_reg[i].proveedor = "ANULADA"
       ELSE
          SELECT fincaNomCt || '-'||nomEstructura || '-'|| NVL(nomValvula,'') || '-' || nombre || '-' || desItemLg 
          INTO gr_reg[i].proveedor
          FROM pemdordpro a, pemmfinca b, mestructura c, glbmtip f, OUTER(pemMValvula d), glbMItems e 
          WHERE a.idfinca = b.idfinca 
          AND a.idestructura = c.idEstructura 
          AND a.idValvula = d.idValvula 
          AND a.idItem = e.idItem 
          AND a.idOrdPro = gr_reg[i].idOrdPro 
          AND a.iddordpro = gr_reg[i].idDOrdPro
          AND e.idtipo = f.idtipo
       END IF

       --DISPLAY "vestructura ", vEstructura 
       --limpia productor
       LET vProductor = NULL 
       LET vItem = NULL 
       --DISPLAY "vProductor ", vProductor
       --DISPLAY "vItem ", vItem 
       
       --Para tipo
       SELECT t1.nombre
       INTO gr_reg[i].tipo
       FROM glbMTip t1, glbmitems t2, pemdordpro t3, pemmboleta t4
       WHERE t1.idtipo = t2.idtipo
       AND t2.iditem = t3.iditem
       AND t3.idordpro = t4.idordpro
       AND t3.iddordpro = t4.iddordpro
       AND t4.idboletaing = gr_reg[i].idboletaing

       --Para finca
       SELECT t1.fincanomct
         INTO gr_reg[i].finca
         FROM pemmfinca t1, pemdordpro t2, pemmboleta t3
         WHERE t1.idfinca = t2.idfinca
         AND t2.idordpro = t3.idordpro
         AND t2.iddordpro = t3.iddordpro
         AND t3.idboletaing = gr_reg[i].idboletaing
--DISPLAY "finca producto ", CURRENT 
       --Para producto
       SELECT trim (b.nombre)||" "|| trim( c.nombre)|| " " || a.desItemLg
         INTO gr_reg[i].producto 
         FROM glbMItems a, glbmfam b, glbmtip c  
        WHERE a.idfamilia = b.idfamilia 
          AND a.idtipo = c.idtipo
          AND a.iditem = 
      (SELECT e.iditem 
        FROM pemmboleta d, pemdordpro e  
       WHERE d.idordpro = e.idordpro 
         AND d.iddordpro = e.iddordpro
         AND d.idboletaing = gr_reg[i].idboletaing)

       --Para numero de entrada
       {LET gr_reg[i].numEntrada = NULL 
       SELECT t1.numentradainvsap
         INTO gr_reg[i].numEntrada
         FROM pemmdatossap t1 
         WHERE t1.idboletaing = gr_reg.idboletaing}

       --Para precios
       {LET gr_reg.precioConLocal = NULL 
       LET gr_reg.precioLocal = NULL 
       SELECT t1.costoart INTO gr_reg.precioConDol FROM pemddatossap t1 
       WHERE t1.idboletaing = gr_reg.idboletaing AND t1.lnkitem = gr_reg.lnkitem}
    ELSE 
       --Para proveedor
       --DISPLAY "===> boleta ", gr_reg.idboletaing, " estado ", gr_reg.estado
       IF gr_reg[i].estado = -1 THEN
          LET gr_reg[i].proveedor = "ANULADA"
       ELSE
          SELECT trim(t1.nombre)
          INTO gr_reg[i].proveedor 
          FROM commemp t1, pemmboleta t2  
          WHERE t1.id_commemp = t2.productor
          AND t2.idboletaing = gr_reg[i].idboletaing
          --DISPLAY "Proveedor ", gr_reg.proveedor
       END IF 
              
       --Para producto / item
       SELECT nombre
       INTO cItem1
       FROM pemmboleta a, glbmitems b, glbmtip c
       WHERE c.idtipo = b.idtipo
       AND b.iditem = a.iditem
       AND a.idboletaing = gr_reg[i].idboletaing

       SELECT FIRST 1 desItemLg
       INTO cItem 
       FROM glbMItems a, pemmboleta c
       WHERE c.idItem = a.idItem 

       --DISPLAY "vItem ", cItem

       --Limpia estructura
       LET vEstructura = cItem1 CLIPPED, "-", cItem 
       --DISPLAY "vEstrctura ", vEstructura
--DISPLAY "productor tipo ", CURRENT 
       --Para tipo
       SELECT t1.nombre
       INTO gr_reg[i].tipo
       FROM glbMTip t1, glbmitems t2, pemmboleta t4
       WHERE t1.idtipo = t2.idtipo
       AND t2.iditem = t4.iditem
       AND t4.idboletaing = gr_reg[i].idboletaing

       --Para finca
       {SELECT t1.nombre
         INTO gr_reg.finca
         FROM commemp t1,pemmboleta t3
         WHERE t3.productor = t1.id_commemp
         AND t3.idboletaing = gr_reg.idboletaing}
       LET gr_reg[i].finca = "Productor"  

       --Para producto
       SELECT trim (b.nombre)||" "|| trim( c.nombre)|| " " || a.desItemLg
         INTO gr_reg[i].producto 
         FROM glbMItems a, glbmfam b, glbmtip c  
        WHERE a.idfamilia = b.idfamilia 
          AND a.idtipo = c.idtipo
          AND a.iditem = 
      (SELECT d.iditem 
        FROM pemmboleta d  
       WHERE d.idboletaing = gr_reg[i].idboletaing)

       --Para numero de entrada
       {LET gr_reg[i].numEntrada = NULL
       SELECT t1.numentmercexpsap
         INTO gr_reg.numEntrada
         FROM pemmdatossap t1 
         WHERE t1.idboletaing = gr_reg.idboletaing
       IF gr_reg.numEntrada IS NULL THEN 
          SELECT t1.numentmerclocsap
         INTO gr_reg.numEntrada
         FROM pemmdatossap t1 
         WHERE t1.idboletaing = gr_reg.idboletaing
       END IF   }

       --Para precios
       {LET gr_reg.precioConDol = NULL 
       LET gr_reg.precioConLocal = NULL 
       LET gr_reg.precioLocal = NULL 
       SELECT t1.costoart INTO gr_reg.precioConDol FROM pemddatossap t1 
       WHERE t1.idboletaing = gr_reg.idboletaing AND t1.lnkitem = gr_reg.lnkitem}  
    END IF
--DISPLAY "condiciones para items de exp y loc ", CURRENT 
    --Separa condiciones para Items de Exportaci�n y Locales
    IF gr_reg[i].tipoItem = "E" THEN --Exportacion
      --Para mercado
      LET gr_reg[i].mercado = "Semillas del Campo - Exportaci�n"
      --Para Calidad
      LET gr_reg[i].calidad = "Aprovechamiento"

    ELSE --Locales  
      --Para mercado
      LET gr_reg[i].mercado = "Semillas del Campo - Mercado Local"

      --Para Calidad
      SELECT a.calidad INTO vCalidad FROM pemdboletaloc a, pemmboletaloc b, pemmboleta c
      WHERE a.idboletaloc = b.idboletaloc AND b.idboletaing = c.idboletaing
      AND a.lnkitem = gr_reg[i].lnkitem
      AND c.idboletaing = gr_reg[i].idboletaing

      CASE vCalidad
        WHEN 1 LET gr_reg[i].calidad = "Aprovechamiento"
        WHEN 2 LET gr_reg[i].calidad = "Mediano"
        WHEN 3 LET gr_reg[i].calidad = "Devoluci�n"
      END CASE 
    END IF 

    --Para Calidad1
    --DISPLAY "calidad1 ", gr_reg.calidad1
       IF gr_reg[i].calidad1 = ' ' THEN 
          SELECT nombre, peso
          INTO gr_reg[i].calidad1, gr_reg[i].librasxcaja 
          FROM itemsxgrupo
          WHERE lnkitem = gr_reg[i].lnkitem
       END IF 
       IF gr_reg[i].calidad1 LIKE '%MERMA%' THEN LET gr_reg[i].librasxcaja = 0 END IF
--DISPLAY "boleta - item ", gr_reg.idboletaing, "-",gr_reg.lnkitem, "-",gr_reg.producto 
--DISPLAY "semana ", CURRENT 
    --Para semana
      {SELECT sem_com --idsem 
      INTO gr_reg.numSemana
      FROM  dsemanacal
      WHERE gr_reg.fecha >= fec_ini
      AND  gr_reg.fecha <= fec_fin}

   --Let gr_reg[i].numSemana = numSem(gr_reg[i].fecha)

    IF gr_reg[i].estado = -1 THEN
       LET gr_reg[i].cantCajas = 0
       LET gr_reg[i].librasxcaja = 0
    END IF 

    
    DECLARE curcau CURSOR FOR 
      SELECT a.nombre, b.porcentaje
        FROM pemMCausa a, pemDBoletaCau b, pemMBoletaCau c
       WHERE gr_reg[i].idboletaing = c.idboletaing
       --WHERE c.idboletaing >= 13133 AND c.idboletaing <= 13135
         AND c.idboletacau = b.idboletacau
         AND b.idcausa = a.idcausa
         ORDER BY 1
    LET j = 1
    CALL gr_cau.clear()     
    FOREACH curcau INTO lcausa_nom, lcausa_val
      LET gr_cau[j].causa_nom = lcausa_nom
      LET gr_cau[j].causa_val = lcausa_val
      --DISPLAY gr_cau[j].*
      LET j = j + 1
    END FOREACH 

    --Para cuadre
    FOR j = 1 TO gr_cau.getLength()
       IF gr_cau[j].causa_nom = gr_tit.tit01 THEN LET gr_reg[i].pos01 = gr_cau[j].causa_val END IF
       IF gr_cau[j].causa_nom = gr_tit.tit02 THEN LET gr_reg[i].pos02 = gr_cau[j].causa_val END IF
       IF gr_cau[j].causa_nom = gr_tit.tit03 THEN LET gr_reg[i].pos03 = gr_cau[j].causa_val END IF
       IF gr_cau[j].causa_nom = gr_tit.tit04 THEN LET gr_reg[i].pos04 = gr_cau[j].causa_val END IF
       IF gr_cau[j].causa_nom = gr_tit.tit05 THEN LET gr_reg[i].pos05 = gr_cau[j].causa_val END IF
       IF gr_cau[j].causa_nom = gr_tit.tit06 THEN LET gr_reg[i].pos06 = gr_cau[j].causa_val END IF
       IF gr_cau[j].causa_nom = gr_tit.tit07 THEN LET gr_reg[i].pos07 = gr_cau[j].causa_val END IF
       IF gr_cau[j].causa_nom = gr_tit.tit08 THEN LET gr_reg[i].pos08 = gr_cau[j].causa_val END IF
       IF gr_cau[j].causa_nom = gr_tit.tit09 THEN LET gr_reg[i].pos09 = gr_cau[j].causa_val END IF
       IF gr_cau[j].causa_nom = gr_tit.tit10 THEN LET gr_reg[i].pos10 = gr_cau[j].causa_val END IF
       IF gr_cau[j].causa_nom = gr_tit.tit11 THEN LET gr_reg[i].pos11 = gr_cau[j].causa_val END IF
       IF gr_cau[j].causa_nom = gr_tit.tit12 THEN LET gr_reg[i].pos12 = gr_cau[j].causa_val END IF
       IF gr_cau[j].causa_nom = gr_tit.tit13 THEN LET gr_reg[i].pos13 = gr_cau[j].causa_val END IF
       IF gr_cau[j].causa_nom = gr_tit.tit14 THEN LET gr_reg[i].pos14 = gr_cau[j].causa_val END IF
       IF gr_cau[j].causa_nom = gr_tit.tit15 THEN LET gr_reg[i].pos15 = gr_cau[j].causa_val END IF
       IF gr_cau[j].causa_nom = gr_tit.tit16 THEN LET gr_reg[i].pos16 = gr_cau[j].causa_val END IF
       IF gr_cau[j].causa_nom = gr_tit.tit17 THEN LET gr_reg[i].pos17 = gr_cau[j].causa_val END IF
       IF gr_cau[j].causa_nom = gr_tit.tit18 THEN LET gr_reg[i].pos18 = gr_cau[j].causa_val END IF
       IF gr_cau[j].causa_nom = gr_tit.tit19 THEN LET gr_reg[i].pos19 = gr_cau[j].causa_val END IF
       IF gr_cau[j].causa_nom = gr_tit.tit20 THEN LET gr_reg[i].pos20 = gr_cau[j].causa_val END IF
       IF gr_cau[j].causa_nom = gr_tit.tit21 THEN LET gr_reg[i].pos21 = gr_cau[j].causa_val END IF
       IF gr_cau[j].causa_nom = gr_tit.tit22 THEN LET gr_reg[i].pos22 = gr_cau[j].causa_val END IF
       IF gr_cau[j].causa_nom = gr_tit.tit23 THEN LET gr_reg[i].pos23 = gr_cau[j].causa_val END IF
       IF gr_cau[j].causa_nom = gr_tit.tit24 THEN LET gr_reg[i].pos24 = gr_cau[j].causa_val END IF
       IF gr_cau[j].causa_nom = gr_tit.tit25 THEN LET gr_reg[i].pos25 = gr_cau[j].causa_val END IF
       IF gr_cau[j].causa_nom = gr_tit.tit26 THEN LET gr_reg[i].pos26 = gr_cau[j].causa_val END IF
       IF gr_cau[j].causa_nom = gr_tit.tit27 THEN LET gr_reg[i].pos27 = gr_cau[j].causa_val END IF
       IF gr_cau[j].causa_nom = gr_tit.tit28 THEN LET gr_reg[i].pos28 = gr_cau[j].causa_val END IF
       IF gr_cau[j].causa_nom = gr_tit.tit29 THEN LET gr_reg[i].pos29 = gr_cau[j].causa_val END IF
       IF gr_cau[j].causa_nom = gr_tit.tit30 THEN LET gr_reg[i].pos30 = gr_cau[j].causa_val END IF
       IF gr_cau[j].causa_nom = gr_tit.tit31 THEN LET gr_reg[i].pos31 = gr_cau[j].causa_val END IF
       IF gr_cau[j].causa_nom = gr_tit.tit32 THEN LET gr_reg[i].pos32 = gr_cau[j].causa_val END IF
       IF gr_cau[j].causa_nom = gr_tit.tit33 THEN LET gr_reg[i].pos33 = gr_cau[j].causa_val END IF
       IF gr_cau[j].causa_nom = gr_tit.tit34 THEN LET gr_reg[i].pos34 = gr_cau[j].causa_val END IF
       IF gr_cau[j].causa_nom = gr_tit.tit35 THEN LET gr_reg[i].pos35 = gr_cau[j].causa_val END IF
       IF gr_cau[j].causa_nom = gr_tit.tit36 THEN LET gr_reg[i].pos36 = gr_cau[j].causa_val END IF
       IF gr_cau[j].causa_nom = gr_tit.tit37 THEN LET gr_reg[i].pos37 = gr_cau[j].causa_val END IF
       IF gr_cau[j].causa_nom = gr_tit.tit38 THEN LET gr_reg[i].pos38 = gr_cau[j].causa_val END IF
       IF gr_cau[j].causa_nom = gr_tit.tit39 THEN LET gr_reg[i].pos39 = gr_cau[j].causa_val END IF
       IF gr_cau[j].causa_nom = gr_tit.tit40 THEN LET gr_reg[i].pos40 = gr_cau[j].causa_val END IF
       IF gr_cau[j].causa_nom = gr_tit.tit41 THEN LET gr_reg[i].pos41 = gr_cau[j].causa_val END IF
       IF gr_cau[j].causa_nom = gr_tit.tit42 THEN LET gr_reg[i].pos42 = gr_cau[j].causa_val END IF
       IF gr_cau[j].causa_nom = gr_tit.tit43 THEN LET gr_reg[i].pos43 = gr_cau[j].causa_val END IF
       IF gr_cau[j].causa_nom = gr_tit.tit44 THEN LET gr_reg[i].pos44 = gr_cau[j].causa_val END IF
       IF gr_cau[j].causa_nom = gr_tit.tit45 THEN LET gr_reg[i].pos45 = gr_cau[j].causa_val END IF
       IF gr_cau[j].causa_nom = gr_tit.tit46 THEN LET gr_reg[i].pos46 = gr_cau[j].causa_val END IF
       IF gr_cau[j].causa_nom = gr_tit.tit47 THEN LET gr_reg[i].pos47 = gr_cau[j].causa_val END IF
       IF gr_cau[j].causa_nom = gr_tit.tit48 THEN LET gr_reg[i].pos48 = gr_cau[j].causa_val END IF
       IF gr_cau[j].causa_nom = gr_tit.tit49 THEN LET gr_reg[i].pos49 = gr_cau[j].causa_val END IF
       IF gr_cau[j].causa_nom = gr_tit.tit50 THEN LET gr_reg[i].pos50 = gr_cau[j].causa_val END IF
    END FOR  
    --DISPLAY "Reporte ", gr_reg[i].*  --idboletaing, "-", gr_reg[i].causa01_nom, gr_reg[i].causa01_val
    OUTPUT TO REPORT repBolIng(gr_tit.*, gr_reg[i].*, vEstructura)

    LET i = i + 1
    
  END FOREACH   

  FINISH REPORT repBolIng  
  CALL fgl_report_stopGraphicalCompatibilityMode()
END FUNCTION 

REPORT repBolIng(lr_tit, lr_reg, lEstructura )
DEFINE lr_reg RECORD
  idboletaing    LIKE pemmboleta.idboletaing,
  tipoBoleta     LIKE pemmboleta.tipoboleta,
  fecha          LIKE pemmboleta.fecha,
  tipoItem       CHAR (1),
  lnkitem        INTEGER ,
  idOrdPro       LIKE pemmboleta.idordpro,
  idDOrdPro      LIKE pemmboleta.iddordpro,
  productor      LIKE pemmboleta.productor,
  tipo           LIKE glbmtip.nombre ,
  proveedor      VARCHAR (200),
  finca          VARCHAR (100) , --LIKE pemmfinca.fincanomct,
  mercado        VARCHAR (100),
  producto       VARCHAR (100),
  calidad1       VARCHAR (100) ,
  calidad        VARCHAR (100) ,
  cantCajas      INTEGER ,
  librasxcaja    SMALLINT,
  totalLibras    INTEGER ,
  estado         INTEGER,
  pos01          INTEGER ,
  pos02          INTEGER ,
  pos03          INTEGER ,
  pos04          INTEGER ,
  pos05          INTEGER ,
  pos06          INTEGER ,
  pos07          INTEGER ,
  pos08          INTEGER ,
  pos09          INTEGER ,
  pos10          INTEGER ,
  pos11          INTEGER ,
  pos12          INTEGER ,
  pos13          INTEGER ,
  pos14          INTEGER ,
  pos15          INTEGER ,
  pos16          INTEGER ,
  pos17          INTEGER ,
  pos18          INTEGER ,
  pos19          INTEGER ,
  pos20          INTEGER ,
  pos21          INTEGER ,
  pos22          INTEGER ,
  pos23          INTEGER ,
  pos24          INTEGER ,
  pos25          INTEGER ,
  pos26          INTEGER ,
  pos27          INTEGER ,
  pos28          INTEGER ,
  pos29          INTEGER ,
  pos30          INTEGER ,
  pos31          INTEGER ,
  pos32          INTEGER ,
  pos33          INTEGER ,
  pos34          INTEGER ,
  pos35          INTEGER ,
  pos36          INTEGER ,
  pos37          INTEGER ,
  pos38          INTEGER ,
  pos39          INTEGER ,
  pos40          INTEGER ,
  pos41          INTEGER ,
  pos42          INTEGER ,
  pos43          INTEGER ,
  pos44          INTEGER ,
  pos45          INTEGER ,
  pos46          INTEGER ,
  pos47          INTEGER ,
  pos48          INTEGER ,
  pos49          INTEGER ,
  pos50          INTEGER
END RECORD  

DEFINE lr_tit RECORD
    tit01        VARCHAR (100),
    tit02        VARCHAR (100),
    tit03        VARCHAR (100),
    tit04        VARCHAR (100),
    tit05        VARCHAR (100),
    tit06        VARCHAR (100),
    tit07        VARCHAR (100),
    tit08        VARCHAR (100),
    tit09        VARCHAR (100),
    tit10        VARCHAR (100),
    tit11        VARCHAR (100),
    tit12        VARCHAR (100),
    tit13        VARCHAR (100),
    tit14        VARCHAR (100),
    tit15        VARCHAR (100),
    tit16        VARCHAR (100),
    tit17        VARCHAR (100),
    tit18        VARCHAR (100),
    tit19        VARCHAR (100),
    tit20        VARCHAR (100),
    tit21        VARCHAR (100),
    tit22        VARCHAR (100),
    tit23        VARCHAR (100),
    tit24        VARCHAR (100),
    tit25        VARCHAR (100),
    tit26        VARCHAR (100),
    tit27        VARCHAR (100),
    tit28        VARCHAR (100),
    tit29        VARCHAR (100),
    tit30        VARCHAR (100),
    tit31        VARCHAR (100),
    tit32        VARCHAR (100),
    tit33        VARCHAR (100),
    tit34        VARCHAR (100),
    tit35        VARCHAR (100),
    tit36        VARCHAR (100),
    tit37        VARCHAR (100),
    tit38        VARCHAR (100),
    tit39        VARCHAR (100),
    tit40        VARCHAR (100),
    tit41        VARCHAR (100),
    tit42        VARCHAR (100),
    tit43        VARCHAR (100),
    tit44        VARCHAR (100),
    tit45        VARCHAR (100),
    tit46        VARCHAR (100),
    tit47        VARCHAR (100),
    tit48        VARCHAR (100),
    tit49        VARCHAR (100),
    tit50        VARCHAR (100)
END RECORD 
DEFINE lproductor LIKE pemmboleta.productor
DEFINE lpesototal LIKE pemmboleta.pesototal
DEFINE lfecha     LIKE pemmboleta.fecha
DEFINE lcantcajas INT 
DEFINE cproductor LIKE commemp.nombre
DEFINE lEstructura VARCHAR (100)

FORMAT 

FIRST PAGE HEADER 
  PRINT COLUMN 01, "RESUMEN BOLETAS DE RECEPCI�N"
  PRINT COLUMN 01, "Fecha inicial: ", fecini
  PRINT COLUMN 01, "Fecha final:   ", fecfin
  PRINT COLUMN 01, "."
  
  PRINT COLUMN 01, "Tipo",
        COLUMN 15, "Fecha", 
        COLUMN 20, "No. Recepci�n", 
        COLUMN 30, "Proveedor",
        COLUMN 65, "Finca",
        COLUMN 80, "Mercado",
        COLUMN 130, "Producto",
        COLUMN 160, "Calidad 1",
        COLUMN 200, "Calidad",
        COLUMN 220, "Cajas",
        COLUMN 230, "Libras por Caja",
        COLUMN 250, "Total Libras",
        COLUMN 270, lr_tit.tit01 CLIPPED ,
        COLUMN 300, lr_tit.tit02 CLIPPED ,
        COLUMN 330, lr_tit.tit03 CLIPPED ,
        COLUMN 360, lr_tit.tit04 CLIPPED ,
        COLUMN 390, lr_tit.tit05 CLIPPED ,
        COLUMN 420, lr_tit.tit06 CLIPPED ,
        COLUMN 450, lr_tit.tit07 CLIPPED ,
        COLUMN 480, lr_tit.tit08 CLIPPED ,
        COLUMN 510, lr_tit.tit09 CLIPPED ,
        COLUMN 540, lr_tit.tit10 CLIPPED ,
        COLUMN 570, lr_tit.tit11 CLIPPED ,
        COLUMN 600, lr_tit.tit12 CLIPPED ,
        COLUMN 630, lr_tit.tit13 CLIPPED ,
        COLUMN 660, lr_tit.tit14 CLIPPED ,
        COLUMN 690, lr_tit.tit15 CLIPPED ,
        COLUMN 720, lr_tit.tit16 CLIPPED ,
        COLUMN 750, lr_tit.tit17 CLIPPED ,
        COLUMN 780, lr_tit.tit18 CLIPPED ,
        COLUMN 810, lr_tit.tit19 CLIPPED ,
        COLUMN 840, lr_tit.tit20 CLIPPED ,
        COLUMN 870, lr_tit.tit21 CLIPPED ,
        COLUMN 900, lr_tit.tit22 CLIPPED ,
        COLUMN 930, lr_tit.tit23 CLIPPED ,
        COLUMN 960, lr_tit.tit24 CLIPPED ,
        COLUMN 990, lr_tit.tit25 CLIPPED ,
        COLUMN 1020, lr_tit.tit26 CLIPPED ,
        COLUMN 1050, lr_tit.tit27 CLIPPED ,
        COLUMN 1080, lr_tit.tit28 CLIPPED ,
        COLUMN 1110, lr_tit.tit29 CLIPPED ,
        COLUMN 1140, lr_tit.tit30 CLIPPED ,
        COLUMN 1170, lr_tit.tit31 CLIPPED ,
        COLUMN 1200, lr_tit.tit32 CLIPPED ,
        COLUMN 1230, lr_tit.tit33 CLIPPED ,
        COLUMN 1260, lr_tit.tit34 CLIPPED ,
        COLUMN 1290, lr_tit.tit35 CLIPPED ,
        COLUMN 1320, lr_tit.tit36 CLIPPED ,
        COLUMN 1350, lr_tit.tit37 CLIPPED ,
        COLUMN 1380, lr_tit.tit38 CLIPPED ,
        COLUMN 1410, lr_tit.tit39 CLIPPED ,
        COLUMN 1440, lr_tit.tit40 CLIPPED ,
        COLUMN 1470, lr_tit.tit41 CLIPPED ,
        COLUMN 1500, lr_tit.tit42 CLIPPED ,
        COLUMN 1530, lr_tit.tit43 CLIPPED ,
        COLUMN 1560, lr_tit.tit44 CLIPPED ,
        COLUMN 1590, lr_tit.tit45 CLIPPED ,
        COLUMN 1620, lr_tit.tit46 CLIPPED ,
        COLUMN 1650, lr_tit.tit47 CLIPPED ,
        COLUMN 1680, lr_tit.tit48 CLIPPED ,
        COLUMN 1710, lr_tit.tit49 CLIPPED ,
        COLUMN 1740, lr_tit.tit50 CLIPPED 
        
        {COLUMN 50, "Cant Cajas",
        COLUMN 60, "Peso Lbs",
        COLUMN 70, "Estructura"}
        
ON EVERY ROW 
  --Productor, pesoLbs, fecha
  SELECT productor, pesoTotal
  INTO lproductor, lpesototal
  FROM pemmboleta
  WHERE idboletaing = lr_reg.idboletaing

  {SELECT nombre INTO cproductor 
  FROM commemp 
  WHERE id_commemp = lproductor}

  --cantCajas
  SELECT SUM (cantCajas) 
  INTO lcantcajas
  FROM pemmboleta a, pemdboleta b
  WHERE a.idboletaing = b.idboletaing
  AND a.idboletaing = lr_reg.idboletaing

  IF (lr_reg.totalLibras IS NULL OR lr_reg.totalLibras = 0) 
   AND lr_reg.proveedor <> "ANULADA" THEN 
   ELSE

   {IF lr_reg.precioConLocal IS NULL THEN LET lr_reg.precioConLocal = "-" END IF 
   IF lr_reg.precioLocal IS NULL THEN LET lr_reg.precioLocal = "-" END IF 
   IF lr_reg.precioConDol IS NULL THEN LET lr_reg.precioConDol = "-" END IF} 

   IF lr_tit.tit01 IS NOT NULL THEN IF lr_reg.pos01 IS NULL THEN LET lr_reg.pos01 = 0 END IF END IF  
   IF lr_tit.tit02 IS NOT NULL THEN IF lr_reg.pos02 IS NULL THEN LET lr_reg.pos02 = 0 END IF END IF  
   IF lr_tit.tit03 IS NOT NULL THEN IF lr_reg.pos03 IS NULL THEN LET lr_reg.pos03 = 0 END IF END IF   
   IF lr_tit.tit04 IS NOT NULL THEN IF lr_reg.pos04 IS NULL THEN LET lr_reg.pos04 = 0 END IF END IF  
   IF lr_tit.tit05 IS NOT NULL THEN IF lr_reg.pos05 IS NULL THEN LET lr_reg.pos05 = 0 END IF END IF   
   IF lr_tit.tit06 IS NOT NULL THEN IF lr_reg.pos06 IS NULL THEN LET lr_reg.pos06 = 0 END IF END IF   
   IF lr_tit.tit07 IS NOT NULL THEN IF lr_reg.pos07 IS NULL THEN LET lr_reg.pos07 = 0 END IF END IF   
   IF lr_tit.tit08 IS NOT NULL THEN IF lr_reg.pos08 IS NULL THEN LET lr_reg.pos08 = 0 END IF END IF  
   IF lr_tit.tit09 IS NOT NULL THEN IF lr_reg.pos09 IS NULL THEN LET lr_reg.pos09 = 0 END IF END IF   
   IF lr_tit.tit10 IS NOT NULL THEN IF lr_reg.pos10 IS NULL THEN LET lr_reg.pos10 = 0 END IF END IF   
   IF lr_tit.tit11 IS NOT NULL THEN IF lr_reg.pos11 IS NULL THEN LET lr_reg.pos11 = 0 END IF END IF   
   IF lr_tit.tit12 IS NOT NULL THEN IF lr_reg.pos12 IS NULL THEN LET lr_reg.pos12 = 0 END IF END IF  
   IF lr_tit.tit13 IS NOT NULL THEN IF lr_reg.pos13 IS NULL THEN LET lr_reg.pos13 = 0 END IF END IF   
   IF lr_tit.tit14 IS NOT NULL THEN IF lr_reg.pos14 IS NULL THEN LET lr_reg.pos14 = 0 END IF END IF   
   IF lr_tit.tit15 IS NOT NULL THEN IF lr_reg.pos15 IS NULL THEN LET lr_reg.pos15 = 0 END IF END IF   
   IF lr_tit.tit16 IS NOT NULL THEN IF lr_reg.pos16 IS NULL THEN LET lr_reg.pos16 = 0 END IF END IF  
   IF lr_tit.tit17 IS NOT NULL THEN IF lr_reg.pos17 IS NULL THEN LET lr_reg.pos17 = 0 END IF END IF  
   IF lr_tit.tit18 IS NOT NULL THEN IF lr_reg.pos18 IS NULL THEN LET lr_reg.pos18 = 0 END IF END IF  
   IF lr_tit.tit19 IS NOT NULL THEN IF lr_reg.pos19 IS NULL THEN LET lr_reg.pos19 = 0 END IF END IF  
   IF lr_tit.tit20 IS NOT NULL THEN IF lr_reg.pos20 IS NULL THEN LET lr_reg.pos20 = 0 END IF END IF  
   IF lr_tit.tit21 IS NOT NULL THEN IF lr_reg.pos21 IS NULL THEN LET lr_reg.pos21 = 0 END IF END IF  
   IF lr_tit.tit22 IS NOT NULL THEN IF lr_reg.pos22 IS NULL THEN LET lr_reg.pos22 = 0 END IF END IF  
   IF lr_tit.tit23 IS NOT NULL THEN IF lr_reg.pos23 IS NULL THEN LET lr_reg.pos23 = 0 END IF END IF  
   IF lr_tit.tit24 IS NOT NULL THEN IF lr_reg.pos24 IS NULL THEN LET lr_reg.pos24 = 0 END IF END IF  
   IF lr_tit.tit25 IS NOT NULL THEN IF lr_reg.pos25 IS NULL THEN LET lr_reg.pos25 = 0 END IF END IF
   IF lr_tit.tit26 IS NOT NULL THEN IF lr_reg.pos26 IS NULL THEN LET lr_reg.pos26 = 0 END IF END IF  
   IF lr_tit.tit27 IS NOT NULL THEN IF lr_reg.pos27 IS NULL THEN LET lr_reg.pos27 = 0 END IF END IF  
   IF lr_tit.tit28 IS NOT NULL THEN IF lr_reg.pos28 IS NULL THEN LET lr_reg.pos28 = 0 END IF END IF   
   IF lr_tit.tit29 IS NOT NULL THEN IF lr_reg.pos29 IS NULL THEN LET lr_reg.pos29 = 0 END IF END IF  
   IF lr_tit.tit30 IS NOT NULL THEN IF lr_reg.pos30 IS NULL THEN LET lr_reg.pos30 = 0 END IF END IF   
   IF lr_tit.tit31 IS NOT NULL THEN IF lr_reg.pos31 IS NULL THEN LET lr_reg.pos31 = 0 END IF END IF   
   IF lr_tit.tit32 IS NOT NULL THEN IF lr_reg.pos32 IS NULL THEN LET lr_reg.pos32 = 0 END IF END IF   
   IF lr_tit.tit33 IS NOT NULL THEN IF lr_reg.pos33 IS NULL THEN LET lr_reg.pos33 = 0 END IF END IF  
   IF lr_tit.tit34 IS NOT NULL THEN IF lr_reg.pos34 IS NULL THEN LET lr_reg.pos34 = 0 END IF END IF   
   IF lr_tit.tit35 IS NOT NULL THEN IF lr_reg.pos35 IS NULL THEN LET lr_reg.pos35 = 0 END IF END IF   
   IF lr_tit.tit36 IS NOT NULL THEN IF lr_reg.pos36 IS NULL THEN LET lr_reg.pos36 = 0 END IF END IF   
   IF lr_tit.tit37 IS NOT NULL THEN IF lr_reg.pos37 IS NULL THEN LET lr_reg.pos37 = 0 END IF END IF  
   IF lr_tit.tit38 IS NOT NULL THEN IF lr_reg.pos38 IS NULL THEN LET lr_reg.pos38 = 0 END IF END IF   
   IF lr_tit.tit39 IS NOT NULL THEN IF lr_reg.pos39 IS NULL THEN LET lr_reg.pos39 = 0 END IF END IF   
   IF lr_tit.tit40 IS NOT NULL THEN IF lr_reg.pos40 IS NULL THEN LET lr_reg.pos40 = 0 END IF END IF   
   IF lr_tit.tit41 IS NOT NULL THEN IF lr_reg.pos41 IS NULL THEN LET lr_reg.pos41 = 0 END IF END IF  
   IF lr_tit.tit42 IS NOT NULL THEN IF lr_reg.pos42 IS NULL THEN LET lr_reg.pos42 = 0 END IF END IF  
   IF lr_tit.tit43 IS NOT NULL THEN IF lr_reg.pos43 IS NULL THEN LET lr_reg.pos43 = 0 END IF END IF  
   IF lr_tit.tit44 IS NOT NULL THEN IF lr_reg.pos44 IS NULL THEN LET lr_reg.pos44 = 0 END IF END IF  
   IF lr_tit.tit45 IS NOT NULL THEN IF lr_reg.pos45 IS NULL THEN LET lr_reg.pos45 = 0 END IF END IF  
   IF lr_tit.tit46 IS NOT NULL THEN IF lr_reg.pos46 IS NULL THEN LET lr_reg.pos46 = 0 END IF END IF  
   IF lr_tit.tit47 IS NOT NULL THEN IF lr_reg.pos47 IS NULL THEN LET lr_reg.pos47 = 0 END IF END IF  
   IF lr_tit.tit48 IS NOT NULL THEN IF lr_reg.pos48 IS NULL THEN LET lr_reg.pos48 = 0 END IF END IF  
   IF lr_tit.tit49 IS NOT NULL THEN IF lr_reg.pos49 IS NULL THEN LET lr_reg.pos49 = 0 END IF END IF  
   IF lr_tit.tit50 IS NOT NULL THEN IF lr_reg.pos50 IS NULL THEN LET lr_reg.pos50 = 0 END IF END IF  
   
  PRINT COLUMN 01, lr_reg.tipo , 
        COLUMN 15, lr_reg.fecha, 
        COLUMN 20, lr_reg.idboletaing USING "###,##&", 
        COLUMN 30, lr_reg.proveedor,
        COLUMN 65, lr_reg.finca,
        COLUMN 80, lr_reg.mercado,
        COLUMN 130, lr_reg.producto ,
        COLUMN 160, lr_reg.calidad1,
        COLUMN 200, lr_reg.calidad ,
        COLUMN 220, lr_reg.cantCajas,
        COLUMN 230, lr_reg.librasxcaja,
        COLUMN 250, lr_reg.totalLibras,
        COLUMN 270, lr_reg.pos01 , 
        COLUMN 300, lr_reg.pos02 ,
        COLUMN 330, lr_reg.pos03 ,
        COLUMN 360, lr_reg.pos04 ,
        COLUMN 390, lr_reg.pos05 ,
        COLUMN 420, lr_reg.pos06 ,
        COLUMN 450, lr_reg.pos07 ,
        COLUMN 480, lr_reg.pos08 ,
        COLUMN 510, lr_reg.pos09 ,
        COLUMN 540, lr_reg.pos10 ,
        COLUMN 570, lr_reg.pos11 ,
        COLUMN 600, lr_reg.pos12 ,
        COLUMN 630, lr_reg.pos13 ,
        COLUMN 660, lr_reg.pos14 ,
        COLUMN 690, lr_reg.pos15 ,
        COLUMN 720, lr_reg.pos16 ,
        COLUMN 750, lr_reg.pos17 ,
        COLUMN 780, lr_reg.pos18 ,
        COLUMN 810, lr_reg.pos19 ,
        COLUMN 840, lr_reg.pos20 ,
        COLUMN 870, lr_reg.pos21 ,
        COLUMN 900, lr_reg.pos22 ,
        COLUMN 930, lr_reg.pos23 ,
        COLUMN 960, lr_reg.pos24 ,
        COLUMN 990, lr_reg.pos25 ,
        COLUMN 1020, lr_reg.pos26 , 
        COLUMN 1050, lr_reg.pos27 ,
        COLUMN 1080, lr_reg.pos28 ,
        COLUMN 1110, lr_reg.pos29 ,
        COLUMN 1140, lr_reg.pos30 ,
        COLUMN 1170, lr_reg.pos31 ,
        COLUMN 1200, lr_reg.pos32 ,
        COLUMN 1230, lr_reg.pos33 ,
        COLUMN 1260, lr_reg.pos34 ,
        COLUMN 1290, lr_reg.pos35 ,
        COLUMN 1320, lr_reg.pos36 ,
        COLUMN 1350, lr_reg.pos37 ,
        COLUMN 1380, lr_reg.pos38 ,
        COLUMN 1410, lr_reg.pos39 ,
        COLUMN 1440, lr_reg.pos40 ,
        COLUMN 1470, lr_reg.pos41 ,
        COLUMN 1500, lr_reg.pos42 ,
        COLUMN 1530, lr_reg.pos43 ,
        COLUMN 1560, lr_reg.pos44 ,
        COLUMN 1590, lr_reg.pos45 ,
        COLUMN 1620, lr_reg.pos46 ,
        COLUMN 1650, lr_reg.pos47 ,
        COLUMN 1680, lr_reg.pos48 ,
        COLUMN 1710, lr_reg.pos49 ,
        COLUMN 1740, lr_reg.pos50 
   END IF 
        {COLUMN 50, lcantcajas USING "##,##&", 
        COLUMN 60, lpesototal USING "##,##&", 
        COLUMN 70, lEstructura}
  LET lproductor = NULL 
  LET lpesototal = NULL 
  LET lfecha = NULL 
  LET cproductor = NULL 
  LET lcantcajas = NULL 
END REPORT 
