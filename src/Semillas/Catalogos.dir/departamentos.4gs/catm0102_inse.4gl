################################################################################
# Funcion     : %M%
# Descripcion : Ingreso de un nuevo umdor 	
# Funciones   : input_umd() funcion para insertar datos
#              
#              
#               
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Anderson Garcia 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################

GLOBALS "catm0102_glob.4gl"

FUNCTION insert_init()
DEFINE strSql STRING 

   LET strSql =
      "INSERT INTO commdep ( ",
        " id_commdep, descripcion, hijo_de, estado ",
        "   ) ",
      " VALUES (?,?,?,?)"

   PREPARE st_insertar FROM strSql

END FUNCTION 

FUNCTION ingreso()
   CALL encabezado("Ingresar")
   --CALL info_usuario()
   IF captura_datos('I') THEN
      RETURN grabar()
   ELSE 
      RETURN FALSE 
   END IF 
END FUNCTION 

FUNCTION grabar()
   LET g_reg.id_commdep = 0
   LET g_reg.estado = 1
   TRY 
      EXECUTE st_insertar USING 
            g_reg.id_commdep, g_reg.descripcion, g_reg.hijo_de, g_reg.estado

      --CS agregarlo cuando se agregue el ID 
      LET g_reg.id_commdep = SQLCA.sqlerrd[2]
   CATCH 
      CALL msgError(sqlca.sqlcode,"Grabar Registro")
      RETURN FALSE 
   END TRY
   DISPLAY BY NAME g_reg.*
   CALL box_valdato ("Registro agregado")
   CALL combo_din2("cmbdepartamento","SELECT * FROM commdep WHERE id_commdep > 0")
   --CALL combo_din2("jefe",'SELECT id_commempl, TRIM(nombre) || " "|| TRIM(apellido) FROM commempl WHERE id_commempl > 0')
   RETURN TRUE 
END FUNCTION 