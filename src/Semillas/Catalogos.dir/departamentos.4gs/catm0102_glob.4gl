################################################################################
# Funcion     : %M%
# Descripcion : Catalogo de Empresas
#               Funcion para definicion de globales
# Funciones   : 
#               
#               
#  
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################
SCHEMA db0001

GLOBALS 
TYPE 
   tDet RECORD 
      id_commdep       LIKE commdep.id_commdep,
      descripcion      LIKE commdep.descripcion,
      hijo_de          LIKE commdep.hijo_de,
      estado           LIKE commdep.estado
   END RECORD

DEFINE
   reg_det DYNAMIC ARRAY OF tDet, 
   g_reg, u_reg tDet,
   dbname      STRING,
   
   condicion   STRING --Condicion de la clausula Where 
   CONSTANT    prog_name = "comm0102"
END GLOBALS