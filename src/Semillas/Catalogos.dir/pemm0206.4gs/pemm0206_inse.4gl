################################################################################
# Funcion     : %M%
# Descripcion : Ingreso de un registro nuevo 	
# Funciones   : input_umd() funcion para insertar datos
#              
#              
#               
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Anderson Garcia 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################

GLOBALS "pemm0206_glob.4gl"

FUNCTION insert_init()
DEFINE strSql STRING 

   LET strSql =
      "INSERT INTO pemMOrdPro ( ",
        " idOrdPro, numOrdPro, nomOrdPro, idItemSap, cueAfe, costostdprod, ",
        " fecInicio, fecFinal, ",
        --" idFinca, idEstructura, idValvula, idItem, estado ",
        " estado ",
        "   ) ",
      " VALUES (?,?,?,?,?,?,?,?,?)"

   PREPARE st_insertar FROM strSql

   LET strSql =
      "INSERT INTO pemDOrdPro ( ",
        " idOrdPro, idDOrdPro, idFinca, idEstructura, idValvula, idItem ",
        "   ) ",
      " VALUES (?,?,?,?,?,?)"

   PREPARE st_insertarDet FROM strSql

END FUNCTION 

FUNCTION ingreso()
   CALL encabezado("Ingresar")
   --CALL info_usuario()
   IF captura_datos('I') THEN
      RETURN grabar()
   ELSE 
      RETURN FALSE 
   END IF 
END FUNCTION 

FUNCTION grabar()
   DEFINE i SMALLINT 
   LET g_reg.idOrdPro = 0

   WHENEVER ERROR CONTINUE
   BEGIN WORK 
   TRY 
      EXECUTE st_insertar USING 
        g_reg.idOrdPro, g_reg.numOrdPro, g_reg.nomOrdPro, 
        g_reg.idItemSap, g_reg.cueafe, g_reg.costostdprod,
        g_reg.fecInicio, g_reg.fecFinal, --g_reg.idFinca,
        --g_reg.idEstructura, g_reg.idValvula, g_reg.idItem,
        g_reg.estado
            
      --CS agregarlo cuando se agregue el ID 
      LET g_reg.idOrdPro = SQLCA.sqlerrd[2]
      TRY 
         FOR i= 1 TO reg_detArr.getLength()
            LET reg_detArr[i].idOrdPro = g_reg.idOrdPro
            EXECUTE st_insertarDet USING 
              reg_detArr[i].idOrdPro, reg_detArr[i].idDOrdPro,
              reg_detArr[i].idFinca, reg_detArr[i].idEstructura,
              reg_detArr[i].idValvula, reg_detArr[i].idItem 
         END FOR 
      CATCH 
         ROLLBACK WORK 
         WHENEVER ERROR STOP
         CALL msgError(sqlca.sqlcode,"Grabar Detalle de Registro")
         RETURN FALSE 
      END TRY
   CATCH 
      ROLLBACK WORK 
      WHENEVER ERROR STOP
      CALL msgError(sqlca.sqlcode,"Grabar Registro")
      RETURN FALSE 
   END TRY
   COMMIT WORK 
   WHENEVER ERROR STOP
   DISPLAY BY NAME g_reg.*
   CALL box_valdato ("Registro agregado")
   --CALL combo_din2("cmbdepartamento","SELECT * FROM commdep WHERE id_commdep > 0")
   RETURN TRUE 
END FUNCTION 