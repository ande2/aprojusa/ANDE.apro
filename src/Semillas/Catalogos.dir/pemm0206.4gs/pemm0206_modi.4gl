################################################################################
# Funcion     : %M%
# Descripcion : Funcion para modificar registros
# Funciones   : updel_init()
#	             delete_umd() 
#               update_umd()
#               pk_estado()
#               valida_modi()
#               verifica_modi()
#               existe_datos()
#               existe_dpro()
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################
GLOBALS "pemm0206_glob.4gl"

FUNCTION update_init()
DEFINE strSql STRING 

   LET strSql = 
      "UPDATE pemMOrdPro ",
      "SET ",
      " numOrdPro       = ?, ",
      " nomOrdPro       = ?, ",
      " idItemSap       = ?, ",
      " cueAfe          = ?, ",
      " costostdprod    = ?, ",
      " fecInicio       = ?, ",
      " fecFinal        = ?, ",
      " estado          = ? ",
      " WHERE idOrdPro  = ? "
      
   PREPARE st_modificar FROM strSql

   LET strSql =
      "INSERT INTO pemDOrdPro ( ",
        " idOrdPro, idDOrdPro, idFinca, idEstructura, idValvula, idItem ",
        "   ) ",
      " VALUES (?,?,?,?,?,?)"

   PREPARE st_insertarDetMod FROM strSql

END FUNCTION 

FUNCTION modifica()
   CALL encabezado("Modificar")
   --CALL info_usuario()
   
   IF captura_datos('M') THEN
      RETURN actualizar()
   ELSE 
      RETURN FALSE 
   END IF 
END FUNCTION 

FUNCTION delete_init()
DEFINE strSql STRING 

   LET strSql =
      "DELETE FROM pemMOrdPro ",
      "WHERE idOrdPro = ? "
      
   PREPARE st_delete FROM strSql

   LET strSql =
      "DELETE FROM pemDOrdPro ",
      "WHERE idOrdPro = ? "
      
   PREPARE st_deleteDet FROM strSql


END FUNCTION 

FUNCTION actualizar()
DEFINE i SMALLINT 

   {IF NOT can_delete("pemMBoleta", "idOrdPro", g_reg.idOrdPro) THEN
      CALL msg("No se puede eliminar, ya existe en boletas de ingreso")
      RETURN FALSE 
   END IF}
   WHENEVER ERROR CONTINUE 
   BEGIN WORK 
   EXECUTE IMMEDIATE "SET CONSTRAINTS ALL DEFERRED"
   --Modifica encabezado
   TRY
      EXECUTE st_modificar USING 
        g_reg.numOrdPro, g_reg.nomOrdPro, 
        g_reg.idItemSap, g_reg.cueafe,
        g_reg.costostdprod,
        g_reg.fecInicio, g_reg.fecFinal, 
        g_reg.estado, 
        u_reg.idOrdPro 
      --Si todo bien elimina el detalle
      TRY
         EXECUTE st_deleteDet USING 
           g_reg.idOrdPro 
         --Si todo bien inserta nuevo detalle
         TRY 
            FOR i= 1 TO reg_detArr.getLength()
               --LET reg_detArr[i].idOrdPro = g_reg.idOrdPro
               EXECUTE st_insertarDetMod USING 
                 g_reg.idOrdPro, i, 
                 reg_detArr[i].idFinca, reg_detArr[i].idEstructura,
                 reg_detArr[i].idValvula, reg_detArr[i].idItem 
            END FOR 
         CATCH 
            ROLLBACK WORK 
            WHENEVER ERROR STOP
            CALL msgError(sqlca.sqlcode,"Grabar Detalle de Registro")
            RETURN FALSE 
         END TRY
      CATCH 
         ROLLBACK WORK
         WHENEVER ERROR STOP 
         CALL msgError(sqlca.sqlcode,"Modificar Detalle")
         RETURN FALSE 
      END TRY
   CATCH 
      WHENEVER ERROR STOP 
      CALL msgError(sqlca.sqlcode,"Modificar Registro")
      ROLLBACK WORK
      RETURN FALSE 
   END TRY
   COMMIT WORK 
   WHENEVER ERROR STOP 
   CALL msg ("Registro actualizado")
   RETURN TRUE 
END FUNCTION 

FUNCTION anular()
   IF NOT box_confirma("Esta seguro de anular el registro") THEN
      RETURN FALSE
   END IF 
   WHENEVER ERROR CONTINUE 
   BEGIN WORK 
   --Eliminar detalle
   TRY
      EXECUTE st_deleteDet USING 
           g_reg.idOrdPro 
      --Si todo bien elimina encabezado
      TRY 
         EXECUTE st_delete USING g_reg.idOrdPro
      CATCH 
         ROLLBACK WORK
         WHENEVER ERROR STOP 
         CALL msgError(sqlca.sqlcode,"Eliminar Registro")
         RETURN FALSE 
      END TRY
   CATCH 
      ROLLBACK WORK
      WHENEVER ERROR STOP 
      CALL msgError(sqlca.sqlcode,"Eliminar detalle de registro")
      RETURN FALSE 
   END TRY
   COMMIT WORK 
   WHENEVER ERROR STOP 
   CALL msg("Registro eliminado")
   RETURN TRUE 
END FUNCTION 