
GLOBALS "pemm0206_glob.4gl"

FUNCTION captura_datos(operacion)
DEFINE operacion CHAR (1)
DEFINE resultado BOOLEAN
DEFINE w ui.Window
DEFINE f ui.Form
DEFINE idx, idx1, scrLn SMALLINT 
DEFINE flagTouchSDet2 SMALLINT 

   LET resultado = FALSE 
   LET u_reg.* = g_reg.*
   IF operacion = 'I' THEN 
      CALL cleanAll()
      
      CALL displayEnc()
   END IF
   LET flagTouchSDet2 = 0
   DIALOG ATTRIBUTES(UNBUFFERED)
       
     INPUT --BY NAME  
      g_reg.numOrdPro, g_reg.nomOrdPro, 
      g_reg.idItemSap, g_reg.cueafe,
      g_reg.costostdprod,
      g_reg.fecInicio, g_reg.fecFinal,
      g_reg.estado --, g_reg.idFinca,
      --g_reg.idEstructura, g_reg.idValvula, g_reg.idItem
     FROM fnumOrdPro, fnomOrdPro,
      fiditemsap, fcueafe, fcostostdprod,
      ffecInicio, ffecFinal, festado
      ATTRIBUTES (WITHOUT DEFAULTS)
      
      BEFORE INPUT
         CALL DIALOG.setActionHidden("close",TRUE)
         IF (operacion = "I") THEN 
            LET g_reg.estado = 1
            DISPLAY g_reg.estado TO festado
         END IF 

      ON CHANGE fnumOrdPro
        SELECT numOrdPro FROM pemMOrdPro
        WHERE numOrdPro = g_reg.numOrdPro
        IF SQLca.sqlcode = 0 THEN
           CALL msg("N�mero de Orden de Producci�n ya existe")
           NEXT FIELD CURRENT 
        END IF   

      ON CHANGE ffecFinal 
        IF g_reg.fecFinal <= g_reg.fecInicio THEN
           CALL msg("Fecha final debe ser mayor a la fecha de inicio")
           NEXT FIELD CURRENT
        END IF  

      BEFORE FIELD festado 
        IF (operacion = "I") THEN
           NEXT FIELD NEXT   
        END IF   
        
            --IF (operacion = 'I') OR (g_reg.emNomCt <> u_reg.emNomCt) THEN 
            --IF existe_cat_cod(g_reg.idBautizo) THEN 
           
               --LET g_reg.idBautizo = NULL 
            
            --END IF   
         --END IF
       

      {AFTER FIELD librob
      IF g_reg.librob IS NOT NULL THEN 
         IF (operacion = 'I') OR (g_reg.librob <> u_reg.librob) THEN 
            IF existe_cat_nom(g_reg.librob) THEN 
               CALL msg("Este id_commdep ya esta siendo utilizado")
               LET g_reg.librob = NULL 
               NEXT FIELD CURRENT 
            END IF   
         END IF
      END IF} 
      
   END INPUT 
   INPUT ARRAY reg_detArr FROM sDet2.*
      --ATTRIBUTES ( WITHOUT DEFAULTS, INSERT ROW = FALSE, APPEND ROW = FALSE, DELETE ROW = FALSE )
      ATTRIBUTES ( WITHOUT DEFAULTS, INSERT ROW = FALSE) --, DELETE ROW = FALSE)

      BEFORE INPUT
        CALL fgl_dialog_setkeylabel("Append", "")
    
      --BEFORE ROW 
      BEFORE FIELD sidfinca
         LET reg_detArr[arr_curr()].idDOrdPro = arr_curr()
         DISPLAY reg_detArr[arr_curr()].idDOrdPro TO sdet2[scr_line()].sidordpro

      AFTER INSERT
         IF reg_detArr[arr_curr()].idDOrdPro IS NULL 
            OR reg_detArr[arr_curr()].idFinca IS NULL
            OR reg_detArr[arr_curr()].idEstructura IS NULL
            OR reg_detArr[arr_curr()].idItem IS NULL THEN
            CANCEL INSERT
         END IF 
         --Revisa que la linea no se duplique 
         IF repLine(arr_curr()) THEN 
            CALL msg("L�nea repetida")
            NEXT FIELD sIdFinca
         END IF 
      ON CHANGE sidfinca 
         LET flagTouchSDet2 = 1 

      ON CHANGE sidestructura 
         LET flagTouchSDet2 = 1 

      ON CHANGE sidvalvula 
         LET flagTouchSDet2 = 1 
         
      ON CHANGE siditem 
         LET flagTouchSDet2 = 1 
            
         
      
         --IF reg_detArr[arr_curr()].idDOrdPro IS NULL 
            --OR reg_detArr[arr_curr()].idFinca IS NULL
            --OR reg_detArr[arr_curr()].idEstructura IS NULL
            --OR reg_detArr[arr_curr()].idItem IS NULL THEN
            --CANCEL INSERT
         --END IF 
         --LET reg_detArr[idx].idFinca = 1
         --LET idx = 100
         --DISPLAY BY NAME reg_detArr[idx].idDOrdPro --TO sDet2[scrLn].diddordpro

      --BEFORE INSERT

      {AFTER FIELD gru_id
         IF existe_id( idx1, d_gru ) THEN
            CALL msg("Ya fue ingresado elemento con el mismo Id.")
            LET d_gru[idx1] = NULL
            NEXT FIELD CURRENT
         END IF

      AFTER INSERT
         IF d_gru[idx1] IS NULL THEN
            CANCEL INSERT
         END IF

      ON ACTION DELETE
         LET idx1 = arr_curr()
         CALL DIALOG.deleteRow("sdet1", idx1)}
      
   END INPUT 
   ON ACTION ACCEPT
      
        IF g_reg.numOrdPro IS NULL THEN
           CALL msg("Debe ingresar tipo de n�mero de Orden de Producci�n")
           NEXT FIELD fnumOrdPro
        END IF  
         
        IF g_reg.nomOrdPro IS NULL THEN
           CALL msg("Debe ingresar nombre de Orden de Producci�n")
           NEXT FIELD fnomOrdPro
        END IF  
         
        IF g_reg.fecInicio IS NULL THEN
           CALL msg("Debe ingresar fecha de inicio")
           NEXT FIELD ffecInicio
        END IF  
 
        IF g_reg.fecFinal IS NULL THEN
           CALL msg("Debe ingresar fecha final")
           NEXT FIELD ffecFinal
        END IF  

         -- Arreglo
         IF flagTouchSDet2 = 1 THEN 
            LET idx = arr_curr() 
         
            IF reg_detArr[arr_curr()].idFinca IS NULL THEN
              CALL msg("Debe ingresar finca")
              NEXT FIELD sidfinca
            END IF 
            IF reg_detArr[arr_curr()].idEstructura IS NULL THEN
              CALL msg("Debe ingresar Estructura")
              NEXT FIELD sidestructura
            END IF 
            IF reg_detArr[arr_curr()].idItem IS NULL THEN
              CALL msg("Debe ingresar Item")
              NEXT FIELD siditem
            END IF 
         END IF 

      IF operacion = 'M' AND g_reg.* = u_reg.* THEN
         IF flagTouchSDet2 = 0 THEN 
            CALL msg("No se efectuaron cambios")
            EXIT DIALOG 
         END IF 
      END IF
      --valida que el detalle no este vac�o
      IF operacion = "I" THEN 
         IF flagTouchSDet2 = 0 THEN
            CALL msg("No ha ingresado detalle")
            CONTINUE DIALOG
         END IF
      END IF    
      CASE box_gradato("Seguro de grabar")
         WHEN "Si"
            LET resultado = TRUE
            EXIT DIALOG
         WHEN "No"
            EXIT DIALOG 
         OTHERWISE
            CONTINUE DIALOG 
      END CASE 
      LET resultado = TRUE
      EXIT DIALOG 
      
     ON ACTION CANCEL
        EXIT dialog
   END DIALOG
   
   IF NOT resultado THEN
      LET g_reg.* = u_reg.*
      CALL displayEnc() 
   END IF 
   RETURN resultado 
END FUNCTION

FUNCTION cleanAll()
   INITIALIZE g_reg.* TO NULL
   CALL reg_detArr.clear()
END FUNCTION 

FUNCTION repLine(cLin)
DEFINE cLin, i SMALLINT

FOR i = 1 TO reg_detArr.getLength()
   IF i <> cLin THEN
      IF reg_detArr[i].idFinca = reg_detArr[cLin].idFinca AND 
         reg_detArr[i].idEstructura = reg_detArr[cLin].idEstructura AND 
         reg_detArr[i].idValvula = reg_detArr[cLin].idValvula AND 
         reg_detArr[i].idItem = reg_detArr[cLin].idItem THEN 
        RETURN TRUE 
      END IF 
   END IF 
   RETURN FALSE 
END FOR 

END FUNCTION 


--FUNCTION existe_cat_cod(l_cod)
--DEFINE l_cod LIKE bautizo.idBautizo
--DEFINE resultado BOOLEAN 
--
   --LET resultado = FALSE 
   --TRY 
      --SELECT FIRST 1 * 
      --FROM bautizo
      --WHERE idBautizo = l_cod
--
      --IF STATUS = 0 THEN
         --LET resultado = TRUE
      --END IF
   --CATCH 
      --CALL msgError(status, "Obtener codigo")
   --END TRY  
   --RETURN resultado 
--END FUNCTION

--FUNCTION existe_cat_nom(l_cod)
--DEFINE l_cod LIKE bautizo.librob
--DEFINE resultado BOOLEAN 
--
   --LET resultado = FALSE 
   --TRY 
      --SELECT FIRST 1 * 
      --FROM bautizo
      --WHERE librob = l_cod
--
      --IF STATUS = 0 THEN
         --LET resultado = TRUE
      --END IF
   --CATCH 
      --CALL msgError(status, "Obtener id_commdep")
   --END TRY  
   --RETURN resultado 
--END FUNCTION