################################################################################
# Funcion     : %M%
# id_commdep  : Catalogo de Valvulas
#               Funcion para definicion de globales
# Funciones   : 
#               
#               
#  
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        id_commdep de la modificacion
#
################################################################################
SCHEMA db0001

GLOBALS 
TYPE 
   tDet RECORD 
      idOrdPro      LIKE pemMOrdPro.idOrdPro,
      numOrdPro     LIKE pemMOrdPro.numOrdPro,
      nomOrdPro     LIKE pemMOrdPro.nomOrdPro,
      idItemSap     LIKE pemMOrdPro.iditemsap,
      cueafe        LIKE pemMOrdPro.cueafe,
      costostdprod  LIKE pemMOrdPro.costostdprod, 
      fecInicio     LIKE pemMOrdPro.fecInicio,
      fecFinal      LIKE pemMOrdPro.fecFinal,
      estado        LIKE pemMOrdPro.estado
   END RECORD

DEFINE reg_det DYNAMIC ARRAY OF tDet
DEFINE g_reg, u_reg tDet

TYPE 
   tDetArr RECORD
      idOrdPro      LIKE pemDOrdPro.idordpro,
      idDOrdPro     LIKE pemDOrdPro.iddordpro,
      idFinca       LIKE pemDOrdPro.idFinca,
      idEstructura  LIKE pemDOrdPro.idEstructura,
      idValvula     LIKE pemDOrdPro.idValvula,
      idItem        LIKE pemDOrdPro.idItem
   END RECORD 

DEFINE reg_detArr DYNAMIC ARRAY OF tDetArr
DEFINE g_regArr, u_regArr tDetArr
   
DEFINE dbname      STRING
   
DEFINE condicion   STRING --Condicion de la clausula Where 
CONSTANT    prog_name = "pemm0206"
END GLOBALS