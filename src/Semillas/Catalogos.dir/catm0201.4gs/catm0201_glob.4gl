################################################################################
# Funcion     : %M%
# Descripcion : Catalogo de Empresas
#               Funcion para definicion de globales
# Funciones   : 
#               
#               
#  
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################
SCHEMA db0001

GLOBALS 
TYPE 
   tDet RECORD 
      idFinca       LIKE pemMFinca.idFinca,
      id_commemp    LIKE pemMFinca.id_commemp,
      fincaNomCt    LIKE pemMFinca.fincaNomCt,
      fincaNomMd    LIKE pemMFinca.fincaNomMd,
      fincaNomLg    LIKE pemMFinca.fincaNomLg,
      serieEnvio    LIKE pemMFinca.serieenvio
   END RECORD

DEFINE
   reg_det DYNAMIC ARRAY OF tDet, 
   g_reg, u_reg tDet,
   dbname      STRING,
   
   condicion   STRING --Condicion de la clausula Where 
   CONSTANT    prog_name = "catm0201"
END GLOBALS