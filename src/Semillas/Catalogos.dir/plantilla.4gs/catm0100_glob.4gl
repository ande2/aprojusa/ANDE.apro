################################################################################
# Funcion     : %M%
# Descripcion : Funcion para definicion de globales
# Funciones   : 
#               
#               
#  
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Anderson Garcia 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################
DATABASE sgp

GLOBALS 
TYPE 
   tDet RECORD 
      idBautizo LIKE bautizo.idBautizo,
      librob    LIKE bautizo.librob,
      foliob    LIKE bautizo.foliob,
      partidab  LIKE bautizo.partidab,
      nombre    LIKE bautizo.nombre,
      sexo1     LIKE bautizo.sexo1,
      dia1      LIKE bautizo.dia1,
      mes1      LIKE bautizo.mes1,
      ano1      LIKE bautizo.ano1,
      dia2      LIKE bautizo.dia2,
      mes2      LIKE bautizo.mes2,
      ano2      LIKE bautizo.ano2,
      sexo2     LIKE bautizo.sexo2,
      papa      LIKE bautizo.papa,
      mama      LIKE bautizo.mama,
      padrino1  LIKE bautizo.padrino1,
      padrino2  LIKE bautizo.padrino2,
      notam     LIKE bautizo.notam,
      dia3      LIKE bautizo.dia3,
      mes3      LIKE bautizo.mes3,
      ano3      LIKE bautizo.ano3
   END RECORD

DEFINE
   reg_det DYNAMIC ARRAY OF tDet, 
   g_reg, u_reg tDet,
   dbname      STRING,
   
   condicion      STRING --Condicion de la clausula Where 
   CONSTANT    prog_name = "bautizos"
END GLOBALS