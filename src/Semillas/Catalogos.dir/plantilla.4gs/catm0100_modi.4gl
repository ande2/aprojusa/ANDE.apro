################################################################################
# Funcion     : %M%
# Descripcion : Funcion para modificar datos de un umdor
# Funciones   : updel_init()
#	             delete_umd() 
#               update_umd()
#               pk_estado()
#               valida_modi()
#               verifica_modi()
#               existe_datos()
#               existe_dpro()
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Anderson Garcia 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################
GLOBALS "catm0002_glob.4gl"

FUNCTION update_init()
DEFINE strSql STRING 

   LET strSql = 
      "UPDATE bautizo ",
      "SET ",
      " librob      = ?, ", 
      " foliob      = ?, ",
      " partidab    = ?, ",
      " nombre      = ?, ",
      " sexo1       = ?, ",
      " dia1        = ?, ",
      " mes1        = ?, ",
      " ano1        = ?, ",
      " dia2        = ?, ",
      " mes2        = ?, ",
      " ano2        = ?, ",
      " sexo2       = ?, ",
      " papa        = ?, ",
      " mama        = ?, ",
      " padrino1    = ?, ",
      " padrino2    = ?, ", 
      " notam       = ?, ",
      " dia3        = ?, ",
      " mes3        = ?, ",
      " ano3        = ? ",

      " WHERE idBautizo = ?"

   PREPARE st_modificar FROM strSql
END FUNCTION 

FUNCTION modifica()
   CALL encabezado("Modificar")
   --CALL info_usuario()
   
   IF captura_datos('M') THEN
      RETURN actualizar()
   ELSE 
      RETURN FALSE 
   END IF 
END FUNCTION 

FUNCTION actualizar()
   TRY
      EXECUTE st_modificar USING 
      g_reg.librob, g_reg.foliob, g_reg.partidab, g_reg.nombre,
      g_reg.sexo1, g_reg.dia1, g_reg.mes1, g_reg.ano1,
      g_reg.dia2, g_reg.mes2, g_reg.ano2, g_reg.sexo2,
      g_reg.papa, g_reg.mama, g_reg.padrino1, g_reg.padrino2,
      g_reg.notam, g_reg.dia3, g_reg.mes3, g_reg.ano3,
      u_reg.idBautizo
   CATCH 
      CALL msgError(sqlca.sqlcode,"Modificar Registro")
      RETURN FALSE 
   END TRY
   CALL msg ("Registro actualizado")
   RETURN TRUE 
END FUNCTION 

FUNCTION delete_init()
DEFINE strSql STRING 

   LET strSql =
      "DELETE FROM bautizo ",
      "WHERE idBautizo = ?"
      
   PREPARE st_delete FROM strSql

END FUNCTION 

FUNCTION anular()
   IF NOT box_confirma("Esta seguro de eliminar registro") THEN
      RETURN FALSE
   END IF 
   TRY 
      EXECUTE st_delete USING g_reg.idBautizo
   CATCH 
      CALL msgError(sqlca.sqlcode,"Eliminar Registro")
      RETURN FALSE 
   END TRY
   CALL msg("Registro eliminado")
   RETURN TRUE 
END FUNCTION 