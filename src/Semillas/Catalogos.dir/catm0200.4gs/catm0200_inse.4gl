################################################################################
# Funcion     : %M%
# Descripcion : Ingreso de un nuevo umdor 	
# Funciones   : input_umd() funcion para insertar datos
#              
#              
#               
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Anderson Garcia 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################

GLOBALS "catm0200_glob.4gl"

FUNCTION insert_init()
DEFINE strSql STRING 

   LET strSql =
      "INSERT INTO mEmpr ( ",
        " idEmpresa, empNomCt, empNomMd, empNomLg, flagPropia, nomBD, ",
        " recBolCodigo, recBolVersion ",
        "   ) ",
      " VALUES (?,?,?,?,?,?,?,?)"

   PREPARE st_insertar FROM strSql

END FUNCTION 

FUNCTION ingreso()
   CALL encabezado("Ingresar")
   --CALL info_usuario()
   IF captura_datos('I') THEN
      RETURN grabar()
   ELSE 
      RETURN FALSE 
   END IF 
END FUNCTION 

FUNCTION grabar()
   LET g_reg.idEmpresa = 0
   TRY 
      EXECUTE st_insertar USING 
         g_reg.idEmpresa, g_reg.empNomCt, g_reg.empNomMd, g_reg.empNomLg,
         g_reg.flagPropia, g_reg.nomBd, g_reg.recBolCodigo, g_reg.recBolVersion

      --CS agregarlo cuando se agregue el ID 
      LET g_reg.idEmpresa = SQLCA.sqlerrd[2]
   CATCH 
      CALL msgError(sqlca.sqlcode,"Grabar Registro")
      RETURN FALSE 
   END TRY
   DISPLAY BY NAME g_reg.*
   CALL box_valdato ("Registro agregado")
   RETURN TRUE 
END FUNCTION 