################################################################################
# Funcion     : %M%
# Descripcion : Catalogo de Empresas
#               Funcion para definicion de globales
# Funciones   : 
#               
#               
#  
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################
SCHEMA db0000

GLOBALS 
TYPE 
   tDet RECORD 
      idEmpresa     LIKE mEmpr.idempresa,
      empNomCt       LIKE mEmpr.empnomct,
      empNomMd      LIKE mEmpr.empnommd,
      empNomLg      LIKE mEmpr.empnomlg,
      flagPropia    LIKE mEmpr.flagPropia,
      recBolCodigo  LIKE mEmpr.recbolcodigo,
      recBolVersion LIKE mEmpr.recbolversion,
      nomBd         LIKE mEmpr.nombd
   END RECORD

DEFINE
   reg_det DYNAMIC ARRAY OF tDet, 
   g_reg, u_reg tDet,
   dbname      STRING,
   
   condicion      STRING --Condicion de la clausula Where 
   CONSTANT    prog_name = "catm0100"
END GLOBALS