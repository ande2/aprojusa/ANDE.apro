################################################################################
# Funcion     : %M%
# id_commdep : Catalogo de Valvulas
#               Funcion para definicion de globales
# Funciones   : 
#               
#               
#  
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        id_commdep de la modificacion
#
################################################################################
SCHEMA db0001

GLOBALS 
TYPE 
   tDet RECORD 
      idCatEmpaque   LIKE pemMCatEmp.idCatEmpaque,
      idTipEmpaque   LIKE pemMCatEmp.idtipempaque,
      nomCatEmpaque  LIKE pemMCatEmp.nomCatEmpaque,
      pesoCatEmpaque LIKE pemMCatEmp.pesocatempaque
      
   END RECORD

DEFINE
   reg_det DYNAMIC ARRAY OF tDet, 
   g_reg, u_reg tDet,
   dbname      STRING,
   
   condicion   STRING --Condicion de la clausula Where 
   CONSTANT    prog_name = "pemm0205"
END GLOBALS