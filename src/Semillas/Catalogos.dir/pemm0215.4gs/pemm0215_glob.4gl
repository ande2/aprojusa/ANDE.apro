################################################################################
# Funcion     : %M%
# nombre      : Catalogo de Familias
#               Funcion para definicion de globales
# Funciones   : 
#               
#               
#  
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        nombre de la modificacion
#
################################################################################
--DATABASE comisiones
SCHEMA db0001

GLOBALS 
TYPE 
   tDet RECORD 
      idCliente     LIKE pemmcli.idCliente,
      codigo        LIKE pemmcli.codigo,
      tipo          LIKE pemmcli.tipo,
      pais          LIKE pemmcli.pais,
      nombre        LIKE pemmcli.nombre,
      estado        LIKE pemmcli.estado
   END RECORD

DEFINE
   reg_det DYNAMIC ARRAY OF tDet, 
   g_reg, u_reg tDet,
   dbname      STRING,
   
   condicion   STRING --Condicion de la clausula Where 
   CONSTANT    prog_name = "pemm0215"
END GLOBALS