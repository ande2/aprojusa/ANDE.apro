################################################################################
# Funcion     : %M%
# Descripcion : Funcion para modificar datos de un umdor
# Funciones   : updel_init()
#	             delete_umd() 
#               update_umd()
#               pk_estado()
#               valida_modi()
#               verifica_modi()
#               existe_datos()
#               existe_dpro()
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################
GLOBALS "catm0101a_glob.4gl"

FUNCTION update_init()
DEFINE strSql STRING 

   LET strSql = 
      "UPDATE commempl ",
      "SET ",
      " nombre          = ?, ",
      " apellido        = ?, ", 
      " direccion       = ?, ", 
      " telefono        = ?, ",

      " usuGrpId        = ?, ",
      " usuLogin        = ?, ",
      " usuPwd          = ?, ",
      
      " codigo          = ?, ",
      " id_commdep      = ?, ",
      " id_commpue      = ?, ", 
      " jefe            = ?, ", 
      " estado          = ?, ", 
       
      " comision        = ?, ", 
      " numCuenta       = ?  ",
      " WHERE id_commempl = ?"

   PREPARE st_modificar FROM strSql
END FUNCTION 

FUNCTION modifica()
   CALL encabezado("Modificar")
   --CALL info_usuario()
   
   IF captura_datos('M') THEN
      RETURN actualizar()
   ELSE 
      RETURN FALSE 
   END IF 
END FUNCTION 

FUNCTION actualizar()
   TRY
      EXECUTE st_modificar USING 
        g_reg.nombre, g_reg.apellido, 
        g_reg.direccion, g_reg.telefono, 
        g_reg.usuGrpId, g_reg.usuLogin, g_reg.usuPwd,
        g_reg.codigo, g_reg.id_commdep, g_reg.id_commpue, 
        g_reg.jefe, g_reg.estado, 
        g_reg.comision,  g_reg.numCuenta,
        u_reg.id_commempl

   CATCH 
      CALL msgError(sqlca.sqlcode,"Modificar Registro")
      RETURN FALSE 
   END TRY
   CALL msg ("Registro actualizado")
   RETURN TRUE 
END FUNCTION 

FUNCTION delete_init()
DEFINE strSql STRING 

   LET strSql =
      "DELETE FROM commempl ",
      "WHERE id_commempl = ? "
      
   PREPARE st_delete FROM strSql

END FUNCTION 

FUNCTION anular()
   IF NOT box_confirma("Esta seguro de anular el registro") THEN
      RETURN FALSE
   END IF 
   TRY 
      EXECUTE st_delete USING g_reg.id_commempl
   CATCH 
      CALL msgError(sqlca.sqlcode,"Eliminar Registro")
      RETURN FALSE 
   END TRY
   CALL msg("Registro eliminado")
   RETURN TRUE 
END FUNCTION 