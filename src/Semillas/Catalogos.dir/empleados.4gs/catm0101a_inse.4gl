################################################################################
# Funcion     : %M%
# Descripcion : Ingreso de un nuevo umdor 	
# Funciones   : input_umd() funcion para insertar datos
#              
#              
#               
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################

GLOBALS "catm0101a_glob.4gl"

FUNCTION insert_init()
DEFINE strSql STRING 

   LET strSql =
      "INSERT INTO commempl ( ",
        " id_commempl, nombre, apellido, ",
        " direccion, telefono,  ",
        " usuGrpid, usuLogin, usuPwd, ",
        " codigo, id_commdep, id_commpue,  jefe, estado, ",
        " comision, numCuenta ",
        "   ) ",
      " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"

   PREPARE st_insertar FROM strSql

END FUNCTION 

FUNCTION ingreso()
   CALL encabezado("Ingresar")
   --CALL info_usuario()
   IF captura_datos('I') THEN
      RETURN grabar()
   ELSE 
      RETURN FALSE 
   END IF 
END FUNCTION 

FUNCTION grabar()
   
   LET g_reg.id_commempl = 0
   LET g_reg.estado = 1
   TRY 
      EXECUTE st_insertar USING 
            g_reg.id_commempl, g_reg.nombre, g_reg.apellido, 
            g_reg.direccion, g_reg.telefono, 
            g_reg.usuGrpId, g_reg.usuLogin, g_reg.usuPwd,
            g_reg.codigo, g_reg.id_commdep, g_reg.id_commpue, 
            g_reg.jefe, g_reg.estado, 
            g_reg.comision,  g_reg.numCuenta

      --CS agregarlo cuando se agregue el ID 
      LET g_reg.id_commempl = SQLCA.sqlerrd[2]
   CATCH 
      CALL msgError(sqlca.sqlcode,"Grabar Registro")
      RETURN FALSE 
   END TRY
   DISPLAY BY NAME g_reg.*
   CALL box_valdato ("Registro agregado")
   CALL combo_din2("jefe",'SELECT id_commempl, TRIM(nombre) || " "|| TRIM(apellido) FROM commempl WHERE id_commempl > 0')
   RETURN TRUE 
END FUNCTION 