################################################################################
# Funcion     : %M%
# nombre      : Catalogo de Tipos
#               Funcion para definicion de globales
# Funciones   : 
#               
#               
#  
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        nombre de la modificacion
#
################################################################################
--DATABASE comisiones
SCHEMA db0001

GLOBALS 
TYPE 
   tDet RECORD 
      idboletaing       LIKE pemmdatosSAP.idBoletaIng,
      numordpro         LIKE pemmdatosSAP.numOrdPro,
      codigosocioneg    LIKE pemmdatosSAP.codigoSocioNeg,
      nombresocioneg    LIKE pemmdatosSAP.nombreSocioNeg,
      comentariosdoc    LIKE pemmdatosSAP.comentariosDoc,
      fechaconta        LIKE pemmdatosSAP.fechaConta,
      estado            LIKE pemmdatosSAP.estado,
      numreciboprosap   LIKE pemmdatosSAP.numreciboprosap,
      numsalidainvsap   LIKE pemmdatosSAP.numsalidainvsap,
      numentradainvsap  LIKE pemmdatosSAP.numentradainvsap,
      numentmercexpsap  LIKE pemmdatosSAP.numentmercexpsap,
      numentmerclocsap  LIKE pemmdatosSAP.numentmerclocsap,
      obs               LIKE pemmdatosSAP.obs
      
   END RECORD

DEFINE
   reg_det DYNAMIC ARRAY OF tDet, 
   g_reg, u_reg tDet,
   dbname      STRING,
   
   condicion   STRING --Condicion de la clausula Where 
   CONSTANT    prog_name = "pemp0250"

   DEFINE gr_reg RECORD
    idBoletaIng     LIKE pemmboletacau.idBoletaIng,
    idBoletaCau     LIKE pemmboletacau.idboletacau,
    fecha           LIKE pemmboleta.fecha,
    estructura      STRING,
    productor       LIKE pemmboleta.productor , 
    iditem          LIKE pemmboleta.iditem,
    pesorecepcion   SMALLINT ,
    pesoaprovexp    SMALLINT,
    pesoaprovloc    SMALLINT,
    pesomediano     SMALLINT ,
    pesodevolucion  SMALLINT ,
    pesomerma       SMALLINT ,
    fectran         LIKE pemMBoletacau.fectran,
    usuOpero        LIKE pemMBoletacau.usuOpero
  END RECORD 

  DEFINE gr_det DYNAMIC ARRAY OF RECORD
   idboletacau    LIKE pemdboletacau.idboletacau, 
   idcausa        LIKE pemdboletacau.idcausa,  
   porcentaje     LIKE pemdboletacau.porcentaje,
   obsevaciones   LIKE pemdboletacau.observaciones
  END RECORD 

  DEFINE gProcesa   SMALLINT --Para saber si ejecutara 1=Carreta, 2=Cenma o 3=Devolución
  DEFINE resultado BOOLEAN
  DEFINE ltipo, i, j, idx, idx2, det2, insdet2 SMALLINT 
  DEFINE lidordpro, liddordpro, lproductor, litem, vpesototal INTEGER 
  DEFINE vEstructura, vProductor, vItem VARCHAR (100) 
  DEFINE vdescripcion, lcondi VARCHAR (100)
  DEFINE lPesoCaja, lPesoTotal, lSumPeso, lPesoSaldo, vpeso LIKE pemmboleta.pesototal
  DEFINE cbCalidad ui.ComboBox
  DEFINE gMsg STRING 
  DEFINE gAccion CHAR (1)
   
END GLOBALS