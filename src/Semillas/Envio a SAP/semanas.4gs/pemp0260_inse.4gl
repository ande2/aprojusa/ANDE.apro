################################################################################
# Funcion     : %M%
# Descripcion : Ingreso de un nuevo umdor 	
# Funciones   : input_umd() funcion para insertar datos
#              
#              
#               
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Anderson Garcia 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################

GLOBALS "pemp0260_glob.4gl"

FUNCTION insert_init()
DEFINE strSql STRING 

   LET strSql =
      "INSERT INTO msemanas ( ",
        " lnksem,idcliente,ano,rango_ini,rango_fin,periodoactivo,sem_cal, ",
        " fec_ini,fec_fin,sem_com,sem_fac,sem_liq",
        "   ) ",
      " VALUES (?,?,?,?,?,?,?,?,?,?,?,?)"

   PREPARE st_insertar FROM strSql

END FUNCTION 

FUNCTION ingreso()
   CALL encabezado("Ingresar")
   --CALL info_usuario()
   IF captura_datos('I') THEN
      RETURN grabar()
   ELSE 
      RETURN FALSE 
   END IF 
END FUNCTION 

FUNCTION grabar()
DEFINE vdias SMALLINT
DEFINE dsem RECORD
    lnksem INTEGER ,
    sem_cal SMALLINT ,
    fec_ini DATE , 
    fec_fin DATE ,
    sem_com SMALLINT ,
    sem_fac SMALLINT ,
    sem_liq SMALLINT 
  END RECORD
DEFINE fCompra, fFactura, fLiquida SMALLINT 

   LET g_reg.lnksem1 = 0
   TRY 
      EXECUTE st_insertar USING 
            g_reg.lnksem1, g_reg.idcliente1, g_reg.ano1, g_reg.rango_ini1,
            g_reg.periodoActivo1, g_reg.rango_fin1, g_reg.sem_cal1, g_reg.fec_ini1, g_reg.fec_fin1,
            g_reg.sem_com1, g_reg.sem_fac1, g_reg.sem_liq1

      --CS agregarlo cuando se agregue el ID 
      LET g_reg.lnksem1 = SQLCA.sqlerrd[2]

      --Definiendo factores
      LET fCompra = g_reg.sem_cal1 - g_reg.sem_com1
      LET fFactura = g_reg.sem_cal1 - g_reg.sem_fac1
      LET fLiquida = g_reg.sem_cal1 - g_reg.sem_liq1
      DISPLAY "Com "
      --LET vdias = g_reg.fec_ini1 - g_reg.fec_fin1
      --Para semana compra
      --LET dsem.

      TRY
         -- Inserta en detalle de semanas calendario
         --CALL msg("INSERT")
         LET dsem.lnksem = g_reg.lnksem1 
         LET dsem.sem_cal = g_reg.sem_cal1
         LET dsem.fec_ini = g_reg.fec_ini1
         LET dsem.fec_fin = g_reg.fec_fin1
         WHILE dsem.fec_fin < g_reg.rango_fin1
           --para sem compra
            LET dsem.sem_com = dsem.sem_cal - fCompra
            IF dsem.sem_com > 52 THEN LET dsem.sem_com = -1*(52 - dsem.sem_com) END IF 
            --para sem facturacion
            LET dsem.sem_fac = dsem.sem_cal - fFactura
            IF dsem.sem_fac > 52 THEN LET dsem.sem_fac = -1*(52 - dsem.sem_fac) END IF 
            --para sem liquidacion
            LET dsem.sem_liq = dsem.sem_cal - fLiquida
            IF dsem.sem_liq > 52 THEN LET dsem.sem_liq = -1*(52 - dsem.sem_liq) END IF
            
           INSERT INTO dsemanacal (idsem, lnksem, sem_cal, sem_com, sem_fac, sem_liq, fec_ini, fec_fin)
           VALUES (0, dsem.lnksem, dsem.sem_cal, dsem.sem_com, dsem.sem_fac, dsem.sem_liq,
                   dsem.fec_ini, dsem.fec_fin)
           LET dsem.sem_cal = dsem.sem_cal + 1
           LET dsem.fec_ini = dsem.fec_fin + 1
           LET dsem.fec_fin = dsem.fec_ini + 6
           IF dsem.sem_cal > 52 THEN LET dsem.sem_cal = -1*(52 - dsem.sem_cal) END IF
         END WHILE  
      CATCH 
         CALL msgError(sqlca.sqlcode,"Insertar detalle de la semana")
         RETURN FALSE 
      END TRY 

   CATCH 
      CALL msgError(sqlca.sqlcode,"Grabar Registro")
      RETURN FALSE 
   END TRY
   DISPLAY BY NAME g_reg.*
   CALL box_valdato ("Registro agregado")
   --CALL combo_din2("cmbdepartamento","SELECT * FROM commdep WHERE id_commdep > 0")
   RETURN TRUE 
END FUNCTION 