
GLOBALS "pemp0260_glob.4gl"

FUNCTION captura_datos(operacion)
DEFINE operacion CHAR (1)
DEFINE resultado BOOLEAN
DEFINE w ui.Window
DEFINE f ui.Form
DEFINE vdescripcion VARCHAR(50,1)

   LET resultado = FALSE 
   LET u_reg.* = g_reg.*
   IF operacion = 'I' THEN 
      INITIALIZE g_reg.* TO NULL
      LET g_reg.lnksem1 = 0
      LET g_reg.ano1 = YEAR(TODAY)
      DISPLAY BY NAME g_reg.*
   END IF
   DIALOG ATTRIBUTES(UNBUFFERED)
     INPUT BY NAME  
      g_reg.lnksem1, g_reg.idcliente1, g_reg.ano1, g_reg.rango_ini1, g_reg.rango_fin1, 
      g_reg.periodoactivo1, g_reg.sem_cal1, g_reg.fec_ini1, g_reg.fec_fin1, 
      g_reg.sem_com1, g_reg.sem_fac1, g_reg.sem_liq1  
      ATTRIBUTES (WITHOUT DEFAULTS)

      ON ACTION buscar
          LABEL opcionBusca:
          
          CALL picklist_2("Clientes", "Codigo","Descripci�n","idcliente", "nombre", "pemmcli", "1=1", "1", 0)
            RETURNING g_reg.idcliente1, vdescripcion, INT_FLAG
          --Que no sea nulo
           IF g_reg.idcliente1 IS NULL THEN
              CALL msg("Debe ingresar un cliente valido")
              NEXT FIELD idcliente1
            ELSE
                LET g_reg.nombre1=vdescripcion
                DISPLAY BY NAME g_reg.nombre1
           END IF

      BEFORE INPUT
         CALL DIALOG.setActionHidden("close",TRUE)

      BEFORE FIELD lnksem1
        NEXT FIELD NEXT
  
      BEFORE FIELD periodoactivo1
        IF operacion = "M" THEN 
           NEXT FIELD NEXT 
        END IF 
  
      ON CHANGE idcliente1
        SELECT nombre 
        INTO vdescripcion 
        FROM pemmcli 
        WHERE idcliente = g_reg.idcliente1
        IF sqlca.sqlcode = NOTFOUND THEN
           CALL msg("Cliente no existe")
           NEXT FIELD CURRENT
        ELSE
            LET g_reg.nombre1 = vdescripcion
            DISPLAY BY NAME g_reg.nombre1
        END IF 
          
      {AFTER FIELD nombre
        LET g_reg.nombre = g_reg.nombre CLIPPED  
        SELECT nombre FROM commemp
        WHERE nombre = g_reg.nombre 
        IF sqlca.sqlcode = 0 THEN
           CALL msg("Nombre de empresa ya existe")
           NEXT FIELD CURRENT 
        END IF }
        

      {AFTER FIELD dbname 
        IF g_reg.nombre IS NOT NULL THEN
           SELECT id_commemp FROM commemp WHERE commemp.dbname = g_reg.dbname
           IF sqlca.sqlcode = 0 THEN 
              CALL msg("Nombre de base de datos ya existe")
              NEXT FIELD CURRENT
           END IF 
        END IF}
 
 
            --IF (operacion = 'I') OR (g_reg.emNomCt <> u_reg.emNomCt) THEN 
            --IF existe_cat_cod(g_reg.idBautizo) THEN 
           
               --LET g_reg.idBautizo = NULL 
            
            --END IF   
         --END IF
      
   END INPUT 

   ON ACTION ACCEPT

        IF g_reg.idcliente1 IS NULL THEN
           CALL msg("Debe ingresar cliente")
           NEXT FIELD idcliente1
        END IF  

        IF g_reg.ano1 IS NULL THEN
           CALL msg("Debe ingresar a�o")
           NEXT FIELD ano1
        END IF  

        IF g_reg.rango_ini1 IS NULL THEN
           CALL msg("Debe ingresar rango inicial de periodo")
           NEXT FIELD rango_ini1
        END IF 
        
        IF g_reg.rango_fin1 IS NULL THEN
           CALL msg("Debe ingresar rango final de periodo")
           NEXT FIELD rango_fin1
        END IF

        IF g_reg.sem_cal1 IS NULL THEN
           CALL msg("Debe ingresar numero de semana calendario al que corresponde este periodo")
           NEXT FIELD sem_cal1
        END IF 

        IF g_reg.fec_ini1 IS NULL THEN
           CALL msg("Debe ingresar fecha inicial")
           NEXT FIELD fec_ini1
        END IF

        IF g_reg.fec_fin1 IS NULL THEN
           CALL msg("Debe ingresar fecha inicial")
           NEXT FIELD fec_fin1
        END IF 
        
        IF g_reg.sem_com1 IS NULL THEN
           CALL msg("Debe ingresar una semana de compra")
           NEXT FIELD sem_com1
        END IF 

        IF g_reg.sem_fac1 IS NULL THEN
           CALL msg("Debe ingresar una semana de facturacion")
           NEXT FIELD sem_fac1
        END IF

        IF g_reg.sem_liq1 IS NULL THEN
           CALL msg("Debe ingresar una semana de liquidacion")
           NEXT FIELD sem_liq1
        END IF

        

      
      --IF g_reg.librob IS NULL THEN
         --CALL msg("Debe ingresar nombre")
         --NEXT FIELD librob
      --END IF
      --IF (operacion = 'I') OR (g_reg.cat_id <> u_reg.cat_id) THEN 
         --IF existe_cat_id(g_reg.cat_id) THEN 
            --CALL msg("Este Id. ya esta siendo utilizado")
            --LET g_reg.cat_id = NULL 
            --NEXT FIELD cat_id 
         --END IF   
      --END IF


      {IF (operacion = 'I') OR (g_reg.librob <> u_reg.librob) THEN 
         IF existe_cat_nom(g_reg.librob) THEN 
            CALL msg("Este nombre ya esta siendo utilizado")
            LET g_reg.librob = NULL 
            NEXT FIELD librob 
         END IF   
      END IF}
      IF operacion = 'M' AND g_reg.* = u_reg.* THEN
         CALL msg("No se efectuaron cambios")
         EXIT DIALOG 
      END IF
      CASE box_gradato("Seguro de grabar")
         WHEN "Si"
            LET resultado = TRUE
            EXIT DIALOG
         WHEN "No"
            EXIT DIALOG 
         OTHERWISE
            CONTINUE DIALOG 
      END CASE 
      LET resultado = TRUE
      EXIT DIALOG 

   
      
     ON ACTION CANCEL
        EXIT dialog
   END DIALOG
   
   IF NOT resultado THEN
      LET g_reg.* = u_reg.*
      DISPLAY BY NAME g_reg.* 
   END IF 
   RETURN resultado 
END FUNCTION
