################################################################################
# Funcion     : %M%
# Descripcion : Funcion para modificar datos de un umdor
# Funciones   : updel_init()
#	             delete_umd() 
#               update_umd()
#               pk_estado()
#               valida_modi()
#               verifica_modi()
#               existe_datos()
#               existe_dpro()
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################
GLOBALS "pemp0260_glob.4gl"

FUNCTION update_init()
DEFINE strSql STRING 

   LET strSql = 
      "UPDATE msemanas ",
      "SET ",
      " idcliente    = ?,  ",
      " ano = ?, ",
      " sem_cal = ?, ",
      " rango_ini = ?, ",
      " rango_fin = ?, ",
      " periodoactivo = ?, ",
      " fec_ini    = ?,  ",
      " fec_fin    = ?,  ",
      " sem_com      = ?,  ",
      " sem_fac      = ?,  ",
      " sem_liq    = ?  ",
      " WHERE lnksem = ?"

   PREPARE st_modificar FROM strSql
END FUNCTION 

FUNCTION modifica()
   CALL encabezado("Modificar")
   --CALL info_usuario()
   
   IF captura_datos('M') THEN
      RETURN actualizar()
   ELSE 
      RETURN FALSE 
   END IF 
END FUNCTION 

FUNCTION actualizar()
  DEFINE dsem RECORD
    lnksem INTEGER ,
    sem_cal, sem_com, sem_fac, sem_liq SMALLINT ,
    fec_ini DATE , 
    fec_fin DATE  
  END RECORD
  DEFINE fCompra, fFactura, fLiquida, fValida SMALLINT

  --Definiendo factores
      LET fCompra = g_reg.sem_cal1 - g_reg.sem_com1
      LET fFactura = g_reg.sem_cal1 - g_reg.sem_fac1
      LET fLiquida = g_reg.sem_cal1 - g_reg.sem_liq1
      
   TRY
      EXECUTE st_modificar USING 
      g_reg.idcliente1, g_reg.ano1, g_reg.sem_cal1, g_reg.rango_ini1,
      g_reg.rango_fin1, g_reg.periodoActivo1, g_reg.fec_ini1, g_reg.fec_fin1, 
      g_reg.sem_com1, g_reg.sem_fac1, g_reg.sem_liq1,  u_reg.lnksem1
      TRY
         --CALL msg("DELETE")
         DELETE FROM dsemanacal WHERE lnksem = u_reg.lnksem1
         TRY
            --CALL msg("INSERT")
            LET dsem.lnksem  = u_reg.lnksem1 
            LET dsem.sem_cal = g_reg.sem_cal1
            LET dsem.fec_ini = g_reg.fec_ini1
            LET dsem.fec_fin = g_reg.fec_fin1
            
            WHILE dsem.fec_fin < g_reg.rango_fin1
              --para sem compra
               LET dsem.sem_com = dsem.sem_cal - fCompra
               IF dsem.sem_com > 52 THEN LET dsem.sem_com = -1*(52 - dsem.sem_com) END IF 
               --para sem facturacion
               LET dsem.sem_fac = dsem.sem_cal - fFactura
               IF dsem.sem_fac > 52 THEN LET dsem.sem_fac = -1*(52 - dsem.sem_fac) END IF 
               --para sem liquidacion
               LET dsem.sem_liq = dsem.sem_cal - fLiquida
               IF dsem.sem_liq > 52 THEN LET dsem.sem_liq = -1*(52 - dsem.sem_liq) END IF  
              
              INSERT INTO dsemanacal (idsem, lnksem, sem_cal, sem_com, sem_fac, sem_liq, fec_ini, fec_fin)
              VALUES (0, dsem.lnksem, dsem.sem_cal, dsem.sem_com, dsem.sem_fac, dsem.sem_liq,
                      dsem.fec_ini, dsem.fec_fin)
              LET dsem.sem_cal = dsem.sem_cal + 1
              LET dsem.fec_ini = dsem.fec_fin + 1
              LET dsem.fec_fin = dsem.fec_ini + 6        
                
              IF dsem.sem_cal > 52 THEN LET dsem.sem_cal = -1*(52 - dsem.sem_cal) END IF      
            END WHILE  
         CATCH 
            CALL msgError(sqlca.sqlcode,"Insertar detalle de la semana")
            RETURN FALSE 
         END TRY 
         
      CATCH    
         CALL msgError(sqlca.sqlcode,"Eliminar detalle de la semana")
         RETURN FALSE 
      END TRY
   CATCH 
      CALL msgError(sqlca.sqlcode,"Modificar Registro")
      RETURN FALSE 
   END TRY
   CALL msg ("Registro actualizado")
   RETURN TRUE 
END FUNCTION 

FUNCTION delete_init()
DEFINE strSql STRING 

   LET strSql =
      "DELETE FROM msemanas ",
      "WHERE lnksem = ? "
      
   PREPARE st_delete FROM strSql

END FUNCTION 

FUNCTION anular()
   --Cuando la tabla es corporativa (creada en ANDE) y necesita validar que 
   -- no se haya usado en las bases de datos de empresas (db0001, db0002, etc)
   {IF NOT can_deleteAll("pemmfinca", "id_commemp", g_reg.id_commemp) THEN
      CALL msg("Empresa no se puede eliminar, ya existe en finca(s)")
      RETURN FALSE 
   END IF  }
   IF NOT box_confirma("Esta seguro de anular el registro") THEN
      RETURN FALSE
   END IF 
   TRY 
      EXECUTE st_delete USING g_reg.lnksem1
   CATCH 
      CALL msgError(sqlca.sqlcode,"Eliminar Registro")
      RETURN FALSE 
   END TRY
   CALL msg("Registro eliminado")
   RETURN TRUE 
END FUNCTION 

