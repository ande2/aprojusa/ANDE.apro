################################################################################
# Funcion     : %M%
# Descripcion : Modulo Principal para umdores 
# Funciones   : main_init(dbname)
#               main_menu()                               
#               dummy() 
#               
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Anderson Garcia  
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################

GLOBALS "pemp0260_glob.4gl"
MAIN
	DEFINE 
		n_param 	SMALLINT,
		prog_name2 	STRING
		
	DEFER INTERRUPT

	OPTIONS
		INPUT  WRAP,
		HELP KEY CONTROL-W,
		COMMENT LINE OFF,
		PROMPT LINE LAST - 2,
		MESSAGE LINE LAST - 1,
		ERROR LINE LAST

	LET n_param = num_args()

	{IF n_param = 0 THEN
		RETURN
	ELSE
        LET dbname = arg_val(1) 
        --LET dbname = "ande"
		CONNECT TO dbname
	END IF}
   CONNECT TO "db0001"

	LET prog_name2 = prog_name||".log"   -- El progr_name es definido como constante en el arch. globals
    
	CALL STARTLOG(prog_name2)
	CALL main_init()
END MAIN

FUNCTION main_init()
DEFINE 
	nom_forma 	STRING,
    w           ui.Window,
    f           ui.Form
	  
	INITIALIZE u_reg.* TO NULL
	LET g_reg.* = u_reg.*

   --CALL trae_usuario() returning g_usuario.*
   --CALL trae_empresa(dbname) returning g_empresa.*
   --CALL trae_programa(prog_name) returning g_programa.*
   --IF g_programa.prog_id = 0 OR 
      --g_programa.prog_id IS NULL THEN
      --CALL box_valdato("Programa no existente, contacte a sistemas")
      --RETURN 
   --END IF 
    CALL ui.Interface.loadActionDefaults("actiondefaults")
    CALL ui.Interface.loadStyles("styles_sc")
   --CALL ui.Interface.loadToolbar("toolbar")

	LET nom_forma = prog_name CLIPPED, "_form"
    CLOSE WINDOW SCREEN 
    LET nom_forma = "pemp0260_form"
	OPEN WINDOW w1 WITH FORM nom_forma
    CALL fgl_settitle("ANDE - Semanas")

	LET w = ui.WINDOW.getcurrent()
	LET f = w.getForm()
	--CALL fgl_settitle(g_programa.prog_des)
    
	CALL insert_init()
    CALL update_init()
    CALL delete_init()
    CALL combo_init()
	CALL main_menu()
END FUNCTION                                                                    

FUNCTION main_menu()                               
DEFINE  
    cuantos, 
    id, ids	   SMALLINT,
    vopciones   CHAR(255), 
    cnt 		   SMALLINT,  
    tol ARRAY[15] OF RECORD
      acc      CHAR(15),
      des_cod  SMALLINT
    END RECORD,
    aui, tb, tbi, tbs om.DomNode

    LET cnt 		= 1
    DISPLAY "SEMANAS" TO gtit_enc
    
	DISPLAY ARRAY reg_det TO sDet.*
      ATTRIBUTE(ACCEPT=FALSE,CANCEL=FALSE,UNBUFFERED)
   
      BEFORE DISPLAY
         LET cuantos = consulta(false)
         IF cuantos > 0 THEN 
            CALL dialog.setCurrentRow("sdet",1)
            LET g_reg.* = reg_det[1].*
            DISPLAY BY NAME g_reg.*
         END IF 
         CALL encabezado("")
         --CALL info_usuario()
         
      BEFORE ROW 
         LET id = arr_curr()
         IF id > 0 THEN 
            LET g_reg.* = reg_det[id].*
            DISPLAY BY NAME g_reg.*
         END IF 

      --ON KEY (CONTROL-W) 
         
      ON ACTION buscar
         
         LET cuantos = consulta(true)
         IF cuantos > 0 THEN 
            LET g_reg.* = reg_det[1].*
            DISPLAY BY NAME g_reg.*
         END IF 
         CALL encabezado("")
         --CALL info_usuario()
         
      --ON ACTION primero
         --CALL fgl_set_arr_curr( 1 )
      --ON ACTION ultimo
         --CALL fgl_set_arr_curr( arr_count() )
      --ON ACTION anterior 
         --CALL fgl_set_arr_curr( arr_curr() - 1 )
      --ON ACTION siguiente 
         --CALL fgl_set_arr_curr( arr_curr() + 1 )
      ON ACTION agregar
         IF ingreso() THEN 
            LET cuantos = consulta(FALSE)
            CALL fgl_set_arr_curr( arr_count() + 1 )
            --Refrescar Pantalla
            DISPLAY ARRAY reg_det TO sDet.*
               BEFORE DISPLAY  EXIT DISPLAY 
            END DISPLAY 
         END IF
         CALL encabezado("")
         --CALL info_usuario()
      ON ACTION modificar
         IF NOT canModificar(g_reg.lnksem1) THEN
            CALL msg("Registro no puede ser modificado porque ya tiene precios asignados")
         ELSE     
            LET id = arr_curr()
            LET ids = scr_line()
            IF id > 0 THEN 
               IF modifica() THEN
                  LET reg_det[id].* = g_reg.*
                  DISPLAY reg_det[id].* TO sDet[ids].*
               END IF   
            END IF 
            CALL encabezado("")
         END IF

       ON ACTION activo
         IF g_reg.periodoActivo1 = 1 THEN 
            CALL msg("Error: Este per�odo ya es el per�odo activo")
          ELSE
            UPDATE msemanas SET periodoactivo = 0 WHERE 1=1
            UPDATE msemanas SET periodoactivo = 1 WHERE lnksem = g_reg.lnksem1
            IF sqlca.sqlcode = 0 THEN 
               CALL msg("Per�odo configurado como activo")
               LET cuantos = consulta(false)
               IF cuantos > 0 THEN 
                  LET g_reg.* = reg_det[1].*
                  DISPLAY BY NAME g_reg.*
               END IF 
               CALL encabezado("")
            ELSE 
               CALL msgError(sqlca.sqlcode," al definir per�odo como activo ")
            END IF 
         END IF    
         --CALL info_usuario()
      {ON ACTION anular
         LET id = arr_curr()
         LET ids = scr_line()         
         IF id > 0 THEN 
            IF anular() THEN
               --Si es anulacion
               --LET reg_det[id].* = g_reg.*
               --DISPLAY reg_det[id].* TO sDet[ids].*
               --Si es eliminacion
               CALL DIALOG.deleteRow("sdet", id)
               IF id = arr_count() THEN 
                  LET id = id - 1
               END IF 
               IF id > 0 THEN 
                  LET g_reg.* = reg_det[id].*
               ELSE 
                  INITIALIZE g_reg.* TO NULL
               END IF 
               DISPLAY BY NAME g_reg.*
            END IF   
         END IF }
      
      --ON ACTION reporte
         --CALL PrintReport()
      ON ACTION salir
         EXIT DISPLAY 
   END DISPLAY 
END FUNCTION

FUNCTION combo_init()
   
   --CALL combo_din2("id_commpue","SELECT * FROM commpue WHERE id_commpue > 0")
   --CALL combo_din2("jefe",'SELECT id_commempl, TRIM(nombre) || " "|| TRIM(apellido) FROM commempl WHERE id_commempl > 0')
   --CALL combo_din2("cmbjefe",'SELECT id_commempl, TRIM(nombre) || " "|| TRIM(apellido) FROM commempl WHERE id_commempl > 0')
   --CALL combo_din2("cmbpuesto","SELECT * FROM commpue WHERE id_commpue > 0")
  
END FUNCTION 

FUNCTION encabezado(gtit_enc)
DEFINE gtit_enc STRING 
  
   DISPLAY BY NAME  gtit_enc
   
END FUNCTION 

FUNCTION canModificar(lidsem)
DEFINE lidsem INTEGER

SELECT FIRST 1 c.idSem FROM msemanas a, dsemanacal b, precioxsem c
   WHERE a.lnksem = b.lnksem
   AND b.idSem = c.idSem

   IF sqlca.sqlcode = 0 THEN 
      RETURN FALSE
   ELSE 
      RETURN TRUE 
   END IF
   
END FUNCTION 