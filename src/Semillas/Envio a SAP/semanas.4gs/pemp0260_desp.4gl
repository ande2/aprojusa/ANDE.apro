################################################################################
# Funcion     : %M%
# param_id : Funcion para consulta de umdores  
# Funciones   : umd_qry_init()    -> Prepara la seleccion
#               query_umd()       -> Ingreso de criterios de busqueda
#               fetch_umd()       -> Navega sobre seleccion de datos
#               clean_up()        -> Limpia el cursor de trabajo
#               display_umd()     -> Despliega el registro en pantalla
#               inicia_null()     -> inicia param_ides a nulos
#               display_estado()  -> Despliega el estado del umdor
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        param_id de la modificacion
#
################################################################################

GLOBALS "pemp0260_glob.4gl"


FUNCTION consulta(flagConsulta)
DEFINE flagConsulta BOOLEAN 
DEFINE 
   rDet tDet,
   x,i,vcon INTEGER,
   consulta STRING  

   IF flagConsulta THEN 
      CALL encabezado("Consulta")
      --CALL info_usuario()
      --Creaci�n de la consulta
      CONSTRUCT condicion 
         ON lnksem, idCliente, ano, rango_ini, rango_fin, periodoactivo, sem_cal, 
            fec_ini, fec_fin, sem_com, sem_fac, sem_liq
         FROM lnksem1, idCliente1, ano1, rango_ini1, rango_fin1, periodoactivo, 
              sem_cal1, fec_ini1, fec_fin1, sem_com1, sem_fac1, sem_liq1 

         ON ACTION ACCEPT
            EXIT CONSTRUCT 
         ON ACTION CANCEL 
            CALL reg_det.clear()
            RETURN 0
             
      END CONSTRUCT 
   ELSE
      LET condicion = " 1=1 "
   END IF
   --Armar la consulta
   LET consulta = 
      "SELECT msemanas.lnksem, msemanas.idCliente, pemmcli.nombre, msemanas.ano, ",
      " msemanas.sem_cal, msemanas.rango_ini, msemanas.rango_fin, msemanas.periodoactivo, ",
      " msemanas.fec_ini, msemanas.fec_fin, msemanas.sem_com, msemanas.sem_fac, ",
      " msemanas.sem_liq  ",
      " FROM msemanas, pemmcli ",
      " WHERE ", condicion,
      " AND pemmcli.idcliente = msemanas.idcliente",
      " ORDER BY 1"
   
   --definir cursor con consulta de BD
   DECLARE curDet CURSOR FROM consulta
   CALL reg_det.clear()
   LET x = 0
   --Llenar el arreglo con el resultado de la consulta
   FOREACH curDet INTO rDet.*
      LET x = x + 1 
      LET reg_det [x].* = rDet.*  
   END FOREACH
   RETURN x --Cantidad de registros
END FUNCTION
