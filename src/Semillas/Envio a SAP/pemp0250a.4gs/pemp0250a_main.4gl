################################################################################
# Funcion     : %M%
# Descripcion : Modulo Interfaz ANDE JAVA SAP
# Funciones   : main_init(dbname)
#               main_menu()                               
#               dummy() 
#               
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Luis de Leon  
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################

GLOBALS
    DEFINE 
       dbname      STRING,
        n_param 	SMALLINT,
        prog_name   STRING,
        prog_name2 	STRING,
        GIdBoletaIng INTEGER

    -- Registro de resultados de retorno SAP
    DEFINE result RECORD
      numReciboPro      VARCHAR(255,1),
      numSalidaInv      VARCHAR(255,1) ,
      numEntradaInv     VARCHAR(255,1) ,
      numEntMerExp      VARCHAR(255,1) ,
      numEntMerLoc      VARCHAR(255,1) ,
      ctaConSalida      VARCHAR(255,1) ,
      ctaConEntrada     VARCHAR(255,1) 
    END RECORD 

    -- Registro de Encabezado datos SAP
    DEFINE reg_mdatosSAP RECORD
      idBoletaIng          INTEGER,
      tipDoc               SMALLINT,
      ipServidorBD         VARCHAR(255,1),
      userServirdorBD      VARCHAR(255,1),
      passuserServidorBD   VARCHAR(255,1),
      ipServirdorSAP       VARCHAR(255,1),
      nombreBD             VARCHAR(255,1),
      usuarioSAP           VARCHAR(255,1),
      contrasenaSAP        VARCHAR(255,1),
      numordpro            VARCHAR(255,1),
      fechaconta           DATE,
      serrecpro            VARCHAR(255,1),
      sersalmer            VARCHAR(255,1),
      serentmer            VARCHAR(255,1),
      comentariosdoc       VARCHAR(255,1),
      codigosocioneg       VARCHAR(255,1),
      nombresocioneg       VARCHAR(255,1), 
      numReciboProSAP      VARCHAR(255,1),
      numSalidaInvSAP      VARCHAR(255,1),
      numEntradaInvSAP     VARCHAR(255,1),
      numEntMercExpSAP     VARCHAR(255,1),
      numEntMercLocSAP     VARCHAR(255,1),
      estado               SMALLINT      
    END RECORD

    -- Registro de Detalle datos SAP
    DEFINE reg_ddatosSAP RECORD
      idBoletaIng          INTEGER,
      numordpro            VARCHAR(255,1),
      tipoart              SMALLINT,
      codart               VARCHAR(255,1),
      descart              VARCHAR(255,1),
      costoart             DECIMAL(18,2),
      cantart              SMALLINT,
      codalmart            VARCHAR(255,1),
      cuentaafe            VARCHAR(255,1)
    END RECORD
    
END GLOBALS

MAIN
	DEFER INTERRUPT

	OPTIONS
		INPUT  WRAP,
		HELP KEY CONTROL-W,
		COMMENT LINE OFF,
		PROMPT LINE LAST - 2,
		MESSAGE LINE LAST - 1,
		ERROR LINE LAST

   LET GIdBoletaIng = NULL
	LET n_param = num_args()

	--IF n_param = 0 THEN
		--RETURN
	--ELSE
      --  LET dbname = arg_val(1)
		CONNECT TO 'db0001' --dbname
      
	--END IF

   IF GIdBoletaIng IS NULL THEN
      LET GIdBoletaIng = 0
   END IF
   
   LET prog_name = "pemp0250"
	LET prog_name2 = prog_name||".log"   
    
	CALL STARTLOG(prog_name2)
	CALL main_init()
END MAIN

FUNCTION main_init()
DEFINE 
	nom_forma 	STRING,
    w           ui.Window,
    f           ui.Form
	  
	INITIALIZE reg_mdatosSAP.*, reg_ddatosSAP TO NULL

    CALL ui.Interface.loadActionDefaults("actiondefaults")
    CALL ui.Interface.loadStyles("styles_sc")

	--LET nom_forma = prog_name CLIPPED, "_form"
   CLOSE WINDOW SCREEN 
	OPEN WINDOW w1 WITH FORM "pemp0250_form1"
    --CALL fgl_settitle("ANDE - Interfaz Java-SAP")

	--LET w = ui.WINDOW.getcurrent()
	--LET f = w.getForm()

   --CALL CreaTablaTemporal()
	CALL main_menu()
    
END FUNCTION                                                                    

FUNCTION main_menu()                               

   MENU
     ON ACTION enviar
        PROMPT 'Numero de boleta ' FOR GIdBoletaIng --= arg_val(2) 
        CALL FuncInterfazJavaSAP()  
     ON ACTION salir
        EXIT MENU 
   END MENU 
   
   
END FUNCTION

FUNCTION FuncInterfazJavaSAP()
DEFINE VCursorQuery STRING
DEFINE VCondicion1 STRING
DEFINE VStringCMD STRING
DEFINE VResCMD STRING
DEFINE VComandoJava VARCHAR(255,1)
DEFINE VTipoOpe SMALLINT
DEFINE VBandera SMALLINT
DEFINE VContador SMALLINT


   -- Selecciona el comando JAVA con el PATH de ejecucion
   LET VComandoJava = NULL
   SELECT glb_paramtrs.valchr
     INTO VComandoJava
     FROM glb_paramtrs
    WHERE glb_paramtrs.numpar = 15
      AND glb_paramtrs.tippar = 1

   LET VComandoJava = '"C:\\\\Program Files (x86)\\\\Java\\\\jre7\\\\bin\\\\java" -jar C:\\\\temp\\\\dist\\\\sc_conector_sap.jar'

   DISPLAY "COMANDO JAVA: ",VComandoJava CLIPPED
   
   -- Cursor para el Encabezado de los Datos SAP
   LET VCursorQuery = "SELECT tb1.* ",
                      "  FROM pemmDatosSap tb1 ",
                      " WHERE tb1.estado = 0 "
                      
   LET VCondicion1 = NULL
   IF GIdBoletaIng = 0 THEN
      LET VCondicion1 = NULL
   ELSE
      LET VCondicion1 = "AND tb1.IdBoletaIng = ",GIdBoletaIng
   END IF

   LET VCursorQuery = VCursorQuery," ",VCondicion1

   DISPLAY "QUERY: ",VCursorQuery CLIPPED
   
   LET VStringCMD = NULL

   BEGIN WORK

   DECLARE CursorQuerySAP CURSOR FROM VCursorQuery
   FOREACH CursorQuerySAP INTO reg_mdatosSAP.*

      IF reg_mdatosSAP.tipDoc = 2 THEN -- PRODUCTOR
         LET VTipoOpe = 3 -- Entradas de mercancía local y extranjero
      ELSE -- FINCA
         -- Selecciona el tipo de Operacion segun documentos
         CASE
            WHEN reg_mdatosSAP.numReciboProSAP IS NULL AND
                 reg_mdatosSAP.numSalidaInvSAP IS NULL AND
                 reg_mdatosSAP.numEntradaInvSAP IS NULL 
                 LET VTipoOpe = 2 -- Proceso completo (recibo de producción, salida de mercadería y entrada de mercadería)
                                  
            WHEN reg_mdatosSAP.numReciboProSAP IS NOT NULL AND
                 reg_mdatosSAP.numSalidaInvSAP IS NULL AND
                 reg_mdatosSAP.numEntradaInvSAP IS NULL 
                 LET VTipoOpe = 4 -- Proceso parcial (salida de mercadería y entrada de mercadería)
                 
            WHEN reg_mdatosSAP.numReciboProSAP IS NOT NULL AND
                 reg_mdatosSAP.numSalidaInvSAP IS NOT NULL AND
                 reg_mdatosSAP.numEntradaInvSAP IS NULL 
                 LET VTipoOpe = 5 -- Proceso parcial (entrada de mercadería)

            OTHERWISE
               LET VTipoOpe = 0
         END CASE
      END IF

      -- Arma el string con los datos a trasladar 
      LET VStringCMD = VComandoJava CLIPPED," ",VTipoOpe USING '<<<<<<&'," ",
         reg_mdatosSAP.ipServidorBD CLIPPED," ",      
         reg_mdatosSAP.userServirdorBD CLIPPED," ",   
         reg_mdatosSAP.passuserServidorBD CLIPPED," ",
         reg_mdatosSAP.ipServirdorSAP CLIPPED," ",    
         '"',reg_mdatosSAP.nombreBD CLIPPED,'"'," ",          
         reg_mdatosSAP.usuarioSAP CLIPPED," ",        
         reg_mdatosSAP.contrasenaSAP CLIPPED," "  
         
      CASE 
         WHEN VTipoOpe = 2 OR VTipoOpe = 4 OR VTipoOpe = 5
            LET VStringCMD = VStringCMD CLIPPED," ",
            reg_mdatosSAP.numordpro CLIPPED," ",         
            reg_mdatosSAP.fechaconta USING 'DD-MM-YYYY' CLIPPED," ",
            reg_mdatosSAP.serrecpro CLIPPED," ",         
            reg_mdatosSAP.sersalmer CLIPPED," ",               
            reg_mdatosSAP.serentmer CLIPPED," ",  
            "1 ", --FuncNumeroArticulosExp(reg_mdatosSAP.idBoletaIng, reg_mdatosSAP.numordpro) USING '<<<<<<&'," ",
            reg_mdatosSAP.comentariosdoc CLIPPED," "   
         WHEN VTipoOpe = 3
            LET VStringCMD = VStringCMD CLIPPED," ",
            reg_mdatosSAP.fechaconta USING 'DD-MM-YYYY' CLIPPED," ",        
            reg_mdatosSAP.serentmer CLIPPED," ",
            reg_mdatosSAP.codigosocioneg CLIPPED," ",    
            '"',reg_mdatosSAP.nombresocioneg CLIPPED,'"'," ",    
            '"',reg_mdatosSAP.comentariosdoc CLIPPED,'"'," ",
            '"',reg_mdatosSAP.comentariosdoc CLIPPED,'"'," ",  
            FuncNumeroArticulosExp(reg_mdatosSAP.idBoletaIng) USING '<<<<<<&'," "
      END CASE

      
      DECLARE CursorQueryDetSAP CURSOR FOR
         SELECT tb1.* 
           FROM pemdDatosSap tb1
          WHERE tb1.IdBoletaIng = reg_mdatosSAP.idBoletaIng
         ORDER BY tb1.tipoart DESC -- 0 = LOCAL, 1 = EXPO

      FOREACH CursorQueryDetSAP INTO reg_ddatosSAP.*
         CASE 
            WHEN VTipoOpe = 2 OR VTipoOpe = 4 OR VTipoOpe = 5
               IF reg_ddatosSAP.tipoart = 1 THEN -- EXPORTACION
                  LET VStringCMD = VStringCMD CLIPPED," ",
                     reg_ddatosSAP.codart CLIPPED," ",
                     '"',reg_ddatosSAP.descart CLIPPED,'"'," ",
                     reg_ddatosSAP.costoart USING '<<<<<<<<<<<<<<<&.&&'," ",
                     reg_ddatosSAP.cantart USING '<<<<<<&'," ",
                     reg_ddatosSAP.codalmart CLIPPED," ",
                     reg_ddatosSAP.cuentaafe CLIPPED
               ELSE -- LOCAL
                  LET VStringCMD = VStringCMD CLIPPED," ",
                     reg_ddatosSAP.codart CLIPPED," ",
                     '"',reg_ddatosSAP.descart CLIPPED,'"'," ",
                     reg_ddatosSAP.costoart USING '<<<<<<<<<<<<<<<&.&&'," ",
                     reg_ddatosSAP.cantart USING '<<<<<<&'," ",
                     reg_ddatosSAP.codalmart CLIPPED
               END IF
            WHEN VTipoOpe = 3
               LET VStringCMD = VStringCMD CLIPPED," ",
                  reg_ddatosSAP.codart CLIPPED," ",
                  '"',reg_ddatosSAP.descart CLIPPED,'"'," ",
                  reg_ddatosSAP.costoart USING '<<<<<<<<<<<<<<<&.&&'," ",
                  reg_ddatosSAP.cantart USING '<<<<<<&'," ",
                  reg_ddatosSAP.codalmart CLIPPED
         END CASE
         
      END FOREACH
      CLOSE CursorQueryDetSAP
      FREE CursorQueryDetSAP

      CALL ui.interface.frontcall("standard","shellexec",[VStringCMD],[VResCMD])
      DISPLAY "Resultado ",VResCMD
      DISPLAY "String: ",VStringCMD

      --SLEEP 30
      LET VBandera = 0
      LET VContador = 0
      WHILE VBandera = FALSE
         SLEEP 5
         CALL LeeResultadoSAP(VTipoOpe, reg_mdatosSAP.comentariosdoc) RETURNING VBandera
         IF VBandera = 0 THEN
            LET VContador = VContador + 1
            DISPLAY "CONTADOR: ",VContador
            IF VContador = 40 THEN
               EXIT WHILE
            END IF
         ELSE
            EXIT WHILE
         END IF
      END WHILE
      
      -- Actualizacion de numeros de Documentos
      CASE VTipoOpe
         WHEN 2
            UPDATE pemmDatosSap
               SET numReciboProSAP = result.numReciboPro,
                   numSalidaInvSAP = result.numSalidaInv,
                   numEntradaInvSAP = result.numEntradaInv
             WHERE pemmDatosSap.IdBoletaIng = reg_mdatosSAP.idBoletaIng
         
         WHEN 3
            UPDATE pemmDatosSap
               SET numEntMercExpSAP = result.numEntMerExp,
                   numEntMercLocSAP = result.numEntMerLoc
             WHERE pemmDatosSap.IdBoletaIng = reg_mdatosSAP.idBoletaIng
         
         WHEN 4
            UPDATE pemmDatosSap
               SET numSalidaInvSAP = result.numSalidaInv,
                   numEntradaInvSAP = result.numEntradaInv
             WHERE pemmDatosSap.IdBoletaIng = reg_mdatosSAP.idBoletaIng
         
         WHEN 5
            UPDATE pemmDatosSap
               SET numEntradaInvSAP = result.numEntradaInv
             WHERE pemmDatosSap.IdBoletaIng = reg_mdatosSAP.idBoletaIng
         
      END CASE
      DISPLAY "Entre al foreach"
   END FOREACH
   DISPLAY "Al salir del foreach"
   CLOSE CursorQuerySAP
   FREE CursorQuerySAP
   
   COMMIT WORK

   UNLOAD TO 'archivo.txt'
   SELECT * FROM pemmDatosSap
   
END FUNCTION
{
FUNCTION CreaTablaTemporal()

   CREATE TEMP TABLE pemmDatosSap
   (
      idBoletaIng          INTEGER,
      tipDoc               SMALLINT,
      ipServidorBD         VARCHAR(255,1),
      userServirdorBD      VARCHAR(255,1),
      passuserServidorBD   VARCHAR(255,1),
      ipServirdorSAP       VARCHAR(255,1),
      nombreBD             VARCHAR(255,1),
      usuarioSAP           VARCHAR(255,1),
      contrasenaSAP        VARCHAR(255,1),
      numordpro            VARCHAR(255,1),
      fechaconta           DATE,
      serrecpro            VARCHAR(255,1),
      sersalmer            VARCHAR(255,1),
      serentmer            VARCHAR(255,1),
      comentariosdoc       VARCHAR(255,1),
      codigosocioneg       SMALLINT,
      nombresocioneg       VARCHAR(255,1),
      numReciboProSAP      VARCHAR(255,1),
      numSalidaInvSAP      VARCHAR(255,1),
      numEntradaInvSAP     VARCHAR(255,1),
      numEntMercExpSAP     VARCHAR(255,1),
      numEntMercLocSAP     VARCHAR(255,1),
      estado               SMALLINT
   ) WITH NO LOG

   INSERT INTO pemmDatosSap
   VALUES(1001, 2, "192.168.0.2", "sa", "Kmaleon2", "192.168.0.2", "WPruebas Semillas", "manager", "0313231719", "5453",
   "21-04-2014", "195", "196", "195","RC-T100",NULL,NULL,NULL,NULL,NULL,NULL,NULL,0)

   CREATE TEMP TABLE pemdDatosSap
   (
      idBoletaIng          INTEGER,
      numordpro            VARCHAR(255,1),
      tipoart              SMALLINT,
      codart               VARCHAR(255,1),
      descart              VARCHAR(255,1),
      costoart             DECIMAL(18,2),
      cantart              SMALLINT,
      codalmart            VARCHAR(255,1),
      cuentaafe            VARCHAR(255,1)
   ) WITH NO LOG

   INSERT INTO pemdDatosSap
   VALUES(1001, "5453", 1, "140.042.005", "CHILE BLOCKY (LIBRA) FINCAS CM", 2.22, 200, "SR-0400", "_SYS00000001152")  
   INSERT INTO pemdDatosSap
   VALUES(1001, "5453", 0, "305.240.003", "TOMATE CHERRY (LB) EXPORTACIÓN", 5.85, 100, "PH-0200",NULL)  
   
END FUNCTION
}

FUNCTION FuncNumeroArticulosExp(VIdBoletaIng)
DEFINE VIdBoletaIng INTEGER
DEFINE VCuentaReg SMALLINT

   LET VCuentaReg = 0
   
   SELECT COUNT(*)
     INTO VCuentaReg
     FROM pemdDatosSap
    WHERE pemdDatosSap.IdBoletaIng = VIdBoletaIng
      AND pemdDatosSap.tipoart = 1

   IF VCuentaReg IS NULL THEN
      LET VCuentaReg = 0
   END IF
   
   RETURN VCuentaReg

END FUNCTION

FUNCTION LeeResultadoSAP(VTipoOpe, VComentBoleta)
DEFINE VTipoOpe SMALLINT
DEFINE VComentBoleta VARCHAR(255,1)
DEFINE PathWin , PathUnix STRING
DEFINE texto STRING
DEFINE accion DYNAMIC ARRAY OF CHAR(3)
DEFINE i INTEGER
DEFINE VBandera SMALLINT
DEFINE VStringCMD, VResCMD STRING

   DISPLAY "LEER RESULTADOS.........",VComentBoleta CLIPPED,"OPE: ",VTipoOpe
   
	CALL accion.clear()
   INITIALIZE result.* TO NULL
   LET texto = NULL
   LET VBandera = 0

	CASE VTipoOpe
		WHEN 2
			LET accion[1] = "-01"
			LET accion[2] = "-02"
			LET accion[3] = "-03"
		WHEN 3
			LET accion[4] = "-04"
         LET accion[5] = "-05"
		WHEN 4
			LET accion[2] = "-02"
			LET accion[3] = "-03"
		WHEN 5
         LET accion[3] = "-03"
		OTHERWISE
			LET accion = NULL
	END CASE

   DISPLAY "ACCION LENGHT: ",accion.getLength()
   
	FOR i = 1 TO accion.getLength()
		IF accion[i] IS NULL THEN
			CONTINUE FOR
		END IF
		LET PathWin = "C:\\ANDE\\FILES\\",VComentBoleta CLIPPED,accion[i] CLIPPED
		LET PathUnix = FGL_GETENV("HOME")
		LET PathUnix = PathUnix CLIPPED,"/",VComentBoleta CLIPPED,accion[i] CLIPPED
      DISPLAY "PATHWIN: ",PathWin
      DISPLAY "PATHUNIX: ",PathUnix

      WHENEVER ERROR CONTINUE
		CALL librut001_getfile(PathWin,PathUnix)
      WHENEVER ERROR STOP

      WHENEVER ERROR CONTINUE
		LET texto = librut001_readfile(PathUnix,1)
      WHENEVER ERROR STOP

		DISPLAY "texto: ",texto
      CASE 
         WHEN VTipoOpe = 2  OR VTipoOpe = 4 OR VTipoOpe = 5
            IF accion[i] = '-03' THEN
               IF LENGTH(texto) > 0 THEN
                  LET VBandera = 1
               END IF
            END IF
         WHEN VTipoOpe = 3
            IF accion[i] = '-05' THEN
               IF LENGTH(texto) > 0 THEN
                  LET VBandera = 1
               END IF
            END IF
      END CASE
            
               
		CASE i
			WHEN 1
				LET result.numReciboPro = texto
			WHEN 2
				LET result.numSalidaInv = texto
			WHEN 3
				LET result.numEntradaInv = texto
			WHEN 4
				LET result.numEntMerExp = texto
			WHEN 5
				LET result.numEntMerLoc = texto
		END CASE
      
      --CALL box_valdato(texto||' '||i)
      
      --LET VStringCMD = 'del ',PathWin
      --CALL ui.interface.frontcall("standard","shellexec",[VStringCMD],[VResCMD])

	END FOR	

   DISPLAY 'VBandera: ',VBandera
   
   RETURN VBandera
   
END FUNCTION
