################################################################################
# Funcion     : %M%
# Descripcion : Modulo Principal para umdores 
# Funciones   : main_init(dbname)
#               main_menu()                               
#               dummy() 
#               
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Anderson Garcia  
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################

GLOBALS "pemp0250_glob.4gl"

DEFINE VIdBoletaIng, vlEstado INTEGER 
DEFINE cuantos SMALLINT 

MAIN
	DEFINE 
		n_param 	SMALLINT,
		prog_name2 	STRING
		
	DEFER INTERRUPT

	OPTIONS
		INPUT  WRAP,
		HELP KEY CONTROL-W,
		COMMENT LINE OFF,
		PROMPT LINE LAST - 2,
		MESSAGE LINE LAST - 1,
		ERROR LINE LAST

	--LET n_param = num_args()

	--IF n_param = 0 THEN
		--RETURN
	--ELSE
        --LET dbname = arg_val(1) 
      LET dbname = "db0001"
      CONNECT TO dbname
	--END IF

	LET prog_name2 = prog_name||".log"   -- El progr_name es definido como constante en el arch. globals
    
	CALL STARTLOG(prog_name2)
	CALL main_init()
END MAIN

FUNCTION main_init()
DEFINE 
	nom_forma 	STRING,
    w           ui.Window,
    f           ui.Form
	  
	INITIALIZE u_reg.* TO NULL
	LET g_reg.* = u_reg.*

   --CALL trae_usuario() returning g_usuario.*
   --CALL trae_empresa(dbname) returning g_empresa.*
   --CALL trae_programa(prog_name) returning g_programa.*
   --IF g_programa.prog_id = 0 OR 
      --g_programa.prog_id IS NULL THEN
      --CALL box_valdato("Programa no existente, contacte a sistemas")
      --RETURN 
   --END IF 
    CALL ui.Interface.loadActionDefaults("actiondefaults")
    CALL ui.Interface.loadStyles("styles_sc")
   --CALL ui.Interface.loadToolbar("toolbar")

	LET nom_forma = prog_name CLIPPED, "_form"
    CLOSE WINDOW SCREEN 

	OPEN WINDOW w1 WITH FORM nom_forma
   CALL fgl_settitle("ANDE - Env�o Datos SAP")

	LET w = ui.WINDOW.getcurrent()
	LET f = w.getForm()
	--CALL fgl_settitle(g_programa.prog_des)
    
	{CALL insert_init()
    CALL update_init()
    CALL delete_init()}
   CALL combo_init()
	CALL main_menu()
END FUNCTION                                                                    

FUNCTION main_menu()                               
DEFINE  
     
    id, ids, sl	   SMALLINT,
    vopciones   CHAR(255), 
    cnt 		   SMALLINT,  
    tol ARRAY[15] OF RECORD
      acc      CHAR(15),
      des_cod  SMALLINT
    END RECORD,
    aui, tb, tbi, tbs om.DomNode

DEFINE var SMALLINT     

    LET cnt 		= 1
    DISPLAY "Env�o Datos SAP" TO gtit_enc
    
	DISPLAY ARRAY reg_det TO sDet.*
      ATTRIBUTE(ACCEPT=FALSE,CANCEL=FALSE,UNBUFFERED)
   
      BEFORE DISPLAY
         LET cuantos = consulta(false)
         IF cuantos > 0 THEN 
            CALL dialog.setCurrentRow("sdet",1)
            LET g_reg.* = reg_det[1].*
            CALL displayDetalle()
         END IF 
         CALL encabezado("")
         --CALL info_usuario()
         
      BEFORE ROW 
         LET id = arr_curr()
         IF id > 0 THEN 
            --LET g_reg.* = reg_det[id].*
            LET g_reg.* = reg_det[id].*
            CALL displayDetalle()
         END IF 

      --ON KEY (CONTROL-W) 

      --Si va, solo lo comente para desarrollo
      --ON IDLE 30
        --CALL actualizar()
     
      ON ACTION buscar
         
         LET cuantos = consulta(true)
         IF cuantos > 0 THEN 
            LET g_reg.* = reg_det[1].*
            CALL displayDetalle()
         END IF 
         CALL encabezado("")

      ON ACTION valida
         LET cuantos = validaEnvioEnc(reg_det[arr_curr()].idboletaing)  
         LET cuantos = consulta(false)
         IF cuantos > 0 THEN 
            LET g_reg.* = reg_det[1].*
            CALL displayDetalle()
            DISPLAY "pase"
            CALL encabezado("") 
         END IF
         DISPLAY "Obs ", g_reg.obs
         --CALL info_usuario()
         
      --ON ACTION primero
         --CALL fgl_set_arr_curr( 1 )
      --ON ACTION ultimo
         --CALL fgl_set_arr_curr( arr_count() )
      --ON ACTION anterior 
         --CALL fgl_set_arr_curr( arr_curr() - 1 )
      --ON ACTION siguiente 
         --CALL fgl_set_arr_curr( arr_curr() + 1 )

       ON ACTION actualiza
          CALL actualizar()
          
         
       ON ACTION carga
         --Se llama en causas de rechazo, antes de actualizar el estado de la boleta
         PROMPT "Boleta " FOR VIdBoletaIng
         SELECT estado INTO vlEstado FROM pemmboleta WHERE idboletaing = vIdBoletaIng
         IF vlEstado <> 0 THEN
            CALL msg("Boleta no est� cerrada")
         ELSE 
            CALL GeneraInformacionDatosSap(VIdBoletaIng)  
         END IF 
         --CALL GeneraInformacionDatosSap(VIdBoletaIng)
         LET cuantos = consulta(false) 
         
       ON ACTION Enviar
         LET id = arr_curr()
         LET sl = scr_line()
         LET reg_det[id].obs = "Enviando...."
         DISPLAY reg_det[id].obs TO sDet[sl].obs
         CALL FuncInterfazJavaSAP(reg_det[id].idboletaing)   

       ON ACTION verifica
          LET id = arr_curr() 
          CALL leeResultadoEnvio(reg_det[id].idboletaing,0)  
          LET cuantos = consulta(false)
      {ON ACTION agregar
         IF ingreso() THEN 
            LET cuantos = consulta(FALSE)
            CALL fgl_set_arr_curr( arr_count() + 1 )
            --Refrescar Pantalla
            DISPLAY ARRAY reg_det TO sDet.*
               BEFORE DISPLAY  EXIT DISPLAY 
            END DISPLAY 
         END IF
         CALL encabezado("")
         --CALL info_usuario()}
      {ON ACTION modificar
         LET id = arr_curr()
         LET ids = scr_line()
         IF id > 0 THEN 
            IF modifica() THEN
               LET reg_det[id].* = g_reg.*
               DISPLAY reg_det[id].* TO sDet[ids].*
            END IF   
         END IF 
         CALL encabezado("")
         --CALL info_usuario()}
      {ON ACTION anular
         LET id = arr_curr()
         LET ids = scr_line()         
         IF id > 0 THEN 
            IF anular() THEN
               --Si es anulacion
               --LET reg_det[id].* = g_reg.*
               --DISPLAY reg_det[id].* TO sDet[ids].*
               --Si es eliminacion
               CALL DIALOG.deleteRow("sdet", id)
               IF id = arr_count() THEN 
                  LET id = id - 1
               END IF 
               IF id > 0 THEN 
                  LET g_reg.* = reg_det[id].*
               ELSE 
                  INITIALIZE g_reg.* TO NULL
               END IF 
               CALL displayDetalle() 
            END IF   
         END IF }
      
      --ON ACTION reporte
         --CALL PrintReport()
      ON ACTION salir
         EXIT DISPLAY 
   END DISPLAY 
END FUNCTION

FUNCTION actualizar()
DEFINE var SMALLINT 

  DECLARE bolcur CURSOR FOR
     --lee las boletas que ya esten cerradas y las carga a tabla para envio a SAP 
     SELECT idboletaing FROM pemmboleta WHERE estado = 0 
     AND fecha >= TODAY - 40
     FOREACH bolcur INTO VIdBoletaIng 
       SELECT idboletaing FROM pemmdatossap WHERE idboletaing = VIdBoletaIng
       IF sqlca.sqlcode = 100 THEN
          CALL GeneraInformacionDatosSap(VIdBoletaIng)
       END IF 
       
     END FOREACH 
    --lee resultados para boletas ya procesadas 
    DECLARE bolcur2 CURSOR FOR
     SELECT idboletaing FROM pemmdatossap 
      WHERE fechaconta >= TODAY - 40 
        AND (numreciboprosap IS NULL 
         OR numsalidainvsap IS NULL 
         OR numentradainvsap IS NULL
         OR numentmercexpsap IS NULL
         OR numentmercexpsap IS NULL) 
     FOREACH bolcur2 INTO VIdBoletaIng 
       CALL leeResultadoEnvio(VIdBoletaIng,1)  
     END FOREACH 
     DECLARE curvalida CURSOR FOR SELECT * FROM pemmdatossap WHERE fechaconta >= TODAY - 40 
     FOREACH curvalida INTO VIdBoletaIng  
         LET var = validaEnvioEnc(VIdBoletaIng)
     END FOREACH     
     LET cuantos = consulta(FALSE) 
END FUNCTION 

FUNCTION combo_init()
   --CALL combo_din2("idfamilia","SELECT * FROM glbmfam")
   --CALL combo_din2("id_commpue","SELECT * FROM commpue WHERE id_commpue > 0")
   --CALL combo_din2("jefe",'SELECT id_commempl, TRIM(nombre) || " "|| TRIM(apellido) FROM commempl WHERE id_commempl > 0')
   --CALL combo_din2("cmbjefe",'SELECT id_commempl, TRIM(nombre) || " "|| TRIM(apellido) FROM commempl WHERE id_commempl > 0')
   --CALL combo_din2("cmbpuesto","SELECT * FROM commpue WHERE id_commpue > 0")
   --CALL combo_din2("cmbfamilia","SELECT * FROM glbmfam")
END FUNCTION 

FUNCTION encabezado(gtit_enc)
DEFINE gtit_enc STRING 

   DISPLAY BY NAME  gtit_enc
END FUNCTION 

FUNCTION displayDetalle()
  DISPLAY g_reg.idboletaing      TO fidboletaing
  DISPLAY g_reg.numordpro        TO fnumordpro
  DISPLAY g_reg.codigosocioneg   TO fcodigosocioneg
  DISPLAY g_reg.nombresocioneg   TO fnombresocioneg
  DISPLAY g_reg.comentariosdoc   TO fcomentariosdoc
  DISPLAY g_reg.fechaconta       TO ffechaconta
  DISPLAY g_reg.estado           TO festado
  DISPLAY g_reg.numreciboprosap  TO fnumreciboprosap
  DISPLAY g_reg.numsalidainvsap  TO fnumsalidainvsap
  DISPLAY g_reg.numentradainvsap TO fnumentradainvsap
  DISPLAY g_reg.numentmercexpsap TO fnumentmercexpsap
  DISPLAY g_reg.numentmerclocsap TO fnumentmerclocsap
  DISPLAY g_reg.obs              TO fobs
END FUNCTION 