################################################################################
# Funcion     : %M%
# param_id : Funcion para consulta de umdores  
# Funciones   : umd_qry_init()    -> Prepara la seleccion
#               query_umd()       -> Ingreso de criterios de busqueda
#               fetch_umd()       -> Navega sobre seleccion de datos
#               clean_up()        -> Limpia el cursor de trabajo
#               display_umd()     -> Despliega el registro en pantalla
#               inicia_null()     -> inicia param_ides a nulos
#               display_estado()  -> Despliega el estado del umdor
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        param_id de la modificacion
#
################################################################################

GLOBALS "pemp0250_glob.4gl"


FUNCTION consulta(flagConsulta)
DEFINE flagConsulta BOOLEAN 
DEFINE 
   rDet tDet,
   x,i,vcon INTEGER,
   consulta STRING 
DEFINE var SMALLINT    

   IF flagConsulta THEN 
      CALL encabezado("Consulta")
      --CALL info_usuario()
      --Creaci�n de la consulta
      CONSTRUCT condicion 
         ON idBoletaIng, numOrdPro, codigoSocioNeg, nombreSocioNeg,
            comentariosDoc, fechaConta, estado,
            numReciboProSAP, numSalidaInvSAP, numEntradaInvSAP,
            numEntMercExpSAP, numEntMercLocSAP
         FROM fidboletaIng, fnumordpro, fcodigosocioneg, fnombresocioneg,
            fcomentariosdoc, ffechaconta, festado,
            fnumreciboprosap, fnumsalidainvsap, fnumentradainvsap,
            fnumentmercexpsap, fnumentmerclocsap

         ON ACTION ACCEPT
            EXIT CONSTRUCT 
         ON ACTION CANCEL 
            CALL reg_det.clear()
            RETURN 0
             
      END CONSTRUCT 
      
   ELSE
      LET condicion = " 1=1"
      
   END IF 

   IF condicion = " 1=1" THEN 
      LET condicion = " s.idBoletaIng NOT IN ( ",
                      " SELECT m.idboletaing ",
                      " FROM  pemdboletaloc d, pemmboletaloc m, itemsxgrupo i, pemmboleta b ",
                      " WHERE  d.idboletaloc = m.idboletaloc ",
                      " AND d.lnkitem = i.lnkitem ",
                      " AND m.idboletaing = b.idboletaing ",
                      " AND i.nombre matches '*DEVOLUCI?N' ",
                      " AND b.pesototal = d.pesoneto ",
                      " AND b.fecha >= TODAY - 40 ) " 
   END IF
   --Armar la consulta
   LET consulta = 
      "SELECT s.idBoletaIng, s.numOrdPro, s.codigoSocioNeg, s.nombreSocioNeg, ",
      "       s.comentariosDoc, s.fechaConta, s.estado, ",
      "       s.numReciboProSAP, s.numSalidaInvSAP, s.numEntradaInvSAP, ",
      "       s.numEntMercExpSAP, s.numEntMercLocSAP, s.obs ",
      " FROM pemMDatosSAP s ",
      " WHERE ", condicion,
      --" AND 
      " ORDER BY 1 desc "
   
   --definir cursor con consulta de BD
   DECLARE curDet CURSOR FROM consulta
   CALL reg_det.clear()
   LET x = 0
   --Llenar el arreglo con el resultado de la consulta
   FOREACH curDet INTO rDet.*
      
      LET x = x + 1 
      LET reg_det [x].* = rDet.*  
   END FOREACH
   RETURN x --Cantidad de registros
END FUNCTION
