################################################################################
# Funcion     : %M%
# nombre      : Catalogo de Familias
#               Funcion para definicion de globales
# Funciones   : 
#               
#               
#  
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        nombre de la modificacion
#
################################################################################

SCHEMA db0001

GLOBALS 
TYPE 
   tEnc RECORD 
      idSem       INTEGER ,
      categoria   INTEGER , --1=Exportación, 2=Local
      tipo        INTEGER  --tipo de cultivo
   END RECORD 

TYPE 
   tDet RECORD 
      idSem       INTEGER ,
      lnkItem     INTEGER , --1=Exportación, 2=Local
      idCateg     SMALLINT ,
      idTipo      INTEGER ,
      idItemSapFac LIKE itemsxgrupo.iditemsapfac,
      nombrefac   LIKE itemsxgrupo.nombrefac,
      precioBruto DECIMAL (6,4), --se ingresa
      precioNeto  DECIMAL (6,4), --se calcula
      precioLibra DECIMAL (6,4) 
   END RECORD 
   
{      lnksem1   INTEGER,
      idcliente1 INTEGER,
      nombre1   VARCHAR(50,1),
      ano1      SMALLINT,
      sem_cal   INTEGER ,
      rango_ini1 DATE ,
      rango_fin1 DATE ,
      fec_ini1   DATE,
      fec_fin1   DATE ,
      sem_com1  SMALLINT,
      sem_fac1  SMALLINT,
      sem_liq1  SMALLINT
      
   END RECORD}

DEFINE
   rDet tDet,
   reg_det DYNAMIC ARRAY OF tDet, 
   g_reg2, g_reg, u_reg tEnc,
   dbname      STRING,
   
   condicion   STRING --Condicion de la clausula Where 
   CONSTANT    prog_name = "pemp0263"
END GLOBALS