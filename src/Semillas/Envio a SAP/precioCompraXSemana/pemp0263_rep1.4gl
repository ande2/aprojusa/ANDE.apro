GLOBALS "pemp0263_glob.4gl"
DEFINE sSem DYNAMIC ARRAY OF SMALLINT
DEFINE gRep RECORD LIKE precioxsem.*
{DEFINE gr_commdep RECORD
  id_commdep  LIKE commdep.id_commdep,
  descripcion LIKE commdep.descripcion,
  hijo_de     LIKE commdep.hijo_de
END RECORD 
}
{MAIN

  MENU "Reporte"
    COMMAND "Generar"
      DATABASE db0001 
      CALL genera()
    COMMAND "Salir"
      EXIT MENU 
  END MENU 

END MAIN} 

FUNCTION repPrecioCompra()
  
  DEFINE nSem, pos SMALLINT   
  --DEFINE pos SMALLINT
  --DEFINE ant LIKE commdep.hijo_de
  --DEFINE pLine VARCHAR(100)
  --DEFINE pLine VARCHAR(100)

  
     
   --Para precios por semana
   {SELECT b.idsem, c.idtipo, e. nombre, b.sem_com,  c.lnkItem, d.nombrefac, c.precioBruto
   FROM  msemanas a, outer (dsemanacal b, outer (precioxsem c, itemsxgrupo d, glbmtip e))
   WHERE a.lnksem = b.lnksem
   AND b.idSem = c.idsem
   AND c.lnkitem = d.lnkitem
   AND c.idtipo = e.idtipo
   AND a.periodoactivo = 1
   ORDER BY b.idsem, b.sem_com, c.lnkitem}

  
  START REPORT rPrecios
  DECLARE ccursor CURSOR FOR 
   --Para semanas
   SELECT b.sem_com
   FROM  msemanas a, outer dsemanacal b --, outer (precioxsem c))
   WHERE a.lnksem = b.lnksem
   --and b.idSem = c.idsem
   AND a.periodoactivo = 1
   ORDER BY b.idsem, b.sem_com

    LET pos = 0
    --LET ant = 0  
    FOREACH ccursor INTO nSem
      LET pos = pos + 1
      LET sSem[pos] = nSem
    END FOREACH  
      --LET pLine = gr_commdep.id_commdep
      
      {IF gr_commdep.hijo_de <> ant THEN
         LET ant = gr_commdep.hijo_de
         LET pos = pos + 3
         LET pLine[pos,100] = gr_commdep.descripcion
      ELSE 
         LET pLine[pos,100] = gr_commdep.descripcion
      END IF }
      --OUTPUT TO REPORT rDeptos(gr_commdep.*)   
      OUTPUT TO REPORT rPrecios(gRep.*)   
    

    FINISH REPORT rPrecios
    
END FUNCTION 

REPORT rPrecios(lRep)
DEFINE i, j, fenc SMALLINT 
DEFINE lRep RECORD LIKE precioxsem.*

FORMAT 
FIRST PAGE HEADER 
  PRINT COLUMN 01, "REPORTE DE PRECIOS"

  PRINT COLUMN 01, "Tipo",
        COLUMN 10, "Nombre",
        COLUMN 30, "Semana"
  LET fenc = 0

PAGE HEADER 
  PRINT COLUMN 01, "REPORTE DE PRECIOS"

  PRINT COLUMN 01, "Tipo",
        COLUMN 10, "Nombre",
        COLUMN 30, "Semana"
  
ON EVERY ROW
  {IF fenc = 0 THEN 
     LET j = 0      
     FOR i = 1 TO sSem.getLength()
       LET j = j + 10
       DISPLAY "pr ", sSem[i], "-",i
       PRINT COLUMN j, sSem[i]
     END FOR   
     LET fenc = 1
  END IF }
  PRINT COLUMN 01, lRep.idTipo,
        COLUMN 10, "nombre_fac",
        COLUMN 30, lRep.preciobruto

END REPORT 
{REPORT rDeptos(lr_commdep,lLine)
  DEFINE lr_commdep RECORD 
    id_commdep  LIKE commdep.id_commdep,
    descripcion LIKE commdep.descripcion,
    hijo_de     LIKE commdep.hijo_de

  END RECORD 
  DEFINE lLine VARCHAR(100)

DEFINE vEmpresa LIKE commemp.nombre
DEFINE vUsuario VARCHAR(50)

FORMAT 

  PAGE HEADER 
    --Para empresa
    LET vEmpresa = "SELECT nombre FROM ande:commemp WHERE id_commemp = 1"
    PREPARE ex_vEmpresa FROM vEmpresa
    EXECUTE ex_vEmpresa INTO vEmpresa
    PRINT COLUMN 001, vEmpresa,
          COLUMN 075, "FECHA   : ", TODAY USING "dd/mm/yyyy"

    PRINT COLUMN 025, "REPORTE DE ORGANIZACIÓN - DEPARTAMENTOS"

    --Para usuario
    LET vUsuario = "SELECT UNIQUE USER FROM systables WHERE tabid = 99"
    PREPARE ex_vUsuario FROM vUsuario 
    EXECUTE ex_vUsuario INTO vUsuario 
    PRINT COLUMN 001, "USUARIO: ", vUsuario,
          COLUMN 075, "HOJA No.: ", PAGENO USING "&&"
    PRINT COLUMN 000, "=============================================================================================="
    SKIP 2 LINES 

    PRINT COLUMN 001, " CODIGO", 
          COLUMN 015, "DESCRIPCIÓN"
    PRINT COLUMN 001, "--------",
          COLUMN 015, "--------------------------"
    --SKIP 1 LINES 

  ON EVERY ROW 
    --PRINT COLUMN 004, lr_commdep.id_commdep USING "&&&",
    --      COLUMN 015, lr_commdep.descripcion
    PRINT COLUMN 001, lLine
END REPORT }