DATABASE db0001

MAIN

DEFINE gr_tmp1 RECORD
   idBoletaIng      INT,
   pesoTotal        SMALLINT 
END RECORD  

DEFINE contador SMALLINT 

--Crea temporal
SELECT idBoletaIng, pesoTotal 
  FROM pemmboleta 
  WHERE fecha >= TODAY - 40
  INTO TEMP tmp1

SELECT COUNT (*) INTO contador 
FROM tmp1

DISPLAY "Total al crear la tmp ", contador
  
DECLARE cur1 CURSOR FOR 
  SELECT * FROM tmp1

FOREACH cur1 INTO gr_tmp1.*
   IF gr_tmp1.idBoletaIng = 21983 THEN
      DISPLAY "Boleta con 0 aprovechamiento ", gr_tmp1.idBoletaIng
   END IF 

   SELECT idboletaing FROM pemmboletaexp
      WHERE idboletaing = gr_tmp1.idBoletaIng
   IF sqlca.sqlcode = 0 THEN
      DELETE FROM tmp1
      WHERE idboletaing = gr_tmp1.idBoletaIng
      --DISPLAY "Borrando ", gr_tmp1.idBoletaIng
   ELSE 
      --DISPLAY "No borre ", gr_tmp1.idBoletaIng
   END IF 
END FOREACH 

SELECT COUNT (*) INTO contador FROM tmp1   
DISPLAY "Total despues de eliminar exp ", contador

END MAIN
