################################################################################
# Funcion     : %M%
# Descripcion : Funcion para modificar datos de un umdor
# Funciones   : updel_init()
#	             delete_umd() 
#               update_umd()
#               pk_estado()
#               valida_modi()
#               verifica_modi()
#               existe_datos()
#               existe_dpro()
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################
GLOBALS "aprom0702_glob.4gl"

FUNCTION update_init()
DEFINE strSql STRING 
-- TODO: Arreglar el Update   
   LET strSql = 
      "UPDATE contnote ",
      "SET ",
       " contnote_text = ?, contnote_cod_tipologia = ?, contnote_fecha_promesa = ?, ",
       " contnote_boleta_numero = ?, contnote_boleta_fecha = ?, contnote_boleta_monto = ?, ",
       " contnote_boleta_copia = ?, contnote_cuenta = ?, contnote_justificacion = ?, ", 
       " contnote_monto_promesa = ?, contnote_when = ?, contnote_estado = ?   ",
       " WHERE contnote_contact = ? AND contnote_num = ? "
 
   PREPARE st_modificar FROM strSql
END FUNCTION 

FUNCTION modifica()
   CALL encabezado("Modificar")
   --CALL info_usuario()
   
   IF captura_datos('M') THEN
      RETURN actualizar()
   ELSE 
      RETURN FALSE 
   END IF 
END FUNCTION 

FUNCTION actualizar()
   TRY
      LET g_reg.contnote_when = CURRENT 
      EXECUTE st_modificar USING 
      g_reg.contnote_text, g_reg.contnote_cod_tipologia, g_reg.contnote_fecha_promesa,
      g_reg.contnote_boleta_numero, g_reg.contnote_boleta_fecha, g_reg.contnote_boleta_monto,
      g_reg.contnote_boleta_copia, g_reg.contnote_cuenta, g_reg.contnote_justificacion,
      g_reg.contnote_monto_promesa, g_reg.contnote_when, g_reg.contnote_estado,
      u_reg.contnote_contact, u_reg.contnote_num
      DISPLAY g_reg.contnote_when TO fcontnote_when
      
      --Actualiza estado_caso
      UPDATE contact SET contact_estado_caso = g_reg.contact_estado_caso WHERE contact_num = g_reg.contnote_contact
      
   CATCH 
      CALL msgError(sqlca.sqlcode,"Modificar Registro")
      RETURN FALSE 
   END TRY
   CALL msg ("Registro actualizado")
   RETURN TRUE 
END FUNCTION 

FUNCTION delete_init()
DEFINE strSql STRING 

   LET strSql =
      "DELETE FROM contnote ",
      "WHERE contnote_contact = ? AND contnote_num = ? "
      
   PREPARE st_delete FROM strSql

END FUNCTION 

FUNCTION anular()
   --Cuando la tabla es corporativa (creada en ANDE) y necesita validar que 
   -- no se haya usado en las bases de datos de empresas (db0001, db0002, etc)
   {IF NOT can_deleteAll("pemmfinca", "id_commemp", g_reg.id_commemp) THEN
      CALL msg("Empresa no se puede eliminar, ya existe en finca(s)")
      RETURN FALSE 
   END IF  }
   IF NOT box_confirma("Esta seguro de anular el registro") THEN
      RETURN FALSE
   END IF 
   TRY 
      EXECUTE st_delete USING g_reg.contnote_contact, g_reg.contnote_num
   CATCH 
      CALL msgError(sqlca.sqlcode,"Eliminar Registro")
      RETURN FALSE 
   END TRY
   CALL msg("Registro eliminado")
   RETURN TRUE 
END FUNCTION 

