################################################################################
# Funcion     : %M%
# nombre      : Catalogo de Tipos
#               Funcion para definicion de globales
# Funciones   : 
#               
#               
#  
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        nombre de la modificacion
#
################################################################################
--DATABASE comisiones
SCHEMA aprojusa

GLOBALS 
TYPE 
   tDet RECORD 
      contnote_contact        LIKE contnote.contnote_contact,
      contnote_num            LIKE contnote.contnote_num,
      contact_operacion       LIKE contact.contact_operacion,
      contnote_rec_mtime      LIKE contnote.contnote_rec_mtime,
      contact_estado_caso     LIKE contact.contact_estado_caso,
      contnote_cod_tipologia  LIKE contnote.contnote_cod_tipologia,
      des_tipologia           LIKE tipologia.des_tipologia,
      contnote_text           LIKE contnote.contnote_text,
      contnote_fecha_promesa  LIKE contnote.contnote_fecha_promesa,
      contnote_monto_promesa  LIKE contnote.contnote_monto_promesa,

      contnote_boleta_numero  LIKE contnote.contnote_boleta_numero,
      contnote_boleta_fecha   LIKE contnote.contnote_boleta_fecha,
      contnote_boleta_monto   LIKE contnote.contnote_boleta_monto,
      contnote_cuenta         LIKE contnote.contnote_cuenta,
      contnote_boleta_copia   LIKE contnote.contnote_boleta_copia,
      contnote_justificacion  LIKE contnote.contnote_justificacion,
      
      contnote_rec_muser      LIKE contnote.contnote_rec_muser,
      contnote_rec_mstat      LIKE contnote.contnote_rec_mstat,
      contnote_when           LIKE contnote.contnote_when,
      contnote_estado         LIKE contnote.contnote_estado
   END RECORD

DEFINE
   reg_det DYNAMIC ARRAY OF tDet, 
   g_reg, u_reg tDet,
   dbname      STRING,
   condicion   STRING --Condicion de la clausula Where

   --Para aplicar los estilos al arreglo principal
   DEFINE reg_det_attr DYNAMIC ARRAY OF RECORD 
      contnote_contact        STRING,
      contnote_num            STRING,
      contact_operacion       STRING,
      contnote_rec_mtime      STRING,
      contact_estado_caso     STRING,
      contnote_cod_tipologia  STRING,
      des_tipologia           STRING,
      contnote_text           STRING,
      contnote_fecha_promesa  STRING,
      contnote_monto_promesa  STRING,
      contnote_boleta_numero  STRING,
      contnote_boleta_fecha   STRING,
      contnote_boleta_monto   STRING,
      contnote_cuenta         STRING,
      contnote_boleta_copia   STRING,
      contnote_justificacion  STRING,
      contnote_rec_muser      STRING,
      contnote_rec_mstat      STRING,
      contnote_when           STRING,
      contnote_estado         STRING 
   END RECORD
  
   CONSTANT    prog_name = "aprom0702"

   DEFINE usuario LIKE users.user_id
   DEFINE grupo   LIKE grupo.grpnombre
   
END GLOBALS