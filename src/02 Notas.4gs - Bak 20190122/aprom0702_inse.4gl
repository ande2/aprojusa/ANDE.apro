################################################################################
# Funcion     : %M%
# Descripcion : Ingreso de un nuevo umdor 	
# Funciones   : input_umd() funcion para insertar datos
#              
#              
#               
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Anderson Garcia 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################

GLOBALS "aprom0702_glob.4gl"

FUNCTION insert_init()
DEFINE strSql STRING 

   {LET strSql =
      "INSERT INTO contnote ( ",
         " contnote_contact, contnote_num, ",
         " contnote_rec_mtime, contnote_text, ",
         " contnote_cod_tipologia, contnote_fecha_promesa, contnote_monto_promesa, ", 
         " contnote_rec_muser, contnote_rec_mstat,  contnote_when ",
         "   ) ",
         " VALUES (?,contnote_seq.NEXTVAL,?,?,?,?,?,?,?,?)"

   PREPARE st_insertar FROM strSql}

END FUNCTION 

FUNCTION ingreso()
   CALL encabezado("Ingresar")
   --CALL info_usuario()
   IF captura_datos('I') THEN
      RETURN grabar()
   ELSE 
      RETURN FALSE 
   END IF 
END FUNCTION 

FUNCTION grabar()
   --LET g_reg.idpais = 0
   TRY 
      {EXECUTE st_insertar USING 
            g_reg.contnote_contact, --g_reg.contnote_num, 
            g_reg.contnote_rec_mtime,  g_reg.contnote_text,
            g_reg.contnote_cod_tipologia, g_reg.contnote_fecha_promesa, g_reg.contnote_monto_promesa,
            g_reg.contnote_rec_muser,g_reg.contnote_rec_mstat, g_reg.contnote_when}
      INSERT INTO contnote ( 
         contnote_contact, contnote_num, 
         contnote_rec_mtime, contnote_text, 
         contnote_cod_tipologia, contnote_fecha_promesa, contnote_monto_promesa,
         contnote_boleta_numero, contnote_boleta_fecha, contnote_boleta_monto, 
         contnote_boleta_copia, contnote_cuenta, contnote_justificacion, 
         contnote_rec_muser, contnote_rec_mstat, contnote_when, contnote_estado 
           ) 
         VALUES (g_reg.contnote_contact,contnote_seq.NEXTVAL, 
            g_reg.contnote_rec_mtime,  g_reg.contnote_text, 
            g_reg.contnote_cod_tipologia, 
            g_reg.contnote_fecha_promesa, g_reg.contnote_monto_promesa,
            g_reg.contnote_boleta_numero, g_reg.contnote_boleta_fecha, g_reg.contnote_boleta_monto, 
            g_reg.contnote_boleta_copia, g_reg.contnote_cuenta, g_reg.contnote_justificacion,
            g_reg.contnote_rec_muser,g_reg.contnote_rec_mstat, g_reg.contnote_when, g_reg.contnote_estado)   
            
      UPDATE contact SET contact_estado_caso = g_reg.contact_estado_caso WHERE contact_num = g_reg.contnote_contact      

      --CS agregarlo cuando se agregue el ID 
      --LET g_reg.idpais = SQLCA.sqlerrd[2]
   CATCH 
      CALL msgError(sqlca.sqlcode,"Grabar Registro")
      RETURN FALSE 
   END TRY
   DISPLAY BY NAME g_reg.*
   CALL box_valdato ("Registro agregado")
   --CALL combo_din2("cmbdepartamento","SELECT * FROM commdep WHERE id_commdep > 0")
   RETURN TRUE 
END FUNCTION 