################################################################################
# Funcion     : %M%
# Descripcion : Modulo Principal para umdores 
# Funciones   : main_init(dbname)
#               main_menu()                               
#               dummy() 
#               
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo  
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################

GLOBALS "aprom0702_glob.4gl"
MAIN
	DEFINE 
		n_param 	SMALLINT,
		prog_name2 	STRING
		
	DEFER INTERRUPT

	OPTIONS
		INPUT  WRAP,
		HELP KEY CONTROL-W,
		COMMENT LINE OFF,
		PROMPT LINE LAST - 2,
		MESSAGE LINE LAST - 1,
		ERROR LINE LAST

   LET n_param = num_args()

   CONNECT TO "aprojusa"
   
	IF n_param = 0 THEN
		RETURN
	ELSE
      LET g_reg.contnote_contact = arg_val(1) 
      LET usuario = arg_val(2)
      LET grupo = grpusu(usuario)
      DISPLAY "grupo ", grupo 
        --LET dbname = "ande"
		--CONNECT TO dbname
	END IF
   

	LET prog_name2 = prog_name||".log"   -- El progr_name es definido como constante en el arch. globals
    
	CALL STARTLOG(prog_name2)
	CALL main_init()
END MAIN

FUNCTION main_init()
DEFINE 
	nom_forma 	STRING,
    w           ui.Window,
    f           ui.FORM
DEFINE titulo STRING     
	  
	INITIALIZE u_reg.* TO NULL
	LET g_reg.* = u_reg.*
   

   --CALL trae_usuario() returning g_usuario.*
   --CALL trae_empresa(dbname) returning g_empresa.*
   --CALL trae_programa(prog_name) returning g_programa.*
   --IF g_programa.prog_id = 0 OR 
      --g_programa.prog_id IS NULL THEN
      --CALL box_valdato("Programa no existente, contacte a sistemas")
      --RETURN 
   --END IF 
    CALL ui.Interface.loadActionDefaults("actiondefaults")
    CALL ui.Interface.loadStyles("styles_sc")
   --CALL ui.Interface.loadToolbar("toolbar")

	LET nom_forma = prog_name CLIPPED, "_form"
    CLOSE WINDOW SCREEN 

	OPEN WINDOW w1 WITH FORM nom_forma
   CALL fgl_settitle("Notas por Caso")
   LET g_reg.contnote_contact = arg_val(1)
   SELECT contact_operacion INTO titulo FROM contact WHERE contact_num = g_reg.contnote_contact 
   CALL encabezado(titulo)

	LET w = ui.WINDOW.getcurrent()
	LET f = w.getForm()
	--CALL fgl_settitle(g_programa.prog_des)
    
	CALL insert_init()
   CALL update_init()
   CALL delete_init()
   CALL combo_init()
	CALL main_menu()
END FUNCTION                                                                    

FUNCTION main_menu()                               
DEFINE  
    cuantos, 
    id, ids	   SMALLINT,
    vopciones   CHAR(255), 
    cnt 		   SMALLINT,  
    tol ARRAY[15] OF RECORD
      acc      CHAR(15),
      des_cod  SMALLINT
    END RECORD,
    aui, tb, tbi, tbs om.DomNode
   DEFINE titulo STRING 
   DEFINE txtmsg STRING 

    LET cnt = 1

   LET g_reg.contnote_contact = arg_val(1)
   SELECT trim(contact_operacion)||"-"||contact_name INTO titulo FROM contact WHERE contact_num = g_reg.contnote_contact 
   DISPLAY titulo TO uno
    --MENU "test" ON ACTION salir EXIT MENU END MENU 
	DISPLAY ARRAY reg_det TO sDet.*
      ATTRIBUTE(ACCEPT=FALSE,CANCEL=FALSE,UNBUFFERED)
   
      BEFORE DISPLAY
         LET cuantos = consulta(false)
         IF cuantos > 0 THEN 
            CALL dialog.setCurrentRow("sdet",1)
            LET g_reg.* = reg_det[1].*
            CALL display_reg()
            CALL setAttr(1)
            CALL DIALOG.setCellAttributes(reg_det_attr)
            --DISPLAY BY NAME g_reg.*
         END IF 
         CALL encabezado("")
         --CALL info_usuario()
         
      BEFORE ROW 
         LET id = arr_curr()
         IF id > 0 THEN 
            LET g_reg.* = reg_det[id].*
            CALL display_reg()
            CALL setAttr(id)
            CALL DIALOG.setCellAttributes(reg_det_attr)
            --DISPLAY BY NAME g_reg.*
         END IF 
         IF g_reg.contnote_estado = "C" THEN
            CALL dialog.setActionHidden("abrir",0)
            CALL dialog.setActionHidden("cerrar",1)
         ELSE 
            CALL dialog.setActionHidden("abrir",1)
            CALL dialog.setActionHidden("cerrar",0)
         END IF
      --ON KEY (CONTROL-W) 
        
      {ON ACTION buscar
         
         LET cuantos = consulta(true)
         IF cuantos > 0 THEN 
            LET g_reg.* = reg_det[1].*
            CALL display_reg()
            CALL setAttr(1)
            CALL DIALOG.setCellAttributes(reg_det_attr)
            CALL setAttr(1)
            CALL DIALOG.setCellAttributes(reg_det_attr)
            --DISPLAY BY NAME g_reg.*
         END IF 
         CALL encabezado("")
         --CALL info_usuario()}
         
      --ON ACTION primero
         --CALL fgl_set_arr_curr( 1 )
      --ON ACTION ultimo
         --CALL fgl_set_arr_curr( arr_count() )
      --ON ACTION anterior 
         --CALL fgl_set_arr_curr( arr_curr() - 1 )
      --ON ACTION siguiente 
         --CALL fgl_set_arr_curr( arr_curr() + 1 )
      ON ACTION agregar
         IF ingreso() THEN 
            LET cuantos = consulta(FALSE)
            CALL fgl_set_arr_curr( arr_count() + 1 )
            --Refrescar Pantalla
            DISPLAY ARRAY reg_det TO sDet.*
               BEFORE DISPLAY  EXIT DISPLAY 
            END DISPLAY 
         END IF
         CALL encabezado("")
         CALL setAttr(1)
         CALL DIALOG.setCellAttributes(reg_det_attr)
         --CALL info_usuario()
      ON ACTION modificar
         DISPLAY "pase upd"
         IF g_reg.contnote_estado = "A" THEN
            LET id = arr_curr()
            LET ids = scr_line()
            IF id > 0 THEN 
               IF modifica() THEN
                  LET reg_det[id].* = g_reg.*
                  DISPLAY reg_det[id].* TO sDet[ids].*
               END IF   
            END IF 
            CALL encabezado("")
            CALL setAttr(id)
            CALL DIALOG.setCellAttributes(reg_det_attr)
         END IF    
         --CALL info_usuario()
      ON ACTION anular
         LET id = arr_curr()
         LET ids = scr_line()         
         IF id > 0 THEN 
            IF anular() THEN
               --Si es anulacion
               --LET reg_det[id].* = g_reg.*
               --DISPLAY reg_det[id].* TO sDet[ids].*
               --Si es eliminacion
               CALL DIALOG.deleteRow("sdet", id)
               IF id = arr_count() THEN 
                  LET id = id - 1
               END IF 
               IF id > 0 THEN 
                  LET g_reg.* = reg_det[id].*
               ELSE 
                  INITIALIZE g_reg.* TO NULL
               END IF 
               DISPLAY BY NAME g_reg.*
               CALL setAttr(id)
               CALL DIALOG.setCellAttributes(reg_det_attr)
            END IF   
         END IF 

      ON ACTION abrir
         LET id = arr_curr()
         IF g_reg.contnote_estado = "C" THEN 
            LET txtmsg = "Desea abrir la nota No. ", g_reg.contnote_num
            IF confirma(txtmsg) THEN
               LET g_reg.contnote_estado = "A"
               UPDATE contnote SET contnote_estado = g_reg.contnote_estado WHERE contnote_num = g_reg.contnote_num
               LET cuantos = consulta(false)
               IF cuantos > 0 THEN 
                  CALL dialog.setCurrentRow("sdet",1)
                  LET g_reg.* = reg_det[id].*
                  CALL display_reg()
                  CALL setAttr(id)
                  CALL DIALOG.setCellAttributes(reg_det_attr)
                  --DISPLAY BY NAME g_reg.*
               END IF 
               CALL encabezado("")
               CALL dialog.setActionHidden ("abrir",1)
               CALL dialog.setActionHidden("cerrar",0)
            ELSE 
               LET txtmsg = "Operación cancelada"
               CALL msg(txtmsg)
            END IF
         END IF 
            
         
      ON ACTION cerrar
         LET id = arr_curr()
         IF g_reg.contnote_estado = "A" THEN 
            LET txtmsg = "Desea cerrar la nota No. ", g_reg.contnote_num
            IF confirma(txtmsg) THEN
               LET g_reg.contnote_estado = "C"
               UPDATE contnote SET contnote_estado = g_reg.contnote_estado WHERE contnote_num = g_reg.contnote_num
               LET cuantos = consulta(false)
               IF cuantos > 0 THEN 
                  CALL dialog.setCurrentRow("sdet",1)
                  LET g_reg.* = reg_det[id].*
                  CALL display_reg()
                  CALL setAttr(id)
                  CALL DIALOG.setCellAttributes(reg_det_attr)
                  --DISPLAY BY NAME g_reg.*
               END IF 
               CALL encabezado("")
               CALL dialog.setActionHidden ("abrir",0)
               CALL dialog.setActionHidden("cerrar",1)
            ELSE 
               LET txtmsg = "Operación cancelada"
               CALL msg(txtmsg)
            END IF
         END IF
      
      --ON ACTION reporte
         --CALL PrintReport()
      ON ACTION salir
         EXIT DISPLAY 
   END DISPLAY 
END FUNCTION

FUNCTION combo_init()
   --CALL combo_din2("idfamilia","SELECT * FROM glbmfam")
   --CALL combo_din2("id_commpue","SELECT * FROM commpue WHERE id_commpue > 0")
   --CALL combo_din2("jefe",'SELECT id_commempl, TRIM(nombre) || " "|| TRIM(apellido) FROM commempl WHERE id_commempl > 0')
   --CALL combo_din2("cmbjefe",'SELECT id_commempl, TRIM(nombre) || " "|| TRIM(apellido) FROM commempl WHERE id_commempl > 0')
   --CALL combo_din2("cmbpuesto","SELECT * FROM commpue WHERE id_commpue > 0")
   --CALL combo_din2("cmbfamilia","SELECT * FROM glbmfam")
END FUNCTION 

FUNCTION encabezado(gtit_enc)
DEFINE gtit_enc STRING 

   --DISPLAY gtit_enc TO uno
END FUNCTION 

FUNCTION display_reg()

   DISPLAY g_reg.contnote_contact, g_reg.contnote_num, g_reg.contact_operacion,
      g_reg.contnote_rec_mtime, g_reg.contact_estado_caso, 
      g_reg.contnote_text, g_reg.contnote_cod_tipologia, g_reg.des_tipologia,
      g_reg.contnote_fecha_promesa, g_reg.contnote_monto_promesa,
      
      g_reg.contnote_boleta_numero, g_reg.contnote_boleta_fecha,
      g_reg.contnote_boleta_monto, g_reg.contnote_cuenta, 
      g_reg.contnote_boleta_copia, g_reg.contnote_justificacion,
      
      g_reg.contnote_rec_muser, g_reg.contnote_rec_mstat, g_reg.contnote_when,
      g_reg.contnote_estado
      TO fcontnote_contact, fcontnote_num, fcontact_operacion,
      fcontnote_rec_mtime, fcontact_estado_caso, 
      fcontnote_text, fcontnote_cod_tipologia, 
      fdes_tipologia, fcontnote_fecha_promesa, fcontnote_monto_promesa,

      fcontnote_boleta_numero, fcontnote_boleta_fecha,
      fcontnote_boleta_monto, fcontnote_cuenta, 
      fcontnote_boleta_copia, fcontnote_justificacion,
      
      fcontnote_rec_muser, fcontnote_rec_mstat, fcontnote_when,
      fcontnote_estado
       

END FUNCTION 

FUNCTION setAttr(id)
DEFINE  i, id  SMALLINT
     
   FOR i=1 TO reg_det.getLength()
      LET reg_det_attr[i].contnote_contact         = "black"
      LET reg_det_attr[i].contnote_num             = "black"
      LET reg_det_attr[i].contact_operacion        = "black"
      LET reg_det_attr[i].contnote_rec_mtime       = "black"
      LET reg_det_attr[i].contact_estado_caso      = "black"
      LET reg_det_attr[i].contnote_text            = "black"
      LET reg_det_attr[i].contnote_cod_tipologia   = "black"
      LET reg_det_attr[i].des_tipologia            = "black"
      LET reg_det_attr[i].contnote_fecha_promesa   = "black"
      LET reg_det_attr[i].contnote_monto_promesa   = "black"

      LET reg_det_attr[i].contnote_boleta_numero   = "black"
      LET reg_det_attr[i].contnote_boleta_fecha    = "black"
      LET reg_det_attr[i].contnote_boleta_monto    = "black"
      LET reg_det_attr[i].contnote_cuenta          = "black"
      LET reg_det_attr[i].contnote_boleta_copia    = "black"
      LET reg_det_attr[i].contnote_justificacion   = "black"
      
      LET reg_det_attr[i].contnote_rec_muser       = "black"
      LET reg_det_attr[i].contnote_rec_mstat       = "black"
      LET reg_det_attr[i].contnote_when            = "black"
      LET reg_det_attr[i].contnote_estado          = "black"
   END FOR  

   LET reg_det_attr[id].contnote_contact        = "blue reverse"
   LET reg_det_attr[id].contnote_num            = "blue reverse"
   LET reg_det_attr[id].contact_operacion       = "blue reverse"
   LET reg_det_attr[id].contnote_rec_mtime      = "blue reverse"
   LET reg_det_attr[id].contact_estado_caso     = "blue reverse"
   LET reg_det_attr[id].contnote_text           = "blue reverse"
   LET reg_det_attr[id].contnote_cod_tipologia  = "blue reverse"
   LET reg_det_attr[id].des_tipologia           = "blue reverse"
   LET reg_det_attr[id].contnote_fecha_promesa  = "blue reverse"
   LET reg_det_attr[id].contnote_monto_promesa  = "blue reverse"

   LET reg_det_attr[id].contnote_boleta_numero   = "blue reverse"
   LET reg_det_attr[id].contnote_boleta_fecha    = "blue reverse"
   LET reg_det_attr[id].contnote_boleta_monto    = "blue reverse"
   LET reg_det_attr[id].contnote_cuenta          = "blue reverse"
   LET reg_det_attr[id].contnote_boleta_copia    = "blue reverse"
   LET reg_det_attr[id].contnote_justificacion   = "blue reverse"
   
   LET reg_det_attr[id].contnote_rec_muser      = "blue reverse"
   LET reg_det_attr[id].contnote_rec_mstat      = "blue reverse"
   LET reg_det_attr[id].contnote_when           = "blue reverse"
   LET reg_det_attr[id].contnote_estado         = "blue reverse"
   
END FUNCTION 