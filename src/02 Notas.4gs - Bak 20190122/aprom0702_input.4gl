
GLOBALS "aprom0702_glob.4gl"

FUNCTION captura_datos(operacion)
DEFINE operacion CHAR (1)
DEFINE resultado BOOLEAN
DEFINE w ui.Window
DEFINE f ui.Form

   LET resultado = FALSE 
   LET u_reg.* = g_reg.*
   IF operacion = 'I' THEN 
      INITIALIZE g_reg.* TO NULL
      DISPLAY BY NAME g_reg.*
      DISPLAY "pase init ", g_reg.contnote_num
   END IF
   DIALOG ATTRIBUTES(UNBUFFERED)
     INPUT 
      g_reg.contact_estado_caso, g_reg.contnote_text, g_reg.contnote_cod_tipologia, 
      g_reg.contnote_fecha_promesa, g_reg.contnote_monto_promesa,
      g_reg.contnote_boleta_numero, g_reg.contnote_boleta_fecha, g_reg.contnote_boleta_monto,
      g_reg.contnote_cuenta, g_reg.contnote_boleta_copia, g_reg.contnote_justificacion,  
      g_reg.contnote_estado
      FROM fcontact_estado_caso, fcontnote_text, fcontnote_cod_tipologia, 
      fcontnote_fecha_promesa, fcontnote_monto_promesa,
      fcontnote_boleta_numero, fcontnote_boleta_fecha, fcontnote_boleta_monto, fcontnote_cuenta,
      fcontnote_boleta_copia, fcontnote_justificacion,  
      fcontnote_estado
      ATTRIBUTES (WITHOUT DEFAULTS)

      BEFORE INPUT
         CALL DIALOG.setActionHidden("close",TRUE)
         IF operacion = 'I' THEN 
            --Caso
            LET g_reg.contnote_contact = arg_val(1)
            --Fecha de la nota
            LET g_reg.contnote_when = CURRENT
            --Fecha de registro
            LET g_reg.contnote_rec_mtime = CURRENT
            --Estado
            LET g_reg.contnote_rec_mstat = "V"
            --Usuario
            LET g_reg.contnote_rec_muser = fgl_getenv("LOGNAME")
            LET g_reg.contnote_num = NULL 

            --Estado Caso
            SELECT contact_estado_caso INTO g_reg.contact_estado_caso
               FROM contact
               WHERE contact_num = g_reg.contnote_contact
               
            DISPLAY g_reg.contact_estado_caso, g_reg.contnote_contact, g_reg.contnote_num, 
               g_reg.contnote_when, 
               g_reg.contnote_rec_muser, g_reg.contnote_rec_mtime, g_reg.contnote_rec_mstat
               TO fcontact_estado_caso, fcontnote_contact, fcontnote_num, 
               fcontnote_when, 
               fcontnote_rec_muser, fcontnote_rec_mtime, fcontnote_rec_mstat
         END IF 

      ON CHANGE fcontact_estado_caso
         CASE g_reg.contact_estado_caso 
            WHEN "RECUPERADO" 
               LET g_reg.contnote_cod_tipologia = "18"
            WHEN "REC-PARCIAL"
               LET g_reg.contnote_cod_tipologia = "86"
         END CASE
         SELECT des_tipologia INTO g_reg.des_tipologia FROM tipologia
            WHERE cod_tipologia = g_reg.contnote_cod_tipologia 
         DISPLAY g_reg.des_tipologia TO fdes_tipologia
         NEXT FIELD fcontnote_text 
         
      ON ACTION buscar
          --LABEL opcionBusca:
         CASE  
            WHEN INFIELD (fcontnote_cod_tipologia)
               CALL picklist_2("Tipología", "Código","Descripción","cod_tipologia", "des_tipologia", "tipologia", "estado ='V'", "1", 1)
                  RETURNING g_reg.contnote_cod_tipologia, g_reg.des_tipologia, INT_FLAG
               --Que no sea nulo
               IF g_reg.contnote_cod_tipologia IS NULL THEN
                  CALL msg("Debe ingresar código de tipología")
                  NEXT FIELD fcontnote_cod_tipologia
               ELSE
                  --LET g_reg.nombre1=vdescripcion
                  DISPLAY g_reg.des_tipologia TO fdes_tipologia
               END IF
         END CASE 

      ON CHANGE fcontnote_cod_tipologia
        SELECT des_tipologia INTO g_reg.des_tipologia FROM tipologia
        WHERE cod_tipologia = g_reg.contnote_cod_tipologia 
        IF sqlca.sqlcode != 0 THEN
           CALL msg("Tipología no existe, ingrese de nuevo")
           NEXT FIELD CURRENT 
        ELSE 
            DISPLAY g_reg.des_tipologia TO fdes_tipologia
        END IF 

      --ON CHANGE fcontnote_boleta_copia
         

      
      {ON CHANGE fcontact_estado_caso
         IF g_reg.contact_estado_caso IS NULL THEN
            CALL msg("Seleccione estado caso")
            NEXT FIELD f_contac_estado_caso
         END IF} 
          
      {AFTER FIELD nombre
        IF g_reg.nombre IS NULL THEN
           CALL msg("Ingrese nombre")
           NEXT FIELD CURRENT  
        END IF 

      AFTER FIELD codigo
        IF g_reg.codigo IS NULL THEN
           CALL msg("Ingrese c�digo")
           NEXT FIELD CURRENT  
        END IF  }
        

      {AFTER FIELD dbname 
        IF g_reg.nombre IS NOT NULL THEN
           SELECT id_commemp FROM commemp WHERE commemp.dbname = g_reg.dbname
           IF sqlca.sqlcode = 0 THEN 
              CALL msg("Nombre de base de datos ya existe")
              NEXT FIELD CURRENT
           END IF 
        END IF}
 
 
            --IF (operacion = 'I') OR (g_reg.emNomCt <> u_reg.emNomCt) THEN 
            --IF existe_cat_cod(g_reg.idBautizo) THEN 
           
               --LET g_reg.idBautizo = NULL 
            
            --END IF   
         --END IF
      
   END INPUT 

   ON ACTION ACCEPT
      IF g_reg.contact_estado_caso = "RECUPERADO" THEN 
         IF g_reg.contnote_boleta_numero IS NULL OR LENGTH(g_reg.contnote_boleta_numero) = 0 THEN
            CALL msg("Numero de boleta no puede ser nula")
            NEXT FIELD fcontnote_boleta_numero
         END IF 
         IF g_reg.contnote_boleta_fecha IS NULL THEN
            CALL msg("Fecha de boleta no puede ser nula")
            NEXT FIELD fcontnote_boleta_fecha
         END IF 
         IF g_reg.contnote_boleta_copia IS NULL OR LENGTH(g_reg.contnote_boleta_copia) = 0 THEN
            CALL msg("Debe indicar si entrego copia de boleta")
            NEXT FIELD fcontnote_boleta_copia
         END IF 
         IF g_reg.contnote_boleta_monto IS NULL THEN
            CALL msg("Ingrese monto de depósito")
            NEXT FIELD fcontnote_boleta_monto
         END IF 
         IF g_reg.contnote_cuenta IS NULL THEN
            CALL msg("Ingrese monto de depósito")
            NEXT FIELD fcontnote_boleta_monto
         END IF 
      END IF

      IF g_reg.contnote_boleta_copia = "N" THEN
         IF g_reg.contnote_justificacion IS NULL OR LENGTH(g_reg.contnote_justificacion)=0 THEN 
            CALL msg("Debe ingresar justificación")
            NEXT FIELD fcontnote_justificacion
         END IF 
      END IF 

        IF g_reg.contnote_text IS NULL THEN
           CALL msg("Ingrese nota")
           NEXT FIELD fcontnote_text  
        END IF

        {IF g_reg.contnote_when IS NULL THEN
           CALL msg("Ingrese fecha")
           NEXT FIELD codigo  
        END IF}
        {IF g_reg.nombre IS NULL THEN
           CALL msg("Debe ingresar nombre")
           NEXT FIELD CURRENT
        END IF  

        IF g_reg.espropia IS NULL THEN
           CALL msg("Debe especificar si es empresa propia")
           NEXT FIELD esPropia
        END IF }

      
      --IF g_reg.librob IS NULL THEN
         --CALL msg("Debe ingresar nombre")
         --NEXT FIELD librob
      --END IF
      --IF (operacion = 'I') OR (g_reg.cat_id <> u_reg.cat_id) THEN 
         --IF existe_cat_id(g_reg.cat_id) THEN 
            --CALL msg("Este Id. ya esta siendo utilizado")
            --LET g_reg.cat_id = NULL 
            --NEXT FIELD cat_id 
         --END IF   
      --END IF


      {IF (operacion = 'I') OR (g_reg.librob <> u_reg.librob) THEN 
         IF existe_cat_nom(g_reg.librob) THEN 
            CALL msg("Este nombre ya esta siendo utilizado")
            LET g_reg.librob = NULL 
            NEXT FIELD librob 
         END IF   
      END IF}
      IF operacion = 'M' AND g_reg.* = u_reg.* THEN
         CALL msg("No se efectuaron cambios")
         EXIT DIALOG 
      END IF
      CASE box_gradato("Seguro de grabar")
         WHEN "Si"
            LET resultado = TRUE
            EXIT DIALOG
         WHEN "No"
            EXIT DIALOG 
         OTHERWISE
            CONTINUE DIALOG 
      END CASE 
      LET resultado = TRUE
      EXIT DIALOG 

   
      
     ON ACTION CANCEL
        EXIT dialog
   END DIALOG
   
   IF NOT resultado THEN
      LET g_reg.* = u_reg.*
      CALL display_reg()
      --DISPLAY BY NAME g_reg.* 
   END IF 
   RETURN resultado 
END FUNCTION
