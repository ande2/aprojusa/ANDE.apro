################################################################################
# Funcion     : %M%
# nombre      : Catalogo de Tipos
#               Funcion para definicion de globales
# Funciones   : 
#               
#               
#  
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        nombre de la modificacion
#
################################################################################
--DATABASE comisiones
SCHEMA aprojusa

GLOBALS 
TYPE 
   tDet RECORD 
      liq_fecha         LIKE contliquidacion.liq_fecha,
      liq_user          LIKE contliquidacion.liq_user,
      liq_fec_ini       LIKE contliquidacion.liq_fec_ini,
      liq_fec_fin       LIKE contliquidacion.liq_fec_fin
   END RECORD

DEFINE
   reg_det DYNAMIC ARRAY OF tDet, 
   g_reg, u_reg tDet,
   dbname      STRING,
   
   condicion   STRING --Condicion de la clausula Where 
   CONSTANT    prog_name = "aprom0713"

DEFINE reg_det_attr DYNAMIC ARRAY OF RECORD 
      liq_fecha         STRING,
      liq_user          STRING,
      liq_fec_ini       STRING,
      liq_fec_fin       STRING
   END RECORD

   DEFINE guser   LIKE users.user_id
   DEFINE gsede   LIKE users.city_num
   DEFINE ggrupo  LIKE grupo.grpnombre
   DEFINE flagdef SMALLINT 
END GLOBALS