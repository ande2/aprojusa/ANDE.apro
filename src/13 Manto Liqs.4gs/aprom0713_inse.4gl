################################################################################
# Funcion     : %M%
# Descripcion : Ingreso de un nuevo umdor 	
# Funciones   : input_umd() funcion para insertar datos
#              
#              
#               
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Anderson Garcia 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################

GLOBALS "aprom0713_glob.4gl"

FUNCTION insert_init()
DEFINE strSql STRING 

   LET strSql =
      "INSERT INTO contliquidacion ( ",
        " liq_fecha, liq_user, liq_fec_ini, liq_fec_fin ",
        "   ) ",
      " VALUES (?,?,?,?)"

   PREPARE st_insertar FROM strSql

END FUNCTION 

FUNCTION ingreso()
   CALL encabezado("Ingresar")
   --CALL info_usuario()
   IF captura_datos('I') THEN
      CALL runrepliq() 
      IF flagdef = 1 THEN 
         RETURN grabar()
      ELSE 
         RETURN FALSE 
      END IF 
   ELSE 
      RETURN FALSE 
   END IF 
END FUNCTION 

FUNCTION grabar()
   --LET g_reg.grpid = 0
   TRY 
      EXECUTE st_insertar USING 
            g_reg.liq_fecha, g_reg.liq_user, g_reg.liq_fec_ini, g_reg.liq_fec_fin

      --CS agregarlo cuando se agregue el ID 
      --LET g_reg.grpid = SQLCA.sqlerrd[2]
   CATCH 
      CALL msgError(sqlca.sqlcode,"Grabar Registro")
      RETURN FALSE 
   END TRY
   DISPLAY BY NAME g_reg.*
   CALL box_valdato ("Registro agregado")
   --CALL combo_din2("cmbdepartamento","SELECT * FROM commdep WHERE id_commdep > 0")
   RETURN TRUE 
END FUNCTION 