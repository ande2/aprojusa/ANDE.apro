################################################################################
# Funcion     : %M%
# Descripcion : Modulo Principal para umdores 
# Funciones   : main_init(dbname)
#               main_menu()                               
#               dummy() 
#               
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Anderson Garcia  
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################

GLOBALS "aprom0713_glob.4gl"
MAIN
	DEFINE 
		n_param 	SMALLINT,
		prog_name2 	STRING
		
	DEFER INTERRUPT

	OPTIONS
		INPUT  WRAP,
		HELP KEY CONTROL-W,
		COMMENT LINE OFF,
		PROMPT LINE LAST - 2,
		MESSAGE LINE LAST - 1,
		ERROR LINE LAST

	LET n_param = num_args()

	--IF n_param = 0 THEN
		--RETURN
	--ELSE
        --LET dbname = arg_val(1) 
        --LET dbname = "ande"
		--CONNECT TO dbname
	--END IF
   
   CONNECT TO "aprojusa"

   LET guser   = arg_val(1)
   LET ggrupo = grpusu(guser)
   SELECT city_num INTO gsede FROM users WHERE user_id = guser

	LET prog_name2 = prog_name||".log"   -- El progr_name es definido como constante en el arch. globals
    
	CALL STARTLOG(prog_name2)
	CALL main_init()
END MAIN

FUNCTION main_init()
DEFINE 
	nom_forma 	STRING,
    w           ui.Window,
    f           ui.Form
	  
	INITIALIZE u_reg.* TO NULL
	LET g_reg.* = u_reg.*

   --CALL trae_usuario() returning g_usuario.*
   --CALL trae_empresa(dbname) returning g_empresa.*
   --CALL trae_programa(prog_name) returning g_programa.*
   --IF g_programa.prog_id = 0 OR 
      --g_programa.prog_id IS NULL THEN
      --CALL box_valdato("Programa no existente, contacte a sistemas")
      --RETURN 
   --END IF 
    CALL ui.Interface.loadActionDefaults("actiondefaults")
    CALL ui.Interface.loadStyles("styles_sc")
   --CALL ui.Interface.loadToolbar("toolbar")

	LET nom_forma = prog_name CLIPPED, "_form"
    CLOSE WINDOW SCREEN 

	OPEN WINDOW w1 WITH FORM nom_forma
    CALL fgl_settitle("ANDE - Liquidación")
   
   
	LET w = ui.WINDOW.getcurrent()
	LET f = w.getForm()
	--CALL fgl_settitle(g_programa.prog_des)
    
	CALL insert_init()
    CALL update_init()
    CALL delete_init()
    CALL combo_init()
	CALL main_menu()
END FUNCTION                                                                    

FUNCTION main_menu()                               
DEFINE  
    cuantos, 
    id, ids	   SMALLINT,
    vopciones   CHAR(255), 
    cnt 		   SMALLINT,  
    tol ARRAY[15] OF RECORD
      acc      CHAR(15),
      des_cod  SMALLINT
    END RECORD,
    aui, tb, tbi, tbs om.DomNode
    DEFINE txtEnc STRING 

    LET cnt 		= 1
    SELECT user_name INTO txtEnc FROM users WHERE user_id =  guser 
    DISPLAY txtEnc TO gtit_enc
    
	DISPLAY ARRAY reg_det TO sDet.*
      ATTRIBUTE(ACCEPT=FALSE,CANCEL=FALSE,UNBUFFERED)
   
      BEFORE DISPLAY
         LET cuantos = consulta(false)
         IF cuantos > 0 THEN 
            CALL dialog.setCurrentRow("sdet",1)
            LET g_reg.* = reg_det[1].*
            DISPLAY BY NAME g_reg.*
            CALL setAttr(1)
            CALL DIALOG.setCellAttributes(reg_det_attr)
         END IF 
         CALL encabezado("")
         --CALL info_usuario()
         
      BEFORE ROW 
         LET id = arr_curr()
         IF id > 0 THEN 
            LET g_reg.* = reg_det[id].*
            DISPLAY BY NAME g_reg.*
            CALL setAttr(id)
            CALL DIALOG.setCellAttributes(reg_det_attr)
         END IF 

      --ON KEY (CONTROL-W) 
         
      {ON ACTION buscar
         
         LET cuantos = consulta(false)
         IF cuantos > 0 THEN 
            LET g_reg.* = reg_det[1].*
            DISPLAY BY NAME g_reg.*
         END IF 
         CALL encabezado("")}
         --CALL info_usuario()
         
      --ON ACTION primero
         --CALL fgl_set_arr_curr( 1 )
      --ON ACTION ultimo
         --CALL fgl_set_arr_curr( arr_count() )
      --ON ACTION anterior 
         --CALL fgl_set_arr_curr( arr_curr() - 1 )
      --ON ACTION siguiente 
         --CALL fgl_set_arr_curr( arr_curr() + 1 )
         
      ON ACTION generar ATTRIBUTES(TEXT="Generar Excel", IMAGE="mars_excel")
         IF ingreso() THEN 
            LET cuantos = consulta(FALSE)
            CALL fgl_set_arr_curr( arr_count() + 1 )
            --Refrescar Pantalla
            DISPLAY ARRAY reg_det TO sDet.*
               BEFORE DISPLAY  EXIT DISPLAY 
            END DISPLAY
         END IF
         CALL encabezado("")
         
         
         --CALL info_usuario()
      ON ACTION modificar
         LET id = arr_curr()
         LET ids = scr_line()
         IF id > 0 THEN 
            IF modifica() THEN
               LET reg_det[id].* = g_reg.*
               DISPLAY reg_det[id].* TO sDet[ids].*
            END IF   
         END IF 
         CALL encabezado("")
         --CALL info_usuario()
      ON ACTION anular
         LET id = arr_curr()
         LET ids = scr_line()         
         IF id > 0 THEN 
            IF anular() THEN
               --Si es anulacion
               --LET reg_det[id].* = g_reg.*
               --DISPLAY reg_det[id].* TO sDet[ids].*
               --Si es eliminacion
               CALL DIALOG.deleteRow("sdet", id)
               IF id = arr_count() THEN 
                  LET id = id - 1
               END IF 
               IF id > 0 THEN 
                  LET g_reg.* = reg_det[id].*
               ELSE 
                  INITIALIZE g_reg.* TO NULL
               END IF 
               DISPLAY BY NAME g_reg.*
            END IF   
         END IF 
      
      --ON ACTION reporte
         --CALL PrintReport()
      ON ACTION salir
         EXIT DISPLAY 
   END DISPLAY 
END FUNCTION

FUNCTION combo_init()
   DEFINE isAdmin CHAR(1)
   DEFINE txtSql STRING 
   
   --SELECT user_mod_cobro_adm INTO isAdmin FROM users WHERE usu_id = guser
   --IF isAdmin = '1' THEN
      --LET txtSql = "SELECT user_id, user_name FROM users WHERE user_status = 1"
   --END IF
   LET txtSql = "SELECT user_id, user_name FROM users WHERE user_status = 1 AND user_mod_cobro = '1'" 
   CALL combo_din2("liq_user",txtSql)
   CALL combo_din2("cmbusers","SELECT user_id, user_name FROM users WHERE user_status = 1")
   --CALL combo_din2("jefe",'SELECT id_commempl, TRIM(nombre) || " "|| TRIM(apellido) FROM commempl WHERE id_commempl > 0')
   --CALL combo_din2("cmbjefe",'SELECT id_commempl, TRIM(nombre) || " "|| TRIM(apellido) FROM commempl WHERE id_commempl > 0')
   --CALL combo_din2("cmbpuesto","SELECT * FROM commpue WHERE id_commpue > 0")
   --CALL combo_din2("cmbfamilia","SELECT * FROM glbmfam")
END FUNCTION 

FUNCTION encabezado(tit_enc)
DEFINE tit_enc STRING 

   --DISPLAY BY NAME  gtit_enc
END FUNCTION 

FUNCTION setAttr(id)
DEFINE  i, id  SMALLINT

   FOR i=1 TO reg_det.getLength()
      LET reg_det_attr[i].liq_fecha    = "black"
      LET reg_det_attr[i].liq_user     = "black"
      LET reg_det_attr[i].liq_fec_ini  = "black"
      LET reg_det_attr[i].liq_fec_fin  = "black"
   END FOR  

   LET reg_det_attr[id].liq_fecha      = "blue reverse"
   LET reg_det_attr[id].liq_user       = "blue reverse"
   LET reg_det_attr[id].liq_fec_ini    = "blue reverse"
   LET reg_det_attr[id].liq_fec_fin    = "blue reverse"
END FUNCTION 

FUNCTION runrepliq()
DEFINE runcmd STRING 

   LET runcmd = "sh ./runrepliq.sh ", guser CLIPPED, " ", ggrupo CLIPPED, " ", gsede, " ", g_reg.liq_fec_ini, " ", g_reg.liq_fec_fin 
   RUN runcmd
   
END FUNCTION 