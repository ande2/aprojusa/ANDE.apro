-- Insertando datos de los regiones

GLOBALS "aprom0805_glob.4gl"

FUNCTION insert_init()
 DEFINE strSql STRING 

 LET strSql =
  "INSERT INTO regiones (",
  "codreg, nombre, numref ",
  ") ",
  "VALUES (?,?,?)"

 PREPARE st_insertar FROM strSql
END FUNCTION 

FUNCTION ingreso()
 CALL encabezado("Ingresar")
 IF captura_datos('I') THEN
    RETURN grabar()
 ELSE 
    RETURN FALSE 
 END IF 
END FUNCTION 

FUNCTION grabar()
 LET g_reg.codreg = 0
 TRY 
  SELECT NVL(MAX(a.codreg),0)+1
   INTO  g_reg.codreg
   FROM  regiones a
   
  EXECUTE st_insertar USING g_reg.codreg, g_reg.nombre, g_reg.numref
 CATCH 
  CALL msgError(sqlca.sqlcode,"Grabar Registro")
  RETURN FALSE 
 END TRY
 DISPLAY BY NAME g_reg.*
 CALL box_valdato ("Registro agregado")
 RETURN TRUE
END FUNCTION 