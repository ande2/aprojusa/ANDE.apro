-- Modificando datos de los regiones

GLOBALS "aprom0805_glob.4gl"

FUNCTION update_init()
 DEFINE strSql STRING 

 LET strSql = 
 "UPDATE regiones ",
 "SET ",
 " nombre  = ?, ",
 " numref  = ? ",
 " WHERE codreg = ? " 

 PREPARE st_modificar FROM strSql
END FUNCTION 

FUNCTION modifica()
 CALL encabezado("Modificar")
   
 IF captura_datos('M') THEN
    RETURN actualizar()
 ELSE 
    RETURN FALSE 
 END IF 
END FUNCTION 

FUNCTION actualizar()
 TRY
  EXECUTE st_modificar USING g_reg.nombre, g_reg.numref, u_reg.codreg 
 CATCH 
  CALL msgError(sqlca.sqlcode,"Modificar Registro")
  RETURN FALSE 
 END TRY
 CALL msg("Registro actualizado")
 RETURN TRUE 
END FUNCTION 

FUNCTION delete_init()
 DEFINE strSql STRING 

 LET strSql =
  "DELETE FROM regiones ",
  "WHERE codreg = ? "
      
 PREPARE st_delete FROM strSql
END FUNCTION 

FUNCTION eliminar()
 IF NOT box_confirma("Esta seguro de eliminar el registro") THEN
    RETURN FALSE
 END IF 

 TRY 
  EXECUTE st_delete USING g_reg.codreg
 CATCH 
  CALL msgError(sqlca.sqlcode,"Eliminar Registro")
  RETURN FALSE 
 END TRY
 CALL msg("Registro eliminado")
 RETURN TRUE 
END FUNCTION 