






{ TABLE "informix".contact row size = 2662 number of columns = 42 index size = 43 }

create table "informix".contact 
  (
    contact_num integer not null ,
    contact_cod_region integer,
    contact_city integer not null ,
    contact_estado_caso char(2),
    contact_operacion char(20) not null ,
    contact_producto varchar(18),
    contact_garantia varchar(40),
    contact_estado_op varchar(20),
    contact_dpi_cliente varchar(20),
    contact_name varchar(100) not null ,
    contact_street varchar(240),
    contact_tel_cliente varchar(240),
    contact_saldo_cap varchar(20),
    contact_cap_vencido varchar(20),
    contact_int_vencido varchar(20),
    contact_saldo_imo varchar(20),
    contact_otros varchar(20),
    contact_total varchar(20),
    contact_porc_serv varchar(20),
    contact_total_cobrar varchar(20),
    contact_fec_ult_pago varchar(20),
    contact_cuotas_mora varchar(20),
    contact_fec_mora varchar(20),
    contact_nom_fia1 varchar(240),
    contact_dir_fia1 varchar(240),
    contact_nom_fia2 varchar(240),
    contact_dir_fia2 varchar(240),
    contact_nom_fia3 varchar(240),
    contact_dir_fia3 varchar(240),
    contact_rec_muser varchar(20) not null ,
    contact_rec_mtime datetime year to fraction(3) not null ,
    contact_rec_mstat char(2) not null ,
    contact_when datetime year to fraction(3),
    contact_valid char(1) not null ,
    contact_num_m varchar(40),
    contact_num_w varchar(40),
    contact_num_h varchar(40),
    contact_user varchar(20) not null ,
    contact_loc_lon decimal(10,6),
    contact_loc_lat decimal(10,6),
    contact_photo_mtime datetime year to fraction(3),
    contact_photo byte,
    unique (contact_operacion)  constraint "informix".contoper,
    primary key (contact_num) 
  );

revoke all on "informix".contact from "public" as "informix";




alter table "informix".contact add constraint (foreign key (contact_city) 
    references "informix".city );


