IMPORT util

DEFINE mail RECORD
    to      STRING,
    cc      STRING, 
    bcc,
    subject STRING,
    body    STRING
END RECORD ,
    attach  STRING 
--END RECORD

MAIN

DEFINE result STRING
  
    LET mail.to = "santizo.carlos@gmail.com"
    LET mail.cc = "santizo.carlos@outlook.com"
    LET mail.bcc = "contabilidad@solc.com.gt"
    LET mail.subject = "Test Subject"
    LET mail.body = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
    LET attach = '"E:/Tmp/report.docx" '
    

    CALL ui.Interface.loadStyles("ex_mail")
    CLOSE WINDOW SCREEN
    OPEN WINDOW w WITH FORM "ex_mail"

    INPUT BY NAME mail.* ATTRIBUTES(WITHOUT DEFAULTS=TRUE, UNBUFFERED)
        
        ON ACTION launchurl1
            CALL ui.Interface.frontCall("standard","launchurl",mailto_url(), result)
        ON ACTION launchurl2
            CALL ui.Interface.frontCall("standard","launchurl",mailto_url(), result)

        AFTER INPUT
            IF int_flag THEN
                EXIT INPUT
            END IF
    END INPUT
END MAIN

FUNCTION mailto_url()
    DISPLAY "URL ", SFMT("mailto:%1?subject=%4&cc=%2&bcc=%3&body=%5&attachment=%6", mail.to, mail.cc, mail.bcc, util.Strings.urlEncode(mail.subject),util.Strings.urlEncode(mail.body), util.Strings.urlEncode(attach))
    RETURN SFMT("mailto:%1?subject=%4&cc=%2&bcc=%3&body=%5&attachment=%6", mail.to, mail.cc, mail.bcc, util.Strings.urlEncode(mail.subject),util.Strings.urlEncode(mail.body), util.Strings.urlEncode(attach))
END FUNCTION