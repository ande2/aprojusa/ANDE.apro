MAIN
  DEFINE result, id INTEGER
  DEFINE str STRING

-- first, we initialize the module
  CALL ui.Interface.frontCall("WinMail", "Init", [], [id])

-- Set the body of the mail
  CALL ui.interface.frontCall("WinMail", "SetBody",
   [id, "This is a text mail using WinMail API - MAPI"], [result])

-- Set the subject of the mail
  CALL ui.interface.frontCall("WinMail", "SetSubject",
   [id, "test mail - ignore it"], [result])

-- Set the mail sender
  CALL ui.Interface.frontCall("WinMail", "SetFrom", [id, "Carlos Santizo", "carlos@solc.com.gt"], [result])

-- Set the SMTP server
  CALL ui.interface.frontCall("WinMail","SetSmtp", [id, "mail.solc.com.gt:2525"],[result])
   
-- Add an Addressee as "TO"
  CALL ui.Interface.frontCall("WinMail", "AddTo",
   [id, "myBoss", "contabilidad@solc.com.gt"], [result])

-- Add another Addresse as "BCC"
  CALL ui.Interface.frontCall("WinMail", "AddBCC",
   [id, "my friend", "santizo.carlos@gmail.com"], [result])

-- Add Two attachments
  CALL ui.Interface.frontCall("WinMail", "AddAttachment",
   [id, "e:\\Tmp\\report.docx"], [result])
  CALL ui.Interface.frontCall("WinMail", "AddAttachment",
   [id, "e:\\Tmp\\demo.png"], [result])

-- Send the mail via the default mailer
  
  CALL ui.Interface.frontCall("WinMail", "SendMailSMTP", [id], [result])
  IF result == TRUE THEN
      DISPLAY "Enviado"
    DISPLAY "Message sent successfully"
  ELSE
      
    CALL ui.Interface.frontCall("WinMail", "GetError", [id], [str])
    DISPLAY "No Enviado - ", id, " - ", str
    DISPLAY str  
  END IF

  CALL ui.Interface.frontCall("WinMail", "Close", [id], [result])
END MAIN