MAIN
  DEFINE s, res STRING
  LET s = "12345.678"
  let res = formatoNum(s)
  display "s ", s
  display "res ", res
  --DISPLAY s.getIndexOf(".",1)
END MAIN

FUNCTION formatoNum(v_ini)
DEFINE v_ini, v_fin, n_ini  STRING 
DEFINE i, j, posP           SMALLINT
DEFINE s_arr DYNAMIC ARRAY OF STRING  

   LET j = 1
   LET posP = NULL 
   LET posP = v_ini.getIndexOf(".",1)
   IF posP > 0 THEN  
      LET n_ini = v_ini.subString(1,posP-1)
      IF v_ini.getLength() >= posP+2 THEN  
         LET s_arr[j] = v_ini.subString(posP+1,posP+2)
      ELSE 
         IF v_ini.getLength() >= posP+1 THEN
            LET s_arr[j] = v_ini.subString(posP+1,posP+1), "0"
         ELSE  
           LET s_arr[j] = "00"
         END IF 
     END IF 
   ELSE  
     LET n_ini = v_ini
     LET s_arr[j] = "00"
   END IF 
   LET j = 2

   FOR i = n_ini.getLength() TO 1 STEP -3
      IF i - 3 > 0 THEN
         LET s_arr[j] = n_ini.subString(i-2,i) 
         LET j = j + 1
      ELSE 
         LET s_arr[j] = n_ini.subString(1,i)
    END IF 
     
   END FOR 

   FOR i = j TO 1 STEP -1
      IF i = 1 THEN 
        LET v_fin = v_fin.subString(1,v_fin.getLength()-1) , ".",s_arr[i] CLIPPED 
      ELSE  
         LET v_fin = v_fin, s_arr[i], ","
      END IF 
   END FOR 
   
RETURN v_fin

END FUNCTION 
