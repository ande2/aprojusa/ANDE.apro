DATABASE aprojusa

MAIN
DEFINE sql_stmt STRING 

  LET sql_stmt = prepsql()

  e
END MAIN 

FUNCTION prepsql()
 DEFINE sql_stmt STRING 
 DEFINE contador SMALLINT 
   
 LET sql_stmt = 
  "SELECT ",
  --DATOS INICIALES
  "a.nomdoc,", --Tipo de Documento: HIPOTECARIO
  "a.numexp,", --Numero de Expediente
  "a.codpro,", --Ges ó Fisico
  "a.numcre,", --Numero de Ges
  "a.nomreg,", --Region
  "a.numres,", --Numero de Resolución
  "a.fecres,", --Fecha de Resolución
  "a.numage,", --No de Agencia
  "a.nomage,", --Nombre de Agencia
  "a.nomasi,", --Nombre Asistente responsable
  "a.fecasi,", --Fecha de Asignación
  
  --DATOS ESPECIFICOS DE LA ESCRITURA
  "a.nomnot,", --Nombre Notario autorizante
  "a.numesc,", --Numero de Escritura
  "a.fecesc,", --Fecha de Escritura
  "a.hojini,", --Correlativo Inicial de la Hoja de protocolo
  "a.hojfin,", --Correlativo Final de la Hoja de protocolo
  "a.todipt,", --Total de Hojas utilizadas 
  "a.todies,", --Total de dias para escriturar
  
  --NOMBRE DEL USUARIO
  "a.nomcli,", --Nombre del cliente o Usuario
  "a.nombre,", --Nombre del tipo de credito o documento
  "a.valcre,", --Monto del Credito

  --"a.honora,a.totiva,a.timnot,a.prites,a.tesesp,a.ivacom,a.honbas,",
  --DESGLOSE PARA COBRO
  "a.honora,", --Honorarios 
  "a.honora*.12,", --IVA
  "a.timnot,", --Timbre Notarial 2 por millar 
  "a.prites,", --Primer Testimonio Q150
  "a.tesesp,", --Testimonio Especial Q150
  "a.ivacom,", --IVA Compra-Venta
  "a.honbas,", --Honorarios Base registro de la propiedad
  "a.lugfin,", --Lugar que ocupará la finca (0,1,2,3....)
  "a.valfin,", --Insc por el lugar que ocupara la finca en RGP Q50
  "a.honexe,", --Honorarios Excedente RGP 1.5 por millar
  "a.redhon,", --Redondeo Honorarios Excedente RGP
  "a.canfin,", --Cantidad de fincas a insc en RGP 
  "a.finext,", --Finca extra a inscribir en RGP Q50
  "a.gastot,", --Gasto Total segundo RGP
  "a.gasmun,", --Gastos de reg Municipalidad
  "a.difcob,", --Diferencia monto Cobrado por garantia
  "a.vtcoba,", -- "a.toacob,", --Total Cobrado reg de la prop base, exced, fincas adic
  "a.canele,", --Cantidad consultas Electronicas
  "a.valele,", --Valor por consultas Electronicas
  "a.gascer,", --Gastos de Certificacion del RGP Q100 c/u
  "a.hojpro,", --Hojas de Protocolo
  "a.docext,", --Documento Extra  
  
  --campo total a cobrar, es la suma de otros campos
  "(nvl(a.honora,0)+nvl((a.honora*.12),0)+nvl(a.timnot,0)+nvl(a.prites,0)+",
  " nvl(a.tesesp,0)+nvl(a.ivacom,0)+nvl(a.honbas,0)+", -- nvl(a.lugfin,0)+",
  " nvl(a.valfin,0)+", --nvl(a.honexe,0)
  " nvl(a.redhon,0)+nvl(a.canfin,0)+",
  " nvl(a.finext,0)+nvl(a.gastot,0)+nvl(a.gasmun,0)+nvl(a.difcob,0)+", --" nvl(a.toacob,0)+nvl(a.canele,0)
  " nvl(a.valele,0)+nvl(a.gascer,0)+",
  " nvl(a.hojpro,0)+nvl(a.docext,0)) total, ", --Total a Cobrar --a.totcob ", 
  --"a.tototr,", --??

  --PROCESO DE FORMALIZACIÓN
  "a.fevfir,", --Fecha Envio a Firma
  "a.frefir,", --Regreso de Firma
  "a.tdifir,", --Total de Dias Firma
  "a.finrpg,", --Fecha Ingreso al RGP
  "a.fegrpg,", --Fecha Egreso del RGP
  "a.nrerpg,", --Numero de Recibo del RGP
  "a.ndbrpg,", --Numero de Documento / No de Boleta prenda RGP
  "a.toparp,", --Total Pagado RGP
  "a.topaco,", --Total Pagado vs Cobrado
  "a.codrgo,", --Codigo de Registro o muni que opera
  "a.codarp,", --Codigo procurador RGP
  "a.todifo,", --Total de días

  --REINGRESO AL RGP
  "a.canrch,", --Cantidad o numero de Rechazos
  "a.totrch,", --Total o monto de Rechazos 
  "a.ferech,", --Fecha del Rechazo
  "a.fecrei,", --Fecha de Reingreso
  "a.nrecrg,", --No de Recibo del RGP
  "a.ndocrg,", --No de Documento del RGP
  "a.fecdev,", --Fecha de Devolucion

  --COMENTARIOS DEL PROCESO DE ELABORACION
  "a.coment,", --Comentarios

  --DATOS DEL PAGO Y FACTURACION - DEPOSITO COMPLETO
  --"a.todidv,", --?? 
  --"a.todich,", --??
  "a.totpag,", --Total Pagado
  "a.numbol,", --No de Boleta
  "a.fecbol,", --Fecha de la Boleta
  "' ',"     , --No de Factura Electrónica
  --"a.numdep,", --??
  
  --ESTADO DE CUENTA, HONORARIOS BUFETE Y GASTOS FORMALIZACION
  "a.fecdeb,", --Fecha del Debito
  "a.nuboho,", --Numero de Boleta de Honorarios
  "a.modeho,", --Monto del Debito de Honorarios
  "a.nuboga,", --Numero de Boleta de Gastos de formalización
  "a.modega,", --Monto de Gastos - debito
  "a.nufade,", --Numero de Factura Electronica por debito

  --ENVIO A PCL O BANCO
  "a.lugenv,", --Lugar de Envio
  "a.fecenv,", --Fecha de Envio
  "a.fecrep,", --Fecha de Recepcion
  "a.nucoev,", --Numero de Correlativo de doc de Envio
  "a.todiev,", --numero Total de Dias de Envio
  "a.diapro,", --total de Dias utilizados en el Proceso

  --ETAPA
  "a.numeta,", --Estatus

  --FECHA DE PAGO
  "a.fpagas,", --Fecha de pago al Asistente
  "a.mpagno,", --Mes de Pago al Notario
  "a.observ ", --Comentarios 

  "FROM  visexcelnotariado a ",
  "WHERE exists (SELECT x.numexp FROM tmp_expedientes x ",
                 "WHERE x.numexp = a.numexp) ",
  "ORDER BY 1"

   RETURN sql_stmt
END FUNCTION