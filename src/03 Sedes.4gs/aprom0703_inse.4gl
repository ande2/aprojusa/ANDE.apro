################################################################################
# Funcion     : %M%
# Descripcion : Ingreso de un nuevo umdor 	
# Funciones   : input_umd() funcion para insertar datos
#              
#              
#               
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Anderson Garcia 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################

GLOBALS "aprom0703_glob.4gl"

FUNCTION insert_init()
DEFINE strSql STRING 

   LET strSql =
      "INSERT INTO city ( ",
        " city_num, city_name, city_country, city_cuenta, city_telefono, city_region, ",
        " city_gerente, city_coordinador ",
        "   ) ",
      " VALUES (?,?,?,?,?,?,?,?)"

   PREPARE st_insertar FROM strSql

END FUNCTION 

FUNCTION ingreso()
   CALL encabezado("Ingresar")
   --CALL info_usuario()
   IF captura_datos('I') THEN
      RETURN grabar()
   ELSE 
      RETURN FALSE 
   END IF 
END FUNCTION 

FUNCTION grabar()
   --SELECT NVL(MAX(city_num)+1,1) INTO g_reg.city_num FROM city
   --LET g_reg.idpais = 0
   TRY 
      EXECUTE st_insertar USING 
            g_reg.city_num, g_reg.city_name, g_reg.city_country, g_reg.city_cuenta, 
            g_reg.city_telefono, g_reg.city_region, g_reg.city_gerente, g_reg.city_coordinador

      --CS agregarlo cuando se agregue el ID 
      --LET g_reg.idpais = SQLCA.sqlerrd[2]
   CATCH 
      CALL msgError(sqlca.sqlcode,"Grabar Registro")
      RETURN FALSE 
   END TRY
   DISPLAY BY NAME g_reg.*
   CALL box_valdato ("Registro agregado")
   --CALL combo_din2("cmbdepartamento","SELECT * FROM commdep WHERE id_commdep > 0")
   RETURN TRUE 
END FUNCTION 