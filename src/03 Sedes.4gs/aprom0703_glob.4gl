################################################################################
# Funcion     : %M%
# nombre      : Catalogo de Tipos
#               Funcion para definicion de globales
# Funciones   : 
#               
#               
#  
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        nombre de la modificacion
#
################################################################################
--DATABASE comisiones
SCHEMA aprojusa

GLOBALS 
TYPE 
   tDet RECORD 
      city_num          LIKE city.city_num,
      city_name         LIKE city.city_name,
      city_country      LIKE city.city_country,
      city_cuenta       LIKE city.city_cuenta,
      city_telefono     LIKE city.city_telefono,
      city_region       LIKE city.city_region,
      city_gerente      LIKE city.city_gerente,
      city_coordinador  LIKE city.city_coordinador
      
   END RECORD

DEFINE reg_det_attr DYNAMIC ARRAY OF RECORD 
      city_num          STRING,
      city_name         STRING,
      city_country      STRING,
      city_cuenta       STRING,
      city_telefono     STRING,
      city_region       STRING,
      city_gerente      STRING,
      city_coordinador  STRING 
   END RECORD
   
DEFINE
   reg_det DYNAMIC ARRAY OF tDet, 
   g_reg, u_reg tDet,
   dbname      STRING,
   
   condicion   STRING --Condicion de la clausula Where 
   CONSTANT    prog_name = "aprom0703"
END GLOBALS