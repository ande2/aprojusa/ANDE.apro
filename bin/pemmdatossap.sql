






{ TABLE "informix".pemmdatossap row size = 4876 number of columns = 23 index size 
              = 0 }
create table "informix".pemmdatossap 
  (
    idboletaing integer not null ,
    tipdoc smallint not null ,
    ipservidorbd varchar(255) not null ,
    userservirdorbd varchar(255) not null ,
    passuserservidorbd varchar(255) not null ,
    ipservirdorsap varchar(255) not null ,
    nombrebd varchar(255) not null ,
    usuariosap varchar(255) not null ,
    contrasenasap varchar(255) not null ,
    numordpro varchar(255),
    fechaconta date,
    serrecpro varchar(255),
    sersalmer varchar(255),
    serentmer varchar(255),
    comentariosdoc varchar(255),
    codigosocioneg varchar(255),
    nombresocioneg varchar(255),
    numreciboprosap varchar(255),
    numsalidainvsap varchar(255),
    numentradainvsap varchar(255),
    numentmercexpsap varchar(255),
    numentmerclocsap varchar(255),
    estado smallint not null 
  );
revoke all on "informix".pemmdatossap from "public" as "informix";




