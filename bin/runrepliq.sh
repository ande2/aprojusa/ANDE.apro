. /opt/informix/ifmx.sh
. /opt/4js/gst310/rt/envgenero

LANG=es_GT.utf8
export LANG

FGL_LENGTH_SEMANTICS=CHAR
export FGL_LENGTH_SEMANTICS

DB_LOCALE=es_es.utf8
export DB_LOCALE

POI_HOME=/app/Aprojusa/ANDE.apro/src/Carga_datos_POI/java/poi-3.10.1
export POI_HOME

CLASSPATH=$POI_HOME/poi-3.10.1-20140818.jar:$POI_HOME/poi-ooxml-3.10.1-20140818.jar:$POI_HOME/poi-ooxml-schemas-3.10.1-20140818.jar:$POI_HOME/ooxml-lib/dom4j-1.6.1.jar:$POI_HOME/ooxml-lib/xmlbeans-2.6.0.jar:$CLASSPATH
export CLASSPATH

JRE_HOME=/app/Aprojusa/ANDE.apro/src/Carga_datos_POI/java/jdk-10.0.1
export JRE_HOME

PATH=$JRE_HOME/bin:$PATH
export PATH

LD_LIBRARY_PATH=$JRE_HOME/lib/server:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH

fglrun apror0710.42r $1 $2 $3 $4 $5
