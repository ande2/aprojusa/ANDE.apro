grant dba to "informix";
grant dba to "carlos";
grant dba to "public";









{ TABLE "informix".grupo row size = 260 number of columns = 2 index size = 9 }

create table "informix".grupo 
  (
    grpid serial not null ,
    grpnombre varchar(255),
    primary key (grpid)  constraint "informix".pk_grupo
  );

revoke all on "informix".grupo from "public" as "informix";

{ TABLE "informix".menu row size = 1266 number of columns = 5 index size = 18 }

create table "informix".menu 
  (
    menid serial not null ,
    mennombre varchar(255),
    mencmd char(1000),
    mentipo smallint,
    menpadre integer,
    primary key (menid)  constraint "informix".pk_menu
  );

revoke all on "informix".menu from "public" as "informix";

{ TABLE "informix".permiso row size = 8 number of columns = 2 index size = 31 }

create table "informix".permiso 
  (
    pergrpid integer not null ,
    permenid integer not null ,
    primary key (pergrpid,permenid)  constraint "informix".pk_permiso
  );

revoke all on "informix".permiso from "public" as "informix";

{ TABLE "informix".usuario row size = 776 number of columns = 5 index size = 18 }

create table "informix".usuario 
  (
    usuid serial not null ,
    usulogin varchar(255),
    usupwd varchar(255),
    usunombre varchar(255),
    usugrpid integer,
    primary key (usuid)  constraint "informix".pk_usuario
  );

revoke all on "informix".usuario from "public" as "informix";

{ TABLE "informix".commdep row size = 268 number of columns = 4 index size = 0 }

create table "informix".commdep 
  (
    id_commdep serial not null ,
    descripcion varchar(255),
    hijo_de integer,
    estado integer
  );

revoke all on "informix".commdep from "public" as "informix";

{ TABLE "informix".commpue row size = 268 number of columns = 4 index size = 0 }

create table "informix".commpue 
  (
    id_commpue serial not null ,
    descripcion varchar(255),
    estado integer,
    id_commdep integer
  );

revoke all on "informix".commpue from "public" as "informix";

{ TABLE "informix".glbmest row size = 262 number of columns = 3 index size = 9 }

create table "informix".glbmest 
  (
    id_estado serial not null ,
    descripcion varchar(255),
    estado smallint,
    primary key (id_estado)  constraint "informix".pk_estado
  );

revoke all on "informix".glbmest from "public" as "informix";

{ TABLE "informix".comdasic row size = 205 number of columns = 2 index size = 0 }

create table "informix".comdasic 
  (
    id_comdasi integer,
    comentario varchar(200)
  );

revoke all on "informix".comdasic from "public" as "informix";

{ TABLE "informix".comdasi row size = 44 number of columns = 11 index size = 9 }

create table "informix".comdasi 
  (
    id_comdasi serial not null ,
    id_commbd integer,
    valor decimal(12,2),
    funcion integer,
    porcentaje decimal(4,2),
    estado integer,
    id_commemp integer,
    id_commdep integer,
    id_commpue integer,
    id_commempl integer,
    nivel smallint,
    primary key (id_comdasi)  constraint "informix".pk_asignaciones
  );

revoke all on "informix".comdasi from "public" as "informix";

{ TABLE "informix".comdtel row size = 21 number of columns = 2 index size = 0 }

create table "informix".comdtel 
  (
    telefono varchar(16),
    id_commempl integer
  );

revoke all on "informix".comdtel from "public" as "informix";

{ TABLE "informix".commasig row size = 25 number of columns = 6 index size = 0 }

create table "informix".commasig 
  (
    id_commempl integer not null ,
    id_commbd integer not null ,
    orden smallint not null ,
    funcion integer,
    operador varchar(1),
    valor decimal(15,5)
  );

revoke all on "informix".commasig from "public" as "informix";

{ TABLE "informix".commesp row size = 46 number of columns = 3 index size = 0 }

create table "informix".commesp 
  (
    id_commesp serial not null ,
    codigo varchar(15),
    descripcion varchar(25)
  );

revoke all on "informix".commesp from "public" as "informix";

{ TABLE "informix".commbd row size = 282 number of columns = 9 index size = 9 }

create table "informix".commbd 
  (
    id_commbd serial not null ,
    operador char(1),
    descripcion varchar(255),
    valor decimal(11,2),
    funcion smallint,
    bonodesc smallint,
    nomespecial integer,
    estado integer,
    modificable "informix".boolean,
    primary key (id_commbd)  constraint "informix".pk_bonidesc
  );

revoke all on "informix".commbd from "public" as "informix";

{ TABLE "informix".glb_paramtrs row size = 349 number of columns = 7 index size = 13 }

create table "informix".glb_paramtrs 
  (
    numpar integer not null ,
    tippar integer not null ,
    nompar varchar(60,1),
    valchr varchar(255,1) not null ,
    userid varchar(15,1) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (numpar,tippar)  constraint "informix".pkglbparamtrs
  );

revoke all on "informix".glb_paramtrs from "public" as "informix";

{ TABLE "informix".mestructura row size = 35 number of columns = 2 index size = 9 }

create table "informix".mestructura 
  (
    idestructura serial not null ,
    nomestructura varchar(30),
    primary key (idestructura) 
  );

revoke all on "informix".mestructura from "public" as "informix";

{ TABLE "informix".pemmvalvula row size = 55 number of columns = 2 index size = 9 }

create table "informix".pemmvalvula 
  (
    idvalvula serial not null ,
    nomvalvula varchar(50),
    primary key (idvalvula) 
  );

revoke all on "informix".pemmvalvula from "public" as "informix";

{ TABLE "informix".pemmcatemp row size = 43 number of columns = 4 index size = 18 }

create table "informix".pemmcatemp 
  (
    idcatempaque serial not null ,
    idtipempaque integer,
    nomcatempaque varchar(30),
    pesocatempaque decimal(5,2),
    primary key (idcatempaque) 
  );

revoke all on "informix".pemmcatemp from "public" as "informix";

{ TABLE "informix".pemmtipemp row size = 35 number of columns = 2 index size = 9 }

create table "informix".pemmtipemp 
  (
    idtipempaque serial not null ,
    nomtipempaque varchar(30),
    primary key (idtipempaque) 
  );

revoke all on "informix".pemmtipemp from "public" as "informix";

{ TABLE "informix".pemmfinca row size = 93 number of columns = 6 index size = 9 }

create table "informix".pemmfinca 
  (
    idfinca serial not null ,
    fincanomct varchar(8) not null ,
    fincanommd varchar(18) not null ,
    fincanomlg varchar(50) not null ,
    id_commemp integer,
    serieenvio varchar(5),
    primary key (idfinca) 
  );

revoke all on "informix".pemmfinca from "public" as "informix";

{ TABLE "informix".pemdordpro row size = 24 number of columns = 6 index size = 49 }

create table "informix".pemdordpro 
  (
    idordpro integer not null ,
    iddordpro integer not null ,
    idfinca integer,
    idestructura integer,
    idvalvula integer,
    iditem integer,
    primary key (idordpro,iddordpro) 
  );

revoke all on "informix".pemdordpro from "public" as "informix";

{ TABLE "informix".pemdboleta row size = 23 number of columns = 6 index size = 20 }

create table "informix".pemdboleta 
  (
    idboletaing integer not null ,
    idlinea smallint not null ,
    cantcajas smallint,
    pesobruto decimal(7,2),
    pesotara decimal(7,2),
    pesoneto decimal(7,2),
    primary key (idboletaing,idlinea) 
  );

revoke all on "informix".pemdboleta from "public" as "informix";

{ TABLE "informix".catdsolicitudnotas row size = 284 number of columns = 5 index size = 20 }

create table "informix".catdsolicitudnotas 
  (
    idsolicitud integer not null ,
    idnota smallint not null ,
    nota varchar(255),
    fectran char(18),
    id_commempl integer,
    primary key (idsolicitud,idnota) 
  );

revoke all on "informix".catdsolicitudnotas from "public" as "informix";

{ TABLE "informix".catemplxempr row size = 8 number of columns = 2 index size = 13 }

create table "informix".catemplxempr 
  (
    id_commempl integer not null ,
    id_commemp integer not null ,
    primary key (id_commempl,id_commemp) 
  );

revoke all on "informix".catemplxempr from "public" as "informix";

{ TABLE "informix".catitemxempr row size = 12 number of columns = 3 index size = 30 }

create table "informix".catitemxempr 
  (
    iditem integer not null ,
    idalmacen integer not null ,
    id_commemp integer not null ,
    primary key (iditem,idalmacen,id_commemp) 
  );

revoke all on "informix".catitemxempr from "public" as "informix";

{ TABLE "informix".catmsolicitud row size = 294 number of columns = 12 index size = 22 }

create table "informix".catmsolicitud 
  (
    idsolicitud serial not null ,
    itemdescripcion varchar(200),
    unimed varchar(10),
    cuentacont varchar(20),
    codigocopiaitem varchar(20),
    fectransolicitud datetime year to second,
    fectranautorizacion datetime year to second,
    estado integer,
    id_commemp integer,
    idalmacen integer,
    ususolicita integer,
    usuautoriza integer,
    primary key (idsolicitud) 
  );

revoke all on "informix".catmsolicitud from "public" as "informix";

{ TABLE "informix".glbmalmacen row size = 61 number of columns = 4 index size = 13 }

create table "informix".glbmalmacen 
  (
    idalmacen serial not null ,
    nombre varchar(50),
    estado smallint,
    id_commemp integer not null ,
    primary key (idalmacen,id_commemp) 
  );

revoke all on "informix".glbmalmacen from "public" as "informix";

{ TABLE "informix".glbmitems row size = 202 number of columns = 11 index size = 22 }

create table "informix".glbmitems 
  (
    iditem serial not null ,
    desitemct varchar(8),
    desitemmd varchar(15),
    desitemlg varchar(50),
    codalterno varchar(18),
    estado smallint,
    idfamilia integer,
    idtipo integer,
    variedad varchar(30),
    calidad varchar(30),
    mercado varchar(30),
    primary key (iditem) 
  );

revoke all on "informix".glbmitems from "public" as "informix";

{ TABLE "informix".glbmfam row size = 55 number of columns = 2 index size = 9 }

create table "informix".glbmfam 
  (
    idfamilia serial not null ,
    nombre varchar(50),
    primary key (idfamilia) 
  );

revoke all on "informix".glbmfam from "public" as "informix";

{ TABLE "informix".glbmtip row size = 111 number of columns = 5 index size = 22 }

create table "informix".glbmtip 
  (
    idtipo serial not null ,
    nombre varchar(50),
    idfamilia integer not null ,
    iditemsap varchar(25),
    cueafe varchar(25),
    primary key (idtipo,idfamilia) 
  );

revoke all on "informix".glbmtip from "public" as "informix";

{ TABLE "informix".pemmcli row size = 114 number of columns = 6 index size = 18 }

create table "informix".pemmcli 
  (
    idcliente serial not null ,
    nombre varchar(75),
    estado smallint,
    codigo varchar(25),
    tipo smallint,
    pais integer,
    primary key (idcliente) 
  );

revoke all on "informix".pemmcli from "public" as "informix";

{ TABLE "informix".pemmboletaexp row size = 27 number of columns = 6 index size = 18 }

create table "informix".pemmboletaexp 
  (
    idboletaing integer not null ,
    idboletaexp serial not null ,
    fectran datetime year to second,
    usuopero integer,
    pesototal decimal(7,2),
    calidadproduccion smallint,
    primary key (idboletaexp) 
  );

revoke all on "informix".pemmboletaexp from "public" as "informix";

{ TABLE "informix".pemcboletaexp row size = 10 number of columns = 3 index size = 31 }

create table "informix".pemcboletaexp 
  (
    idboletaexp integer not null ,
    idcliente integer not null ,
    peso smallint,
    primary key (idboletaexp,idcliente) 
  );

revoke all on "informix".pemcboletaexp from "public" as "informix";

{ TABLE "informix".pemdboletaexp row size = 18 number of columns = 6 index size = 30 }

create table "informix".pemdboletaexp 
  (
    idboletaexp integer not null ,
    idcliente integer not null ,
    lnkitem integer not null ,
    cantcajas smallint,
    peso smallint,
    pesototal smallint,
    primary key (lnkitem,idboletaexp,idcliente) 
  );

revoke all on "informix".pemdboletaexp from "public" as "informix";

{ TABLE "informix".itemsxgrupo row size = 251 number of columns = 12 index size = 32 }

create table "informix".itemsxgrupo 
  (
    lnkitem integer not null ,
    iditem integer not null ,
    iditemsap char(12) not null ,
    nombre varchar(100),
    categoria char(1),
    peso smallint,
    iditemfac integer,
    iditemsapfac char(12),
    nombrefac varchar(100),
    pesofac smallint,
    fletecaja decimal(6,2),
    costodirecto decimal(6,2),
    unique (iditem,iditemsap,peso) 
  );

revoke all on "informix".itemsxgrupo from "public" as "informix";

{ TABLE "informix".andevarlog row size = 106 number of columns = 4 index size = 30 }

create table "informix".andevarlog 
  (
    andesesid char(25) not null ,
    parnombre varchar(25),
    parvalor varchar(50),
    fecha date,
    primary key (andesesid) 
  );

revoke all on "informix".andevarlog from "public" as "informix";

{ TABLE "informix".commempl row size = 987 number of columns = 15 index size = 0 }

create table "informix".commempl 
  (
    id_commempl serial not null ,
    nombre varchar(255),
    apellido varchar(255),
    telefono varchar(20),
    direccion varchar(100),
    codigo varchar(20),
    jefe integer,
    comision smallint,
    estado smallint,
    id_commpue integer,
    id_commdep integer,
    numcuenta varchar(25),
    usulogin varchar(25),
    usupwd varchar(255),
    usugrpid integer
  );

revoke all on "informix".commempl from "public" as "informix";

{ TABLE "informix".usuemp row size = 10 number of columns = 3 index size = 0 }

create table "informix".usuemp 
  (
    usuid integer not null ,
    id_commemp integer not null ,
    estado smallint
  );

revoke all on "informix".usuemp from "public" as "informix";

{ TABLE "informix".paises row size = 87 number of columns = 3 index size = 9 }

create table "informix".paises 
  (
    idpais serial not null ,
    codigo char(2),
    nombre varchar(80),
    primary key (idpais) 
  );

revoke all on "informix".paises from "public" as "informix";

{ TABLE "informix".itemsxempresa row size = 8 number of columns = 2 index size = 13 }

create table "informix".itemsxempresa 
  (
    id_commemp integer not null ,
    iditem integer not null ,
    primary key (id_commemp,iditem) 
  );

revoke all on "informix".itemsxempresa from "public" as "informix";

{ TABLE "informix".pemmcausa row size = 268 number of columns = 4 index size = 22 }

create table "informix".pemmcausa 
  (
    idcausa serial not null ,
    nombre varchar(255),
    idfamilia integer,
    idtipo integer,
    primary key (idcausa) 
  );

revoke all on "informix".pemmcausa from "public" as "informix";

{ TABLE "informix".pemdboletacau row size = 265 number of columns = 4 index size = 31 }

create table "informix".pemdboletacau 
  (
    idboletacau integer not null ,
    idcausa integer not null ,
    observaciones char(255),
    porcentaje smallint,
    primary key (idboletacau,idcausa) 
  );

revoke all on "informix".pemdboletacau from "public" as "informix";

{ TABLE "informix".pemmboletacau row size = 24 number of columns = 5 index size = 18 }

create table "informix".pemmboletacau 
  (
    idboletaing integer not null ,
    fectran datetime year to second,
    idboletacau serial not null ,
    usuopero integer,
    merma integer,
    primary key (idboletacau) 
  );

revoke all on "informix".pemmboletacau from "public" as "informix";

{ TABLE "informix".pemmboletaloc row size = 33 number of columns = 8 index size = 27 }

create table "informix".pemmboletaloc 
  (
    idboletaloc serial not null ,
    pesocli integer,
    tienetarima char(1),
    tipocaja integer,
    idboletaing integer,
    idcliente integer,
    fectran datetime year to second,
    usuopero integer,
    primary key (idboletaloc) 
  );

revoke all on "informix".pemmboletaloc from "public" as "informix";

{ TABLE "informix".pemdboletaloc row size = 26 number of columns = 8 index size = 20 }

create table "informix".pemdboletaloc 
  (
    idboletaloc integer not null ,
    lnkitem integer not null ,
    pesobruto integer,
    calidad smallint,
    idlinea smallint not null ,
    cantcajas smallint,
    pesotara integer,
    pesoneto integer,
    primary key (idboletaloc,idlinea) 
  );

revoke all on "informix".pemdboletaloc from "public" as "informix";

{ TABLE "informix".pemdcajemp row size = 40 number of columns = 10 index size = 40 }

create table "informix".pemdcajemp 
  (
    fecha date,
    prestamo smallint,
    devolucion smallint,
    tipomov smallint,
    idcatempaque integer not null ,
    id_commemp integer not null ,
    saldo integer,
    usuopero integer,
    fectran datetime year to fraction(3),
    idcajemp serial not null ,
    unique (idcatempaque,id_commemp) ,
    primary key (idcajemp) 
  );

revoke all on "informix".pemdcajemp from "public" as "informix";

{ TABLE "informix".itemsxfact row size = 148 number of columns = 7 index size = 30 }

create table "informix".itemsxfact 
  (
    lnkifac serial not null ,
    iditemsap char(12) not null ,
    nombre varchar(100),
    costo decimal(16,4),
    flete decimal(16,4),
    porc decimal(16,4),
    linkitem integer,
    unique (linkitem,iditemsap) ,
    primary key (lnkifac) 
  );

revoke all on "informix".itemsxfact from "public" as "informix";

{ TABLE "informix".mclientes row size = 117 number of columns = 3 index size = 9 }

create table "informix".mclientes 
  (
    lnkcli serial not null ,
    idclisap char(12) not null ,
    nombre varchar(100),
    primary key (lnkcli) 
  );

revoke all on "informix".mclientes from "public" as "informix";

{ TABLE "informix".preciosxitemfac row size = 21 number of columns = 4 index size = 9 }

create table "informix".preciosxitemfac 
  (
    lnkpre serial not null ,
    lnksem integer,
    lnkifac integer,
    precio decimal(16,4),
    primary key (lnkpre) 
  );

revoke all on "informix".preciosxitemfac from "public" as "informix";

{ TABLE "sistemas".glb_permxusr row size = 90 number of columns = 10 index size = 0 }

create table "sistemas".glb_permxusr 
  (
    codpro char(20) not null ,
    progid smallint not null ,
    userid char(15) not null ,
    activo smallint not null ,
    passwd char(20),
    fecini date,
    fecfin date,
    usuaid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null 
  );

revoke all on "sistemas".glb_permxusr from "public" as "sistemas";

{ TABLE "sistemas".glb_programs row size = 293 number of columns = 8 index size = 20 }

create table "sistemas".glb_programs 
  (
    codpro char(15) not null ,
    nompro varchar(100,1) not null ,
    dcrpro varchar(100,1) not null ,
    ordpro smallint not null ,
    actpro varchar(50,1),
    userid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (codpro)  constraint "sistemas".pkglbprograms
  );

revoke all on "sistemas".glb_programs from "public" as "sistemas";

{ TABLE "informix".pemmliquidacion row size = 16 number of columns = 5 index size = 9 }

create table "informix".pemmliquidacion 
  (
    idliq serial not null ,
    idcliente integer not null ,
    liqanio smallint not null ,
    liqsemliq smallint not null ,
    id_estado integer not null ,
    primary key (idliq) 
  );

revoke all on "informix".pemmliquidacion from "public" as "informix";

{ TABLE "informix".pemdliquidacion row size = 305 number of columns = 27 index size = 20 }

create table "informix".pemdliquidacion 
  (
    idliq integer not null ,
    liqcor smallint not null ,
    linkfac integer not null ,
    liqfacser varchar(30,1),
    liqfacnum varchar(30,1) not null ,
    liqfacfec date not null ,
    liqfacfecrec date,
    liqfacsemfec smallint not null ,
    liqfacsemrec smallint,
    liqcodigosap varchar(30,1) not null ,
    lnkitem integer,
    liqcajassap smallint not null ,
    liqpreciosap decimal(18,2) not null ,
    liqtotalsap decimal(18,2) not null ,
    liqcajasing smallint,
    liqprecioing decimal(18,2),
    liqtotaling decimal(18,2),
    liqpreciocliente decimal(18,2),
    liqtotalcliente decimal(18,2),
    liqdesemp decimal(18,2),
    liqdesrecl decimal(18,2),
    liqprecondes decimal(18,2),
    liqtotalcondes decimal(18,2),
    liqdifenprecio decimal(18,2),
    liqdiftotalenprecio decimal(18,2),
    liqnotacnotad varchar(30,1),
    liqnumncnd varchar(30,1),
    primary key (idliq,liqcor) 
  );

revoke all on "informix".pemdliquidacion from "public" as "informix";

{ TABLE "informix".pemmconsolidadoliq row size = 8 number of columns = 3 index size = 9 }

create table "informix".pemmconsolidadoliq 
  (
    idconliq serial not null ,
    cliqsemana smallint,
    cliqanio smallint,
    primary key (idconliq) 
  );

revoke all on "informix".pemmconsolidadoliq from "public" as "informix";

{ TABLE "informix".pemrelmconliq row size = 8 number of columns = 2 index size = 31 }

create table "informix".pemrelmconliq 
  (
    idconliq integer not null ,
    idliq integer not null ,
    primary key (idconliq,idliq) 
  );

revoke all on "informix".pemrelmconliq from "public" as "informix";

{ TABLE "informix".pemdconsolidadoliq row size = 131 number of columns = 15 index size = 20 }

create table "informix".pemdconsolidadoliq 
  (
    idconliq integer not null ,
    cliqcor smallint not null ,
    cliqcodigosap varchar(30,1) not null ,
    lnkitem integer not null ,
    cliqtotalcajas smallint not null ,
    cliqtotalvalor decimal(18,2) not null ,
    cliqprecio decimal(18,2) not null ,
    cliqdesemp decimal(18,2) not null ,
    cliqdescal decimal(18,2) not null ,
    cliqcostoporcaja decimal(18,2) not null ,
    cliqflete decimal(18,2) not null ,
    cliqcosdirecto decimal(18,2) not null ,
    cliqpordes decimal(18,2) not null ,
    cliqpreciocajaprod decimal(6,2) not null ,
    cliqpreciolibraprod decimal(6,2) not null ,
    primary key (idconliq,cliqcor) 
  );

revoke all on "informix".pemdconsolidadoliq from "public" as "informix";

{ TABLE "informix".pemmdesitem row size = 59 number of columns = 3 index size = 9 }

create table "informix".pemmdesitem 
  (
    iddescuento serial not null ,
    desdescripcion varchar(50,1) not null ,
    desvalor decimal(5,2) not null ,
    primary key (iddescuento) 
  );

revoke all on "informix".pemmdesitem from "public" as "informix";

{ TABLE "informix".commemp row size = 541 number of columns = 12 index size = 9 }

create table "informix".commemp 
  (
    id_commemp serial not null ,
    nombre varchar(255) not null ,
    nit varchar(15),
    telefono varchar(20),
    direccion varchar(100),
    imagen varchar(100),
    estado integer,
    dbname varchar(8),
    espropia "informix".boolean,
    cajasprestamosaldo smallint,
    codigo varchar(15),
    iniciales varchar(8),
    primary key (id_commemp)  constraint "informix".pk_empresa
  );

revoke all on "informix".commemp from "public" as "informix";

{ TABLE "informix".pemmboleta row size = 138 number of columns = 21 index size = 31 }

create table "informix".pemmboleta 
  (
    idboletaing serial not null ,
    fecha date,
    nomentrego varchar(30),
    tienetarima "informix".boolean,
    tipocaja integer,
    pesototal decimal(7,2),
    estado integer,
    fectran datetime year to second,
    tipoboleta smallint,
    idordpro integer,
    iddordpro integer,
    iditem integer,
    emplentrego integer,
    emplrecibio integer,
    productor integer,
    docpropio varchar(18),
    docproductor varchar(18),
    esdirecto smallint,
    docpropioserie varchar(5),
    estadoexportacion smallint,
    estadodevolucion smallint,
    primary key (idboletaing) 
  );

revoke all on "informix".pemmboleta from "public" as "informix";

{ TABLE "informix".dsemanacal row size = 14 number of columns = 4 index size = 9 }

create table "informix".dsemanacal 
  (
    lnksem integer not null ,
    sem_cal smallint,
    fec_ini date,
    fec_fin date
  );

revoke all on "informix".dsemanacal from "public" as "informix";

{ TABLE "informix".dsemanas row size = 30 number of columns = 9 index size = 24 }

create table "informix".dsemanas 
  (
    lnksem integer not null ,
    sem_com smallint not null ,
    sem_fac smallint not null ,
    sem_liq smallint not null ,
    pre_defcli decimal(5,2) not null ,
    cos_stdcom decimal(5,2) not null ,
    lnkitem integer not null ,
    fec_ini date,
    fec_fin date,
    primary key (lnksem,lnkitem,sem_liq) 
  );

revoke all on "informix".dsemanas from "public" as "informix";

{ TABLE "informix".msemanas row size = 36 number of columns = 11 index size = 18 }

create table "informix".msemanas 
  (
    lnksem serial not null ,
    idcliente integer not null ,
    ano smallint not null ,
    sem_com smallint not null ,
    sem_fac smallint not null ,
    sem_liq smallint not null ,
    fec_ini date not null ,
    fec_fin date not null ,
    sem_cal integer,
    rango_ini date,
    rango_fin date,
    primary key (lnksem) 
  );

revoke all on "informix".msemanas from "public" as "informix";

{ TABLE "informix".pemmordpro row size = 244 number of columns = 12 index size = 9 }

create table "informix".pemmordpro 
  (
    idordpro serial not null ,
    numordpro varchar(25),
    nomordpro varchar(50),
    fecinicio date,
    estado integer,
    fecfinal date,
    iditemsap varchar(12),
    costostdprod decimal(15,5),
    serrecpro varchar(25),
    sersalmer varchar(25),
    almart varchar(25),
    cueafe varchar(50),
    primary key (idordpro) 
  );

revoke all on "informix".pemmordpro from "public" as "informix";


grant select on "informix".grupo to "public" as "informix";
grant update on "informix".grupo to "public" as "informix";
grant insert on "informix".grupo to "public" as "informix";
grant delete on "informix".grupo to "public" as "informix";
grant index on "informix".grupo to "public" as "informix";
grant select on "informix".menu to "public" as "informix";
grant update on "informix".menu to "public" as "informix";
grant insert on "informix".menu to "public" as "informix";
grant delete on "informix".menu to "public" as "informix";
grant index on "informix".menu to "public" as "informix";
grant select on "informix".permiso to "public" as "informix";
grant update on "informix".permiso to "public" as "informix";
grant insert on "informix".permiso to "public" as "informix";
grant delete on "informix".permiso to "public" as "informix";
grant index on "informix".permiso to "public" as "informix";
grant select on "informix".usuario to "public" as "informix";
grant update on "informix".usuario to "public" as "informix";
grant insert on "informix".usuario to "public" as "informix";
grant delete on "informix".usuario to "public" as "informix";
grant index on "informix".usuario to "public" as "informix";
grant select on "informix".commasig to "public" as "informix";
grant update on "informix".commasig to "public" as "informix";
grant insert on "informix".commasig to "public" as "informix";
grant delete on "informix".commasig to "public" as "informix";
grant index on "informix".commasig to "public" as "informix";
grant select on "informix".commesp to "public" as "informix";
grant update on "informix".commesp to "public" as "informix";
grant insert on "informix".commesp to "public" as "informix";
grant delete on "informix".commesp to "public" as "informix";
grant index on "informix".commesp to "public" as "informix";
grant select on "informix".glb_paramtrs to "public" as "informix";
grant update on "informix".glb_paramtrs to "public" as "informix";
grant insert on "informix".glb_paramtrs to "public" as "informix";
grant delete on "informix".glb_paramtrs to "public" as "informix";
grant index on "informix".glb_paramtrs to "public" as "informix";
grant select on "informix".pemmvalvula to "public" as "informix";
grant update on "informix".pemmvalvula to "public" as "informix";
grant insert on "informix".pemmvalvula to "public" as "informix";
grant delete on "informix".pemmvalvula to "public" as "informix";
grant index on "informix".pemmvalvula to "public" as "informix";
grant select on "informix".pemmcatemp to "public" as "informix";
grant update on "informix".pemmcatemp to "public" as "informix";
grant insert on "informix".pemmcatemp to "public" as "informix";
grant delete on "informix".pemmcatemp to "public" as "informix";
grant index on "informix".pemmcatemp to "public" as "informix";
grant select on "informix".pemmtipemp to "public" as "informix";
grant update on "informix".pemmtipemp to "public" as "informix";
grant insert on "informix".pemmtipemp to "public" as "informix";
grant delete on "informix".pemmtipemp to "public" as "informix";
grant index on "informix".pemmtipemp to "public" as "informix";
grant select on "informix".pemmfinca to "public" as "informix";
grant update on "informix".pemmfinca to "public" as "informix";
grant insert on "informix".pemmfinca to "public" as "informix";
grant delete on "informix".pemmfinca to "public" as "informix";
grant index on "informix".pemmfinca to "public" as "informix";
grant select on "informix".pemdordpro to "public" as "informix";
grant update on "informix".pemdordpro to "public" as "informix";
grant insert on "informix".pemdordpro to "public" as "informix";
grant delete on "informix".pemdordpro to "public" as "informix";
grant index on "informix".pemdordpro to "public" as "informix";
grant select on "informix".pemdboleta to "public" as "informix";
grant update on "informix".pemdboleta to "public" as "informix";
grant insert on "informix".pemdboleta to "public" as "informix";
grant delete on "informix".pemdboleta to "public" as "informix";
grant index on "informix".pemdboleta to "public" as "informix";
grant select on "informix".catdsolicitudnotas to "public" as "informix";
grant update on "informix".catdsolicitudnotas to "public" as "informix";
grant insert on "informix".catdsolicitudnotas to "public" as "informix";
grant delete on "informix".catdsolicitudnotas to "public" as "informix";
grant index on "informix".catdsolicitudnotas to "public" as "informix";
grant select on "informix".catemplxempr to "public" as "informix";
grant update on "informix".catemplxempr to "public" as "informix";
grant insert on "informix".catemplxempr to "public" as "informix";
grant delete on "informix".catemplxempr to "public" as "informix";
grant index on "informix".catemplxempr to "public" as "informix";
grant select on "informix".catitemxempr to "public" as "informix";
grant update on "informix".catitemxempr to "public" as "informix";
grant insert on "informix".catitemxempr to "public" as "informix";
grant delete on "informix".catitemxempr to "public" as "informix";
grant index on "informix".catitemxempr to "public" as "informix";
grant select on "informix".catmsolicitud to "public" as "informix";
grant update on "informix".catmsolicitud to "public" as "informix";
grant insert on "informix".catmsolicitud to "public" as "informix";
grant delete on "informix".catmsolicitud to "public" as "informix";
grant index on "informix".catmsolicitud to "public" as "informix";
grant select on "informix".glbmalmacen to "public" as "informix";
grant update on "informix".glbmalmacen to "public" as "informix";
grant insert on "informix".glbmalmacen to "public" as "informix";
grant delete on "informix".glbmalmacen to "public" as "informix";
grant index on "informix".glbmalmacen to "public" as "informix";
grant select on "informix".glbmitems to "public" as "informix";
grant update on "informix".glbmitems to "public" as "informix";
grant insert on "informix".glbmitems to "public" as "informix";
grant delete on "informix".glbmitems to "public" as "informix";
grant index on "informix".glbmitems to "public" as "informix";
grant select on "informix".glbmfam to "public" as "informix";
grant update on "informix".glbmfam to "public" as "informix";
grant insert on "informix".glbmfam to "public" as "informix";
grant delete on "informix".glbmfam to "public" as "informix";
grant index on "informix".glbmfam to "public" as "informix";
grant select on "informix".glbmtip to "public" as "informix";
grant update on "informix".glbmtip to "public" as "informix";
grant insert on "informix".glbmtip to "public" as "informix";
grant delete on "informix".glbmtip to "public" as "informix";
grant index on "informix".glbmtip to "public" as "informix";
grant select on "informix".pemmcli to "public" as "informix";
grant update on "informix".pemmcli to "public" as "informix";
grant insert on "informix".pemmcli to "public" as "informix";
grant delete on "informix".pemmcli to "public" as "informix";
grant index on "informix".pemmcli to "public" as "informix";
grant select on "informix".pemmboletaexp to "public" as "informix";
grant update on "informix".pemmboletaexp to "public" as "informix";
grant insert on "informix".pemmboletaexp to "public" as "informix";
grant delete on "informix".pemmboletaexp to "public" as "informix";
grant index on "informix".pemmboletaexp to "public" as "informix";
grant select on "informix".pemcboletaexp to "public" as "informix";
grant update on "informix".pemcboletaexp to "public" as "informix";
grant insert on "informix".pemcboletaexp to "public" as "informix";
grant delete on "informix".pemcboletaexp to "public" as "informix";
grant index on "informix".pemcboletaexp to "public" as "informix";
grant select on "informix".pemdboletaexp to "public" as "informix";
grant update on "informix".pemdboletaexp to "public" as "informix";
grant insert on "informix".pemdboletaexp to "public" as "informix";
grant delete on "informix".pemdboletaexp to "public" as "informix";
grant index on "informix".pemdboletaexp to "public" as "informix";
grant select on "informix".itemsxgrupo to "public" as "informix";
grant update on "informix".itemsxgrupo to "public" as "informix";
grant insert on "informix".itemsxgrupo to "public" as "informix";
grant delete on "informix".itemsxgrupo to "public" as "informix";
grant index on "informix".itemsxgrupo to "public" as "informix";
grant select on "informix".paises to "public" as "informix";
grant update on "informix".paises to "public" as "informix";
grant insert on "informix".paises to "public" as "informix";
grant delete on "informix".paises to "public" as "informix";
grant index on "informix".paises to "public" as "informix";
grant select on "informix".pemmcausa to "public" as "informix";
grant update on "informix".pemmcausa to "public" as "informix";
grant insert on "informix".pemmcausa to "public" as "informix";
grant delete on "informix".pemmcausa to "public" as "informix";
grant index on "informix".pemmcausa to "public" as "informix";
grant select on "informix".pemdboletacau to "public" as "informix";
grant update on "informix".pemdboletacau to "public" as "informix";
grant insert on "informix".pemdboletacau to "public" as "informix";
grant delete on "informix".pemdboletacau to "public" as "informix";
grant index on "informix".pemdboletacau to "public" as "informix";
grant select on "informix".pemmboletacau to "public" as "informix";
grant update on "informix".pemmboletacau to "public" as "informix";
grant insert on "informix".pemmboletacau to "public" as "informix";
grant delete on "informix".pemmboletacau to "public" as "informix";
grant index on "informix".pemmboletacau to "public" as "informix";
grant select on "informix".pemmboletaloc to "public" as "informix";
grant update on "informix".pemmboletaloc to "public" as "informix";
grant insert on "informix".pemmboletaloc to "public" as "informix";
grant delete on "informix".pemmboletaloc to "public" as "informix";
grant index on "informix".pemmboletaloc to "public" as "informix";
grant select on "informix".pemdboletaloc to "public" as "informix";
grant update on "informix".pemdboletaloc to "public" as "informix";
grant insert on "informix".pemdboletaloc to "public" as "informix";
grant delete on "informix".pemdboletaloc to "public" as "informix";
grant index on "informix".pemdboletaloc to "public" as "informix";
grant select on "informix".pemdcajemp to "public" as "informix";
grant update on "informix".pemdcajemp to "public" as "informix";
grant insert on "informix".pemdcajemp to "public" as "informix";
grant delete on "informix".pemdcajemp to "public" as "informix";
grant index on "informix".pemdcajemp to "public" as "informix";
grant select on "sistemas".glb_permxusr to "public" as "sistemas";
grant update on "sistemas".glb_permxusr to "public" as "sistemas";
grant insert on "sistemas".glb_permxusr to "public" as "sistemas";
grant delete on "sistemas".glb_permxusr to "public" as "sistemas";
grant index on "sistemas".glb_permxusr to "public" as "sistemas";
grant select on "sistemas".glb_programs to "public" as "sistemas";
grant update on "sistemas".glb_programs to "public" as "sistemas";
grant insert on "sistemas".glb_programs to "public" as "sistemas";
grant delete on "sistemas".glb_programs to "public" as "sistemas";
grant index on "sistemas".glb_programs to "public" as "sistemas";
grant select on "informix".pemmliquidacion to "public" as "informix";
grant update on "informix".pemmliquidacion to "public" as "informix";
grant insert on "informix".pemmliquidacion to "public" as "informix";
grant delete on "informix".pemmliquidacion to "public" as "informix";
grant index on "informix".pemmliquidacion to "public" as "informix";
grant select on "informix".pemdliquidacion to "public" as "informix";
grant update on "informix".pemdliquidacion to "public" as "informix";
grant insert on "informix".pemdliquidacion to "public" as "informix";
grant delete on "informix".pemdliquidacion to "public" as "informix";
grant index on "informix".pemdliquidacion to "public" as "informix";
grant select on "informix".pemmconsolidadoliq to "public" as "informix";
grant update on "informix".pemmconsolidadoliq to "public" as "informix";
grant insert on "informix".pemmconsolidadoliq to "public" as "informix";
grant delete on "informix".pemmconsolidadoliq to "public" as "informix";
grant index on "informix".pemmconsolidadoliq to "public" as "informix";
grant select on "informix".pemrelmconliq to "public" as "informix";
grant update on "informix".pemrelmconliq to "public" as "informix";
grant insert on "informix".pemrelmconliq to "public" as "informix";
grant delete on "informix".pemrelmconliq to "public" as "informix";
grant index on "informix".pemrelmconliq to "public" as "informix";
grant select on "informix".pemdconsolidadoliq to "public" as "informix";
grant update on "informix".pemdconsolidadoliq to "public" as "informix";
grant insert on "informix".pemdconsolidadoliq to "public" as "informix";
grant delete on "informix".pemdconsolidadoliq to "public" as "informix";
grant index on "informix".pemdconsolidadoliq to "public" as "informix";
grant select on "informix".pemmdesitem to "public" as "informix";
grant update on "informix".pemmdesitem to "public" as "informix";
grant insert on "informix".pemmdesitem to "public" as "informix";
grant delete on "informix".pemmdesitem to "public" as "informix";
grant index on "informix".pemmdesitem to "public" as "informix";
grant select on "informix".pemmboleta to "public" as "informix";
grant update on "informix".pemmboleta to "public" as "informix";
grant insert on "informix".pemmboleta to "public" as "informix";
grant delete on "informix".pemmboleta to "public" as "informix";
grant index on "informix".pemmboleta to "public" as "informix";
grant select on "informix".pemmordpro to "public" as "informix";
grant update on "informix".pemmordpro to "public" as "informix";
grant insert on "informix".pemmordpro to "public" as "informix";
grant delete on "informix".pemmordpro to "public" as "informix";
grant index on "informix".pemmordpro to "public" as "informix";


revoke usage on language SPL from public ;

grant usage on language SPL to public ;




alter table "informix".menu add constraint (foreign key (menpadre) 
    references "informix".menu  constraint "Administrador".fk_referen_menu);
    
alter table "informix".permiso add constraint (foreign key (pergrpid) 
    references "informix".grupo  constraint "informix".fk_ref2_grupo);
    
alter table "informix".permiso add constraint (foreign key (permenid) 
    references "informix".menu  constraint "informix".fk_ref3_menu);
    
alter table "informix".usuario add constraint (foreign key (usugrpid) 
    references "informix".grupo  constraint "informix".fk_ref1_grupo);
    
alter table "informix".pemmcatemp add constraint (foreign key 
    (idtipempaque) references "informix".pemmtipemp );
alter table "informix".pemdordpro add constraint (foreign key 
    (idordpro) references "informix".pemmordpro );
alter table "informix".pemdordpro add constraint (foreign key 
    (idfinca) references "informix".pemmfinca );
alter table "informix".pemdordpro add constraint (foreign key 
    (idestructura) references "informix".mestructura );
alter table "informix".pemdordpro add constraint (foreign key 
    (idvalvula) references "informix".pemmvalvula );
alter table "informix".pemdboleta add constraint (foreign key 
    (idboletaing) references "informix".pemmboleta );
alter table "informix".catdsolicitudnotas add constraint (foreign 
    key (idsolicitud) references "informix".catmsolicitud );
alter table "informix".catitemxempr add constraint (foreign key 
    (idalmacen,id_commemp) references "informix".glbmalmacen );
    
alter table "informix".catmsolicitud add constraint (foreign 
    key (idalmacen,id_commemp) references "informix".glbmalmacen 
    );
alter table "informix".glbmitems add constraint (foreign key 
    (idtipo,idfamilia) references "informix".glbmtip );
alter table "informix".glbmtip add constraint (foreign key (idfamilia) 
    references "informix".glbmfam );
alter table "informix".pemmcli add constraint (foreign key (pais) 
    references "informix".paises  constraint "informix".u_132_fk);
    
alter table "informix".pemmboletaexp add constraint (foreign 
    key (idboletaing) references "informix".pemmboleta );
alter table "informix".pemcboletaexp add constraint (foreign 
    key (idboletaexp) references "informix".pemmboletaexp );
alter table "informix".pemcboletaexp add constraint (foreign 
    key (idcliente) references "informix".pemmcli );
alter table "informix".pemdboletaexp add constraint (foreign 
    key (idboletaexp,idcliente) references "informix".pemcboletaexp 
    );
alter table "informix".itemsxgrupo add constraint (foreign key 
    (iditem) references "informix".glbmitems );
alter table "informix".pemmcausa add constraint (foreign key 
    (idtipo,idfamilia) references "informix".glbmtip  constraint 
    "informix".fk_tipo);
alter table "informix".pemdboletacau add constraint (foreign 
    key (idboletacau) references "informix".pemmboletacau );
alter table "informix".pemdboletacau add constraint (foreign 
    key (idcausa) references "informix".pemmcausa );
alter table "informix".pemmboletacau add constraint (foreign 
    key (idboletaing) references "informix".pemmboleta );
alter table "informix".pemmboletaloc add constraint (foreign 
    key (idcliente) references "informix".pemmcli  constraint 
    "informix".r147_182);
alter table "informix".pemmboletaloc add constraint (foreign 
    key (idboletaing) references "informix".pemmboleta );
alter table "informix".pemdboletaloc add constraint (foreign 
    key (idboletaloc) references "informix".pemmboletaloc );
alter table "informix".pemdcajemp add constraint (foreign key 
    (idcatempaque) references "informix".pemmcatemp );
alter table "informix".pemdcajemp add constraint (foreign key 
    (id_commemp) references "informix".commemp );
alter table "informix".pemdliquidacion add constraint (foreign 
    key (idliq) references "informix".pemmliquidacion );
alter table "informix".pemrelmconliq add constraint (foreign 
    key (idconliq) references "informix".pemmconsolidadoliq );
    
alter table "informix".pemrelmconliq add constraint (foreign 
    key (idliq) references "informix".pemmliquidacion );
alter table "informix".pemdconsolidadoliq add constraint (foreign 
    key (idconliq) references "informix".pemmconsolidadoliq );
    
alter table "informix".pemmboleta add constraint (foreign key 
    (tipocaja) references "informix".pemmcatemp );
alter table "informix".pemmboleta add constraint (foreign key 
    (idordpro,iddordpro) references "informix".pemdordpro );
alter table "informix".dsemanacal add constraint (foreign key 
    (lnksem) references "informix".msemanas );
alter table "informix".dsemanas add constraint (foreign key (lnksem) 
    references "informix".msemanas );
alter table "informix".msemanas add constraint (foreign key (idcliente) 
    references "informix".pemmcli );


