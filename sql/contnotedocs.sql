






{ TABLE "informix".contnotedocs row size = 325 number of columns = 6 index size = 31 }

create table "informix".contnotedocs 
  (
    contnote_num integer,
    contact_num integer,
    contdocs_id integer,
    contdocs_tipo char(1),
    contdocs_desc varchar(255),
    contdocs_doc byte,
    primary key (contnote_num,contdocs_id)  constraint "informix".contdocs_pk
  );

revoke all on "informix".contnotedocs from "public" as "informix";




alter table "informix".contnotedocs add constraint (foreign key 
    (contnote_num) references "informix".contnote  constraint 
    "informix".contdocs_fk_note);
alter table "informix".contnotedocs add constraint (foreign key 
    (contact_num) references "informix".contact  constraint "informix"
    .contdocs_fk_cont);


