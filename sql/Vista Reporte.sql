# Modificada el 4-Nov-2021
# Carlos Santizo

DROP VIEW visexcelnotariado;
CREATE VIEW visexcelnotariado 
(
 --DATOS INICIALES
 nomdoc, --Tipo de documento: Hipotecario
 numexp, --Numero de Expediente
 codpro, --Ges � Fisico
 numcre, --Numero de Ges
 nomreg, --Region
 numres, --No de Resoluci�n
 fecres, --Fecha de Resoluci�n
 numage, --Numero de Agencia
 nomage, --Nombre de Agencia
 nomasi, --Nombre Asistente responsable
 fecasi, --Fecha de Asignaci�n
 
 --DATOS ESPECIFICOS DE LA ESCRITURA
 nomnot, --Nombre Notario autorizante
 numesc, --Numero de Escritura
 fecesc, --Fecha de Escritura
 hojini, --Correlativo Inicial de la Hoja de protocolo
 hojfin, --Correlativo Final de la Hoja de protocolo
 hojin2, --Correlativo Inicial de la Hoja de protocolo 2
 hojfi2, --Correlativo Final de la Hoja de protocolo 2
 todipt, --Total de Hojas utilizadas
 todies, --Total de dias para escriturar
 
 --NOMBRE DEL USUARIO
 nomcli, --Nombre del cliente o Usuario
 nombre, --Nombre del tipo de credito o documento
 valcre, --Monto del Credito

 --DESGLOSE PARA COBRO
 honora, --Honorarios 
 totiva, --IVA
 timnot, --Timbre Notarial 2 por millar
 prites, --Primer Testimonio Q150
 tesesp, --Testimonio Especial Q150
 ivacom, --IVA Compra-Venta
 honbas, --Honorarios Base registro de la propiedad
 lugfin, --Lugar que ocupar� la Finca (0,1,2,3....)
 valfin, --Insc por el lugar que ocupara la finca en RGP Q50
 honexe, --Honorarios Excedente RGP 1.5 por millar
 redhon, --Redondeo Honorarios Excedente RGP
 canfin, --Cantidad de fincas a insc en RGP
 finext, --Finca extra a inscribir en RGP Q50
 gastot, --Gasto Total segundo RGP
 gasmun, --Gastos de reg Municipalidad
 difcob, --Diferencia monto Cobrado por garantia
 vtcoba, --toacob, --Total Cobrado reg de la prop base, exced, fincas adic
 canele, --Cantidad consultas Electronicas
 valele, --Valor por consultas Electronicas
 gascer, --Gastos de Certificacion del RGP Q100 c/u
 hojpro, --Hojas de Protocolo
 docext, --Documento Extra
 totcob, --Total a Cobrar
 --tototr, --??
 
 --PROCESO DE FORMALIZACI�N
 fevfir, --Fecha Envio a Firma
 frefir, --Regreso de Firma
 tdifir, --Total de Dias Firma
 finrpg, --Fecha Ingreso al RGP
 fegrpg, --Fecha Egreso del RGP
 nrerpg, --Numero de Recibo del RGP
 ndbrpg, --Numero de Documento / No de Boleta prenda RGP
 toparp, --Total Pagado RGP
 topaco, --Total Pagado vs Cobrado
 codrgo, --Codigo de Registro o muni que opera
 codarp, --Codigo procurador RGP
 todifo, --Total de d�as
 
 --REINGRESO AL RGP
 canrch, --Cantidad o numero de Rechazos
 totrch, --Total o monto de Rechazos
 ferech, --Fecha del Rechazo
 fecrei, --Fecha de Reingreso
 nrecrg, --No de Recibo del RGP
 ndocrg, --No de Documento del RGP
 fecdev, --Fecha de Devolucion
 
 --COMENTARIOS DEL PROCESO DE ELABORACION
 coment, --Comentarios
 
 --DATOS DEL PAGO Y FACTURACION - DEPOSITO COMPLETO
 --todidv, 
 --todich, 
 totpag, --Total Pagado
 numbol, --No de Boleta
 fecbol, --Fecha de la Boleta
 numdep, --Numero de dep�sito electr�nico
 
 --ESTADO DE CUENTA, HONORARIOS BUFETE Y GASTOS FORMALIZACION
 fecdeb, --Fecha del Debito
 nuboho, --Numero de Boleta de Honorarios
 modeho, --Monto del Debito de Honorarios
 nuboga, --Numero de Boleta de Gastos de formalizaci�n
 modega, --Monto de Gastos - debito
 nufade, --Numero de Factura Electronica por debito
 
 --ENVIO A PCL O BANCO
 lugenv, --Lugar de Envio
 fecenv, --Fecha de Envio
 fecrep, --Fecha de recepcion
 nucoev, --Numero de Correlativo de doc de Envio
 todiev, --numero Total de Dias de Envio
 diapro, --total de Dias utilizados en el Proceso 
 
 --ETAPA
 numeta, --Estatus
 
 --FECHA DE PAGO
 fpagas, --Fecha de pago al Asistente
 mpagno, --Mes de Pago al Notario
 observ  --Comentarios
 ) 
 as 
 
 SELECT 
   --DATOS INICIALES
   x1.nombre, 	--Tipo de Documento: HIPOTECARIO
   x0.numexp,		--Numero de Expediente
   CASE 
     WHEN (x0.codpro = 1 )  THEN 'GES'  
     WHEN (x0.codpro = 2 )  THEN 'FISICO'  
   END,				--Ges � Fisico
   x0.numcre, 		--Numero de Ges
   x2.city_name,	--Region
   x0.numres,		--Numero de Resoluci�n
   x0.fecres,		--Fecha de Resoluci�n
   x0.numage,		--No de Agencia
   trim(x3.nombre) || '-' || trim(x3.nomlug),		--Nombre de Agencia
   x4.nombre,		--Nombre Asistente responsable	
   x0.fecasi,		--Fecha de Asignaci�n
   
   --DATOS ESPECIFICOS DE LA ESCRITURA
   x5.user_name,	--Nombre Notario autorizante
   x0.numesc,		--Numero de Escritura
   x0.fecesc,		--Fecha de Escritura
   x0.hojini,		--Correlativo Inicial de la Hoja de protocolo
   x0.hojfin,		--Correlativo Final de la Hoja de protocolo	
   x0.hojin2,		--Correlativo Inicial de la Hoja de protocolo 2
   x0.hojfi2,		--Correlativo Final de la Hoja de protocolo	2
   x0.todipt,		--Total de Hojas utilizadas
   x0.todies,		--Total de dias para escriturar
   
   --NOMBRE DEL USUARIO
   x0.nomcli,		--Nombre del cliente o Usuario	
   x1.nombre,		--Nombre del tipo de credito o documento	
   x0.valcre,		--Monto del Credito
   
   --DESGLOSE PARA COBRO
   (select NVL(x6.totcob,0) from notariado_cob x6 where ((x6.numexp = x0.numexp) AND (x6.tipcob = 13))), --Honorarios	
   0,	--IVA  (Calcular en programa)
   (select NVL( x7.totcob,0) from notariado_cob x7  where (( x7.numexp = x0.numexp) AND ( x7.tipcob = 15))), --Timbre Notarial 2 por millar
   (select NVL( x8.totcob,0) from notariado_cob x8  where (( x8.numexp = x0.numexp) AND ( x8.tipcob = 1 ))), --Primer Testimonio Q150
   (select NVL( x9.totcob,0) from notariado_cob x9  where (( x9.numexp = x0.numexp) AND ( x9.tipcob = 2 ))), --Testimonio Especial Q150
   (select NVL(x10.totcob,0) from notariado_cob x10 where ((x10.numexp = x0.numexp) AND (x10.tipcob = 3 ))), --IVA Compra-Venta
   (select NVL(x11.totcob,0) from notariado_cob x11 where ((x11.numexp = x0.numexp) AND (x11.tipcob = 4 ))), --Honorarios Base registro de la propiedad
   --0, --Lugar que ocupar� la finca (0,1,2,3....) ??
   (select NVL(x13.cancob,0) from notariado_cob x13 where ((x13.numexp = x0.numexp) AND (x13.tipcob = 12))), --Lugar que ocupar� la finca (0,1,2,3....) 
   (select NVL(x13.totcob,0) from notariado_cob x13 where ((x13.numexp = x0.numexp) AND (x13.tipcob = 12))), --Insc por el lugar que ocupara la finca en RGP Q50
   (select NVL(x14.totcob,0) from notariado_cob x14 where ((x14.numexp = x0.numexp) AND (x14.tipcob = 16))), --Honorarios Excedente RGP 1.5 por millar
   (select round(NVL(x15.totcob,0),2) from notariado_cob x15 where ((x15.numexp = x0.numexp) AND (x15.tipcob = 16))), --Redondeo Honorarios Excedente RGP
   --(select NVL(x12.totcob,0) from notariado_cob x12 where ((x12.numexp = x0.numexp) AND (x12.tipcob = 9 ))), 	--Cantidad de fincas a insc en RGP
   (select NVL(x12.cancob,0) from notariado_cob x12 where ((x12.numexp = x0.numexp) AND (x12.tipcob = 9 ))), 	--Cantidad de fincas a insc en RGP
   (select NVL(x12.totcob,0) from notariado_cob x12 where ((x12.numexp = x0.numexp) AND (x12.tipcob = 9 ))), 	--Finca extra a inscribir en RGP Q50
   --0, 	--Finca extra a inscribir en RGP Q50
   (select NVL(x16.totcob,0) from notariado_cob x16 where ((x16.numexp = x0.numexp) AND (x16.tipcob = 5 ))), --Gasto Total segundo RGP
   --(select NVL(x17.totcob,0) from notariado_cob x17 where ((x17.numexp = x0.numexp) AND (x17.tipcob = 6 ))), --Gastos de reg Municipalidad
   (select NVL(x17.totcob,0) from notariado_cob x17 where ((x17.numexp = x0.numexp) AND (x17.tipcob = 20 ))), --Gastos de reg Municipalidad
   (select NVL(x18.totcob,0) from notariado_cob x18 where ((x18.numexp = x0.numexp) AND (x18.tipcob = 17))), --Diferencia monto Cobrado por garantia
   --x0.toparp , --??
   x0.vtcoba, --(select NVL(x20.totcob,0) from notariado_cob x20 where ((x20.numexp = x0.numexp) AND (x20.tipcob = 10))), --Total Cobrado reg de la prop base, exced, fincas adic
   (select NVL(x19.cancob,0) from notariado_cob x19 where ((x19.numexp = x0.numexp) AND (x19.tipcob = 10))), --Cantidad consultas Electronicas
   (select NVL(x19.totcob,0) from notariado_cob x19 where ((x19.numexp = x0.numexp) AND (x19.tipcob = 10))), --Valor por consultas Electronicas
   (select NVL(x19.valcob,0) from notariado_cob x19 where ((x19.numexp = x0.numexp) AND (x19.tipcob = 7 ))), --Gastos de Certificacion del RGP Q100 c/u
   
   (select NVL(x19.totcob,0) from notariado_cob x19 where ((x19.numexp = x0.numexp) AND (x19.tipcob = 11))), --Hojas de Protocolo
   (select NVL(x19.totcob,0) from notariado_cob x19 where ((x19.numexp = x0.numexp) AND (x19.tipcob = 8 ))), --Documento Extra
   x0.vtocob ,	--Total a Cobrar (Calcular en programa)
   
   --PROCESO DE FORMALIZACI�N
   x0.fevfir,	--Fecha Envio a Firma
   x0.frefir,	--Regreso de Firma
   x0.tdifir,	--Total de Dias Firma
   x0.finrpg,	--Fecha Ingreso al RGP
   x0.fegrpg,	--Fecha Egreso del RGP
   x0.nrerpg,	--Numero de Recibo del RGP
   x0.ndbrpg,	--Numero de Documento / No de Boleta prenda RGP
   x0.toparp,	--Total Pagado RGP
   x0.topaco,	--Total Pagado vs Cobrado
  (select NVL(x31.nombre,'Error en nombre de registro') from registros_ope x31 where (x31.codrgo = x0.codrgo)),	--Codigo de Registro o muni que opera
  (select NVL(x32.nombre,'Error en nombre de procurador') from asistentes_rp x32 where (x32.codarp = x0.codarp))  ,	--Codigo procurador RGP
   x0.todifo,	--Total de d�as
   
   --REINGRESO AL RGP
   x0.canrch,	--Cantidad o numero de Rechazos
   x0.totrch,	--Total o monto de Rechazos
   x0.ferech,	--Fecha del Rechazo
   x0.fecrei,	--Fecha de Reingreso
   x0.nrecrg,	--No de Recibo del RGP
   x0.ndocrg,	--No de Documento del RGP
   x0.fecdev,	--Fecha de Devolucion
   
   --COMENTARIOS DEL PROCESO DE ELABORACION
   --x0.coment,	
   x0.observ, --Comentarios
   --DATOS DEL PAGO Y FACTURACION - DEPOSITO COMPLETO
   --x0.todidv,	--??
   --x0.todich , --??
   x0.totpag,	--Total Pagado
   x0.numbol,	--No de Boleta
   x0.fecbol,	--Fecha de la Boleta
   x0.numdep,	--No de Factura Electr�nica
   
   --ESTADO DE CUENTA, HONORARIOS BUFETE Y GASTOS FORMALIZACION
   x0.fecdeb,	--Fecha del Debito
   x0.nuboho,	--Numero de Boleta de Honorarios
   x0.modeho,	--Monto del Debito de Honorarios
   x0.nuboga,	--Numero de Boleta de Gastos de formalizaci�n
   x0.modega,	--Monto de Gastos - debito
   x0.nufade,	--Numero de Factura Electronica por debito
   
   --ENVIO A PCL O BANCO
   x0.lugenv,	--Lugar de Envio
   x0.fecenv,	--Fecha de Envio
   x0.fecrep,	--Fecha de Recepcion
   x0.nucoev,	--Numero de Correlativo de doc de Envio
   x0.todiev,	--numero Total de Dias de Envio
   x0.diapro,	--total de Dias utilizados en el Proceso
   
   --ETAPA
   (select NVL(x40.nometa,'Error en nombre de etapa') from etapas_not x40 where (x40.numeta = x0.numeta)),	--Estatus
   
   --FECHA DE PAGO
   x0.fpagas,	--Fecha de pago al Asistente
   x0.mpagno,	--Mes de Pago al Notario
   x0.coment --Comentarios
    	
 
FROM notariado x0, tiposdocumento x1, city x2, 
  outer(agencias_bco  x3),
  outer(asistentes_nt x4),
  outer(users         x5) 
  
WHERE x1.tipdoc     = x0.tipdoc   
  AND x2.city_num   = x0.city_num 
  AND (x3.city_num    = x0.city_num )
  AND x3.numage     = x0.numage    
  AND x4.codasn     = x0.codasn    
  AND x5.usuid      = x0.codabo   
  
  ;  