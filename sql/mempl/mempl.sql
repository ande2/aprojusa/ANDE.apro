






{ TABLE "informix".mempl row size = 449 number of columns = 35 index size = 54 }
create table "informix".mempl 
  (
    empl_id serial not null ,
    empl_cod varchar(20),
    empl_nom varchar(25) not null ,
    empl_ape varchar(25) not null ,
    empl_apec varchar(25),
    empl_dir varchar(60),
    empl_zona smallint,
    empl_colonia varchar(50),
    empl_telefono varchar(20),
    empl_fnacimiento date,
    empl_sexo char(1),
    empl_dpi varchar(20),
    empl_rcedula varchar(3),
    empl_ncedula integer,
    empl_cedextendida varchar(30),
    empl_nacionalidad varchar(20),
    empl_nit varchar(20),
    empl_igss varchar(20),
    empl_estcivil char(1),
    empl_fechai date,
    esc_id integer,
    pla_id integer,
    dep_id integer,
    sdep_id integer,
    pue_id integer,
    tur_id integer,
    empl_marcareloj integer,
    empl_salario decimal(10,2),
    empl_salhor decimal(10,2),
    empl_mediopago char(1),
    empl_cuentabanco varchar(20),
    empl_fechab date,
    motbaj_id integer,
    est_id integer not null ,
    tipper_id integer,
    
    check (empl_sexo IN ('M' ,'F' )),
    
    check (empl_estcivil IN ('S' ,'C' ,'U' ,'V' ,'D' )),
    primary key (empl_id) 
  );
revoke all on "informix".mempl from "public" as "informix";




alter table "informix".mempl add constraint (foreign key (est_id) 
    references "informix".mest );
alter table "informix".mempl add constraint (foreign key (sdep_id) 
    references "informix".msdep );
alter table "informix".mempl add constraint (foreign key (esc_id) 
    references "informix".mesc );
alter table "informix".mempl add constraint (foreign key (pue_id) 
    references "informix".mpue );
alter table "informix".mempl add constraint (foreign key (motbaj_id) 
    references "informix".mmotbaj );


