grant dba to "informix";









{ TABLE "informix".andevarlog row size = 106 number of columns = 4 index size = 30 }

create table "informix".andevarlog 
  (
    andesesid char(25) not null ,
    parnombre varchar(25),
    parvalor varchar(50),
    fecha date,
    primary key (andesesid) 
  );

revoke all on "informix".andevarlog from "public" as "informix";

{ TABLE "informix".commemp row size = 531 number of columns = 12 index size = 9 }

create table "informix".commemp 
  (
    id_commemp serial not null ,
    nombre varchar(255) not null ,
    nit varchar(15),
    telefono varchar(20),
    direccion varchar(100),
    imagen varchar(100),
    estado integer,
    dbname varchar(8),
    espropia "informix".boolean,
    cajasprestamosaldo smallint,
    codigo varchar(5),
    iniciales varchar(8),
    primary key (id_commemp)  constraint "informix".pk_empresa
  );

revoke all on "informix".commemp from "public" as "informix";

{ TABLE "informix".glb_programs row size = 293 number of columns = 8 index size = 20 }

create table "informix".glb_programs 
  (
    codpro char(15) not null ,
    nompro varchar(100,1) not null ,
    dcrpro varchar(100,1) not null ,
    ordpro smallint not null ,
    actpro varchar(50,1),
    userid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (codpro)  constraint "informix".pkglbprograms
  );

revoke all on "informix".glb_programs from "public" as "informix";

{ TABLE "informix".glb_paramtrs row size = 349 number of columns = 7 index size = 13 }

create table "informix".glb_paramtrs 
  (
    numpar integer not null ,
    tippar integer not null ,
    nompar varchar(60,1),
    valchr varchar(255,1) not null ,
    userid varchar(15,1) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (numpar,tippar)  constraint "informix".pkglbparamtrs
  );

revoke all on "informix".glb_paramtrs from "public" as "informix";

{ TABLE "informix".glb_permxusr row size = 90 number of columns = 10 index size = 0 }

create table "informix".glb_permxusr 
  (
    codpro char(20) not null ,
    progid smallint not null ,
    userid char(15) not null ,
    activo smallint not null ,
    passwd char(20),
    fecini date,
    fecfin date,
    usuaid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null 
  );

revoke all on "informix".glb_permxusr from "public" as "informix";

{ TABLE "informix".users row size = 337 number of columns = 7 index size = 9 }

create table "informix".users 
  (
    user_id varchar(20) not null ,
    user_auth varchar(200),
    user_name varchar(100) not null ,
    user_status smallint not null ,
    usuid integer,
    usugrpid integer,
    city_num integer,
    primary key (usuid)  constraint "informix".pk_users
  );

revoke all on "informix".users from "public" as "informix";

{ TABLE "informix".datafilter row size = 343 number of columns = 5 index size = 77 }

create table "informix".datafilter 
  (
    user_id varchar(20) not null ,
    table_name varchar(50) not null ,
    last_mtime datetime year to fraction(3) not null ,
    temp_mtime datetime year to fraction(3),
    where_part varchar(250),
    primary key (user_id,table_name) 
  );

revoke all on "informix".datafilter from "public" as "informix";

{ TABLE "informix".city row size = 106 number of columns = 5 index size = 76 }

create table "informix".city 
  (
    city_num integer not null ,
    city_name varchar(30) not null ,
    city_country varchar(30) not null ,
    city_cuenta char(20),
    city_telefono char(20),
    unique (city_name,city_country) ,
    primary key (city_num) 
  );

revoke all on "informix".city from "public" as "informix";

{ TABLE "informix".grupo row size = 260 number of columns = 2 index size = 9 }

create table "informix".grupo 
  (
    grpid serial not null ,
    grpnombre varchar(255),
    primary key (grpid)  constraint "informix".pk_grupo
  );

revoke all on "informix".grupo from "public" as "informix";

{ TABLE "informix".menu row size = 1266 number of columns = 5 index size = 9 }

create table "informix".menu 
  (
    menid serial not null ,
    mennombre varchar(255),
    mencmd char(1000),
    mentipo smallint,
    menpadre integer,
    primary key (menid)  constraint "informix".pk_menu
  );

revoke all on "informix".menu from "public" as "informix";

{ TABLE "informix".permiso row size = 8 number of columns = 2 index size = 13 }

create table "informix".permiso 
  (
    pergrpid integer not null ,
    permenid integer not null ,
    primary key (pergrpid,permenid) 
  );

revoke all on "informix".permiso from "public" as "informix";

{ TABLE "informix".usuario row size = 776 number of columns = 5 index size = 9 }

create table "informix".usuario 
  (
    usuid serial not null ,
    usulogin varchar(255),
    usupwd varchar(255),
    usunombre varchar(255),
    usugrpid integer,
    primary key (usuid)  constraint "informix".pk_usuario
  );

revoke all on "informix".usuario from "public" as "informix";

{ TABLE "informix".contact row size = 2722 number of columns = 46 index size = 43 }

create table "informix".contact 
  (
    contact_num integer not null ,
    contact_cod_region integer,
    contact_city integer not null ,
    contact_estado_caso char(18),
    contact_operacion char(20) not null ,
    contact_producto varchar(18),
    contact_garantia varchar(40),
    contact_estado_op varchar(20),
    contact_dpi_cliente varchar(20),
    contact_name varchar(100) not null ,
    contact_street varchar(240),
    contact_tel_cliente varchar(240),
    contact_tel_casa varchar(10),
    contact_tel_cel varchar(10),
    contact_tel_trab varchar(10),
    contact_tel_otro varchar(10),
    contact_saldo_cap varchar(20),
    contact_cap_vencido varchar(20),
    contact_int_vencido varchar(20),
    contact_saldo_imo varchar(20),
    contact_otros varchar(20),
    contact_total varchar(20),
    contact_porc_serv varchar(20),
    contact_total_cobrar varchar(20),
    contact_fec_ult_pago varchar(20),
    contact_cuotas_mora varchar(20),
    contact_fec_mora varchar(20),
    contact_nom_fia1 varchar(240),
    contact_dir_fia1 varchar(240),
    contact_nom_fia2 varchar(240),
    contact_dir_fia2 varchar(240),
    contact_nom_fia3 varchar(240),
    contact_dir_fia3 varchar(240),
    contact_rec_muser varchar(20) not null ,
    contact_rec_mtime datetime year to fraction(3) not null ,
    contact_rec_mstat char(2) not null ,
    contact_when datetime year to fraction(3),
    contact_valid char(1) not null ,
    contact_num_m varchar(40),
    contact_num_w varchar(40),
    contact_num_h varchar(40),
    contact_user varchar(20) not null ,
    contact_loc_lon decimal(10,6),
    contact_loc_lat decimal(10,6),
    contact_photo_mtime datetime year to fraction(3),
    contact_photo byte,
    unique (contact_operacion)  constraint "informix".contoper,
    primary key (contact_num) 
  );

revoke all on "informix".contact from "public" as "informix";

{ TABLE "informix".tipologia row size = 66 number of columns = 4 index size = 66 }

create table "informix".tipologia 
  (
    cod_tipologia char(2) not null ,
    des_tipologia varchar(60),
    estado char(1),
    orden smallint,
    unique (des_tipologia) 
  );

revoke all on "informix".tipologia from "public" as "informix";

{ TABLE "informix".uno row size = 1006 number of columns = 3 index size = 0 }

create table "informix".uno 
  (
    cod smallint,
    des char(1000),
    fecha date
  );

revoke all on "informix".uno from "public" as "informix";

{ TABLE "informix".contnote row size = 1067 number of columns = 11 index size = 37 }

create table "informix".contnote 
  (
    contnote_num integer not null ,
    contnote_rec_muser varchar(20) not null ,
    contnote_rec_mtime datetime year to fraction(3) not null ,
    contnote_rec_mstat char(2) not null ,
    contnote_contact integer not null ,
    contnote_when datetime year to fraction(3) not null ,
    contnote_text char(1000),
    contnote_cod_tipologia char(2),
    contnote_fecha_promesa date,
    contnote_monto_promesa decimal(15,5),
    contnote_estado char(1),
    unique (contnote_contact,contnote_when) ,
    primary key (contnote_num) 
  );

revoke all on "informix".contnote from "public" as "informix";


grant select on "informix".andevarlog to "public" as "informix";
grant update on "informix".andevarlog to "public" as "informix";
grant insert on "informix".andevarlog to "public" as "informix";
grant delete on "informix".andevarlog to "public" as "informix";
grant index on "informix".andevarlog to "public" as "informix";
grant select on "informix".commemp to "public" as "informix";
grant update on "informix".commemp to "public" as "informix";
grant insert on "informix".commemp to "public" as "informix";
grant delete on "informix".commemp to "public" as "informix";
grant index on "informix".commemp to "public" as "informix";
grant select on "informix".glb_programs to "public" as "informix";
grant update on "informix".glb_programs to "public" as "informix";
grant insert on "informix".glb_programs to "public" as "informix";
grant delete on "informix".glb_programs to "public" as "informix";
grant index on "informix".glb_programs to "public" as "informix";
grant select on "informix".glb_paramtrs to "public" as "informix";
grant update on "informix".glb_paramtrs to "public" as "informix";
grant insert on "informix".glb_paramtrs to "public" as "informix";
grant delete on "informix".glb_paramtrs to "public" as "informix";
grant index on "informix".glb_paramtrs to "public" as "informix";
grant select on "informix".glb_permxusr to "public" as "informix";
grant update on "informix".glb_permxusr to "public" as "informix";
grant insert on "informix".glb_permxusr to "public" as "informix";
grant delete on "informix".glb_permxusr to "public" as "informix";
grant index on "informix".glb_permxusr to "public" as "informix";
grant select on "informix".users to "public" as "informix";
grant update on "informix".users to "public" as "informix";
grant insert on "informix".users to "public" as "informix";
grant delete on "informix".users to "public" as "informix";
grant index on "informix".users to "public" as "informix";
grant select on "informix".datafilter to "public" as "informix";
grant update on "informix".datafilter to "public" as "informix";
grant insert on "informix".datafilter to "public" as "informix";
grant delete on "informix".datafilter to "public" as "informix";
grant index on "informix".datafilter to "public" as "informix";
grant select on "informix".city to "public" as "informix";
grant update on "informix".city to "public" as "informix";
grant insert on "informix".city to "public" as "informix";
grant delete on "informix".city to "public" as "informix";
grant index on "informix".city to "public" as "informix";
grant select on "informix".grupo to "public" as "informix";
grant update on "informix".grupo to "public" as "informix";
grant insert on "informix".grupo to "public" as "informix";
grant delete on "informix".grupo to "public" as "informix";
grant index on "informix".grupo to "public" as "informix";
grant select on "informix".menu to "public" as "informix";
grant update on "informix".menu to "public" as "informix";
grant insert on "informix".menu to "public" as "informix";
grant delete on "informix".menu to "public" as "informix";
grant index on "informix".menu to "public" as "informix";
grant select on "informix".permiso to "public" as "informix";
grant update on "informix".permiso to "public" as "informix";
grant insert on "informix".permiso to "public" as "informix";
grant delete on "informix".permiso to "public" as "informix";
grant index on "informix".permiso to "public" as "informix";
grant select on "informix".usuario to "public" as "informix";
grant update on "informix".usuario to "public" as "informix";
grant insert on "informix".usuario to "public" as "informix";
grant delete on "informix".usuario to "public" as "informix";
grant index on "informix".usuario to "public" as "informix";
grant select on "informix".contact to "public" as "informix";
grant update on "informix".contact to "public" as "informix";
grant insert on "informix".contact to "public" as "informix";
grant delete on "informix".contact to "public" as "informix";
grant index on "informix".contact to "public" as "informix";
grant select on "informix".tipologia to "public" as "informix";
grant update on "informix".tipologia to "public" as "informix";
grant insert on "informix".tipologia to "public" as "informix";
grant delete on "informix".tipologia to "public" as "informix";
grant index on "informix".tipologia to "public" as "informix";
grant select on "informix".uno to "public" as "informix";
grant update on "informix".uno to "public" as "informix";
grant insert on "informix".uno to "public" as "informix";
grant delete on "informix".uno to "public" as "informix";
grant index on "informix".uno to "public" as "informix";
grant select on "informix".contnote to "public" as "informix";
grant update on "informix".contnote to "public" as "informix";
grant insert on "informix".contnote to "public" as "informix";
grant delete on "informix".contnote to "public" as "informix";
grant index on "informix".contnote to "public" as "informix";


create sequence "informix".contnote_seq increment by 1 maxvalue 9223372036854775807 minvalue 1000 cache 20  order;
alter sequence "informix".contnote_seq restart with 15969;

revoke all on "informix".contnote_seq from "public" as "informix";

create sequence "informix".contact_seq increment by 1 maxvalue 9223372036854775807 minvalue 2000 cache 20  order;
alter sequence "informix".contact_seq restart with 13942;

revoke all on "informix".contact_seq from "public" as "informix";


revoke usage on language SPL from public ;

grant usage on language SPL to public ;


grant select on "informix".contnote_seq to "public" as "informix";
grant select on "informix".contact_seq to "public" as "informix";


alter table "informix".contact add constraint (foreign key (contact_city) 
    references "informix".city );
alter table "informix".contnote add constraint (foreign key (contnote_contact) 
    references "informix".contact );


